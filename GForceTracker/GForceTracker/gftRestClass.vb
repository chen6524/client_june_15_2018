﻿Imports System.Management
Imports System.Security.Cryptography
Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Net
Imports System.Xml
Imports System.ComponentModel
Imports gForceTracker.frmNotify


Public Class gftRest

    Private gftCloudURL As String = "http://gft.gforcetracker.com/v73/resources/"       ' Address to the REST API http://gft.gforcetracker.com/resources 'res_test http://gdev.jstock.ca/resources
    Private gftPerfCloudURL As String = "http://gft.gforcetracker.com/v73/resources_perf" ' http://gdev.jstock.ca/resources_perf
    Private gftCloudSecureKey As String = "stakuRE=*CHUt*AgU7AmAChude4w+@r@"    ' Security Key **MUST MATCH SERVER**
    Private gftCloudSecureIV As String = "C?AQUnaC8aWU!&a6raCracHeva39eYAw"     ' Security IV Key **MUST MATCH SERVER**

    Public mToken As String = ""                                               ' Session Token for Cloud Session
    Private mUsername As String = ""
    Private mPassword As String = ""
    Private mDevices As Array
    Private mMID As String = ""
    Private mGID As String = ""

    Private mDeviceState As gftDeviceState

    Public Enum gftDeviceState
        DEVICE_NOT_FOUND            ' NOT FOUND
        DEVICE_NOT_REGISTERED       ' L
        DEVICE_REGISTERED           ' R
        DEVICE_REPLACED             ' W
        DEVICE_NOT_ACTIVE           ' D
        DEVICE_UNKNOWN              ' default fallthrough
    End Enum

    ReadOnly Property cloudToken As String
        Get
            Return mToken
        End Get
    End Property

    ReadOnly Property deviceState As gftDeviceState
        Get
            Return mDeviceState
        End Get
    End Property


    Public Function writeSessions(ByRef colSession As Collection, ByVal batchID As String, ByVal serial As String, bw As BackgroundWorker, Optional perf As Boolean = False) As String
        Dim reader As StreamReader
        Dim sResponse As String = Nothing

        Try

            Dim xmlSettings As XmlWriterSettings = New XmlWriterSettings()
            'xmlSettings.Indent = True

            ' lets write out a file first, then we will do a file upload instead of a regular POST
            Using writer As XmlWriter = XmlWriter.Create("xmltopost.xml", xmlSettings)
                writer.WriteStartDocument()
                writer.WriteStartElement("sessions")

                ' Loop through sessions
                For Each session As gftSession In colSession
                    'If session.iDuration < 15 Then
                    '    If MessageBox.Show("The session that is being uploaded is " & session.iDuration.ToString() & " minute(s) are you sure you would like to upload this?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    '        Return 0
                    '    End If
                    'End If
                    writer.WriteElementString("batch", batchID)
                    writer.WriteElementString("gid", serial)
                    writer.WriteStartElement("session")
                    writer.WriteElementString("date", TimeToUnix(session.dDate))
                    writer.WriteElementString("on", TimeToUnix(session.dOnTime))
                    writer.WriteElementString("off", TimeToUnix(session.dOffTime))
                    writer.WriteElementString("odate", TimeToUnix(session.dO_Date))
                    writer.WriteElementString("oon", TimeToUnix(session.dO_OnTime))
                    writer.WriteElementString("ooff", TimeToUnix(session.dO_OffTime))
                    writer.WriteElementString("fon", TimeToUnix(session.dO_FirstOnTime))
                    writer.WriteElementString("lon", TimeToUnix(session.dO_LastOnTime))
                    writer.WriteElementString("foff", TimeToUnix(session.dO_FirstOffTime))
                    If Not session.oldTime Is Nothing Then
                        writer.WriteElementString("c_from", TimeToUnix(session.oldTime))
                    End If
                    If Not session.newTime Is Nothing Then
                        writer.WriteElementString("c_to", TimeToUnix(session.newTime))
                    End If
                    writer.WriteElementString("onCnt", session.dSesOnCnt)
                    writer.WriteElementString("offCnt", session.dSesOffCnt)
                    writer.WriteElementString("impacts", session.iImpact)
                    writer.WriteElementString("alarms", session.iAlarm)
                    writer.WriteElementString("active", session.iActiveTime)
                    writer.WriteElementString("wth", session.iWTH)
                    writer.WriteElementString("type", "U")
                    writer.WriteElementString("iLocation0", session.iLocation(0))
                    writer.WriteElementString("iLocation1", session.iLocation(1))
                    writer.WriteElementString("iLocation2", session.iLocation(2))
                    writer.WriteElementString("iLocation3", session.iLocation(3))
                    If session.iMntInside Then
                        writer.WriteElementString("iMntInside", "1")
                    Else
                        writer.WriteElementString("iMntInside", "0")
                    End If
                    writer.WriteElementString("fw", session.fwVersion)
                    writer.WriteElementString("sw", session.swVersion)
                    writer.WriteElementString("batStatus", session.batStatus)
                    writer.WriteElementString("udate", TimeToUnix(session.uDate))
                    writer.WriteElementString("coefficient", session.dCoefficient)
                    For Each impact In session.aImpacts
                        writer.WriteStartElement("impact")
                        writer.WriteElementString("date", TimeToUnix(impact.dTime))
                        writer.WriteElementString("datems", impact.dTimeMS)
                        writer.WriteElementString("odate", TimeToUnix(impact.d_OTime))
                        writer.WriteElementString("odatems", impact.d_OTimeMS)
                        writer.WriteElementString("gforce", impact.iGForce)
                        writer.WriteElementString("hic", impact.iHIC)
                        writer.WriteElementString("gsi", impact.iGSI)
                        writer.WriteElementString("bric", impact.iBrIC)
                        writer.WriteElementString("mRotX", impact.iMaxRotX)
                        writer.WriteElementString("mRotY", impact.iMaxRotY)
                        writer.WriteElementString("mRotZ", impact.iMaxRotZ)
                        writer.WriteElementString("linX", impact.iLinX)
                        writer.WriteElementString("linY", impact.iLinY)
                        writer.WriteElementString("linZ", impact.iLinZ)
                        writer.WriteElementString("rotX", impact.iRotX)
                        writer.WriteElementString("rotY", impact.iRotY)
                        writer.WriteElementString("rotZ", impact.iRotZ)
                        writer.WriteElementString("rotR", impact.iRotR)
                        writer.WriteElementString("azimuth", impact.iAzimuth)
                        writer.WriteElementString("elevation", impact.iElevation)
                        writer.WriteElementString("rDataX", Convert.ToBase64String(impact.bRDataX))
                        writer.WriteElementString("rDataY", Convert.ToBase64String(impact.bRDataY))
                        writer.WriteElementString("rDataZ", Convert.ToBase64String(impact.bRDataZ))
                        writer.WriteElementString("dataX", Convert.ToBase64String(impact.bDataX))
                        writer.WriteElementString("dataY", Convert.ToBase64String(impact.bDataY))
                        writer.WriteElementString("dataZ", Convert.ToBase64String(impact.bDataZ))

                        writer.WriteElementString("rDataX_raw", Convert.ToBase64String(impact.bRDataX_raw))
                        writer.WriteElementString("rDataY_raw", Convert.ToBase64String(impact.bRDataY_raw))
                        writer.WriteElementString("rDataZ_raw", Convert.ToBase64String(impact.bRDataZ_raw))
                        writer.WriteElementString("dataX_raw", Convert.ToBase64String(impact.bDataX_raw))
                        writer.WriteElementString("dataY_raw", Convert.ToBase64String(impact.bDataY_raw))
                        writer.WriteElementString("dataZ_raw", Convert.ToBase64String(impact.bDataZ_raw))

                        writer.WriteElementString("b1", impact.bucket(0)) 'impact.bucket(0) * 256 + impact.bucket(1))
                        writer.WriteElementString("b2", impact.bucket(1))
                        writer.WriteElementString("b3", impact.bucket(2))
                        writer.WriteElementString("b4", impact.bucket(3))
                        writer.WriteElementString("b5", impact.bucket(4))
                        writer.WriteElementString("b6", impact.bucket(5))
                        writer.WriteElementString("b7", impact.bucket(6))
                        writer.WriteElementString("b8", impact.bucket(7))
                        writer.WriteElementString("b9", impact.bucket(8))
                        writer.WriteElementString("b10", impact.bucket(9))
                        writer.WriteElementString("exp_count", impact.bucket(10))
                        writer.WriteElementString("exp_power", impact.bucket(11))
                        writer.WriteEndElement()
                    Next
                    writer.WriteEndElement()
                Next
                writer.WriteEndDocument()
            End Using


            ' Copy it to the desktop
            '  If File.Exists("xmltopost.xml") = True Then
            'File.Copy("xmltopost.xml", Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\GFT-XML.xml", True)
            'MsgBox("GFT-XML.xml has been placed on your desktop.")
            'End If

            Dim cloudURL As String
            If perf Then
                cloudURL = gftPerfCloudURL
            Else
                cloudURL = gftCloudURL
            End If
            ' lets upload the file!
            Dim boundary As String = Guid.NewGuid().ToString().Replace("-", String.Empty)
            Dim request As HttpWebRequest = CType(WebRequest.Create(cloudURL & "/uploadSessionLarge"), HttpWebRequest)
            'request.ReadWriteTimeout = 20 * 60 * 1000 ' Timeout after 20 minutes
            ' request.ContentType = "application/xml"
            request.ContentType = "multipart/form-data; boundary=" + boundary
            request.Method = "POST"
            request.KeepAlive = True
            request.AllowWriteStreamBuffering = False
            Dim fileStream = New MemoryStream(102400)
            Dim fileWriter = New StreamWriter(fileStream)

            fileWriter.Write("--" + boundary + vbCrLf)
            fileWriter.Write("Content-Disposition: form-data; name=""{0}""; filename=""{1}""{2}", "file", "xmltopost.xml", vbCrLf)
            fileWriter.Write("Content-Type: text/xml " + vbCrLf + vbCrLf)
            fileWriter.Write(File.ReadAllText("xmltopost.xml"))
            fileWriter.Write(vbCrLf)

            fileWriter.Write("--{0}--{1}", boundary, vbCrLf)

            fileWriter.Flush()

            request.ContentLength = fileStream.Length
            Dim requestStream = request.GetRequestStream()
            'fileStream.WriteTo(requestStream)
            ' Read 4 KB at a time so we can track progress
            Debug.WriteLine("Filestream position: " & fileStream.Position)
            fileStream.Position = 0
            Using fileStream
                Dim ReadSoFar As Integer = 0
                Dim buffer(4096) As Byte
                Dim Bytesread As Int32 = fileStream.Read(buffer, 0, buffer.Length)
                Do While (Bytesread > 0)
                    ReadSoFar += Bytesread
                    requestStream.Write(buffer, 0, Bytesread)
                    Bytesread = fileStream.Read(buffer, 0, buffer.Length)
                    Debug.WriteLine("Upload Progress: " & CInt(Math.Round((ReadSoFar / (fileStream.Length)) * 100)))
                    bw.ReportProgress(10, CInt(Math.Round((ReadSoFar / (fileStream.Length)) * 75)))
                Loop
            End Using
            requestStream.Close()

            fileStream.Close()

            Dim response As HttpWebResponse = CType(request.GetResponse(), HttpWebResponse)

            Console.WriteLine("Received response with status {0} {1}.", response.StatusCode, response.StatusDescription)

            If response.StatusCode = HttpStatusCode.OK Then
                ' Get the response stream  
                reader = New StreamReader(response.GetResponseStream())
                ' Read it into a StringBuilder  
                sResponse = reader.ReadToEnd()
                Console.WriteLine(Now.ToString & vbTab & "RECEIVING REST REPLY: " & sResponse)
                bw.ReportProgress(10, 100)
            Else
                Console.WriteLine("Session Upload Failed to Cloud")
                Return False
            End If
        Catch e As WebException
            MsgBox("ERROR OCCURED UPLOADING TO CLOUD")
            Return False
        End Try

        Return True
    End Function

    'Public Function writeImpact(ByVal sSession As String, ByVal aImpact As gftImpactData) As String

    '    Dim param = New StringBuilder
    '    Dim sResponse As String = Nothing

    '    ' Add parameters for REST call
    '    param.Append("session=" + HttpUtility.UrlEncode(EncryptData(sSession)))
    '    param.Append("&date=" + HttpUtility.UrlEncode(EncryptData(TimeToUnix(aImpact.dTime))))
    '    param.Append("&datems=" + HttpUtility.UrlEncode(EncryptData(aImpact.dTimeMS)))
    '    param.Append("&gforce=" + HttpUtility.UrlEncode(EncryptData(aImpact.iGForce)))
    '    param.Append("&hic=" + HttpUtility.UrlEncode(EncryptData(aImpact.iHIC)))
    '    param.Append("&gsi=" + HttpUtility.UrlEncode(EncryptData(aImpact.iGSI)))
    '    param.Append("&linx=" + HttpUtility.UrlEncode(EncryptData(aImpact.iLinX)))
    '    param.Append("&liny=" + HttpUtility.UrlEncode(EncryptData(aImpact.iLinY)))
    '    param.Append("&linz=" + HttpUtility.UrlEncode(EncryptData(aImpact.iLinZ)))
    '    param.Append("&rotx=" + HttpUtility.UrlEncode(EncryptData(aImpact.iRotX)))
    '    param.Append("&roty=" + HttpUtility.UrlEncode(EncryptData(aImpact.iRotY)))
    '    param.Append("&rotz=" + HttpUtility.UrlEncode(EncryptData(aImpact.iRotZ)))
    '    param.Append("&rotr=" + HttpUtility.UrlEncode(EncryptData(aImpact.iRotR)))
    '    param.Append("&azimuth=" + HttpUtility.UrlEncode(EncryptData(aImpact.iAzimuth)))
    '    param.Append("&elevation=" + HttpUtility.UrlEncode(EncryptData(aImpact.iElevation)))
    '    param.Append("&rdatax=" + HttpUtility.UrlEncode(Convert.ToBase64String(aImpact.bRDataX)))
    '    param.Append("&rdatay=" + HttpUtility.UrlEncode(Convert.ToBase64String(aImpact.bRDataY)))
    '    param.Append("&rdataz=" + HttpUtility.UrlEncode(Convert.ToBase64String(aImpact.bRDataZ)))
    '    param.Append("&datax=" + HttpUtility.UrlEncode(Convert.ToBase64String(aImpact.bDataX)))
    '    param.Append("&datay=" + HttpUtility.UrlEncode(Convert.ToBase64String(aImpact.bDataY)))
    '    param.Append("&dataz=" + HttpUtility.UrlEncode(Convert.ToBase64String(aImpact.bDataZ)))
    '    param.Append("&mntinside=" + HttpUtility.UrlEncode(EncryptData(currentGftProfile.bMountInside)))

    '    Console.WriteLine(Now.ToString & vbTab & param.ToString())
    '    ' try to send the rest request
    '    sResponse = sendREST("/createImpact", param.ToString())

    '    ' Get Device State from XML Feed
    '    Dim xmlResponse As New XmlDocument
    '    Dim getXMLNode As XmlNode
    '    Dim impactID As String = ""
    '    xmlResponse.LoadXml(sResponse)
    '    getXMLNode = xmlResponse.SelectSingleNode("//impact/id")
    '    If getXMLNode IsNot Nothing Then
    '        impactID = DecryptData(getXMLNode.InnerText)
    '    End If

    '    Return impactID

    'End Function

    'Public Function login(ByVal sUsername As String, ByVal sPassword As String, ByVal bRememberMe As Boolean, ByVal bWithDevice As Boolean) As String
    '    Dim param = New StringBuilder
    '    Dim sResponse As String = Nothing

    '    ' Add parameters for REST call
    '    param.Append("username=" + HttpUtility.UrlEncode(EncryptData(sUsername)))
    '    param.Append("&password=" + HttpUtility.UrlEncode(EncryptData(sPassword)))
    '    param.Append("&mid=" + HttpUtility.UrlEncode(EncryptData(mMID)))
    '    param.Append("&gid=" + HttpUtility.UrlEncode(EncryptData(mGID)))
    '    If bRememberMe = False Then
    '        param.Append("&remember=" + HttpUtility.UrlEncode(EncryptData("NO")))
    '    Else
    '        param.Append("&remember=" + HttpUtility.UrlEncode(EncryptData("YES")))
    '    End If
    '    If bWithDevice = False Then
    '        param.Append("&device=" + HttpUtility.UrlEncode(EncryptData("NO")))
    '    Else
    '        param.Append("&device=" + HttpUtility.UrlEncode(EncryptData("YES")))
    '    End If

    '    ' try to send the rest request
    '    Try
    '        sResponse = sendREST("/login", param.ToString())

    '        Dim xmlResponse As New XmlDocument
    '        Dim getXMLNode As XmlNode
    '        Dim evalVal As String
    '        xmlResponse.LoadXml(sResponse)

    '        ' Get Login State from XML Feed
    '        getXMLNode = xmlResponse.SelectSingleNode("//login/status")
    '        If getXMLNode IsNot Nothing Then
    '            evalVal = DecryptData(getXMLNode.InnerText)
    '            If evalVal = "SUCCESS" Then
    '                ' Get security token 
    '                ' Get Device State from XML Feed
    '                getXMLNode = xmlResponse.SelectSingleNode("//login/token")
    '                If getXMLNode IsNot Nothing Then
    '                    evalVal = DecryptData(getXMLNode.InnerText)
    '                    mToken = evalVal
    '                End If

    '                Console.WriteLine(Now.ToString & vbTab & "Logged in Successfully")
    '                Return True
    '            Else
    '                Console.WriteLine(Now.ToString & vbTab & "Logged in Failed")
    '                Return False
    '            End If
    '        End If
    '        Return True
    '    Catch ex As Exception
    '        Throw
    '    End Try

    '    Return False

    'End Function


    Public Function calibrate(ByVal mq() As Int16, ByVal bMntPosition As Boolean, ByVal bMntFront As Boolean, ByVal bMntRear As Boolean, ByVal bMntLeft As Boolean, ByVal bMntRight As Boolean) As Boolean
        Dim param = New StringBuilder
        Dim sResponse As String = Nothing

        ' Add parameters for REST call
        param.Append("mid=" + HttpUtility.UrlEncode(EncryptData(mMID)))
        param.Append("&gid=" + HttpUtility.UrlEncode(EncryptData(mGID)))
        param.Append("&loc0=" + HttpUtility.UrlEncode(EncryptData(mq(0))))
        param.Append("&loc1=" + HttpUtility.UrlEncode(EncryptData(mq(1))))
        param.Append("&loc2=" + HttpUtility.UrlEncode(EncryptData(mq(2))))
        param.Append("&loc3=" + HttpUtility.UrlEncode(EncryptData(mq(3))))
        param.Append("&mnt=" + HttpUtility.UrlEncode(EncryptData(bMntPosition)))
        '  param.Append("&front=" + HttpUtility.UrlEncode(EncryptData(bMntUSB)))

        param.Append("&front=" + HttpUtility.UrlEncode(EncryptData(bMntFront)))
        'param.Append("&Rear=" + HttpUtility.UrlEncode(EncryptData(bMntRear))) ' Keep backward compatible 
        param.Append("&Left=" + HttpUtility.UrlEncode(EncryptData(bMntLeft)))
        param.Append("&Right=" + HttpUtility.UrlEncode(EncryptData(bMntRight)))


        'ByVal bMntFrontLeft As Boolean, ByVal bMntFrontRight As Boolean, ByVal bMntFrontDown As Boolean, ByVal bMntFrontUp As Boolean, ByVal bMntBackUp As Boolean, ByVal bMntBackDown As Boolean, ByVal bMntTopFront As Boolean, ByVal bMntTopBack As Boolean, ByVal bMntLeftRightBack As Boolean, ByVal bMntLeftRightFront As Boolean

        ' try to send the rest request
        Try


            sResponse = sendREST("/calibrate", param.ToString())

            Dim xmlResponse As New XmlDocument
            Dim getXMLNode As XmlNode
            Dim evalVal As String
            xmlResponse.LoadXml(sResponse)

            ' Get Login State from XML Feed
            getXMLNode = xmlResponse.SelectSingleNode("//calibrate/status")
            If getXMLNode IsNot Nothing Then
                evalVal = DecryptData(getXMLNode.InnerText)
                If evalVal = "SUCCESS" Then
                    Return True
                End If
            End If
            Return False
        Catch ex As Exception
            Throw
        End Try
        Return False

    End Function

    'Public Function getSports() As ArrayList
    '    Dim sResponse As String = Nothing

    '    ' try to send the rest request
    '    Try
    '        sResponse = sendREST("/getSports", "")

    '        Dim xmlResponse As New XmlDocument
    '        Dim getXMLNodeList As XmlNodeList
    '        Dim getXMLNode As XmlNode
    '        Dim retArray As New ArrayList
    '        xmlResponse.LoadXml(sResponse)

    '        ' Get Device State from XML Feed
    '        getXMLNodeList = xmlResponse.SelectNodes("//sports/sport")
    '        If getXMLNodeList IsNot Nothing Then
    '            For Each getXMLNode In getXMLNodeList
    '                retArray.Add(getXMLNode.InnerText)
    '            Next
    '        End If
    '        Return retArray
    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return Nothing
    'End Function

    Public Function createBatch() As String
        Dim param = New StringBuilder
        Dim sResponse As String = Nothing
        Dim sRetVal As String = ""

        param.Append("gid=" + HttpUtility.UrlEncode(EncryptData(currentGftProfile.sSerial)))

        ' try to send the rest request
        Try
            sResponse = sendREST("/createBatch", param.ToString())

            Dim xmlResponse As New XmlDocument
            Dim retArray As New ArrayList
            xmlResponse.LoadXml(sResponse)

            ' Process XML Feed
            sRetVal = DecryptData(xmlResponse.SelectSingleNode("//batch/id").InnerText)

            Return sRetVal
        Catch ex As Exception
            Throw
        End Try
        Return Nothing
    End Function

    Public Function completeBatch(ByVal batchId As String) As Boolean
        Dim param = New StringBuilder
        Dim sResponse As String = Nothing
        Dim sRetVal As String = ""

        param.Append("batch=" + HttpUtility.UrlEncode(EncryptData(batchId)))

        ' try to send the rest request
        Try
            sResponse = sendREST("/closeBatch", param.ToString())

            Dim xmlResponse As New XmlDocument
            Dim retArray As New ArrayList
            xmlResponse.LoadXml(sResponse)

            ' Process XML Feed
            Return True
        Catch ex As Exception
            Throw
        End Try
        Return Nothing
    End Function


    'Public Function getSportLevel() As ArrayList
    '    Dim sResponse As String = Nothing

    '    ' try to send the rest request
    '    Try
    '        sResponse = sendREST("/getSportLevel", "")

    '        Dim xmlResponse As New XmlDocument
    '        Dim getXMLNodeList As XmlNodeList
    '        Dim getXMLNode As XmlNode
    '        Dim retArray As New ArrayList
    '        xmlResponse.LoadXml(sResponse)

    '        ' Get Device State from XML Feed
    '        getXMLNodeList = xmlResponse.SelectNodes("//sports/level")
    '        If getXMLNodeList IsNot Nothing Then
    '            For Each getXMLNode In getXMLNodeList
    '                retArray.Add(getXMLNode.InnerText)
    '            Next
    '        End If
    '        Return retArray
    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return Nothing
    'End Function

    Public Sub deviceUpdated(ByVal sGID As String)
        Dim param = New StringBuilder
        Dim sResponse As String = Nothing
        If Not sGID Is Nothing Then
            Me.mGID = sGID
            ' Add parameters for REST call
            param.Append("gid=" + HttpUtility.UrlEncode(EncryptData(sGID)))
            Try
                sResponse = sendREST("/deviceUpdated", param.ToString())
            Catch ex As Exception
                Throw
            End Try
        End If
    End Sub

    Public Function checkDevice(ByVal sGID As String, gftProfile As gftProfile, gftBattVol As Integer, gftType As String) As Boolean
        Dim param = New StringBuilder
        Dim sResponse As String = Nothing
        Dim mntLoc As Byte = 0
        If Not Offline And Not needsFirmware Then
            Me.mGID = sGID
            If Not sGID Is Nothing And Not currentGftProfile Is Nothing Then
                ' Add parameters for REST call
                param.Append("gid=" + HttpUtility.UrlEncode(EncryptData(sGID)))
                param.Append("&bat_status=" + HttpUtility.UrlEncode(EncryptData(gftBattVol)))
                param.Append("&type=" + HttpUtility.UrlEncode(EncryptData(gftType)))

                ' try to send the rest request
                Try
                    sResponse = sendREST("/checkDevice", param.ToString())

                    Dim xmlResponse As New XmlDocument
                    Dim getXMLNode As XmlNode
                    Dim evalVal As String
                    xmlResponse.LoadXml(sResponse)

                    If Not currentGftProfile Is Nothing Then ' Check again to be sure the GFT was not unplugged, it could have happened while waiting for a server response..
                        ' Get Device State from XML Feed
                        getXMLNode = xmlResponse.SelectSingleNode("//device/state")
                        If getXMLNode IsNot Nothing Then
                            evalVal = DecryptData(getXMLNode.InnerText)
                            If evalVal = "NOT FOUND" Then
                                Me.mDeviceState = gftDeviceState.DEVICE_NOT_FOUND
                            ElseIf evalVal = "R" Then
                                Me.mDeviceState = gftDeviceState.DEVICE_REGISTERED
                            ElseIf evalVal = "L" Then
                                Me.mDeviceState = gftDeviceState.DEVICE_NOT_REGISTERED
                            ElseIf evalVal = "W" Then
                                Me.mDeviceState = gftDeviceState.DEVICE_REPLACED
                            ElseIf evalVal = "D" Then
                                Me.mDeviceState = gftDeviceState.DEVICE_NOT_ACTIVE
                            Else
                                Me.mDeviceState = gftDeviceState.DEVICE_UNKNOWN
                            End If
                        End If

                        frmNotify.sCloudFirmwareVer = DecryptData(xmlResponse.SelectSingleNode("//software/firmware").InnerText)
                        frmNotify.sCloudFirmwareVer3 = DecryptData(xmlResponse.SelectSingleNode("//software/firmware_3").InnerText)
                        frmNotify.sCloudFirmwareVer4 = DecryptData(xmlResponse.SelectSingleNode("//software/firmware_4").InnerText)
                        frmNotify.sCloudFirmwareVer3Perf = DecryptData(xmlResponse.SelectSingleNode("//software/firmware_3_perf").InnerText)
                        ' Get Device Settings
                        gftProfile.sOverrideDeviceType = CStr(DecryptData(xmlResponse.SelectSingleNode("//device/overrideDeviceType").InnerText))
                        gftProfile.iCloudThreshold = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/threshold").InnerText))
                        gftProfile.iCloudRecordThreshold = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/recordThreshold").InnerText))
                        gftProfile.iCloudHICAlarm = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/hicAlarm").InnerText))
                        gftProfile.iCloudGSIAlarm = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/gsiAlarm").InnerText))
                        gftProfile.iCloudRAMAlarm = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/ramAlarm").InnerText))
                        gftProfile.iCloudAlarmEnable = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/alarmEnable").InnerText))
                        gftProfile.iCloudAlarmAudio = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/alarmAudio").InnerText))
                        gftProfile.iCloudAlarmVisual = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/alarmVisual").InnerText))
                        gftProfile.iCloudAlarmInterlock = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/alarmInterlock").InnerText))
                        gftProfile.sCloudName = DecryptData(xmlResponse.SelectSingleNode("//device/deviceName").InnerText)
                        gftProfile.iCloudPlayerNumber = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/playerNum").InnerText))
                        gftProfile.iCloudSports = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/sport").InnerText))
                        gftProfile.iLocation(0) = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/loc0").InnerText))
                        gftProfile.iLocation(1) = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/loc1").InnerText))
                        gftProfile.iLocation(2) = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/loc2").InnerText))
                        gftProfile.iLocation(3) = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/loc3").InnerText))
                        gftProfile.bCloudTestMode = DecryptData(xmlResponse.SelectSingleNode("//device/testMode").InnerText)
                        gftProfile.bMountInside = DecryptData(xmlResponse.SelectSingleNode("//device/mountInside").InnerText)
                        ' gftProfile.bMountUSB = DecryptData(xmlResponse.SelectSingleNode("//device/mountFront").InnerText)
                        gftProfile.bMountUSBFront = DecryptData(xmlResponse.SelectSingleNode("//device/mountFront").InnerText)
                        gftProfile.bMountUSBRear = Not gftProfile.bMountUSBFront
                        'old calibaration of rear option will be all false 
                        'gftProfile.bMountUSBRear = DecryptData(xmlResponse.SelectSingleNode("//device/mountRear").InnerText)

                        gftProfile.bMountUSBLeft = DecryptData(xmlResponse.SelectSingleNode("//device/mountLeft").InnerText)
                        gftProfile.bMountUSBRight = DecryptData(xmlResponse.SelectSingleNode("//device/mountRight").InnerText)
                        'gftProfile.bMountUSBBackUp = DecryptData(xmlResponse.SelectSingleNode("//device/mountBackUp").InnerText)


                        gftProfile.bCloudWirelessEnabled = DecryptData(xmlResponse.SelectSingleNode("//device/wirelessEnable").InnerText)

                        '      gftProfile.mntLoc = CByte(If(gftProfile.bMountUSB, GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK, 0))
                        '     gftProfile.mntLoc += CByte(If(gftProfile.bMountInside, GFT.GFT_MNT_LOC_INOUTMSK, 0))

                        Dim presetCalibration As Boolean = DecryptData(xmlResponse.SelectSingleNode("//device/cpreset").InnerText)
                        If presetCalibration Then
                            Dim cLoc0 As Integer = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/cloc0").InnerText))
                            Dim cLoc1 As Integer = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/cloc1").InnerText))
                            Dim cLoc2 As Integer = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/cloc2").InnerText))
                            Dim cLoc3 As Integer = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/cloc3").InnerText))
                            Dim cMntLoc As Integer = CInt(DecryptData(xmlResponse.SelectSingleNode("//device/cmntLoc").InnerText))
                            Dim cMntInside As Boolean = DecryptData(xmlResponse.SelectSingleNode("//device/cmountInside").InnerText)
                            Dim cMntUSBFront As Boolean = DecryptData(xmlResponse.SelectSingleNode("//device/cmountFront").InnerText)
                            Dim cMntUSBRear As Boolean = Not cMntUSBFront

                            Dim cMntUSBLeft As Boolean = DecryptData(xmlResponse.SelectSingleNode("//device/cmountLeft").InnerText)
                            Dim cMntUSBRight As Boolean = DecryptData(xmlResponse.SelectSingleNode("//device/cmountRight").InnerText)
                            Dim cProx As Integer = 0
                            Dim cProx_3 As Boolean = DecryptData(xmlResponse.SelectSingleNode("//device/cprox").InnerText)
                            Dim cProx_3s As Integer = DecryptData(xmlResponse.SelectSingleNode("//device/cprox_3s").InnerText)
                            Dim cCoefficient As Byte = CByte(CDbl(DecryptData(xmlResponse.SelectSingleNode("//device/ccoefficient").InnerText)) * 100) ' Stored on GFT as value between 0 and 255, stored in the cloud as decimal
                            If currentGftProfile.hwMajor >= 4 Then
                                cProx = cProx_3s ' 3S supports more modes
                            Else
                                If cProx_3 Then ' 3 and below support simple on/off - We are explicitly setting this because VB.Net is silly and says boolean true = -1; so we can't just do an implicit cast..
                                    cProx = 1
                                Else
                                    cProx = 0
                                End If
                                cCoefficient = CByte(100) ' These GFT's can't store a mount coefficient as the firmware doesn't support it
                            End If
                            If cMntLoc >= 0 Then
                                If (currentGftProfile.bMountCoefficient <> cCoefficient Or currentGftProfile.bProximityEnabled <> cProx Or currentGftProfile.bMountInside <> cMntInside Or cMntLoc <> currentGftProfile.mntLoc Or currentGftProfile.iLocation(0) <> cLoc0 Or currentGftProfile.iLocation(1) <> cLoc1 Or currentGftProfile.iLocation(2) <> cLoc2 Or currentGftProfile.iLocation(3) <> cLoc3) And Not ReConfig Then
                                    ReConfig = True
                                    Debug.WriteLine("Device is set for Preset Calibration")
                                    gMessage.MessageBox("Device Calibration Update Requred. The attached device will be updated with a pre-set Calibration.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    System.Threading.Thread.Sleep(5000) ' Take a nap..
                                    gUsbComm.calibrateGFTPreset(cLoc0, cLoc1, cLoc2, cLoc3, cMntInside, cMntUSBFront, cMntUSBRear, cMntUSBLeft, cMntUSBRight, cProx, cMntLoc, cCoefficient)
                                    ReConfig = False
                                End If
                            Else
                                If (currentGftProfile.bMountCoefficient <> cCoefficient Or currentGftProfile.bProximityEnabled <> cProx Or currentGftProfile.bMountInside <> cMntInside Or currentGftProfile.bMountUSBFront <> cMntUSBFront Or currentGftProfile.bMountUSBRear <> cMntUSBRear Or currentGftProfile.bMountUSBLeft <> cMntUSBLeft Or currentGftProfile.bMountUSBRight <> cMntUSBRight Or currentGftProfile.iLocation(0) <> cLoc0 Or currentGftProfile.iLocation(1) <> cLoc1 Or currentGftProfile.iLocation(2) <> cLoc2 Or currentGftProfile.iLocation(3) <> cLoc3) And Not ReConfig Then
                                    ReConfig = True
                                    Debug.WriteLine("Device is set for Preset Calibration")
                                    gMessage.MessageBox("Device Calibration Update Requred. The attached device will be updated with a pre-set Calibration.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    System.Threading.Thread.Sleep(5000) ' Take a nap..
                                    gUsbComm.calibrateGFTPreset(cLoc0, cLoc1, cLoc2, cLoc3, cMntInside, cMntUSBFront, cMntUSBRear, cMntUSBLeft, cMntUSBRight, cProx, cMntLoc, cCoefficient)
                                    ReConfig = False
                                End If
                            End If
                        Else
                        End If
                    End If

                    Return True
                Catch ex As Exception
                    Throw
                End Try

            End If
        End If
        Return False
    End Function

    Public Function checkVersion() As Boolean
        Dim param = New StringBuilder
        Dim sResponse As String = Nothing

        ' try to send the rest request
        Try
            sResponse = sendREST("/checkVersion", param.ToString())

            Dim xmlResponse As New XmlDocument
            xmlResponse.LoadXml(sResponse)

            frmNotify.sCloudSoftwareVer = DecryptData(xmlResponse.SelectSingleNode("//software/software").InnerText)
            frmNotify.sCloudFirmwareVer = DecryptData(xmlResponse.SelectSingleNode("//software/firmware").InnerText)
            frmNotify.sCloudFirmwareVer3 = DecryptData(xmlResponse.SelectSingleNode("//software/firmware_3").InnerText)
            frmNotify.sCloudFirmwareVer4 = DecryptData(xmlResponse.SelectSingleNode("//software/firmware_4").InnerText)
            frmNotify.sCloudFirmwareVer3Perf = DecryptData(xmlResponse.SelectSingleNode("//software/firmware_3_perf").InnerText)
            Return True
        Catch ex As Exception
            Throw
        End Try

        Return False
    End Function

    Public Function sendREST(ByVal sRoute As String, ByVal sParams As String) As String
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader
        Dim sResponse As String = Nothing
        Dim byteData() As Byte
        Dim postStream As Stream = Nothing

        Debug.WriteLine(Now.ToString & vbTab & "**** REST TRANSACTION START ****")
        Debug.WriteLine(gftCloudURL & sRoute)
        Debug.WriteLine("PARAMS: " & sParams)

        If gftCloudURL Is Nothing Then Throw New ArgumentNullException("gftCloudURL")

        Try
            ' Create and initialize the web request  
            request = DirectCast(WebRequest.Create(gftCloudURL & sRoute), HttpWebRequest)
            request.UserAgent = My.Application.Info.AssemblyName & " v" & My.Application.Info.Version.Build
            request.KeepAlive = True
            request.Timeout = 100 * 1000
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"
            request.Proxy = Nothing

            byteData = UTF8Encoding.UTF8.GetBytes(sParams)
            request.ContentLength = byteData.Length
            Try
                postStream = request.GetRequestStream()
                postStream.Write(byteData, 0, byteData.Length)
            Finally
                If Not postStream Is Nothing Then postStream.Close()
            End Try

            Try
                ' Get response  
                response = DirectCast(request.GetResponse(), HttpWebResponse)

                ' Get the response stream  
                reader = New StreamReader(response.GetResponseStream())
                ' Read it into a StringBuilder  
                sResponse = reader.ReadToEnd()

                Console.WriteLine(Now.ToString & vbTab & "RECEIVING REST REPLY: " & sResponse)
            Finally
                If Not response Is Nothing Then response.Close()
            End Try
        Catch wex As WebException
            ' This exception will be raised if the server didn't return 200 - OK  
            ' Try to retrieve more information about the network error  
            If Not wex.Response Is Nothing Then
                Dim errorResponse As HttpWebResponse = Nothing
                Try
                    errorResponse = DirectCast(wex.Response, HttpWebResponse)
                    Console.WriteLine(Now.ToString & vbTab & _
                      "The server returned '{0}' with the status code {1} ({2:d}).", _
                      errorResponse.StatusDescription, errorResponse.StatusCode, _
                      errorResponse.StatusCode)
                Finally
                    If Not errorResponse Is Nothing Then errorResponse.Close()
                End Try
            End If
        End Try
        Console.WriteLine(Now.ToString & vbTab & "**** REST TRANSACTION END ****")
        Return sResponse
    End Function

    Public Function DecryptData(ByVal sEncryptedString As String) As String
        Dim myRijndael As New RijndaelManaged
        Dim returnVal As String
        myRijndael.Padding = PaddingMode.Zeros
        myRijndael.Mode = CipherMode.CBC
        myRijndael.KeySize = 256
        myRijndael.BlockSize = 256

        Dim key() As Byte
        Dim IV() As Byte

        key = System.Text.Encoding.ASCII.GetBytes(gftCloudSecureKey)
        IV = System.Text.Encoding.ASCII.GetBytes(gftCloudSecureIV)

        Dim decryptor As ICryptoTransform = myRijndael.CreateDecryptor(key, IV)

        Dim sEncrypted As Byte() = Convert.FromBase64String(sEncryptedString)

        Dim fromEncrypt() As Byte = New Byte(sEncrypted.Length) {}

        Dim msDecrypt As New MemoryStream(sEncrypted)
        Dim csDecrypt As New CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)

        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length)

        returnVal = System.Text.Encoding.UTF8.GetString(fromEncrypt).Trim(Chr(0)).ToString()
        returnVal = returnVal.ToString().TrimEnd("\0")

        Return returnVal

    End Function

    Public Function EncryptData(ByVal sToEncrypt As String) As String
        Dim myRijndael As New RijndaelManaged
        myRijndael.Padding = PaddingMode.Zeros
        myRijndael.Mode = CipherMode.CBC
        myRijndael.KeySize = 256
        myRijndael.BlockSize = 256

        Dim encrypted() As Byte
        Dim toEncrypt() As Byte
        Dim key() As Byte
        Dim IV() As Byte

        key = System.Text.Encoding.ASCII.GetBytes(gftCloudSecureKey)
        IV = System.Text.Encoding.ASCII.GetBytes(gftCloudSecureIV)

        Dim encryptor As ICryptoTransform = myRijndael.CreateEncryptor(key, IV)

        Dim msEncrypt As New MemoryStream()
        Dim csEncrypt As New CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)

        toEncrypt = System.Text.Encoding.ASCII.GetBytes(sToEncrypt)

        csEncrypt.Write(toEncrypt, 0, toEncrypt.Length)
        csEncrypt.FlushFinalBlock()

        encrypted = msEncrypt.ToArray()

        Return (Convert.ToBase64String(encrypted))
    End Function

    Public Function GetMachineID() As String

        If Me.mMID Is Nothing Or Me.mMID = "" Then
            Dim objMOS As ManagementObjectSearcher
            Dim objMOC As ManagementObjectCollection
            Dim objMO As System.Management.ManagementObject
            Dim sMachineID As String = Nothing

            Try
                objMOS = New ManagementObjectSearcher("Select * From Win32_Processor")
                objMOC = objMOS.Get
                For Each objMO In objMOC
                    sMachineID += CStr(objMO("ProcessorID"))
                Next
                Return sMachineID

            Catch ex As Exception
                Return Nothing
            End Try
        End If

        Return Me.mMID
    End Function

    Public Function getMID() As String
        ' Get the machine ID
        Me.mMID = GetMachineID()
        Return Me.mMID
    End Function

    Public Function TimeToUnix(ByVal dteDate As Date) As String
        Dim stDate As DateTime
        stDate = #1/1/1970#
        'dteDate = dteDate.ToUniversalTime ' GFT dates are returned as UTC already
        TimeToUnix = DateDiff(DateInterval.Second, stDate, dteDate)
    End Function

    Public Function DownloadFile(ByVal _URL As String, ByVal _SaveAs As String) As Boolean
        Try
            Dim _WebClient As New System.Net.WebClient()
            ' Downloads the resource with the specified URI to a local file.
            _WebClient.DownloadFile(_URL, _SaveAs)
            Return True
        Catch _Exception As Exception
            ' Error
            Console.WriteLine(Now.ToString & vbTab & "Exception caught in process: {0}", _Exception.ToString())
        End Try
        Return False
    End Function

    Public Sub New()
        Debug.WriteLine("STARTING NEW REST THREAD")
    End Sub
End Class

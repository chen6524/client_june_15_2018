﻿Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Threading
Imports System.Web
Imports System.Net
Imports gForceTracker.gftRest
Imports WirelessGFTViewer
Imports gForceTrackerWireless


Public Class frmNotify
    Public Shared currentGftProfile As gftProfile
    Public Shared rest As New gftRest
    Public Shared gUsbComm As gUsbComm
    Public Shared gUsbCommOld As gUsbCommOld
    Public Shared gUsbCommPerformance As gUsbCommPerformance
    Public Shared sFirmwareFile As String = Application.StartupPath & "\gft_fw.bin"
    Public Shared sFirmwareFile_3 As String = Application.StartupPath & "\gft_fw_3.bin"
    Public Shared sFirmwareFile_3_Perf As String = Application.StartupPath & "\gft_fw_3_perf.bin"
    Public Shared sFirmwareFile_4 As String = Application.StartupPath & "\gft_fw_4.bin"
    Public Shared sUpdateFirmwareFile As String = ""
    Public Shared newUser As Boolean = False
    Public Shared readWirelessDB As readWirelessDB
    Public Shared UploadSVSessions As UploadSVSessions

    Private eDownload As downloadType = downloadType.NONE
    Private HasWirelessViewer As Boolean = False

    Private firmware_download_cnt As Integer = 0

    Public Shared myHandle As IntPtr

    Public Shared systemShutdown As Boolean = False

    Public Shared Offline As Boolean = False
    Public Shared needsCalibration As Boolean = False
    Public Shared needsSettings As Boolean = False
    Public Shared needsSettingsReasons As String = ""
    Public Shared ReConfig As Boolean = False
    Public Shared doingFWUpdate As Boolean = False
    Public Shared fwUpdateFailed As Boolean = False
    Public Shared doingConfig As Boolean = False
    Public Shared didConfig As Boolean = False
    Public Shared didConfigSerial As String = ""
    Public Shared didConfigTime As DateTime = DateTime.MinValue
    Public Shared needsUpdate As Boolean = False
    Public Shared needsFirmware As Boolean = False
    Public Shared sCloudSoftwareVer As String = ""
    Public Shared sCloudFirmwareVer As String = ""
    Public Shared sCloudPerformanceFirmwareVer As String = ""

    Public Shared sCloudFirmwareVer3 As String = ""
    Public Shared sCloudFirmwareVer3Perf As String = ""
    Public Shared sCloudFirmwareVer4 As String = ""
    Public Shared WirelessViewer As Form1
    Public Shared WirelessViewerOpen As Boolean = False
    Public Shared timeDiff As TimeSpan = Nothing ' The difference between the GFT's clock and the computers clock

    Public Shared gftType As String = "Impact"

    Public fwUpdateAttempts As Integer = 0

    Public Shared gMessage As gftMessage

    Private Enum downloadType
        NONE
        GFT_SOFTWARE
        GFT_FIRMARE
    End Enum

    Public Sub TestDBRead()
        'Dim readWirelessDB As readWirelessDB = New readWirelessDB()
        'readWirelessDB.dbToColSession("000")

        UploadSVSessions = New UploadSVSessions()
        UploadSVSessions.ShowDialog()

    End Sub

    Public Sub UploadSVData()
        If Not bwWirelessData.IsBusy Then bwWirelessData.RunWorkerAsync()
        'Perhaps we should allow events to be raised!?
        For Each c As Control In UploadSVSessions.Controls
            c.Enabled = False
        Next
        While bwWirelessData.IsBusy
            'Thread.Sleep(10)
            'UploadSVSessions.Refresh()

            Application.DoEvents()
        End While

        For Each c As Control In UploadSVSessions.Controls
            c.Enabled = True
        Next
    End Sub

    Public Sub SessionIsEnding(ByVal sender As Object, ByVal e As Microsoft.Win32.SessionEndingEventArgs)
        systemShutdown = True
        Select Case e.Reason
            Case Microsoft.Win32.SessionEndReasons.Logoff
            Case Microsoft.Win32.SessionEndReasons.SystemShutdown
        End Select
    End Sub

    Private Sub CloseWirelessViewer()
        If WirelessViewerOpen Then
            WirelessViewer.Close()
            WirelessViewer.DisposeHub()
            WirelessViewerOpen = False
            WirelessViewer.Dispose()
        End If
    End Sub

    Private Sub menuExit_Click(sender As Object, e As EventArgs) Handles menuExit.Click
        If MsgBox("Are you sure you want to exit the GForce Tracker software?" & vbCrLf & vbCrLf & "You will not be able to communicate with a GFT device until you reopen this program.", MsgBoxStyle.YesNo, "Close GForce Tracker") = MsgBoxResult.Yes Then
            If HasWirelessViewer Then
                If WirelessViewerOpen Then
                    CloseWirelessViewer() ' Moved to seperate subroutine to avoid JIT compilation issues if WirelessViewer dll is not present
                End If
            End If
            Me.Close()
            Application.Exit()
        End If
    End Sub

    Private Sub menuSite_Click(sender As Object, e As EventArgs) Handles menuSite.Click
        Dim wndName As String = "| GForce Tracker"
        Dim ptr As IntPtr
        For Each proc As Process In Process.GetProcesses()
            If proc.MainWindowTitle.Contains(wndName) Then
                ptr = proc.MainWindowHandle
            End If
        Next
        If ptr <> 0 Then
            BringWindowToTop(ptr)
            ShowWindow(ptr, SW_SHOWMAXIMIZED)
        Else
            System.Diagnostics.Process.Start("http://gft.gforcetracker.com")
        End If
    End Sub

    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal _
    lpClassName As String, ByVal lpWindowName As String) As Long
    Private Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal _
    nCmdShow As Long) As Long
    Private Const SW_SHOWMAXIMIZED = 3

    <DllImport("user32.dll", SetLastError:=True)> _
    Private Shared Function BringWindowToTop(ByVal hwnd As IntPtr) As Boolean
    End Function

    Private Sub frmNotify_Activated(sender As Object, e As EventArgs) Handles Me.Activated
    End Sub


    Public Sub unPlugUsb()
        If bwUploadData.IsBusy Then
            bwUploadData.CancelAsync()
        End If
        If bwCheckDevice.IsBusy Then
            bwCheckDevice.CancelAsync()
        End If
        If frmCalibrate_new.Enabled Then frmCalibrate_new.Close()
        If frmDiagnostics.Enabled Then frmDiagnostics.Close()

        menuCalibrate.Enabled = False
        menuDiagnostics.Enabled = False
        menuWireless.Enabled = True
        currentGftProfile = Nothing

        gUsbComm.gftStat = 0
        frmMain.timerBatteryMon.Stop()
        frmMain.timerBatteryImage.Enabled = False

        frmMain.timerStatus.Stop()

        needsSettings = False
        needsFirmware = False
        If (didConfig = False Or didConfigTime < DateTime.UtcNow.AddSeconds(-15)) Then
            unPlugUsbUI()
        Else
            frmMain.lblStatus.Text = "Please Wait - Your Device is Rebooting"
            frmMain.picBattery.Hide()
        End If
    End Sub

    Private Sub notifyIcon_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles notifyIcon.MouseDoubleClick
        If Not WirelessViewer Is Nothing Then
            If WirelessViewer.WirelessViewerOpen Then
                Return
            End If
        End If

        If frmMain.WindowState = FormWindowState.Minimized Or frmMain.Visible = False Then
            frmMain.ShowMe()
        Else
            'frmMain.WindowState = FormWindowState.Minimized ' Minimize immediately...
            frmMain.ShowInTaskbar = False
            frmMain.hideMe(True)
        End If
    End Sub

    Private Sub menuUpdateSidelineDB_Click(sender As Object, e As EventArgs) Handles menuUpdateSidelineDB.Click
        UploadSVSessions = New UploadSVSessions()
        UploadSVSessions.ShowDialog()
    End Sub

    Private Sub menuDiagnostics_Click(sender As Object, e As EventArgs) Handles menuDiagnostics.Click
        frmDiagnostics.ShowDialog()
    End Sub

    Private Sub WirelessViewer_Closed(sender As Object, e As EventArgs)
        WirelessViewerOpen = False
        menuCalibrate.Enabled = False
        menuDiagnostics.Enabled = False
        menuUpdateSidelineDB.Enabled = True
        menuWireless.Enabled = True
        If Not WirelessViewer.systemShutdown Then
            If isOnline() Then
                UploadSVSessions = New UploadSVSessions()
                UploadSVSessions.ShowDialog()
            End If
            frmMain.lblStatus.Text = "Please Connect Your Device to Sync"
            frmMain.statusText = "GFT Disconnected"
            'WirelessViewer.Dispose()
            'WirelessViewer = Nothing
            WirelessViewer.WirelessViewerOpen = False
            WirelessViewer.mtimer_Disable()
            If Not WirelessViewer.gForceHid Is Nothing Then
                WirelessViewer.gForceHid.Dispose() ' Dispose any connected devices
            End If
            WirelessViewer.DisposeHub()
            WirelessViewer.Dispose()
            WirelessViewer = Nothing
            gUsbComm.MyHid.findTargetDevice(GFT.MY_VENDOR_ID, GFT.MY_PRODUCT_ID) ' Check if any devices are connected
        Else
            systemShutdown = True
            Application.Exit()
        End If
    End Sub

    Private Sub menuWireless_Click(sender As Object, e As EventArgs) Handles menuWireless.Click
        gMessage.close()
        If WirelessViewer Is Nothing Then
            WirelessViewer = New Form1()
            AddHandler WirelessViewer.Closed, AddressOf WirelessViewer_Closed
        End If
        bwCheckDevice.CancelAsync()
        unPlugUsb()
        WirelessViewerOpen = True
        menuCalibrate.Enabled = False
        menuDiagnostics.Enabled = False
        menuWireless.Enabled = False
        menuUpdateSidelineDB.Enabled = False
        frmMain.lblStatus.Text = "Please Close the Sideline Viewer"
        frmMain.statusText = "Sideline Viewer Active"
        'ShowWirelessViewer()
        'If Not Application.OpenForms("frmMain") Is Nothing Then Application.OpenForms("frmMain").BringToFront()
        WirelessViewer.WirelessViewerOpen = True
        WirelessViewer.ShowDialog()
    End Sub
    Delegate Sub ShowWirelessViewerCallback()
    Private Sub ShowWirelessViewer()
        gMessage.close()
        If frmNotify.WirelessViewer.InvokeRequired Then
            Dim d As New ShowWirelessViewerCallback(AddressOf ShowWirelessViewer)
            Me.Invoke(d, New Object() {})
        Else
            WirelessViewer.ShowDialog()
        End If

    End Sub

    Private Sub menuCalibrate_Click(sender As Object, e As EventArgs) Handles menuCalibrate.Click

        frmMain.lblStatus.Text = "Device Calibration in Progress"
        frmMain.lblStatus.Visible = True
        frmCalibrate_new.ShowDialog()
    End Sub

    Private Function plugUsb() As Boolean
        Dim result As Boolean = False
        gMessage.close() ' Close any open popups
        isOnline()
        doingConfig = False
        frmMain.SetProgressBarText()
        frmMain.ShowMe()
        If Not WirelessViewerOpen And Not Offline And Not needsUpdate Then ' Don't let the thin client connect to the GFT while the wireless viewer is open. Or when we aren't connected to the Cloud... Or when we are doing a software update

            Try
                checkSoftwareUpdate() ' Lets make sure the software is updated
                If Not needsUpdate Then ' In case we are updating..
                    currentGftProfile = New gftProfile

                    Dim cnt As Int32 = 2

                    Debug.WriteLine("GETTING GFT VERSION")
                    While Not gUsbComm.sendCmdGftVersion(CByte(gUsbComm.mhidCmd.MHID_CMD_GET_DEV_INFO), 0)
                        cnt -= 1
                        If (cnt = 0) Then
                            Return False
                        End If
                        Thread.Sleep(1000)

                    End While


                    '                labelGftVersion.Text = String.Format("{0:X2}", currentGftProfile.curcurrentGftProfile.hwMajor) & ":" & String.Format("{0:X2}", currentGftProfile.curcurrentGftProfile.hwMinor)
                    If currentGftProfile.gftWhereAmI = GFT.GFT_STATE_BOOT Then
                        If frmMain.WindowState = FormWindowState.Minimized Then
                            frmMain.Visible = True
                            frmMain.WindowState = FormWindowState.Normal
                        End If
                        gUsbComm.MyHid.setTimeOut(5000)
                        'frmMain.tslStatus3.BackColor = System.Drawing.Color.LightSkyBlue
                        'frmMain.tslStatus3.Text = "Firmware Update Mode"
                        frmMain.statusText = "Firmware Update Mode"
                        frmMain.SetProgressBarText()
                        If Not doingFWUpdate Then
                            If Not bwUpdateFirmware.IsBusy Then bwUpdateFirmware.RunWorkerAsync()
                        Else
                            doingFWUpdate = False
                        End If
                        Return True
                    End If
                    'sendCmdGftVersion((byte)mhidCmd.MHID_CMD_GET_DEV_INFO, 0);
                    'labelGftVersion.Text += "Firm:" & String.Format("{0:X2}", currentGftProfile.curcurrentGftProfile.firmMajor) & ":" & String.Format("{0:X2}", currentGftProfile.curcurrentGftProfile.firmMinor)
                    'gUsbComm.MyHid.setTimeOut(1000)
                    'End If
                    frmMain.statusText = "GFT Connected"
                    frmMain.SetProgressBarText()

                    gUsbComm.gftStat = 0
                    Debug.WriteLine("GETTING GFT PROFILE")
                    If gUsbComm.getProfile() Then
                        'menuDiagnostics.Enabled = True
                        menuWireless.Enabled = False
                        'If frmMain.WindowState = FormWindowState.Minimized Then
                        'frmMain.Visible = True
                        'frmMain.WindowState = FormWindowState.Normal
                        'End If
                        frmMain.picBattery.Show()
                        frmMain.lblStatus.Text = "Checking Device for Session Data"
                        frmMain.timerStatusFirstEvent = True
                        frmMain.timerStatus.Start()
                        '                progressBarStatus.Show()
                        Debug.WriteLine("STARTING WORKER ASYNC FOR PLUG USB")
                        If Not frmMain.bwCheckBattery.IsBusy Then
                            frmMain.checkBattery()
                            frmMain.timerBatteryMon.Start()
                        End If
                        If Not bwPlugUSB.IsBusy Then bwPlugUSB.RunWorkerAsync()
                        If Not bwCheckDevice.IsBusy Then bwCheckDevice.RunWorkerAsync()
                        If Not bwUpdateUI.IsBusy Then bwUpdateUI.RunWorkerAsync() ' Initial start
                        result = True
                    Else
                        ' can not communicate with gft
                        result = False
                        Debug.WriteLine("plugUsb() -> Can not communicate with gFT")

                    End If

                End If

            Catch ex As Exception
            Return False
            gUtility.DisplayException("PlugUsb", ex)
        End Try


        End If
        Return result

    End Function


    Protected Overrides Sub WndProc(ByRef m As Message)
        If Not WirelessViewerOpen Then ' Don't do anything if the wireless viewer is open

            System.Diagnostics.Debug.WriteLine(m.Msg & ": " & m.WParam.ToInt32.ToString())
            If gUsbComm.MyHid IsNot Nothing Then
                'gUsbComm.MyHid.checkMessage(m)
                'gUsbComm.MyHid.
            End If

            If m.Msg = &H219 Then
                'Debug.WriteLine("USB Devices Changed: " + m.WParam.ToInt32() + " " + m.LParam.ToInt32()); //0X8004
                If m.WParam.ToInt32() = &H8000 Then
                    'gForceTracker.gUsbComm.MyHid
                    If gForceTracker.gUsbComm.MyHid IsNot Nothing Then
                        gForceTracker.gUsbComm.MyHid.onUsbEvent(Me, 0, IntPtr.Zero)
                    End If
                    'If gForceTracker.gUsbComm.MyHub IsNot Nothing Then
                    '   gForceTracker.gUsbComm.MyHub.onUsbEvent(Me, 0, IntPtr.Zero)
                    'End If
                ElseIf m.WParam.ToInt32() = &H8004 Then
                    If gForceTracker.gUsbComm.MyHid IsNot Nothing Then
                        gForceTracker.gUsbComm.MyHid.onUsbEvent(Me, 1, IntPtr.Zero)
                    End If
                    'If gForceTracker.gUsbComm.MyHub IsNot Nothing Then
                    '    gForceTracker.gUsbComm.MyHub.onUsbEvent(Me, 1, IntPtr.Zero)
                    'End If
                End If
            End If

            If GFT.lpHub IsNot Nothing Then
                LP_HUB.MyLp.checkMessage(m)
            End If
            If m.Msg = gNativeMethod.WM_SHOWME Then
                frmMain.ShowMe()
            End If
            Select Case m.Msg
                'case gNativeMethod.WM_SHOWME: //WM_USER+1
                '    {
                '        ShowMe();
                '    }
                '    break;
                Case gNativeMethod.WM_USER + 2
                    If True Then
                        frmMain.tslGFT.Text = "USB ON"
                        frmMain.tslGFT.BackColor = System.Drawing.Color.Lime
                        If Not plugUsb() Then
                        End If
                        'MessageBox.Show("Please Unplug Usb Cable", "Try Again", MessageBoxButtons.OK);
                    End If
                    Exit Select
                Case gNativeMethod.WM_USER + 3
                    If True Then
                        frmMain.tslGFT.Text = "USB OFF"
                        frmMain.tslGFT.BackColor = System.Drawing.Color.LightGray
                        unPlugUsb()
                    End If

                    Exit Select
                Case gNativeMethod.WM_USER + 4
                    If True Then
                        LP_HUB.hubSyncState = LP_HUB.MAX_DEVICE_CNT
                        ' Device is currently attached  
                        frmMain.tslHub.Text = "Wireless ON"
                        frmMain.tslHub.BackColor = System.Drawing.Color.Lime
                        'If backgroundWorkerLpRx.IsBusy Then
                        'backgroundWorkerLpRx.CancelAsync()
                        'MessageBox.Show("Unplug Wireless Dongle, and try again", "R")
                        'Else
                        'backgroundWorkerLpRx.RunWorkerAsync()
                        'End If
                        'If backgroundWorkerLpTx.IsBusy Then
                        'backgroundWorkerLpTx.CancelAsync()
                        'MessageBox.Show("Unplug Wireless Dongle, and try again", "T")
                        'Else
                        'backgroundWorkerLpTx.RunWorkerAsync()
                        'End If
                    End If
                    Exit Select
                Case gNativeMethod.WM_USER + 5
                    'unplugLp()
                    Exit Select
                Case &H11 'WM_QUERYENDSESSION
                    ' session ending..
                    frmNotify.systemShutdown = True
                    Exit Select
            End Select
            MyBase.WndProc(m)

        End If
    End Sub

    Private Sub bwPlugUSB_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bwPlugUSB.ProgressChanged
        Select Case e.ProgressPercentage
            Case 1
                ' Change Name
                If e.UserState Is Nothing Then Exit Select
                Dim sName As String = e.UserState.ToString().Trim()
                'frmMain.tslStatus3.Text = sName

                frmMain.statusText = sName
                frmMain.SetProgressBarText()
                Exit Select
            Case 2
                ' Calibration Required
                If needsCalibration = False Then
                    needsCalibration = True
                End If
                Exit Select
            Case 3
                ' Firmware Update Required
                If needsFirmware = False Then
                    needsFirmware = True
                End If
                updateFirmware()
                Exit Select
            Case 4
                ' New User
                'frmMain.tslStatus3.Text = "New User"
                If Not needsFirmware Then
                    If Not gUsbComm.resetGft() Then
                        If Not frmRegister.Visible Then
                            frmMain.statusText = "New User"
                            frmMain.SetProgressBarText()
                            frmMain.lblStatus.Text = "After Registration Disconnect and Reconnect Device"
                            frmRegister.ShowDialog()
                        End If
                    Else
                        frmMain.statusText = "New User"
                    End If
                End If

        End Select
    End Sub


    Private Sub bwPlugUSB_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bwPlugUSB.RunWorkerCompleted
        If Not Offline Then ' If we're offline, then don't do anything...
            Try
                ' Get Battery Status before we check for sessions
                If gUsbComm.MyHid.deviceAttached AndAlso (currentGftProfile.gftWhereAmI = GFT.GFT_STATE_FIRM) AndAlso (gUsbComm.gftStat = 0) Then
                    Debug.WriteLine("SENDING BATTERY CHECK CMD")
                    Dim batVol As Integer = gUsbComm.sendCmdBattery(CByte(gUsbComm.mhidCmd.MHID_CMD_CHECKBAT), 0)
                    frmMain.gftBattVol = batVol
                    Console.WriteLine("BATT VO" & DateTime.Now.ToString() & "-batStatus=" & frmMain.gftBattVol.ToString())
                End If

                If frmMain.statusText <> "New User" And needsCalibration = False And needsSettings = False And needsFirmware = False And Not currentGftProfile Is Nothing Then
                    If Not bwUploadData.IsBusy Then bwUploadData.RunWorkerAsync()
                    ' Perhaps we should allow events to be raised!?
                    While bwUploadData.IsBusy
                        Application.DoEvents()
                    End While
                    'gUsbComm.waitEvent.WaitOne()
                ElseIf needsCalibration = True Then
                    'bwPlugUSB.CancelAsync()
                    'bwUploadData.CancelAsync()
                    'MsgBox("Before using your device, you must first calibrate it.  Please press OK to calibrate now.", MsgBoxStyle.OkOnly, "GForce Tracker - Calibration Required")
                    'frmCalibrate_new.ShowDialog()
                ElseIf needsSettings = True Then
                    ' bwPlugUSB.CancelAsync()
                    ' bwUploadData.CancelAsync()
                    'MessageBox.Show("New Settings have been detected for your device.  Press OK to update your device.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'gUsbComm.configGft()
                    'Thread.Sleep(1000)
                    'rest.deviceUpdated(currentGftProfile.sSerial)
                ElseIf currentGftProfile Is Nothing Then
                    Thread.Sleep(2000) ' Sleep for 2 seconds, GFT May not be ready (Happens after calibration.. Sometimes)
                    If Not bwPlugUSB.IsBusy And Not needsFirmware Then plugUsb() 'bwPlugUSB.RunWorkerAsync()
                Else
                    bwPlugUSB.CancelAsync()
                    bwUploadData.CancelAsync()
                End If
            Catch ex As Exception
                unPlugUsb()
                Thread.Sleep(2000) ' Something went wrong, go to sleep
            End Try
        Else
            ' We're offline...
            Thread.Sleep(60000) ' Sleep for a minute before trying again...
            isOnline()
            If Not bwPlugUSB.IsBusy And Not needsFirmware Then bwPlugUSB.RunWorkerAsync()
        End If

    End Sub

    Private Sub bwUploadData_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bwUploadData.ProgressChanged
        Debug.WriteLine("GOT MESSAGE FROM UPLOAD THREAD : " & e.ProgressPercentage)

        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Select Case e.ProgressPercentage
            Case 1
                ' Change Label
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    '                    progressBarStatus.Show()
                    frmMain.lblStatus.Text = DirectCast(e.UserState, String)
                End If
                Exit Select
            Case 2
                ' Error in Transmission
                bw.CancelAsync()
                gMessage.MessageBox("There was an error in retrieving the session data." & vbCrLf & "Please disconnect and reconnect the device to try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                'MsgBox("There was an error in retrieving the session data.  Please disconnect and reconnect the device to try again." & vbCrLf & vbCrLf & "Error Message: " & e.UserState, MsgBoxStyle.OkOnly, "GForce Tracker")
                Exit Select
            Case 3
                'Report # of Sessions
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    frmMain.iSessionCnt = 1
                    frmMain.lblStatus.Text = "Uploading Session Data to Cloud"
                End If
                Exit Select
            Case 4
                ' Retrieve Sessions from Device
            Case 5
                ' Upload Session
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    frmMain.iSessionCnt = e.UserState
                    frmMain.lblStatus.Text = "Uploading Session Data to Cloud"
                End If
                Exit Select
            Case 6
                ' Uploading Impacts
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    frmMain.lblStatus.Text = "Uploading Impacts for Session"
                End If
                Exit Select
            Case 7
                'Report # of Impacts
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    frmMain.iImpactCnt = e.UserState
                End If
                Exit Select
            Case 8
                ' Session Upload Complete
                Exit Select
            Case 9
                ' Erase Session Data
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    frmMain.lblStatus.Text = "Erasing Session Data from Device - Do Not Disconnect"
                End If
            Case 10
                ' Update progress bar
                Debug.WriteLine("Update progress bar: " & e.UserState)
                'frmMain.progBar.Value = CInt(e.UserState)
                frmMain.UpdateProgressBar(CInt(e.UserState))
                Exit Select
        End Select
    End Sub

    Private Sub bwUploadData_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bwUploadData.RunWorkerCompleted
        Debug.WriteLine("COMPLETING WORKER ASYNC FOR UPLOAD DATA")
        Dim ts As TimeSpan
        gUsbComm.gftFinishedUpload = True
        gUsbComm.gftStat = 0
        '        progressBarStatus.Hide()
        frmMain.timerStatus.Stop()
        'frmMain.progBar.Value = 100
        frmMain.UpdateProgressBar(100)
        frmMain.SetProgressBarText()
        Me.menuCalibrate.Enabled = True
        Me.menuDiagnostics.Enabled = True
        Me.menuWireless.Enabled = True
        Dim needs302Update As Boolean = False
        If Not currentGftProfile Is Nothing Then
            If (((currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor < 2) Or currentGftProfile.firmMajor < 3) And currentGftProfile.firmMajor > 0) Then needs302Update = True
        End If
        If needs302Update Then
            bwCheckDevice.CancelAsync()
            frmMain.isUploading = False
            ' Firmware Update Required
            If needsFirmware = False Then
                ' Erase Calibration Data
                Dim mq As Int16() = New Int16(3) {}
                mq(0) = 0
                mq(1) = 0
                mq(3) = 0
                mq(3) = 0
                rest.calibrate(mq, False, False, False, False, False) ' Wipe Cloud Calibration data
                'gUsbComm.resetGft()
                needsFirmware = True
                needsSettings = False
                updateFirmware(True)
            End If
        Else
            frmMain.lblStatus.Text = "Sync Complete - You Can Safely Remove the Device"
            frmMain.lblStatus.Visible = True

            ' Reset the GFT's clock
            Debug.WriteLine("GFT SYNC TIME")
            gUsbComm.syncSystemTime()
            Dim stopWatch As New Stopwatch()
            stopWatch.Start()
            While True
                stopWatch.Stop()
                ts = stopWatch.Elapsed
                If ts.Seconds > 1 Then
                    'frm.Close()
                    'gDatabase.curSessionID = gDatabase.gDataGetSessionLastSID()
                    'gChart.loadFileAuto(Me, Nothing, dataGridView1, dataGridView2, chartRaw1, chartRawLin, _
                    '    chartRawGyro)
                    Exit While
                Else
                    stopWatch.Start()
                End If
            End While
            If frmMain.iSessionCnt > 0 Then
                frmMain.iSessionCnt = 0
                'If MessageBox.Show("New session data has been uploaded to the Cloud.  Would you like to review this data now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                If gMessage.MessageBox("New session data has been uploaded to the Cloud.  Would you like to review this data now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Dim wndName As String = "| GForce Tracker"
                    Dim ptr As IntPtr
                    For Each proc As Process In Process.GetProcesses()
                        If proc.MainWindowTitle.Contains(wndName) Then
                            ptr = proc.MainWindowHandle
                        End If
                    Next
                    If ptr <> 0 Then
                        BringWindowToTop(ptr)
                        ShowWindow(ptr, SW_SHOWMAXIMIZED)
                    Else
                        System.Diagnostics.Process.Start("http://gft.gforcetracker.com")
                    End If
                End If
            End If

        End If

        frmMain.isUploading = False
        Debug.WriteLine("Upload thread finished")
    End Sub

    Public Sub bwUploadData_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwUploadData.DoWork
        If Not Offline Then
            frmMain.isUploading = True ' Set this flag to prevent bwCheckDevice from doing any work.
            Debug.WriteLine("STARTING TO DO WORK IN UPLOAD DATA THREAD")
            Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
            Dim retValue As Char = "0"c
            Try
                Thread.Sleep(1000)
                If Thread.CurrentThread.Name Is Nothing Then
                    Thread.CurrentThread.Name = "uploadData"
                End If
                Debug.WriteLine("Upload thread created")
                gUsbComm.gftFinishedUpload = False
                gUsbComm.gftStat = CInt(gUsbComm.mhidCmd.MHID_CMD_GET_ACC_ENTRY_CNT)
                gUsbCommOld = Nothing
                If (((currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor < 2) Or currentGftProfile.firmMajor < 3) And currentGftProfile.firmMajor > 0) Then
                    gUsbCommOld = New gUsbCommOld(myHandle, GFT.MY_VENDOR_ID, GFT.MY_PRODUCT_ID, gUsbComm.MyHid)
                    retValue = gUsbCommOld.uploadData(sender, e) ' 
                    gUsbCommOld = Nothing
                ElseIf currentGftProfile.firmMajor = 9 Or (currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor = 9) Or (currentGftProfile.hwMajor = 3 And currentGftProfile.hwMinor = 1) Then
                    gUsbCommPerformance = New gUsbCommPerformance(myHandle, GFT.MY_VENDOR_ID, GFT.MY_PRODUCT_ID, gUsbComm.MyHid)
                    gUsbCommPerformance.MyHid.setTimeOut(5000)
                    retValue = gUsbCommPerformance.uploadData(sender, e)

                ElseIf (currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor >= 2) Or currentGftProfile.firmMajor > 3 Then
                    retValue = gUsbComm.uploadData(sender, e)
                End If
                gUsbComm.gftStat = 0
                If (retValue = "i"c) Then
                    gMessage.MessageBox("Error reading session data. The GFT did not respond. " & vbCrLf & "Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Error reading session data. The GFT did not respond. Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue = "o"c) Then
                    gMessage.MessageBox("Please re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Please re-attach the GFT.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue = "O"c) Then
                    'MessageBox.Show("Please re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    gMessage.MessageBox("Please re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Please re-attach the GFT.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue = "I"c) Then
                    gMessage.MessageBox("Please re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Please re-attach the GFT.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue = "t"c) Then
                    gMessage.MessageBox("Error reading device data. Timestamp is invalid. " & vbCrLf & "Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ' MessageBox.Show("Error reading device data. Timestamp is invalid. Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue = "T"c) Then
                    gMessage.MessageBox("Error reading device data. Timestamp is invalid. " & vbCrLf & "Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Error reading device data. Timestamp is invalid. Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue = "e"c) Then
                    gMessage.MessageBox("Error while erasing session data from device.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Error while erasing session data from device.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue <> "0"c) AndAlso (retValue <> "1"c) Then
                    gMessage.MessageBox("Data retrieval was interrupted. Please re-attach the GFT.  " & vbCrLf & "If this problem persists, attempt to unassign the device in the " & vbCrLf & "Cloud Control Panel under Device Settings and reassign the device to the user.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Data retrieval was interrupted. Please re-attach the GFT.  If this problem persists, attempt to unassign the device in the Cloud Control Panel under Device Settings and reassign the device to the user.", "Try Again", MessageBoxButtons.OK)
                ElseIf (retValue = "f"c) Then
                    gMessage.MessageBox("Unable to read data from device. " & vbCrLf & "Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Unable to read data from device. Please check your USB cable and re-attach the GFT.", "Try Again", MessageBoxButtons.OK)
                End If

                Debug.WriteLine("Upload thread exit")
            Catch ex As Exception
                gMessage.MessageBox("Data retrieval was interrupted." & vbCrLf & "Please re-attach the GFT. If this problem persists, " & vbCrLf & "attempt to unassign the device in the Cloud Control Panel " & vbCrLf & "under Device Settings and reassign the device to the user.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Information) ' & vbCrLf & vbCrLf & "Error: " & ex.Message
                'MessageBox.Show("Data retrieval was interrupted. Please re-attach the GFT. If this problem persists, attempt to unassign the device in the Cloud Control Panel under Device Settings and reassign the device to the user." & vbCrLf & vbCrLf & "Error: " & ex.Message, "Try Again", MessageBoxButtons.OK)
                'gUtility.DisplayException("Do Receive Work", ex)
            End Try
            'gUsbComm.waitEvent.Set()
        End If
        Return
    End Sub

    ' Common location for UI changes that occur on device unplugged
    Private Sub unPlugUsbUI()
        frmMain.lblStatus.Text = "Please Connect Your Device to Sync"
        frmMain.picBattery.Hide()
        frmMain.statusText = "GFT Disconnected"
        frmMain.SetProgressBarText()

        If (frmMain.WindowState = FormWindowState.Normal Or frmMain.WindowState = FormWindowState.Maximized) AndAlso Not (frmCalibrate_new.Visible Or frmDiagnostics.Visible Or needsFirmware) Then
            frmMain.hideMe()
        End If
    End Sub

    Private Sub bwUpdateUI_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwUpdateUI.DoWork
        Try
            ' If we reconfigured a GFT and the grace period has elapsed OR a different GFT has been plugged in
            If (didConfig = True And (didConfigTime < DateTime.UtcNow.AddSeconds(-15))) Then 'Or (Not currentGftProfile Is Nothing And currentGftProfile.sSerial <> didConfigSerial)
                didConfig = False
                ' If no new GFT has been plugged in, change status to disconnected
                If (currentGftProfile Is Nothing) Then
                    unPlugUsbUI()
                End If
            ElseIf didConfig = True And Not currentGftProfile Is Nothing Then
                If currentGftProfile.sSerial <> didConfigSerial Then
                    didConfig = False
                End If
            End If

            Thread.Sleep(500)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bwUpdateUI_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bwUpdateUI.RunWorkerCompleted
        If (Not bwUpdateUI.IsBusy) Then
            bwUpdateUI.RunWorkerAsync()
        End If
    End Sub

    Private Shared Sub bwPlugUSB_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwPlugUSB.DoWork
        If Not Offline And Not currentGftProfile Is Nothing Then
            Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
            Debug.WriteLine("DOING WORK IN PLUG USB THREAD")
            Debug.WriteLine("REST CHECK DEVICE")
            If Not currentGftProfile.sSerial Is Nothing Then
                rest.checkDevice(currentGftProfile.sSerial, currentGftProfile, frmMain.gftBattVol, gftType)
                If Not currentGftProfile Is Nothing Then
                    Dim cloudMyFirmware = sCloudFirmwareVer
                    If currentGftProfile.firmMajor = 9 Or (currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor = 9) Or (currentGftProfile.hwMajor = 3 And currentGftProfile.hwMinor = 1) Then
                        cloudMyFirmware = sCloudFirmwareVer3Perf
                        gftType = "Performance"
                        currentGftProfile.hwType = gftType
                    ElseIf currentGftProfile.hwMajor = 3 Then
                        cloudMyFirmware = sCloudFirmwareVer3
                        gftType = "Impact"
                        currentGftProfile.hwType = gftType
                    ElseIf currentGftProfile.hwMajor = 4 Then
                        cloudMyFirmware = sCloudFirmwareVer4
                        gftType = "Impact"
                        currentGftProfile.hwType = gftType
                    Else
                        cloudMyFirmware = sCloudFirmwareVer
                        gftType = "Impact"
                        currentGftProfile.hwType = gftType
                    End If
                    ' Firmware Check
                    Dim cloudMajor As Integer = 0
                    Dim cloudMinor As Integer = 0
                    Dim fwbits = cloudMyFirmware.Split(".")
                    If fwbits.Length >= 2 Then
                        cloudMajor = CInt(fwbits(0))
                        cloudMinor = CInt(fwbits(1))
                    End If
                    If cloudMajor > currentGftProfile.firmMajor Or (cloudMajor = currentGftProfile.firmMajor And cloudMinor > currentGftProfile.firmMinor) Or (currentGftProfile.sOverrideDeviceType <> "" And currentGftProfile.sOverrideDeviceType <> currentGftProfile.hwType) Then
                        'If currentGftProfile.firmMajor & "." & currentGftProfile.firmMinor <> sCloudFirmwareVer Then
                        ' we need to download the new firmware an install it
                        If Not needsFirmware Then
                            bw.ReportProgress(3, "") ' In case firmware update is triggered by the other thread already...
                        End If
                        bw.CancelAsync()
                        Return
                    End If

                    ' Calculate how far the GFT's clock is
                    Dim gftTime, utcTime As DateTime
                    utcTime = gUsbComm.getUTC()
                    gftTime = gUsbComm.getGftTime()
                    timeDiff = utcTime - gftTime



                    'Debug.WriteLine("GFT SYNC TIME")
                    'gUsbComm.syncSystemTime()

                    If rest.deviceState = gftDeviceState.DEVICE_REGISTERED Then
                        Debug.WriteLine("DEVICE IS REGISTERED")
                        ' Existing User
                        bw.ReportProgress(1, currentGftProfile.sName)

                        checkDeviceSettings()
                        If needsSettings Then
                            bw.CancelAsync()
                            Return
                        End If

                        'init calibration
                        If currentGftProfile.iLocation(0) = 0 AndAlso currentGftProfile.iLocation(1) = 0 AndAlso currentGftProfile.iLocation(2) = 0 AndAlso currentGftProfile.iLocation(3) = 0 Then
                            bw.ReportProgress(2, "Please calibrate your GFT from the Tools menu!")
                            bw.CancelAsync()
                        Else
                            gCalc.initQ(currentGftProfile.iLocation, currentGftProfile.bMountInside)

                            'download data from gft
                            'Refresh()
                            gUsbComm.gftFinishedUpload = False
                            frmMain.iSessionCnt = 0
                        End If
                    Else
                        Debug.WriteLine("GFT IS NEW USER")
                        If newUser = False Then
                            If gUsbComm.sendCmd(CByte(gUsbComm.mhidCmd.MHID_CMD_ERASE_ACC_DATA), 0) = "passed" Then
                                gUsbComm.gftStat = CByte(gUsbComm.mhidCmd.MHID_CMD_ERASE_ACC_DATA)
                            Else
                                gUsbComm.gftStat = 0
                            End If
                            bw.ReportProgress(4, "New")
                        End If
                        'bw.ReportProgress(4, "New")
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub updateCleanup()
        ' Remove leftovers after software update
        If System.IO.File.Exists(Replace(Application.ExecutablePath.ToLower, ".exe", ".old")) Then
            My.Computer.FileSystem.DeleteFile(Replace(Application.ExecutablePath.ToLower, ".exe", ".old"))
        End If

        If System.IO.File.Exists(Replace(Application.StartupPath & "/WirelessGFTViewer.dll".ToLower, ".dll", ".old")) Then
            My.Computer.FileSystem.DeleteFile(Replace(Application.StartupPath & "/WirelessGFTViewer.dll".ToLower, ".dll", ".old"))
        End If
        If System.IO.File.Exists(Replace(Application.StartupPath & "/ArtaFlexHidIO.dll".ToLower, ".dll", ".old")) Then
            My.Computer.FileSystem.DeleteFile(Replace(Application.StartupPath & "/ArtaFlexHidIO.dll".ToLower, ".dll", ".old"))
        End If
        If System.IO.File.Exists(Replace(Application.StartupPath & "/HidSharp.dll".ToLower, ".dll", ".old")) Then
            My.Computer.FileSystem.DeleteFile(Replace(Application.StartupPath & "/HidSharp.dll".ToLower, ".dll", ".old"))
        End If
        If System.IO.File.Exists(Replace(Application.StartupPath & "/gForceTracker.sdf".ToLower, ".sdf", ".old")) Then
            My.Computer.FileSystem.DeleteFile(Replace(Application.StartupPath & "/gForceTracker.sdf".ToLower, ".sdf", ".old"))
        End If
    End Sub

    Private Sub checkMissingFiles()
        If Not System.IO.File.Exists(Application.StartupPath & "/gForceTracker.sdf") Then
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/gForceTracker.sdf", Application.StartupPath & "/gForceTracker.sdf")
        End If
        If Not System.IO.File.Exists(Application.StartupPath & "/HidSharp.dll") Then
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/HidSharp.dll", Application.StartupPath & "/HidSharp.dll")
        End If

        If Not System.IO.File.Exists(Application.StartupPath & "/System.Data.SqlServerCe.dll") Then
            ' Download SQLEngineCE files
            If (Not System.IO.Directory.Exists(Application.StartupPath & "/amd64/Microsoft.VC90.CRT")) Then
                System.IO.Directory.CreateDirectory(Application.StartupPath & "amd64/Microsoft.VC90.CRT")
            End If
            If (Not System.IO.Directory.Exists(Application.StartupPath & "/amd64/Microsoft.VC90.CRT")) Then
                System.IO.Directory.CreateDirectory(Application.StartupPath & "amd64/Microsoft.VC90.CRT")
            End If
            If (Not System.IO.Directory.Exists(Application.StartupPath & "/x86/Microsoft.VC90.CRT")) Then
                System.IO.Directory.CreateDirectory(Application.StartupPath & "/x86/Microsoft.VC90.CRT")
            End If
            If (Not System.IO.Directory.Exists(Application.StartupPath & "/x86/Microsoft.VC90.CRT")) Then
                System.IO.Directory.CreateDirectory(Application.StartupPath & "/x86/Microsoft.VC90.CRT")
            End If
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/System.Data.SqlServerCe.dll", Application.StartupPath & "/System.Data.SqlServerCe.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/sqlceca40.dll", Application.StartupPath & "/amd64/sqlceca40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/sqlcecompact40.dll", Application.StartupPath & "/amd64/sqlcecompact40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/sqlceer40EN.dll", Application.StartupPath & "/amd64/sqlceer40EN.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/sqlceme40.dll", Application.StartupPath & "/amd64/sqlceme40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/sqlceqp40.dll", Application.StartupPath & "/amd64/sqlceqp40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/sqlcese40.dll", Application.StartupPath & "/amd64/sqlcese40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/Microsoft.VC90.CRT/Microsoft.VC90.CRT.manifest", Application.StartupPath & "/amd64/Microsoft.VC90.CRT/Microsoft.VC90.CRT.manifest")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/amd64/Microsoft.VC90.CRT/msvcr90.dll", Application.StartupPath & "/amd64/Microsoft.VC90.CRT/msvcr90.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/sqlceca40.dll", Application.StartupPath & "/x86/sqlceca40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/sqlcecompact40.dll", Application.StartupPath & "/x86/sqlcecompact40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/sqlceer40EN.dll", Application.StartupPath & "/x86/sqlceer40EN.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/sqlceme40.dll", Application.StartupPath & "/x86/sqlceme40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/sqlceqp40.dll", Application.StartupPath & "/x86/sqlceqp40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/sqlcese40.dll", Application.StartupPath & "/x86/sqlcese40.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/Microsoft.VC90.CRT/Microsoft.VC90.CRT.manifest", Application.StartupPath & "/x86/Microsoft.VC90.CRT/Microsoft.VC90.CRT.manifest")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/sqlenginece/x86/Microsoft.VC90.CRT/msvcr90.dll", Application.StartupPath & "/x86/Microsoft.VC90.CRT/msvcr90.dll")
        End If
    End Sub

    Public Sub checkSoftwareUpdate()
        If isOnline() Then
            ' Do Software Version Check
            If rest.checkVersion() Then
                ' Grab firmware in case we don't have it already.
                If Not System.IO.File.Exists(sFirmwareFile) Or Not System.IO.File.Exists(sFirmwareFile_3) Or Not System.IO.File.Exists(sFirmwareFile_4) Or Not System.IO.File.Exists(sFirmwareFile_3_Perf) Then
                    eDownload = downloadType.GFT_FIRMARE
                    downloadFile()
                End If
                If Application.ProductVersion < sCloudSoftwareVer Then
                    ' download new software and update
                    needsUpdate = True
                    updateSoftware()
                    Return
                End If
            End If
        End If
    End Sub

    Public Sub New()
        Debug.WriteLine("NEW NOTIFY FORM INSTANCE")
        ' This call is required by the designer.
        InitializeComponent()
        gMessage = New gftMessage()

        updateCleanup() ' Post update cleanup routine

        ' Add any initialization after the InitializeComponent() call.
        HasWirelessViewer = System.IO.File.Exists(Application.StartupPath & "/WirelessGFTViewer.dll") ' Check if the Wireless Viewer is Installed
        If HasWirelessViewer Then
            Dim curWVer = FileVersionInfo.GetVersionInfo(Application.StartupPath & "/WirelessGFTViewer.dll").FileVersion
            If FileVersionInfo.GetVersionInfo(Application.StartupPath & "/WirelessGFTViewer.dll").FileVersion = "1.0.0.31" Then
                My.Computer.FileSystem.RenameFile(Application.StartupPath & "/WirelessGFTViewer.dll", "WirelessGFTViewer.old")
                My.Computer.FileSystem.RenameFile(Application.StartupPath & "/ArtaFlexHidIO.dll", "ArtaFlexHidIO.old")
                HasWirelessViewer = False
            End If
        End If

        checkMissingFiles() ' Check for any application files which are missing and download them

        If Not HasWirelessViewer Then ' Hide menu options for wireless viewer if it's not present
            'menuWireless.Visible = False
            'menuUpdateSidelineDB.Visible = False

            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/WirelessGFTViewer.dll", Application.StartupPath & "/WirelessGFTViewer.dll")
            My.Computer.Network.DownloadFile("http://gft.gforcetracker.com/software/ArtaFlexHidIO.dll", Application.StartupPath & "/ArtaFlexHidIO.dll")
            HasWirelessViewer = True

        End If
        If isOnline() Then
            ' Do Software Version Check
            checkSoftwareUpdate()
        Else
            gMessage.MessageBox("Unable to connect to the Cloud. Please check your internet connection and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'MessageBox.Show("Unable to connect to the Cloud. Please check your internet connection and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        registerForDeviceNotifications(Me.Handle)
        frmMain.Show()
        frmMain.Activate()
        frmMain.WindowState = FormWindowState.Minimized
        frmMain.ShowInTaskbar = False
    End Sub

    Private Sub frmNotify_Load(sender As Object, e As EventArgs) Handles Me.Load
        'AddHandler Microsoft.Win32.SystemEvents.SessionEnding, New Microsoft.Win32.SessionEndingEventHandler(AddressOf SessionIsEnding)
        myHandle = Me.Handle
        If Not bwLoad.IsBusy Then bwLoad.RunWorkerAsync()
    End Sub


    Private Sub updateSoftware()
        If Not isOnline() Then
            gMessage.MessageBox("You must first connect to the Internet to perform the software update.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'MessageBox.Show("You must first connect to the Internet to perform the software update.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        eDownload = downloadType.GFT_SOFTWARE
        downloadFile()
    End Sub

    Public Function isOnline() As Boolean
        'Return My.Computer.Network.Ping("4.2.2.2")

        Try
            Dim myHttpWebRequest As HttpWebRequest = DirectCast(WebRequest.Create("http://gft.gforcetracker.com"), HttpWebRequest)
            Dim myHttpWebResponse As HttpWebResponse = DirectCast(myHttpWebRequest.GetResponse(), HttpWebResponse)
            myHttpWebResponse.Close()
            Offline = False
            Return True
        Catch e As WebException
            'There is a problem accessing the site
            'MessageBox.Show("Unable to connect to Cloud.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Offline = True
            Return False
        End Try

    End Function

    Private Sub downloadFile()
        Dim _WebClient As New System.Net.WebClient()
        AddHandler _WebClient.DownloadFileCompleted, AddressOf _DownloadFileCompleted
        AddHandler _WebClient.DownloadProgressChanged, AddressOf _DownloadProgressChanged
        If eDownload = downloadType.GFT_SOFTWARE Then
            frmMain.lblStatus.Text = "Downloading Software Update"
            Dim SoftwareUpdate As New SoftwareUpdate() ' Start the Software Updater
            'My.Computer.FileSystem.RenameFile(Application.ExecutablePath, "GForceTracker.old")
            '_WebClient.DownloadFileAsync(New Uri("http://gft.gforcetracker.com/software/GForceTracker.exe"), Application.ExecutablePath)
        ElseIf eDownload = downloadType.GFT_FIRMARE Then
            frmMain.lblStatus.Text = "Downloading Firmware Update"
            If firmware_download_cnt = 4 Or firmware_download_cnt = 0 Then
                firmware_download_cnt = 0
                _WebClient.DownloadFileAsync(New Uri("http://gft.gforcetracker.com/software/gft_fw_" & sCloudFirmwareVer & ".bin"), sFirmwareFile)
            ElseIf firmware_download_cnt = 1 Then
                _WebClient.DownloadFileAsync(New Uri("http://gft.gforcetracker.com/software/gft_fw_3_" & sCloudFirmwareVer3 & ".bin"), sFirmwareFile_3)
            ElseIf firmware_download_cnt = 2 Then
                _WebClient.DownloadFileAsync(New Uri("http://gft.gforcetracker.com/software/gft_fw_4_" & sCloudFirmwareVer4 & ".bin"), sFirmwareFile_4)
            ElseIf firmware_download_cnt = 3 Then
                _WebClient.DownloadFileAsync(New Uri("http://gft.gforcetracker.com/software/gft_fw_3P_" & sCloudFirmwareVer3Perf & ".bin"), sFirmwareFile_3_Perf)
            End If
        End If
    End Sub

    ' Occurs when an asynchronous file download operation completes.
    Private Sub _DownloadFileCompleted(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
        If eDownload = downloadType.GFT_SOFTWARE Then ' This path is depracated
            'MessageBox.Show("GForceTracker software has been successfully updated." & vbCrLf & vbCrLf & "Please wait while the program restarts.")
            'Application.Restart()
        ElseIf eDownload = downloadType.GFT_FIRMARE And Not needsFirmware Then
            firmware_download_cnt += 1
            If firmware_download_cnt = 4 Then ' we have 4 files to download and store...
                frmMain.lblStatus.Text = "Please Connect Your Device to Sync"
            Else
                downloadFile()
            End If
        ElseIf eDownload = downloadType.GFT_FIRMARE And needsFirmware Then
            firmware_download_cnt += 1
            If firmware_download_cnt = 4 Then ' we have 4 files to download and store...
                'If Not fwUpdateFailed Then gMessage.MessageBox("Firmware has been successfully downloaded." & vbCrLf & vbCrLf & "Your device will be updated." & vbCrLf & vbCrLf & "Please do not disconnect the GFT.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                'If Not fwUpdateFailed Then MessageBox.Show("Firmware has been successfully downloaded." & vbCrLf & vbCrLf & "Your device will be updated." & vbCrLf & vbCrLf & "Please do not disconnect the GFT.")
                If Not bwUpdateFirmware.IsBusy Then bwUpdateFirmware.RunWorkerAsync()
            Else
                downloadFile()
            End If

        End If
    End Sub

    ' Occurs when an asynchronous download operation successfully transfers some or all of the data.
    Private Sub _DownloadProgressChanged(ByVal sender As Object, ByVal e As System.Net.DownloadProgressChangedEventArgs)
        ' Update progress bar
    End Sub

    ' Check to ensure the device settings are in sync
    Public Shared Sub checkDeviceSettings()
        Dim bSync As Boolean = False
        Dim updateReasons As List(Of String) = New List(Of String)
        If Not currentGftProfile.sName Is Nothing Then
            gUsbComm.setAlarmMode()
            Console.WriteLine(Mid(currentGftProfile.sCloudName, 1, 20))
            If String.Compare(Mid(currentGftProfile.sCloudName, 1, 20), currentGftProfile.sName) Then
                bSync = True
                updateReasons.Add(" - Device Name Updated.")
            End If
            If Math.Round((CInt(currentGftProfile.bCloudAlarmMode) / 10), 0) <> Math.Round((CInt(currentGftProfile.bAlarmMode) / 10), 0) Then
                bSync = True
                updateReasons.Add(" - Device Alarm Mode Updated.")
            End If
            If currentGftProfile.iCloudPlayerNumber <> CInt(currentGftProfile.iPlayerNumber) Then
                bSync = True
                updateReasons.Add(" - Device Player Number Updated.")
            End If
            If currentGftProfile.iCloudRecordThreshold <> CInt(currentGftProfile.bThresholdR) Then
                bSync = True
                updateReasons.Add(" - Device Recording Threshold Updated.")
            End If
            If currentGftProfile.iCloudThreshold <> CInt(currentGftProfile.bThresholdAlarm) Then
                bSync = True
                updateReasons.Add(" - Device Alarm Threshold Updated.")
            End If
            'If currentGftProfile.bCloudWirelessEnabled <> currentGftProfile.bWirelessEnabled Then bSync = True ' Don't overwrite GFT's setting for Wireless
            If currentGftProfile.bCloudTestMode <> currentGftProfile.bTestMode Then
                bSync = True
                updateReasons.Add(" - Device Test Mode Updated.")
            End If
            If bSync Then
                needsSettings = True
                needsSettingsReasons = String.Join(vbCrLf, updateReasons)
            Else
                needsSettings = False
                needsSettingsReasons = ""
            End If
        End If
    End Sub

    Private Sub updateFirmware(Optional update302 As Boolean = False)
        If Not isOnline() Then
            gMessage.MessageBox("Please check your internet connection and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        frmMain.Show()
        If (Not bwUpdateFirmware.IsBusy()) Then
            Dim result As DialogResult
            If update302 Then
                result = gMessage.MessageBox("Re-Calibration required After this Firmware Update." & vbCrLf & vbCrLf & "Please do not unplug the GFT until you have completed the new Calibration steps." & vbCrLf & "You will be prompted to do this after the Firmware Update.", "Re-Calibration Required", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                result = gMessage.MessageBox("A new version of the firmware is available for the GFT." & vbCrLf & "We will download and apply this update now.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            If (result = Windows.Forms.DialogResult.OK) Then
                eDownload = downloadType.GFT_FIRMARE
                downloadFile()
            End If
        End If
    End Sub

    Private Sub bwUpdateFirmware_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwUpdateFirmware.DoWork

        ' Check the HW type
        If Not currentGftProfile Is Nothing Then
            If currentGftProfile.sOverrideDeviceType <> "" Then
                ' Firmware type is being overridden
                If currentGftProfile.sOverrideDeviceType = "Performance" Then
                    ' Performance firmware is only valid for V3 and V4 hardware
                    If currentGftProfile.hwMajor = 3 Then
                        sUpdateFirmwareFile = sFirmwareFile_3_Perf
                    ElseIf currentGftProfile.hwMajor = 4 Then
                        sUpdateFirmwareFile = sFirmwareFile_3_Perf
                    End If
                ElseIf currentGftProfile.sOverrideDeviceType = "Impact" Then
                    If currentGftProfile.hwMajor = 3 Then
                        sUpdateFirmwareFile = sFirmwareFile_3
                    ElseIf currentGftProfile.hwMajor = 4 Then
                        sUpdateFirmwareFile = sFirmwareFile_4
                    Else
                        sUpdateFirmwareFile = sFirmwareFile ' Fall back for older GFT 2's
                    End If
                End If
            ElseIf currentGftProfile.firmMajor = 9 Or (currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor = 9) Or (currentGftProfile.hwMajor = 3 And currentGftProfile.hwMinor = 1) Then
                sUpdateFirmwareFile = sFirmwareFile_3_Perf
            ElseIf currentGftProfile.hwMajor = 3 Then
                sUpdateFirmwareFile = sFirmwareFile_3
            ElseIf currentGftProfile.hwMajor = 4 Then
                sUpdateFirmwareFile = sFirmwareFile_4
            Else
                sUpdateFirmwareFile = sFirmwareFile ' Fall back for older GFT 2's
            End If
        End If



        doingFWUpdate = True
        fwUpdateFailed = False
        Debug.WriteLine("Firmware Update")
        ' Initializes the variables to pass to the MessageBox.Show method.
        Dim outputReportBuffer As Byte() = New [Byte](64) {}
        Dim inputReportBuffer As Byte() = New [Byte](64) {}
        outputReportBuffer(0) = 0
        Dim firmStatus As [String]
        Dim nread As Integer = 0
        Dim err As Boolean = False

        'reboot 
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        gUsbComm.gftStat = CInt(gUsbComm.mhidCmd.MHID_CMD_START_BOOT)
        If Not gUsbComm.MyHid.deviceAttached Then
            needsFirmware = False
            bw.ReportProgress(1, "Please reconnect USB cable and try again.")
            Return
        End If

        If currentGftProfile.gftWhereAmI = GFT.GFT_STATE_FIRM Then
            gUsbComm.sendCmd(CByte(gUsbComm.mhidCmd.MHID_CMD_START_BOOT), 0)
            bw.ReportProgress(2, "Preparing to Update Firmware")
            Thread.Sleep(1000)
        End If

        'erease flash
        '  Don't allow another transfer request until this one completes.
        outputReportBuffer(0) = 0
        outputReportBuffer(1) = CByte(gUsbComm.mhidCmd.MHID_CMD_ERASE_FIRMWARE)
        'outputReportBuffer[1] = (byte)mhidCmd.MHID_CMD_GET_ACC_DATA_FIRST;
        outputReportBuffer(2) = &H59
        outputReportBuffer(3) = 0
        bw.ReportProgress(3, "Firmware Update in Progress")
        Thread.Sleep(2000)
        err = gUsbComm.MyHid.OutputReports(outputReportBuffer)
        gUsbComm.MyHid.inputReports(inputReportBuffer, nread)

        If (inputReportBuffer(1) = CByte(CInt(gUsbComm.mhidCmd.MHID_CMD_ERASE_FIRMWARE) Or gUsbComm.MHID_CMD_RESPONSE_BIT)) AndAlso (inputReportBuffer(2) = &H1) AndAlso (inputReportBuffer(3) = CByte(AscW("S"))) Then
            bw.ReportProgress(4, "Firmware Update in Progress")
            firmStatus = gUsbComm.program()
            If firmStatus = "finished" Then
                fwUpdateFailed = False
                'firmStatus = "Reboot GFT";
                bw.ReportProgress(5, "Rebooting Device")
                'reboot
                Thread.Sleep(200)
                outputReportBuffer(1) = CByte(gUsbComm.mhidCmd.MHID_CMD_REBOOT)
                err = gUsbComm.MyHid.OutputReports(outputReportBuffer)
                Debug.WriteLine("Send Output Report -> bwUpdateFirmware_DoWork()")
                needsFirmware = False
            ElseIf firmStatus = "badfile" Then
                fwUpdateFailed = True
                bw.ReportProgress(2, "Downloading Firmware")
            Else
                'firmStatus = "Program firmware failed, try again";
                bw.ReportProgress(6, "Firmware Update Failed. Please Try Again.")
                needsFirmware = False
                fwUpdateFailed = True
            End If
        Else
            'lblUpdateFirm.Text = "Erase firmware failed, try again";ReportProgress
            bw.ReportProgress(7, "Firmware Update Failed. Please Try Again.")
            needsFirmware = False
            fwUpdateFailed = True
        End If
    End Sub

    Private Sub bwUpdateFirmware_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bwUpdateFirmware.ProgressChanged
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Select Case e.ProgressPercentage
            Case 1
                'MessageBox.Show(e.UserState)
                gMessage.MessageBox(e.UserState, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Select
            Case 2
                frmMain.lblStatus.Text = "Preparing to Update Firmware"
                frmMain.tslGFT.BackColor = System.Drawing.Color.LightGreen
                'frmMain.progBar.Value = 25
                frmMain.UpdateProgressBar(25)
                Exit Select
            Case 3
                frmMain.lblStatus.Text = "Firmware Update in Progress"
                frmMain.tslGFT.BackColor = System.Drawing.Color.LightGreen
                'frmMain.progBar.Value = 50
                frmMain.UpdateProgressBar(50)
                Exit Select
            Case 4
                frmMain.lblStatus.Text = "Firmware Update in Progress"
                frmMain.tslGFT.BackColor = System.Drawing.Color.LightGreen
                'frmMain.progBar.Value = 75
                frmMain.UpdateProgressBar(75)
                Exit Select
            Case 5
                frmMain.lblStatus.Text = "Rebooting Device"
                frmMain.tslGFT.BackColor = System.Drawing.Color.Green
                'frmMain.progBar.Value = 100
                frmMain.UpdateProgressBar(100)
                Exit Select
            Case 6
                frmMain.lblStatus.Text = "Firmware Update Failed. Please Try Again."
                frmMain.tslGFT.BackColor = System.Drawing.Color.Red
                Exit Select
            Case 7
                frmMain.lblStatus.Text = "Firmware Update Failed. Please Try Again."
                frmMain.tslGFT.BackColor = System.Drawing.Color.Red
        End Select
    End Sub

    Private Sub bwUpdateFirmware_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bwUpdateFirmware.RunWorkerCompleted
        'fwUpdateFailed = True ' Force "fail" for testing
        If fwUpdateFailed Then
            fwUpdateAttempts += 1
            doingFWUpdate = False
            If fwUpdateAttempts > 5 Then
                gMessage.MessageBox("Unable to Update Device Firmware." & vbCrLf & "Please unplug the device and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                System.IO.File.Delete(sFirmwareFile)
                System.IO.File.Delete(sFirmwareFile_3)
                System.IO.File.Delete(sFirmwareFile_4)
                System.IO.File.Delete(sFirmwareFile_3_Perf)
                eDownload = downloadType.GFT_FIRMARE
                needsFirmware = True
                downloadFile()
            End If
        Else
            doingFWUpdate = False
            fwUpdateAttempts = 0
        End If
    End Sub

    Private Sub bwLoad_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwLoad.DoWork
        If gUsbComm Is Nothing And Not needsUpdate And Not WirelessViewerOpen Then
            gUsbComm = New gUsbComm(myHandle, GFT.MY_VENDOR_ID, GFT.MY_PRODUCT_ID)
            gUsbComm.MyHid.findTargetDevice(GFT.MY_VENDOR_ID, GFT.MY_PRODUCT_ID)
        End If
        If readWirelessDB Is Nothing Then
            readWirelessDB = New readWirelessDB()
            readWirelessDB.updateWirelessDb()
        End If
    End Sub


    Private Sub bwCheckDevice_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwCheckDevice.DoWork
        'Thread.Sleep(10000)
        Dim i
        For i = 1 To 10
            If Not bwCheckDevice.CancellationPending Then Thread.Sleep(1000)
        Next

        If Not bwCheckDevice.CancellationPending Then
            If Not bwUploadData.IsBusy() And Not frmMain.isUploading Then ' Don't do any work while uploaddata is running
                If Not currentGftProfile Is Nothing Then
                    If Not needsFirmware And Not currentGftProfile.sSerial Is Nothing Then
                        Dim cloudMyFirmware = sCloudFirmwareVer
                        If currentGftProfile.firmMajor = 9 Or (currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor = 9) Or (currentGftProfile.hwMajor = 3 And currentGftProfile.hwMinor = 1) Then
                            cloudMyFirmware = sCloudFirmwareVer3Perf
                            gftType = "Performance"
                            currentGftProfile.hwType = gftType
                        ElseIf currentGftProfile.hwMajor = 3 Then
                            cloudMyFirmware = sCloudFirmwareVer3
                            gftType = "Impact"
                            currentGftProfile.hwType = gftType
                        ElseIf currentGftProfile.hwMajor = 4 Then
                            cloudMyFirmware = sCloudFirmwareVer4
                            gftType = "Impact"
                            currentGftProfile.hwType = gftType
                        Else
                            cloudMyFirmware = sCloudFirmwareVer
                            gftType = "Impact"
                            currentGftProfile.hwType = gftType
                        End If
                        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
                        Debug.WriteLine("DOING WORK IN CHECKDEVICE THREAD")
                        Debug.WriteLine("REST CHECK DEVICE")
                        rest.checkDevice(currentGftProfile.sSerial, currentGftProfile, frmMain.gftBattVol, gftType)

                        ' Firmware Check... Who Knows? Maybe it was updated while the client was connected?
                        Dim cloudMajor As Integer = 0
                        Dim cloudMinor As Integer = 0
                        Dim fwbits = cloudMyFirmware.Split(".")
                        If fwbits.Length >= 2 Then
                            cloudMajor = CInt(fwbits(0))
                            cloudMinor = CInt(fwbits(1))
                        End If
                        If (((currentGftProfile.firmMajor = 3 And currentGftProfile.firmMinor < 2) Or currentGftProfile.firmMajor < 3) And currentGftProfile.firmMajor > 0) Then
                            ' Erase Calibration Data
                            Dim mq As Int16() = New Int16(3) {}
                            mq(0) = 0
                            mq(1) = 0
                            mq(3) = 0
                            mq(3) = 0
                            rest.calibrate(mq, False, False, False, False, False) ' Wipe Cloud Calibration data
                            gUsbComm.resetGft()
                            bw.ReportProgress(5, "")
                        ElseIf (cloudMajor > currentGftProfile.firmMajor Or (cloudMajor = currentGftProfile.firmMajor And cloudMinor > currentGftProfile.firmMinor)) And currentGftProfile.firmMajor > 0 Then
                            'If currentGftProfile.firmMajor & "." & currentGftProfile.firmMinor <> sCloudFirmwareVer And Not needsFirmware Then
                            ' we need to download the new firmware an install it
                            bw.ReportProgress(3, "")
                            bw.CancelAsync()
                        ElseIf rest.deviceState = gftDeviceState.DEVICE_REGISTERED Then
                            Debug.WriteLine("DEVICE IS REGISTERED")
                            ' Existing User
                            bw.ReportProgress(1, currentGftProfile.sName)

                            checkDeviceSettings()
                            If needsSettings Then
                                bw.CancelAsync()
                                Return
                            End If

                            'init calibration
                            If currentGftProfile.iLocation(0) = 0 AndAlso currentGftProfile.iLocation(1) = 0 AndAlso currentGftProfile.iLocation(2) = 0 AndAlso currentGftProfile.iLocation(3) = 0 Then
                                bw.ReportProgress(2, "Please calibrate your GFT from the Tools menu!")
                                bw.CancelAsync()
                            Else
                                gCalc.initQ(currentGftProfile.iLocation, currentGftProfile.bMountInside)

                                'download data from gft
                                'Refresh()
                                gUsbComm.gftFinishedUpload = False
                                frmMain.iSessionCnt = 0
                            End If
                        Else
                            ' GFT is not assigned.. do nothing
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub bwCheckDevice_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bwCheckDevice.ProgressChanged
        Select Case e.ProgressPercentage
            Case 1
                If Not e.UserState Is Nothing Then
                    ' Change Name
                    Dim sName As String = e.UserState.ToString().Trim()
                    'frmMain.tslStatus3.Text = sName

                    frmMain.statusText = sName
                    frmMain.SetProgressBarText()
                End If
                Exit Select
            Case 2
                ' Calibration Required
                If needsCalibration = False Then
                    needsCalibration = True
                End If
                Exit Select
            Case 3
                ' Firmware Update Required
                If needsFirmware = False Then
                    needsFirmware = True
                    updateFirmware()
                End If
                Exit Select
            Case 4
                ' Do nothing...

                ' New User
                'frmMain.tslStatus3.Text = "New User"

                'frmMain.statusText = "New User"
                'frmMain.SetProgressBarText()
                'frmMain.lblStatus.Text = "After Registration Disconnect and Reconnect Device"
                'frmRegister.ShowDialog()
            Case 5
                ' Firmware Update Required
                If needsFirmware = False Then
                    needsFirmware = True
                    updateFirmware(True)
                End If
                Exit Select
        End Select
    End Sub


    Private Sub bwCheckDevice_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bwCheckDevice.RunWorkerCompleted
        If Not bwUploadData.IsBusy And Not bwPlugUSB.IsBusy And Not doingConfig And Not doingFWUpdate And Not currentGftProfile Is Nothing Then ' Don't interrupt the upload thread...
            If frmMain.statusText = "New User" Then
                ' do nothing..
            ElseIf needsFirmware Then
                bwCheckDevice.CancelAsync()
            ElseIf needsCalibration = True Then
                bwPlugUSB.CancelAsync()
                bwUploadData.CancelAsync()
                bwCheckDevice.CancelAsync()
                If currentGftProfile.iCloudSports = 37 Then
                    ' Temporary workaround for Artaflex. They need automatic calibration for Chin Strapped GFT's
                    gMessage.MessageBox("Device Calibration Update Requred. The attached device will be updated with the Chin Strap Calibration.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    gUsbComm.calibrateGFTChinStrap()
                Else
                    If Not frmCalibrate_new.Visible Then
                        Dim dresult As DialogResult = gMessage.MessageBox("Before using your device, you must first calibrate it.  Please press OK to calibrate now.", Application.ProductName & " - Calibration Required", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        If dresult = Windows.Forms.DialogResult.OK And Not currentGftProfile Is Nothing Then
                            '/MsgBox("", MsgBoxStyle.OkOnly, "GForce Tracker")

                            frmMain.lblStatus.Text = "Device Calibration in Progress"
                            frmMain.lblStatus.Visible = True
                            frmCalibrate_new.ShowDialog()
                        End If
                    End If
                End If
            ElseIf needsSettings = True Then
                bwPlugUSB.CancelAsync()
                bwUploadData.CancelAsync()
                bwCheckDevice.CancelAsync()
                doingConfig = True
                Dim dresult As DialogResult = gMessage.MessageBox("New Settings have been detected for your device. " & vbCrLf & "Changes:" & vbCrLf & needsSettingsReasons & vbCrLf & "Press OK to update your device.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                If dresult = Windows.Forms.DialogResult.OK And Not currentGftProfile Is Nothing Then
                    Debug.WriteLine("ok")
                    gUsbComm.configGft()
                    Thread.Sleep(3000)
                    rest.deviceUpdated(currentGftProfile.sSerial)
                    If Not gUsbComm.getProfile() Then
                        unPlugUsb() ' Config failed... disconnect
                    End If
                End If
                'MessageBox.Show("New Settings have been detected for your device.  Press OK to update your device.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                doingConfig = False
            End If
        End If
        If Not bwCheckDevice.IsBusy And Not needsFirmware And Not WirelessViewerOpen And Not needsCalibration And Not bwCheckDevice.CancellationPending Then bwCheckDevice.RunWorkerAsync()
    End Sub



    Private Sub bwWirelessData_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwWirelessData.DoWork
        frmMain.isUploading = True ' Set this flag to prevent bwCheckDevice from doing any work.
        Debug.WriteLine("STARTING TO DO WORK IN UPLOAD DATA THREAD - SIDELINE VIEWER DATA")
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim retValue As Char = "0"c
        Try
            'Thread.Sleep(1000)
            If Thread.CurrentThread.Name Is Nothing Then
                Thread.CurrentThread.Name = "uploadDataSidelineViewer"
            End If
            Debug.WriteLine("Upload thread created")
            currentGftProfile = New gftProfile ' Need to have a valid reference to this. We are going to pretend that the selected GFT is plugged in
            'currentGftProfile.sSerial =   '"000D1C0000F4" ' the value stored in Jason's database is a bit odd. need to split it every two chars, reverse and recombine
            retValue = readWirelessDB.dbToColSession(sender, e, UploadSVSessions.currentGID)
            If (retValue <> "0"c) AndAlso (retValue <> "1"c) Then
                gMessage.MessageBox("Data retrieval was interrupted. " & vbCrLf & "Please re-attach the GFT.  " & vbCrLf & "If this problem persists, attempt to unassign the device in the " & vbCrLf & "Cloud Control Panel under Device Settings and reassign the device " & vbCrLf & "to the user.", "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'MessageBox.Show("Data retrieval was interrupted. Please re-attach the GFT.  If this problem persists, attempt to unassign the device in the Cloud Control Panel under Device Settings and reassign the device to the user.", "Try Again", MessageBoxButtons.OK)
            End If

            Debug.WriteLine("Upload thread exit")
        Catch ex As Exception
            gMessage.MessageBox("Data retrieval was interrupted. " & vbCrLf & "Please try again. " & vbCrLf & "If this problem persists, attempt to unassign the device in the " & vbCrLf & "Cloud Control Panel under Device Settings and reassign the device " & vbCrLf & "to the user." & vbCrLf & vbCrLf & "Error: " & ex.Message, "Try Again", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'MessageBox.Show("Data retrieval was interrupted. Please try again. If this problem persists, attempt to unassign the device in the Cloud Control Panel under Device Settings and reassign the device to the user." & vbCrLf & vbCrLf & "Error: " & ex.Message, "Try Again", MessageBoxButtons.OK)
        End Try
        Return
    End Sub

    Private Sub bwWirelessData_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bwWirelessData.RunWorkerCompleted
        Debug.WriteLine("COMPLETING WORKER ASYNC FOR UPLOAD DATA")
        Dim DoneUpload As Boolean = True
        Dim ts As TimeSpan
        frmMain.timerStatus.Stop()
        UploadSVSessions.UpdateProgressBar(100)
        'frmMain.lblStatus.Text = "Wireless Viewer Data Uploaded to Cloud"
        'frmMain.SetProgressBarText()

        'Me.menuCalibrate.Enabled = True
        'Me.menuDiagnostics.Enabled = True
        Dim stopWatch As New Stopwatch()
        stopWatch.Start()
        While True
            stopWatch.Stop()
            ts = stopWatch.Elapsed
            If ts.Seconds > 1 Then
                Exit While
            Else
                stopWatch.Start()
            End If
        End While
        If UploadSVSessions.uploadAll Then
            ' Get the next GID to upload
            Dim NextGID = UploadSVSessions.getNextGID()
            If NextGID <> "" Then
                DoneUpload = False
            End If

        End If
        If DoneUpload Then
            'If MessageBox.Show("New session data has been uploaded to the Cloud.  Would you like to review this data now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            If gMessage.MessageBox("New session data has been uploaded to the Cloud.  Would you like to review this data now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim wndName As String = "| GForce Tracker"
                Dim ptr As IntPtr
                For Each proc As Process In Process.GetProcesses()
                    If proc.MainWindowTitle.Contains(wndName) Then
                        ptr = proc.MainWindowHandle
                    End If
                Next
                If ptr <> 0 Then
                    BringWindowToTop(ptr)
                    ShowWindow(ptr, SW_SHOWMAXIMIZED)
                Else
                    System.Diagnostics.Process.Start("http://gft.gforcetracker.com")
                End If
            End If
        End If
        frmMain.lblStatus.Visible = True

        frmMain.isUploading = False
        'unPlugUsb()

        currentGftProfile = Nothing ' Unset the ephemeral profile
        Debug.WriteLine("Sideline Viewer Upload thread finished")
        If DoneUpload Then
            UploadSVSessions.EnableUploadButtons()
        Else
            UploadSVData() ' Upload the next GID in the list
        End If
    End Sub

    Private Sub bwWirelessData_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bwWirelessData.ProgressChanged
        Debug.WriteLine("GOT MESSAGE FROM SIDELINE VIEWER UPLOAD THREAD : " & e.ProgressPercentage)

        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Select Case e.ProgressPercentage
            Case 1
                ' Change Label
                ' If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                '                    progressBarStatus.Show()
                'frmMain.lblStatus.Text = DirectCast(e.UserState, String)
                ' End If
                Exit Select
            Case 2
                ' Error in Transmission
                bw.CancelAsync()
                gMessage.MessageBox("There was an error in retrieving the session data." & vbCrLf & "Please try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                'MsgBox("" & vbCrLf & vbCrLf & "Error Message: " & e.UserState, MsgBoxStyle.OkOnly, "GForce Tracker")
                Exit Select
            Case 3
                'Report # of Sessions
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    ' frmMain.iSessionCnt = 1
                    ' frmMain.lblStatus.Text = "Uploading Session Data to Cloud"
                End If
                Exit Select
            Case 4
                ' Retrieve Sessions from Device
            Case 5
                ' Upload Session
                If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                    ' frmMain.iSessionCnt = e.UserState
                    'frmMain.lblStatus.Text = "Uploading Session Data to Cloud"
                End If
                Exit Select
            Case 6
                ' Uploading Impacts
                'If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                ' frmMain.lblStatus.Text = "Uploading Impacts for Session"
                'End If
                Exit Select
            Case 7
                'Report # of Impacts
                'If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                ' frmMain.iImpactCnt = e.UserState
                'End If
                Exit Select
            Case 8
                ' Session Upload Complete
                Exit Select
            Case 9
                ' Erase Session Data
                'If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                'frmMain.lblStatus.Text = "Erasing Session Data from Device - Do Not Disconnect"
                'End If
            Case 10
                ' Update progress bar
                Debug.WriteLine("Update progress bar: " & e.UserState)
                'frmMain.progBar.Value = CInt(e.UserState)
                UploadSVSessions.UpdateProgressBar(CInt(e.UserState))
                Exit Select
        End Select
    End Sub


    Friend Const DBT_DEVICEARRIVAL As Int32 = &H8000
    Friend Const DBT_DEVICEREMOVECOMPLETE As Int32 = &H8004
    Friend Const DBT_DEVTYP_DEVICEINTERFACE As Int32 = 5
    Friend Const DBT_DEVTYP_HANDLE As Int32 = 6
    Friend Const DEVICE_NOTIFY_ALL_INTERFACE_CLASSES As Int32 = 4
    Friend Const DEVICE_NOTIFY_SERVICE_HANDLE As Int32 = 1
    Friend Const DEVICE_NOTIFY_WINDOW_HANDLE As Int32 = 0
    Friend Const WM_DEVICECHANGE As Int32 = &H219
    Friend Const DBT_DEVNODES_CHANGED As Int32 = &H7


    <DllImport("user32.dll", SetLastError:=True)> _
    Friend Shared Function UnregisterDeviceNotification(Handle As IntPtr) As [Boolean]
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
    Friend Shared Function RegisterDeviceNotification(hRecipient As IntPtr, NotificationFilter As IntPtr, Flags As Int32) As IntPtr
    End Function

    <DllImport("hid.dll", SetLastError:=True)> _
    Friend Shared Sub HidD_GetHidGuid(ByRef HidGuid As System.Guid)
    End Sub

    ' Two declarations for the DEV_BROADCAST_DEVICEINTERFACE structure.

    ' Use this one in the call to RegisterDeviceNotification() and
    ' in checking dbch_devicetype in a DEV_BROADCAST_HDR structure:

    <StructLayout(LayoutKind.Sequential)> _
    Friend Class DEV_BROADCAST_DEVICEINTERFACE
        Friend dbcc_size As Int32
        Friend dbcc_devicetype As Int32
        Friend dbcc_reserved As Int32
        Friend dbcc_classguid As Guid
        Friend dbcc_name As Int16
    End Class

    ' Use this to read the dbcc_name String and classguid:

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Friend Class DEV_BROADCAST_DEVICEINTERFACE_1
        Friend dbcc_size As Int32
        Friend dbcc_devicetype As Int32
        Friend dbcc_reserved As Int32
        <MarshalAs(UnmanagedType.ByValArray, ArraySubType:=UnmanagedType.U1, SizeConst:=16)> _
        Friend dbcc_classguid As [Byte]()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=255)> _
        Friend dbcc_name As [Char]()
    End Class


    Private Function registerForDeviceNotifications(windowHandle As IntPtr) As Boolean
        Dim deviceNotificationHandle As IntPtr
        Debug.WriteLine("easyHID:registerForDeviceNotifications() -> Method called")

        ' A DEV_BROADCAST_DEVICEINTERFACE header holds information about the request.
        Dim devBroadcastDeviceInterface As New DEV_BROADCAST_DEVICEINTERFACE()
        Dim devBroadcastDeviceInterfaceBuffer As IntPtr = IntPtr.Zero
        Dim size As Int32 = 0

        ' Get the required GUID
        Dim systemHidGuid As System.Guid = New Guid()
        HidD_GetHidGuid(systemHidGuid)

        Try
            ' Set the parameters in the DEV_BROADCAST_DEVICEINTERFACE structure.
            size = Marshal.SizeOf(devBroadcastDeviceInterface)
            devBroadcastDeviceInterface.dbcc_size = size
            devBroadcastDeviceInterface.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE
            devBroadcastDeviceInterface.dbcc_reserved = 0
            devBroadcastDeviceInterface.dbcc_classguid = systemHidGuid

            devBroadcastDeviceInterfaceBuffer = Marshal.AllocHGlobal(size)
            Marshal.StructureToPtr(devBroadcastDeviceInterface, devBroadcastDeviceInterfaceBuffer, True)

            ' Register for notifications and store the returned handle
            deviceNotificationHandle = RegisterDeviceNotification(windowHandle, devBroadcastDeviceInterfaceBuffer, DEVICE_NOTIFY_WINDOW_HANDLE)
            'deviceInformation.deviceNotificationHandle = deviceNotificationHandle;
            Marshal.PtrToStructure(devBroadcastDeviceInterfaceBuffer, devBroadcastDeviceInterface)

            If (deviceNotificationHandle.ToInt32() = IntPtr.Zero.ToInt32()) Then
                Debug.WriteLine("easyHID:registerForDeviceNotifications() -> Notification registration failed")
                Return False
            Else
                Debug.WriteLine("easyHID:registerForDeviceNotifications() -> Notification registration succeded")
                Return True
            End If
        Catch generatedExceptionName As Exception
            Debug.WriteLine("easyHID:registerForDeviceNotifications() -> EXCEPTION: An unknown exception has occured!")
        Finally
            ' Free the memory allocated previously by AllocHGlobal.
            If devBroadcastDeviceInterfaceBuffer <> IntPtr.Zero Then
                Marshal.FreeHGlobal(devBroadcastDeviceInterfaceBuffer)
            End If
        End Try
        Return False
    End Function



End Class
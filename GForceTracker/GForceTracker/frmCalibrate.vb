﻿Imports gForceTracker.frmNotify

Class frmCalibrate

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        If Not currentGftProfile Is Nothing Then

            ' Add any initialization after the InitializeComponent() call.
            radioMntLocationInside.Checked = currentGftProfile.bMountInside
            radioMntLocationOutside.Checked = Not currentGftProfile.bMountInside
            'currentGftProfile.bMountUSB = radioMountUSBFront.Checked

            radioButtonFront.Checked = currentGftProfile.bMountUSBFront
            radioButtonRear.Checked = Not currentGftProfile.bMountUSBFront
            radioButtonLeft.Checked = currentGftProfile.bMountUSBLeft
            radioButtonRight.Checked = currentGftProfile.bMountUSBRight

            radioButtonProxOn.Checked = currentGftProfile.bProximityEnabled
        End If

    End Sub

    Private Sub btnCalibrate_Click(sender As Object, e As EventArgs) Handles btnCalibrate.Click
        currentGftProfile.bMountInside = radioMntLocationInside.Checked
        'currentGftProfile.bMountUSB = radioMountUSBFront.Checked

        currentGftProfile.bMountUSBFront = radioButtonFront.Checked
        currentGftProfile.bMountUSBRear = radioButtonRear.Checked
        currentGftProfile.bMountUSBLeft = radioButtonLeft.Checked
        currentGftProfile.bMountUSBRight = radioButtonRight.Checked

        currentGftProfile.bProximityEnabled = radioButtonProxOn.Checked
        ' currentGftProfile.bTestMode = radioButtonTestOn.Checked

        gUsbComm.calibrateGFT(radioMntLocationInside.Checked, radioButtonFront.Checked, radioButtonRear.Checked, radioButtonLeft.Checked, radioButtonRight.Checked)



        'gUsbComm.calibrateGFT(radioMntLocationInside.Checked, radioMountUSBFront.Checked)
        needsCalibration = False
        Me.Close()
    End Sub


End Class

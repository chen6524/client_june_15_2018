﻿Imports System.Data.SqlServerCe
Imports System.Data.SqlClient
Imports System.Text.Encoding
Imports gForceTracker.frmNotify
Imports System.ComponentModel
Public Class readWirelessDB


    Public Function FlipGID(ByVal GID As String)
        Dim strings As New List(Of String)
        For i As Integer = 0 To GID.Length - 1 Step 2
            strings.Add(GID.Substring(i, 2))
        Next
        strings.Reverse()
        GID = String.Join("", strings)
        Return GID
    End Function

    Public Function updateWirelessDb()
        ' Check if we need to make structural changes to the wireless database
        Dim con As SqlCeConnection = New SqlCeConnection()
        con = New SqlCeConnection("Data Source=gForceTracker.sdf;Password=123456;Persist Security Info=True")
        '
        Using con
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim activeColExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SessionWireless' AND COLUMN_NAME = 'ACTIVE' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    activeColExists = sqlResult.Read()
                End With
                If Not activeColExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD ACTIVE int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                End If
            Catch ex As Exception
                'uhoh
            End Try
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim activeColExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'gftWireless' AND COLUMN_NAME = 'MID' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    activeColExists = sqlResult.Read()
                End With
                If Not activeColExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE gftWireless ADD MID NCHAR(10)"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                End If
            Catch ex As Exception
                'uhoh
            End Try

            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim activeColExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SessionWireless' AND COLUMN_NAME = 'ERASED' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    activeColExists = sqlResult.Read()
                End With
                If Not activeColExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD ERASED NCHAR(1)"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                End If
            Catch ex As Exception
                'uhoh
            End Try

            Dim swMIDColExists = True
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SessionWireless' AND COLUMN_NAME = 'MID' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    swMIDColExists = sqlResult.Read()
                End With
                If Not swMIDColExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD MID NCHAR(10)"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                End If
            Catch ex As Exception
                'uhoh
            End Try
            If Not swMIDColExists Then
                ' We added the MID column.. We must update any existing records so that they do not "disappear" now
                Dim gidmid As DataTable
                Try
                    Dim sqlResult As SqlCeDataReader = Nothing
                    Dim cmd As SqlCeCommand = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "SELECT GID, MID FROM gftWireless"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        Using adapter = New SqlCeDataAdapter(cmd)
                            If con.State.ToString <> "Open" Then
                                con.Open()
                            End If
                            Debug.WriteLine("Connection Status: " & con.State.ToString)
                            gidmid = New DataTable
                            adapter.Fill(gidmid)
                        End Using
                    End With
                    If Not gidmid Is Nothing Then
                        For Each r As DataRow In gidmid.Rows
                            cmd = New SqlCeCommand
                            With cmd
                                .Connection = con
                                .CommandType = CommandType.Text
                                .CommandText = "UPDATE SessionWireless SET MID = @MID WHERE GID = @GID"
                                .Parameters.AddWithValue("@MID", r("MID").ToString())
                                .Parameters.AddWithValue("@GID", r("GID").ToString())
                                If con.State.ToString <> "Open" Then
                                    con.Open()
                                End If
                                Debug.WriteLine("Connection Status: " & con.State.ToString)
                                cmd.ExecuteNonQuery()
                            End With
                        Next
                    End If
                Catch ex As Exception
                    'uhoh
                End Try
            End If


            ' Columns added at the same time as BrIC
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim rotThreshold = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SessionWireless' AND COLUMN_NAME = 'ROT_THRESHOLD' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    rotThreshold = sqlResult.Read()
                End With
                If Not rotThreshold Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD ROT_THRESHOLD int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD GFT_FIRMWARE NVARCHAR(10)"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD GFT_SOFTWARE NVARCHAR(10)"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With


                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD iLOCATION0 int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD iLOCATION1 int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD iLOCATION2 int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD iLOCATION3 int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD MNT_INSIDE int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD UDATE DATETIME"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD BAT_STATUS int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD MAX_ROT_X float"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD MAX_ROT_Y float"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD MAX_ROT_Z float"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD BrIC float"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                End If
            Catch ex As Exception
                'uhoh
            End Try

            ' We need to store the original date attached to the session as per the upload data(before timestamp corrections)
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim osDateExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SessionWireless' AND COLUMN_NAME = 'O_SDATE' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    osDateExists = sqlResult.Read()
                End With
                If Not osDateExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD O_SDATE datetime"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD O_ON_TIME datetime"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD O_OFF_TIME datetime"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD O_IMPACT_TIME datetime"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD O_millisecond int"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                End If
            Catch ex As Exception
                'uhoh
            End Try

            ' Check if we have the time correction table
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim timeCorrectionTableExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TimeCorrection'"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    timeCorrectionTableExists = sqlResult.Read()
                End With
                If Not timeCorrectionTableExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "CREATE TABLE [TimeCorrection] ( [TID] INT NOT NULL IDENTITY(1,1), [SID] INT, [UID] INT NOT NULL, [DATE_FROM] DATETIME, [DATE_TO] DATETIME, [RAW_DATA] IMAGE);"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                End If
            Catch ex As Exception
                'uhoh
            End Try
            ' Add Columns for uncalibrated(raw) data
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim rawDataExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventsWireless' AND COLUMN_NAME = 'dataX_raw' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    rawDataExists = sqlResult.Read()
                End With
                If Not rawDataExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD dataX_raw image"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD dataY_raw image"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD dataZ_raw image"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD rdataX_raw image"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD rdataY_raw image"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With

                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE EventsWireless ADD rdataZ_raw image"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                End If
            Catch ex As Exception
                'uhoh
            End Try
            ' --------------


            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim rawDataExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SessionWireless' AND COLUMN_NAME = 'coefficient' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    rawDataExists = sqlResult.Read()
                End With
                If Not rawDataExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE SessionWireless ADD coefficient float"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With


                End If
            Catch ex As Exception
                'uhoh
            End Try


            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim rawDataExists = True
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'gftWireless' AND COLUMN_NAME = 'coefficient' ORDER BY ORDINAL_POSITION"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()
                    rawDataExists = sqlResult.Read()
                End With
                If Not rawDataExists Then
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "ALTER TABLE gftWireless ADD coefficient float NOT NULL DEFAULT '1.00'"
                        If con.State.ToString <> "Open" Then
                            con.Open()
                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With


                End If
            Catch ex As Exception
                'uhoh
            End Try

            ' If there are any downloaded sessions that don't have the ERASED flag set.. Set it!
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim activeColExists = True

                cmd = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE SessionWireless SET ERASED = 'Y' WHERE ERASED = '' OR ERASED IS NULL"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    cmd.ExecuteNonQuery()
                End With
            Catch ex As Exception
                'uhoh
            End Try

            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                Dim activeColExists = True

                cmd = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE PerfSessionWireless SET ERASED = 'Y' WHERE ERASED = '' OR ERASED IS NULL"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    cmd.ExecuteNonQuery()
                End With
            Catch ex As Exception
                'uhoh
            End Try
            ' If we have bad entries for the gftWireless table, clean it up
            '
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand

                cmd = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE SessionWireless SET ERASED = 'Y' WHERE ERASED = '' OR ERASED IS NULL"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    cmd.ExecuteNonQuery()
                End With
            Catch ex As Exception
                'uhoh
            End Try
        End Using


        Dim gw2_GID As List(Of String) = New List(Of String)
        Dim gw2_NODEID As List(Of String) = New List(Of String)
        Dim gw2_DATE As List(Of DateTime) = New List(Of DateTime)
        Dim con2 As SqlCeConnection = New SqlCeConnection("Data Source=gForceTracker.sdf;Password=123456;Persist Security Info=True")
        Using con2
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                With cmd
                    .Connection = con2
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT gw.GID, gw.nodeID, gw.DATE FROM gftWireless gw LEFT JOIN (SELECT COUNT(*) as cnt, GID FROM gftWireless GROUP BY GID) gw2 ON gw2.GID = gw.GID WHERE gw.MID = '' AND gw2.cnt > 1"
                    If con2.State.ToString <> "Open" Then
                        con2.Open()

                    End If
                    sqlResult = cmd.ExecuteReader()

                    Do While sqlResult.Read
                        gw2_GID.Add(sqlResult.Item("GID").ToString)
                        gw2_NODEID.Add(sqlResult.Item("nodeID").ToString)
                        gw2_DATE.Add(sqlResult.Item("DATE"))
                    Loop
                End With
            Catch ex As Exception
                'uhoh
            End Try
        End Using
        If (gw2_GID.Count > 0) Then
            con = New SqlCeConnection("Data Source=gForceTracker.sdf;Password=123456;Persist Security Info=True")
            Using con
                Try
                    Dim cmd As SqlCeCommand
                    For i As Integer = 0 To gw2_GID.Count - 1
                        cmd = New SqlCeCommand
                        With cmd
                            .Connection = con
                            .CommandType = CommandType.Text
                            .CommandText = "DELETE FROM gftWireless WHERE GID = @GID AND nodeID = @NODEID AND DATE = @DATE AND MID = ''"
                            .Parameters.AddWithValue("@GID", gw2_GID(i))
                            .Parameters.AddWithValue("@NODEID", gw2_NODEID(i))
                            .Parameters.AddWithValue("@DATE", gw2_DATE(i))
                            If con.State.ToString <> "Open" Then
                                con.Open()
                            End If
                            Debug.WriteLine("Connection Status: " & con.State.ToString)
                            cmd.ExecuteNonQuery()
                        End With
                    Next
                Catch ex As Exception
                    'uhoh
                End Try
            End Using
        End If



        ' Check if we have the Performance Tables
        Try
            con = New SqlCeConnection("Data Source=gForceTracker.sdf;Password=123456;Persist Security Info=True")
            Dim sqlResult As SqlCeDataReader = Nothing
            Dim cmd As SqlCeCommand = New SqlCeCommand
            Dim PerfTablesExist = True
            With cmd
                .Connection = con
                .CommandType = CommandType.Text
                .CommandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'PerfSessionWireless'"
                If con.State.ToString <> "Open" Then
                    con.Open()
                End If
                Debug.WriteLine("Connection Status: " & con.State.ToString)
                sqlResult = cmd.ExecuteReader()
                PerfTablesExist = sqlResult.Read()
            End With
            If Not PerfTablesExist Then
                cmd = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "CREATE TABLE [PerfSessionWireless](   [SID] INT NOT NULL IDENTITY (3,1),   [UID] INT NOT NULL,   [GID] NCHAR(12),   [SPORTS] INT NOT NULL,   [NAME] NVARCHAR(20),   [SDATE] DATETIME,   [ON_TIME] DATETIME NOT NULL,   [OFF_TIME] DATETIME NOT NULL,   [DURATION] INT,   [PLACE] NVARCHAR(20),   [IMPACT] INT,   [ALARM] INT,   [WTH_RANGE] TINYINT NOT NULL,   [WTH] INT,   [THRESHOLD] INT,   [ExerciseType] NCHAR(10),   [NOTES] NVARCHAR(100),   [ACTIVE] INT,   [ERASED] NCHAR(1),   [MID] NCHAR(10),   [ROT_THRESHOLD] INT,   [GFT_FIRMWARE] NVARCHAR(10),   [GFT_SOFTWARE] NVARCHAR(10),   [iLOCATION0] INT,   [iLOCATION1] INT,   [iLOCATION2] INT,   [iLOCATION3] INT,   [MNT_INSIDE] INT,   [UDATE] DATETIME,   [BAT_STATUS] INT,   [O_SDATE] DATETIME,   [O_ON_TIME] DATETIME,   [O_OFF_TIME] DATETIME,  [coefficient] FLOAT);"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    cmd.ExecuteNonQuery()
                End With

                cmd = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "CREATE TABLE [PerfWireless](   [EID] INT NOT NULL IDENTITY (1,1),   [SID] INT,   [PERF_TIME] DATETIME,   [LINEAR_ACC_X] FLOAT,   [LINEAR_ACC_Y] FLOAT,   [LINEAR_ACC_Z] FLOAT,   [LINEAR_ACC_RESULTANT] FLOAT,   [AZIMUTH] FLOAT,   [ELEVATION] FLOAT,   [ROTATIONAL_ACC_X] FLOAT,   [ROTATIONAL_ACC_Y] FLOAT,   [ROTATIONAL_ACC_Z] FLOAT,   [ROTATIONAL_ACC_RESULTANT] FLOAT,   [LOCATION_ID] TINYINT,   [dataX] IMAGE,   [dataY] IMAGE,   [dataZ] IMAGE,   [rdataX] IMAGE,   [rdataY] IMAGE,   [rdataZ] IMAGE,   [millisecond] INT,   [UID] INT,   [MAX_ROT_X] FLOAT,   [MAX_ROT_Y] FLOAT,   [MAX_ROT_Z] FLOAT,   [BrIC] FLOAT,   [O_PERF_TIME] DATETIME,   [O_millisecond] INT,   [dataX_raw] IMAGE,   [dataY_raw] IMAGE,   [dataZ_raw] IMAGE,   [rdataX_raw] IMAGE,   [rdataY_raw] IMAGE,   [rdataZ_raw] IMAGE,   [bucket_1] INT,   [bucket_2] INT,   [bucket_3] INT,   [bucket_4] INT,   [bucket_5] INT,   [bucket_6] INT,   [bucket_7] INT,   [bucket_8] INT,   [bucket_9] INT,   [bucket_10] INT,   [exp_count] INT,   [exp_power] INT);"
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    cmd.ExecuteNonQuery()
                End With
            End If
        Catch ex As Exception
            'uhoh
        End Try




        Return True
    End Function

    Public Function dbToColSession(sender As Object, e As DoWorkEventArgs, ByVal GID As String)
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim result As Boolean = False
        Dim Sessions As Dictionary(Of String, gftSession) = New Dictionary(Of String, gftSession)
        Dim PerfSessions As Dictionary(Of String, gftSession) = New Dictionary(Of String, gftSession)
        Dim colSession As New Collection()
        Dim colPerfSession As New Collection()
        Dim aSessions(0) As gftSession
        Dim aImpacts(0) As gftImpactData

        frmNotify.currentGftProfile.sSerial = FlipGID(GID)

        ' Get the sessions and load them into the Sessions dictionary
        Dim con As SqlCeConnection = New SqlCeConnection("Data Source=gForceTracker.sdf;Password=123456;Persist Security Info=True")
        Using con
            Try
                Dim sqlResult As SqlCeDataReader = Nothing
                Dim cmd As SqlCeCommand = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT sw.SID, sw.UID, sw.SDATE, sw.O_SDATE, sw.ON_TIME, sw.O_ON_TIME, sw.OFF_TIME, sw.O_OFF_TIME, sw.WTH, sw.THRESHOLD, sw.ACTIVE, sw.ROT_THRESHOLD,COALESCE(sw.GFT_FIRMWARE,'') as GFT_FIRMWARE,COALESCE(sw.GFT_SOFTWARE, '') as GFT_SOFTWARE,COALESCE(sw.iLOCATION0, 0) as iLOCATION0,COALESCE(sw.iLOCATION1, 0) as iLOCATION1,COALESCE(sw.iLOCATION2,0) as iLOCATION2,COALESCE(sw.iLOCATION3,0) as iLOCATION3,COALESCE(sw.MNT_INSIDE, 0) as MNT_INSIDE,COALESCE(sw.UDATE, GETDATE()) as UDATE, COALESCE(sw.BAT_STATUS, 50) as BAT_STATUS, COALESCE(sw.coefficient,1) as coefficient FROM SessionWireless sw WHERE GID=@GID@ AND ERASED = 'Y'"
                    .Parameters.AddWithValue("@GID@", GID)
                    If con.State.ToString <> "Open" Then
                        con.Open()

                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    sqlResult = cmd.ExecuteReader()

                    Do While sqlResult.Read
                        Debug.WriteLine("SID: " & sqlResult.Item("SID").ToString)
                        Debug.WriteLine("UID: " & sqlResult.Item("UID").ToString)
                        Debug.WriteLine("SDATE: " & sqlResult.Item("SDATE").ToString)
                        Debug.WriteLine("ON_TIME: " & sqlResult.Item("ON_TIME").ToString)
                        Debug.WriteLine("OFF_TIME: " & sqlResult.Item("OFF_TIME").ToString)
                        Debug.WriteLine("THRESHOLD: " & sqlResult.Item("THRESHOLD").ToString)
                        Debug.WriteLine("ACTIVE TIME: " & sqlResult.Item("ACTIVE").ToString)
                        Dim sData As New gftSession
                        Dim sessionLen As TimeSpan
                        sData.iImpact = 0
                        sData.iWTH = 0
                        sData.iAlarm = 0
                        sData.dOnTime = CDate(sqlResult.Item("ON_TIME"))
                        sData.dO_OnTime = CDate(sqlResult.Item("O_ON_TIME"))
                        sData.iWTH = sData.iWTH - sData.iAlarm
                        sData.dOffTime = CDate(sqlResult.Item("OFF_TIME"))
                        sData.dO_OffTime = CDate(sqlResult.Item("O_OFF_TIME"))
                        sData.dDate = sData.dOnTime
                        sData.dO_Date = sData.dO_OnTime
                        sessionLen = (sData.dOffTime - sData.dOnTime)
                        sData.iActiveTime = CInt(sqlResult.Item("ACTIVE"))
                        sData.iDuration = CInt(Math.Truncate(sessionLen.TotalMinutes))
                        If sData.iDuration = 0 Then sData.iDuration = 1
                        sData.iUID = CInt(sqlResult.Item("UID"))
                        sData.iThreshold = CInt(sqlResult.Item("THRESHOLD"))
                        If sqlResult.Item("BAT_STATUS").ToString() <> "" Then
                            sData.batStatus = CInt(sqlResult.Item("BAT_STATUS"))
                        End If
                        If sqlResult.Item("UDATE").ToString() <> "" Then
                            sData.uDate = CDate(sqlResult.Item("UDATE"))
                        End If
                        sData.dCoefficient = CDbl(sqlResult.Item("coefficient"))



                        'sData.iRotThreshold = CInt(sqlResult.Item("ROT_THRESHOLD"))
                        sData.fwVersion = sqlResult.Item("GFT_FIRMWARE")
                        sData.swVersion = sqlResult.Item("GFT_SOFTWARE")
                        sData.iLocation(0) = CInt(sqlResult.Item("iLOCATION0"))
                        sData.iLocation(1) = CInt(sqlResult.Item("iLOCATION1"))
                        sData.iLocation(2) = CInt(sqlResult.Item("iLOCATION2"))
                        sData.iLocation(3) = CInt(sqlResult.Item("iLOCATION3"))
                        If (CInt(sqlResult.Item("MNT_INSIDE")) = 0) Then
                            sData.iMntInside = False
                        Else
                            sData.iMntInside = True
                        End If
                        'Add it to the dictionary of sessions
                        Sessions.Add(sqlResult.Item("SID").ToString(), sData)
                    Loop
                End With
            Catch ex As Exception
                'uhoh
            End Try

            ' Now lets loop through the Sessions dictionary and retrieve the impacts
            Dim session As KeyValuePair(Of String, gftSession)
            For Each session In Sessions
                Dim colImpacts As New Collection
                ' Using con
                Try
                    Dim sqlResult As SqlCeDataReader = Nothing
                    Dim cmd As SqlCeCommand = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text 'ROTATIONAL_ACC_Z
                        .CommandText = "SELECT ew.IMPACT_TIME, ew.O_IMPACT_TIME, ew.LINEAR_ACC_X, ew.LINEAR_ACC_Y, ew.LINEAR_ACC_Z, ew.LINEAR_ACC_RESULTANT, ew.AZIMUTH, ew.ELEVATION, ew.ROTATIONAL_ACC_X, ew.ROTATIONAL_ACC_Y," _
                                    & "ew.ROTATIONAL_ACC_Z, ew.ROTATIONAL_ACC_RESULTANT, ew.HIC, ew.GSI, ew.PCS, ew.LOCATION_ID, ew.dataX, ew.dataY, ew.dataZ, ew.rdataX, ew.rdataY, ew.rdataZ, ew.millisecond, ew.O_millisecond, ew.UID,ew.MAX_ROT_X, ew.MAX_ROT_Y, ew.MAX_ROT_Z, ew.BrIC, ew.dataX_raw, ew.dataY_raw, ew.dataZ_raw, ew.rdataX_raw, ew.rdataY_raw, ew.rdataZ_raw " _
                                    & "FROM EventsWireless ew WHERE ew.SID = @SID@ AND ew.UID = @UID@ ORDER BY ew.EID ASC"
                        .Parameters.AddWithValue("@SID@", session.Key)
                        .Parameters.AddWithValue("@UID@", session.Value.iUID)
                        If con.State.ToString <> "Open" Then
                            con.Open()

                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        sqlResult = cmd.ExecuteReader()

                        Do While sqlResult.Read
                            Debug.WriteLine("IMPACT_TIME: " & sqlResult.Item("IMPACT_TIME").ToString)

                            Dim iData As New gftImpactData
                            iData.bDataX = New [Byte](119) {}
                            iData.bDataY = New [Byte](119) {}
                            iData.bDataZ = New [Byte](119) {}
                            iData.iLinX = CDbl(sqlResult.Item("LINEAR_ACC_X")) 'LINEAR_ACC_X
                            iData.iLinY = CDbl(sqlResult.Item("LINEAR_ACC_Y")) 'LINEAR_ACC_Y
                            iData.iLinZ = CDbl(sqlResult.Item("LINEAR_ACC_Z")) 'LINEAR_ACC_Z
                            iData.iLinR = CDbl(sqlResult.Item("LINEAR_ACC_RESULTANT")) 'LINEAR_ACC_RESULTANT

                            iData.bRDataX = New [Byte](63) {}
                            iData.bRDataY = New [Byte](63) {}
                            iData.bRDataZ = New [Byte](63) {}
                            iData.iGForce = iData.iLinR
                            iData.dTime = CDate(sqlResult.Item("IMPACT_TIME")) 'IMPACT_TIME
                            iData.d_OTime = CDate(sqlResult.Item("O_IMPACT_TIME")) 'IMPACT_TIME

                            iData.dTimeMS = CShort(sqlResult.Item("millisecond")) 'millisecond
                            iData.d_OTimeMS = CShort(sqlResult.Item("O_millisecond")) 'millisecond
                            iData.iRotX = CDbl(sqlResult.Item("ROTATIONAL_ACC_X")) 'ROTATIONAL_ACC_X
                            iData.iRotY = CDbl(sqlResult.Item("ROTATIONAL_ACC_Y")) 'ROTATIONAL_ACC_Y
                            iData.iRotZ = CDbl(sqlResult.Item("ROTATIONAL_ACC_Z")) 'ROTATIONAL_ACC_Z
                            iData.iRotR = CDbl(sqlResult.Item("ROTATIONAL_ACC_RESULTANT")) 'ROTATIONAL_ACC_RESULTANT

                            iData.bDataX = sqlResult.Item("dataX") 'dataX
                            iData.bDataY = sqlResult.Item("dataY") 'dataY
                            iData.bDataZ = sqlResult.Item("dataZ") 'dataZ

                            iData.bRDataX = sqlResult.Item("rdataX") 'rdataX
                            iData.bRDataY = sqlResult.Item("rdataY") 'rdataY
                            iData.bRDataZ = sqlResult.Item("rdataZ") 'rdataZ

                            If Not sqlResult.Item("dataX_raw") Is System.DBNull.Value Then
                                iData.bDataX_raw = sqlResult.Item("dataX_raw") 'dataX
                                iData.bDataY_raw = sqlResult.Item("dataY_raw") 'dataY
                                iData.bDataZ_raw = sqlResult.Item("dataZ_raw") 'dataZ
                            End If

                            If Not sqlResult.Item("rdataX_raw") Is System.DBNull.Value Then
                                iData.bRDataX_raw = sqlResult.Item("rdataX_raw") 'rdataX
                                iData.bRDataY_raw = sqlResult.Item("rdataY_raw") 'rdataY
                                iData.bRDataZ_raw = sqlResult.Item("rdataZ_raw") 'rdataZ
                            End If


                            iData.iGSI = CDbl(sqlResult.Item("GSI")) 'GSI
                            iData.iHIC = CDbl(sqlResult.Item("HIC")) 'HIC
                            iData.iAzimuth = CDbl(sqlResult.Item("AZIMUTH")) 'AZIMUTH
                            iData.iElevation = CDbl(sqlResult.Item("ELEVATION")) 'ELEVATION

                            'MAX_ROT_X, ew.MAX_ROT_Y, ew.MAX_ROT_Z, ew.BrIC
                            iData.iMaxRotX = CDbl(sqlResult.Item("MAX_ROT_X")) '
                            iData.iMaxRotY = CDbl(sqlResult.Item("MAX_ROT_Y")) '
                            iData.iMaxRotZ = CDbl(sqlResult.Item("MAX_ROT_Z")) '
                            iData.iBrIC = CDbl(sqlResult.Item("BrIC")) 'BrIC

                            'session.Value.dOffTime = iData.dTime ' Offtime should be the last impact time

                            Dim sessionLen As TimeSpan
                            sessionLen = (session.Value.dOffTime - session.Value.dOnTime)
                            session.Value.iDuration = CInt(Math.Truncate(sessionLen.TotalMinutes))
                            If session.Value.iDuration = 0 Then session.Value.iDuration = 1



                            session.Value.iImpact += 1
                            If iData.iLinR >= session.Value.iThreshold Then
                                session.Value.iAlarm += 1
                            End If
                            If iData.iLinR >= (session.Value.iThreshold) * 0.9 Then
                                session.Value.iWTH += 1
                            End If
                            colImpacts.Add(iData)
                        Loop
                    End With
                Catch ex As Exception
                    'uhoh
                End Try
                'End Using

                Dim iCopyIndex
                For iCopyIndex = 1 To colImpacts.Count
                    session.Value.aImpacts.Add(colImpacts(iCopyIndex))
                Next iCopyIndex
                aSessions(UBound(aSessions)) = session.Value
                colSession.Add(session.Value)
                colImpacts.Clear()
                ReDim Preserve aSessions(UBound(aSessions) + 1)
            Next

            ' Get Performance Sessions
                Try
                    Dim sqlResult As SqlCeDataReader = Nothing
                    Dim cmd As SqlCeCommand = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                    .CommandText = "SELECT sw.SID, sw.UID, sw.SDATE, sw.O_SDATE, sw.ON_TIME, sw.O_ON_TIME, sw.OFF_TIME, sw.O_OFF_TIME, sw.WTH, sw.THRESHOLD, sw.ACTIVE, sw.ROT_THRESHOLD,COALESCE(sw.GFT_FIRMWARE,'') as GFT_FIRMWARE,COALESCE(sw.GFT_SOFTWARE, '') as GFT_SOFTWARE,COALESCE(sw.iLOCATION0, 0) as iLOCATION0,COALESCE(sw.iLOCATION1, 0) as iLOCATION1,COALESCE(sw.iLOCATION2,0) as iLOCATION2,COALESCE(sw.iLOCATION3,0) as iLOCATION3,COALESCE(sw.MNT_INSIDE, 0) as MNT_INSIDE,COALESCE(sw.UDATE, GETDATE()) as UDATE, COALESCE(sw.BAT_STATUS, 50) as BAT_STATUS, sw.coefficient FROM PerfSessionWireless sw WHERE GID=@GID@ AND ERASED = 'Y'"
                        .Parameters.AddWithValue("@GID@", GID)
                        If con.State.ToString <> "Open" Then
                            con.Open()

                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        sqlResult = cmd.ExecuteReader()

                        Do While sqlResult.Read
                            Debug.WriteLine("SID: " & sqlResult.Item("SID").ToString)
                            Debug.WriteLine("UID: " & sqlResult.Item("UID").ToString)
                            Debug.WriteLine("SDATE: " & sqlResult.Item("SDATE").ToString)
                            Debug.WriteLine("ON_TIME: " & sqlResult.Item("ON_TIME").ToString)
                            Debug.WriteLine("OFF_TIME: " & sqlResult.Item("OFF_TIME").ToString)
                            Debug.WriteLine("THRESHOLD: " & sqlResult.Item("THRESHOLD").ToString)
                            Debug.WriteLine("ACTIVE TIME: " & sqlResult.Item("ACTIVE").ToString)
                            Dim sData As New gftSession
                            Dim sessionLen As TimeSpan
                            sData.iImpact = 0
                            sData.iWTH = 0
                            sData.iAlarm = 0
                            sData.dOnTime = CDate(sqlResult.Item("ON_TIME"))
                            sData.dO_OnTime = CDate(sqlResult.Item("O_ON_TIME"))
                            sData.iWTH = sData.iWTH - sData.iAlarm
                            sData.dOffTime = CDate(sqlResult.Item("OFF_TIME"))
                            sData.dO_OffTime = CDate(sqlResult.Item("O_OFF_TIME"))
                            sData.dDate = sData.dOnTime
                            sData.dO_Date = sData.dO_OnTime
                            sessionLen = (sData.dOffTime - sData.dOnTime)
                            sData.iActiveTime = CInt(sqlResult.Item("ACTIVE"))
                            sData.iDuration = CInt(Math.Truncate(sessionLen.TotalMinutes))
                            If sData.iDuration = 0 Then sData.iDuration = 1
                            sData.iUID = CInt(sqlResult.Item("UID"))
                            sData.iThreshold = CInt(sqlResult.Item("THRESHOLD"))
                            If sqlResult.Item("BAT_STATUS").ToString() <> "" Then
                                sData.batStatus = CInt(sqlResult.Item("BAT_STATUS"))
                            End If
                            If sqlResult.Item("UDATE").ToString() <> "" Then
                                sData.uDate = CDate(sqlResult.Item("UDATE"))
                            End If
                            sData.dCoefficient = CDbl(sqlResult.Item("coefficient"))



                            'sData.iRotThreshold = CInt(sqlResult.Item("ROT_THRESHOLD"))
                            sData.fwVersion = sqlResult.Item("GFT_FIRMWARE")
                            sData.swVersion = sqlResult.Item("GFT_SOFTWARE")
                            sData.iLocation(0) = CInt(sqlResult.Item("iLOCATION0"))
                            sData.iLocation(1) = CInt(sqlResult.Item("iLOCATION1"))
                            sData.iLocation(2) = CInt(sqlResult.Item("iLOCATION2"))
                            sData.iLocation(3) = CInt(sqlResult.Item("iLOCATION3"))
                            If (CInt(sqlResult.Item("MNT_INSIDE")) = 0) Then
                                sData.iMntInside = False
                            Else
                                sData.iMntInside = True
                            End If
                            'Add it to the dictionary of sessions
                        PerfSessions.Add(sqlResult.Item("SID").ToString(), sData)
                        Loop
                    End With
                Catch ex As Exception
                    'uhoh
                End Try
            ' Now lets loop through the Sessions dictionary and retrieve the impacts

            For Each session In PerfSessions
                    Dim colImpacts As New Collection
                    ' Using con
                    Try
                        Dim sqlResult As SqlCeDataReader = Nothing
                        Dim cmd As SqlCeCommand = New SqlCeCommand
                        With cmd
                            .Connection = con
                            .CommandType = CommandType.Text 'ROTATIONAL_ACC_Z
                            .CommandText = "SELECT ew.PERF_TIME, ew.O_PERF_TIME, ew.LINEAR_ACC_X, ew.LINEAR_ACC_Y, ew.LINEAR_ACC_Z, ew.LINEAR_ACC_RESULTANT, ew.AZIMUTH, ew.ELEVATION, ew.ROTATIONAL_ACC_X, ew.ROTATIONAL_ACC_Y," _
                                        & "ew.ROTATIONAL_ACC_Z, ew.ROTATIONAL_ACC_RESULTANT, ew.LOCATION_ID, ew.dataX, ew.dataY, ew.dataZ, ew.rdataX, ew.rdataY, ew.rdataZ, ew.millisecond, ew.O_millisecond, ew.UID,ew.MAX_ROT_X, ew.MAX_ROT_Y, ew.MAX_ROT_Z, ew.BrIC, ew.dataX_raw, ew.dataY_raw, ew.dataZ_raw, ew.rdataX_raw, ew.rdataY_raw, ew.rdataZ_raw, " _
                                        & "bucket_1,bucket_2,bucket_3,bucket_4,bucket_5,bucket_6,bucket_7,bucket_8,bucket_9,bucket_10,exp_count,exp_power " _
                                        & "FROM PerfWireless ew WHERE ew.SID = @SID@ AND ew.UID = @UID@ ORDER BY ew.EID ASC"
                        .Parameters.AddWithValue("@SID@", session.Key)
                            .Parameters.AddWithValue("@UID@", session.Value.iUID)
                            If con.State.ToString <> "Open" Then
                                con.Open()

                            End If
                            Debug.WriteLine("Connection Status: " & con.State.ToString)
                            sqlResult = cmd.ExecuteReader()

                            Do While sqlResult.Read
                                Debug.WriteLine("PERF_TIME: " & sqlResult.Item("PERF_TIME").ToString)

                                Dim iData As New gftImpactData
                                iData.bDataX = New [Byte](119) {}
                                iData.bDataY = New [Byte](119) {}
                                iData.bDataZ = New [Byte](119) {}
                                iData.iLinX = CDbl(sqlResult.Item("LINEAR_ACC_X")) 'LINEAR_ACC_X
                                iData.iLinY = CDbl(sqlResult.Item("LINEAR_ACC_Y")) 'LINEAR_ACC_Y
                                iData.iLinZ = CDbl(sqlResult.Item("LINEAR_ACC_Z")) 'LINEAR_ACC_Z
                                iData.iLinR = CDbl(sqlResult.Item("LINEAR_ACC_RESULTANT")) 'LINEAR_ACC_RESULTANT

                                iData.bRDataX = New [Byte](63) {}
                                iData.bRDataY = New [Byte](63) {}
                                iData.bRDataZ = New [Byte](63) {}
                                iData.iGForce = iData.iLinR
                                iData.dTime = CDate(sqlResult.Item("PERF_TIME")) 'IMPACT_TIME
                                iData.d_OTime = CDate(sqlResult.Item("O_PERF_TIME")) 'IMPACT_TIME

                                iData.dTimeMS = CShort(sqlResult.Item("millisecond")) 'millisecond
                                iData.d_OTimeMS = CShort(sqlResult.Item("O_millisecond")) 'millisecond
                                iData.iRotX = CDbl(sqlResult.Item("ROTATIONAL_ACC_X")) 'ROTATIONAL_ACC_X
                                iData.iRotY = CDbl(sqlResult.Item("ROTATIONAL_ACC_Y")) 'ROTATIONAL_ACC_Y
                                iData.iRotZ = CDbl(sqlResult.Item("ROTATIONAL_ACC_Z")) 'ROTATIONAL_ACC_Z
                                iData.iRotR = CDbl(sqlResult.Item("ROTATIONAL_ACC_RESULTANT")) 'ROTATIONAL_ACC_RESULTANT

                                iData.bDataX = sqlResult.Item("dataX") 'dataX
                                iData.bDataY = sqlResult.Item("dataY") 'dataY
                                iData.bDataZ = sqlResult.Item("dataZ") 'dataZ

                                iData.bRDataX = sqlResult.Item("rdataX") 'rdataX
                                iData.bRDataY = sqlResult.Item("rdataY") 'rdataY
                                iData.bRDataZ = sqlResult.Item("rdataZ") 'rdataZ

                                If Not sqlResult.Item("dataX_raw") Is System.DBNull.Value Then
                                    iData.bDataX_raw = sqlResult.Item("dataX_raw") 'dataX
                                    iData.bDataY_raw = sqlResult.Item("dataY_raw") 'dataY
                                    iData.bDataZ_raw = sqlResult.Item("dataZ_raw") 'dataZ
                                End If

                                If Not sqlResult.Item("rdataX_raw") Is System.DBNull.Value Then
                                    iData.bRDataX_raw = sqlResult.Item("rdataX_raw") 'rdataX
                                    iData.bRDataY_raw = sqlResult.Item("rdataY_raw") 'rdataY
                                    iData.bRDataZ_raw = sqlResult.Item("rdataZ_raw") 'rdataZ
                                End If


                                iData.iAzimuth = CDbl(sqlResult.Item("AZIMUTH")) 'AZIMUTH
                                iData.iElevation = CDbl(sqlResult.Item("ELEVATION")) 'ELEVATION

                                'MAX_ROT_X, ew.MAX_ROT_Y, ew.MAX_ROT_Z, ew.BrIC
                                iData.iMaxRotX = CDbl(sqlResult.Item("MAX_ROT_X")) '
                                iData.iMaxRotY = CDbl(sqlResult.Item("MAX_ROT_Y")) '
                                iData.iMaxRotZ = CDbl(sqlResult.Item("MAX_ROT_Z")) '
                                iData.iBrIC = CDbl(sqlResult.Item("BrIC")) 'BrIC

                                iData.bucket(0) = CInt(sqlResult.Item("bucket_1"))
                                iData.bucket(1) = CInt(sqlResult.Item("bucket_2"))
                                iData.bucket(2) = CInt(sqlResult.Item("bucket_3"))
                                iData.bucket(3) = CInt(sqlResult.Item("bucket_4"))
                                iData.bucket(4) = CInt(sqlResult.Item("bucket_5"))
                                iData.bucket(5) = CInt(sqlResult.Item("bucket_6"))
                                iData.bucket(6) = CInt(sqlResult.Item("bucket_7"))
                                iData.bucket(7) = CInt(sqlResult.Item("bucket_8"))
                                iData.bucket(8) = CInt(sqlResult.Item("bucket_9"))
                            iData.bucket(9) = CInt(sqlResult.Item("bucket_10"))
                            iData.bucket(10) = CInt(sqlResult.Item("exp_count"))
                            iData.bucket(11) = CInt(sqlResult.Item("exp_power"))

                                'session.Value.dOffTime = iData.dTime ' Offtime should be the last impact time

                                Dim sessionLen As TimeSpan
                                sessionLen = (session.Value.dOffTime - session.Value.dOnTime)
                                session.Value.iDuration = CInt(Math.Truncate(sessionLen.TotalMinutes))
                                If session.Value.iDuration = 0 Then session.Value.iDuration = 1



                                session.Value.iImpact += 1
                                If iData.iLinR >= session.Value.iThreshold Then
                                    session.Value.iAlarm += 1
                                End If
                                If iData.iLinR >= (session.Value.iThreshold) * 0.9 Then
                                    session.Value.iWTH += 1
                                End If
                                colImpacts.Add(iData)
                            Loop
                        End With
                    Catch ex As Exception
                        'uhoh
                    End Try
                    'End Using

                    Dim iCopyIndex
                    For iCopyIndex = 1 To colImpacts.Count
                        session.Value.aImpacts.Add(colImpacts(iCopyIndex))
                    Next iCopyIndex
                    aSessions(UBound(aSessions)) = session.Value
                colPerfSession.Add(session.Value)
                    colImpacts.Clear()
                    ReDim Preserve aSessions(UBound(aSessions) + 1)
            Next


            End Using

            Dim sessionSuccess As Boolean = False
            Dim impactID As String = ""
            Dim batchID As String = ""
            If colSession.Count > 0 Then
                bw.ReportProgress(3, UBound(aSessions))
                batchID = rest.createBatch()
                If batchID = "" Then
                    ' bw.ReportProgress(2, "Error Creating Batch on Server")
                    Return "0"c
                End If
                sessionSuccess = rest.writeSessions(colSession, batchID, frmNotify.currentGftProfile.sSerial, bw)
                If sessionSuccess = False Then
                    bw.ReportProgress(2, "Error Uploading Session Data to Cloud")
                    Return "0"c
                End If
                rest.completeBatch(batchID)
            eraseSessions(Sessions) ' erase the uploaded sessions from the database
                bw.ReportProgress(8, "Upload to Cloud Completed")
        End If

        batchID = ""

        If colPerfSession.Count > 0 Then
            bw.ReportProgress(3, UBound(aSessions))
            batchID = rest.createBatch()
            If batchID = "" Then
                ' bw.ReportProgress(2, "Error Creating Batch on Server")
                Return "0"c
            End If
            sessionSuccess = rest.writeSessions(colPerfSession, batchID, frmNotify.currentGftProfile.sSerial, bw, True)
            If sessionSuccess = False Then
                bw.ReportProgress(2, "Error Uploading Session Data to Cloud")
                Return "0"c
            End If
            rest.completeBatch(batchID)
            eraseSessions(PerfSessions, "PerfSessionWireless", "PerfWireless") ' erase the uploaded sessions from the database
            bw.ReportProgress(8, "Upload to Cloud Completed")
        End If


        Return "1"c
    End Function

    Public Sub eraseSessions(Sessions As Dictionary(Of String, gftSession), Optional SessionTable As String = "SessionWireless", Optional EventsTable As String = "EventsWireless")
        Dim con As SqlCeConnection = New SqlCeConnection("Data Source=gForceTracker.sdf;Password=123456;Persist Security Info=True")
        Using con
            Try
                Dim session As KeyValuePair(Of String, gftSession)
                For Each session In Sessions
                    Dim cmd As SqlCeCommand = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM " & SessionTable & " WHERE SID=@SID@ AND UID=@UID@"
                        .Parameters.AddWithValue("@SID@", session.Key)
                        .Parameters.AddWithValue("@UID@", session.Value.iUID)
                        If con.State.ToString <> "Open" Then
                            con.Open()

                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                    cmd = New SqlCeCommand
                    With cmd
                        .Connection = con
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM " & EventsTable & " WHERE SID=@SID@ AND UID=@UID@"
                        .Parameters.AddWithValue("@SID@", session.Key)
                        .Parameters.AddWithValue("@UID@", session.Value.iUID)
                        If con.State.ToString <> "Open" Then
                            con.Open()

                        End If
                        Debug.WriteLine("Connection Status: " & con.State.ToString)
                        cmd.ExecuteNonQuery()
                    End With
                Next
            Catch ex As Exception
                'uhoh
            End Try
        End Using
    End Sub

End Class

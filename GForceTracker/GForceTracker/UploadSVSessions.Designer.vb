﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UploadSVSessions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UploadSVSessions))
        Me.dvSessionList = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnUploadAll = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.progBar = New System.Windows.Forms.ProgressBar()
        Me.GID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlayerNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FNAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LNAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.numSessions = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.numImpacts = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Upload = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.dvSessionList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dvSessionList
        '
        Me.dvSessionList.AllowUserToAddRows = False
        Me.dvSessionList.AllowUserToDeleteRows = False
        Me.dvSessionList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvSessionList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GID, Me.PlayerNumber, Me.FNAME, Me.LNAME, Me.numSessions, Me.numImpacts, Me.Upload})
        Me.dvSessionList.Location = New System.Drawing.Point(12, 12)
        Me.dvSessionList.Name = "dvSessionList"
        Me.dvSessionList.ReadOnly = True
        Me.dvSessionList.Size = New System.Drawing.Size(677, 149)
        Me.dvSessionList.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 164)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(648, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Sideline Viewer has recorded Session and Impact Data. You may sync it with the cl" & _
    "oud here by clicking on the appropriate ""Cloud Sync"""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(81, 177)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "button on the right."
        '
        'btnUploadAll
        '
        Me.btnUploadAll.Location = New System.Drawing.Point(433, 198)
        Me.btnUploadAll.Name = "btnUploadAll"
        Me.btnUploadAll.Size = New System.Drawing.Size(127, 23)
        Me.btnUploadAll.TabIndex = 3
        Me.btnUploadAll.Text = "Sync All Sessions"
        Me.btnUploadAll.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(566, 198)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(123, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(171, 177)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(424, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Or, you may choose to sync all data by clicking on the ""Sync All Sessions"" button" & _
    " below."
        '
        'progBar
        '
        Me.progBar.Location = New System.Drawing.Point(12, 198)
        Me.progBar.Name = "progBar"
        Me.progBar.Size = New System.Drawing.Size(415, 23)
        Me.progBar.TabIndex = 6
        '
        'GID
        '
        Me.GID.DataPropertyName = "GID"
        Me.GID.HeaderText = "GID"
        Me.GID.Name = "GID"
        Me.GID.ReadOnly = True
        Me.GID.Visible = False
        '
        'PlayerNumber
        '
        Me.PlayerNumber.DataPropertyName = "PlayerNumber"
        Me.PlayerNumber.HeaderText = "Player"
        Me.PlayerNumber.MinimumWidth = 60
        Me.PlayerNumber.Name = "PlayerNumber"
        Me.PlayerNumber.ReadOnly = True
        Me.PlayerNumber.Width = 60
        '
        'FNAME
        '
        Me.FNAME.DataPropertyName = "FNAME"
        Me.FNAME.HeaderText = "First Name"
        Me.FNAME.MinimumWidth = 150
        Me.FNAME.Name = "FNAME"
        Me.FNAME.ReadOnly = True
        Me.FNAME.Width = 150
        '
        'LNAME
        '
        Me.LNAME.DataPropertyName = "LNAME"
        Me.LNAME.HeaderText = "Last Name"
        Me.LNAME.MinimumWidth = 150
        Me.LNAME.Name = "LNAME"
        Me.LNAME.ReadOnly = True
        Me.LNAME.Width = 150
        '
        'numSessions
        '
        Me.numSessions.DataPropertyName = "numSessions"
        Me.numSessions.HeaderText = "Sessions"
        Me.numSessions.MinimumWidth = 80
        Me.numSessions.Name = "numSessions"
        Me.numSessions.ReadOnly = True
        Me.numSessions.Width = 80
        '
        'numImpacts
        '
        Me.numImpacts.DataPropertyName = "numImpacts"
        Me.numImpacts.HeaderText = "Reports"
        Me.numImpacts.MinimumWidth = 60
        Me.numImpacts.Name = "numImpacts"
        Me.numImpacts.ReadOnly = True
        Me.numImpacts.Width = 60
        '
        'Upload
        '
        Me.Upload.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Upload.HeaderText = "Cloud Sync"
        Me.Upload.MinimumWidth = 100
        Me.Upload.Name = "Upload"
        Me.Upload.ReadOnly = True
        Me.Upload.Text = "Cloud Sync"
        Me.Upload.UseColumnTextForButtonValue = True
        '
        'UploadSVSessions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(701, 233)
        Me.Controls.Add(Me.progBar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnUploadAll)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dvSessionList)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(717, 272)
        Me.MinimumSize = New System.Drawing.Size(717, 272)
        Me.Name = "UploadSVSessions"
        Me.Text = "Sideline Viewer Cloud Sync"
        CType(Me.dvSessionList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dvSessionList As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnUploadAll As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents progBar As System.Windows.Forms.ProgressBar
    Friend WithEvents GID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PlayerNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FNAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LNAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents numSessions As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents numImpacts As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Upload As System.Windows.Forms.DataGridViewButtonColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCalibrate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCalibrate))
        Me.btnCalibrate = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.radioMntLocationOutside = New System.Windows.Forms.RadioButton()
        Me.radioMntLocationInside = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.radioButtonRight = New System.Windows.Forms.RadioButton()
        Me.radioButtonLeft = New System.Windows.Forms.RadioButton()
        Me.radioButtonFront = New System.Windows.Forms.RadioButton()
        Me.radioButtonRear = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.radioButtonProxOn = New System.Windows.Forms.RadioButton()
        Me.radioButtonProxOff = New System.Windows.Forms.RadioButton()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCalibrate
        '
        Me.btnCalibrate.Location = New System.Drawing.Point(366, 255)
        Me.btnCalibrate.Margin = New System.Windows.Forms.Padding(2)
        Me.btnCalibrate.Name = "btnCalibrate"
        Me.btnCalibrate.Size = New System.Drawing.Size(147, 32)
        Me.btnCalibrate.TabIndex = 15
        Me.btnCalibrate.Text = "Calibrate GFT"
        Me.btnCalibrate.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(340, 95)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = resources.GetString("Label2.Text")
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(9, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(344, 32)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "In order to utlize your device, it requires Calibration.  To calibrate your devic" & _
    "e, please follow the instructions below."
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(8, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(154, 20)
        Me.lblTitle.TabIndex = 9
        Me.lblTitle.Text = "Device Calibration"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(14, 186)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(133, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "USB Facing Direction:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(14, 161)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(150, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Inside or Outside Helmet:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.radioMntLocationOutside)
        Me.Panel1.Controls.Add(Me.radioMntLocationInside)
        Me.Panel1.Location = New System.Drawing.Point(169, 161)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(155, 23)
        Me.Panel1.TabIndex = 19
        '
        'radioMntLocationOutside
        '
        Me.radioMntLocationOutside.AutoSize = True
        Me.radioMntLocationOutside.Location = New System.Drawing.Point(87, 4)
        Me.radioMntLocationOutside.Margin = New System.Windows.Forms.Padding(2)
        Me.radioMntLocationOutside.Name = "radioMntLocationOutside"
        Me.radioMntLocationOutside.Size = New System.Drawing.Size(61, 17)
        Me.radioMntLocationOutside.TabIndex = 16
        Me.radioMntLocationOutside.Text = "Outside"
        Me.radioMntLocationOutside.UseVisualStyleBackColor = True
        '
        'radioMntLocationInside
        '
        Me.radioMntLocationInside.AutoSize = True
        Me.radioMntLocationInside.Checked = True
        Me.radioMntLocationInside.Location = New System.Drawing.Point(7, 3)
        Me.radioMntLocationInside.Margin = New System.Windows.Forms.Padding(2)
        Me.radioMntLocationInside.Name = "radioMntLocationInside"
        Me.radioMntLocationInside.Size = New System.Drawing.Size(53, 17)
        Me.radioMntLocationInside.TabIndex = 15
        Me.radioMntLocationInside.TabStop = True
        Me.radioMntLocationInside.Text = "Inside"
        Me.radioMntLocationInside.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.radioButtonRight)
        Me.Panel2.Controls.Add(Me.radioButtonLeft)
        Me.Panel2.Controls.Add(Me.radioButtonFront)
        Me.Panel2.Controls.Add(Me.radioButtonRear)
        Me.Panel2.Location = New System.Drawing.Point(169, 190)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(343, 29)
        Me.Panel2.TabIndex = 20
        '
        'radioButtonRight
        '
        Me.radioButtonRight.AutoSize = True
        Me.radioButtonRight.Location = New System.Drawing.Point(244, 5)
        Me.radioButtonRight.Name = "radioButtonRight"
        Me.radioButtonRight.Size = New System.Drawing.Size(50, 17)
        Me.radioButtonRight.TabIndex = 21
        Me.radioButtonRight.Text = "Right" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.radioButtonRight.UseVisualStyleBackColor = True
        '
        'radioButtonLeft
        '
        Me.radioButtonLeft.AutoSize = True
        Me.radioButtonLeft.Location = New System.Drawing.Point(170, 5)
        Me.radioButtonLeft.Name = "radioButtonLeft"
        Me.radioButtonLeft.Size = New System.Drawing.Size(43, 17)
        Me.radioButtonLeft.TabIndex = 20
        Me.radioButtonLeft.Text = "Left" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.radioButtonLeft.UseVisualStyleBackColor = True
        '
        'radioButtonFront
        '
        Me.radioButtonFront.AutoSize = True
        Me.radioButtonFront.Location = New System.Drawing.Point(87, 5)
        Me.radioButtonFront.Name = "radioButtonFront"
        Me.radioButtonFront.Size = New System.Drawing.Size(49, 17)
        Me.radioButtonFront.TabIndex = 17
        Me.radioButtonFront.Text = "Front"
        Me.radioButtonFront.UseVisualStyleBackColor = True
        '
        'radioButtonRear
        '
        Me.radioButtonRear.AutoSize = True
        Me.radioButtonRear.Checked = True
        Me.radioButtonRear.Location = New System.Drawing.Point(5, 3)
        Me.radioButtonRear.Name = "radioButtonRear"
        Me.radioButtonRear.Size = New System.Drawing.Size(48, 17)
        Me.radioButtonRear.TabIndex = 14
        Me.radioButtonRear.TabStop = True
        Me.radioButtonRear.Text = "Rear"
        Me.radioButtonRear.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Location = New System.Drawing.Point(14, 225)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Proximity Sensor:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.radioButtonProxOn)
        Me.Panel3.Controls.Add(Me.radioButtonProxOff)
        Me.Panel3.Location = New System.Drawing.Point(169, 225)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(155, 23)
        Me.Panel3.TabIndex = 22
        '
        'radioButtonProxOn
        '
        Me.radioButtonProxOn.AutoSize = True
        Me.radioButtonProxOn.Location = New System.Drawing.Point(87, 4)
        Me.radioButtonProxOn.Margin = New System.Windows.Forms.Padding(2)
        Me.radioButtonProxOn.Name = "radioButtonProxOn"
        Me.radioButtonProxOn.Size = New System.Drawing.Size(39, 17)
        Me.radioButtonProxOn.TabIndex = 16
        Me.radioButtonProxOn.Text = "On"
        Me.radioButtonProxOn.UseVisualStyleBackColor = True
        '
        'radioButtonProxOff
        '
        Me.radioButtonProxOff.AutoSize = True
        Me.radioButtonProxOff.Checked = True
        Me.radioButtonProxOff.Location = New System.Drawing.Point(7, 3)
        Me.radioButtonProxOff.Margin = New System.Windows.Forms.Padding(2)
        Me.radioButtonProxOff.Name = "radioButtonProxOff"
        Me.radioButtonProxOff.Size = New System.Drawing.Size(39, 17)
        Me.radioButtonProxOff.TabIndex = 15
        Me.radioButtonProxOff.TabStop = True
        Me.radioButtonProxOff.Text = "Off"
        Me.radioButtonProxOff.UseVisualStyleBackColor = True
        '
        'frmCalibrate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 298)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnCalibrate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCalibrate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calibrate Device"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCalibrate As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents radioMntLocationOutside As System.Windows.Forms.RadioButton
    Friend WithEvents radioMntLocationInside As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Private WithEvents radioButtonRight As System.Windows.Forms.RadioButton
    Private WithEvents radioButtonLeft As System.Windows.Forms.RadioButton
    Private WithEvents radioButtonFront As System.Windows.Forms.RadioButton
    Private WithEvents radioButtonRear As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents radioButtonProxOn As System.Windows.Forms.RadioButton
    Friend WithEvents radioButtonProxOff As System.Windows.Forms.RadioButton

End Class

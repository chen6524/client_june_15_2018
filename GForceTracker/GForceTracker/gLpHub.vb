﻿Imports System.Diagnostics
Imports System.Collections.Generic
Imports System.Text
Imports System.Windows.Forms
Imports System.Threading
Imports System.ComponentModel
Imports easyHID
'using CyUSB;



Public Class LP_HUB
    Friend Const MAX_DEVICE_CNT As Byte = 103 '63
    Friend Const HID_REPORT_ID As Byte = 0

    Friend Enum gLpCmd

        'hub is lp dongle host is PC
        MSG_LP_MODE_SHELF = &HE
        MSG_LP_SHOW_SHELF
        MSG_LP_DATA
        '10
        MSG_LP_HUB_GET_INFO
        '0x11
        MSG_LP_BIND_MODE
        ' 0x12
        MSG_LP_NODE_INFO
        '  0x13,
        MSG_LP_UPLOAD
        '   0x14,
        MSG_LP_RESET
        '    0x15,
        MSG_LP_SYNC
        '   0x16,
        MSG_LP_CLONE
        '  0x17
        MSG_LP_UNBIND
        '0x18
        MSG_LP_MY_OUTPUT
        ' this is for worker thread only
    End Enum

    Friend Enum LP_TX_CMD
        LP_TX_CMD_NONE = 0
        LP_TX_CMD_INIT
        LP_TX_CMD_UPDATE_NODE
        LP_TX_CMD_BIND
        LP_TX_CMD_UNBIND
        LP_TX_CMD_SYNC
        LP_TX_CMD_UPLOAD
        LP_TX_CMD_PWR_ON
        LP_TX_CMD_PWR_OFF

    End Enum

    Friend Const LP_CMD_OUT_BIT As Byte = &H40
    ' message types
    Public Enum gLpMsgType
        CHUNK_BINDING = 0
        ' no packet transmission
        CHUNK_SHELF_MODE_U = 2
        ' packet size will be  2
        CHUNK_SHELF_MODE_D = 5
        ' packet size will be  5
        CHUNK_POWER_OFF = 11
        ' packet size will be 11
        CHUNK_POWER_ON
        '      12,      // packet size will be 12
        CHUNK_IMMEDIATE
        '     13,      // packet size will be 13
        CHUNK_HIT_DATA
        '      14      // packet size will be 14
        CHUNK_UPLOAD_MORE
        '15
        CHUNK_UPLOAD_LAST
    End Enum

    Friend Class LP_DATA_PKT
        Friend nodeID As Byte
        Friend type As Byte
        Friend seq As Byte
        Friend len As Byte
        Friend isLast As Boolean
        Friend load As Byte() = New Byte(17) {}
    End Class

    Friend Enum RX_MSG_TYPE

        'hub is lp dongle host is PC
        RX_MSG_TYPE_SHELF
        RX_MSG_TYPE_SHOW_SHELF
        RX_MSG_TYPE_HUB_GET_INFO
        RX_MSG_TYPE_BIND_MODE
        RX_MSG_TYPE_NODE_INFO
        RX_MSG_TYPE_UPLOAD
        RX_MSG_TYPE_RESET
        RX_MSG_TYPE_SYNC
        RX_MSG_TYPE_CLONE
        RX_MSG_TYPE_DATA_BIDING
        RX_MSG_TYPE_DATA_SHELF_MODE_U
        RX_MSG_TYPE_DATA_SHELF_MODE_D
        RX_MSG_TYPE_DATA_POWER_OFF
        RX_MSG_TYPE_DATA_POWER_ON
        RX_MSG_TYPE_DATA_IMMEDIATE
        RX_MSG_TYPE_DATA_HIT_DATA
        RX_MSG_TYPE_DATA_UPLOAD_MORE
        RX_MSG_TYPE_DATA_UPLOAD_LAST
        RX_MSG_TYPE_STRING
    End Enum
    Public Shared lpDevices As New List(Of LP_DEVICE)()
    ' public SyncList<LP_DEVICE_VIEW> lpDevicesView = new SyncList<LP_DEVICE_VIEW>();

    Shared dgv As DataGridView
    Private Shared _MyLp As Hid

    Friend Shared curNodeNumber As Byte = 0
    Friend version As Byte

    Friend Shared sopIdx As Byte
    Friend Shared chBase As Byte
    Friend Shared chHop As Byte
    Friend Shared hubSeed As Byte
    Friend Shared maxNodes As Byte
    Friend Shared BCD As Byte
    Friend Shared chnMin As Byte
    Friend Shared chnMax As Byte
    Friend Shared curCh As Byte
    Friend Shared upldMod As Byte
    Private hFormWnd As IntPtr

    Public Sub New(Handle As IntPtr, vid As Integer, pid As Integer, dgvg As DataGridView)
        Dim lpd As LP_DEVICE
        dgv = dgvg

        For i As Byte = 0 To MAX_DEVICE_CNT - 1
            lpd = New LP_DEVICE()
            lpd.nodeID = i
            lpDevices.Add(lpd)
        Next

        'dgv.DataSource = lpDevicesView;  
        hFormWnd = Handle
        'MyHid = new Hid(hFormWnd, MY_VENDOR_ID, MY_PRODUCT_ID);
        MyLp = New Hid(hFormWnd, vid, pid)
        'MyLp.usbEvent += New Hid.usbEventsHandler(AddressOf usbEvent_receiver)
        AddHandler MyLp.usbEvent, New Hid.usbEventsHandler(AddressOf usbEvent_receiver)

        dgvLpInit()
        MyLp.findTargetDevice()
    End Sub
    Public Shared Property MyLp() As Hid
        Get
            Return _MyLp
        End Get
        Set(value As Hid)
            _MyLp = value
        End Set
    End Property
    Private Sub usbEvent_receiver(o As Object, e As EventArgs)

        'MessageHelper msg = new MessageHelper();
        Dim result As Integer = 0
        'First param can be null
        Dim hWnd As IntPtr = hFormWnd
        Try
            ' Check the status of the USB device and update the form accordingly
            If MyLp.isDeviceAttached Then
                ' Device is currently attached  

                result = gNativeMethod.sendWindowsMessage(CInt(hWnd), gNativeMethod.WM_USER + 4, 0, 0)
            Else
                ' Device is currently unattached
                ' Update the status label

                result = gNativeMethod.sendWindowsMessage(CInt(hWnd), gNativeMethod.WM_USER + 5, 0, 0)

            End If
        Catch ex As Exception

            gUtility.DisplayException("easyHID", ex)
        End Try
    End Sub
    Friend Shared Sub refreshBinding()
        'dgv.DataSource = null;
        'dgv.DataSource = lpDevicesView;  
    End Sub

    Friend Shared Function updateDevice(nodId As Byte, ByRef MID As Byte(), pwrMode As Byte) As Boolean

        For Each dev As LP_DEVICE In lpDevices
            If dev.nodeID = nodId Then
                dev.valid = True
                dev.nodePwrMode = pwrMode
                Array.Copy(MID, dev.MID, dev.MID.Length)


                Exit For
            End If
        Next
        dgvLpPopulateView()
        Return True
    End Function
    Public Shared Sub lpDevicesClear()
        For Each dev As LP_DEVICE In lpDevices
            dev.valid = False
        Next
    End Sub
    Friend Shared Sub dgvLpPopulateView()
        Dim i As Integer = 0
        dgv.Rows.Clear()

        For Each dev As LP_DEVICE In lpDevices
            If dev.valid = True Then

                Dim myDGVR As New DataGridViewRow()
                i = dgv.Rows.Add(myDGVR)
                Dim cellID As DataGridViewCell = New DataGridViewTextBoxCell()
                cellID.Value = dev.nodeID
                dgv.Rows(i).Cells(0) = cellID

                Dim cellName As DataGridViewCell = New DataGridViewTextBoxCell()
                cellName.Value = (New String(dev.name)).TrimEnd(New Char() {ChrW(0)})


                dgv.Rows(i).Cells(1) = cellName
                Dim cellPwr As DataGridViewCell = New DataGridViewImageCell()
                If dev.live Then
                    'DBRM-cellPwr.Value = Global.gForceTracker.Properties.Resources.status_online
                Else
                    'DBRM-cellPwr.Value = Global.gForceTracker.Properties.Resources.status_offline
                End If

                dgv.Rows(i).Cells(2) = cellPwr
                Dim cellAlarm As DataGridViewCell = New DataGridViewImageCell()
                If dev.alarm Then
                    'DBRM-cellAlarm.Value = Global.gForceTracker.Properties.Resources.user_red
                ElseIf dev.live Then
                    'DBRM-cellAlarm.Value = Global.gForceTracker.Properties.Resources.user_green
                Else
                    'DBRM-cellAlarm.Value = Global.gForceTracker.Properties.Resources.user_gray
                End If
                dgv.Rows(i).Cells(3) = cellAlarm

                Dim cellImpact As DataGridViewCell = New DataGridViewTextBoxCell()
                cellImpact.Value = dev.pn

                dgv.Rows(i).Cells(4) = cellImpact

                Dim cellUnBind As DataGridViewCell = New DataGridViewButtonCell()
                cellUnBind.Value = "Unbind"
                dgv.Rows(i).Cells(5) = cellUnBind
            End If
        Next
    End Sub

    Friend Function isNodeBinded(ByRef mid As Byte()) As Byte
        Dim nodeID As Byte = 0
        'DBRM-            For Each dev As LP_DEVICE In lpDevices
        'DBRM-                If dev.MID.SequenceEqual(mid) Then
        'DBRM-                nodeID = dev.nodeID
        'DBRM-                Exit For
        'DBRM-                End If
        'DBRM-            Next
        Return nodeID
    End Function

    Friend Shared Sub processImmediate(lpDataPkt As LP_DATA_PKT)
        For Each dev As LP_DEVICE In lpDevices
            If dev.nodeID = lpDataPkt.nodeID Then
                dev.alarm = True

                dev.gForce = lpDataPkt.load(1)
            End If
        Next
        dgvLpPopulateView()
    End Sub
    Friend Shared Sub processHitData(lpDataPkt As LP_DATA_PKT)
        For Each dev As LP_DEVICE In lpDevices
            If dev.nodeID = lpDataPkt.nodeID Then
                dev.alarm = (lpDataPkt.load(0) = 1)

                dev.gForce = CInt(Math.Truncate(Math.Sqrt(lpDataPkt.load(1) + lpDataPkt.load(2) * 256)))
            End If
        Next
        dgvLpPopulateView()
    End Sub
    ''' <summary>
    ''' processing uploaded data
    ''' </summary>
    ''' <param name="lpDataPkt"></param>
    Friend Shared Sub processUploadData(lpDataPkt As LP_DATA_PKT)
        For Each dev As LP_DEVICE In lpDevices
            If dev.nodeID = lpDataPkt.nodeID Then



            End If
        Next

    End Sub
    Friend Shared Sub liveChanged(lpDataPkt As LP_DATA_PKT)

        For Each dev As LP_DEVICE In lpDevices
            If dev.nodeID = lpDataPkt.nodeID Then
                If lpDataPkt.type = CByte(gLpMsgType.CHUNK_POWER_ON) Then
                    If (lpDataPkt.seq = 0) AndAlso (lpDataPkt.len = 0) Then
                        Exit For
                    Else
                        If lpDataPkt.seq = 0 Then

                            dev.live = True
                            If lpDataPkt.len - 1 > 0 Then
                                Array.Copy(lpDataPkt.load, 6, dev.name, 0, 5)
                            End If
                            Exit For
                        ElseIf lpDataPkt.seq = 1 Then

                            Array.Copy(lpDataPkt.load, 0, dev.name, 5, 11)
                        ElseIf lpDataPkt.seq = 2 Then
                            Array.Copy(lpDataPkt.load, 0, dev.name, 16, 4)
                            dev.pn = lpDataPkt.load(4)
                            For i As Integer = 0 To dev.name.Length - 1
                                'DBRM-                                    If dev.name(i) = 0 Then
                                'DBRM-
                                'DBRM-Array.Clear(dev.name, i, dev.name.Length - i)
                                'DBRM-
                                'DBRM-End If
                            Next
                        End If
                    End If
                Else
                    ' power off

                    dev.live = False
                End If
            End If
        Next
        dgvLpPopulateView()
    End Sub

    Public Shared hubSyncState As Byte = MAX_DEVICE_CNT
    Shared gLpTxCmd As Byte = CByte(LP_TX_CMD.LP_TX_CMD_INIT)

#Region "lpRx"

    Friend Const LP_NODES_INFO_PER_GROUP As Byte = 7
    Friend Const LP_MID_LEN As Byte = 4
    Private Shared Sub gLpMsgNodeInfoProcess(sender As BackgroundWorker, ByRef inputReport As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim md As LP_DEVICE() = New LP_DEVICE(LP_NODES_INFO_PER_GROUP - 1) {}
        For k As Integer = 0 To LP_NODES_INFO_PER_GROUP - 1
            md(k) = New LP_DEVICE()
        Next

        ' byte nodePwrMode;
        ' byte[] MID = new byte[LP_MID_LEN];
        Dim nodeID As Byte
        Dim omsg As String = "nData"

        Dim i As Byte, j As Byte

        If inputReport(2) = 0 Then
            omsg = "NodeInfo" & System.DateTime.Now
        ElseIf inputReport(2) = 8 Then
            omsg = "Last" & System.DateTime.Now & " " & vbCr & vbLf
        End If

        i = 3
        j = 0
        While i < LP_NODES_INFO_PER_GROUP * 8 + 3
            If inputReport(i + 1) = &H0 Then
                'bypass null init node info space
                Continue While
            End If
            nodeID = inputReport(i + 1)
            md(j).nodePwrMode = inputReport(i + 0)
            md(j).nodeID = nodeID
            md(j).MID(0) = inputReport(i + 2)
            md(j).MID(1) = inputReport(i + 3)
            md(j).MID(2) = inputReport(i + 4)

            md(j).MID(3) = inputReport(i + 5)
            i += 8
            j += 1
        End While
        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_NODE_INFO), md)
    End Sub

    Private Shared Sub gLpMsgHubInfoProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())

        Dim os As String = ""
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        os += String.Format("sopIdx={0:X2}", inputReportBuffer(1))
        os += String.Format("chBase={0:X2}", inputReportBuffer(2))
        os += String.Format("chHop={0:X2}", inputReportBuffer(3))
        os += String.Format("hubSeed={0:X2}", inputReportBuffer(4)) & String.Format("chBase={0:X2}", inputReportBuffer(5))
        os += String.Format("maxNodes={0:X2}", inputReportBuffer(9))
        os += String.Format("BCD={0:X2}", inputReportBuffer(10))
        os += vbCr & vbLf
        os += String.Format("chnMin={0:X2}", inputReportBuffer(11))
        os += String.Format("chnMax={0:X2}", inputReportBuffer(12))
        os += String.Format("curCh={0:X2}", inputReportBuffer(13))

        os += String.Format("upldMod={0:X2}", inputReportBuffer(14))
        'os += inputReportBuffer[18];
        os += vbCr & vbLf
        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_STRING), os)

        sopIdx = inputReportBuffer(1)
        chBase = inputReportBuffer(2)
        chHop = inputReportBuffer(3)
        hubSeed = inputReportBuffer(4)
        chBase = inputReportBuffer(5)
        maxNodes = inputReportBuffer(9)
        BCD = inputReportBuffer(10)
        chnMin = inputReportBuffer(11)
        chnMax = inputReportBuffer(12)
        curCh = inputReportBuffer(13)
        upldMod = inputReportBuffer(14)

    End Sub

    Private Shared Sub gLpMsgPowerProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)

        Dim newPkt As LP_DATA_PKT = glpMsgDataCopy(inputReportBuffer)
        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_DATA_POWER_OFF), newPkt)

    End Sub


    Private Shared Function glpMsgDataCopy(ByRef inputReportBuffer As [Byte]()) As LP_DATA_PKT
        Dim newPkt As New LP_DATA_PKT()
        newPkt.len = CByte(inputReportBuffer(5) And &HF)
        newPkt.seq = CByte((inputReportBuffer(5) And &HF0) \ 16)
        newPkt.nodeID = inputReportBuffer(2)
        newPkt.type = inputReportBuffer(4)
        Array.Copy(inputReportBuffer, 6, newPkt.load, 0, 13)
        Return newPkt
    End Function

    Private Shared Sub gLpMsgImmeDataProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim newPkt As LP_DATA_PKT = glpMsgDataCopy(inputReportBuffer)
        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_DATA_IMMEDIATE), newPkt)
    End Sub

    Private Shared Sub gLpMsgHitDataProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim newPkt As LP_DATA_PKT = glpMsgDataCopy(inputReportBuffer)
        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_DATA_HIT_DATA), newPkt)
    End Sub

    Private Shared Sub gLpMsgUploadDataProcess(sender As BackgroundWorker, type As Byte, ByRef inputReportBuffer As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim newPkt As LP_DATA_PKT = glpMsgDataCopy(inputReportBuffer)
        If type = CByte(gLpMsgType.CHUNK_UPLOAD_LAST) Then
            newPkt.isLast = True
        Else
            newPkt.isLast = False
        End If

        bw.ReportProgress(CByte(RX_MSG_TYPE.RX_MSG_TYPE_DATA_UPLOAD_MORE), newPkt)
    End Sub

    Private Shared Sub gLpMsgShelfUProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim os As String = ""

        os += "Msg shelf U data"
        os += vbCr & vbLf
        bw.ReportProgress(CInt(gLpMsgType.CHUNK_SHELF_MODE_D), os)
    End Sub

    Private Shared Sub gLpMsgShelfDProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim os As String = ""

        os += "Msg shelf D data"
        os += vbCr & vbLf
        bw.ReportProgress(CInt(gLpMsgType.CHUNK_SHELF_MODE_D), os)
    End Sub

    Private Shared Sub gLpMsgBindProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim os As String = ""

        os += "Msg bind data"
        os += vbCr & vbLf

        bw.ReportProgress(CInt(gLpCmd.MSG_LP_MY_OUTPUT), os)
    End Sub

    Private Shared Sub gLpMsgNodeDataProcess(sender As BackgroundWorker, ByRef pBufferIn As Byte())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        '        Dim i As Integer, j As Integer, nEditLen As Integer
        '        Dim node As Integer, plen As Integer
        Dim time As DateTime = DateTime.Now
        Select Case pBufferIn(4)
            Case CByte(gLpMsgType.CHUNK_POWER_ON)
                gLpMsgPowerProcess(bw, pBufferIn)
                Exit Select
            Case CByte(gLpMsgType.CHUNK_POWER_OFF)
                gLpMsgPowerProcess(bw, pBufferIn)
                Exit Select
            Case CByte(gLpMsgType.CHUNK_IMMEDIATE)
                gLpMsgImmeDataProcess(bw, pBufferIn)
                Exit Select
            Case CByte(gLpMsgType.CHUNK_HIT_DATA)
                gLpMsgHitDataProcess(bw, pBufferIn)
                Exit Select
            Case CByte(gLpMsgType.CHUNK_UPLOAD_MORE), CByte(gLpMsgType.CHUNK_UPLOAD_LAST)
                gLpMsgUploadDataProcess(bw, pBufferIn(4), pBufferIn)
                Exit Select
            Case CByte(gLpMsgType.CHUNK_SHELF_MODE_D)
                gLpMsgShelfDProcess(bw, pBufferIn)
                Exit Select
            Case CByte(gLpMsgType.CHUNK_SHELF_MODE_U)
                gLpMsgShelfUProcess(bw, pBufferIn)
                Exit Select
            Case CByte(gLpMsgType.CHUNK_BINDING)
                gLpMsgBindProcess(bw, pBufferIn)
                Exit Select
        End Select


    End Sub

    Private Shared Sub gLpHubBindModeProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        ' 0: 0x12
        ' 1: ret (0/1/2/3/4/5)
        ' 2: node
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim node As Byte
        Dim os As String = ""
        node = inputReportBuffer(3)

        os += "Bind -"
        If inputReportBuffer(2) = &HFF Then
            os += "Error: Nok But..." & vbCr & vbLf
        ElseIf inputReportBuffer(2) = 2 Then
            os += "Error: Already in Bind mode..." & vbCr & vbLf
        ElseIf inputReportBuffer(2) = 0 Then
            os += "Ok: Entering Bind mode..." & vbCr & vbLf
        ElseIf inputReportBuffer(2) = 3 Then
            os += "Error: Upload mode active..." & vbCr & vbLf
        End If
        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_STRING), os)
        'textBoxViewer.Text += os;
    End Sub
    Private Shared Sub gLpHubUnBindModeProcess(sender As BackgroundWorker, ByRef inputReportBuffer As [Byte]())
        ' 0: 0x12
        ' 1: ret (0/1/2/3/4/5)
        ' 2: node
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim node As Byte
        Dim os As String = ""
        node = inputReportBuffer(3)

        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_STRING), os)
        'textBoxViewer.Text += os;
    End Sub
    Private Shared Sub gLpHubModeShelfProcess(sender As BackgroundWorker, ByRef inputReportBuffer As Byte())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        'if(inputReportBuffer[2] == 0)
        Dim myRead As Byte
        myRead = inputReportBuffer(2)
        hubSyncState = inputReportBuffer(2)

        If inputReportBuffer(2) <> 0 Then
            bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_SHELF), myRead)
        End If

    End Sub

    Private Shared Sub gLpMsgUploadRequestProcess(sender As BackgroundWorker, ByRef inputReportBuffer As Byte())
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        bw.ReportProgress(CInt(RX_MSG_TYPE.RX_MSG_TYPE_STRING), "Upload enabled" & vbCr & vbLf)
    End Sub



    Public Shared Sub gLpProcessRx(sender As Object, e As DoWorkEventArgs)
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)

        Dim myInput As Byte() = New Byte(64) {}
        Dim nread As Integer = 0
        'CyHidDevice lp;
        Try
            'if (Thread.CurrentThread.Name == null)
            Thread.CurrentThread.Name = "gLpProcessRx"

            While Not bw.CancellationPending

                'lp = MyLp;

                ' while (!bw.CancellationPending && !gUsbComm.gftFinishedUpload)
                '    Thread.Sleep(100);
                If MyLp Is Nothing Then
                    bw.CancelAsync()
                    Continue While
                End If
                If MyLp.inputReports(myInput, nread) Then

                    'Array.Copy(MyLp.Inputs.DataBuf, myInput, MyLp.Inputs.RptByteLen);
                    If True Then
                        Select Case myInput(1)

                            Case CByte(gLpCmd.MSG_LP_MODE_SHELF)
                                gLpHubModeShelfProcess(bw, myInput)
                                Exit Select

                            Case CByte(gLpCmd.MSG_LP_DATA)
                                gLpMsgNodeDataProcess(bw, myInput)
                                Exit Select

                            Case CByte(gLpCmd.MSG_LP_HUB_GET_INFO)
                                gLpMsgHubInfoProcess(bw, myInput)
                                Exit Select

                            Case CByte(gLpCmd.MSG_LP_BIND_MODE)
                                gLpHubBindModeProcess(bw, myInput)
                                Exit Select

                            Case CByte(gLpCmd.MSG_LP_UPLOAD)
                                gLpMsgUploadRequestProcess(bw, myInput)
                                Exit Select

                            Case CByte(gLpCmd.MSG_LP_NODE_INFO)
                                gLpMsgNodeInfoProcess(bw, myInput)
                                Exit Select
                            Case CByte(gLpCmd.MSG_LP_UNBIND)
                                gLpHubUnBindModeProcess(bw, myInput)
                                Exit Select
                            Case Else
                                Exit Select
                        End Select
                    End If
                    Array.Clear(myInput, 0, myInput.Length)
                Else
                    Thread.Sleep(100)

                End If
            End While
        Catch ex As Exception
            ' gUtility.DisplayException("Lp Rx", ex);
            'this exception come from the readInput I haven't figure out how to cancel current reading yet


        End Try
    End Sub
#End Region
#Region "lpTx"
    'private void lpWaitDongleReady()
    '{
    '    // byte[] inputReportBuffer = new Byte[64 + 1];
    '    int nread = 0;
    '    do
    '    {
    '        MyLp.ReadInput();//(ref inputReportBuffer, ref nread);
    '    } while (nread != 0);

    '}
    Public Shared Function lpSendCmd(cmd As Byte, subCmd As Byte) As Boolean
        Dim result As Boolean = False
        Dim nread As Integer = 0
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}
            outputReportBuffer(0) = HID_REPORT_ID
            outputReportBuffer(1) = cmd
            outputReportBuffer(2) = subCmd



            result = MyLp.OutputReports(outputReportBuffer)
        Catch ex As Exception

            gUtility.DisplayException("Send Command", ex)
        End Try
        Return result

    End Function


    Public Shared Sub lpReqHubInfo()
        lpSendCmd(CByte(gLpCmd.MSG_LP_HUB_GET_INFO) Or LP_CMD_OUT_BIT, 0)

    End Sub

    Public Shared Sub lpReqBind()
        'int nread =0;
        'byte[] inputReportBuffer = new Byte[64 + 1];
        'lpSendCmd((byte)gLpCmd.MSG_LP_BIND_MODE | LP_CMD_OUT_BIT, 0);
        gLpTxCmd = CByte(LP_TX_CMD.LP_TX_CMD_BIND)
        'MyLp.inputReports(ref inputReportBuffer, ref nread);


    End Sub
    Public Shared Sub lpReqUnBind(nn As Byte)
        'int nread =0;
        'byte[] inputReportBuffer = new Byte[64 + 1];
        'lpSendCmd((byte)gLpCmd.MSG_LP_BIND_MODE | LP_CMD_OUT_BIT, 0);
        lpDevicesClear()
        dgvLpPopulateView()
        curNodeNumber = nn
        gLpTxCmd = CByte(LP_TX_CMD.LP_TX_CMD_UNBIND)
        'MyLp.inputReports(ref inputReportBuffer, ref nread);


    End Sub
    Public Shared Sub lpReqUpload()
        'int nread =0;
        'byte[] inputReportBuffer = new Byte[64 + 1];
        lpSendCmd(CByte(gLpCmd.MSG_LP_UPLOAD) Or LP_CMD_OUT_BIT, 0)
        ' MyLp.inputReports(ref inputReportBuffer, ref nread);

    End Sub

    Private Shared Sub lpReqNodeInfo()
        'int nread =0;
        'byte[] inputReportBuffer = new Byte[64 + 1];
        lpSendCmd(CByte(gLpCmd.MSG_LP_NODE_INFO) Or LP_CMD_OUT_BIT, 0)
        ' MyLp.inputReports(ref inputReportBuffer, ref nread);

    End Sub
    Friend Enum LP_TX_STATE
        LP_TX_STAT_INIT = 0
        LP_TX_STAT_IDLE
        LP_TX_STAT_BIND
        LP_TX_STAT_UNBIND
        LP_TX_STAT_SYNC
        LP_TX_STAT_UPLOAD
        LP_TX_STAT_HUB_INFO
        LP_TX_STAT_HUB_INFO_WAIT
        LP_TX_STAT_NODE_INFO
    End Enum
    Public Shared Sub gLpProcessTx(sender As Object, e As DoWorkEventArgs)
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        'Hid lp;
        Dim lpDongleStat As Byte = 0
        Dim stay As Boolean = True
        Dim ts As TimeSpan
        Dim oldSyncState As Byte = CByte(LP_TX_STATE.LP_TX_STAT_INIT)
        ' byte[] outputReportBuffer = new Byte[64 + 1];
        Try
            If Thread.CurrentThread.Name Is Nothing Then
                Thread.CurrentThread.Name = "gLpProcessTx"
            End If
            Dim stopWatch As New Stopwatch()
            stopWatch.Start()
            While stay AndAlso Not bw.CancellationPending
                'lp = MyLp;
                If MyLp Is Nothing Then
                    Exit While
                End If

                'while (!bw.CancellationPending && !gUsbComm.gftFinishedUpload)
                '    Thread.Sleep(100);
                Select Case lpDongleStat
                    Case CByte(LP_TX_STATE.LP_TX_STAT_INIT)
                        lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_HUB_INFO)
                        Exit Select
                        If hubSyncState = 0 Then
                            stopWatch.[Stop]()
                            lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_HUB_INFO)
                        ElseIf oldSyncState = hubSyncState Then

                            stopWatch.[Stop]()
                            ts = stopWatch.Elapsed
                            If ts.Seconds > 3 Then

                                lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_HUB_INFO)
                            Else
                                stopWatch.Start()
                                Thread.Sleep(50)

                            End If
                        Else
                            oldSyncState = hubSyncState
                            stopWatch.Reset()

                            stopWatch.Start()
                        End If
                        Exit Select

                    Case CByte(LP_TX_STATE.LP_TX_STAT_HUB_INFO)
                        lpReqHubInfo()
                        lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_HUB_INFO_WAIT)
                        stopWatch.Reset()
                        stopWatch.Start()
                        Exit Select
                    Case CByte(LP_TX_STATE.LP_TX_STAT_HUB_INFO_WAIT)
                        stopWatch.[Stop]()
                        ts = stopWatch.Elapsed
                        If ts.Seconds > 2 Then
                            'stopWatch.Stop();
                            lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_NODE_INFO)
                        Else
                            stopWatch.Start()
                        End If
                        Exit Select
                    Case CByte(LP_TX_STATE.LP_TX_STAT_NODE_INFO)

                        lpReqNodeInfo()
                        lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_IDLE)
                        Exit Select
                    Case CByte(LP_TX_STATE.LP_TX_STAT_IDLE)
                        If gLpTxCmd = CByte(LP_TX_CMD.LP_TX_CMD_BIND) Then
                            lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_BIND)
                            lpSendCmd(CByte(gLpCmd.MSG_LP_BIND_MODE) Or LP_CMD_OUT_BIT, 0)
                            stopWatch.Reset()
                            stopWatch.Start()
                            gLpTxCmd = CByte(LP_TX_CMD.LP_TX_CMD_NONE)
                        ElseIf gLpTxCmd = CByte(LP_TX_CMD.LP_TX_CMD_UNBIND) Then
                            lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_UNBIND)
                            lpSendCmd(CByte(gLpCmd.MSG_LP_UNBIND) Or LP_CMD_OUT_BIT, curNodeNumber)
                            stopWatch.Reset()
                            stopWatch.Start()
                            gLpTxCmd = CByte(LP_TX_CMD.LP_TX_CMD_NONE)
                        End If
                        Thread.Sleep(100)
                        Exit Select
                    Case CByte(LP_TX_STATE.LP_TX_STAT_BIND)
                        stopWatch.[Stop]()
                        ts = stopWatch.Elapsed
                        If ts.Seconds > 10 Then
                            'seconds
                            stopWatch.[Stop]()
                            lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_NODE_INFO)
                        Else
                            stopWatch.Start()
                            Thread.Sleep(100)
                        End If
                        Exit Select
                    Case CByte(LP_TX_STATE.LP_TX_STAT_UNBIND)
                        stopWatch.[Stop]()
                        ts = stopWatch.Elapsed
                        If ts.Seconds > 1 Then
                            'seconds
                            stopWatch.[Stop]()
                            lpDongleStat = CByte(LP_TX_STATE.LP_TX_STAT_NODE_INFO)
                        Else
                            stopWatch.Start()
                            Thread.Sleep(100)
                        End If
                        Exit Select
                    Case Else
                        Exit Select
                End Select
            End While
        Catch ex As Exception

            gUtility.DisplayException("Lp Tx", ex)
        End Try

    End Sub


#End Region
#Region "lpView"

    Private Structure myrow
        Friend nodId As String
        Friend name As String
        Friend count As String
        Friend image As System.Drawing.Image


    End Structure

    Friend Shared Sub dgvLpInit()

        dgv.ColumnCount = 6
        dgv.Columns(0).Name = "#"
        dgv.Columns(1).Name = "Player Name"
        dgv.Columns(2).Name = "Power"
        dgv.Columns(3).Name = "Status"
        dgv.Columns(4).Name = "Player Number"
        dgv.Columns(5).Name = "Unbind"
        dgv.AllowUserToDeleteRows = False
        dgv.AllowUserToAddRows = False
        dgv.[ReadOnly] = True
        dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
        dgv.RowTemplate.Height = 30


    End Sub

#End Region

End Class

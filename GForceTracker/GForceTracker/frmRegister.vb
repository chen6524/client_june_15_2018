﻿Imports gForceTracker.frmNotify
Imports System.Threading

Public Class frmRegister

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        System.Diagnostics.Process.Start("http://gft.gforcetracker.com/register?gid=" & currentGftProfile.sSerial & "&type=" & currentGftProfile.hwType)
        EraseFlash()
        Me.Close()
    End Sub

    Private Sub btnExisting_Click(sender As Object, e As EventArgs) Handles btnExisting.Click
        System.Diagnostics.Process.Start("http://gft.gforcetracker.com/login?new=" & currentGftProfile.sSerial & "&type=" & currentGftProfile.hwType)
        EraseFlash()
        Me.Close()
    End Sub
    Private Sub EraseFlash()
        Thread.Sleep(1000)
        Dim comResult = gUsbComm.sendCmd(CByte(gUsbComm.mhidCmd.MHID_CMD_ERASE_ACC_DATA), 0)
        If comResult = "passed" Then
            Thread.Sleep(15000) 'Device appears to require 10 seconds to become ready after erase command + Add another 5 seconds to be safe
        End If
    End Sub
End Class
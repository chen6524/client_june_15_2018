﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegister
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRegister))
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnExisting = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblHeader
        '
        Me.lblHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.Location = New System.Drawing.Point(12, 9)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(342, 25)
        Me.lblHeader.TabIndex = 6
        Me.lblHeader.Text = "Registration Required"
        '
        'lblMsg
        '
        Me.lblMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg.Location = New System.Drawing.Point(13, 34)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(391, 39)
        Me.lblMsg.TabIndex = 5
        Me.lblMsg.Text = "Before you can begin using your device, you must first register it to an existing" & _
    " account or create a new account."
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(11, 84)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(179, 23)
        Me.btnNew.TabIndex = 7
        Me.btnNew.Text = "&New Account"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnExisting
        '
        Me.btnExisting.Location = New System.Drawing.Point(215, 84)
        Me.btnExisting.Name = "btnExisting"
        Me.btnExisting.Size = New System.Drawing.Size(179, 23)
        Me.btnExisting.TabIndex = 8
        Me.btnExisting.Text = "&Add to Existing Account"
        Me.btnExisting.UseVisualStyleBackColor = True
        '
        'frmRegister
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(405, 120)
        Me.Controls.Add(Me.btnExisting)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.lblHeader)
        Me.Controls.Add(Me.lblMsg)
        Me.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRegister"
        Me.Text = "Registration Required"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents lblMsg As System.Windows.Forms.Label
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnExisting As System.Windows.Forms.Button
End Class

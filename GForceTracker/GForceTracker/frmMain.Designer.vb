﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tslGFT = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tslInternet = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tslHub = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.bwCheckBattery = New System.ComponentModel.BackgroundWorker()
        Me.timerStatus = New System.Windows.Forms.Timer(Me.components)
        Me.timerBatteryImage = New System.Windows.Forms.Timer(Me.components)
        Me.timerBatteryMon = New System.Windows.Forms.Timer(Me.components)
        Me.progBar = New System.Windows.Forms.ProgressBar()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.picBattery = New System.Windows.Forms.PictureBox()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBattery, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.Top
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tslGFT, Me.tslInternet, Me.tslHub})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 0)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(406, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "footerStatusStrip"
        Me.StatusStrip1.Visible = False
        '
        'tslGFT
        '
        Me.tslGFT.AutoSize = False
        Me.tslGFT.Name = "tslGFT"
        Me.tslGFT.Size = New System.Drawing.Size(135, 17)
        Me.tslGFT.Text = "USB"
        '
        'tslInternet
        '
        Me.tslInternet.AutoSize = False
        Me.tslInternet.Name = "tslInternet"
        Me.tslInternet.Size = New System.Drawing.Size(135, 17)
        Me.tslInternet.Text = "Internet"
        '
        'tslHub
        '
        Me.tslHub.AutoSize = False
        Me.tslHub.Name = "tslHub"
        Me.tslHub.Size = New System.Drawing.Size(135, 17)
        Me.tslHub.Text = "HUB"
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lblStatus.Location = New System.Drawing.Point(0, 69)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(406, 30)
        Me.lblStatus.TabIndex = 5
        Me.lblStatus.Text = "Please Connect Your Device to Sync"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'bwCheckBattery
        '
        Me.bwCheckBattery.WorkerReportsProgress = True
        '
        'timerStatus
        '
        Me.timerStatus.Interval = 500
        '
        'timerBatteryImage
        '
        Me.timerBatteryImage.Interval = 500
        '
        'timerBatteryMon
        '
        Me.timerBatteryMon.Interval = 1000
        '
        'progBar
        '
        Me.progBar.Location = New System.Drawing.Point(0, 96)
        Me.progBar.Name = "progBar"
        Me.progBar.Size = New System.Drawing.Size(406, 23)
        Me.progBar.Step = 1
        Me.progBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progBar.TabIndex = 7
        '
        'picLogo
        '
        Me.picLogo.Image = Global.gForceTracker.My.Resources.uiResources.header_logo
        Me.picLogo.Location = New System.Drawing.Point(100, 25)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(212, 41)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picLogo.TabIndex = 4
        Me.picLogo.TabStop = False
        '
        'picBattery
        '
        Me.picBattery.Location = New System.Drawing.Point(371, 4)
        Me.picBattery.Name = "picBattery"
        Me.picBattery.Size = New System.Drawing.Size(32, 32)
        Me.picBattery.TabIndex = 2
        Me.picBattery.TabStop = False
        Me.picBattery.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(406, 119)
        Me.Controls.Add(Me.progBar)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.picLogo)
        Me.Controls.Add(Me.picBattery)
        Me.Controls.Add(Me.StatusStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(422, 158)
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "GForce Tracker"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBattery, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tslGFT As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslHub As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslInternet As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents picBattery As System.Windows.Forms.PictureBox
    Friend WithEvents picLogo As System.Windows.Forms.PictureBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents bwCheckBattery As System.ComponentModel.BackgroundWorker
    Friend WithEvents timerStatus As System.Windows.Forms.Timer
    Friend WithEvents timerBatteryImage As System.Windows.Forms.Timer
    Friend WithEvents timerBatteryMon As System.Windows.Forms.Timer
    Friend WithEvents progBar As System.Windows.Forms.ProgressBar

End Class

﻿Public Class gftMessage
    Private dialog As gftMessageBox

    Public Sub New()

    End Sub
    'MessageBox.Show("New Settings have been detected for your device.  Press OK to update your device.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    Public Function MessageBox(text As String, productName As String, buttons As MessageBoxButtons, icon As MessageBoxIcon) As DialogResult
        If Not dialog Is Nothing Then
            dialog.Close()
            dialog.Dispose()
        End If
        dialog = New gftMessageBox(text, productName, buttons, icon)

        ' get our current "TopMost" value (ours will always be false though)
        Dim top As Boolean = frmMain.TopMost
        ' make our form jump to the top of everything
        frmMain.TopMost = True
        ' set it back to whatever it was
        frmMain.TopMost = top
        Return dialog.ShowDialog()
        'dialog.Show()
    End Function

    Delegate Sub closeCallback()
    Public Sub close()
        If Not dialog Is Nothing Then
            If dialog.InvokeRequired Then
                Dim d As New closeCallback(AddressOf close)
                dialog.Invoke(d, New Object() {})
            Else
                dialog.Close()
                dialog.Dispose()
            End If
        End If
    End Sub

End Class

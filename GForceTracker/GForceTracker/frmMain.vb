﻿Imports gForceTracker.gftProfile
Imports gForceTracker.gftRest
Imports gForceTracker.gUsbComm
Imports gForceTracker.frmNotify

Imports System.Timers.Timer
Imports System.ComponentModel
Imports System.Threading
Imports System.Runtime.InteropServices

Public Class frmMain

    Public statusText As String = "GFT Disconnected"

    Public Shared backgroundWorkerLpRx As BackgroundWorker


    Public Shared showMainWindow As Boolean = False

    Public Shared sCloudSoftwareVer As String
    Public Shared sCloudFirmwareVer As String

    Public Shared iSessionCnt As Integer
    Public Shared iImpactCnt As Integer
    Public Shared isUploading As Boolean = False

    Public Shared timerStatusFirstEvent As Boolean = True

    Private batImage As Integer = 0
    Public Shared gftBattVol As Integer = 0

    Public Sub SessionIsEnding(ByVal sender As Object, ByVal e As Microsoft.Win32.SessionEndingEventArgs)
        frmNotify.systemShutdown = True
        Select Case e.Reason
            Case Microsoft.Win32.SessionEndReasons.Logoff
            Case Microsoft.Win32.SessionEndReasons.SystemShutdown
        End Select
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If frmNotify.systemShutdown = False Then
            If Me.WindowState = FormWindowState.Maximized Or Me.WindowState = FormWindowState.Normal Then
                Me.WindowState = FormWindowState.Minimized
                Me.ShowInTaskbar = False
                Me.Visible = False
            End If
            e.Cancel = True
        End If
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "GForceTracker v" & Application.ProductVersion
        Me.tslGFT.Text = "USB OFF"
        Me.tslGFT.BackColor = System.Drawing.Color.LightGray
        Me.tslHub.Text = "WIRELESS OFF"
        Me.tslHub.BackColor = System.Drawing.Color.LightGray


        Me.progBar.Minimum = 0
        Me.progBar.Maximum = 100
        UpdateProgressBar(0)


    End Sub
    Public Sub UpdateProgressBar(ByVal Value As Integer)
        Me.progBar.Value = Value
        Me.progBar.Refresh()
        SetProgressBarText()
    End Sub
    Public Sub SetProgressBarText()
        Dim Location As ProgressBarTextLocation = ProgressBarTextLocation.Centered
        Using gr As Graphics = progBar.CreateGraphics()
            progBar.Refresh()
            gr.DrawString(Me.statusText, New Font(Font.Name, 9.75, FontStyle.Bold), New SolidBrush(Color.Black), New PointF(If(Location = ProgressBarTextLocation.Left, 5, Me.progBar.Width / 2 - (gr.MeasureString(Me.statusText, New Font(Font.Name, 10, FontStyle.Regular)).Width / 2.0F)), progBar.Height / 2 - (gr.MeasureString(Me.statusText, New Font(Font.Name, 9.75, FontStyle.Bold)).Height / 2.0F)))
        End Using
    End Sub
    Public Enum ProgressBarTextLocation
        Left
        Centered
    End Enum

    Protected Overrides Sub WndProc(ByRef m As Message)
        If Not WirelessViewerOpen Then ' Don't do anything if the wireless viewer is open
            Select Case m.Msg
                Case &H11 'WM_QUERYENDSESSION
                    ' session ending..
                    frmNotify.systemShutdown = True
                    Exit Select
            End Select
            MyBase.WndProc(m)

        End If
    End Sub

    Public Sub hideMe(Optional now As Boolean = False)
        showMainWindow = False
        Dim i As Integer
        If (Not now) Then
            For i = 1 To 10
                Threading.Thread.Sleep(500) : Application.DoEvents()
            Next
        End If
        If showMainWindow = False And Visible Then 'WindowState <> FormWindowState.Minimized
            ShowInTaskbar = False
            'WindowState = FormWindowState.Minimized
            Hide()
        End If
    End Sub

    Public Shared Sub ShowMe()
        showMainWindow = True
        'Dim currentWindowState As FormWindowState = frmMain.WindowState
        'If frmMain.WindowState <> FormWindowState.Normal Then
        frmMain.WindowState = FormWindowState.Normal
        'End If
        frmMain.Show()
        ' get our current "TopMost" value (ours will always be false though)
        Dim top As Boolean = frmMain.TopMost
        ' make our form jump to the top of everything
        frmMain.TopMost = True
        ' set it back to whatever it was
        frmMain.TopMost = False 'top
    End Sub


#Region "bwUploadData"

    <DllImport("user32.dll", SetLastError:=True)> _
    Private Shared Function BringWindowToTop(ByVal hwnd As IntPtr) As Boolean
    End Function

    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal _
    lpClassName As String, ByVal lpWindowName As String) As Long
    Private Declare Function ShowWindow Lib "user32" (ByVal hwnd As IntPtr, ByVal _
    nCmdShow As Long) As Long
    Private Const SW_SHOWMAXIMIZED = 3


    Private Sub backgroundWorkerLpRx_DoWork(sender As Object, e As DoWorkEventArgs)
        LP_HUB.gLpProcessRx(sender, e)
    End Sub

    Private Sub backgroundWorkerLpRx_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
        GFT.gftLpTxDone = True
    End Sub

    Private Sub backgroundWorkerLpTx_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)

    End Sub
    Private Sub backgroundWorkerLpTx_DoWork(sender As Object, e As DoWorkEventArgs)
        LP_HUB.gLpProcessTx(sender, e)
    End Sub

    Private Sub backgroundWorkerT_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
        GFT.gftLpTxDone = True
    End Sub

    Public Sub backgroundWorkerLpTx_ProgressChanged(sender As Object, e As ProgressChangedEventArgs)

    End Sub
    Shared lpDisplay As Char() = New Char() {"|"c, "/"c, "-"c, "\"c}
    Shared lpDisplayIndex As Integer = 0
    Public Sub backgroundWorkerLpRx_ProgressChanged(sender As Object, e As ProgressChangedEventArgs)
        '        Dim myPkt As LP_HUB.LP_DATA_PKT
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        If e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_SHELF) Then
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                'string.Format("{0:X2}", (byte)(e.UserState));


            Else
                MessageBox.Show("backgroundWorkerLpRx percentage RX_MSG_TYPE_SHELF not ready")
            End If

        ElseIf (e.ProgressPercentage = CInt(LP_HUB.gLpMsgType.CHUNK_SHELF_MODE_D)) OrElse (e.ProgressPercentage = CInt(LP_HUB.gLpMsgType.CHUNK_SHELF_MODE_U)) Then
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                'textBoxViewer.Text += DirectCast(e.UserState, [String])
            Else
                MessageBox.Show("backgroundWorkerLpRx percentage CHUNK_SHELF_MODE_D not ready")
            End If
        ElseIf e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_NODE_INFO) Then
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                Dim md As LP_DEVICE
                For i As Integer = 0 To LP_HUB.LP_NODES_INFO_PER_GROUP - 1
                    md = DirectCast(e.UserState, LP_DEVICE())(i)
                    If md.nodeID <> 0 Then
                        LP_HUB.updateDevice(md.nodeID, md.MID, md.nodePwrMode)
                    End If
                Next
            Else
                MessageBox.Show("backgroundWorkerLpRx percentage RX_MSG_TYPE_NODE_INFO not ready")
            End If

        ElseIf (e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_DATA_POWER_ON)) OrElse (e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_DATA_POWER_OFF)) Then
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                LP_HUB.liveChanged(DirectCast(e.UserState, LP_HUB.LP_DATA_PKT))
            Else
                MessageBox.Show("backgroundWorkerLpRx percentage RX_MSG_TYPE_DATA_POWER_ON not ready")
            End If
        ElseIf e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_DATA_IMMEDIATE) Then
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                LP_HUB.processImmediate(DirectCast(e.UserState, LP_HUB.LP_DATA_PKT))
            Else
                MessageBox.Show("backgroundWorkerLpRx percentage RX_MSG_TYPE_DATA_IMMEDIATE not ready")
            End If
        ElseIf e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_DATA_HIT_DATA) Then
            'hit data
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                LP_HUB.processHitData(DirectCast(e.UserState, LP_HUB.LP_DATA_PKT))
            Else
                MessageBox.Show("backgroundWorkerLpRx percentage RX_MSG_TYPE_DATA_HIT_DATA not ready")

            End If
        ElseIf e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_DATA_UPLOAD_MORE) Then
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                LP_HUB.processUploadData(DirectCast(e.UserState, LP_HUB.LP_DATA_PKT))
            Else
                MessageBox.Show("backgroundWorkerLpRx percentage RX_MSG_TYPE_DATA_UPLOAD_MORE not ready")

            End If
        ElseIf e.ProgressPercentage = CInt(LP_HUB.RX_MSG_TYPE.RX_MSG_TYPE_STRING) Then
            If e.UserState IsNot Nothing AndAlso Not bw.CancellationPending Then
                'textBoxViewer.Text += DirectCast(e.UserState, [String])
            Else
                MessageBox.Show("backgroundWorkerLpRx percentage RX_MSG_TYPE_STRING not ready")
            End If
        End If
        'tslLpStatus.Text = lpDisplay(System.Math.Max(System.Threading.Interlocked.Increment(lpDisplayIndex), lpDisplayIndex - 1) Mod 4).ToString()

        LP_HUB.refreshBinding()

    End Sub
#End Region

    Public Sub New()

        '    currentGftProfile = New gftProfile()
        '    currentGftProfile.mntLoc = &H44

        '   currentGftProfile.bMountInside = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_INOUTMSK

        '    currentGftProfile.bMountUSBFront = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
        '    currentGftProfile.bMountUSBLeft = currentGftProfile.mntLoc And (GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK And GFT.GFT_MNT_LOC_VER)
        '    currentGftProfile.bMountUSBRight = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_VER
        '    currentGftProfile.bMountUSBRear = False
        '    If Not currentGftProfile.bMountUSBFront And Not currentGftProfile.bMountUSBLeft And Not currentGftProfile.bMountUSBRight Then
        '    currentGftProfile.bMountUSBRear = True
        '     End If

        Debug.WriteLine("NEW MAIN FORM INSTANCE")
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        'tslStatus.Parent = progBar
        'tslStatus.Location = New Point(0, 0)
        'tslStatus.BackColor = Color.Transparent

    End Sub


    Private Sub bwCheckBattery_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwCheckBattery.DoWork
        Dim count As Integer = 0

        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        If gUsbComm.MyHid.deviceAttached AndAlso (currentGftProfile.gftWhereAmI = GFT.GFT_STATE_FIRM) AndAlso (gUsbComm.gftStat = 0) AndAlso (gUsbComm.gftFinishedUpload) Then
            Debug.WriteLine("SENDING BATTERY CHECK CMD")
            Dim batVol As Integer = gUsbComm.sendCmdBattery(CByte(gUsbComm.mhidCmd.MHID_CMD_CHECKBAT), 0)
            bw.ReportProgress(batVol)
            Console.WriteLine("BATT VO" & DateTime.Now.ToString() & "-batStatus=" & gftBattVol.ToString())
        End If
    End Sub

    Private Sub timerStatus_Tick(sender As Object, e As EventArgs) Handles timerStatus.Tick
        Debug.WriteLine("HIT STATUS TIMEOUT ")
        Return
        If timerStatusFirstEvent Then
            timerStatusFirstEvent = False
            Return
        End If
        If lblStatus.Visible = True Then
            lblStatus.Visible = False
        Else
            lblStatus.Visible = True
        End If
    End Sub

    Private Sub timerBatteryImage_Tick(sender As Object, e As EventArgs) Handles timerBatteryImage.Tick
        Debug.WriteLine("HIT BATTERY IMAGE TIMEOUT")
    End Sub

    Private Sub timerBatteryMon_Tick(sender As Object, e As EventArgs) Handles timerBatteryMon.Tick
        Debug.WriteLine("HIT BATTERY MONITOR TIMEOUT")
        checkBattery()
    End Sub

    Public Sub checkBattery()
        If Not bwCheckBattery.IsBusy Then
            bwCheckBattery.RunWorkerAsync()
        End If
    End Sub

    Private Sub bwCheckBattery_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bwCheckBattery.ProgressChanged
        gftBattVol = e.ProgressPercentage

        If gftBattVol < 100 Then
            picBattery.Visible = True
            Select Case batImage
                Case 0
                    picBattery.Image = My.Resources.uiResources.batteryCharging1
                    batImage = 1
                Case 1
                    picBattery.Image = My.Resources.uiResources.batteryCharging2
                    batImage = 2
                Case 2
                    picBattery.Image = My.Resources.uiResources.batteryCharging3
                    batImage = 3
                Case 3
                    picBattery.Image = My.Resources.uiResources.batteryCharging4
                    batImage = 0
            End Select
        ElseIf gftBattVol = 0 Then
            picBattery.Visible = False
        Else
            picBattery.Visible = True
            timerBatteryImage.Enabled = False
            picBattery.Image = My.Resources.uiResources.batteryCharged
            batImage = 4
        End If

    End Sub

    Private Sub footerStrip_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs)

    End Sub
End Class

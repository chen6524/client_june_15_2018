﻿Imports gForceTracker.frmNotify

Class frmCalibrate_new

    Private calRawReadingX As Double() = New Double(2) {}
    Private calRawReadingY As Double() = New Double(2) {}
    Private calRawReadingZ As Double() = New Double(2) {}


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        If Not currentGftProfile Is Nothing Then

            ' Add any initialization after the InitializeComponent() call.
            radioMntLocationInside.Checked = currentGftProfile.bMountInside
            radioMntLocationOutside.Checked = Not currentGftProfile.bMountInside
            'currentGftProfile.bMountUSB = radioMountUSBFront.Checked

            'radioButtonFront.Checked = currentGftProfile.bMountUSBFront
            'radioButtonRear.Checked = Not currentGftProfile.bMountUSBFront
            'radioButtonLeft.Checked = currentGftProfile.bMountUSBLeft
            'radioButtonRight.Checked = currentGftProfile.bMountUSBRight
            If currentGftProfile.bProximityEnabled > 0 Then
                radioButtonProxOn.Checked = True
            Else
                radioButtonProxOn.Checked = False
            End If
            'radioButtonProxOn.Checked = currentGftProfile.bProximityEnabled
        End If
    End Sub

    Private Function gCalGetRaw(ByRef R As Double()) As Boolean

        Dim nread As Integer = 0

        Dim sucess As Boolean = False
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}
            If Not gUsbComm.MyHid.deviceAttached Then
                'MessageBox.Show("Please connect GFT to this PC", "Warning")
                frmNotify.gMessage.MessageBox("Please connect GFT to this PC", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
            End If
            If gUsbComm.sendCmd(CByte(gUsbComm.mhidCmd.MHID_CMD_CAL_START), 0) = "passed" Then
                System.Threading.Thread.Sleep(200)
                outputReportBuffer(0) = 0
                outputReportBuffer(1) = CByte(gUsbComm.mhidCmd.MHID_CMD_CAL_GET)
                outputReportBuffer(2) = 0
                gUsbComm.MyHid.OutputReports(outputReportBuffer)

                gUsbComm.MyHid.inputReports(inputReportBuffer, nread)
                gUsbComm.MyHid.OutputReports(outputReportBuffer)
                gUsbComm.MyHid.inputReports(inputReportBuffer, nread)
                If True Then
                    If inputReportBuffer(1) = CByte(CByte(gUsbComm.mhidCmd.MHID_CMD_CAL_GET) Or gUsbComm.MHID_CMD_RESPONSE_BIT) Then
                        R(0) = gUsbComm.lgDecode(inputReportBuffer(3), inputReportBuffer(4))
                        R(1) = gUsbComm.lgDecode(inputReportBuffer(5), inputReportBuffer(6))
                        R(2) = gUsbComm.lgDecode(inputReportBuffer(7), inputReportBuffer(8))
                        sucess = True
                    Else
                        'MessageBox.Show("Communication failed, try again")
                        frmNotify.gMessage.MessageBox("Communication failed, try again", "Calibration", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End If
                End If
            Else
                'MessageBox.Show("Communication failed, try again")
                frmNotify.gMessage.MessageBox("Communication failed, try again", "Calibration", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        Catch ex As Exception

            'gUtility.DisplayException("Get System Time", ex)
            frmNotify.gMessage.MessageBox("Communication failed, try again", "Calibration", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
        Return sucess
    End Function

    Private Sub btnCalibrate_Click(sender As Object, e As EventArgs) Handles btnCalibrate.Click
        If radioButtonProxOn.Checked Then
            Dim presult As DialogResult
            presult = frmNotify.gMessage.MessageBox("Proximity Sensor is set to ON. " & vbCrLf & "If used incorrectly, the Proximity Sensor could result in impacts not being recorded." & vbCrLf & "Are you sure you want to proceed?", "Calibration", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
            If presult = DialogResult.No Then
                Return
            End If
        End If
        Dim calibrateWizard = New frmCalibrateWizard()
        With calibrateWizard
            calibrateWizard.wizardPictureBox.Image = My.Resources.uiResources.gftHelmetLevelSmall
            calibrateWizard.wizartText.Text = "Place the Helmet Upright" & vbCrLf & "on a Level Surface and" & vbCrLf & "Click ""Next"" to Proceed"
            calibrateWizard.StartPosition = FormStartPosition.CenterScreen

            If (calibrateWizard.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                gCalGetRaw(calRawReadingX)

                calibrateWizard.wizardPictureBox.Image = My.Resources.uiResources.helmetleftsideSmall
                calibrateWizard.wizartText.Text = "Place the Helmet on its'" & vbCrLf & "Left Side on a Level" & vbCrLf & "Surface and Click" & vbCrLf & """Next"" to Proceed"
                calibrateWizard.StartPosition = FormStartPosition.CenterScreen

                If (calibrateWizard.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                    gCalGetRaw(calRawReadingY)

                    If Not gCalc.validateRotation(calRawReadingX, calRawReadingY, 1) Then
                        'MessageBox.Show("GFT is not properly positioned, please try again.")
                        frmNotify.gMessage.MessageBox("GFT is not properly positioned, please try again.", "Calibration", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        calibrateWizard.wizardPictureBox.Image = My.Resources.uiResources.gftHelmetBackSmall
                        calibrateWizard.wizartText.Text = "Place the Helmet on its'" & vbCrLf & "Back Side on a Level" & vbCrLf & "Surface and Click" & vbCrLf & """Next"" to Proceed"
                        calibrateWizard.StartPosition = FormStartPosition.CenterScreen
                        If (calibrateWizard.ShowDialog() = Windows.Forms.DialogResult.OK) Then


                            Dim b As Double() = New Double(2) {}
                            Dim v As Double() = New Double(2) {}
                            Dim azmuthElev As Double() = New Double(1) {}
                            Dim index As Integer
                            Dim myPoint As Double() = New Double(2) {}
                            Dim mountPosion As String
                            Dim calResult As Double()
                            Dim result As DialogResult
                            gCalGetRaw(calRawReadingZ)

                            If Not gCalc.validateRotation(calRawReadingX, calRawReadingZ, 1) Then
                                'MessageBox.Show("GFT is not properly positioned, try it again please.")
                                frmNotify.gMessage.MessageBox("GFT is not properly positioned, please try again.", "Calibration", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Else
                                b(0) = calRawReadingX(0)
                                b(1) = calRawReadingX(1)
                                b(2) = calRawReadingX(2)
                                buttonCalibFun()

                                'calResult = gCalc.mCalibrate(ref b, ref gCalc.qua);
                                'gUsbComm.transLoc(ref calResult, GFT.gftProfile.mntLoc);

                                'azmuthElev = gReport.getAzmuthAuto(calResult);
                                'index = gCalc.getHitLocIndexCal(azmuthElev[0], azmuthElev[1]);
                                mountPosion = getHitLocStr(GFT.gftProfile.mntLoc)
                                'result = MessageBox.Show()
                                result = frmNotify.gMessage.MessageBox((Convert.ToString("Is the GFT on ") & mountPosion) + " ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                                If result = DialogResult.Yes Then

                                    Dim mq As Int16() = New Int16(3) {}
                                    mq(0) = currentGftProfile.iLocation(0)
                                    mq(1) = currentGftProfile.iLocation(1)
                                    mq(2) = currentGftProfile.iLocation(2)
                                    mq(3) = currentGftProfile.iLocation(3)

                                    currentGftProfile.bMountCoefficient = 100 ' Default value
                                    'currentGftProfile.bProximityEnabled = radioButtonProxOn.Checked
                                    If radioButtonProxOn.Checked Then
                                        currentGftProfile.bProximityEnabled = 1
                                    Else
                                        currentGftProfile.bProximityEnabled = 0
                                    End If
                                    rest.calibrate(mq, radioMntLocationInside.Checked, False, False, False, False)
                                    gUsbComm.configGft()
                                    ' to do

                                    needsCalibration = False
                                    'MessageBox.Show("GFT has been calibrated for location and ready to use.")
                                    frmNotify.gMessage.MessageBox("GFT has been calibrated for location and ready to use.", "Calibration", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    frmNotify.unPlugUsb()
                                    Me.Close()
                                ElseIf result = DialogResult.No Then
                                    'MessageBox.Show("Please try again.")
                                    frmNotify.gMessage.MessageBox("Please try again.", "Calibration", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Else
                                    '...
                                End If
                            End If


                        End If
                    End If

                End If
            End If
        End With
        'currentGftProfile.bMountInside = radioMntLocationInside.Checked
        ''currentGftProfile.bMountUSB = radioMountUSBFront.Checked

        'currentGftProfile.bMountUSBFront = radioButtonFront.Checked
        'currentGftProfile.bMountUSBRear = radioButtonRear.Checked
        'currentGftProfile.bMountUSBLeft = radioButtonLeft.Checked
        'currentGftProfile.bMountUSBRight = radioButtonRight.Checked

        'currentGftProfile.bProximityEnabled = radioButtonProxOn.Checked
        '' currentGftProfile.bTestMode = radioButtonTestOn.Checked

        'gUsbComm.calibrateGFT(radioMntLocationInside.Checked, radioButtonFront.Checked, radioButtonRear.Checked, radioButtonLeft.Checked, radioButtonRight.Checked)



        ''gUsbComm.calibrateGFT(radioMntLocationInside.Checked, radioMountUSBFront.Checked)
        'needsCalibration = False
        'Me.Close()
    End Sub

    Private Sub buttonCalibFun()

        Dim mq As Int16()
        mq = gCalc.getCaliQ(calRawReadingX(0), calRawReadingX(1), calRawReadingX(2), radioMntLocationInside.Checked)
        GFT.gftProfile.location(0) = mq(0)
        GFT.gftProfile.location(1) = mq(1)
        GFT.gftProfile.location(2) = mq(2)
        GFT.gftProfile.location(3) = mq(3)

        GFT.gftProfile.mntLoc = gCalGetMntLoc(calRawReadingX, calRawReadingY, calRawReadingZ, radioMntLocationInside.Checked)

        currentGftProfile.iLocation(0) = GFT.gftProfile.location(0)
        currentGftProfile.iLocation(1) = GFT.gftProfile.location(1)
        currentGftProfile.iLocation(2) = GFT.gftProfile.location(2)
        currentGftProfile.iLocation(3) = GFT.gftProfile.location(3)
        currentGftProfile.mntLoc = GFT.gftProfile.mntLoc

    End Sub

    Public Shared Function getHitLocStr(mntLoc As Byte) As String

        Dim strMnt As String = "Unkown"
        If (mntLoc And GFT.GFT_MNT_LOC_INOUTMSK) > 0 Then
            strMnt = "Inside "
        Else
            strMnt = "Outside"
        End If
        If (mntLoc And GFT.GFT_MNT_LOC_ZONE_CROWN) > 0 Then
            'top
            strMnt += "Crown"
        ElseIf (mntLoc And GFT.GFT_MNT_LOC_ZONE_FRONT) > 0 Then
            'front
            strMnt += "Front"
        ElseIf (mntLoc And GFT.GFT_MNT_LOC_ZONE_BACK) > 0 Then
            strMnt += "Back"
        ElseIf (mntLoc And GFT.GFT_MNT_LOC_ZONE_LEFT) > 0 Then
            strMnt += "Left"
        ElseIf (mntLoc And GFT.GFT_MNT_LOC_ZONE_RIGHT) > 0 Then
            strMnt += "Right"
        End If
        Return strMnt

    End Function

    Public Shared Function gCalGetMntLoc(ByRef A As Double(), ByRef B As Double(), ByRef C As Double(), inside As Boolean) As Byte
        Dim location As Byte = 0


        'side
        If Math.Abs(B(2)) >= Math.Abs(B(1)) AndAlso Math.Abs(B(2)) >= Math.Abs(B(0)) Then

            'left
            If B(2) <= 0 Then
                location = If(Not inside, GFT.GFT_MNT_LOC_ZONE_RIGHT, GFT.GFT_MNT_LOC_ZONE_LEFT)

                If Math.Abs(C(1)) >= Math.Abs(C(0)) Then
                    If C(1) > 0 Then
                        location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    End If
                ElseIf (inside AndAlso C(0) > 0) OrElse (Not inside AndAlso C(0) < 0) Then
                    location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    'front
                    location = location Or GFT.GFT_MNT_LOC_VER
                Else

                    location = location Or GFT.GFT_MNT_LOC_VER

                End If
            Else
                'right
                location = If(Not inside, GFT.GFT_MNT_LOC_ZONE_LEFT, GFT.GFT_MNT_LOC_ZONE_RIGHT)
                If Math.Abs(C(1)) >= Math.Abs(C(0)) Then
                    If C(1) > 0 Then
                        location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    End If
                ElseIf (inside AndAlso C(0) > 0) OrElse (Not inside AndAlso C(0) < 0) Then
                    location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    'front
                    location = location Or GFT.GFT_MNT_LOC_VER
                Else
                    location = location Or GFT.GFT_MNT_LOC_VER
                End If



            End If
            'front+rear
        ElseIf Math.Abs(C(2)) >= Math.Abs(C(1)) AndAlso Math.Abs(C(2)) >= Math.Abs(C(0)) Then
            If C(2) > 0 Then
                location = If(Not inside, GFT.GFT_MNT_LOC_ZONE_BACK, GFT.GFT_MNT_LOC_ZONE_FRONT)
                If Math.Abs(A(0)) >= Math.Abs(A(1)) Then
                    location = location Or GFT.GFT_MNT_LOC_VER
                    If (Not inside AndAlso A(0) < 0) OrElse (inside AndAlso A(0) > 0) Then
                        location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    End If
                ElseIf (A(1) > 0 AndAlso Not inside) OrElse (A(1) < 0 AndAlso inside) Then

                    location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK


                End If
            Else
                location = If(Not inside, GFT.GFT_MNT_LOC_ZONE_FRONT, GFT.GFT_MNT_LOC_ZONE_BACK)
                If Math.Abs(A(0)) >= Math.Abs(A(1)) Then
                    location = location Or GFT.GFT_MNT_LOC_VER
                    If (Not inside AndAlso A(0) > 0) OrElse (inside AndAlso A(0) < 0) Then
                        location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    End If
                ElseIf (A(1) < 0 AndAlso Not inside) OrElse (A(1) > 0 AndAlso inside) Then

                    location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                End If
            End If

            'top
        Else  'Math.Abs(A(2)) >= Math.Abs(A(1)) AndAlso Math.Abs(A(2)) >= Math.Abs(A(0)) Then
            location = GFT.GFT_MNT_LOC_ZONE_CROWN
            If Math.Abs(B(0)) >= Math.Abs(B(1)) Then
                If inside Then
                    If B(0) >= 0 Then
                        location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                        'front
                    End If
                ElseIf B(0) <= 0 Then
                    location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    'front
                End If
            ElseIf B(1) <= 0 Then
                location = location Or GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                'front
                location = location Or GFT.GFT_MNT_LOC_VER
            Else
                location = location Or GFT.GFT_MNT_LOC_VER
            End If
        End If
        location = location Or If(inside, GFT.GFT_MNT_LOC_INOUTMSK, CByte(0))
        Return location
    End Function

End Class

﻿Imports System.Data.SqlServerCe
Imports System.Data.SqlClient
Imports gForceTracker.frmNotify


Public Class UploadSVSessions
    Private conn As String = "Data Source=gForceTracker.sdf;Password=123456;Persist Security Info=True"
    Private con As SqlCeConnection
    Private bindingSource As New BindingSource
    Private adapter As New SqlCeDataAdapter
    Private dt As DataTable
    Public currentGID As String = ""
    Public uploadAll As Boolean = False

    Event dvSessionListButtonClick(sender As DataGridView, e As DataGridViewCellEventArgs)
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.progBar.Minimum = 0
        Me.progBar.Maximum = 100
        UpdateProgressBar(0)
    End Sub
    Public Sub UpdateProgressBar(ByVal Value As Integer)
        Me.progBar.Value = Value
        Me.progBar.Refresh()
    End Sub

    Public Sub GetData()
        Using con = New SqlCeConnection(conn)
            Try
                Dim cmd As SqlCeCommand = New SqlCeCommand
                With cmd
                    .Connection = con
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT gw.PlayerNumber, gw.FNAME, gw.LNAME, COUNT(sw.SID) AS numSessions, SUM(ew.numImpacts) as numImpacts, sw.GID  FROM SessionWireless sw " _
                                 & "LEFT JOIN gftWireless gw ON sw.GID = gw.GID AND (gw.MID = sw.MID OR gw.MID = '' OR sw.MID = '' OR sw.MID = '0000000000') AND gw.nodeID = sw.UID " _
                                 & "LEFT JOIN (SELECT SID, COUNT(EID) As numImpacts FROM EventsWireless GROUP BY SID) ew ON sw.SID = ew.SID " _
                                 & "WHERE sw.ERASED = 'Y' " _
                                 & "GROUP BY GID, FNAME, LNAME, PlayerNumber " _
                                 & "UNION ALL " _
                                 & "SELECT gw.PlayerNumber, gw.FNAME, gw.LNAME, COUNT(sw.SID) AS numSessions, SUM(ew.numImpacts) as numImpacts, sw.GID  FROM PerfSessionWireless sw " _
                                 & "LEFT JOIN gftWireless gw ON sw.GID = gw.GID AND (gw.MID = sw.MID OR gw.MID = '' OR sw.MID = '' OR sw.MID = '0000000000') AND gw.nodeID = sw.UID " _
                                 & "LEFT JOIN (SELECT SID, COUNT(EID) As numImpacts FROM PerfWireless GROUP BY SID) ew ON sw.SID = ew.SID " _
                                 & "GROUP BY GID, FNAME, LNAME, PlayerNumber"
                End With
                Using adapter = New SqlCeDataAdapter(cmd)
                    If con.State.ToString <> "Open" Then
                        con.Open()
                    End If
                    Debug.WriteLine("Connection Status: " & con.State.ToString)
                    dt = New DataTable
                    adapter.Fill(dt)
                    bindingSource.DataSource = dt
                End Using
            Catch ex As Exception
                'uhoh
            End Try
        End Using
    End Sub
    Public Sub DisableUploadButtons()
        dvSessionList.Enabled = False
        Me.btnUploadAll.Enabled = False
    End Sub
    Public Sub EnableUploadButtons()
        dvSessionList.Enabled = True
        Me.btnUploadAll.Enabled = True
    End Sub
    Private Sub dvSessionList_CellContentClick(sender As System.Object, e As DataGridViewCellEventArgs) Handles dvSessionList.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)
        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            RaiseEvent dvSessionListButtonClick(senderGrid, e)
        End If
    End Sub

    Private Sub btnUploadAll_Click(sender As Object, e As EventArgs) Handles btnUploadAll.Click
        Me.uploadAll = True
        Me.DisableUploadButtons()
        getNextGID()
        frmNotify.UploadSVData()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Public Function getNextGID() As String
        Dim GID As String = ""
        If dvSessionList.RowCount > 0 Then
            GID = dvSessionList.Rows(0).Cells(0).Value
            Me.currentGID = GID
            Me.dvSessionList.Rows.RemoveAt(0)
        End If
        Return GID
    End Function

    Private Sub dvSessionListButton_Click(sender As DataGridView, e As DataGridViewCellEventArgs) Handles Me.dvSessionListButtonClick
        Me.uploadAll = False
        Dim GID As String = dvSessionList.Rows(e.RowIndex).Cells(0).Value
        Debug.WriteLine("Hey " & e.RowIndex & " GID: " & GID)
        Me.DisableUploadButtons()
        Me.currentGID = GID
        Me.dvSessionList.Rows.RemoveAt(e.RowIndex)
        frmNotify.UploadSVData()
    End Sub

    Private Sub UploadSVSessions_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dvSessionList.AutoGenerateColumns = False
        Me.dvSessionList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect

        Me.dvSessionList.RowsDefaultCellStyle.BackColor = System.Drawing.Color.White
        Me.dvSessionList.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(226, 228, 255)
        Me.dvSessionList.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(242, 245, 169)
        Me.dvSessionList.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(0, 0, 0)
        Me.dvSessionList.DataSource = Me.bindingSource
        GetData()
        If dvSessionList.RowCount = 0 Then
            Me.Close() ' No rows, close!
        End If
    End Sub
End Class
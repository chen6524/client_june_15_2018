﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCalibrateWizard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCalibrateWizard))
        Me.wizardPictureBox = New System.Windows.Forms.PictureBox()
        Me.wizartText = New System.Windows.Forms.Label()
        Me.wizardButton = New System.Windows.Forms.Button()
        CType(Me.wizardPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'wizardPictureBox
        '
        Me.wizardPictureBox.Location = New System.Drawing.Point(12, 12)
        Me.wizardPictureBox.Name = "wizardPictureBox"
        Me.wizardPictureBox.Size = New System.Drawing.Size(181, 144)
        Me.wizardPictureBox.TabIndex = 0
        Me.wizardPictureBox.TabStop = False
        '
        'wizartText
        '
        Me.wizartText.AutoSize = True
        Me.wizartText.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wizartText.Location = New System.Drawing.Point(208, 12)
        Me.wizartText.Name = "wizartText"
        Me.wizartText.Size = New System.Drawing.Size(51, 18)
        Me.wizartText.TabIndex = 1
        Me.wizartText.Text = "Label1"
        '
        'wizardButton
        '
        Me.wizardButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.wizardButton.Location = New System.Drawing.Point(326, 133)
        Me.wizardButton.Name = "wizardButton"
        Me.wizardButton.Size = New System.Drawing.Size(75, 23)
        Me.wizardButton.TabIndex = 2
        Me.wizardButton.Text = "Next"
        Me.wizardButton.UseVisualStyleBackColor = True
        '
        'frmCalibrateWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 169)
        Me.Controls.Add(Me.wizardButton)
        Me.Controls.Add(Me.wizartText)
        Me.Controls.Add(Me.wizardPictureBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCalibrateWizard"
        Me.ShowInTaskbar = False
        Me.Text = "Calibration Wizard"
        CType(Me.wizardPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents wizardPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents wizartText As System.Windows.Forms.Label
    Friend WithEvents wizardButton As System.Windows.Forms.Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotify
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNotify))
        Me.notifyIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.contextMenuScr = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.menuDiagnostics = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuCalibrate = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuWireless = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuUpdateSidelineDB = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuSite = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bwUploadData = New System.ComponentModel.BackgroundWorker()
        Me.bwPlugUSB = New System.ComponentModel.BackgroundWorker()
        Me.bwUpdateFirmware = New System.ComponentModel.BackgroundWorker()
        Me.bwLoad = New System.ComponentModel.BackgroundWorker()
        Me.bwCheckDevice = New System.ComponentModel.BackgroundWorker()
        Me.bwWirelessData = New System.ComponentModel.BackgroundWorker()
        Me.bwUpdateUI = New System.ComponentModel.BackgroundWorker()
        Me.contextMenuScr.SuspendLayout()
        Me.SuspendLayout()
        '
        'notifyIcon
        '
        Me.notifyIcon.BalloonTipText = "Click to launch GForce Tracker"
        Me.notifyIcon.BalloonTipTitle = "GForce Tracker"
        Me.notifyIcon.ContextMenuStrip = Me.contextMenuScr
        Me.notifyIcon.Icon = CType(resources.GetObject("notifyIcon.Icon"), System.Drawing.Icon)
        Me.notifyIcon.Text = "GForce Tracker"
        Me.notifyIcon.Visible = True
        '
        'contextMenuScr
        '
        Me.contextMenuScr.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuDiagnostics, Me.menuCalibrate, Me.menuWireless, Me.menuUpdateSidelineDB, Me.menuSite, Me.menuExit})
        Me.contextMenuScr.Name = "contextMenuScr"
        Me.contextMenuScr.Size = New System.Drawing.Size(217, 136)
        '
        'menuDiagnostics
        '
        Me.menuDiagnostics.Enabled = False
        Me.menuDiagnostics.Name = "menuDiagnostics"
        Me.menuDiagnostics.Size = New System.Drawing.Size(216, 22)
        Me.menuDiagnostics.Text = "Device Diagnostics"
        '
        'menuCalibrate
        '
        Me.menuCalibrate.Enabled = False
        Me.menuCalibrate.Name = "menuCalibrate"
        Me.menuCalibrate.Size = New System.Drawing.Size(216, 22)
        Me.menuCalibrate.Text = "Calibrate Device"
        '
        'menuWireless
        '
        Me.menuWireless.Name = "menuWireless"
        Me.menuWireless.Size = New System.Drawing.Size(216, 22)
        Me.menuWireless.Text = "Sideline Viewer"
        '
        'menuUpdateSidelineDB
        '
        Me.menuUpdateSidelineDB.Name = "menuUpdateSidelineDB"
        Me.menuUpdateSidelineDB.Size = New System.Drawing.Size(216, 22)
        Me.menuUpdateSidelineDB.Text = "Sideline Viewer Cloud Sync"
        '
        'menuSite
        '
        Me.menuSite.Name = "menuSite"
        Me.menuSite.Size = New System.Drawing.Size(216, 22)
        Me.menuSite.Text = "GForce Tracker Web Portal"
        '
        'menuExit
        '
        Me.menuExit.Name = "menuExit"
        Me.menuExit.Size = New System.Drawing.Size(216, 22)
        Me.menuExit.Text = "Exit GForce Tracker"
        '
        'bwUploadData
        '
        Me.bwUploadData.WorkerReportsProgress = True
        Me.bwUploadData.WorkerSupportsCancellation = True
        '
        'bwPlugUSB
        '
        Me.bwPlugUSB.WorkerReportsProgress = True
        Me.bwPlugUSB.WorkerSupportsCancellation = True
        '
        'bwUpdateFirmware
        '
        Me.bwUpdateFirmware.WorkerReportsProgress = True
        '
        'bwLoad
        '
        '
        'bwCheckDevice
        '
        Me.bwCheckDevice.WorkerReportsProgress = True
        Me.bwCheckDevice.WorkerSupportsCancellation = True
        '
        'bwWirelessData
        '
        Me.bwWirelessData.WorkerReportsProgress = True
        Me.bwWirelessData.WorkerSupportsCancellation = True
        '
        'frmNotify
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(0, 0)
        Me.ControlBox = False
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNotify"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "GForce Tracker"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.contextMenuScr.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents notifyIcon As System.Windows.Forms.NotifyIcon
    Friend WithEvents contextMenuScr As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents menuDiagnostics As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuCalibrate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuSite As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bwUpdateUI As System.ComponentModel.BackgroundWorker
    Public WithEvents bwUploadData As System.ComponentModel.BackgroundWorker
    Friend WithEvents bwPlugUSB As System.ComponentModel.BackgroundWorker
    Friend WithEvents bwUpdateFirmware As System.ComponentModel.BackgroundWorker
    Friend WithEvents bwLoad As System.ComponentModel.BackgroundWorker
    Friend WithEvents bwCheckDevice As System.ComponentModel.BackgroundWorker
    Friend WithEvents menuWireless As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuUpdateSidelineDB As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents bwWirelessData As System.ComponentModel.BackgroundWorker
End Class

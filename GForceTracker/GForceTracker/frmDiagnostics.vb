﻿Imports System.Threading
Imports System.ComponentModel
Imports gForceTracker.frmNotify

Public Class frmDiagnostics
    Private Sub buttonTestFlash_Click(sender As System.Object, e As System.EventArgs) Handles buttonTestFlash.Click
        sendCommand("Flash")
    End Sub

    Private Sub sendCommand(ByVal sCmd As String)
        Dim stopTest As Boolean = False
        Dim stopwatch As Stopwatch = New Stopwatch()
        Dim ts As TimeSpan
        Dim sResult As String = "Failed"
        Select Case sCmd
            Case "Flash"
                labelTestFlash.Text = "Testing"
                sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_FLASH, 0)
                labelTestFlash.Text = labelTestFlash.Text & " -> " & sResult
            Case "Gyro"
                labelTestGyro.Text = "Testing"
                sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_GYRO, 0)
                labelTestGyro.Text = labelTestGyro.Text & " -> " & sResult
            Case "Acce"
                labelTestAcc.Text = "Testing"
                sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_ACCE, 0)
                labelTestAcc.Text = labelTestAcc.Text & " -> " & sResult
            Case "RTC"
                labelTestRTC.Text = "Testing"
                '  sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_RTC, 0)

                Dim mtime As DateTime
                Dim setTime As DateTime = gUsbComm.getUTC() 'DateTime.Now
                gUsbComm.syncSystemTime() 'DateTime.Now
                Thread.Sleep(2000)
                mtime = gUsbComm.getGftTime()
                If mtime > setTime Then
                    sResult = "Passed"
                Else
                    sResult = "Failed"
                End If

                labelTestRTC.Text = labelTestRTC.Text & " -> " & sResult
            Case "LED"
                labelTestLed.Text = "Check LED Colour"
                If Not bwTestLED.IsBusy Then
                    bwTestLED.RunWorkerAsync()
                End If
            Case "BUZZ"
                labelTestBuzz.Text = "Listen"
                sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_BUZZ, 0)
                Dim result = MessageBox.Show("Did you hear the buzzer sound?", "Device Diagnostics", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then
                    sResult = "Passed"
                ElseIf result = DialogResult.No Then
                    sResult = "Failed"
                End If

                labelTestBuzz.Text = labelTestBuzz.Text & " -> " & sResult
            Case "TORCH"
                labelTorchLed.Text = "Check Alarm LED"

                stopwatch.Reset()
                stopwatch.Start()
                MsgBox("Please watch the Alarm LED on the Device", MsgBoxStyle.OkOnly, "Device Diagnostics")
                While stopTest = False
                    stopwatch.Stop()
                    ts = stopwatch.Elapsed
                    If (ts.Seconds > 6) Then
                        stopTest = True
                        Exit While
                    Else
                        gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_TORCH_LED, 0)
                        gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_DEFAULT_SET, 0)
                        stopwatch.Start()
                        Thread.Sleep(300)
                    End If
                End While
                'sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_TORCH_LED, 0)
                Thread.Sleep(1000)
                Dim result = MessageBox.Show("Did you see the Alarm LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then
                    sResult = "Passed"
                ElseIf result = DialogResult.No Then
                    sResult = "Failed"
                End If
                labelTorchLed.Text = labelTorchLed.Text & " -> " & sResult
            Case "DEFAULT"
        End Select
    End Sub

    Private Sub buttonTestAcc_Click(sender As System.Object, e As System.EventArgs) Handles buttonTestAcc.Click
        sendCommand("Acce")
    End Sub

    Private Sub buttonTestGyro_Click(sender As System.Object, e As System.EventArgs) Handles buttonTestGyro.Click
        sendCommand("Gyro")
    End Sub

    Private Sub buttonRTC_Click(sender As System.Object, e As System.EventArgs) Handles buttonRTC.Click
        sendCommand("RTC")
    End Sub

    Private Sub buttonLed_Click(sender As System.Object, e As System.EventArgs) Handles buttonLed.Click
        sendCommand("LED")
    End Sub

    Private Sub buttonBuzz_Click(sender As System.Object, e As System.EventArgs) Handles buttonBuzz.Click
        sendCommand("BUZZ")
    End Sub

    Private Sub buttonTorchLed_Click(sender As System.Object, e As System.EventArgs) Handles buttonTorchLed.Click
        sendCommand("TORCH")
    End Sub

    Private Sub btnTestAll_Click(sender As System.Object, e As System.EventArgs) Handles btnTestAll.Click
        If Not bwDiagnostics.IsBusy Then
            bwDiagnostics.RunWorkerAsync()
        End If
    End Sub

    Private Sub frmDiagnostics_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        lblFirmwareRelease.Text = currentgftprofile.firmmajor & "." & currentgftprofile.firmminor
    End Sub

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        labelTestFlash.Text = ""
        labelTestAcc.Text = ""
        labelTestGyro.Text = ""
        labelTestRTC.Text = ""
        labelTestLed.Text = ""
        labelTestBuzz.Text = ""
        labelTorchLed.Text = ""
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
        frmMain.Focus()
    End Sub

    Private Sub bwDiagnostics_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwDiagnostics.ProgressChanged
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Select Case e.ProgressPercentage
            Case 1
                ' Flash Test
                labelTestFlash.Text = DirectCast("Testing", String)
                Exit Select
            Case 2
                ' Flash Test Complete
                labelTestFlash.Text = DirectCast(labelTestFlash.Text & " -> " & e.UserState, String)
                Exit Select
            Case 3
                ' Acc Test
                labelTestAcc.Text = DirectCast("Testing", String)
                Exit Select
            Case 4
                ' Acc Test Complete
                labelTestAcc.Text = DirectCast(labelTestAcc.Text & " -> " & e.UserState, String)
                Exit Select
            Case 5
                ' Gyro Test
                labelTestGyro.Text = DirectCast("Testing", String)
                Exit Select
            Case 6
                ' Gyro Test Complete
                labelTestGyro.Text = DirectCast(labelTestGyro.Text & " -> " & e.UserState, String)
                Exit Select
            Case 7
                ' RTC Test
                labelTestRTC.Text = DirectCast("Testing", String)
                Exit Select
            Case 8
                ' RTC Test Complete
                labelTestRTC.Text = DirectCast(labelTestRTC.Text & " -> " & e.UserState, String)
                Exit Select
            Case 9
                ' LED Start
                labelTestLed.Text = DirectCast("Check LED Colour", String)
                Exit Select
            Case 10
                ' RED LED
                labelTestLed.Text = DirectCast(labelTestLed.Text & " -> White", String)
                Exit Select
            Case 11
                ' YELLOW LED
                labelTestLed.Text = DirectCast(labelTestLed.Text & " | Green", String)
                Exit Select
            Case 12
                ' GREEN LED
                labelTestLed.Text = DirectCast(labelTestLed.Text & " | Red", String)
                Exit Select
            Case 13
                ' Buzzer
                labelTestBuzz.Text = DirectCast("Listen", String)
                Exit Select
            Case 14
                ' Buzzer Complete
                labelTestBuzz.Text = DirectCast(labelTestBuzz.Text & " -> " & e.UserState, String)
                Exit Select
            Case 15
                ' Torch
                labelTorchLed.Text = DirectCast("Check Torch LED", String)
                Exit Select
            Case 16
                ' Torch Complete
                labelTorchLed.Text = DirectCast(labelTorchLed.Text & " -> " & e.UserState, String)
                Exit Select
        End Select
    End Sub

    Private Sub bwDiagnostics_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwDiagnostics.RunWorkerCompleted
        MessageBox.Show(Me, "All Tests have been Completed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub bwTestLED_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwTestLED.DoWork
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim sResult As String

        bw.ReportProgress(9, "Testing")
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_LED, 0)
        Dim result = MessageBox.Show("Do you see the White LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            sResult = "Passed"
        ElseIf result = DialogResult.No Then
            sResult = "Failed"
        End If
        bw.ReportProgress(10, sResult)
        'Thread.Sleep(2000)
        'sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_LED, 1)
        'result = MessageBox.Show("Did you see the Green LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
        'If result = DialogResult.Yes Then
        '    sResult = "Passed"
        'ElseIf result = DialogResult.No Then
        '    sResult = "Failed"
        'End If
        'bw.ReportProgress(11, sResult)
        'Thread.Sleep(2000)
        'sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_LED, 2)
        '
        'result = MessageBox.Show("Did you see the Red LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
        'If result = DialogResult.Yes Then
        '    sResult = "Passed"
        'ElseIf result = DialogResult.No Then
        '    sResult = "Failed"
        'End If
        'bw.ReportProgress(12, sResult)
        Thread.Sleep(2000)
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_DEFAULT_SET, 0)

    End Sub

    Private Sub bwTestLED_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bwTestLED.ProgressChanged
        Select Case e.ProgressPercentage
            Case 9
                ' LED Start
                labelTestLed.Text = DirectCast("Check LED Colour", String)
                Exit Select
            Case 10
                ' RED LED
                labelTestLed.Text = DirectCast(labelTestLed.Text & " -> White", String)
                Exit Select
            Case 11
                ' YELLOW LED
                labelTestLed.Text = DirectCast(labelTestLed.Text & " | Green", String)
                Exit Select
            Case 12
                ' GREEN LED
                labelTestLed.Text = DirectCast(labelTestLed.Text & " | Red", String)
                Exit Select
        End Select
    End Sub

    Private Sub bwDiagnostics_DoWork(sender As Object, e As DoWorkEventArgs) Handles bwDiagnostics.DoWork
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Dim stopTest As Boolean = False
        Dim stopwatch As Stopwatch = New Stopwatch()
        Dim ts As TimeSpan
        Dim sResult As String
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_DEFAULT_SET, 0)
        Thread.Sleep(1000) 'Give the device a moment
        bw.ReportProgress(1, "Testing")
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_FLASH, 0)
        bw.ReportProgress(2, sResult)
        Thread.Sleep(1000)
        bw.ReportProgress(3, "Testing")
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_ACCE, 0)
        bw.ReportProgress(4, sResult)
        Thread.Sleep(1000)
        bw.ReportProgress(5, "Testing")
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_GYRO, 0)
        bw.ReportProgress(6, sResult)
        Thread.Sleep(1000)
        bw.ReportProgress(7, "Testing")
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_RTC, 0)
        Dim mtime As DateTime
        Dim setTime As DateTime = gUsbComm.getUTC() 'DateTime.Now
        gUsbComm.syncSystemTime() 'DateTime.Now
        Thread.Sleep(2000)
        mtime = gUsbComm.getGftTime()
        If mtime > setTime Then
            sResult = "Passed"
        Else
            sResult = "Failed"
        End If
        bw.ReportProgress(8, sResult)
        Thread.Sleep(1000)
        bw.ReportProgress(9, "Testing")
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_LED, 0)
        Dim result = MessageBox.Show("Do you see the White LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            sResult = "Passed"
        ElseIf result = DialogResult.No Then
            sResult = "Failed"
        End If
        bw.ReportProgress(10, sResult)
        'Thread.Sleep(2000)
        'sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_LED, 1)
        'result = MessageBox.Show("Did you see the Green LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
        'If result = DialogResult.Yes Then
        '    sResult = "Passed"
        'ElseIf result = DialogResult.No Then
        '    sResult = "Failed"
        'End If
        'bw.ReportProgress(11, sResult)
        'Thread.Sleep(2000)
        'sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_LED, 2)
        'result = MessageBox.Show("Did you see the Red LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
        'If result = DialogResult.Yes Then
        '    sResult = "Passed"
        'ElseIf result = DialogResult.No Then
        '    sResult = "Failed"
        'End If
        'bw.ReportProgress(12, sResult)
        Thread.Sleep(1000)
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_DEFAULT_SET, 0)
        Thread.Sleep(1000) 'Device needs a second before it can accept the buzz command...
        bw.ReportProgress(13, "Listen")
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_BUZZ, 0)
        result = MessageBox.Show("Did you hear the buzzer sound?", "Device Diagnostics", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            sResult = "Passed"
        ElseIf result = DialogResult.No Then
            sResult = "Failed"
        End If
        bw.ReportProgress(14, sResult)
        Thread.Sleep(1000)
        bw.ReportProgress(15, "Listen")
        stopwatch.Reset()
        stopwatch.Start()
        MsgBox("Please watch the Alarm LED on the Device", MsgBoxStyle.OkOnly, "Device Diagnostics")
        While stopTest = False
            stopwatch.Stop()
            ts = stopwatch.Elapsed
            If (ts.Seconds > 6) Then
                stopTest = True
                Exit While
            Else
                gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_TORCH_LED, 0)
                gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_DEFAULT_SET, 0)
                stopwatch.Start()
                Thread.Sleep(300)
            End If
        End While
        'sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_TORCH_LED, 0)
        Thread.Sleep(1000)
        result = MessageBox.Show("Did you see the Alarm LED on?", "Device Diagnostics", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            sResult = "Passed"
        ElseIf result = DialogResult.No Then
            sResult = "Failed"
        End If
        bw.ReportProgress(16, sResult)
        sResult = gUsbComm.sentTestCmd(gUsbComm.mft_cmd.MFT_CMD_DEFAULT_SET, 0)

    End Sub

    Private Sub btnFactoryDefault_Click(sender As Object, e As EventArgs) Handles btnFactoryDefault.Click
        Dim result = MessageBox.Show("Reset Device to Factory Defaults? All data will be erased permanently from the device.", "Reset Device to Factory Defaults", MessageBoxButtons.YesNo) 'and settings 
        If result = DialogResult.Yes Then
            Thread.Sleep(1000)
            Dim comResult = gUsbComm.sendCmd(CByte(gUsbComm.mhidCmd.MHID_CMD_ERASE_ACC_DATA), 0)
            If comResult = "passed" Then
                Thread.Sleep(15000) 'Device appears to require 10 seconds to become ready after erase command + Add another 5 seconds to be safe
                'gUsbComm.resetGft()
                MessageBox.Show("Flash Data Erased", "Reset Device to Factory Defaults")
                'gUsbComm.getProfile()
                'MsgBox("Before using your device, you must first calibrate it.  Please press OK to calibrate now.", MsgBoxStyle.OkOnly, "GForce Tracker - Calibration Required")
                'frmCalibrate.ShowDialog()
                'Thread.Sleep(2000)
                'MessageBox.Show("New Settings have been detected for the device.  Press OK to update your device.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                'gUsbComm.configGft()
                'Thread.Sleep(1000)

            Else
                MessageBox.Show("Device reset Failed.", "Reset Device to Factory Defaults")
            End If

        ElseIf DialogResult.No Then
            ' Do nothing
        End If
    End Sub
End Class
﻿'#define ENABLE_DATA_LOGGER
Imports Microsoft.Win32.SafeHandles
Imports System.ComponentModel

'using ExcelLibrary.SpreadSheet;
'using ExcelLibrary.CompoundDocumentFormat;
Imports System.Collections.Generic
'using System.ComponentModel;
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Text
Imports System.Windows.Forms
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Diagnostics
Imports System.Runtime.InteropServices
Imports System.Collections
Imports System.Threading
Imports System.Timers
Imports System.Globalization
'Imports easyHID
Imports HidSharp
Imports gForceTracker.frmNotify

Public Class gUsbCommOld
    Public Shared filenameFirm As String = ""
    Friend Const APPLICATION_STRING As String = "gForce Firmware"

    Public Shared PROFILE_ALARM_ENABLE As Byte = Convert.ToByte("80", 16)
    Public Shared PROFILE_ALARM_AUDIO As Byte = Convert.ToByte("10", 16)
    Public Shared PROFILE_ALARM_VISUAL As Byte = Convert.ToByte("20", 16)
    Public Shared PROFILE_ALARM_INTERLOCK As Byte = Convert.ToByte("40", 16)
    Public Shared PROFILE_ALARM_ON As Byte = Convert.ToByte("1", 16)

    Friend Const MY_HID_REPORT_LENGTH As Byte = 64
    Friend Const ACC_SENSITIVITY_LSB As Double = 0.244
    Friend Const ACC_SENSITIVITY_LSB2 As Double = (200D / 128D)
    Friend Const GYRO_SENSITIVITY_LSB As Double = 0.07
    '70mdps/digit
    Friend Const MHID_CMD_RESPONSE_BIT As Byte = &H80

    Friend Const HID_REPORT_ID As Byte = 0
    Friend Const HID_INT_OUT_EP_SIZE As Byte = 64
    Friend Const HID_INT_IN_EP_SIZE As Byte = 41

    Friend Const ACC_ENTRY_MAGIC As Byte = &H48
    Public Const ACC_ENTRY_START As Byte = &H49
    Friend Const ACC_ENTRY_END As Byte = &H4A

    Private Enum update_firmware_state
        UPDATE_FIRM_STATE_START
        UPDATE_FIRM_STATE_WAIT
        UPDATE_FIRM_STATE_RD
        UPDATE_FIRM_STATE_RD_WAIT
        UPDATE_FIRM_STATE_IDL
        UPDATE_FIRM_STATE_ERR
        UPDATE_FIRM_STATE_TX

    End Enum
    Public Enum mhidCmd
        MHID_CMD_GET_ACC_ENTRY = 0
        MHID_CMD_GET_ACC_ENTRY_CNT
        '1
        MHID_CMD_GET_ACC_ENTRY_NEXT
        MHID_CMD_GET_ACC_ENTRY_FIRST
        MHID_CMD_GET_ACC_DATA_MAX
        MHID_CMD_GET_ACC_DATA_NEXT
        MHID_CMD_GET_ACC_DATA_FIRST
        MHID_CMD_GET_ACC_DATA_LAST
        MHID_CMD_SET_USR_PROFILE
        MHID_CMD_GET_USR_PROFILE
        MHID_CMD_ERASE_ACC_DATA
        '0xa
        MHID_CMD_ERASE_USR_PROFILE
        MHID_CMD_SET_TIME
        MHID_CMD_START_BOOT
        MHID_CMD_REBOOT
        'Bootldr & firmwre
        MHID_CMD_ERASE_FIRMWARE
        'Only used by bootloader 0xf
        MHID_CMD_PROG_FIRM
        'Only used by bootloader 0x10
        MHID_CMD_GET_BATTERY_STAT
        'abandoned
        MHID_CMD_GET_ACC_ENTRY_DATA_ALL
        MHID_CMD_GET_ACC_ENTRY_DATA_STOP
        MHID_CMD_TEST
        MHID_CMD_CHECKBAT
        '0x15
        MHID_CMD_DEBUG
        MHID_CMD_GET_GID
        MHID_CMD_GET_DEV_INFO
        'bootldr & firmware bootldr id: 0x1E firm:0x2D
        MHID_CMD_GET_TIME
        MHID_CMD_SET_SN
        MHID_CMD_GET_SN
        MHID_CMD_RUN_FIRM
        'bootldr
        MHID_CMD_LP_UNBIND
        MHID_CMD_CAL_START
        MHID_CMD_CAL_GET
    End Enum

    Public Enum mft_cmd

        MFT_CMD_NULL
        MFT_CMD_FLASH
        MFT_CMD_LED
        MFT_CMD_RTC
        MFT_CMD_GYRO
        MFT_CMD_ACCE
        MFT_CMD_BUZZ
        MFT_CMD_TORCH_LED
        MFT_CMD_DEFAULT_SET
        MFT_CMD_PROX
    End Enum

    'Public Shared MyHid As Hid
    Public Shared MyHid As HidSharp.HidDeviceLoader
    Private hFormWnd As IntPtr
    Public Shared gftStat As Integer = 0
    Public Shared waitEvent As New ManualResetEvent(False)
    Private Const rom_endaddr As Integer = &HEFFF

    Private Shared transferSuccess As Boolean = False
    Public Shared gftFinishedUpload As Boolean = True

    Public Sub New(Handle As IntPtr, vid As Integer, pid As Integer, ByRef theHid As HidSharp.HidDeviceLoader)
        Debug.WriteLine("STARTING NEW GFT THREAD")
        hFormWnd = Handle
        'MyHid = new Hid(hFormWnd, MY_VENDOR_ID, MY_PRODUCT_ID);
        'MyHid = New Hid(hFormWnd, vid, pid)
        MyHid = theHid

        'MyHid.usbEvent += New Hid.usbEventsHandler(AddressOf usbEvent_receiver)
        'AddHandler MyHid.usbEvent, New Hid.usbEventsHandler(AddressOf usbEvent_receiver)

    End Sub

    Private Sub usbEvent_receiver(o As Object, e As EventArgs)

        'MessageHelper msg = new MessageHelper();
        Dim result As Integer = 0
        'First param can be null
        Dim hWnd As IntPtr = hFormWnd
        Try
            ' Check the status of the USB device and update the form accordingly
            If MyHid.deviceAttached Then
                ' Device is currently attached  

                result = gNativeMethod.sendWindowsMessage(CInt(hWnd), gNativeMethod.WM_USER + 2, 0, 0)
            Else
                ' Device is currently unattached
                ' Update the status label

                result = gNativeMethod.sendWindowsMessage(CInt(hWnd), gNativeMethod.WM_USER + 3, 0, 0)

            End If
        Catch ex As Exception

            gUtility.DisplayException("easyHID", ex)
        End Try
    End Sub

    Public Shared Function resetGft() As Boolean
        If currentGftProfile.mntLoc = 0 And currentGftProfile.iLocation(0) = 0 And currentGftProfile.iLocation(1) = 0 And currentGftProfile.iLocation(2) = 0 And currentGftProfile.iLocation(3) = 0 And (currentGftProfile.sName = "GFT" Or currentGftProfile.sName = "") Then
            Return False
        Else
            getProfile()
            Dim tempSerial(6) As Byte
            Dim tempsSerial As String
            tempsSerial = currentGftProfile.sSerial
            Array.Copy(currentGftProfile.bSerial, 0, tempSerial, 0, 6)
            currentGftProfile = New gftProfile
            currentGftProfile.sSerial = tempsSerial
            Array.Copy(tempSerial, 0, currentGftProfile.bSerial, 0, 6)
            Thread.Sleep(5000)
            configGft()
            Return True
        End If
    End Function

    Public Shared Function configGft() As Boolean

        '  byte alarmMode = 0;
        ' byte cnt = 0;
        Dim tmpByteArray As Char() = New Char(15) {}
        Dim nread As Integer = 0
        Dim tmpB As Byte()
        Dim result As Boolean = False
        If Not currentGftProfile Is Nothing Then
            Try
                Dim outputReportBuffer As Byte() = New [Byte](64) {}
                Dim inputReportBuffer As Byte() = New [Byte](64) {}

                outputReportBuffer(0) = 0
                outputReportBuffer(1) = CByte(gUsbCommOld.mhidCmd.MHID_CMD_SET_USR_PROFILE)
                outputReportBuffer(2) = 0


                outputReportBuffer(3 + 0) = currentGftProfile.bMagic

                Array.Copy(currentGftProfile.bSerial, 0, outputReportBuffer, 3 + 1, 6)

                outputReportBuffer(3 + 7) = currentGftProfile.iCloudThreshold
                outputReportBuffer(3 + 8) = currentGftProfile.iCloudRecordThreshold
                outputReportBuffer(3 + 9) = currentGftProfile.bCloudAlarmMode
                outputReportBuffer(3 + 10) = currentGftProfile.bWirelessEnabled 'currentGftProfile.bCloudWirelessEnabled ' keep the GFT's setting for wireless
                Dim nameBytes As Byte() = Nothing
                If currentGftProfile.sCloudName Is Nothing Then
                    currentGftProfile.sCloudName = ""
                End If
                nameBytes = ConvertStringToByteArray(currentGftProfile.sCloudName)
                ReDim Preserve nameBytes(20)
                Array.Copy(nameBytes, 0, outputReportBuffer, 3 + 11, 20)
                outputReportBuffer(3 + 31) = currentGftProfile.iCloudPlayerNumber
                'outputReportBuffer(3 + 43) = currentGftProfile.iCloudSports
                tmpB = BitConverter.GetBytes(Convert.ToInt16(currentGftProfile.iLocation(0)))
                outputReportBuffer(3 + 32) = tmpB(0)
                outputReportBuffer(3 + 33) = tmpB(1)
                tmpB = BitConverter.GetBytes(Convert.ToInt16(currentGftProfile.iLocation(1)))
                outputReportBuffer(3 + 34) = tmpB(0)
                outputReportBuffer(3 + 35) = tmpB(1)
                tmpB = BitConverter.GetBytes(Convert.ToInt16(currentGftProfile.iLocation(2)))
                outputReportBuffer(3 + 36) = tmpB(0)
                outputReportBuffer(3 + 37) = tmpB(1)
                tmpB = BitConverter.GetBytes(Convert.ToInt16(currentGftProfile.iLocation(3)))
                outputReportBuffer(3 + 38) = tmpB(0)
                outputReportBuffer(3 + 39) = tmpB(1)



                'outputReportBuffer(3 + 40) = CByte(If(currentGftProfile.bMountInside, 1, 0))
                outputReportBuffer(3 + 40) = currentGftProfile.mntLoc ' Store mntLoc instead as it contains the bMountInside information
                outputReportBuffer(3 + 41) = CByte(If(currentGftProfile.bCloudTestMode, 1, 0))
                outputReportBuffer(3 + 42) = CByte(If(currentGftProfile.bProximityEnabled, 1, 0))
                transferSuccess = False
                result = MyHid.OutputReports(outputReportBuffer)
                Debug.WriteLine("Send Output Report -> configGFT()")
                If result Then
                    If MyHid.inputReports(inputReportBuffer, nread) Then
                        If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_SET_USR_PROFILE) Or MHID_CMD_RESPONSE_BIT) Then
                            result = False
                        End If
                    Else
                        result = False
                    End If
                End If
            Catch ex As Exception
                gUtility.DisplayException("Config GFT", ex)
            End Try
        End If
        needsSettings = False

        Return result
    End Function

    Public Shared Sub setAlarmMode()
        Dim alarmMode As Byte = 0
        If currentGftProfile.iCloudAlarmEnable = 1 Then
            alarmMode = alarmMode Or PROFILE_ALARM_ENABLE
            If currentGftProfile.iCloudAlarmAudio = 1 Then
                alarmMode = alarmMode Or PROFILE_ALARM_AUDIO
            End If
            If currentGftProfile.iCloudAlarmVisual = 1 Then
                alarmMode = alarmMode Or PROFILE_ALARM_VISUAL
            End If
            If currentGftProfile.iCloudAlarmInterlock = 1 Then
                alarmMode = alarmMode Or PROFILE_ALARM_INTERLOCK
            End If
        Else
            alarmMode = 0
        End If
        currentGftProfile.bCloudAlarmMode = alarmMode
    End Sub

    Private Shared Function ConvertStringToByteArray(sIn As String) As Byte()
        Dim hexString As String = Nothing
        hexString = String_To_Hex(sIn)
        If hexString.Length Mod 2 <> 0 Then
            Throw New ArgumentException([String].Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString))
        End If

        Dim HexAsBytes As Byte() = New Byte(hexString.Length \ 2 - 1) {}
        For index As Integer = 0 To HexAsBytes.Length - 1
            Dim byteValue As String = hexString.Substring(index * 2, 2)
            HexAsBytes(index) = Byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture)
        Next

        Return HexAsBytes
    End Function

    Private Shared Function String_To_Hex(ByVal sIn As String) As String
        Dim encText As New System.Text.UTF8Encoding
        Dim outByte() As Byte = Nothing

        Dim sVal As String
        Dim sHex As String = ""
        While sIn.Length > 0
            sVal = Conversion.Hex(Strings.Asc(sIn.Substring(0, 1).ToString()))
            sIn = sIn.Substring(1, sIn.Length - 1)
            sHex = sHex & sVal
        End While

        Return sHex
    End Function

    Private Shared Function ToBytes(hexLine As String, ByRef hexRecord As Byte()) As Integer
        hexRecord = Nothing

        '            if (hexLine[0] != ':')
        '                return (-1);

        Dim lineLen As Integer = hexLine.Length

        '            if ((lineLen % 2) != 1)
        '                return (-2);			// Wrong length

        hexRecord = New Byte(lineLen \ 2 - 1) {}

        Dim line As String = hexLine.ToUpper()
        Dim i As Integer, len As Integer = 0
        For i = 0 To lineLen - 2 Step 2
            Dim b1 As Byte = CByte(AscW(line(i)))
            Dim b0 As Byte = CByte(AscW(line(i + 1)))

            If (AscW("0") <= b1) AndAlso (b1 <= AscW("9")) Then
                b1 -= CByte(AscW("0"))
            ElseIf (AscW("A") <= b1) AndAlso (b1 <= AscW("F")) Then
                b1 = CByte(10 + (b1 - AscW("A")))
            Else
                Return (-2)
            End If

            If (AscW("0") <= b0) AndAlso (b0 <= AscW("9")) Then
                b0 -= CByte(AscW("0"))
            ElseIf (AscW("A") <= b0) AndAlso (b0 <= AscW("F")) Then
                b0 = CByte(10 + (b0 - AscW("A")))
            Else
                Return (-2)
            End If

            hexRecord(len) = CByte((b1 << 4) Or b0)
            len += 1
        Next
        '
        '            byte checkSum = 0;
        '            for (i = 0; i < len; i++)
        '                checkSum += hexRecord[i];
        '            checkSum = (byte)~checkSum;
        '
        '            if (checkSum != 0x00)
        '                return (-3);
        '            


        Return len
    End Function
    Public Shared Function program() As String
        'BackgroundWorker worker = sender as BackgroundWorker;
        Dim outputReportBuffer As Byte() = New [Byte](64) {}
        Dim inputReportBuffer As Byte() = New [Byte](64) {}
        Dim ImageLen As UShort
        Dim OneLineRecord As String = " "
        Dim StartAddress As UShort, i As UShort
        Dim temp As String
        Dim succ As Boolean = False
        Dim nread As Integer = 0
        'string retstr = null;
        Dim RecordLen As Byte() = Nothing
        Dim hexRecord As Byte() = Nothing
        Dim firmValid = False

        Dim sw As StreamReader = File.OpenText(frmNotify.sFirmwareFile)
        While MyHid.deviceAttached
            OneLineRecord = sw.ReadLine()
            If OneLineRecord Is Nothing Then
                Exit While ' Not a valid firmware file
            End If
            If OneLineRecord.Length < 2 Then
                Exit While ' Not a valid firmware file
            End If
            temp = OneLineRecord.Substring(0, 2)
            If OneLineRecord.Substring(0, 2) = "S1" Then
                firmValid = True
                temp = OneLineRecord.Substring(2, 2)

                Dim len1 As Integer = ToBytes(temp, RecordLen)

                ImageLen = CUShort(RecordLen(0))
                temp = OneLineRecord.Substring(2, (ImageLen) * 2)

                Dim len As Integer = ToBytes(temp, hexRecord)
                StartAddress = CUShort(CUShort(hexRecord(2)) Or (CUShort(hexRecord(1)) << 8))
                If (StartAddress < rom_endaddr + 1) Then

                    '                    If (StartAddress >= &H1960) AndAlso (StartAddress < rom_endaddr + 1) Then

                    If (StartAddress + ImageLen - 3) > rom_endaddr Then
                        ImageLen = CUShort(rom_endaddr - StartAddress + 1 + 3)
                    End If

                    If ImageLen - 3 <= 55 Then
                        ' we can have 64-4-1

                        For i = 0 To ImageLen - 4
                            outputReportBuffer(i + 5) = hexRecord(i + 3)
                        Next

                        outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_PROG_FIRM)
                        outputReportBuffer(2) = CByte(CByte(ImageLen - 3))
                        outputReportBuffer(3) = CByte((StartAddress And &HFF00) >> 8)
                        outputReportBuffer(4) = CByte(StartAddress And &HFF)

                        succ = MyHid.OutputReports(outputReportBuffer)
                        If succ Then
                            succ = MyHid.inputReports(inputReportBuffer, nread)
                            If succ Then
                                If (inputReportBuffer(1) = CByte(CInt(mhidCmd.MHID_CMD_PROG_FIRM) Or MHID_CMD_RESPONSE_BIT)) AndAlso (inputReportBuffer(3) = 0) Then
                                    ' worker.ReportProgress(StartAddress, ImageLen - 3);
                                Else
                                    succ = False
                                    Exit While
                                End If
                            Else
                                succ = False
                                Exit While
                            End If
                        Else
                            succ = False
                            Exit While
                        End If
                    Else
                        succ = False
                        Exit While
                    End If
                Else
                    succ = False
                    Exit While
                End If
            ElseIf OneLineRecord.Substring(0, 2) = "S9" Then
                'worker.ReportProgress(100,0);
                'm_progress.Value = m_progress.Maximum;
                succ = True
                Exit While
            End If
        End While
        sw.Close()
        If Not firmValid Then
            Return "badfile"
        End If
        Return "finished"
    End Function

    Public Shared Function sendCmd(cmd As Byte, subCmd As Byte) As String
        Dim result As String = "failed"
        Dim nread As Integer = 0
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}
            outputReportBuffer(0) = HID_REPORT_ID
            outputReportBuffer(1) = cmd
            outputReportBuffer(2) = subCmd
            transferSuccess = False

            If Not MyHid.OutputReports(outputReportBuffer) Then
                Return "failed"
            End If
            Thread.Sleep(800)
            If Not MyHid.inputReports(inputReportBuffer, nread) Then
                Return "failed"
            End If

            If inputReportBuffer(1) = CByte(cmd Or MHID_CMD_RESPONSE_BIT) Then
                result = "passed"

            End If
        Catch ex As Exception

            gUtility.DisplayException("Send Command", ex)
        End Try
        Return result

    End Function

    Friend Structure GFT_TEST_RESULT
        Friend g As Byte

    End Structure

    Private gftTestResult As GFT_TEST_RESULT


    Public Shared Function sentTestCmd(subCmd As mft_cmd, value As Byte) As String
        ' USR_PROFILE usr = new USR_PROFILE();
        'string result = "failed";

        Dim testR As String = "Failed"
        Dim nread As Integer = 0
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}

            outputReportBuffer(0) = 0
            outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_TEST)
            outputReportBuffer(2) = CByte(subCmd)
            outputReportBuffer(3) = CByte(value)
            transferSuccess = False
            If Not MyHid.deviceAttached Then
                testR = "Check USB cable."

                Return testR
            End If
            MyHid.OutputReports(outputReportBuffer)
            'Thread.Sleep(1);
            'transferInProgress = true;
            MyHid.inputReports(inputReportBuffer, nread)
            'transferInProgress = false;


            If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_TEST) Or MHID_CMD_RESPONSE_BIT) Then


                Select Case subCmd

                    Case mft_cmd.MFT_CMD_FLASH

                        If inputReportBuffer(3 + 1) = 0 Then
                            testR = "Passed"
                        Else
                            testR = "Failed"
                        End If
                        Exit Select
                    Case mft_cmd.MFT_CMD_GYRO
                        If inputReportBuffer(3 + 1) = 0 Then
                            testR = "Passed"
                        Else
                            testR = "Failed"
                        End If
                        Exit Select
                    Case mft_cmd.MFT_CMD_ACCE
                        If inputReportBuffer(3 + 1) = 0 Then
                            testR = "Passed"
                        Else
                            testR = "Failed"
                        End If

                        Exit Select

                    Case mft_cmd.MFT_CMD_RTC
                        If inputReportBuffer(3 + 1) = 0 Then
                            testR = "Passed"
                        Else
                            testR = "Failed"
                        End If
                        Exit Select
                    Case mft_cmd.MFT_CMD_LED
                        If inputReportBuffer(3 + 1) = 0 Then
                            testR = "Passed"
                        Else
                            testR = "Failed"
                        End If
                        Exit Select
                    Case mft_cmd.MFT_CMD_BUZZ
                        If inputReportBuffer(3 + 1) = 0 Then
                            testR = "Passed"
                        Else
                            testR = "Failed"
                        End If
                        Exit Select
                    Case mft_cmd.MFT_CMD_TORCH_LED
                        If inputReportBuffer(3 + 1) = 0 Then
                            testR = "Passed"
                        Else
                            testR = "Failed"
                        End If
                        Exit Select
                    Case mft_cmd.MFT_CMD_DEFAULT_SET


                        Exit Select
                    Case mft_cmd.MFT_CMD_PROX
                        testR = String.Format("{0:X2}", inputReportBuffer(3 + 1))
                        Exit Select


                End Select
            Else
                'communication error

                testR = "Usb error"
            End If


        Catch ex As Exception

            gUtility.DisplayException("Get Profile", ex)
        End Try
        Return testR
    End Function

    Public Shared Function sendCmdGftVersion(cmd As Byte, subCmd As Byte) As Boolean

        Dim nread As Integer = 0
        Dim result As Boolean = False
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}
            outputReportBuffer(0) = 0
            outputReportBuffer(1) = cmd
            outputReportBuffer(2) = subCmd
            transferSuccess = False

            result = MyHid.OutputReports(outputReportBuffer)
            If result Then

                result = MyHid.inputReports(inputReportBuffer, nread)

                If result = True Then
                    If inputReportBuffer(1) = CByte(cmd Or MHID_CMD_RESPONSE_BIT) Then
                        If CByte(inputReportBuffer(7)) = &H1E Then
                            currentGFTProfile.bootMajor = inputReportBuffer(3)
                            currentGFTProfile.bootMinor = inputReportBuffer(4)
                            currentGFTProfile.hwMajor = inputReportBuffer(5)
                            currentGFTProfile.hwMinor = inputReportBuffer(6)
                            currentGFTProfile.gftWhereAmI = GFT.GFT_STATE_BOOT
                        ElseIf CByte(inputReportBuffer(7)) = &H2D Then
                            currentGFTProfile.firmMajor = inputReportBuffer(3)
                            currentGFTProfile.firmMinor = inputReportBuffer(4)
                            currentGFTProfile.hwMajor = inputReportBuffer(5)
                            currentGFTProfile.hwMinor = inputReportBuffer(6)
                            currentGFTProfile.gftWhereAmI = GFT.GFT_STATE_FIRM
                        End If
                    End If
                End If

            End If
        Catch ex As Exception

            gUtility.DisplayException("Send Version Command", ex)
        End Try
        Return result

    End Function
    Public Shared Function sendCmdBattery(cmd As Byte, subCmd As Byte) As Integer
        Dim mvol As Integer = 0
        Dim result As Boolean = False
        Dim nread As Integer = 0
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}
            outputReportBuffer(0) = 0
            outputReportBuffer(1) = cmd
            outputReportBuffer(2) = subCmd
            transferSuccess = False

            If Not MyHid.OutputReports(outputReportBuffer) Then
                Return mvol
            End If


            If Not MyHid.inputReports(inputReportBuffer, nread) Then
                Return mvol
            End If


            If inputReportBuffer(1) = CByte(cmd Or MHID_CMD_RESPONSE_BIT) Then
                ' result = inputReportBuffer[3];
                If inputReportBuffer(2) = CByte(&H1) Then
                    mvol = 100
                Else
                    mvol = inputReportBuffer(4) * 256 + inputReportBuffer(3)
                    If mvol < 3200 Then
                        mvol = 3200
                    End If
                    mvol = (mvol - 3200) * 100 \ (4220 - 3200)
                    If mvol > 100 Then
                        mvol = 100

                    End If
                End If


            End If
        Catch ex As Exception

            gUtility.DisplayException("Send Battery Command", ex)
        End Try
        Return mvol

    End Function
    Private Sub btnStartBoot_Click(sender As Object, e As EventArgs)
        ' bool ret = true;
        sendCmd(CByte(mhidCmd.MHID_CMD_START_BOOT), 0)
        'return ret;
    End Sub

    Private Sub btnSyncTime_Click(sender As Object, e As EventArgs)

    End Sub


    Public Shared Function AccDecode(c As Byte) As Double
        'if((curGFT.hwMajor ==0x1) && (curGFT.hwMinor == 0))
        Return (ACC_SENSITIVITY_LSB2 * (c - 128))

        '  return (ACC_SENSITIVITY_LSB * (c * 4 - 512));
    End Function
    Public Shared Function AccDecode2(c As Byte) As Double
        Return (ACC_SENSITIVITY_LSB2 * (c - 128))
    End Function
    Public Shared Function gyroDecode(h As Byte, l As Byte) As Double
        Return (GYRO_SENSITIVITY_LSB * CShort((CShort(h) << 8) Or l))
    End Function
    Public Shared Function lgDecode(h As Byte, l As Byte) As Double
        Return (0.002F * (CShort((CShort(h) << 8) Or l) >> 4))
    End Function

    'DBRM-Public Shared sessionData As gDatabase.SESSION_DATA
    Friend Const LOCATION_CAL_CNT_THRESHOLD As Byte = 9
    Friend Const LOCATION_CAL_HIGH_G_CUNT As Byte = 1
    'uploading hit data from GFT to PC use usb cable
    Public Shared Function uploadData(sender As Object, e As DoWorkEventArgs) As Char

        ' Calculate how far the GFT's clock is
        Dim gftTime, utcTime As DateTime
        utcTime = getUTC()
        gftTime = getGftTime()
        Dim timeDiff As TimeSpan = utcTime - gftTime

        Dim sessionStartTime As DateTime
        Dim sessionEndTime As DateTime
        Dim sessionStartTimeF As Integer = 0
        Dim sessionEndTimeF As Integer = 0
        Dim lastTimeStamp As DateTime
        Dim repCmd As Integer = 0
        '        Dim myDateTime As DateTime
        Dim outTime As DateTime
        Dim hasData As Boolean = False
        Dim bGettMyPoint As Boolean = False
        ' IMPACT_DATA iData = impactData;

        Dim colSession As New Collection()

        Dim aSessions(0) As gftSession
        Dim aImpacts(0) As gftImpactData

        Dim sData As New gftSession
        'Dim iData As gftImpactData
        gftStat = 1
        'DBRM-Dim lstMyImpact As New List(Of gDatabase.IMPACT_DATA)()

        Dim azmuthElev As Double() = New Double(1) {}
        'hold max point
        Dim myPoint As Double() = New Double(2) {}
        Dim maxIndex As Integer = 0
        Dim readyToStore As Boolean = False
        Dim builder As New StringBuilder()
        Dim cnt As UInteger = 0
        Dim result As Boolean = False
        'string tmp;
        Dim x As Double, y As Double, z As Double, v As Double
        '        Dim w As Byte, wt As Byte
        Dim i As UInteger = 0, j As UInteger = 0, k As UInteger = 0
        Dim c As Byte = 0
        ' byte mySessionID = 0;
        ' bool needRecordSession =false;
        Dim mLinMax As Double = 0
        Dim sessionLen As TimeSpan
        sData.iImpact = 0
        sData.iWTH = 0
        sData.iAlarm = 0
        sData.iActiveTime = 0
        Dim myShort As UShort = 0
        Dim locCnt As Integer = 0
        Dim nread As Integer = 0
        Dim my40 As Double() = New Double(39) {}
        Dim bw As BackgroundWorker = TryCast(sender, BackgroundWorker)
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}
            outputReportBuffer(0) = 0
            outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_GET_ACC_ENTRY_CNT)
            outputReportBuffer(2) = 0
            transferSuccess = False

            result = MyHid.OutputReports(outputReportBuffer)
            If result Then
                result = MyHid.inputReports(inputReportBuffer, nread)
                If Not result Then
                    Return "i"c
                End If
            Else
                Return "o"c
            End If

            If inputReportBuffer(1) = CByte(CInt(mhidCmd.MHID_CMD_GET_ACC_ENTRY_CNT) Or MHID_CMD_RESPONSE_BIT) Then
                bw.ReportProgress(1, "Retrieving Session Data from Device - Do Not Disconnect")
                cnt = CUInt(inputReportBuffer(3)) * 256 Or inputReportBuffer(4)
            Else
                cnt = 0
            End If
            ' tmp = Convert.ToString(cnt);
            'If cnt > 2 Then

            'End If
            If cnt = 0 Then
                Return "0"c
            End If

            'bw.ReportProgress(1, "Downloading Data -- Do Not Disconnect")

            Dim uploadEnd As Boolean = False
            Dim colImpacts As New Collection
            For i = 0 To cnt - 1

                If bw.CancellationPending Then Exit For
                ' Our rety loop..
                'frmMain.updateProgBar(Math.Round((i + 1 / cnt) * 100))
                For f = 0 To 10
                    If f = 1 Then
                        Debug.WriteLine("Retry")
                    End If
                    If f = 10 Then
                        Return "t"c ' We tried enough times.. give up.
                    End If

                    Dim iData As New gftImpactData
                    iData.bDataX = New [Byte](119) {}
                    iData.bDataY = New [Byte](119) {}
                    iData.bDataZ = New [Byte](119) {}
                    ' iData.dataW = new Byte[120];

                    iData.iLinX = 0
                    iData.iLinY = 0
                    iData.iLinZ = 0
                    iData.iLinR = 0
                    myPoint(0) = 0
                    myPoint(1) = 0
                    myPoint(2) = 0
                    locCnt = 0
                    iData.bRDataX = New [Byte](63) {}
                    iData.bRDataY = New [Byte](63) {}
                    iData.bRDataZ = New [Byte](63) {}
                    bGettMyPoint = False
                    j = 0
                    While j < 11

                        outputReportBuffer(0) = 0
                        outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_GET_ACC_ENTRY)
                        outputReportBuffer(2) = CByte(j + 1)
                        outputReportBuffer(3) = CByte(i \ 256)
                        outputReportBuffer(4) = CByte(i Mod 256)
                        If Not MyHid.OutputReports(outputReportBuffer) Then
                            Return "O"c
                        Else
                            result = MyHid.inputReports(inputReportBuffer, nread)
                            If Not result Then
                                Return "I"c
                            End If
                        End If

                        'get data 
                        If inputReportBuffer(1) <> CByte(CInt(mhidCmd.MHID_CMD_GET_ACC_ENTRY) Or MHID_CMD_RESPONSE_BIT) Then
                            'something is fishy here
                            Continue While
                        End If


                        If j = 0 Then
                            If CByte(inputReportBuffer(2)) = 0 Then
                                uploadEnd = True
                                '                            bw.ReportProgress(2, "Data upload failed")
                                Exit While
                            Else

                                If CByte(inputReportBuffer(3)) = gUsbCommOld.ACC_ENTRY_START Then
                                    sessionStartTimeF = 1
                                    sessionEndTimeF = 0
                                    If gUtility.ValidateTime(outTime, inputReportBuffer(4), inputReportBuffer(5), inputReportBuffer(6), inputReportBuffer(7), inputReportBuffer(8), _
                                        inputReportBuffer(9), myShort) Then
                                        sessionStartTime = outTime
                                        If getUTC() > sessionStartTime.Add(timeDiff) Then ' If the device lost it's time after the session was recorded, we don't want to put it into the future
                                            sessionStartTime = sessionStartTime.Add(timeDiff)
                                        End If
                                        lastTimeStamp = sessionStartTime
                                    Else
                                        ' comment this out for now...
                                        'bw.ReportProgress(2, "Upload data has invalid Start date time")
                                        'Return "t"c
                                        ' do this instead
                                        'sessionStartTime = outTime
                                        If f < 9 Then
                                            Continue For ' Try again
                                        Else
                                            sessionStartTime = lastTimeStamp 'New DateTime(2000, 1, 1)
                                        End If


                                    End If
                                    sData.dOnTime = sessionStartTime

                                        Exit While
                                ElseIf (CByte(inputReportBuffer(3)) = ACC_ENTRY_END) Then
                                        If sessionEndTimeF = 0 Then
                                            sessionEndTimeF = 1
                                            myShort = gUtility.byteToShort(inputReportBuffer(10), inputReportBuffer(11))

                                        If gUtility.ValidateTime(outTime, inputReportBuffer(4), inputReportBuffer(5), inputReportBuffer(6), inputReportBuffer(7), inputReportBuffer(8), _
                                            inputReportBuffer(9), myShort) Then
                                            sessionEndTime = outTime
                                            If getUTC() > sessionEndTime.Add(timeDiff) Then ' If the device lost it's time after the session was recorded, we don't want to put it into the future
                                                sessionEndTime = sessionEndTime.Add(timeDiff)
                                            End If
                                            lastTimeStamp = sessionEndTime
                                        Else
                                            'take last impact time as session end time
                                            ' comment this out for now...
                                            'bw.ReportProgress(2, "Upload data has invalid End date time")
                                            'Return "T"c
                                            ' do this instead
                                            'sessionEndTime = outTime
                                            If f < 9 Then
                                                Continue For ' Try again
                                            Else
                                                sessionEndTime = lastTimeStamp 'New DateTime(2000, 1, 1)
                                            End If
                                        End If
                                        'If (sData.iImpact > 0) AndAlso (sessionStartTimeF = 1) Then
                                        sData.dOffTime = sessionEndTime
                                        sessionLen = (sData.dOffTime - sData.dOnTime)
                                        sData.iDuration = CInt(Math.Truncate(sessionLen.TotalMinutes))
                                        If (sData.iImpact > 0 Or sData.iDuration >= 5) AndAlso (sessionStartTimeF = 1) Then
                                            sData.iWTH = sData.iWTH - sData.iAlarm
                                            'session time 
                                            sData.dDate = sData.dOnTime

                                            If sData.iDuration = 0 Then sData.iDuration = 1
                                            sData.iUID = GFT.curUser.uid
                                            sData.iThreshold = currentGftProfile.bThresholdAlarm
                                            'sData.iSessionID = gDatabase.SessionID
                                            readyToStore = True
                                            hasData = True
                                            'bw.ReportProgress(2, "")
                                        End If
                                        sData.iActiveTime = Math.Round(BitConverter.ToInt32(New Byte(3) {inputReportBuffer(21), inputReportBuffer(20), inputReportBuffer(19), inputReportBuffer(18)}, 0) / 3000)
                                    End If
                                    Exit While
                                End If

                                k = 0
                                mLinMax = (Math.Sqrt(gUtility.byteToShort(inputReportBuffer(16), inputReportBuffer(17))) * (ACC_SENSITIVITY_LSB2))
                                'CDbl(Math.Sqrt(gUtility.byteToShort(CByte(inputReportBuffer(16)), CByte(inputReportBuffer(17)))) * (ACC_SENSITIVITY_LSB2))

                                iData.iGForce = mLinMax

                                'impact
                                'iData.sid = gDatabase.SessionID + 1
                                'gDatabase.EventID += 1
                                'iData.iEID = gDatabase.EventID
                                myShort = gUtility.byteToShort(inputReportBuffer(10), inputReportBuffer(11))

                                If gUtility.ValidateTime(outTime, inputReportBuffer(4), inputReportBuffer(5), inputReportBuffer(6), inputReportBuffer(7), inputReportBuffer(8), _
                                    inputReportBuffer(9), myShort) Then
                                    iData.dTime = outTime
                                    If getUTC() > iData.dTime.Add(timeDiff) Then ' If the device lost it's time after the session was recorded, we don't want to put it into the future
                                        iData.dTime = iData.dTime.Add(timeDiff)
                                    End If
                                    'Debug.WriteLine(iData.dTime & " :: " & iData.dTime.Millisecond)

                                    lastTimeStamp = iData.dTime

                                Else

                                    'bw.ReportProgress(2, "Data upload has invalid date time")
                                    'Return "T"c

                                    ' temporarily we are ignoring this error and proceeding as is...
                                    'iData.dTime = outTime
                                    If f < 9 Then
                                        Continue For ' Try again
                                    Else
                                        iData.dTime = lastTimeStamp ' New DateTime(2000, 1, 1)
                                    End If
                                End If
                                ' Let keep count AFTER a possible Continue
                                sData.iImpact = sData.iImpact + 1
                                If mLinMax >= currentGftProfile.bThresholdAlarm Then
                                    sData.iAlarm = sData.iAlarm + 1
                                End If
                                If mLinMax >= (currentGftProfile.bThresholdAlarm) * 0.9 Then
                                    sData.iWTH = sData.iWTH + 1
                                End If

                                iData.dTimeMS = iData.dTime.Millisecond
                                iData.iRotX = CShort(gyroDecode(inputReportBuffer(18), inputReportBuffer(19)))
                                iData.iRotY = CShort(gyroDecode(inputReportBuffer(20), inputReportBuffer(21)))
                                iData.iRotZ = CShort(gyroDecode(inputReportBuffer(22), inputReportBuffer(23)))

                                iData.iRotR = CShort(Math.Sqrt(iData.iRotX * iData.iRotX + iData.iRotY * iData.iRotY + iData.iRotZ * iData.iRotZ))
                                'CShort(Math.Truncate(Math.Sqrt(iData.iRotX * iData.iRotX + iData.iRotY * iData.iRotY + iData.iRotZ * iData.iRotZ)))

                                maxIndex = CByte(inputReportBuffer(15))
                            End If
                        ElseIf j < 7 Then
                            If CByte(inputReportBuffer(2)) = 0 Then
                                bw.ReportProgress(2, "Data upload has error")
                            Else
                                For c = 0 To 19
                                    builder.Length = 0
                                    x = AccDecode(CByte(inputReportBuffer(c * 3 + 3)))
                                    y = AccDecode(CByte(inputReportBuffer(c * 3 + 4)))
                                    z = AccDecode(CByte(inputReportBuffer(c * 3 + 5)))
                                    v = System.Math.Sqrt(x * x + y * y + z * z)

                                    ' low g use first sample after recording, high g take 3 sample average
                                    If bGettMyPoint = False AndAlso v > currentGftProfile.bThresholdR Then 'GFT.gftProfile.tR
                                        If (currentGftProfile.bThresholdR < LOCATION_CAL_CNT_THRESHOLD) Then 'GFT.gftProfile.tR
                                            myPoint(0) = x
                                            myPoint(1) = y
                                            myPoint(2) = z
                                            bGettMyPoint = True
                                        Else
                                            If (locCnt < LOCATION_CAL_HIGH_G_CUNT) Then
                                                locCnt += 1
                                                myPoint(0) += x
                                                myPoint(1) += y
                                                myPoint(2) += z

                                            Else

                                                myPoint(0) /= LOCATION_CAL_HIGH_G_CUNT
                                                myPoint(1) /= LOCATION_CAL_HIGH_G_CUNT
                                                myPoint(2) /= LOCATION_CAL_HIGH_G_CUNT
                                                bGettMyPoint = True

                                            End If
                                        End If
                                    End If

#If True Then
                                    If k = maxIndex Then
                                        iData.iLinX = Math.Round(x, 2)
                                        iData.iLinY = Math.Round(y, 2)
                                        iData.iLinZ = Math.Round(z, 2)
                                        iData.iLinR = Math.Round(v, 2)
                                    End If

                                    iData.bDataX(c + (j - 1) * 20) = CByte(inputReportBuffer(c * 3 + 3))
                                    iData.bDataY(c + (j - 1) * 20) = CByte(inputReportBuffer(c * 3 + 4))
                                    iData.bDataZ(c + (j - 1) * 20) = CByte(inputReportBuffer(c * 3 + 5))

                                    If (k Mod 3 = 0) Then
                                        my40(k \ 3) = v
                                    End If
#End If
                                    k += 1
                                Next

                            End If
                        Else
                            For c = 0 To 7
                                ' iData.rdataX  //for rotation data
                                iData.bRDataX(c * 2 + (j - 7) * 16) = inputReportBuffer(c * 6 + 3)
                                iData.bRDataX(c * 2 + 1 + (j - 7) * 16) = inputReportBuffer(c * 6 + 4)

                                iData.bRDataY(c * 2 + (j - 7) * 16) = inputReportBuffer(c * 6 + 5)
                                iData.bRDataY(c * 2 + 1 + (j - 7) * 16) = inputReportBuffer(c * 6 + 6)

                                iData.bRDataZ(c * 2 + (j - 7) * 16) = inputReportBuffer(c * 6 + 7)

                                iData.bRDataZ(c * 2 + 1 + (j - 7) * 16) = inputReportBuffer(c * 6 + 8)
                                ' if(j == 10)
                                '    readySaveImpact = true;

                            Next
                        End If
                        If j = 10 Then
                            'readySaveImpact) //store to database
                            'gFirFilter.fir400(ref my40, 40);
                            iData.iGSI = Math.Round(GSI(my40), 1)
                            iData.iHIC = Math.Round(HIC15(my40), 1)
                            'myPoint[0] = iData.linX;
                            'myPoint[1] = iData.linY;
                            'myPoint[2] = iData.linZ;
                            'if ((tbUserGID.Text == GFT_J13) || (tbUserGID.Text == GFT_J3) || (tbUserGID.Text == GFT_J7))
                            '    azmuthElev = getAzmuthJaw(myPoint);
                            'else if (tbUserGID.Text == GFT_J12)
                            '    azmuthElev = getAzmuthYasZ(myPoint);
                            'else

                            'azmuthElev = gReport.getAzmuthTopG2(myPoint);
                            'azmuthElev = getAzmuthAuto(myPoint)
                            'iData.iAzimuth = Math.Round(azmuthElev(0), 2)
                            'If (currentGftProfile.bMountInside) Then
                            ' iData.iElevation = -Math.Round(azmuthElev(1), 2)
                            'Else
                            'iData.iElevation = Math.Round(azmuthElev(1), 2)
                            'End If

                            azmuthElev = getAzmuthAuto(myPoint)
                            iData.iAzimuth = Math.Round(azmuthElev(0), 2)

                            If (currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_INOUTMSK) > 0 Then
                                iData.iElevation = -Math.Round(azmuthElev(1), 2)
                            Else
                                iData.iElevation = Math.Round(azmuthElev(1), 2)
                                iData.iAzimuth = 360 - iData.iAzimuth
                            End If

                            If (currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_VER) <> 0 Then
                                iData.iAzimuth = iData.iAzimuth + 270
                                iData.iAzimuth = iData.iAzimuth Mod 360
                            End If
#If True Then
                            If (currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK) > 0 Then
                                iData.iAzimuth = (iData.iAzimuth + 180) Mod 360
                            End If

                            If (currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_ZONE_LEFT) > 0 Then
                                If (iData.iAzimuth > 250) Then
                                    iData.iAzimuth = iData.iAzimuth - 30
                                End If
                            ElseIf (currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_ZONE_CROWN) > 0 Then
                                ' Nothing to do?
                            End If



#End If


                            'save impact to database
                            If iData IsNot Nothing Then
                                colImpacts.Add(iData)
                            End If
                            k = 0
                        End If

                        j += 1
                    End While
                    'j
                    If readyToStore Then
                        Dim iCopyIndex
                        For iCopyIndex = 1 To colImpacts.Count
                            sData.aImpacts.Add(colImpacts(iCopyIndex))
                        Next iCopyIndex
                        aSessions(UBound(aSessions)) = sData
                        colSession.Add(sData)
                        colImpacts.Clear()
                        ReDim Preserve aSessions(UBound(aSessions) + 1)
                        sData = New gftSession
                        sessionStartTimeF = 0
                        'iData = New gftImpactData
                        readyToStore = False
                    End If
                    bw.ReportProgress(10, CInt(Math.Truncate((CSng(i) * 100) / cnt)))

                    Exit For ' This is our retry loop.. we made it to the end, so we should exit
                Next
            Next
            'i
            'If i = cnt Then
            'bw.ReportProgress(1, "Data Retrieved From Device")
            'End If
        Catch ex As Exception
            Debug.WriteLine("form -> uploadData")
            Return "0"c
        End Try

        Console.WriteLine(Now.ToString & vbTab & "TOTAL NUMBER OF SESSIONS: " & colSession.Count)

        Dim sessionSuccess As Boolean = False
        Dim impactID As String = ""
        Dim batchID As String = ""
        If colSession.Count > 0 Then
            bw.ReportProgress(3, UBound(aSessions))
            batchID = rest.createBatch()
            If batchID = "" Then
                bw.ReportProgress(2, "Error Creating Batch on Server")
                Return "0"c
            End If
            sessionSuccess = rest.writeSessions(colSession, batchID, currentGftProfile.sSerial, bw)
            If sessionSuccess = False Then
                bw.ReportProgress(2, "Error Uploading Session Data to Cloud")
                Return "0"c
            End If
            rest.completeBatch(batchID)
            bw.ReportProgress(8, "Upload to Cloud Completed")
        End If

        ' Erase Data -- Not so fast.. we want to leave this in here for testing
        If colSession.Count > 0 AndAlso MyHid.deviceAttached Then
            bw.ReportProgress(9, "Erasing Device Data")
            If sendCmd(CByte(mhidCmd.MHID_CMD_ERASE_ACC_DATA), 0) = "passed" Then
                gftStat = CByte(mhidCmd.MHID_CMD_ERASE_ACC_DATA)
            Else
                gftStat = 0
            End If
        End If
        Return "1"c
    End Function


    Public Shared Function HIC15(ByVal my40 As [Double]()) As Double
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim tHic As Double = 0
        Dim hic As Double = 0
        For i = 0 To 25
            tHic = 0
            For j = 0 To 14
                tHic += my40(i + j)
            Next
            tHic = 15 * Math.Pow((tHic) / 15, 2.5)
            If hic < tHic Then
                hic = tHic
            End If
        Next
        Return hic / 1000
    End Function
    Public Shared Function GSI(ByVal my40 As [Double]()) As Double
        Dim i As Integer = 0
        Dim gsi__1 As Double = 0
        ' gsi = Accumulation a(t)power2.5dt (in impact duration)
        For i = 0 To 39
            gsi__1 += Math.Pow(my40(i), 2.5)
        Next
        Return gsi__1 / 1000
    End Function

    Public Shared Function getAzmuthAuto(myPoint As [Double]()) As Double()
        'map to desired axis

        Dim x As Double = myPoint(0)
        Dim y As Double = myPoint(1)
        Dim z As Double = myPoint(2)
#If True Then
        Dim b As Double() = New Double(2) {}
        Dim v As Double() = New Double(2) {}
        b(0) = myPoint(0)
        b(1) = myPoint(1)
        b(2) = myPoint(2)
#If True Then
        v = gCalc.mCalibrate(b, currentGFTProfile.iLocation)

        x = v(0)
        y = v(1)
        z = v(2)
#End If
#End If
        Dim myResult As Double() = New Double(1) {}
        'int i = 0;
        Dim azmuth As Double = 0
        Dim elevation As Double = 0
        Dim res As Double = Math.Sqrt(x * x + y * y)
        If y > 0 AndAlso x >= 0 Then

            azmuth = (Math.Atan(x / y) / Math.PI) * 180
        ElseIf x < 0 AndAlso y > 0 Then


            azmuth = (Math.Atan(x / y) / Math.PI) * 180 + 360

        ElseIf y < 0 Then


            azmuth = 180 + (Math.Atan(x / y) / Math.PI) * 180

        ElseIf y = 0 Then
            If x > 0 Then

                azmuth = 90
            ElseIf x < 0 Then

                azmuth = 270
            Else

                azmuth = 0
            End If
        End If

        If x = 0 AndAlso y = 0 Then
            elevation = If(z > 0, 90, If(z = 0, 0, -90))
        End If
        If z < 0 Then
            elevation = (-Math.Atan(Math.Abs(z / res)) / Math.PI) * 180
        ElseIf z > 0 Then
            elevation = (Math.Atan(Math.Abs(z / res)) / Math.PI) * 180
        Else
            elevation = 0
        End If

        myResult(0) = azmuth
        myResult(1) = elevation

        Return myResult
    End Function

    Public Shared Function getProfile() As Boolean

        Dim res As Boolean = False
        Dim nread As Integer = 0
        Dim result As Boolean = False
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}

            outputReportBuffer(0) = 0
            outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_GET_USR_PROFILE)
            outputReportBuffer(2) = 0
            transferSuccess = False

            result = MyHid.OutputReports(outputReportBuffer)
            If Not result Then
                Return False
            End If
            result = MyHid.inputReports(inputReportBuffer, nread)
            If result Then
                If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_GET_USR_PROFILE) Or MHID_CMD_RESPONSE_BIT) Then

                    currentGftProfile.bMagic = inputReportBuffer(3 + 0)
                    Array.Copy(inputReportBuffer, 3 + 1, currentGftProfile.bSerial, 0, 6)

                    currentGftProfile.bThresholdAlarm = inputReportBuffer(3 + 7)
                    currentGftProfile.bThresholdR = inputReportBuffer(3 + 8)
                    currentGftProfile.bAlarmMode = inputReportBuffer(3 + 9)
                    currentGftProfile.bWirelessEnabled = inputReportBuffer(3 + 10)
                    Array.Copy(inputReportBuffer, 3 + 11, currentGftProfile.bName, 0, 20)
                    currentGftProfile.iPlayerNumber = inputReportBuffer(3 + 31)

                    currentGftProfile.iLocation(0) = BitConverter.ToInt16(New Byte(1) {inputReportBuffer(3 + 32), inputReportBuffer(3 + 33)}, 0)
                    currentGftProfile.iLocation(1) = BitConverter.ToInt16(New Byte(1) {inputReportBuffer(3 + 34), inputReportBuffer(3 + 35)}, 0)
                    currentGftProfile.iLocation(2) = BitConverter.ToInt16(New Byte(1) {inputReportBuffer(3 + 36), inputReportBuffer(3 + 37)}, 0)
                    currentGftProfile.iLocation(3) = BitConverter.ToInt16(New Byte(1) {inputReportBuffer(3 + 38), inputReportBuffer(3 + 39)}, 0)

                    'currentGftProfile.bMountInside = If(inputReportBuffer(3 + 40) = 1, True, False)
                    currentGftProfile.mntLoc = inputReportBuffer(3 + 40)
                    currentGftProfile.bMountInside = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_INOUTMSK

                    currentGftProfile.bMountUSBFront = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
                    currentGftProfile.bMountUSBLeft = currentGftProfile.mntLoc And (GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK And GFT.GFT_MNT_LOC_VER)
                    currentGftProfile.bMountUSBRight = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_VER
                    currentGftProfile.bMountUSBRear = False
                    If Not currentGftProfile.bMountUSBFront And Not currentGftProfile.bMountUSBLeft And Not currentGftProfile.bMountUSBRight Then
                        currentGftProfile.bMountUSBRear = True
                    End If

                    currentGftProfile.bTestMode = If(inputReportBuffer(3 + 41) = &H1, True, False)
                    currentGftProfile.bProximityEnabled = If(inputReportBuffer(3 + 42) = 1, True, False)

                    currentGftProfile.bSports = inputReportBuffer(3 + 43)
                    currentGftProfile.sSerial = gGetSerialNo()

                    currentGftProfile.sName = HexToString(currentGftProfile.bName)

                    result = True
                Else
                    'communication error
                    result = False
                End If
            End If
        Catch ex As Exception
            gUtility.DisplayException("Get Profile", ex)
        End Try
        Return result
    End Function

    Public Function gSetSerialNo(sn As Byte()) As Boolean
        Dim result As Boolean = False
        Dim nread As Integer = 0
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}

            outputReportBuffer(0) = 0
            outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_SET_SN)
            outputReportBuffer(2) = 0
            outputReportBuffer(3 + 0) = CByte(sn(0))
            outputReportBuffer(3 + 1) = CByte(sn(1))
            outputReportBuffer(3 + 2) = CByte(sn(2))
            outputReportBuffer(3 + 3) = CByte(sn(3))
            outputReportBuffer(3 + 4) = CByte(sn(4))
            outputReportBuffer(3 + 5) = CByte(sn(5))

            transferSuccess = False
            MyHid.OutputReports(outputReportBuffer)
            'Thread.Sleep(10);
            'transferInProgress = true;
            MyHid.inputReports(inputReportBuffer, nread)
            ' if (transferSuccess == true)
            If True Then
                If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_SET_SN) Or MHID_CMD_RESPONSE_BIT) Then

                    result = True
                End If

            End If
        Catch ex As Exception

            gUtility.DisplayException("Set Serial Number", ex)
        End Try
        Return result

    End Function

    Public Shared Function Byte_to_String(ByVal bytesIn() As Byte) As String
        Dim sOutputString As String = Nothing
        Dim sTest As String = Nothing

        Try
            For ctr = 0 To UBound(bytesIn)
                sOutputString += String.Format("{0:X2}", CByte(bytesIn(ctr)))
            Next
            Console.WriteLine(Now.ToString & vbTab & "RECEIVED THE FOLLOWING DATA: " & sOutputString)
        Catch ex As Exception
            Console.WriteLine(Now.ToString & vbTab & ex.Message)
        End Try

        Return sOutputString
    End Function

    Public Shared Function HexToString(ByVal bytesIn() As Byte) As String
        Dim hex As String = Byte_to_String(bytesIn)
        Dim text As New System.Text.StringBuilder(hex.Length \ 2)
        For i As Integer = 0 To hex.Length - 2 Step 2
            If hex.Substring(i, 2) <> "00" Then
                text.Append(Chr(Convert.ToByte(hex.Substring(i, 2), 16)))
            End If
        Next
        Return text.ToString
    End Function

    Public Shared Function gGetSerialNo() As String

        Dim nread As Integer = 0
        Dim sn As Byte() = New Byte(5) {}
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}

            outputReportBuffer(0) = 0
            outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_GET_SN)
            outputReportBuffer(2) = 0
            transferSuccess = False
            MyHid.OutputReports(outputReportBuffer)
            'Thread.Sleep(10);
            'transferInProgress = true;
            MyHid.inputReports(inputReportBuffer, nread)
            ' if (transferSuccess == true)
            If True Then
                If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_GET_SN) Or MHID_CMD_RESPONSE_BIT) Then
                    sn(5) = inputReportBuffer(3 + 0)
                    sn(4) = inputReportBuffer(3 + 1)
                    sn(3) = inputReportBuffer(3 + 2)
                    sn(2) = inputReportBuffer(3 + 3)
                    sn(1) = inputReportBuffer(3 + 4)
                    sn(0) = inputReportBuffer(3 + 5)
                End If
            End If
        Catch ex As Exception
            gUtility.DisplayException("get serial number", ex)
        End Try
        Return Byte_to_String(sn)
    End Function

    Public Shared Function getUTC() As DateTime
        ' We need to get the UTC Time as we use this for the key
        Dim utcTime As DateTime = TimeZone.CurrentTimeZone.ToUniversalTime(Now())
        Return utcTime
    End Function

    Public Shared Function syncSystemTime() As Boolean

        ' USR_PROFILE usr = new USR_PROFILE();

        Dim nread As Integer = 0
        Dim success As Boolean = False
        If Not MyHid.deviceAttached Then
            Return success
        End If

        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}

            Dim mTime As DateTime = getUTC()
            Console.WriteLine(Now.ToString & vbTab & "Attempting to Sync Time: " & mTime.ToString)

            outputReportBuffer(0) = 0
            outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_SET_TIME)
            outputReportBuffer(2) = 0
            outputReportBuffer(3 + 0) = CByte(mTime.Year Mod 100)
            outputReportBuffer(3 + 1) = CByte(mTime.Month)
            outputReportBuffer(3 + 2) = CByte(mTime.Day)
            outputReportBuffer(3 + 3) = CByte(mTime.Hour)
            outputReportBuffer(3 + 4) = CByte(mTime.Minute)
            outputReportBuffer(3 + 5) = CByte(mTime.Second)

            transferSuccess = False
            MyHid.OutputReports(outputReportBuffer)
            'Thread.Sleep(10);
            'transferInProgress = true;
            If MyHid.inputReports(inputReportBuffer, nread) Then
                ' if (transferSuccess == true)
                If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_SET_TIME) Or MHID_CMD_RESPONSE_BIT) Then
                    success = True
                Else
                    success = False
                End If
            End If
        Catch ex As Exception

            gUtility.DisplayException("SyncSystemTime", ex)
        End Try
        Return success
    End Function

    Public Shared Function getGftTime() As DateTime
        Dim gftTime As New DateTime(2000, 1, 1, 1, 1, 1)
        ' USR_PROFILE usr = new USR_PROFILE();
        Dim nread As Integer = 0
        Try
            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}

            outputReportBuffer(0) = 0
            outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_GET_TIME)
            outputReportBuffer(2) = 0
            transferSuccess = False
            MyHid.OutputReports(outputReportBuffer)
            'Thread.Sleep(10);
            'transferInProgress = true;
            MyHid.inputReports(inputReportBuffer, nread)
            ' if (transferSuccess == true)
            If True Then
                If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_GET_TIME) Or MHID_CMD_RESPONSE_BIT) Then
                    If inputReportBuffer(4) > 0 AndAlso inputReportBuffer(4) < 13 AndAlso inputReportBuffer(5) > 0 AndAlso inputReportBuffer(5) < 32 AndAlso inputReportBuffer(6) < 24 AndAlso inputReportBuffer(7) < 60 AndAlso inputReportBuffer(8) < 60 Then
                        gftTime = New DateTime(2000 + inputReportBuffer(3 + 0), inputReportBuffer(3 + 1), inputReportBuffer(3 + 2), inputReportBuffer(3 + 3), inputReportBuffer(3 + 4), inputReportBuffer(3 + 5))
                    End If
                End If

            End If
        Catch ex As Exception

            gUtility.DisplayException("Get System Time", ex)
        End Try
        Return gftTime
    End Function
    'Public Function gCalibration() As Byte()

    '    Dim nread As Integer = 0
    '    Dim sn As Byte() = New Byte(5) {}
    '    Try
    '        Dim outputReportBuffer As Byte() = New [Byte](64) {}
    '        Dim inputReportBuffer As Byte() = New [Byte](64) {}

    '        outputReportBuffer(0) = 0
    '        outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_GET_SN)
    '        outputReportBuffer(2) = 0
    '        transferSuccess = False
    '        MyHid.OutputReports(outputReportBuffer)
    '        'Thread.Sleep(10);
    '        'transferInProgress = true;
    '        MyHid.inputReports(inputReportBuffer, nread)
    '        ' if (transferSuccess == true)
    '        If True Then
    '            If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_GET_SN) Or MHID_CMD_RESPONSE_BIT) Then
    '                sn(0) = inputReportBuffer(3 + 0)
    '                sn(1) = inputReportBuffer(3 + 1)
    '                sn(2) = inputReportBuffer(3 + 2)
    '                sn(3) = inputReportBuffer(3 + 3)
    '                sn(4) = inputReportBuffer(3 + 4)
    '                sn(5) = inputReportBuffer(3 + 5)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        gUtility.DisplayException("get serial number", ex)
    '    End Try
    '    Return sn
    'End Function

    ' Public Shared Function calibrateGFT(ByVal mntPositionInside As Boolean, ByVal mntPositionUSB As Boolean) As Boolean
    Public Shared Function calibrateGFT(ByVal mntPositionInside, ByVal radioButtonFront, ByVal radioButtonRear, ByVal radioButtonLeft, ByVal radioButtonRight, Optional ByVal ShowDialogs = True) As Boolean



        Dim x As Double, y As Double, z As Double
        Dim nread As Integer = 0

        Dim mq As Int16()
        Dim mntLoc As Byte = 0

        Try

            Dim outputReportBuffer As Byte() = New [Byte](64) {}
            Dim inputReportBuffer As Byte() = New [Byte](64) {}
            If Not MyHid.deviceAttached Then
                If ShowDialogs Then MessageBox.Show("Please connect GFT to this PC", "Warning")
                Return False
            End If
            If sendCmd(CByte(mhidCmd.MHID_CMD_CAL_START), 0) = "passed" Then
                gftStat = CByte(mhidCmd.MHID_CMD_CAL_START)
                Thread.Sleep(200)

                outputReportBuffer(0) = 0
                outputReportBuffer(1) = CByte(mhidCmd.MHID_CMD_CAL_GET)
                outputReportBuffer(2) = 0

                MyHid.OutputReports(outputReportBuffer)
                MyHid.inputReports(inputReportBuffer, nread)
                MyHid.OutputReports(outputReportBuffer)
                MyHid.inputReports(inputReportBuffer, nread)
                If True Then
                    If inputReportBuffer(1) = CByte(CByte(mhidCmd.MHID_CMD_CAL_GET) Or MHID_CMD_RESPONSE_BIT) Then
                        x = lgDecode(inputReportBuffer(3), inputReportBuffer(4))
                        y = lgDecode(inputReportBuffer(5), inputReportBuffer(6))
                        z = lgDecode(inputReportBuffer(7), inputReportBuffer(8))
                        mq = gCalc.getCaliQ(x, y, z, mntPositionInside)

                        currentGftProfile.iLocation(0) = mq(0)
                        currentGftProfile.iLocation(1) = mq(1)
                        currentGftProfile.iLocation(2) = mq(2)
                        currentGftProfile.iLocation(3) = mq(3)


                        mntLoc = CByte(If(mntPositionInside, GFT.GFT_MNT_LOC_INOUTMSK, 0))
                        If radioButtonFront Then
                            mntLoc += CByte(GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK)
                        ElseIf radioButtonLeft Then
                            mntLoc += CByte(GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK)
                            mntLoc += CByte(GFT.GFT_MNT_LOC_VER)
                        ElseIf radioButtonRight Then

                            mntLoc += CByte(GFT.GFT_MNT_LOC_VER)
                        End If


                        currentGftProfile.mntLoc = mntLoc
                        '   currentGftProfile.mntLoc = CByte(If(mntPositionUSB, GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK, 0))
                        '   currentGftProfile.mntLoc += CByte(If(mntPositionInside, GFT.GFT_MNT_LOC_INOUTMSK, 0))




                        '  rest.calibrate(mq, mntPositionInside, mntPositionUSB)

                        rest.calibrate(mq, mntPositionInside, radioButtonFront, radioButtonRear, radioButtonLeft, radioButtonRight)
                        configGft()
                        If frmCalibrate.Enabled Then frmCalibrate.Close()
                        If ShowDialogs Then MessageBox.Show(frmCalibrate, "GFT has been calibrated for location and ready to use.")
                    Else
                        If ShowDialogs Then MessageBox.Show("Communication failed, try again")
                        gftStat = 0
                    End If
                End If
                Return True
            Else
                If ShowDialogs Then MessageBox.Show("Communication failed, try again")
                gftStat = 0
            End If

        Catch ex As Exception
            Console.WriteLine(Now.ToString & vbTab & ex.Message)
            Return False
        End Try
        Return True
    End Function

    Public Shared Function calibrateGFTChinStrap() As Boolean
        Dim mq As Int16() = New Int16(3) {}
        Dim mntLoc As Byte = 0

        Try
            If Not MyHid.deviceAttached Then
                MessageBox.Show("Please connect GFT to this PC", "Warning")
                Return False
            End If
            mq(0) = 928
            mq(1) = 1
            mq(2) = -370
            mq(3) = -3

            currentGftProfile.iLocation(0) = mq(0)
            currentGftProfile.iLocation(1) = mq(1)
            currentGftProfile.iLocation(2) = mq(2)
            currentGftProfile.iLocation(3) = mq(3)

            'currentGftProfile.bMountInside = True
            'currentGftProfile.bMountUSBFront = False
            'currentGftProfile.bMountUSBRear = False
            'currentGftProfile.bMountUSBLeft = False
            'currentGftProfile.bMountUSBRight = True


            'mntLoc = CByte(If(currentGftProfile.bMountInside = False, GFT.GFT_MNT_LOC_INOUTMSK, 0))
            'If currentGftProfile.bMountUSBFront Then
            '    mntLoc += CByte(GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK)
            'ElseIf currentGftProfile.bMountUSBLeft Then
            '    mntLoc += CByte(GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK)
            '    mntLoc += CByte(GFT.GFT_MNT_LOC_VER)
            'ElseIf currentGftProfile.bMountUSBRight Then
            '    mntLoc += CByte(GFT.GFT_MNT_LOC_VER)
            'End If

            currentGftProfile.mntLoc = &H46 'mntLoc

            currentGftProfile.bMountInside = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_INOUTMSK

            currentGftProfile.bMountUSBFront = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK
            currentGftProfile.bMountUSBLeft = currentGftProfile.mntLoc And (GFT.GFT_MNT_LOC_USB_FRONT_REAR_MSK And GFT.GFT_MNT_LOC_VER)
            currentGftProfile.bMountUSBRight = currentGftProfile.mntLoc And GFT.GFT_MNT_LOC_VER
            currentGftProfile.bMountUSBRear = False
            If Not currentGftProfile.bMountUSBFront And Not currentGftProfile.bMountUSBLeft And Not currentGftProfile.bMountUSBRight Then
                currentGftProfile.bMountUSBRear = True
            End If

            rest.calibrate(mq, currentGftProfile.bMountInside, currentGftProfile.bMountUSBFront, currentGftProfile.bMountUSBRear, currentGftProfile.bMountUSBLeft, currentGftProfile.bMountUSBRight)
            configGft()
            needsCalibration = False
            ' If frmCalibrate.Enabled Then frmCalibrate.Close()
            'MessageBox.Show(frmCalibrate, "GFT has been calibrated for location and ready to use.")

            Return True

        Catch ex As Exception
            Console.WriteLine(Now.ToString & vbTab & ex.Message)
            Return False
        End Try
        Return True
    End Function
End Class

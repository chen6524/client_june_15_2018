﻿Public Class gftMessageBox
    Private text2 As String
    Private productName As String
    Private buttons As MessageBoxButtons
    Private iconicon As MessageBoxIcon

    Private MyButtonOK As Button
    Private MyButtonYes As Button
    Private MyButtonNo As Button
    Private MyButtonCancel As Button
    Public Sub New(text As String, productName As String, buttons As MessageBoxButtons, icon As MessageBoxIcon)
        InitializeComponent()
        Me.text2 = text
        Me.productName = productName
        Me.buttons = buttons
        Me.iconicon = icon
    End Sub

    Public Sub gftMessageBox_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim MyIcon As New Drawing.Icon(Me.iconicon)
        'Dim MyRect As New Rectangle(New Point(20, 20), New Drawing.Size(New Point(64, 64)))
        'Me.Graphics.DrawIcon()
        'Dim MyGraphics As Graphics = Graphics.FromHwnd(Me.Handle)
        'MyGraphics.DrawIcon(MyIcon, MyRect)
        Dim padding As Integer = 20
        Debug.WriteLine("Loading")
        Me.Text = productName
        Dim MyLabel As New Label
        With MyLabel
            .AutoSize = True
            .Location = New Point(padding, padding)
            .Text = Me.text2
        End With
        Me.Controls.Add(MyLabel)
        If MyLabel.Width > 315 Then
            Me.Width = MyLabel.Width + (padding * 3)
        Else
            Me.Width = 315
        End If

        Me.Height = MyLabel.Height + 25 + (padding * 5)
        If buttons = MessageBoxButtons.OK Then
            MyButtonOK = New Button()
            With MyButtonOK
                .Size = New Drawing.Size(New Point(120, 25))
                .Location = New Point(Me.Width - 120 - (padding * 2), Me.Height - 25 - (padding * 3))
                .Text = "OK"
            End With
            AddHandler MyButtonOK.Click, AddressOf onButtonOKClick
            Me.Controls.Add(MyButtonOK)
        ElseIf buttons = MessageBoxButtons.YesNo Then
            MyButtonYes = New Button()
            MyButtonNo = New Button()
            With MyButtonYes
                .Size = New Drawing.Size(New Point(120, 25))
                .Location = New Point(Me.Width - 120 - (padding * 3) - 120, Me.Height - 25 - (padding * 3))
                .Text = "Yes"
            End With
            With MyButtonNo
                .Size = New Drawing.Size(New Point(120, 25))
                .Location = New Point(Me.Width - 120 - (padding * 2), Me.Height - 25 - (padding * 3))
                .Text = "No"
            End With
            AddHandler MyButtonYes.Click, AddressOf onButtonYesClick
            AddHandler MyButtonNo.Click, AddressOf onButtonNoClick
            Me.Controls.Add(MyButtonYes)
            Me.Controls.Add(MyButtonNo)

        End If
    End Sub

    Private Sub onButtonOKClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub
    Private Sub onButtonYesClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub onButtonNoClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub
End Class
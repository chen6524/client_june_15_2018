﻿Imports System.ComponentModel
Imports System.Net
Public Class FileDownload
    Private SoftwareUpdate As SoftwareUpdate
    Public Complete As Boolean = False
    Public Cancelled As Boolean = False
    Public CancelPending As Boolean = False
    Private _WebClient As System.Net.WebClient
    Private Path As String
    Private ReplacePath As String

    Public Sub New(ByRef SoftwareUpdate As SoftwareUpdate, URL As String, Path As String, ReplacePath As String, ReplaceFile As Boolean)
        Me.Path = Path
        Me.ReplacePath = ReplacePath
        Me.SoftwareUpdate = SoftwareUpdate ' Keep a reference to the parent class
        Dim FileExists = System.IO.File.Exists(Path) ' Check if the file exists

        If Not FileExists Or ReplaceFile Then ' Only update if the file doesn't exist or replace is true
            _WebClient = New System.Net.WebClient()
            AddHandler _WebClient.DownloadFileCompleted, AddressOf _DownloadFileCompleted
            AddHandler _WebClient.DownloadProgressChanged, AddressOf _DownloadProgressChanged

            My.Computer.FileSystem.RenameFile(Path, ReplacePath)
            _WebClient.DownloadFileAsync(New Uri(URL), Path)
        Else
            Me.Complete = True
            Me.SoftwareUpdate.NotifyDownloadComplete()
        End If

    End Sub

    Public Sub cancel()
        Me._WebClient.CancelAsync()
        Me.CancelPending = True
    End Sub

    ' Occurs when an asynchronous file download operation completes.
    Private Sub _DownloadFileCompleted(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
        If e.Error Is Nothing And Not e.Cancelled Then
            Me.Complete = True
            Me.SoftwareUpdate.NotifyDownloadComplete()
        Else
            Me.Cancelled = True
            Dim fnParts = Path.Split("\\")
            Dim ReplacePath2 As String = Path
            Path = fnParts(fnParts.Length - 1)
            ReplacePath = ReplacePath2.Replace(Path, ReplacePath)
            If System.IO.File.Exists(Path) Then My.Computer.FileSystem.DeleteFile(Path) ' Check if file exists, if so, delete it
            My.Computer.FileSystem.RenameFile(ReplacePath, Path) ' Rename file back to what it was
            Me.SoftwareUpdate.NotifyDownloadFailed()
        End If
    End Sub

    ' Occurs when an asynchronous download operation successfully transfers some or all of the data.
    Private Sub _DownloadProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        ' Update progress bar
    End Sub
End Class

﻿Imports System.Windows.Forms
Imports System.Collections.Generic
Imports System.Text
'using CyUSB;
Imports System.Threading
Imports System.ComponentModel
Imports System.Diagnostics

Public Class GFT
    Public Class locDataInByteStruct
        Public loc As Byte() = New Byte(7) {}
    End Class
    Friend Const GFT_MNT_LOC_INOUTMSK As Byte = &H1
    Friend Const GFT_MNT_LOC_USB_FRONT_REAR_MSK As Byte = &H2
    Friend Const GFT_MNT_LOC_ZONE_MSK As Byte = &H30
    Friend Const GFT_MNT_LOC_ZONE_CROWN As Byte = &H80
    Friend Const GFT_MNT_LOC_ZONE_RIGHT As Byte = &H10
    Friend Const GFT_MNT_LOC_ZONE_LEFT As Byte = &H20
    Friend Const GFT_MNT_LOC_ZONE_BACK As Byte = &H40
    Friend Const GFT_MNT_LOC_ZONE_FRONT As Byte = &H8
    Friend Const GFT_MNT_LOC_VER As Byte = &H4
    Public Class GFT_PROFILE
        Friend magic As Byte
        Public GID As Byte() = New Byte(5) {}
        Friend tAlam As Byte
        Friend tR As Byte
        Friend AlarmMode As Byte
        Friend lpEnable As Byte
        Friend name As Byte() = New Byte(19) {}
        Friend pn As Byte
        Friend location As Int16() = New Int16(3) {}
        Friend sports As Integer
        Friend level As Integer
        Friend countDay As Integer
        Friend countWeek As Integer
        Friend countMonth As Integer
        Friend countYear As Integer
        Friend mntInside As Boolean
        Friend testMode As Boolean
        Friend mntLoc As Byte
    End Class
    Public Class GFT_USR
        Friend uid As Int32
        Friend FNAME As String = ""
        Friend LNAME As String = ""
        Friend BDAY As DateTime
        Friend WEIGHT As Double
        Friend HEIGHT As Double
        Friend email As String = ""
        Friend country As Integer
        Friend male As Boolean
        Friend pn As Byte
    End Class
    Public Class GFT_DEVICE
        Friend hwMajor As Byte
        Friend hwMinor As Byte
        Friend bootMajor As Byte
        Friend bootMinor As Byte
        Friend firmMajor As Byte
        Friend firmMinor As Byte
    End Class
    'int[] sn = { 0, 0, 0, 0, 0, 0 };//sn[0] oem number 

    Public Shared gftProfile As New GFT_PROFILE()
    'gft settings
    Public Shared curUser As New GFT_USR()
    Public Shared curGFT As New GFT_DEVICE()
    'gft info

    Public Const MY_PRODUCT_BOOT_ID As UInt16 = &H401
    ' not used 
    Public Const MY_PRODUCT_DONGLE_ID As UInt16 = &H403
    ' usb dongle 
    Public Const MY_PRODUCT_ID As UInt16 = &H402
    ' usb cable 
    Public Const MY_VENDOR_ID As UInt16 = &H1CD1


    Public Const PROFILE_ALARM_EN_MSK As Byte = &H80
    Public Const PROFILE_ALARM_AU_MSK As Byte = &H10
    Public Const PROFILE_ALARM_VI_MSK As Byte = &H20
    Public Const PROFILE_ALARM_LOCK_MSK As Byte = &H40
    'return to play interlock
    Public Const PROFILE_ALARM_ON_MSK As Byte = &H1
    'CyHidDevice MyLp;
    'static USBDeviceList usbDevices;
    Public lpLock As New Object()
    Public Shared lpHub As LP_HUB = Nothing

    Public Shared gftLpTxDone As Boolean = True
    Public Shared gftLpRxDone As Boolean = True

    Friend Const GFT_STATE_NONE As Byte = 0
    Friend Const GFT_STATE_BOOT As Byte = 1
    Friend Const GFT_STATE_FIRM As Byte = 2
    Public Shared gftWhereAmI As Byte = 0

End Class

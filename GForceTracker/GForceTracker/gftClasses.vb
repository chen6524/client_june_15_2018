﻿Public Class gftProfile
    Public bMagic As Byte
    Public bSerial(6) As Byte
    Public bootMajor As Integer
    Public bootMinor As Integer
    Public firmMajor As Integer
    Public firmMinor As Integer
    Public hwMajor As Integer
    Public hwMinor As Integer
    Public gftWhereAmI As Integer
    Public bThresholdAlarm As Byte
    Public bThresholdR As Byte
    Public bWirelessEnabled As Byte ' LP
    Public bCloudWirelessEnabled As Byte ' LP
    Public bProximityEnabled As Byte
    Public bAlarmMode As Byte
    Public bName(20) As Byte
    Public sSerial As String
    Public sName As String
    Public iRecordThreshold As Integer
    Public iCloudRecordThreshold As Integer
    Public iCloudThreshold As Integer
    Public iCloudHICAlarm As Integer
    Public iCloudGSIAlarm As Integer
    Public iCloudRAMAlarm As Integer
    Public iCloudAlarmEnable As Integer
    Public iCloudAlarmAudio As Integer
    Public iCloudAlarmVisual As Integer
    Public iCloudAlarmInterlock As Integer
    Public sCloudName As String
    Public bCloudAlarmMode As Byte
    Public iLocation(4) As Double
    Public iPlayerNumber As Integer
    Public iCloudPlayerNumber As Integer
    Public bTestMode As Boolean
    Public bMountInside As Boolean
    Public bCloudTestMode As Boolean
    Public iCloudSports As Integer
    Public sOverrideDeviceType As String

    Public bMountUSBFront As Boolean
    Public bMountUSBRear As Boolean
    Public bMountUSBLeft As Boolean
    Public bMountUSBRight As Boolean
    Public sEmergencyContact As String
    Public bPositon As Byte
    Public iLocCoefficent(9) As Double
    Public bSports As Byte
    Public mntLoc As Byte
    Public hwType As String

    Public bMountCoefficient As Byte
End Class

Public Class gftSession
    Public iSessionID As Int32
    Public iUID As Integer
    Public dDate As DateTime
    Public dOnTime As DateTime
    Public dOffTime As DateTime
    Public dO_Date As DateTime
    Public dO_OnTime As DateTime
    Public dO_OffTime As DateTime
    Public dO_FirstOnTime As DateTime
    Public dO_LastOnTime As DateTime
    Public dO_FirstOffTime As DateTime
    Public dSesOnCnt As Int32
    Public dSesOffCnt As Int32
    Public iActiveTime As Int32
    Public iDuration As Int32
    Public iImpact As Int32
    Public iAlarm As Int32
    Public bWTHR As Byte
    Public iWTH As Int32
    Public iThreshold As Int32
    Public iSports As Int32
    Public aImpacts As New Collection
    Public oldTime? As DateTime = Nothing
    Public newTime? As DateTime = Nothing
    Public dCoefficient As Double

    Public iMntInside As Boolean
    Public iLocation As Double() = New Double(4) {}
    
    Public fwVersion As String

    Public swVersion As String
    Public uDate As DateTime
    Public batStatus As Int32

    Public Sub fixTimeStamps()
        If Not Me.oldTime Is Nothing And Not newTime Is Nothing And newTime > DateTime.Parse("2010-01-01") And newTime < DateTime.UtcNow.AddYears(1) Then ' No point adjusting anything if the GFT gave us garbage
            If (Me.oldTime > dOnTime) Then
                Dim timeDiff As TimeSpan = newTime - oldTime
                ' Great, we have a time change during the session
                Me.dOnTime = Me.dOnTime + timeDiff ' Update the power on time
                ' Iterate over the impacts until...
                Dim lastImpactTime? As DateTime = Nothing
                If oldTime < newTime Then
                    ' a) We reach oldTime (only works if adjustment is an increase) - This is our primary use case
                    Dim newCol As New Collection
                    Dim updateImpacts = True
                    For Each impact As gftImpactData In aImpacts
                        ' Ensure we are moving forward in time...
                        If lastImpactTime Is Nothing Or impact.dTime > lastImpactTime Then
                            lastImpactTime = impact.dTime
                            If updateImpacts Then
                                If (impact.dTime < oldTime) Then
                                    impact.dTime = impact.dTime + timeDiff
                                Else
                                    updateImpacts = False ' We don't need to update anymore impacts
                                End If
                            End If
                        End If
                        newCol.Add(impact)
                    Next
                    Me.aImpacts = newCol ' Overwrite old collection with new
                End If

                lastImpactTime = Nothing
                If oldTime > newTime Then
                    ' b) We reach a timestamp that is earlier than one already read - This is possible, but unlikely use case
                    Dim newCol As New Collection
                    Dim updateImpacts = True
                    For Each impact As gftImpactData In aImpacts
                        ' Ensure we are moving forward in time...
                        If lastImpactTime Is Nothing Or impact.dTime > lastImpactTime Then
                            lastImpactTime = impact.dTime
                            If updateImpacts Then
                                If (impact.dTime < oldTime) Then
                                    impact.dTime = impact.dTime + timeDiff
                                Else
                                    updateImpacts = False ' We don't need to update anymore impacts
                                End If
                            End If
                        Else
                            ' We're going back in time.. This could only mean the time change happened here
                            updateImpacts = False ' We don't need to update anymore impacts
                        End If
                        newCol.Add(impact)
                    Next
                    Me.aImpacts = newCol ' Overwrite old collection with new
                End If
            End If
        End If
    End Sub

End Class

Public Class gftImpactData
    Public iEID As Int32
    Public iSID As Int32
    Public sGID As String
    Public dTime As DateTime
    Public dTimeMS As UShort
    Public d_OTime As DateTime
    Public d_OTimeMS As UShort
    Public iLinX As Double
    Public iLinY As Double
    Public iLinZ As Double
    Public iLinR As Double

    Public iGForce As Double
    Public iAzimuth As Double
    Public iElevation As Double

    Public iRotX As Double
    Public iRotY As Double
    Public iRotZ As Double
    Public iRotR As Double

    Public iBrIC As Double

    Public iMaxRotX As Double
    Public iMaxRotY As Double
    Public iMaxRotZ As Double

    Public iHIC As Double
    Public iGSI As Double
    Public iPCS As Double

    Public bLocationID As Byte

    Public bDataX(119) As Byte
    Public bDataY(119) As Byte
    Public bDataZ(119) As Byte

    Public bRDataX(119) As Byte
    Public bRDataY(119) As Byte
    Public bRDataZ(119) As Byte


    Public bDataX_raw(119) As Byte
    Public bDataY_raw(119) As Byte
    Public bDataZ_raw(119) As Byte

    Public bRDataX_raw(119) As Byte
    Public bRDataY_raw(119) As Byte
    Public bRDataZ_raw(119) As Byte

    Public bucket(12) As Integer

End Class

Public Class gCalc
    ' we make them public so that users can interact directly with device classes
    Public Shared qua As [Double]() = New Double(3) {}
    Public Shared mntInside As Boolean
    Public Shared Sub initQ(ByRef q As Double(), inside As Boolean)
        gCalc.qua(0) = q(0) / 1000.0F
        gCalc.qua(1) = q(1) / 1000.0F
        gCalc.qua(2) = q(2) / 1000.0F
        gCalc.qua(3) = q(3) / 1000.0F
        mntInside = inside
    End Sub
    Public Shared Function validateQ() As Boolean
        If gCalc.qua(0) = 0 And gCalc.qua(1) = 0 And gCalc.qua(2) = 0 And gCalc.qua(3) = 0 Then Return False

        Return True
    End Function

    Public Shared Function validateRotation(ByRef a As Double(), ByRef b As Double(), mode As Integer) As Boolean
        Dim dot As Double
        Dim angle As Double
        Dim c As Double() = New Double(3) {}
        Dim d As Double() = New Double(3) {}
        dot = a(0) * b(0) + a(1) * b(1) + a(2) * b(2)
        dot = dot / (Math.Sqrt(b(0) * b(0) + b(1) * b(1) + b(2) * b(2)) * Math.Sqrt(a(0) * a(0) + a(1) * a(1) + a(2) * a(2)))
        c = mCross(a, b)
        angle = Math.Acos(dot)
        ' d = toEuler(c[0], c[1], c[2], angle);
        If angle * 2 < Math.PI * 0.85 OrElse angle * 2 > Math.PI * 1.15 Then
            Return False
        End If
        Return True
    End Function
    Public Shared Function getCaliQ(x As Double, y As Double, z As Double, inside As Boolean) As Int16()
        Dim a As [Double]() = New [Double](2) {}
        Dim b As [Double]() = New [Double](2) {}
        Dim c As Int16() = New Int16(3) {}
        Dim v As [Double]() = New [Double](3) {}

        Dim myResult As Double() = New Double(1) {}
        'if (x > 0.99) x = 0.99;
        'if (y > 0.99) y = 0.99;
        'if (z > 0.99) z = 0.99;
        'if (x < -0.99) x = -0.99;
        'if (y < -0.99) y = -0.99;
        'if (z < -0.99) z = -0.99;
        b(0) = -x
        b(1) = y
        b(2) = z
        'reference 
        If inside Then
            a(0) = 0.01
            a(1) = 0.01
            a(2) = 0.99
        Else

            a(0) = -0.01
            a(1) = -0.01

            a(2) = -0.99
        End If
        'b[0] = 0.01;
        'b[1] = 0.01;
        'b[2] = 0.99;
        c = gCalc.mGetQ(a, b)
        'c[0] = 0.92;
        'c[1] = 0;
        'c[2] = 0;
        'c[3] = -0.38;
        'v = gCalc.mCalibrate(ref b, ref c);
        qua(0) = c(0) / 1000
        qua(1) = c(1) / 1000
        qua(2) = c(2) / 1000
        qua(3) = c(3) / 1000
        Return c
    End Function
    Public Shared Function mGetQold(ByRef a As Double(), ByRef b As Double()) As Int16()
        Dim c As Double() = New Double(3) {}
        Dim d As Int16() = New Int16(3) {}
        'double[] rpy;
        Dim r As Double
        'double normal;
        Dim dot As Double

        c(1) = a(1) * b(2) - a(2) * b(1)
        c(2) = a(2) * b(0) - a(0) * b(2)
        c(3) = a(0) * b(1) - a(1) * b(0)
        r = Math.Sqrt(a(0) * a(0) + a(1) * a(1) + a(2) * a(2)) * Math.Sqrt(b(0) * b(0) + b(1) * b(1) + b(2) * b(2))

        dot = a(0) * b(0) + a(1) * b(1) + a(2) * b(2)
        If Math.Abs(dot - r) < 0.15 Then

            c(0) = 1
        Else
            c(0) = Math.Acos(dot / r)
        End If
        'normal = Math.Sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2] +c[3] * c[3]);
        'c[0] /= normal;
        'c[1] /= normal;
        'c[2] /= normal;
        'c[3] /= normal;

        'rpy = mGetEuler(0, 0, Math.PI / 4);
        d(0) = CShort(c(0) * 1000)
        d(1) = CShort(c(1) * 1000)
        d(2) = CShort(c(2) * 1000)
        d(3) = CShort(c(3) * 1000)

        Return d
    End Function

    Public Shared Function mCross(ByRef a As Double(), ByRef b As Double()) As Double()
        Dim cross As Double() = New Double(2) {}
        cross(0) = a(2 - 1) * b(3 - 1) - a(3 - 1) * b(2 - 1)
        cross(1) = a(1 - 1) * b(3 - 1) - a(3 - 1) * b(1 - 1)
        cross(2) = a(1 - 1) * b(2 - 1) - a(2 - 1) * b(1 - 1)

        Return cross

    End Function

    Private Shared Function mCrossLen(ByRef a As Double(), ByRef b As Double()) As Double
        Dim len As Double, temp As Double, n As Double, s As Double
        temp = a(1 - 1) * b(1 - 1) + a(2 - 1) * b(2 - 1) + a(3 - 1) * b(3 - 1)
        n = Math.Sqrt(a(0) * a(0) + a(1) * a(1) + a(2) * a(2)) * Math.Sqrt(b(0) * b(0) + b(1) * b(1) + b(2) * b(2))
        s = temp / n
        temp = Math.Sqrt(1 - s * s)
        len = n * temp
        Return len

    End Function

    Private Shared Function mNormalize(ByRef a As Double()) As Double()
        Dim n As Double() = New Double(2) {}
        Dim l As Double
        n(0) = a(0)
        n(1) = a(1)
        n(2) = a(2)
        l = a(0) * a(0) + a(1) * a(1) + a(2) * a(2)
        If l = 1.0F Then
            Return n
        End If

        l = Math.Sqrt(l)

        If l < 0.000001F Then
            Return n
        End If

        n(0) = a(0) / l
        n(1) = a(1) / l
        n(2) = a(2) / l
        Return n
    End Function
    Public Shared Function mGetQ(ByRef b As Double(), ByRef a As Double()) As Int16()
        Dim c As Double() = New Double(3) {}
        Dim d As Int16() = New Int16(3) {}
        Dim v As Double(), w As Double()
        Dim r As Double
        Dim normal As Double
        Dim dot As Double
        Dim cross As Double() = New Double(2) {}
        Dim xUnitvec As Double() = New Double(2) {1, 0, 0}
        Dim yUnitvec As Double() = New Double(2) {0, 1, 0}


        dot = a(0) * b(0) + a(1) * b(1) + a(2) * b(2)
        If dot < -0.999999 Then
            v = mCross(xUnitvec, a)
            If mCrossLen(xUnitvec, a) < 0.000001 Then
                v = mCross(yUnitvec, a)
            End If
            w = mNormalize(v)

            c(1) = w(0)
            c(2) = w(1)
            c(3) = w(2)
            c(0) = 0
        ElseIf dot > 0.999999 Then
            c(1) = 0
            c(2) = 0
            c(3) = 0
            c(0) = 1
        Else
            w = mCross(a, b)
            c(1) = w(0)
            c(2) = w(1)
            c(3) = w(2)
            c(0) = 1 + dot

            normal = Math.Sqrt(c(0) * c(0) + c(1) * c(1) + c(2) * c(2) + c(3) * c(3))
            c(0) /= normal
            c(1) /= normal
            c(2) /= normal
            c(3) /= normal
        End If


        d(0) = CShort(c(0) * 1000)
        d(1) = CShort(c(1) * 1000)
        d(2) = CShort(c(2) * 1000)
        d(3) = CShort(c(3) * 1000)


        Return d
    End Function

    Public Shared Function mGetQx(ByRef a As Double(), ByRef b As Double()) As Int16()
        Dim c As Double() = New Double(3) {}
        Dim d As Int16() = New Int16(3) {}
        'double[] rpy;
        Dim r1 As Double, r2 As Double, r3 As Double
        'double normal;
        Dim dot As Double
        Dim t As Double
        Dim an As Double() = New Double(2) {}
        Dim bn As Double() = New Double(2) {}
        Dim u As Double() = New Double(2) {}
        r1 = Math.Sqrt(a(0) * a(0) + a(1) * a(1) + a(2) * a(2))
        r2 = Math.Sqrt(b(0) * b(0) + b(1) * b(1) + b(2) * b(2))
        an(0) = a(0) / r1
        an(1) = a(1) / r1
        an(2) = a(2) / r1
        bn(0) = b(0) / r2
        bn(1) = b(1) / r2
        bn(2) = b(2) / r2

        dot = an(0) * bn(0) + an(1) * bn(1) + an(2) * bn(2)

        u(0) = an(1) * bn(2) - an(2) * bn(1)
        u(1) = an(2) * bn(0) - an(0) * bn(2)
        u(2) = an(0) * bn(1) - an(1) * bn(0)
        r3 = Math.Sqrt(u(0) * u(0) + u(1) * u(1) + u(2) * u(2))
        u(0) = u(0) / r3
        u(1) = u(1) / r3
        u(2) = u(2) / r3

        t = Math.Acos(dot) / 2
        c(0) = Math.Cos(t)
        c(1) = u(0) * Math.Sin(t)
        c(2) = u(1) * Math.Sin(t)
        c(3) = u(2) * Math.Sin(t)
        'normal = Math.Sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2] +c[3] * c[3]);
        'c[0] /= normal;
        'c[1] /= normal;
        'c[2] /= normal;
        'c[3] /= normal;

        'rpy = mGetEuler(0, 0, Math.PI / 4);
        d(0) = CShort(c(0) * 1000)
        d(1) = CShort(c(1) * 1000)
        d(2) = CShort(c(2) * 1000)
        d(3) = CShort(c(3) * 1000)

        Return d
    End Function
    Public Shared Function mQmulti(ByRef a As Double(), ByRef b As Double()) As Double()
        Dim c As Double() = New Double(3) {}
        c(0) = a(0) * b(0) - a(1) * b(1) - a(2) * b(2) - a(3) * b(3)
        c(1) = a(0) * b(1) + a(1) * b(0) + a(2) * b(3) - a(3) * b(2)
        c(2) = a(0) * b(2) + a(2) * b(0) + a(3) * b(1) - a(1) * b(3)
        c(3) = a(0) * b(3) + a(3) * b(0) + a(1) * b(2) - a(2) * b(1)
        Return c

    End Function
    '
    Friend Const PIOVER180 As Single = CSng(180 / Math.PI)
    Public Shared Function mGetEuler(pitch As Double, yaw As Double, roll As Double) As Double()
        ' Basically we create 3 Quaternions, one for pitch, one for yaw, one for roll
        ' and multiply those together.
        ' the calculation below does the same, just shorter
        Dim normal As Double = 0
        Dim q As Double() = New Double(3) {}
        Dim p As Double = pitch * PIOVER180 / 2.0F
        Dim y As Double = yaw * PIOVER180 / 2.0F
        Dim r As Double = roll * PIOVER180 / 2.0

        Dim sinp As Double = Math.Sin(p)
        Dim siny As Double = Math.Sin(y)
        Dim sinr As Double = Math.Sin(r)
        Dim cosp As Double = Math.Cos(p)
        Dim cosy As Double = Math.Cos(y)
        Dim cosr As Double = Math.Cos(r)

        q(1) = sinr * cosp * cosy - cosr * sinp * siny
        q(2) = cosr * sinp * cosy + sinr * cosp * siny
        q(3) = cosr * cosp * siny - sinr * sinp * cosy
        q(0) = cosr * cosp * cosy + sinr * sinp * siny

        normal = Math.Sqrt(q(0) * q(0) + q(1) * q(1) + q(2) * q(2) + q(3) * q(3))
        q(0) /= normal
        q(1) /= normal
        q(2) /= normal
        q(3) /= normal
        Return q
    End Function
    Public Shared Function mQrotation(ByRef a As Double(), ByRef q As Double()) As Double()
        Dim c As Double() = New Double(2) {}
        c(0) = a(0) * (q(1) * q(1) + q(0) * q(0) - q(2) * q(2) - q(3) * q(3)) + a(1) * (2 * q(1) * q(2) - 2 * q(0) * q(3)) + a(3) * (2 * q(1) * q(3) + 2 * q(0) * q(2))
        c(1) = a(0) * (2 * q(0) * q(3) + 2 * q(1) * q(2)) + a(1) * (q(0) * q(0) - q(1) * q(1) + q(2) * q(2) - q(3) * q(3)) + a(3) * (-2 * q(0) * q(1) + 2 * q(2) * q(3))
        c(2) = a(0) * (-2 * q(0) * q(2) + 2 * q(1) * q(3)) + a(1) * (2 * q(0) * q(1) + 2 * q(2) * q(3)) + a(3) * (q(0) * q(0) - q(1) * q(1) - q(2) * q(2) + q(3) * q(3))
        Return c

    End Function
    Public Shared Function mCalibrate(ByRef a As Double(), ByRef q As Double()) As Double()

        'q[0] = 0;
        'q[1] = 0;
        'q[2] = 1;
        'q[3] = 0;


        Dim v As Double() = New Double(3) {}
        Dim c As Double() = New Double(3) {}
        Dim m As Double() = New Double(3) {}
        Dim r As Double()
        c(0) = q(0)
        c(1) = -q(1)
        c(2) = -q(2)
        c(3) = -q(3)

        v(0) = 0
        v(1) = a(0)
        v(2) = a(1)
        v(3) = a(2)
        m = mQmulti(v, c)
        r = mQmulti(q, m)
        r(0) = r(1)
        r(1) = r(2)
        r(2) = r(3)
        'r = mQrotation(ref v, ref q);
        Return r
    End Function
    'auto calibration

    Public Shared Function gCalcPosition(azimuth As Double, elevation As Double, ByRef hitPosition As String, inside As Boolean) As Integer
        Dim index As Integer = 0
        If inside Then

            'If elevation >= 45 Then
            '    hitPosition = "BOTTOM"
            '"LEFT";
            'Else

            If elevation <= -45 Then
                hitPosition = "TOP"
                '"RIGHT";
            ElseIf azimuth <= (225) AndAlso azimuth >= (135) Then
                hitPosition = "BACK"
                '"FRONT";
            ElseIf azimuth <= (45) OrElse azimuth >= (315) Then
                hitPosition = "FRONT"
                ' "BACK";
            ElseIf azimuth >= (45) AndAlso azimuth <= (135) Then
                hitPosition = "RIGHT"
            Else
                '"LEFT";
                '
                hitPosition = "LEFT"
                '"RIGHT";
            End If
        Else
            'ElseIf elevation <= -45 Then
            'hitPosition = "BOTTOM"
            ''"RIGHT";
            If elevation >= 45 Then
                hitPosition = "TOP"
                '"LEFT";
            ElseIf azimuth <= (225) AndAlso azimuth >= (135) Then
                hitPosition = "BACK"
                '"FRONT";
            ElseIf azimuth <= (45) OrElse azimuth >= (315) Then
                hitPosition = "FRONT"
                '"BACK";
            ElseIf azimuth >= (45) AndAlso azimuth <= (135) Then
                hitPosition = "LEFT"
            Else
                '"RIGHT";
                '
                hitPosition = "RIGHT"
                '"LEFT";
            End If
        End If

        'get location index
        If hitPosition = "RIGHT" Then
            index = 1
        ElseIf hitPosition = "LEFT" Then
            index = 2
        ElseIf hitPosition = "BACK" Then
            index = 3
        ElseIf hitPosition = "FRONT" Then
            index = 4
        ElseIf hitPosition = "TOP" Then
            index = 5
        ElseIf hitPosition = "BOTTOM" Then
            index = 6
        End If
        Return index
    End Function

End Class


﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Windows.Forms
Imports System.Threading
Imports System.Diagnostics
Imports System.Globalization

Class gUtility
    Public Shared Function FirstDayOfWeek() As DateTime
        Dim currentCulture = Thread.CurrentThread.CurrentCulture
        Dim fdow = currentCulture.DateTimeFormat.FirstDayOfWeek
        Dim offset = If(System.DateTime.Now.DayOfWeek - fdow < 0, 7, 0)
        Dim numberOfDaysSinceBeginningOfTheWeek = System.DateTime.Now.DayOfWeek + offset - fdow

        Return New DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day).AddDays(-numberOfDaysSinceBeginningOfTheWeek)

    End Function
    Public Shared Function LastDayOfWeek() As DateTime

        Return FirstDayOfWeek().AddDays(6)
    End Function
    Public Shared Function FirstDayOfYear() As DateTime

        Return New DateTime(System.DateTime.Now.Year, 1, 1)
    End Function
    Public Shared Function LastDayofYear() As DateTime
        Return New DateTime(System.DateTime.Now.Year, 12, 31)
    End Function

    Public Shared Function FirstDayOfMonth() As DateTime
        Return New DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, 1)
    End Function
    Public Shared Function LastDayOfMonth() As DateTime
        Return New DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1)
    End Function
    Public Shared Function getWeek([date] As DateTime) As Integer
        Dim cult_info As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("no")
        Dim cal As System.Globalization.Calendar = cult_info.Calendar
        Dim weekCount As Integer = cal.GetWeekOfYear([date], cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek)
        Return weekCount

    End Function
    Private Function gFindLargeIndex(my40 As [Double]()) As Integer
        Dim i As Integer = 0
        Dim index As Integer = 0
        Dim tmp As Double = 0

        ' gsi = Accumulation a(t)power2.5dt (in impact duration)
        For i = 0 To 39
            If tmp > my40(i) Then
                tmp = my40(i)
                index = i
            End If
        Next
        Return i

    End Function
    '''  <summary>
    '''  Provides a central mechanism for exception handling.
    '''  Displays a message box that describes the exception.
    '''  </summary>
    '''  
    '''  <param name="moduleName"> the module where the exception occurred. </param>
    '''  <param name="e"> the exception </param>

    Public Shared Sub DisplayException(moduleName As [String], e As Exception)
        Dim message As [String] = Nothing
        Dim caption As [String] = Nothing
        '  Create an error message.
        message = "Please report this:" & vbCr & vbLf
        message += "Error: " & e.Message & vbCr & vbLf & "Module: " & moduleName & vbCr & vbLf & "Method: " & e.TargetSite.Name
        caption = "We are sorry"
        MessageBox.Show(message, caption, MessageBoxButtons.OK)
        Debug.Write(message)
    End Sub

    'ToDo need change it to be more efficent
    ' String.ToCharArray();
    Public Shared Function StringToByteArray(hex As String) As Byte()
        Dim hexString As String = Nothing
        hexString = String_To_Hex(hex)
        If hexString.Length Mod 2 <> 0 Then
            Throw New ArgumentException([String].Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString))
        End If

        Dim HexAsBytes As Byte() = New Byte(hexString.Length \ 2 - 1) {}
        For index As Integer = 0 To HexAsBytes.Length - 1
            Dim byteValue As String = hexString.Substring(index * 2, 2)
            HexAsBytes(index) = Byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture)
        Next

        Return HexAsBytes
    End Function

    Public Shared Function String_To_Hex(ByVal sIn As String) As String
        Dim encText As New System.Text.UTF8Encoding
        Dim outByte() As Byte = Nothing

        Dim sVal As String
        Dim sHex As String = ""
        While sIn.Length > 0
            sVal = Conversion.Hex(Strings.Asc(sIn.Substring(0, 1).ToString()))
            sIn = sIn.Substring(1, sIn.Length - 1)
            sHex = sHex & sVal
        End While

        Return sHex
    End Function

    Public Shared Function gidToString(ByRef GID As Byte()) As String
        Return String.Format("{0:X2}", GID(5)) & String.Format("{0:X2}", GID(4)) & String.Format("{0:X2}", GID(3)) & String.Format("{0:X2}", GID(2)) & String.Format("{0:X2}", GID(1)) & String.Format("{0:X2}", GID(0))

    End Function
    Public Shared Function byteToShort(h As Byte, l As Byte) As UShort
        Return CUShort((CUShort(h) << 8) Or l)
    End Function
    Public Shared Function byteToSignedShort(h As Byte, l As Byte) As Short
        Return CShort((CShort(h) << 8) Or l)

    End Function
    Public Shared Function ValidateTime(ByRef outTime As DateTime, year As Byte, month As Byte, day As Byte, hour As Byte, minute As Byte, _
        second As Byte, milli As UShort) As Boolean
        Dim format As String = "yyyy:MM:dd:HH:mm:ss:fff"
        Dim myTime As String
        myTime = String.Format("{0:d2}", 2000 + year) & ":"
        myTime += String.Format("{0:d2}", month) & ":"
        myTime += String.Format("{0:d2}", day) & ":"
        myTime += String.Format("{0:d2}", hour) & ":"
        myTime += String.Format("{0:d2}", minute) & ":"
        myTime += String.Format("{0:d2}", second) & ":"

        myTime += String.Format("{0:d3}", milli)

        Return DateTime.TryParseExact(myTime, format, CultureInfo.InvariantCulture, DateTimeStyles.None, outTime)

    End Function
End Class


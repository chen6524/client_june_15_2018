﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Runtime.InteropServices
Class gNativeMethod
    Public Const WM_USER As Integer = &H400
    Public Const HWND_BROADCAST As Integer = &HFFFF
    Public Const WM_COPYDATA As Integer = &H4A
    Public Shared ReadOnly WM_SHOWME As Integer = RegisterWindowMessage("WM_SHOWME")


    <DllImport("User32.dll")> _
    Private Shared Function RegisterWindowMessage(lpString As String) As Integer
    End Function

    <DllImport("User32.dll", EntryPoint:="FindWindow")> _
    Public Shared Function FindWindow(lpClassName As [String], lpWindowName As [String]) As Int32
    End Function

    'For use with WM_COPYDATA and COPYDATASTRUCT
    <DllImport("User32.dll", EntryPoint:="SendMessage")> _
    Public Shared Function SendMessage(hWnd As IntPtr, Msg As Integer, wParam As Integer, ByRef lParam As COPYDATASTRUCT) As Integer
    End Function

    'For use with WM_COPYDATA and COPYDATASTRUCT
    <DllImport("User32.dll", EntryPoint:="PostMessage")> _
    Public Shared Function PostMessage(hWnd As IntPtr, Msg As Integer, wParam As Integer, ByRef lParam As COPYDATASTRUCT) As Integer
    End Function

    <DllImport("User32.dll", EntryPoint:="SendMessage")> _
    Public Shared Function SendMessage(hWnd As IntPtr, Msg As Integer, wParam As Integer, lParam As Integer) As Integer
    End Function

    <DllImport("User32.dll", EntryPoint:="PostMessage")> _
    Public Shared Function PostMessage(hWnd As IntPtr, Msg As Integer, wParam As Integer, lParam As Integer) As Integer
    End Function

    <DllImport("User32.dll", EntryPoint:="SetForegroundWindow")> _
    Public Shared Function SetForegroundWindow(hWnd As IntPtr) As Boolean
    End Function



    'Used for WM_COPYDATA for string messages
    Public Structure COPYDATASTRUCT
        Public dwData As IntPtr
        Public cbData As Integer
        <MarshalAs(UnmanagedType.LPStr)> _
        Public lpData As String
    End Structure

    Public Shared Function bringAppToFront(hWnd As IntPtr) As Boolean
        Return SetForegroundWindow(hWnd)
    End Function

    Public Shared Function sendWindowsStringMessage(hWnd As IntPtr, wParam As Integer, msg As String) As Integer
        Dim result As Integer = 0

        If hWnd <> IntPtr.Zero Then
            Dim sarr As Byte() = System.Text.Encoding.[Default].GetBytes(msg)
            Dim len As Integer = sarr.Length
            Dim cds As COPYDATASTRUCT
            cds.dwData = CType(100, IntPtr)
            cds.lpData = msg
            cds.cbData = len + 1
            result = SendMessage(hWnd, WM_COPYDATA, wParam, cds)
        End If

        Return result
    End Function

    Public Shared Function sendWindowsMessage(hWnd As IntPtr, Msg As Integer, wParam As Integer, lParam As Integer) As Integer
        Dim result As Integer = 0

        If hWnd <> IntPtr.Zero Then
            result = SendMessage(hWnd, Msg, wParam, lParam)
        End If

        Return result
    End Function

    Public Shared Function getWindowId(className As String, windowName As String) As Integer
        Return FindWindow(className, windowName)
    End Function
End Class

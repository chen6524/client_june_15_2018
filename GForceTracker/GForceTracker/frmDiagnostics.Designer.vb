﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiagnostics
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDiagnostics))
        Me.lblFirmwareRelease = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.buttonTorchLed = New System.Windows.Forms.Button()
        Me.buttonTestFlash = New System.Windows.Forms.Button()
        Me.buttonTestAcc = New System.Windows.Forms.Button()
        Me.buttonBuzz = New System.Windows.Forms.Button()
        Me.buttonTestGyro = New System.Windows.Forms.Button()
        Me.buttonRTC = New System.Windows.Forms.Button()
        Me.buttonLed = New System.Windows.Forms.Button()
        Me.btnTestAll = New System.Windows.Forms.Button()
        Me.labelTestFlash = New System.Windows.Forms.Label()
        Me.labelTestGyro = New System.Windows.Forms.Label()
        Me.labelTestRTC = New System.Windows.Forms.Label()
        Me.labelTestLed = New System.Windows.Forms.Label()
        Me.labelTestBuzz = New System.Windows.Forms.Label()
        Me.labelTorchLed = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tableDiagnostics = New System.Windows.Forms.TableLayoutPanel()
        Me.labelTestAcc = New System.Windows.Forms.Label()
        Me.lblDesc = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.bwDiagnostics = New System.ComponentModel.BackgroundWorker()
        Me.bwTestLED = New System.ComponentModel.BackgroundWorker()
        Me.btnFactoryDefault = New System.Windows.Forms.Button()
        Me.tableDiagnostics.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblFirmwareRelease
        '
        Me.lblFirmwareRelease.AutoSize = True
        Me.lblFirmwareRelease.Location = New System.Drawing.Point(130, 71)
        Me.lblFirmwareRelease.Name = "lblFirmwareRelease"
        Me.lblFirmwareRelease.Size = New System.Drawing.Size(36, 13)
        Me.lblFirmwareRelease.TabIndex = 32
        Me.lblFirmwareRelease.Text = "<FW>"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(285, 442)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(96, 28)
        Me.btnClose.TabIndex = 30
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(233, 398)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(96, 28)
        Me.btnClear.TabIndex = 29
        Me.btnClear.Text = "C&lear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'buttonTorchLed
        '
        Me.buttonTorchLed.Location = New System.Drawing.Point(4, 250)
        Me.buttonTorchLed.Name = "buttonTorchLed"
        Me.buttonTorchLed.Size = New System.Drawing.Size(114, 28)
        Me.buttonTorchLed.TabIndex = 12
        Me.buttonTorchLed.Text = "Alarm LED"
        Me.buttonTorchLed.UseVisualStyleBackColor = True
        '
        'buttonTestFlash
        '
        Me.buttonTestFlash.Location = New System.Drawing.Point(4, 4)
        Me.buttonTestFlash.Name = "buttonTestFlash"
        Me.buttonTestFlash.Size = New System.Drawing.Size(114, 28)
        Me.buttonTestFlash.TabIndex = 0
        Me.buttonTestFlash.Text = "Flash"
        Me.buttonTestFlash.UseVisualStyleBackColor = True
        '
        'buttonTestAcc
        '
        Me.buttonTestAcc.Location = New System.Drawing.Point(4, 45)
        Me.buttonTestAcc.Name = "buttonTestAcc"
        Me.buttonTestAcc.Size = New System.Drawing.Size(114, 28)
        Me.buttonTestAcc.TabIndex = 2
        Me.buttonTestAcc.Text = "Accelerometer"
        Me.buttonTestAcc.UseVisualStyleBackColor = True
        '
        'buttonBuzz
        '
        Me.buttonBuzz.Location = New System.Drawing.Point(4, 209)
        Me.buttonBuzz.Name = "buttonBuzz"
        Me.buttonBuzz.Size = New System.Drawing.Size(114, 28)
        Me.buttonBuzz.TabIndex = 10
        Me.buttonBuzz.Text = "Buzzer"
        Me.buttonBuzz.UseVisualStyleBackColor = True
        '
        'buttonTestGyro
        '
        Me.buttonTestGyro.Location = New System.Drawing.Point(4, 86)
        Me.buttonTestGyro.Name = "buttonTestGyro"
        Me.buttonTestGyro.Size = New System.Drawing.Size(114, 28)
        Me.buttonTestGyro.TabIndex = 4
        Me.buttonTestGyro.Text = "Gyro"
        Me.buttonTestGyro.UseVisualStyleBackColor = True
        '
        'buttonRTC
        '
        Me.buttonRTC.Location = New System.Drawing.Point(4, 127)
        Me.buttonRTC.Name = "buttonRTC"
        Me.buttonRTC.Size = New System.Drawing.Size(114, 28)
        Me.buttonRTC.TabIndex = 6
        Me.buttonRTC.Text = "RTC"
        Me.buttonRTC.UseVisualStyleBackColor = True
        '
        'buttonLed
        '
        Me.buttonLed.Location = New System.Drawing.Point(4, 168)
        Me.buttonLed.Name = "buttonLed"
        Me.buttonLed.Size = New System.Drawing.Size(114, 28)
        Me.buttonLed.TabIndex = 8
        Me.buttonLed.Text = "LED"
        Me.buttonLed.UseVisualStyleBackColor = True
        '
        'btnTestAll
        '
        Me.btnTestAll.Location = New System.Drawing.Point(70, 398)
        Me.btnTestAll.Name = "btnTestAll"
        Me.btnTestAll.Size = New System.Drawing.Size(96, 28)
        Me.btnTestAll.TabIndex = 28
        Me.btnTestAll.Text = "&Test All"
        Me.btnTestAll.UseVisualStyleBackColor = True
        '
        'labelTestFlash
        '
        Me.labelTestFlash.AutoSize = True
        Me.labelTestFlash.Location = New System.Drawing.Point(125, 1)
        Me.labelTestFlash.Name = "labelTestFlash"
        Me.labelTestFlash.Size = New System.Drawing.Size(13, 13)
        Me.labelTestFlash.TabIndex = 1
        Me.labelTestFlash.Text = "  "
        '
        'labelTestGyro
        '
        Me.labelTestGyro.AutoSize = True
        Me.labelTestGyro.Location = New System.Drawing.Point(125, 83)
        Me.labelTestGyro.Name = "labelTestGyro"
        Me.labelTestGyro.Size = New System.Drawing.Size(10, 13)
        Me.labelTestGyro.TabIndex = 5
        Me.labelTestGyro.Text = " "
        '
        'labelTestRTC
        '
        Me.labelTestRTC.AutoSize = True
        Me.labelTestRTC.Location = New System.Drawing.Point(125, 124)
        Me.labelTestRTC.Name = "labelTestRTC"
        Me.labelTestRTC.Size = New System.Drawing.Size(10, 13)
        Me.labelTestRTC.TabIndex = 7
        Me.labelTestRTC.Text = " "
        '
        'labelTestLed
        '
        Me.labelTestLed.AutoSize = True
        Me.labelTestLed.Location = New System.Drawing.Point(125, 165)
        Me.labelTestLed.Name = "labelTestLed"
        Me.labelTestLed.Size = New System.Drawing.Size(10, 13)
        Me.labelTestLed.TabIndex = 9
        Me.labelTestLed.Text = " "
        '
        'labelTestBuzz
        '
        Me.labelTestBuzz.AutoSize = True
        Me.labelTestBuzz.Location = New System.Drawing.Point(125, 206)
        Me.labelTestBuzz.Name = "labelTestBuzz"
        Me.labelTestBuzz.Size = New System.Drawing.Size(10, 13)
        Me.labelTestBuzz.TabIndex = 11
        Me.labelTestBuzz.Text = " "
        '
        'labelTorchLed
        '
        Me.labelTorchLed.AutoSize = True
        Me.labelTorchLed.Location = New System.Drawing.Point(125, 247)
        Me.labelTorchLed.Name = "labelTorchLed"
        Me.labelTorchLed.Size = New System.Drawing.Size(10, 13)
        Me.labelTorchLed.TabIndex = 13
        Me.labelTorchLed.Text = " "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 13)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Firmware Release:"
        '
        'tableDiagnostics
        '
        Me.tableDiagnostics.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tableDiagnostics.ColumnCount = 2
        Me.tableDiagnostics.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.tableDiagnostics.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 253.0!))
        Me.tableDiagnostics.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 398.0!))
        Me.tableDiagnostics.Controls.Add(Me.buttonTorchLed, 0, 6)
        Me.tableDiagnostics.Controls.Add(Me.buttonTestFlash, 0, 0)
        Me.tableDiagnostics.Controls.Add(Me.buttonTestAcc, 0, 1)
        Me.tableDiagnostics.Controls.Add(Me.buttonBuzz, 0, 5)
        Me.tableDiagnostics.Controls.Add(Me.buttonTestGyro, 0, 2)
        Me.tableDiagnostics.Controls.Add(Me.buttonRTC, 0, 3)
        Me.tableDiagnostics.Controls.Add(Me.buttonLed, 0, 4)
        Me.tableDiagnostics.Controls.Add(Me.labelTestFlash, 2, 0)
        Me.tableDiagnostics.Controls.Add(Me.labelTestAcc, 2, 1)
        Me.tableDiagnostics.Controls.Add(Me.labelTestGyro, 2, 2)
        Me.tableDiagnostics.Controls.Add(Me.labelTestRTC, 2, 3)
        Me.tableDiagnostics.Controls.Add(Me.labelTestLed, 2, 4)
        Me.tableDiagnostics.Controls.Add(Me.labelTestBuzz, 2, 5)
        Me.tableDiagnostics.Controls.Add(Me.labelTorchLed, 2, 6)
        Me.tableDiagnostics.Location = New System.Drawing.Point(12, 99)
        Me.tableDiagnostics.Name = "tableDiagnostics"
        Me.tableDiagnostics.RowCount = 7
        Me.tableDiagnostics.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tableDiagnostics.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tableDiagnostics.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tableDiagnostics.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tableDiagnostics.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tableDiagnostics.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tableDiagnostics.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tableDiagnostics.Size = New System.Drawing.Size(369, 288)
        Me.tableDiagnostics.TabIndex = 27
        '
        'labelTestAcc
        '
        Me.labelTestAcc.AutoSize = True
        Me.labelTestAcc.Location = New System.Drawing.Point(125, 42)
        Me.labelTestAcc.Name = "labelTestAcc"
        Me.labelTestAcc.Size = New System.Drawing.Size(16, 13)
        Me.labelTestAcc.TabIndex = 3
        Me.labelTestAcc.Text = "   "
        '
        'lblDesc
        '
        Me.lblDesc.Location = New System.Drawing.Point(13, 35)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(368, 33)
        Me.lblDesc.TabIndex = 26
        Me.lblDesc.Text = "From this screen you are able to test the various components of the GFT Device to" & _
    " ensure that everything is working as expected."
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(12, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(162, 20)
        Me.lblTitle.TabIndex = 25
        Me.lblTitle.Text = "Device Diagnostics"
        '
        'bwDiagnostics
        '
        Me.bwDiagnostics.WorkerReportsProgress = True
        '
        'bwTestLED
        '
        Me.bwTestLED.WorkerReportsProgress = True
        '
        'btnFactoryDefault
        '
        Me.btnFactoryDefault.Location = New System.Drawing.Point(12, 442)
        Me.btnFactoryDefault.Name = "btnFactoryDefault"
        Me.btnFactoryDefault.Size = New System.Drawing.Size(135, 28)
        Me.btnFactoryDefault.TabIndex = 33
        Me.btnFactoryDefault.Text = "Reset Device"
        Me.btnFactoryDefault.UseVisualStyleBackColor = True
        '
        'frmDiagnostics
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(396, 484)
        Me.Controls.Add(Me.btnFactoryDefault)
        Me.Controls.Add(Me.lblFirmwareRelease)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnTestAll)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tableDiagnostics)
        Me.Controls.Add(Me.lblDesc)
        Me.Controls.Add(Me.lblTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmDiagnostics"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Device Diagnostics"
        Me.tableDiagnostics.ResumeLayout(False)
        Me.tableDiagnostics.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblFirmwareRelease As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Private WithEvents buttonTorchLed As System.Windows.Forms.Button
    Private WithEvents buttonTestFlash As System.Windows.Forms.Button
    Private WithEvents buttonTestAcc As System.Windows.Forms.Button
    Private WithEvents buttonBuzz As System.Windows.Forms.Button
    Private WithEvents buttonTestGyro As System.Windows.Forms.Button
    Private WithEvents buttonRTC As System.Windows.Forms.Button
    Private WithEvents buttonLed As System.Windows.Forms.Button
    Friend WithEvents btnTestAll As System.Windows.Forms.Button
    Private WithEvents labelTestFlash As System.Windows.Forms.Label
    Private WithEvents labelTestGyro As System.Windows.Forms.Label
    Private WithEvents labelTestRTC As System.Windows.Forms.Label
    Private WithEvents labelTestLed As System.Windows.Forms.Label
    Private WithEvents labelTestBuzz As System.Windows.Forms.Label
    Private WithEvents labelTorchLed As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents tableDiagnostics As System.Windows.Forms.TableLayoutPanel
    Private WithEvents labelTestAcc As System.Windows.Forms.Label
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents bwDiagnostics As System.ComponentModel.BackgroundWorker
    Friend WithEvents bwTestLED As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnFactoryDefault As System.Windows.Forms.Button
End Class

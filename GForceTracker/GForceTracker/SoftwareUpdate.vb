﻿
Public Class SoftwareUpdate

    Private Files As List(Of FileDownload) = New List(Of FileDownload)
    Private CancellationPending As Boolean = False

    Public Sub New()
        Dim HasWirelessViewer = System.IO.File.Exists(Application.StartupPath & "/WirelessGFTViewer.dll") ' Check if the Wireless Viewer is Installed

        ' Main Executable
        Files.Add(New FileDownload(Me, "http://gft.gforcetracker.com/software/GForceTracker.exe", Application.ExecutablePath, "GForceTracker.old", True)) ' Always replace

        If HasWirelessViewer Then ' Only update the wireless viewer if the user has it installed already
            Files.Add(New FileDownload(Me, "http://gft.gforcetracker.com/software/WirelessGFTViewer.dll", Application.StartupPath & "\\WirelessGFTViewer.dll", Replace("WirelessGFTViewer.dll".ToLower, ".dll", ".old"), True)) ' Always replace
            Files.Add(New FileDownload(Me, "http://gft.gforcetracker.com/software/ArtaFlexHidIO.dll", Application.StartupPath & "\\ArtaFlexHidIO.dll", Replace("ArtaFlexHidIO.dll".ToLower, ".dll", ".old"), True)) ' Always replace
            Files.Add(New FileDownload(Me, "http://gft.gforcetracker.com/software/gForceTracker.sdf", Application.StartupPath & "\\gForceTracker.sdf", Replace("gForceTracker.sdf".ToLower, ".sdf", ".old"), False)) ' Never replace
        End If
    End Sub

    Public Sub NotifyDownloadComplete()
        If checkComplete() Then UpdateComplete()
    End Sub

    Public Sub NotifyDownloadFailed()
        UpdateFailed()
    End Sub

    Private Function checkComplete()
        Dim Complete = True
        For Each key As FileDownload In Files
            If Not key.Complete Then Complete = False
        Next
        Return Complete
    End Function

    Private Sub UpdateComplete()
        MessageBox.Show("GForceTracker software has been successfully updated." & vbCrLf & vbCrLf & "Please wait while the program restarts.")
        frmNotify.Close()
        Application.Restart()
    End Sub


    Private Sub UpdateFailed()
        If Not CancellationPending Then
            CancellationPending = True
            MessageBox.Show("Software Update Failed." & vbCrLf & vbCrLf & "Please check your Internet Connectivity and ensure" & vbCrLf & "any Anti-Virus or Firewall settings are not blocking" & vbCrLf & "the update." & vbCrLf & vbCrLf & "You may also update by downloading and running" & vbCrLf & " the Setup Program from http://gft.gforcetracker.com")
            For Each key As FileDownload In Files
                If Not key.Complete And Not key.CancelPending Then key.cancel()
            Next
        End If
        Dim cancelComplete = True
        For Each key As FileDownload In Files
            If Not key.Complete And Not key.Cancelled Then cancelComplete = False
        Next
        If cancelComplete Then
            frmNotify.Close()
            Application.Exit()
        End If
    End Sub

End Class

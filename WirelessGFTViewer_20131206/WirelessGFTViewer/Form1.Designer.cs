﻿


namespace gForceTrackerWireless
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 50D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 56D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 10D);
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, -20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, -30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, -25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, -50D);
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 50D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 56D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 10D);
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, -20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, -30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint15 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, -25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint16 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, -50D);
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title6 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title7 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint17 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 50D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint18 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint19 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 56D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint20 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 10D);
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint21 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, -20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint22 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, -30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint23 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, -25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint24 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, -50D);
            System.Windows.Forms.DataVisualization.Charting.Title title8 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title9 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title10 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelChart = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelGFT = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_pStaticMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.mtimer = new System.Windows.Forms.Timer(this.components);
            this.openFileDialogFirm = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.dashboard = new System.Windows.Forms.TableLayoutPanel();
            this.teamChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mBT_Name = new System.Windows.Forms.ComboBox();
            this.mBT_Addr = new System.Windows.Forms.ComboBox();
            this.mConnBle = new System.Windows.Forms.Button();
            this.mGetNodeInfo = new System.Windows.Forms.Button();
            this.mGetHubInfo = new System.Windows.Forms.Button();
            this.mInitBLE = new System.Windows.Forms.Button();
            this.mEraseWholeData = new System.Windows.Forms.CheckBox();
            this.simUploads = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrGForce = new System.Windows.Forms.Label();
            this.lblTeamGForceAveMed = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblTeamGForce = new System.Windows.Forms.Label();
            this.lnkGForce = new System.Windows.Forms.LinkLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrRotation = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblTeamRotationAveMed = new System.Windows.Forms.Label();
            this.lnkRotation = new System.Windows.Forms.LinkLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrThreshold = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblTeamThreshold = new System.Windows.Forms.Label();
            this.lblTeamThresholdWithin = new System.Windows.Forms.Label();
            this.lblTeamThresholdImpacts = new System.Windows.Forms.Label();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.linkRPE = new System.Windows.Forms.LinkLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.linkPL = new System.Windows.Forms.LinkLabel();
            this.HdrExposure = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblTeamExposure = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.lblExpSec = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.linkEP = new System.Windows.Forms.LinkLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnEmptyHub = new System.Windows.Forms.Button();
            this.powerOffGFTs = new System.Windows.Forms.Button();
            this.UploadAll = new System.Windows.Forms.Button();
            this.powerOnGFTs = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mStopBroadCast = new System.Windows.Forms.Button();
            this.mTryCount = new System.Windows.Forms.Label();
            this.DongleEmpty = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.playerReviewer = new WirelessGFTViewer.PlayerReview();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ActivePct = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridViewPlayer = new System.Windows.Forms.DataGridView();
            this.chartViewerPlayer = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrResGForce = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblGForceAveMed = new System.Windows.Forms.Label();
            this.lblHighestG = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrResImpacts = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblThresholdTotal = new System.Windows.Forms.Label();
            this.lblThresholdWithinAbove = new System.Windows.Forms.Label();
            this.lblThresholdImpacts = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrResRotation = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblRotationAveMed = new System.Windows.Forms.Label();
            this.lblHighestR = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrResExposure = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblExposure = new System.Windows.Forms.Label();
            this.activityChartPlayer = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pbHead = new System.Windows.Forms.PictureBox();
            this.pbHeatMap = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPlayer = new System.Windows.Forms.Label();
            this.lblDeviceSettings = new System.Windows.Forms.Label();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pbBatStat = new System.Windows.Forms.PictureBox();
            this.locationChartPlayer = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblRecovery = new System.Windows.Forms.Label();
            this.consumerPlayerPanel = new System.Windows.Forms.TableLayoutPanel();
            this.perfChartPL = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.perfChartEP = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.perfChartViewerPlayer = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPerfPlayer = new System.Windows.Forms.Label();
            this.lblPerfStatus = new System.Windows.Forms.Label();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPerfBack = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblPerfRecovery = new System.Windows.Forms.Label();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPerfGForceAve = new System.Windows.Forms.Label();
            this.HdrPerfGForce = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPerfGForceMed = new System.Windows.Forms.Label();
            this.lblPerfHighestG = new System.Windows.Forms.Label();
            this.panel111 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPerfRotationAve = new System.Windows.Forms.Label();
            this.HdrPerfRotation = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblPerfRotationMed = new System.Windows.Forms.Label();
            this.lblPerfHighestR = new System.Windows.Forms.Label();
            this.perfLocationChartPlayer = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel101 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.HdrPerfThreshold = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblPerfThresholdAbove = new System.Windows.Forms.Label();
            this.lblPerfThresholdWithinAbove = new System.Windows.Forms.Label();
            this.lblPerfThresholdImpacts = new System.Windows.Forms.Label();
            this.pbPerfHead = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPerfEPRating = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPerfEPHighest = new System.Windows.Forms.Label();
            this.lblPerfEP = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblPerfRPE = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPerfPLHighest = new System.Windows.Forms.Label();
            this.lblPerfPLAve = new System.Windows.Forms.Label();
            this.lblPerfPlayerLoadText = new System.Windows.Forms.Label();
            this.HdrPerfPlayerLoad = new System.Windows.Forms.Label();
            this.lblPerfPlayerLoad = new System.Windows.Forms.Label();
            this.perfChartRPE = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel13 = new System.Windows.Forms.Panel();
            this.pbRPEHeatMap = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.liveView = new System.Windows.Forms.TableLayoutPanel();
            this.chartPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBoxHeadW = new System.Windows.Forms.PictureBox();
            this.chartViewer = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.chartRawGyroW = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartRawLinW = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.m_button_Clone = new System.Windows.Forms.Button();
            this.mUpdateByUSB2 = new System.Windows.Forms.Button();
            this.m_timeSync = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.usbUpload_Enable = new System.Windows.Forms.CheckBox();
            this.autoUpload_enable = new System.Windows.Forms.CheckBox();
            this.mWirelessEnable = new System.Windows.Forms.CheckBox();
            this.mbutton_Bind_with_unbind = new System.Windows.Forms.Button();
            this.mGFT = new System.Windows.Forms.Label();
            this.wirelessViewerCtl = new WirelessGFTViewer.WirelessViewer();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.dashboard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamChart)).BeginInit();
            this.tableLayoutPanel27.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActivePct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewerPlayer)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.activityChartPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeatMap)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBatStat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.locationChartPlayer)).BeginInit();
            this.consumerPlayerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartEP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartViewerPlayer)).BeginInit();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tableLayoutPanel29.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.panel111.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.perfLocationChartPlayer)).BeginInit();
            this.panel101.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPerfHead)).BeginInit();
            this.tableLayoutPanel30.SuspendLayout();
            this.panel14.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartRPE)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRPEHeatMap)).BeginInit();
            this.liveView.SuspendLayout();
            this.chartPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeadW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartRawGyroW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartRawLinW)).BeginInit();
            this.buttonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatus,
            this.toolStripStatusLabelChart,
            this.toolStripStatusLabelGFT,
            this.m_pStaticMode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 631);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 28, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1334, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.Name = "toolStripStatus";
            this.toolStripStatus.Size = new System.Drawing.Size(68, 17);
            this.toolStripStatus.Text = "Connected ";
            // 
            // toolStripStatusLabelChart
            // 
            this.toolStripStatusLabelChart.Name = "toolStripStatusLabelChart";
            this.toolStripStatusLabelChart.Size = new System.Drawing.Size(1236, 17);
            this.toolStripStatusLabelChart.Spring = true;
            // 
            // toolStripStatusLabelGFT
            // 
            this.toolStripStatusLabelGFT.Name = "toolStripStatusLabelGFT";
            this.toolStripStatusLabelGFT.Size = new System.Drawing.Size(0, 17);
            // 
            // m_pStaticMode
            // 
            this.m_pStaticMode.Name = "m_pStaticMode";
            this.m_pStaticMode.Size = new System.Drawing.Size(0, 17);
            // 
            // mtimer
            // 
            this.mtimer.Interval = 1500;
            this.mtimer.Tick += new System.EventHandler(this.mtimer_Tick);
            // 
            // openFileDialogFirm
            // 
            this.openFileDialogFirm.FileName = "openFileDialogFirm";
            this.openFileDialogFirm.Filter = "\"Hex file(*.s19)|*.s19";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.buttonPanel);
            this.splitContainer1.Size = new System.Drawing.Size(1334, 631);
            this.splitContainer1.SplitterDistance = 556;
            this.splitContainer1.TabIndex = 30;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            this.splitContainer2.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer2_Panel1_Paint);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.liveView);
            this.splitContainer2.Panel2Collapsed = true;
            this.splitContainer2.Size = new System.Drawing.Size(1334, 556);
            this.splitContainer2.SplitterDistance = 613;
            this.splitContainer2.TabIndex = 30;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.dashboard);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Panel2Collapsed = true;
            this.splitContainer3.Size = new System.Drawing.Size(1334, 556);
            this.splitContainer3.SplitterDistance = 966;
            this.splitContainer3.TabIndex = 1;
            // 
            // dashboard
            // 
            this.dashboard.ColumnCount = 3;
            this.dashboard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.dashboard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.dashboard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.dashboard.Controls.Add(this.teamChart, 1, 1);
            this.dashboard.Controls.Add(this.tableLayoutPanel27, 0, 1);
            this.dashboard.Controls.Add(this.tableLayoutPanel28, 2, 1);
            this.dashboard.Controls.Add(this.label23, 0, 0);
            this.dashboard.Controls.Add(this.label25, 1, 0);
            this.dashboard.Controls.Add(this.label28, 2, 0);
            this.dashboard.Controls.Add(this.playerReviewer, 1, 2);
            this.dashboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dashboard.Location = new System.Drawing.Point(0, 0);
            this.dashboard.Name = "dashboard";
            this.dashboard.RowCount = 3;
            this.dashboard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.dashboard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.dashboard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.dashboard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.dashboard.Size = new System.Drawing.Size(1334, 556);
            this.dashboard.TabIndex = 0;
            // 
            // teamChart
            // 
            this.teamChart.BackSecondaryColor = System.Drawing.Color.White;
            this.teamChart.BorderSkin.BorderColor = System.Drawing.Color.White;
            chartArea1.Area3DStyle.Inclination = 50;
            chartArea1.Area3DStyle.IsRightAngleAxes = false;
            chartArea1.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic;
            chartArea1.Area3DStyle.PointDepth = 5;
            chartArea1.Area3DStyle.PointGapDepth = 5;
            chartArea1.Area3DStyle.Rotation = 0;
            chartArea1.Area3DStyle.WallWidth = 1;
            chartArea1.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.ScrollBar.BackColor = System.Drawing.Color.Silver;
            chartArea1.AxisX.ScrollBar.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            chartArea1.AxisX.Title = " Impacts";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea1.AxisX2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            chartArea1.AxisY.MajorTickMark.Enabled = false;
            chartArea1.AxisY.Title = "gForce";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea1.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea1.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea1.BorderColor = System.Drawing.Color.Gainsboro;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.Name = "Session At A Glance";
            this.teamChart.ChartAreas.Add(chartArea1);
            this.teamChart.Cursor = System.Windows.Forms.Cursors.Cross;
            this.teamChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Enabled = false;
            legend1.Name = "Legend1";
            this.teamChart.Legends.Add(legend1);
            this.teamChart.Location = new System.Drawing.Point(153, 23);
            this.teamChart.Name = "teamChart";
            series1.ChartArea = "Session At A Glance";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series1.Color = System.Drawing.Color.Silver;
            series1.CustomProperties = "LabelStyle=BottomLeft";
            series1.IsVisibleInLegend = false;
            series1.LabelAngle = 45;
            series1.Legend = "Legend1";
            series1.Name = " ";
            series1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series2.ChartArea = "Session At A Glance";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series2.Color = System.Drawing.Color.Silver;
            series2.Legend = "Legend1";
            series2.Name = "Series2";
            series2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series2.Points.Add(dataPoint5);
            series2.Points.Add(dataPoint6);
            series2.Points.Add(dataPoint7);
            series2.Points.Add(dataPoint8);
            this.teamChart.Series.Add(series1);
            this.teamChart.Series.Add(series2);
            this.teamChart.Size = new System.Drawing.Size(1028, 294);
            this.teamChart.TabIndex = 5;
            this.teamChart.Text = "chart1";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            title1.Name = "History Data";
            title1.Text = "Team At A Glance";
            title1.Visible = false;
            this.teamChart.Titles.Add(title1);
            this.teamChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.teamChart_mouseClick);
            this.teamChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.teamChart_MouseMove);
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 1;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Controls.Add(this.groupBox2, 0, 3);
            this.tableLayoutPanel27.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel27.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel27.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(0, 20);
            this.tableLayoutPanel27.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 4;
            this.dashboard.SetRowSpan(this.tableLayoutPanel27, 3);
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel27.Size = new System.Drawing.Size(150, 536);
            this.tableLayoutPanel27.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mBT_Name);
            this.groupBox2.Controls.Add(this.mBT_Addr);
            this.groupBox2.Controls.Add(this.mConnBle);
            this.groupBox2.Controls.Add(this.mGetNodeInfo);
            this.groupBox2.Controls.Add(this.mGetHubInfo);
            this.groupBox2.Controls.Add(this.mInitBLE);
            this.groupBox2.Controls.Add(this.mEraseWholeData);
            this.groupBox2.Controls.Add(this.simUploads);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 378);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(144, 155);
            this.groupBox2.TabIndex = 164;
            this.groupBox2.TabStop = false;
            // 
            // mBT_Name
            // 
            this.mBT_Name.FormattingEnabled = true;
            this.mBT_Name.Location = new System.Drawing.Point(3, 35);
            this.mBT_Name.Name = "mBT_Name";
            this.mBT_Name.Size = new System.Drawing.Size(101, 21);
            this.mBT_Name.TabIndex = 171;
            // 
            // mBT_Addr
            // 
            this.mBT_Addr.FormattingEnabled = true;
            this.mBT_Addr.Location = new System.Drawing.Point(3, 58);
            this.mBT_Addr.Name = "mBT_Addr";
            this.mBT_Addr.Size = new System.Drawing.Size(101, 21);
            this.mBT_Addr.TabIndex = 170;
            this.mBT_Addr.SelectedIndexChanged += new System.EventHandler(this.mBT_Addr_SelectedIndexChanged);
            // 
            // mConnBle
            // 
            this.mConnBle.Location = new System.Drawing.Point(3, 145);
            this.mConnBle.Name = "mConnBle";
            this.mConnBle.Size = new System.Drawing.Size(101, 25);
            this.mConnBle.TabIndex = 169;
            this.mConnBle.Text = "GetNodeProfile";
            this.mConnBle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mConnBle.UseVisualStyleBackColor = true;
            this.mConnBle.Click += new System.EventHandler(this.mConnBle_Click);
            // 
            // mGetNodeInfo
            // 
            this.mGetNodeInfo.Location = new System.Drawing.Point(3, 114);
            this.mGetNodeInfo.Name = "mGetNodeInfo";
            this.mGetNodeInfo.Size = new System.Drawing.Size(89, 25);
            this.mGetNodeInfo.TabIndex = 167;
            this.mGetNodeInfo.Text = "Get Node Info";
            this.mGetNodeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mGetNodeInfo.UseVisualStyleBackColor = true;
            this.mGetNodeInfo.Click += new System.EventHandler(this.mGetNodeInfo_Click);
            // 
            // mGetHubInfo
            // 
            this.mGetHubInfo.Location = new System.Drawing.Point(3, 83);
            this.mGetHubInfo.Name = "mGetHubInfo";
            this.mGetHubInfo.Size = new System.Drawing.Size(89, 25);
            this.mGetHubInfo.TabIndex = 166;
            this.mGetHubInfo.Text = "Get Hub Info";
            this.mGetHubInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mGetHubInfo.UseVisualStyleBackColor = true;
            this.mGetHubInfo.Click += new System.EventHandler(this.mGetHubInfo_Click);
            // 
            // mInitBLE
            // 
            this.mInitBLE.Location = new System.Drawing.Point(3, 8);
            this.mInitBLE.Name = "mInitBLE";
            this.mInitBLE.Size = new System.Drawing.Size(53, 25);
            this.mInitBLE.TabIndex = 165;
            this.mInitBLE.Text = "Init BLE";
            this.mInitBLE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mInitBLE.UseVisualStyleBackColor = true;
            this.mInitBLE.Click += new System.EventHandler(this.mInitBLE_Click);
            // 
            // mEraseWholeData
            // 
            this.mEraseWholeData.AutoSize = true;
            this.mEraseWholeData.Location = new System.Drawing.Point(64, 12);
            this.mEraseWholeData.Margin = new System.Windows.Forms.Padding(6);
            this.mEraseWholeData.Name = "mEraseWholeData";
            this.mEraseWholeData.Size = new System.Drawing.Size(79, 17);
            this.mEraseWholeData.TabIndex = 163;
            this.mEraseWholeData.Text = "Erase Data";
            this.mEraseWholeData.UseVisualStyleBackColor = true;
            // 
            // simUploads
            // 
            this.simUploads.Location = new System.Drawing.Point(107, 35);
            this.simUploads.Margin = new System.Windows.Forms.Padding(6);
            this.simUploads.Name = "simUploads";
            this.simUploads.Size = new System.Drawing.Size(30, 20);
            this.simUploads.TabIndex = 159;
            this.simUploads.Text = "2";
            this.simUploads.TextChanged += new System.EventHandler(this.simUploads_TextChanged);
            this.simUploads.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.simUploads_KeyPress);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Window;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tableLayoutPanel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 128);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(144, 119);
            this.panel2.TabIndex = 69;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.HdrGForce, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblTeamGForceAveMed, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblTeamGForce, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lnkGForce, 0, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(142, 117);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // HdrGForce
            // 
            this.HdrGForce.AutoSize = true;
            this.HdrGForce.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrGForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrGForce.Location = new System.Drawing.Point(3, 0);
            this.HdrGForce.Name = "HdrGForce";
            this.HdrGForce.Size = new System.Drawing.Size(136, 23);
            this.HdrGForce.TabIndex = 0;
            this.HdrGForce.Text = "gForce";
            // 
            // lblTeamGForceAveMed
            // 
            this.lblTeamGForceAveMed.AutoSize = true;
            this.lblTeamGForceAveMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTeamGForceAveMed.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamGForceAveMed.Location = new System.Drawing.Point(3, 97);
            this.lblTeamGForceAveMed.Name = "lblTeamGForceAveMed";
            this.lblTeamGForceAveMed.Size = new System.Drawing.Size(136, 20);
            this.lblTeamGForceAveMed.TabIndex = 3;
            this.lblTeamGForceAveMed.Text = "Average: 0g, Median: 0g";
            this.lblTeamGForceAveMed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Highest";
            // 
            // lblTeamGForce
            // 
            this.lblTeamGForce.AutoSize = true;
            this.lblTeamGForce.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTeamGForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamGForce.Location = new System.Drawing.Point(3, 40);
            this.lblTeamGForce.Name = "lblTeamGForce";
            this.lblTeamGForce.Size = new System.Drawing.Size(136, 40);
            this.lblTeamGForce.TabIndex = 1;
            this.lblTeamGForce.Text = "0";
            this.lblTeamGForce.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTeamGForce.Visible = false;
            // 
            // lnkGForce
            // 
            this.lnkGForce.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.lnkGForce.AutoSize = true;
            this.lnkGForce.BackColor = System.Drawing.Color.Transparent;
            this.lnkGForce.LinkColor = System.Drawing.Color.Black;
            this.lnkGForce.Location = new System.Drawing.Point(3, 80);
            this.lnkGForce.Name = "lnkGForce";
            this.lnkGForce.Size = new System.Drawing.Size(55, 13);
            this.lnkGForce.TabIndex = 5;
            this.lnkGForce.TabStop = true;
            this.lnkGForce.Text = "linkLabel1";
            this.lnkGForce.Visible = false;
            this.lnkGForce.VisitedLinkColor = System.Drawing.Color.Black;
            this.lnkGForce.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lnkHighestG_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Window;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.tableLayoutPanel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 253);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(144, 119);
            this.panel3.TabIndex = 70;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Controls.Add(this.HdrRotation, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label22, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.lblTeamRotationAveMed, 0, 5);
            this.tableLayoutPanel7.Controls.Add(this.lnkRotation, 0, 4);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 6;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(142, 117);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // HdrRotation
            // 
            this.HdrRotation.AutoSize = true;
            this.HdrRotation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrRotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrRotation.Location = new System.Drawing.Point(3, 0);
            this.HdrRotation.Name = "HdrRotation";
            this.HdrRotation.Size = new System.Drawing.Size(136, 19);
            this.HdrRotation.TabIndex = 0;
            this.HdrRotation.Text = "Rotation";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Highest(°/s)";
            // 
            // lblTeamRotationAveMed
            // 
            this.lblTeamRotationAveMed.AutoSize = true;
            this.lblTeamRotationAveMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTeamRotationAveMed.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamRotationAveMed.Location = new System.Drawing.Point(3, 94);
            this.lblTeamRotationAveMed.Name = "lblTeamRotationAveMed";
            this.lblTeamRotationAveMed.Size = new System.Drawing.Size(136, 23);
            this.lblTeamRotationAveMed.TabIndex = 3;
            this.lblTeamRotationAveMed.Text = "Average: 0°/s, Median: 0°/s";
            this.lblTeamRotationAveMed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lnkRotation
            // 
            this.lnkRotation.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.lnkRotation.AutoSize = true;
            this.lnkRotation.BackColor = System.Drawing.Color.Transparent;
            this.lnkRotation.LinkColor = System.Drawing.Color.Black;
            this.lnkRotation.Location = new System.Drawing.Point(3, 80);
            this.lnkRotation.Name = "lnkRotation";
            this.lnkRotation.Size = new System.Drawing.Size(55, 13);
            this.lnkRotation.TabIndex = 5;
            this.lnkRotation.TabStop = true;
            this.lnkRotation.Text = "linkLabel2";
            this.lnkRotation.Visible = false;
            this.lnkRotation.VisitedLinkColor = System.Drawing.Color.Black;
            this.lnkRotation.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lnkHighestR_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tableLayoutPanel6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(144, 119);
            this.panel1.TabIndex = 68;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Paint);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.HdrThreshold, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label17, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblTeamThreshold, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.lblTeamThresholdWithin, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.lblTeamThresholdImpacts, 0, 2);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 5;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(142, 117);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // HdrThreshold
            // 
            this.HdrThreshold.AutoSize = true;
            this.HdrThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrThreshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrThreshold.Location = new System.Drawing.Point(3, 0);
            this.HdrThreshold.Name = "HdrThreshold";
            this.HdrThreshold.Size = new System.Drawing.Size(136, 23);
            this.HdrThreshold.TabIndex = 0;
            this.HdrThreshold.Text = "Threshold Impacts";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Within or Above Threshold";
            // 
            // lblTeamThreshold
            // 
            this.lblTeamThreshold.AutoSize = true;
            this.lblTeamThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTeamThreshold.Location = new System.Drawing.Point(3, 97);
            this.lblTeamThreshold.Name = "lblTeamThreshold";
            this.lblTeamThreshold.Size = new System.Drawing.Size(136, 20);
            this.lblTeamThreshold.TabIndex = 3;
            this.lblTeamThreshold.Text = "Above: 0 (0%)";
            this.lblTeamThreshold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTeamThresholdWithin
            // 
            this.lblTeamThresholdWithin.AutoSize = true;
            this.lblTeamThresholdWithin.Location = new System.Drawing.Point(3, 80);
            this.lblTeamThresholdWithin.Name = "lblTeamThresholdWithin";
            this.lblTeamThresholdWithin.Size = new System.Drawing.Size(49, 13);
            this.lblTeamThresholdWithin.TabIndex = 2;
            this.lblTeamThresholdWithin.Text = "Within: 0";
            this.lblTeamThresholdWithin.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblTeamThresholdImpacts
            // 
            this.lblTeamThresholdImpacts.AutoSize = true;
            this.lblTeamThresholdImpacts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTeamThresholdImpacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamThresholdImpacts.Location = new System.Drawing.Point(3, 40);
            this.lblTeamThresholdImpacts.Name = "lblTeamThresholdImpacts";
            this.lblTeamThresholdImpacts.Size = new System.Drawing.Size(136, 40);
            this.lblTeamThresholdImpacts.TabIndex = 1;
            this.lblTeamThresholdImpacts.Text = "0";
            this.lblTeamThresholdImpacts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTeamThresholdImpacts.Visible = false;
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 1;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel28.Controls.Add(this.panel11, 0, 2);
            this.tableLayoutPanel28.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.panel10, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.tableLayoutPanel4, 0, 3);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(1184, 20);
            this.tableLayoutPanel28.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 4;
            this.dashboard.SetRowSpan(this.tableLayoutPanel28, 3);
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel28.Size = new System.Drawing.Size(150, 536);
            this.tableLayoutPanel28.TabIndex = 8;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.Window;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.tableLayoutPanel25);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(3, 253);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(144, 119);
            this.panel11.TabIndex = 77;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 1;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Controls.Add(this.linkRPE, 0, 4);
            this.tableLayoutPanel25.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 5;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(142, 117);
            this.tableLayoutPanel25.TabIndex = 1;
            // 
            // linkRPE
            // 
            this.linkRPE.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.linkRPE.AutoSize = true;
            this.linkRPE.BackColor = System.Drawing.Color.Transparent;
            this.linkRPE.LinkColor = System.Drawing.Color.Black;
            this.linkRPE.Location = new System.Drawing.Point(3, 97);
            this.linkRPE.Name = "linkRPE";
            this.linkRPE.Size = new System.Drawing.Size(55, 13);
            this.linkRPE.TabIndex = 6;
            this.linkRPE.TabStop = true;
            this.linkRPE.Text = "linkLabel1";
            this.linkRPE.Visible = false;
            this.linkRPE.VisitedLinkColor = System.Drawing.Color.Black;
            this.linkRPE.Click += new System.EventHandler(this.linkRPE_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Highest";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 23);
            this.label7.TabIndex = 0;
            this.label7.Text = "Exertion";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 40);
            this.label10.Name = "label10";
            this.tableLayoutPanel25.SetRowSpan(this.label10, 2);
            this.label10.Size = new System.Drawing.Size(136, 57);
            this.label10.TabIndex = 1;
            this.label10.Text = "0";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label10.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Window;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.tableLayoutPanel8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(144, 119);
            this.panel4.TabIndex = 71;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.linkPL, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.HdrExposure, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label26, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.lblTeamExposure, 0, 2);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 5;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(142, 117);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // linkPL
            // 
            this.linkPL.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.linkPL.AutoSize = true;
            this.linkPL.BackColor = System.Drawing.Color.Transparent;
            this.linkPL.LinkColor = System.Drawing.Color.Black;
            this.linkPL.Location = new System.Drawing.Point(3, 97);
            this.linkPL.Name = "linkPL";
            this.linkPL.Size = new System.Drawing.Size(55, 13);
            this.linkPL.TabIndex = 6;
            this.linkPL.TabStop = true;
            this.linkPL.Text = "linkLabel1";
            this.linkPL.Visible = false;
            this.linkPL.VisitedLinkColor = System.Drawing.Color.Black;
            this.linkPL.Click += new System.EventHandler(this.linkPL_Click);
            // 
            // HdrExposure
            // 
            this.HdrExposure.AutoSize = true;
            this.HdrExposure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrExposure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrExposure.Location = new System.Drawing.Point(3, 0);
            this.HdrExposure.Name = "HdrExposure";
            this.HdrExposure.Size = new System.Drawing.Size(136, 23);
            this.HdrExposure.TabIndex = 0;
            this.HdrExposure.Text = "Player Load";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Location = new System.Drawing.Point(3, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(136, 17);
            this.label26.TabIndex = 3;
            this.label26.Text = "Highest";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label26.Click += new System.EventHandler(this.label26_Click);
            // 
            // lblTeamExposure
            // 
            this.lblTeamExposure.AutoSize = true;
            this.lblTeamExposure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTeamExposure.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamExposure.Location = new System.Drawing.Point(3, 40);
            this.lblTeamExposure.Name = "lblTeamExposure";
            this.tableLayoutPanel8.SetRowSpan(this.lblTeamExposure, 2);
            this.lblTeamExposure.Size = new System.Drawing.Size(136, 57);
            this.lblTeamExposure.TabIndex = 1;
            this.lblTeamExposure.Text = "0";
            this.lblTeamExposure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTeamExposure.Visible = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.Window;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.tableLayoutPanel16);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(3, 128);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(144, 119);
            this.panel10.TabIndex = 76;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.lblExpSec, 0, 4);
            this.tableLayoutPanel16.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel16.Controls.Add(this.linkEP, 0, 3);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 5;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(142, 117);
            this.tableLayoutPanel16.TabIndex = 1;
            // 
            // lblExpSec
            // 
            this.lblExpSec.AutoSize = true;
            this.lblExpSec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExpSec.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpSec.Location = new System.Drawing.Point(3, 97);
            this.lblExpSec.Name = "lblExpSec";
            this.lblExpSec.Size = new System.Drawing.Size(136, 20);
            this.lblExpSec.TabIndex = 7;
            this.lblExpSec.Text = "0 Seconds";
            this.lblExpSec.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Highest";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Explosive Power";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 40);
            this.label6.TabIndex = 1;
            this.label6.Text = "0";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label6.Visible = false;
            // 
            // linkEP
            // 
            this.linkEP.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.linkEP.AutoSize = true;
            this.linkEP.BackColor = System.Drawing.Color.Transparent;
            this.linkEP.LinkColor = System.Drawing.Color.Black;
            this.linkEP.Location = new System.Drawing.Point(3, 80);
            this.linkEP.Name = "linkEP";
            this.linkEP.Size = new System.Drawing.Size(55, 13);
            this.linkEP.TabIndex = 6;
            this.linkEP.TabStop = true;
            this.linkEP.Text = "linkLabel1";
            this.linkEP.Visible = false;
            this.linkEP.VisitedLinkColor = System.Drawing.Color.Black;
            this.linkEP.Click += new System.EventHandler(this.linkEP_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.btnEmptyHub, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.powerOffGFTs, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.UploadAll, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.powerOnGFTs, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 375);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(150, 255);
            this.tableLayoutPanel4.TabIndex = 78;
            // 
            // btnEmptyHub
            // 
            this.btnEmptyHub.AutoSize = true;
            this.btnEmptyHub.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEmptyHub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEmptyHub.Location = new System.Drawing.Point(3, 228);
            this.btnEmptyHub.Name = "btnEmptyHub";
            this.btnEmptyHub.Size = new System.Drawing.Size(200, 24);
            this.btnEmptyHub.TabIndex = 76;
            this.btnEmptyHub.Text = "Empty Hub";
            this.btnEmptyHub.UseVisualStyleBackColor = true;
            this.btnEmptyHub.Click += new System.EventHandler(this.btnEmptyHub_Click);
            // 
            // powerOffGFTs
            // 
            this.powerOffGFTs.AutoSize = true;
            this.powerOffGFTs.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.powerOffGFTs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.powerOffGFTs.Location = new System.Drawing.Point(3, 198);
            this.powerOffGFTs.Name = "powerOffGFTs";
            this.powerOffGFTs.Size = new System.Drawing.Size(200, 24);
            this.powerOffGFTs.TabIndex = 74;
            this.powerOffGFTs.Text = "Power Off All";
            this.powerOffGFTs.UseVisualStyleBackColor = true;
            this.powerOffGFTs.Click += new System.EventHandler(this.wirelessViewer_OnButtonPowerOffAll);
            // 
            // UploadAll
            // 
            this.UploadAll.AutoSize = true;
            this.UploadAll.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.UploadAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UploadAll.Location = new System.Drawing.Point(3, 168);
            this.UploadAll.Name = "UploadAll";
            this.UploadAll.Size = new System.Drawing.Size(200, 24);
            this.UploadAll.TabIndex = 75;
            this.UploadAll.Text = "Download All";
            this.UploadAll.UseVisualStyleBackColor = true;
            this.UploadAll.Click += new System.EventHandler(this.wirelessViewer_OnButtonUploadAll);
            // 
            // powerOnGFTs
            // 
            this.powerOnGFTs.AutoSize = true;
            this.powerOnGFTs.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.powerOnGFTs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.powerOnGFTs.Location = new System.Drawing.Point(3, 138);
            this.powerOnGFTs.Name = "powerOnGFTs";
            this.powerOnGFTs.Size = new System.Drawing.Size(200, 24);
            this.powerOnGFTs.TabIndex = 73;
            this.powerOnGFTs.Text = "Power On All";
            this.powerOnGFTs.UseVisualStyleBackColor = true;
            this.powerOnGFTs.Click += new System.EventHandler(this.wirelessViewer_OnButtonPowerOnAll);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mStopBroadCast);
            this.groupBox1.Controls.Add(this.mTryCount);
            this.groupBox1.Controls.Add(this.DongleEmpty);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 129);
            this.groupBox1.TabIndex = 77;
            this.groupBox1.TabStop = false;
            // 
            // mStopBroadCast
            // 
            this.mStopBroadCast.Location = new System.Drawing.Point(11, 54);
            this.mStopBroadCast.Name = "mStopBroadCast";
            this.mStopBroadCast.Size = new System.Drawing.Size(88, 23);
            this.mStopBroadCast.TabIndex = 2;
            this.mStopBroadCast.Text = "Stop broadcast";
            this.mStopBroadCast.UseVisualStyleBackColor = true;
            this.mStopBroadCast.Click += new System.EventHandler(this.mStopBroadCast_Click);
            // 
            // mTryCount
            // 
            this.mTryCount.AutoSize = true;
            this.mTryCount.Location = new System.Drawing.Point(9, 38);
            this.mTryCount.Name = "mTryCount";
            this.mTryCount.Size = new System.Drawing.Size(13, 13);
            this.mTryCount.TabIndex = 1;
            this.mTryCount.Text = "0";
            // 
            // DongleEmpty
            // 
            this.DongleEmpty.AutoSize = true;
            this.DongleEmpty.Location = new System.Drawing.Point(9, 16);
            this.DongleEmpty.Name = "DongleEmpty";
            this.DongleEmpty.Size = new System.Drawing.Size(38, 13);
            this.DongleEmpty.TabIndex = 0;
            this.DongleEmpty.Text = "NONE";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(144, 20);
            this.label23.TabIndex = 9;
            this.label23.Text = "IMPACTS";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(153, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(1028, 20);
            this.label25.TabIndex = 10;
            this.label25.Text = "TEAM AT A GLANCE";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(1187, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(144, 20);
            this.label28.TabIndex = 11;
            this.label28.Text = "PERFORMANCE";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerReviewer
            // 
            this.playerReviewer.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.playerReviewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerReviewer.Location = new System.Drawing.Point(150, 320);
            this.playerReviewer.Margin = new System.Windows.Forms.Padding(0);
            this.playerReviewer.MinimumSize = new System.Drawing.Size(200, 100);
            this.playerReviewer.Name = "playerReviewer";
            this.playerReviewer.node_ID = 0;
            this.playerReviewer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dashboard.SetRowSpan(this.playerReviewer, 2);
            this.playerReviewer.selectedRow = 0;
            this.playerReviewer.Size = new System.Drawing.Size(1034, 236);
            this.playerReviewer.TabIndex = 0;
            this.playerReviewer.OnRowSelected += new WirelessGFTViewer.LightOnChangedEventHandler(this.wirelessViewer_OnRowSelected);
            this.playerReviewer.OnButtonUploadAll += new WirelessGFTViewer.UploadAllHandler(this.wirelessViewer_OnButtonUploadAll);
            this.playerReviewer.OnButtonPowerOffAll += new WirelessGFTViewer.PowerOffAllHandler(this.wirelessViewer_OnButtonPowerOffAll);
            this.playerReviewer.OnButtonEnableProximityAll += new WirelessGFTViewer.EnableProximityAllHandler(this.wirelessViewer_OnButtonEnableProximityAll);
            this.playerReviewer.OnButtonDisableProximityAll += new WirelessGFTViewer.DisableProximityAllHandler(this.wirelessViewer_OnButtonDisableProximityAll);
            this.playerReviewer.OnButtonStopRecordingAll += new WirelessGFTViewer.StopRecordingAllHandler(this.wirelessViewer_OnButtonStopRecordingAll);
            this.playerReviewer.OnButtonStartRecordingAll += new WirelessGFTViewer.StartRecordingAllHandler(this.wirelessViewer_OnButtonStartRecordingAll);
            this.playerReviewer.OnButtonPowerOnAll += new WirelessGFTViewer.PowerOnAllHandler(this.wirelessViewer_OnButtonPowerOnAll);
            this.playerReviewer.Load += new System.EventHandler(this.playerReviewer_Load);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer4.Panel1Collapsed = true;
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.consumerPlayerPanel);
            this.splitContainer4.Size = new System.Drawing.Size(96, 100);
            this.splitContainer4.SplitterDistance = 25;
            this.splitContainer4.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Controls.Add(this.ActivePct, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewPlayer, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.chartViewerPlayer, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.activityChartPlayer, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.pbHead, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.pbHeatMap, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel15, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.locationChartPlayer, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblRecovery, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(25, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ActivePct
            // 
            chartArea2.Name = "ChartArea1";
            this.ActivePct.ChartAreas.Add(chartArea2);
            this.ActivePct.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Alignment = System.Drawing.StringAlignment.Center;
            legend2.DockedToChartArea = "ChartArea1";
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend2.IsDockedInsideChartArea = false;
            legend2.Name = "Legend1";
            legend2.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Wide;
            this.ActivePct.Legends.Add(legend2);
            this.ActivePct.Location = new System.Drawing.Point(25, 97);
            this.ActivePct.Margin = new System.Windows.Forms.Padding(6, 0, 6, 6);
            this.ActivePct.Name = "ActivePct";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.ActivePct.Series.Add(series3);
            this.ActivePct.Size = new System.Drawing.Size(1, 1);
            this.ActivePct.TabIndex = 62;
            this.ActivePct.Text = "chart2";
            title2.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title2.Name = "Title1";
            title2.Text = "Exertion";
            this.ActivePct.Titles.Add(title2);
            // 
            // dataGridViewPlayer
            // 
            this.dataGridViewPlayer.AllowUserToAddRows = false;
            this.dataGridViewPlayer.AllowUserToDeleteRows = false;
            this.dataGridViewPlayer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel1.SetColumnSpan(this.dataGridViewPlayer, 5);
            this.dataGridViewPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPlayer.Location = new System.Drawing.Point(6, 104);
            this.dataGridViewPlayer.Margin = new System.Windows.Forms.Padding(6);
            this.dataGridViewPlayer.Name = "dataGridViewPlayer";
            this.dataGridViewPlayer.ReadOnly = true;
            this.dataGridViewPlayer.RowHeadersWidth = 30;
            this.dataGridViewPlayer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlayer.Size = new System.Drawing.Size(13, 1);
            this.dataGridViewPlayer.TabIndex = 55;
            // 
            // chartViewerPlayer
            // 
            this.chartViewerPlayer.BackSecondaryColor = System.Drawing.Color.White;
            this.chartViewerPlayer.BorderSkin.BorderColor = System.Drawing.Color.White;
            chartArea3.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea3.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea3.AxisX.MajorGrid.Enabled = false;
            chartArea3.AxisX.ScrollBar.BackColor = System.Drawing.Color.Silver;
            chartArea3.AxisX.ScrollBar.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            chartArea3.AxisX.Title = " Impacts";
            chartArea3.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea3.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea3.AxisX2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea3.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            chartArea3.AxisY.MajorTickMark.Enabled = false;
            chartArea3.AxisY.Title = "gForce";
            chartArea3.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea3.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea3.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea3.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea3.BorderColor = System.Drawing.Color.Gainsboro;
            chartArea3.CursorX.IsUserEnabled = true;
            chartArea3.CursorX.IsUserSelectionEnabled = true;
            chartArea3.Name = "ChartArea1";
            this.chartViewerPlayer.ChartAreas.Add(chartArea3);
            this.tableLayoutPanel1.SetColumnSpan(this.chartViewerPlayer, 3);
            this.chartViewerPlayer.Cursor = System.Windows.Forms.Cursors.Cross;
            this.chartViewerPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            legend3.Enabled = false;
            legend3.Name = "Legend1";
            this.chartViewerPlayer.Legends.Add(legend3);
            this.chartViewerPlayer.Location = new System.Drawing.Point(6, 102);
            this.chartViewerPlayer.Margin = new System.Windows.Forms.Padding(6);
            this.chartViewerPlayer.Name = "chartViewerPlayer";
            this.tableLayoutPanel1.SetRowSpan(this.chartViewerPlayer, 2);
            series4.ChartArea = "ChartArea1";
            series4.Color = System.Drawing.Color.Silver;
            series4.CustomProperties = "LabelStyle=BottomLeft";
            series4.IsVisibleInLegend = false;
            series4.LabelAngle = 45;
            series4.Legend = "Legend1";
            series4.Name = " ";
            series4.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series4.Points.Add(dataPoint9);
            series4.Points.Add(dataPoint10);
            series4.Points.Add(dataPoint11);
            series4.Points.Add(dataPoint12);
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series5.Color = System.Drawing.Color.Silver;
            series5.Legend = "Legend1";
            series5.Name = "Series2";
            series5.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series5.Points.Add(dataPoint13);
            series5.Points.Add(dataPoint14);
            series5.Points.Add(dataPoint15);
            series5.Points.Add(dataPoint16);
            this.chartViewerPlayer.Series.Add(series4);
            this.chartViewerPlayer.Series.Add(series5);
            this.chartViewerPlayer.Size = new System.Drawing.Size(4, 1);
            this.chartViewerPlayer.TabIndex = 6;
            this.chartViewerPlayer.Text = "chart1";
            title3.Name = "History Data";
            this.chartViewerPlayer.Titles.Add(title3);
            this.chartViewerPlayer.Click += new System.EventHandler(this.chartViewer_Click);
            this.chartViewerPlayer.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chartViewer_MouseMove);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.panel6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel7, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel8, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 102);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(4, 1);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Window;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.tableLayoutPanel12);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(7, 6);
            this.panel6.Margin = new System.Windows.Forms.Padding(6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1, 1);
            this.panel6.TabIndex = 70;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel12.Controls.Add(this.HdrResGForce, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.lblGForceAveMed, 0, 4);
            this.tableLayoutPanel12.Controls.Add(this.lblHighestG, 0, 2);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 5;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(0, 0);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // HdrResGForce
            // 
            this.HdrResGForce.AutoSize = true;
            this.HdrResGForce.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrResGForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrResGForce.Location = new System.Drawing.Point(6, 0);
            this.HdrResGForce.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrResGForce.Name = "HdrResGForce";
            this.HdrResGForce.Size = new System.Drawing.Size(1, 1);
            this.HdrResGForce.TabIndex = 0;
            this.HdrResGForce.Text = "gForce";
            this.HdrResGForce.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(1, 1);
            this.label8.TabIndex = 4;
            this.label8.Text = "Highest gForce";
            // 
            // lblGForceAveMed
            // 
            this.lblGForceAveMed.AutoSize = true;
            this.lblGForceAveMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGForceAveMed.Location = new System.Drawing.Point(6, 0);
            this.lblGForceAveMed.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblGForceAveMed.Name = "lblGForceAveMed";
            this.lblGForceAveMed.Size = new System.Drawing.Size(1, 1);
            this.lblGForceAveMed.TabIndex = 2;
            this.lblGForceAveMed.Text = "Average: 0g, Median: 0g";
            this.lblGForceAveMed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHighestG
            // 
            this.lblHighestG.AutoSize = true;
            this.lblHighestG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHighestG.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHighestG.Location = new System.Drawing.Point(6, 0);
            this.lblHighestG.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblHighestG.Name = "lblHighestG";
            this.tableLayoutPanel12.SetRowSpan(this.lblHighestG, 2);
            this.lblHighestG.Size = new System.Drawing.Size(1, 1);
            this.lblHighestG.TabIndex = 1;
            this.lblHighestG.Text = "0";
            this.lblHighestG.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblHighestG.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Window;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.tableLayoutPanel9);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(6, 6);
            this.panel5.Margin = new System.Windows.Forms.Padding(6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1, 1);
            this.panel5.TabIndex = 69;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.Controls.Add(this.HdrResImpacts, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.lblThresholdTotal, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.lblThresholdWithinAbove, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.lblThresholdImpacts, 0, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 5;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(0, 0);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // HdrResImpacts
            // 
            this.HdrResImpacts.AutoSize = true;
            this.HdrResImpacts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrResImpacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrResImpacts.Location = new System.Drawing.Point(6, 0);
            this.HdrResImpacts.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrResImpacts.Name = "HdrResImpacts";
            this.HdrResImpacts.Size = new System.Drawing.Size(1, 1);
            this.HdrResImpacts.TabIndex = 0;
            this.HdrResImpacts.Text = "Threshold Impacts";
            this.HdrResImpacts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1, 1);
            this.label2.TabIndex = 4;
            this.label2.Text = "Impacts Within or Above Threshold";
            // 
            // lblThresholdTotal
            // 
            this.lblThresholdTotal.AutoSize = true;
            this.lblThresholdTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThresholdTotal.Location = new System.Drawing.Point(6, 0);
            this.lblThresholdTotal.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblThresholdTotal.Name = "lblThresholdTotal";
            this.lblThresholdTotal.Size = new System.Drawing.Size(1, 1);
            this.lblThresholdTotal.TabIndex = 3;
            this.lblThresholdTotal.Text = "0 Impacts Total, 0% Above Threshold";
            this.lblThresholdTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblThresholdWithinAbove
            // 
            this.lblThresholdWithinAbove.AutoSize = true;
            this.lblThresholdWithinAbove.Location = new System.Drawing.Point(6, 0);
            this.lblThresholdWithinAbove.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblThresholdWithinAbove.Name = "lblThresholdWithinAbove";
            this.lblThresholdWithinAbove.Size = new System.Drawing.Size(1, 1);
            this.lblThresholdWithinAbove.TabIndex = 2;
            this.lblThresholdWithinAbove.Text = "0 Within, 0 Above";
            this.lblThresholdWithinAbove.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblThresholdImpacts
            // 
            this.lblThresholdImpacts.AutoSize = true;
            this.lblThresholdImpacts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThresholdImpacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThresholdImpacts.Location = new System.Drawing.Point(6, 0);
            this.lblThresholdImpacts.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblThresholdImpacts.Name = "lblThresholdImpacts";
            this.lblThresholdImpacts.Size = new System.Drawing.Size(1, 1);
            this.lblThresholdImpacts.TabIndex = 1;
            this.lblThresholdImpacts.Text = "0";
            this.lblThresholdImpacts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblThresholdImpacts.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1, 1);
            this.label4.TabIndex = 7;
            this.label4.Text = "Threshold Impacts";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Window;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.tableLayoutPanel13);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(8, 6);
            this.panel7.Margin = new System.Windows.Forms.Padding(6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1, 1);
            this.panel7.TabIndex = 71;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.Controls.Add(this.HdrResRotation, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.label15, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.lblRotationAveMed, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.lblHighestR, 0, 2);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 5;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(0, 0);
            this.tableLayoutPanel13.TabIndex = 2;
            // 
            // HdrResRotation
            // 
            this.HdrResRotation.AutoSize = true;
            this.HdrResRotation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrResRotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrResRotation.Location = new System.Drawing.Point(6, 0);
            this.HdrResRotation.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrResRotation.Name = "HdrResRotation";
            this.HdrResRotation.Size = new System.Drawing.Size(1, 1);
            this.HdrResRotation.TabIndex = 0;
            this.HdrResRotation.Text = "Rotation";
            this.HdrResRotation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(1, 1);
            this.label15.TabIndex = 4;
            this.label15.Text = "Highest Rotational Velocity(°/s)";
            // 
            // lblRotationAveMed
            // 
            this.lblRotationAveMed.AutoSize = true;
            this.lblRotationAveMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRotationAveMed.Location = new System.Drawing.Point(6, 0);
            this.lblRotationAveMed.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblRotationAveMed.Name = "lblRotationAveMed";
            this.lblRotationAveMed.Size = new System.Drawing.Size(1, 1);
            this.lblRotationAveMed.TabIndex = 2;
            this.lblRotationAveMed.Text = "Average: 0°/s, Median: 0°/s";
            this.lblRotationAveMed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHighestR
            // 
            this.lblHighestR.AutoSize = true;
            this.lblHighestR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHighestR.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHighestR.Location = new System.Drawing.Point(6, 0);
            this.lblHighestR.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblHighestR.Name = "lblHighestR";
            this.tableLayoutPanel13.SetRowSpan(this.lblHighestR, 2);
            this.lblHighestR.Size = new System.Drawing.Size(1, 1);
            this.lblHighestR.TabIndex = 1;
            this.lblHighestR.Text = "0";
            this.lblHighestR.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblHighestR.Visible = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Window;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.tableLayoutPanel14);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(9, 6);
            this.panel8.Margin = new System.Windows.Forms.Padding(6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1, 1);
            this.panel8.TabIndex = 72;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel14.Controls.Add(this.HdrResExposure, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.label24, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.lblExposure, 0, 2);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 5;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(0, 0);
            this.tableLayoutPanel14.TabIndex = 2;
            // 
            // HdrResExposure
            // 
            this.HdrResExposure.AutoSize = true;
            this.HdrResExposure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrResExposure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrResExposure.Location = new System.Drawing.Point(6, 0);
            this.HdrResExposure.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrResExposure.Name = "HdrResExposure";
            this.HdrResExposure.Size = new System.Drawing.Size(1, 1);
            this.HdrResExposure.TabIndex = 0;
            this.HdrResExposure.Text = "Exposure";
            this.HdrResExposure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 0);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(1, 1);
            this.label24.TabIndex = 4;
            this.label24.Text = "Impacts/Minute";
            // 
            // lblExposure
            // 
            this.lblExposure.AutoSize = true;
            this.lblExposure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExposure.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExposure.Location = new System.Drawing.Point(6, 0);
            this.lblExposure.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblExposure.Name = "lblExposure";
            this.tableLayoutPanel14.SetRowSpan(this.lblExposure, 3);
            this.lblExposure.Size = new System.Drawing.Size(1, 1);
            this.lblExposure.TabIndex = 1;
            this.lblExposure.Text = "0";
            this.lblExposure.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblExposure.Visible = false;
            // 
            // activityChartPlayer
            // 
            chartArea4.Name = "ChartArea1";
            this.activityChartPlayer.ChartAreas.Add(chartArea4);
            this.activityChartPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            legend4.Alignment = System.Drawing.StringAlignment.Center;
            legend4.DockedToChartArea = "ChartArea1";
            legend4.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend4.IsDockedInsideChartArea = false;
            legend4.Name = "Legend1";
            legend4.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Wide;
            this.activityChartPlayer.Legends.Add(legend4);
            this.activityChartPlayer.Location = new System.Drawing.Point(25, 102);
            this.activityChartPlayer.Margin = new System.Windows.Forms.Padding(6, 6, 6, 0);
            this.activityChartPlayer.Name = "activityChartPlayer";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.activityChartPlayer.Series.Add(series6);
            this.activityChartPlayer.Size = new System.Drawing.Size(1, 1);
            this.activityChartPlayer.TabIndex = 8;
            this.activityChartPlayer.Text = "chart2";
            title4.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title4.Name = "Title1";
            title4.Text = "Active Time";
            this.activityChartPlayer.Titles.Add(title4);
            // 
            // pbHead
            // 
            this.pbHead.BackColor = System.Drawing.Color.White;
            this.pbHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbHead.Image = global::WirelessGFTViewer.Properties.Resources.FRONT;
            this.pbHead.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbHead.InitialImage")));
            this.pbHead.Location = new System.Drawing.Point(22, 97);
            this.pbHead.Margin = new System.Windows.Forms.Padding(6, 0, 6, 6);
            this.pbHead.Name = "pbHead";
            this.pbHead.Size = new System.Drawing.Size(1, 1);
            this.pbHead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHead.TabIndex = 56;
            this.pbHead.TabStop = false;
            // 
            // pbHeatMap
            // 
            this.pbHeatMap.BackColor = System.Drawing.SystemColors.Window;
            this.pbHeatMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.pbHeatMap, 2);
            this.pbHeatMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbHeatMap.Location = new System.Drawing.Point(22, 108);
            this.pbHeatMap.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.pbHeatMap.Name = "pbHeatMap";
            this.pbHeatMap.Size = new System.Drawing.Size(1, 1);
            this.pbHeatMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbHeatMap.TabIndex = 9;
            this.pbHeatMap.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.lblPlayer, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblDeviceSettings, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(10, 96);
            this.tableLayoutPanel3.TabIndex = 58;
            // 
            // lblPlayer
            // 
            this.lblPlayer.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.lblPlayer, 3);
            this.lblPlayer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayer.Location = new System.Drawing.Point(6, 0);
            this.lblPlayer.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPlayer.Name = "lblPlayer";
            this.lblPlayer.Size = new System.Drawing.Size(1, 24);
            this.lblPlayer.TabIndex = 1;
            this.lblPlayer.Text = "lblPlayer";
            this.lblPlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDeviceSettings
            // 
            this.lblDeviceSettings.AutoSize = true;
            this.lblDeviceSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDeviceSettings.Location = new System.Drawing.Point(6, 67);
            this.lblDeviceSettings.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblDeviceSettings.Name = "lblDeviceSettings";
            this.lblDeviceSettings.Size = new System.Drawing.Size(1, 29);
            this.lblDeviceSettings.TabIndex = 2;
            this.lblDeviceSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel15, 2);
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.btnBack, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.lblStatus, 1, 1);
            this.tableLayoutPanel15.Controls.Add(this.pbBatStat, 1, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(6, 96);
            this.tableLayoutPanel15.TabIndex = 59;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(6, 6);
            this.btnBack.Margin = new System.Windows.Forms.Padding(6);
            this.btnBack.Name = "btnBack";
            this.tableLayoutPanel15.SetRowSpan(this.btnBack, 2);
            this.btnBack.Size = new System.Drawing.Size(188, 84);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Location = new System.Drawing.Point(206, 46);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(1, 50);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbBatStat
            // 
            this.pbBatStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbBatStat.Location = new System.Drawing.Point(200, 0);
            this.pbBatStat.Margin = new System.Windows.Forms.Padding(0);
            this.pbBatStat.Name = "pbBatStat";
            this.pbBatStat.Size = new System.Drawing.Size(1, 46);
            this.pbBatStat.TabIndex = 57;
            this.pbBatStat.TabStop = false;
            // 
            // locationChartPlayer
            // 
            chartArea5.Name = "ChartArea1";
            this.locationChartPlayer.ChartAreas.Add(chartArea5);
            this.locationChartPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            legend5.Alignment = System.Drawing.StringAlignment.Center;
            legend5.DockedToChartArea = "ChartArea1";
            legend5.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend5.IsDockedInsideChartArea = false;
            legend5.Name = "Legend1";
            legend5.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Wide;
            this.locationChartPlayer.Legends.Add(legend5);
            this.locationChartPlayer.Location = new System.Drawing.Point(22, 102);
            this.locationChartPlayer.Margin = new System.Windows.Forms.Padding(6, 6, 6, 0);
            this.locationChartPlayer.Name = "locationChartPlayer";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series7.Legend = "Legend1";
            series7.Name = "Series1";
            this.locationChartPlayer.Series.Add(series7);
            this.locationChartPlayer.Size = new System.Drawing.Size(1, 1);
            this.locationChartPlayer.TabIndex = 60;
            this.locationChartPlayer.Text = "chart2";
            title5.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title5.DockedToChartArea = "ChartArea1";
            title5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title5.IsDockedInsideChartArea = false;
            title5.Name = "impLocation";
            title5.Text = "Impact Location";
            this.locationChartPlayer.Titles.Add(title5);
            // 
            // lblRecovery
            // 
            this.lblRecovery.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblRecovery, 2);
            this.lblRecovery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRecovery.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecovery.Location = new System.Drawing.Point(22, 0);
            this.lblRecovery.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblRecovery.Name = "lblRecovery";
            this.lblRecovery.Size = new System.Drawing.Size(1, 96);
            this.lblRecovery.TabIndex = 61;
            this.lblRecovery.Text = "label3";
            this.lblRecovery.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // consumerPlayerPanel
            // 
            this.consumerPlayerPanel.ColumnCount = 5;
            this.consumerPlayerPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.consumerPlayerPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.42857F));
            this.consumerPlayerPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.14286F));
            this.consumerPlayerPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.42857F));
            this.consumerPlayerPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 316F));
            this.consumerPlayerPanel.Controls.Add(this.perfChartPL, 0, 4);
            this.consumerPlayerPanel.Controls.Add(this.perfChartEP, 1, 3);
            this.consumerPlayerPanel.Controls.Add(this.perfChartViewerPlayer, 1, 2);
            this.consumerPlayerPanel.Controls.Add(this.tableLayoutPanel23, 2, 0);
            this.consumerPlayerPanel.Controls.Add(this.tableLayoutPanel24, 0, 0);
            this.consumerPlayerPanel.Controls.Add(this.lblPerfRecovery, 3, 0);
            this.consumerPlayerPanel.Controls.Add(this.tableLayoutPanel29, 0, 2);
            this.consumerPlayerPanel.Controls.Add(this.tableLayoutPanel30, 4, 2);
            this.consumerPlayerPanel.Controls.Add(this.panel13, 1, 5);
            this.consumerPlayerPanel.Controls.Add(this.label19, 0, 1);
            this.consumerPlayerPanel.Controls.Add(this.label20, 2, 1);
            this.consumerPlayerPanel.Controls.Add(this.label21, 4, 1);
            this.consumerPlayerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consumerPlayerPanel.Location = new System.Drawing.Point(0, 0);
            this.consumerPlayerPanel.Margin = new System.Windows.Forms.Padding(6);
            this.consumerPlayerPanel.Name = "consumerPlayerPanel";
            this.consumerPlayerPanel.RowCount = 6;
            this.consumerPlayerPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.consumerPlayerPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.consumerPlayerPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.consumerPlayerPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.consumerPlayerPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.consumerPlayerPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.consumerPlayerPanel.Size = new System.Drawing.Size(96, 100);
            this.consumerPlayerPanel.TabIndex = 1;
            // 
            // perfChartPL
            // 
            chartArea6.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea6.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(244)))));
            chartArea6.AxisX.MajorGrid.Enabled = false;
            chartArea6.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea6.AxisX.Title = "Time";
            chartArea6.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea6.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea6.AxisY.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea6.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea6.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea6.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea6.AxisY.MajorTickMark.Enabled = false;
            chartArea6.AxisY.Title = "Player Load";
            chartArea6.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea6.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea6.AxisY2.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea6.AxisY2.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea6.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea6.AxisY2.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea6.AxisY2.MajorGrid.Enabled = false;
            chartArea6.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea6.AxisY2.MajorTickMark.Enabled = false;
            chartArea6.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea6.AxisY2.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea6.BackColor = System.Drawing.Color.White;
            chartArea6.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea6.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea6.BorderColor = System.Drawing.Color.Gainsboro;
            chartArea6.Name = "Player Load";
            this.perfChartPL.ChartAreas.Add(chartArea6);
            this.consumerPlayerPanel.SetColumnSpan(this.perfChartPL, 3);
            this.perfChartPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.perfChartPL.Location = new System.Drawing.Point(306, 2);
            this.perfChartPL.Margin = new System.Windows.Forms.Padding(6);
            this.perfChartPL.Name = "perfChartPL";
            series8.BorderWidth = 3;
            series8.ChartArea = "Player Load";
            series8.Name = "Series1";
            this.perfChartPL.Series.Add(series8);
            this.perfChartPL.Size = new System.Drawing.Size(1, 1);
            this.perfChartPL.TabIndex = 81;
            this.perfChartPL.Text = "PERFORMANCE";
            title6.Alignment = System.Drawing.ContentAlignment.TopCenter;
            title6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title6.Name = "Title1";
            title6.Text = "PLAYER LOAD";
            this.perfChartPL.Titles.Add(title6);
            // 
            // perfChartEP
            // 
            chartArea7.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea7.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(244)))));
            chartArea7.AxisX.MajorGrid.Enabled = false;
            chartArea7.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea7.AxisX.Title = "Time";
            chartArea7.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea7.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea7.AxisY.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea7.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea7.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea7.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea7.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea7.AxisY.MajorTickMark.Enabled = false;
            chartArea7.AxisY.Maximum = 2D;
            chartArea7.AxisY.Title = "Explosive Power";
            chartArea7.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea7.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea7.AxisY2.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea7.AxisY2.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea7.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea7.AxisY2.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea7.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea7.AxisY2.MajorTickMark.Enabled = false;
            chartArea7.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea7.AxisY2.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea7.BackColor = System.Drawing.Color.White;
            chartArea7.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea7.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea7.BorderColor = System.Drawing.Color.Gainsboro;
            chartArea7.Name = "Explosive Events";
            this.perfChartEP.ChartAreas.Add(chartArea7);
            this.consumerPlayerPanel.SetColumnSpan(this.perfChartEP, 3);
            this.perfChartEP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.perfChartEP.Location = new System.Drawing.Point(306, 41);
            this.perfChartEP.Margin = new System.Windows.Forms.Padding(6);
            this.perfChartEP.Name = "perfChartEP";
            series9.BorderWidth = 3;
            series9.ChartArea = "Explosive Events";
            series9.Name = "Series1";
            this.perfChartEP.Series.Add(series9);
            this.perfChartEP.Size = new System.Drawing.Size(1, 1);
            this.perfChartEP.TabIndex = 74;
            this.perfChartEP.Text = "EXPLOSIVE POWER";
            title7.Alignment = System.Drawing.ContentAlignment.TopCenter;
            title7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title7.Name = "Title1";
            title7.Text = "EXPLOSIVE POWER";
            this.perfChartEP.Titles.Add(title7);
            // 
            // perfChartViewerPlayer
            // 
            this.perfChartViewerPlayer.BackSecondaryColor = System.Drawing.Color.White;
            this.perfChartViewerPlayer.BorderSkin.BorderColor = System.Drawing.Color.White;
            chartArea8.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea8.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea8.AxisX.IsLabelAutoFit = false;
            chartArea8.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea8.AxisX.MajorGrid.Enabled = false;
            chartArea8.AxisX.ScrollBar.BackColor = System.Drawing.Color.Silver;
            chartArea8.AxisX.ScrollBar.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            chartArea8.AxisX.Title = " Impacts";
            chartArea8.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea8.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea8.AxisX2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea8.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea8.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            chartArea8.AxisY.MajorTickMark.Enabled = false;
            chartArea8.AxisY.Title = "gForce";
            chartArea8.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea8.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea8.AxisY2.LineColor = System.Drawing.Color.White;
            chartArea8.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea8.AxisY2.MajorTickMark.Enabled = false;
            chartArea8.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            chartArea8.AxisY2.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea8.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea8.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea8.BorderColor = System.Drawing.Color.Gainsboro;
            chartArea8.CursorX.IsUserEnabled = true;
            chartArea8.CursorX.IsUserSelectionEnabled = true;
            chartArea8.Name = "ChartArea1";
            this.perfChartViewerPlayer.ChartAreas.Add(chartArea8);
            this.consumerPlayerPanel.SetColumnSpan(this.perfChartViewerPlayer, 3);
            this.perfChartViewerPlayer.Cursor = System.Windows.Forms.Cursors.Cross;
            this.perfChartViewerPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            legend6.Enabled = false;
            legend6.Name = "Legend1";
            this.perfChartViewerPlayer.Legends.Add(legend6);
            this.perfChartViewerPlayer.Location = new System.Drawing.Point(306, 93);
            this.perfChartViewerPlayer.Margin = new System.Windows.Forms.Padding(6);
            this.perfChartViewerPlayer.Name = "perfChartViewerPlayer";
            series10.ChartArea = "ChartArea1";
            series10.Color = System.Drawing.Color.Silver;
            series10.CustomProperties = "LabelStyle=BottomLeft";
            series10.IsVisibleInLegend = false;
            series10.LabelAngle = 45;
            series10.Legend = "Legend1";
            series10.Name = " ";
            series10.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series10.Points.Add(dataPoint17);
            series10.Points.Add(dataPoint18);
            series10.Points.Add(dataPoint19);
            series10.Points.Add(dataPoint20);
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series11.Color = System.Drawing.Color.Silver;
            series11.Legend = "Legend1";
            series11.Name = "Series2";
            series11.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series11.Points.Add(dataPoint21);
            series11.Points.Add(dataPoint22);
            series11.Points.Add(dataPoint23);
            series11.Points.Add(dataPoint24);
            this.perfChartViewerPlayer.Series.Add(series10);
            this.perfChartViewerPlayer.Series.Add(series11);
            this.perfChartViewerPlayer.Size = new System.Drawing.Size(1, 1);
            this.perfChartViewerPlayer.TabIndex = 6;
            this.perfChartViewerPlayer.Text = "Impacts";
            title8.Alignment = System.Drawing.ContentAlignment.TopCenter;
            title8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title8.Name = "History Data";
            title8.Text = "IMPACTS";
            this.perfChartViewerPlayer.Titles.Add(title8);
            this.perfChartViewerPlayer.Click += new System.EventHandler(this.chartViewer_Click);
            this.perfChartViewerPlayer.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chartViewer_MouseMove);
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 1;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Controls.Add(this.lblPerfPlayer, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.lblPerfStatus, 0, 1);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(189, 0);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(1, 58);
            this.tableLayoutPanel23.TabIndex = 58;
            // 
            // lblPerfPlayer
            // 
            this.lblPerfPlayer.AutoSize = true;
            this.tableLayoutPanel23.SetColumnSpan(this.lblPerfPlayer, 3);
            this.lblPerfPlayer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPerfPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfPlayer.Location = new System.Drawing.Point(6, 0);
            this.lblPerfPlayer.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfPlayer.Name = "lblPerfPlayer";
            this.lblPerfPlayer.Size = new System.Drawing.Size(1, 20);
            this.lblPerfPlayer.TabIndex = 1;
            this.lblPerfPlayer.Text = "label33";
            this.lblPerfPlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPerfStatus
            // 
            this.lblPerfStatus.AutoSize = true;
            this.lblPerfStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfStatus.Location = new System.Drawing.Point(6, 34);
            this.lblPerfStatus.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfStatus.Name = "lblPerfStatus";
            this.lblPerfStatus.Size = new System.Drawing.Size(1, 24);
            this.lblPerfStatus.TabIndex = 2;
            this.lblPerfStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.consumerPlayerPanel.SetColumnSpan(this.tableLayoutPanel24, 2);
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Controls.Add(this.btnPerfBack, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.label35, 1, 1);
            this.tableLayoutPanel24.Controls.Add(this.pictureBox3, 1, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel24.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 2;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(189, 58);
            this.tableLayoutPanel24.TabIndex = 59;
            // 
            // btnPerfBack
            // 
            this.btnPerfBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPerfBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPerfBack.Location = new System.Drawing.Point(6, 6);
            this.btnPerfBack.Margin = new System.Windows.Forms.Padding(6);
            this.btnPerfBack.Name = "btnPerfBack";
            this.tableLayoutPanel24.SetRowSpan(this.btnPerfBack, 2);
            this.btnPerfBack.Size = new System.Drawing.Size(188, 46);
            this.btnPerfBack.TabIndex = 5;
            this.btnPerfBack.Text = "Back";
            this.btnPerfBack.UseVisualStyleBackColor = true;
            this.btnPerfBack.Click += new System.EventHandler(this.button1_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Location = new System.Drawing.Point(206, 46);
            this.label35.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(1, 12);
            this.label35.TabIndex = 2;
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(200, 0);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1, 46);
            this.pictureBox3.TabIndex = 57;
            this.pictureBox3.TabStop = false;
            // 
            // lblPerfRecovery
            // 
            this.lblPerfRecovery.AutoSize = true;
            this.consumerPlayerPanel.SetColumnSpan(this.lblPerfRecovery, 2);
            this.lblPerfRecovery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfRecovery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfRecovery.Location = new System.Drawing.Point(-102, 0);
            this.lblPerfRecovery.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfRecovery.Name = "lblPerfRecovery";
            this.lblPerfRecovery.Size = new System.Drawing.Size(193, 58);
            this.lblPerfRecovery.TabIndex = 61;
            this.lblPerfRecovery.Text = "label3";
            this.lblPerfRecovery.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 1;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel29.Controls.Add(this.panel9, 0, 1);
            this.tableLayoutPanel29.Controls.Add(this.panel111, 0, 2);
            this.tableLayoutPanel29.Controls.Add(this.perfLocationChartPlayer, 0, 4);
            this.tableLayoutPanel29.Controls.Add(this.panel101, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.pbPerfHead, 0, 3);
            this.tableLayoutPanel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel29.Location = new System.Drawing.Point(0, 87);
            this.tableLayoutPanel29.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 5;
            this.consumerPlayerPanel.SetRowSpan(this.tableLayoutPanel29, 4);
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel29.Size = new System.Drawing.Size(300, 14);
            this.tableLayoutPanel29.TabIndex = 75;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Window;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.tableLayoutPanel19);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(6, 208);
            this.panel9.Margin = new System.Windows.Forms.Padding(6);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(288, 190);
            this.panel9.TabIndex = 70;
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel19.Controls.Add(this.lblPerfGForceAve, 0, 3);
            this.tableLayoutPanel19.Controls.Add(this.HdrPerfGForce, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.lblPerfGForceMed, 0, 4);
            this.tableLayoutPanel19.Controls.Add(this.lblPerfHighestG, 0, 2);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 5;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(286, 188);
            this.tableLayoutPanel19.TabIndex = 1;
            // 
            // lblPerfGForceAve
            // 
            this.lblPerfGForceAve.AutoSize = true;
            this.lblPerfGForceAve.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfGForceAve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfGForceAve.Location = new System.Drawing.Point(6, 130);
            this.lblPerfGForceAve.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfGForceAve.Name = "lblPerfGForceAve";
            this.lblPerfGForceAve.Size = new System.Drawing.Size(274, 28);
            this.lblPerfGForceAve.TabIndex = 5;
            this.lblPerfGForceAve.Text = "Average: 0g";
            this.lblPerfGForceAve.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HdrPerfGForce
            // 
            this.HdrPerfGForce.AutoSize = true;
            this.HdrPerfGForce.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrPerfGForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrPerfGForce.Location = new System.Drawing.Point(6, 0);
            this.HdrPerfGForce.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrPerfGForce.Name = "HdrPerfGForce";
            this.HdrPerfGForce.Size = new System.Drawing.Size(274, 37);
            this.HdrPerfGForce.TabIndex = 0;
            this.HdrPerfGForce.Text = "gForce";
            this.HdrPerfGForce.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 37);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Highest";
            // 
            // lblPerfGForceMed
            // 
            this.lblPerfGForceMed.AutoSize = true;
            this.lblPerfGForceMed.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfGForceMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfGForceMed.Location = new System.Drawing.Point(6, 158);
            this.lblPerfGForceMed.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfGForceMed.Name = "lblPerfGForceMed";
            this.lblPerfGForceMed.Size = new System.Drawing.Size(274, 30);
            this.lblPerfGForceMed.TabIndex = 2;
            this.lblPerfGForceMed.Text = "Median: 0g";
            this.lblPerfGForceMed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfHighestG
            // 
            this.lblPerfHighestG.AutoSize = true;
            this.lblPerfHighestG.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfHighestG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfHighestG.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfHighestG.Location = new System.Drawing.Point(6, 65);
            this.lblPerfHighestG.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfHighestG.Name = "lblPerfHighestG";
            this.lblPerfHighestG.Size = new System.Drawing.Size(274, 65);
            this.lblPerfHighestG.TabIndex = 1;
            this.lblPerfHighestG.Text = "0";
            this.lblPerfHighestG.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPerfHighestG.Visible = false;
            // 
            // panel111
            // 
            this.panel111.BackColor = System.Drawing.SystemColors.Window;
            this.panel111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel111.Controls.Add(this.tableLayoutPanel21);
            this.panel111.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel111.Location = new System.Drawing.Point(6, 410);
            this.panel111.Margin = new System.Windows.Forms.Padding(6);
            this.panel111.Name = "panel111";
            this.panel111.Size = new System.Drawing.Size(288, 190);
            this.panel111.TabIndex = 71;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel21.Controls.Add(this.lblPerfRotationAve, 0, 3);
            this.tableLayoutPanel21.Controls.Add(this.HdrPerfRotation, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.label27, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.lblPerfRotationMed, 0, 4);
            this.tableLayoutPanel21.Controls.Add(this.lblPerfHighestR, 0, 2);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel21.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 5;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(286, 188);
            this.tableLayoutPanel21.TabIndex = 2;
            // 
            // lblPerfRotationAve
            // 
            this.lblPerfRotationAve.AutoSize = true;
            this.lblPerfRotationAve.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfRotationAve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfRotationAve.Location = new System.Drawing.Point(6, 130);
            this.lblPerfRotationAve.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfRotationAve.Name = "lblPerfRotationAve";
            this.lblPerfRotationAve.Size = new System.Drawing.Size(274, 28);
            this.lblPerfRotationAve.TabIndex = 5;
            this.lblPerfRotationAve.Text = "Average: 0°/s";
            this.lblPerfRotationAve.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HdrPerfRotation
            // 
            this.HdrPerfRotation.AutoSize = true;
            this.HdrPerfRotation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrPerfRotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrPerfRotation.Location = new System.Drawing.Point(6, 0);
            this.HdrPerfRotation.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrPerfRotation.Name = "HdrPerfRotation";
            this.HdrPerfRotation.Size = new System.Drawing.Size(274, 37);
            this.HdrPerfRotation.TabIndex = 0;
            this.HdrPerfRotation.Text = "Rotation";
            this.HdrPerfRotation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 37);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 13);
            this.label27.TabIndex = 4;
            this.label27.Text = "Highest";
            // 
            // lblPerfRotationMed
            // 
            this.lblPerfRotationMed.AutoSize = true;
            this.lblPerfRotationMed.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfRotationMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfRotationMed.Location = new System.Drawing.Point(6, 158);
            this.lblPerfRotationMed.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfRotationMed.Name = "lblPerfRotationMed";
            this.lblPerfRotationMed.Size = new System.Drawing.Size(274, 30);
            this.lblPerfRotationMed.TabIndex = 2;
            this.lblPerfRotationMed.Text = "Median: 0°/s";
            this.lblPerfRotationMed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfHighestR
            // 
            this.lblPerfHighestR.AutoSize = true;
            this.lblPerfHighestR.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfHighestR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfHighestR.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfHighestR.Location = new System.Drawing.Point(6, 65);
            this.lblPerfHighestR.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfHighestR.Name = "lblPerfHighestR";
            this.lblPerfHighestR.Size = new System.Drawing.Size(274, 65);
            this.lblPerfHighestR.TabIndex = 1;
            this.lblPerfHighestR.Text = "0";
            this.lblPerfHighestR.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPerfHighestR.Visible = false;
            // 
            // perfLocationChartPlayer
            // 
            this.perfLocationChartPlayer.BorderlineColor = System.Drawing.Color.Black;
            chartArea9.Name = "ChartArea1";
            this.perfLocationChartPlayer.ChartAreas.Add(chartArea9);
            this.perfLocationChartPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            legend7.Alignment = System.Drawing.StringAlignment.Center;
            legend7.DockedToChartArea = "ChartArea1";
            legend7.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend7.IsDockedInsideChartArea = false;
            legend7.IsTextAutoFit = false;
            legend7.Name = "Legend1";
            legend7.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Wide;
            this.perfLocationChartPlayer.Legends.Add(legend7);
            this.perfLocationChartPlayer.Location = new System.Drawing.Point(6, 814);
            this.perfLocationChartPlayer.Margin = new System.Windows.Forms.Padding(6, 6, 6, 0);
            this.perfLocationChartPlayer.Name = "perfLocationChartPlayer";
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series12.Legend = "Legend1";
            series12.Name = "Series1";
            this.perfLocationChartPlayer.Series.Add(series12);
            this.perfLocationChartPlayer.Size = new System.Drawing.Size(288, 312);
            this.perfLocationChartPlayer.TabIndex = 60;
            this.perfLocationChartPlayer.Text = "chart2";
            title9.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title9.DockedToChartArea = "ChartArea1";
            title9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title9.IsDockedInsideChartArea = false;
            title9.Name = "impLocation";
            title9.Text = "Impact Location";
            title9.Visible = false;
            this.perfLocationChartPlayer.Titles.Add(title9);
            // 
            // panel101
            // 
            this.panel101.BackColor = System.Drawing.SystemColors.Window;
            this.panel101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel101.Controls.Add(this.tableLayoutPanel20);
            this.panel101.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel101.Location = new System.Drawing.Point(6, 6);
            this.panel101.Margin = new System.Windows.Forms.Padding(6);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(288, 190);
            this.panel101.TabIndex = 69;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel20.Controls.Add(this.HdrPerfThreshold, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.lblPerfThresholdAbove, 0, 4);
            this.tableLayoutPanel20.Controls.Add(this.lblPerfThresholdWithinAbove, 0, 3);
            this.tableLayoutPanel20.Controls.Add(this.lblPerfThresholdImpacts, 0, 2);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 5;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(286, 188);
            this.tableLayoutPanel20.TabIndex = 1;
            // 
            // HdrPerfThreshold
            // 
            this.HdrPerfThreshold.AutoSize = true;
            this.HdrPerfThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrPerfThreshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrPerfThreshold.Location = new System.Drawing.Point(6, 0);
            this.HdrPerfThreshold.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrPerfThreshold.Name = "HdrPerfThreshold";
            this.HdrPerfThreshold.Size = new System.Drawing.Size(274, 37);
            this.HdrPerfThreshold.TabIndex = 0;
            this.HdrPerfThreshold.Text = "Threshold Impacts";
            this.HdrPerfThreshold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(6, 37);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Within or Above Threshold";
            // 
            // lblPerfThresholdAbove
            // 
            this.lblPerfThresholdAbove.AutoSize = true;
            this.lblPerfThresholdAbove.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfThresholdAbove.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfThresholdAbove.Location = new System.Drawing.Point(6, 158);
            this.lblPerfThresholdAbove.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfThresholdAbove.Name = "lblPerfThresholdAbove";
            this.lblPerfThresholdAbove.Size = new System.Drawing.Size(274, 30);
            this.lblPerfThresholdAbove.TabIndex = 3;
            this.lblPerfThresholdAbove.Text = "Above: 0 (0%)";
            this.lblPerfThresholdAbove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfThresholdWithinAbove
            // 
            this.lblPerfThresholdWithinAbove.AutoSize = true;
            this.lblPerfThresholdWithinAbove.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfThresholdWithinAbove.Location = new System.Drawing.Point(6, 130);
            this.lblPerfThresholdWithinAbove.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfThresholdWithinAbove.Name = "lblPerfThresholdWithinAbove";
            this.lblPerfThresholdWithinAbove.Size = new System.Drawing.Size(49, 13);
            this.lblPerfThresholdWithinAbove.TabIndex = 2;
            this.lblPerfThresholdWithinAbove.Text = "Within: 0";
            this.lblPerfThresholdWithinAbove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfThresholdImpacts
            // 
            this.lblPerfThresholdImpacts.AutoSize = true;
            this.lblPerfThresholdImpacts.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfThresholdImpacts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfThresholdImpacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfThresholdImpacts.Location = new System.Drawing.Point(6, 65);
            this.lblPerfThresholdImpacts.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfThresholdImpacts.Name = "lblPerfThresholdImpacts";
            this.lblPerfThresholdImpacts.Size = new System.Drawing.Size(274, 65);
            this.lblPerfThresholdImpacts.TabIndex = 1;
            this.lblPerfThresholdImpacts.Text = "0";
            this.lblPerfThresholdImpacts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPerfThresholdImpacts.Visible = false;
            // 
            // pbPerfHead
            // 
            this.pbPerfHead.BackColor = System.Drawing.Color.White;
            this.pbPerfHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbPerfHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbPerfHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbPerfHead.Image = global::WirelessGFTViewer.Properties.Resources.FRONT;
            this.pbPerfHead.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbPerfHead.InitialImage")));
            this.pbPerfHead.Location = new System.Drawing.Point(6, 606);
            this.pbPerfHead.Margin = new System.Windows.Forms.Padding(6, 0, 6, 6);
            this.pbPerfHead.Name = "pbPerfHead";
            this.pbPerfHead.Size = new System.Drawing.Size(288, 196);
            this.pbPerfHead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPerfHead.TabIndex = 56;
            this.pbPerfHead.TabStop = false;
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 1;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Controls.Add(this.panel14, 0, 1);
            this.tableLayoutPanel30.Controls.Add(this.panel15, 0, 2);
            this.tableLayoutPanel30.Controls.Add(this.panel12, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.perfChartRPE, 0, 3);
            this.tableLayoutPanel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel30.Location = new System.Drawing.Point(-219, 87);
            this.tableLayoutPanel30.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 4;
            this.consumerPlayerPanel.SetRowSpan(this.tableLayoutPanel30, 4);
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel30.Size = new System.Drawing.Size(316, 14);
            this.tableLayoutPanel30.TabIndex = 76;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.Window;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.tableLayoutPanel17);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(6, 208);
            this.panel14.Margin = new System.Windows.Forms.Padding(6);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(304, 190);
            this.panel14.TabIndex = 73;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 1;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel17.Controls.Add(this.lblPerfEPRating, 0, 3);
            this.tableLayoutPanel17.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.lblPerfEPHighest, 0, 4);
            this.tableLayoutPanel17.Controls.Add(this.lblPerfEP, 0, 2);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel17.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 5;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(302, 188);
            this.tableLayoutPanel17.TabIndex = 2;
            // 
            // lblPerfEPRating
            // 
            this.lblPerfEPRating.AutoSize = true;
            this.lblPerfEPRating.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfEPRating.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfEPRating.Location = new System.Drawing.Point(6, 130);
            this.lblPerfEPRating.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfEPRating.Name = "lblPerfEPRating";
            this.lblPerfEPRating.Size = new System.Drawing.Size(290, 28);
            this.lblPerfEPRating.TabIndex = 8;
            this.lblPerfEPRating.Text = "Total: 0 sec";
            this.lblPerfEPRating.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 37);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Events";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(290, 37);
            this.label3.TabIndex = 0;
            this.label3.Text = "Explosive Power";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfEPHighest
            // 
            this.lblPerfEPHighest.AutoSize = true;
            this.lblPerfEPHighest.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfEPHighest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfEPHighest.Location = new System.Drawing.Point(6, 158);
            this.lblPerfEPHighest.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfEPHighest.Name = "lblPerfEPHighest";
            this.lblPerfEPHighest.Size = new System.Drawing.Size(290, 30);
            this.lblPerfEPHighest.TabIndex = 2;
            this.lblPerfEPHighest.Text = "Highest: 0 sec";
            this.lblPerfEPHighest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfEP
            // 
            this.lblPerfEP.AutoSize = true;
            this.lblPerfEP.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfEP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfEP.Location = new System.Drawing.Point(6, 65);
            this.lblPerfEP.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfEP.Name = "lblPerfEP";
            this.lblPerfEP.Size = new System.Drawing.Size(290, 65);
            this.lblPerfEP.TabIndex = 1;
            this.lblPerfEP.Text = "0";
            this.lblPerfEP.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPerfEP.Visible = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.Window;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.tableLayoutPanel26);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(6, 410);
            this.panel15.Margin = new System.Windows.Forms.Padding(6);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(304, 190);
            this.panel15.TabIndex = 74;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 1;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel26.Controls.Add(this.label13, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.lblPerfRPE, 0, 2);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel26.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 5;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(302, 188);
            this.tableLayoutPanel26.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 37);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Session Exertion";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(290, 37);
            this.label16.TabIndex = 0;
            this.label16.Text = "Exertion";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfRPE
            // 
            this.lblPerfRPE.AutoSize = true;
            this.lblPerfRPE.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfRPE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfRPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfRPE.Location = new System.Drawing.Point(6, 65);
            this.lblPerfRPE.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfRPE.Name = "lblPerfRPE";
            this.lblPerfRPE.Size = new System.Drawing.Size(290, 65);
            this.lblPerfRPE.TabIndex = 1;
            this.lblPerfRPE.Text = "0";
            this.lblPerfRPE.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPerfRPE.Visible = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Window;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.tableLayoutPanel22);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(6, 6);
            this.panel12.Margin = new System.Windows.Forms.Padding(6);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(304, 190);
            this.panel12.TabIndex = 72;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.lblPerfPLHighest, 0, 4);
            this.tableLayoutPanel22.Controls.Add(this.lblPerfPLAve, 0, 3);
            this.tableLayoutPanel22.Controls.Add(this.lblPerfPlayerLoadText, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.HdrPerfPlayerLoad, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.lblPerfPlayerLoad, 0, 2);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel22.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 5;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(302, 188);
            this.tableLayoutPanel22.TabIndex = 2;
            // 
            // lblPerfPLHighest
            // 
            this.lblPerfPLHighest.AutoSize = true;
            this.lblPerfPLHighest.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfPLHighest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfPLHighest.Location = new System.Drawing.Point(6, 158);
            this.lblPerfPLHighest.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfPLHighest.Name = "lblPerfPLHighest";
            this.lblPerfPLHighest.Size = new System.Drawing.Size(290, 30);
            this.lblPerfPLHighest.TabIndex = 8;
            this.lblPerfPLHighest.Text = "Highest: 0 PL/min";
            this.lblPerfPLHighest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfPLAve
            // 
            this.lblPerfPLAve.AutoSize = true;
            this.lblPerfPLAve.BackColor = System.Drawing.Color.Transparent;
            this.lblPerfPLAve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfPLAve.Location = new System.Drawing.Point(6, 130);
            this.lblPerfPLAve.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfPLAve.Name = "lblPerfPLAve";
            this.lblPerfPLAve.Size = new System.Drawing.Size(290, 28);
            this.lblPerfPLAve.TabIndex = 7;
            this.lblPerfPLAve.Text = "Average: 0 PL/min";
            this.lblPerfPLAve.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfPlayerLoadText
            // 
            this.lblPerfPlayerLoadText.AutoSize = true;
            this.lblPerfPlayerLoadText.Location = new System.Drawing.Point(6, 37);
            this.lblPerfPlayerLoadText.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfPlayerLoadText.Name = "lblPerfPlayerLoadText";
            this.lblPerfPlayerLoadText.Size = new System.Drawing.Size(69, 13);
            this.lblPerfPlayerLoadText.TabIndex = 6;
            this.lblPerfPlayerLoadText.Text = "Accumulated";
            // 
            // HdrPerfPlayerLoad
            // 
            this.HdrPerfPlayerLoad.AutoSize = true;
            this.HdrPerfPlayerLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HdrPerfPlayerLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrPerfPlayerLoad.Location = new System.Drawing.Point(6, 0);
            this.HdrPerfPlayerLoad.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HdrPerfPlayerLoad.Name = "HdrPerfPlayerLoad";
            this.HdrPerfPlayerLoad.Size = new System.Drawing.Size(290, 37);
            this.HdrPerfPlayerLoad.TabIndex = 0;
            this.HdrPerfPlayerLoad.Text = "Player Load";
            this.HdrPerfPlayerLoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPerfPlayerLoad
            // 
            this.lblPerfPlayerLoad.AutoSize = true;
            this.lblPerfPlayerLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfPlayerLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfPlayerLoad.Location = new System.Drawing.Point(6, 65);
            this.lblPerfPlayerLoad.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPerfPlayerLoad.Name = "lblPerfPlayerLoad";
            this.lblPerfPlayerLoad.Size = new System.Drawing.Size(290, 65);
            this.lblPerfPlayerLoad.TabIndex = 1;
            this.lblPerfPlayerLoad.Text = "0";
            this.lblPerfPlayerLoad.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPerfPlayerLoad.Visible = false;
            // 
            // perfChartRPE
            // 
            this.perfChartRPE.BorderlineColor = System.Drawing.Color.Black;
            chartArea10.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea10.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(244)))));
            chartArea10.AxisX.MajorGrid.Enabled = false;
            chartArea10.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea10.AxisX.Title = "Time";
            chartArea10.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea10.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea10.AxisY.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea10.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea10.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea10.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisY.MajorTickMark.Enabled = false;
            chartArea10.AxisY.Title = "Exertion";
            chartArea10.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            chartArea10.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea10.BackColor = System.Drawing.Color.White;
            chartArea10.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea10.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea10.BorderColor = System.Drawing.Color.Gainsboro;
            chartArea10.Name = "Exertion Timeline";
            this.perfChartRPE.ChartAreas.Add(chartArea10);
            this.perfChartRPE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.perfChartRPE.Location = new System.Drawing.Point(6, 612);
            this.perfChartRPE.Margin = new System.Windows.Forms.Padding(6);
            this.perfChartRPE.Name = "perfChartRPE";
            series13.BorderWidth = 3;
            series13.ChartArea = "Exertion Timeline";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series13.Name = "Series1";
            this.perfChartRPE.Series.Add(series13);
            this.perfChartRPE.Size = new System.Drawing.Size(304, 508);
            this.perfChartRPE.TabIndex = 63;
            this.perfChartRPE.Text = "chart5";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.Window;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.consumerPlayerPanel.SetColumnSpan(this.panel13, 3);
            this.panel13.Controls.Add(this.pbRPEHeatMap);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(312, -31);
            this.panel13.Margin = new System.Windows.Forms.Padding(12);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1, 120);
            this.panel13.TabIndex = 73;
            // 
            // pbRPEHeatMap
            // 
            this.pbRPEHeatMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbRPEHeatMap.Location = new System.Drawing.Point(0, 0);
            this.pbRPEHeatMap.Margin = new System.Windows.Forms.Padding(0);
            this.pbRPEHeatMap.Name = "pbRPEHeatMap";
            this.pbRPEHeatMap.Size = new System.Drawing.Size(0, 118);
            this.pbRPEHeatMap.TabIndex = 0;
            this.pbRPEHeatMap.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 58);
            this.label19.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(288, 29);
            this.label19.TabIndex = 78;
            this.label19.Text = "IMPACTS";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(195, 58);
            this.label20.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(1, 29);
            this.label20.TabIndex = 79;
            this.label20.Text = "AT A GLANCE";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(-213, 58);
            this.label21.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(304, 29);
            this.label21.TabIndex = 80;
            this.label21.Text = "PERFORMANCE";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // liveView
            // 
            this.liveView.ColumnCount = 2;
            this.liveView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1200F));
            this.liveView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.liveView.Controls.Add(this.chartPanel, 1, 0);
            this.liveView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveView.Location = new System.Drawing.Point(0, 0);
            this.liveView.Margin = new System.Windows.Forms.Padding(6);
            this.liveView.Name = "liveView";
            this.liveView.RowCount = 2;
            this.liveView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.liveView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.liveView.Size = new System.Drawing.Size(96, 100);
            this.liveView.TabIndex = 29;
            this.liveView.Paint += new System.Windows.Forms.PaintEventHandler(this.liveView_Paint);
            // 
            // chartPanel
            // 
            this.chartPanel.BackColor = System.Drawing.SystemColors.Control;
            this.chartPanel.ColumnCount = 1;
            this.chartPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.chartPanel.Controls.Add(this.dataGridView3, 0, 3);
            this.chartPanel.Controls.Add(this.tableLayoutPanel11, 0, 1);
            this.chartPanel.Controls.Add(this.tableLayoutPanel10, 0, 2);
            this.chartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel.Location = new System.Drawing.Point(1206, 6);
            this.chartPanel.Margin = new System.Windows.Forms.Padding(6);
            this.chartPanel.Name = "chartPanel";
            this.chartPanel.RowCount = 4;
            this.liveView.SetRowSpan(this.chartPanel, 2);
            this.chartPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.chartPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.chartPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.chartPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.chartPanel.Size = new System.Drawing.Size(1456, 88);
            this.chartPanel.TabIndex = 51;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(6, 99);
            this.dataGridView3.Margin = new System.Windows.Forms.Padding(6);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersWidth = 30;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(1444, 1);
            this.dataGridView3.TabIndex = 54;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.Controls.Add(this.pictureBoxHeadW, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.chartViewer, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(6, 102);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1444, 1);
            this.tableLayoutPanel11.TabIndex = 53;
            // 
            // pictureBoxHeadW
            // 
            this.pictureBoxHeadW.BackColor = System.Drawing.Color.White;
            this.pictureBoxHeadW.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxHeadW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxHeadW.Image = global::WirelessGFTViewer.Properties.Resources.HeadBlank1;
            this.pictureBoxHeadW.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxHeadW.InitialImage")));
            this.pictureBoxHeadW.Location = new System.Drawing.Point(1161, 6);
            this.pictureBoxHeadW.Margin = new System.Windows.Forms.Padding(6);
            this.pictureBoxHeadW.Name = "pictureBoxHeadW";
            this.pictureBoxHeadW.Size = new System.Drawing.Size(277, 1);
            this.pictureBoxHeadW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxHeadW.TabIndex = 9;
            this.pictureBoxHeadW.TabStop = false;
            // 
            // chartViewer
            // 
            this.chartViewer.BackSecondaryColor = System.Drawing.Color.White;
            this.chartViewer.BorderSkin.BorderColor = System.Drawing.Color.White;
            chartArea11.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea11.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea11.AxisX.IsLabelAutoFit = false;
            chartArea11.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea11.AxisX.MajorGrid.Enabled = false;
            chartArea11.AxisX.ScrollBar.BackColor = System.Drawing.Color.Silver;
            chartArea11.AxisX.ScrollBar.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            chartArea11.AxisX.Title = " Impacts";
            chartArea11.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea11.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea11.AxisX2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea11.AxisY.Interval = 50D;
            chartArea11.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea11.AxisY.IsStartedFromZero = false;
            chartArea11.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea11.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            chartArea11.AxisY.MajorTickMark.Enabled = false;
            chartArea11.AxisY.Title = "gForce";
            chartArea11.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea11.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea11.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea11.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            chartArea11.BorderColor = System.Drawing.Color.Gainsboro;
            chartArea11.CursorX.IsUserEnabled = true;
            chartArea11.CursorX.IsUserSelectionEnabled = true;
            chartArea11.Name = "ChartArea1";
            this.chartViewer.ChartAreas.Add(chartArea11);
            this.chartViewer.Cursor = System.Windows.Forms.Cursors.Cross;
            this.chartViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            legend8.Enabled = false;
            legend8.Name = "Legend1";
            this.chartViewer.Legends.Add(legend8);
            this.chartViewer.Location = new System.Drawing.Point(6, 6);
            this.chartViewer.Margin = new System.Windows.Forms.Padding(6);
            this.chartViewer.Name = "chartViewer";
            series14.ChartArea = "ChartArea1";
            series14.Color = System.Drawing.Color.Silver;
            series14.CustomProperties = "LabelStyle=BottomLeft";
            series14.IsVisibleInLegend = false;
            series14.LabelAngle = 45;
            series14.Legend = "Legend1";
            series14.Name = " ";
            series14.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            this.chartViewer.Series.Add(series14);
            this.chartViewer.Size = new System.Drawing.Size(1143, 1);
            this.chartViewer.TabIndex = 4;
            this.chartViewer.Text = "chart1";
            title10.Name = "History Data";
            this.chartViewer.Titles.Add(title10);
            this.chartViewer.Click += new System.EventHandler(this.chartViewer_Click);
            this.chartViewer.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chartViewer_MouseMove);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.chartRawGyroW, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.chartRawLinW, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(6, 99);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1444, 1);
            this.tableLayoutPanel10.TabIndex = 52;
            // 
            // chartRawGyroW
            // 
            this.chartRawGyroW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea12.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea12.AxisX.MajorGrid.Enabled = false;
            chartArea12.AxisX.ScrollBar.BackColor = System.Drawing.Color.Silver;
            chartArea12.AxisX.ScrollBar.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            chartArea12.AxisX.Title = "Rotation Velocity";
            chartArea12.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea12.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea12.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea12.AxisY.MajorGrid.Enabled = false;
            chartArea12.AxisY.MajorGrid.LineColor = System.Drawing.SystemColors.InactiveCaptionText;
            chartArea12.AxisY.MajorTickMark.Enabled = false;
            chartArea12.AxisY.Title = "Degree / S";
            chartArea12.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea12.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea12.CursorX.IsUserEnabled = true;
            chartArea12.CursorX.IsUserSelectionEnabled = true;
            chartArea12.Name = "ChartArea1";
            this.chartRawGyroW.ChartAreas.Add(chartArea12);
            this.chartRawGyroW.Cursor = System.Windows.Forms.Cursors.Cross;
            this.chartRawGyroW.Dock = System.Windows.Forms.DockStyle.Fill;
            legend9.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend9.Name = "Legend1";
            this.chartRawGyroW.Legends.Add(legend9);
            this.chartRawGyroW.Location = new System.Drawing.Point(728, 6);
            this.chartRawGyroW.Margin = new System.Windows.Forms.Padding(6);
            this.chartRawGyroW.Name = "chartRawGyroW";
            series15.ChartArea = "ChartArea1";
            series15.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            series15.Legend = "Legend1";
            series15.Name = "Rot. X";
            series15.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series16.ChartArea = "ChartArea1";
            series16.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            series16.Legend = "Legend1";
            series16.Name = "Rot. Y";
            series17.ChartArea = "ChartArea1";
            series17.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            series17.Legend = "Legend1";
            series17.Name = "Linear Z";
            series18.ChartArea = "ChartArea1";
            series18.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));
            series18.Legend = "Legend1";
            series18.Name = "Linear Res.";
            this.chartRawGyroW.Series.Add(series15);
            this.chartRawGyroW.Series.Add(series16);
            this.chartRawGyroW.Series.Add(series17);
            this.chartRawGyroW.Series.Add(series18);
            this.chartRawGyroW.Size = new System.Drawing.Size(710, 1);
            this.chartRawGyroW.TabIndex = 7;
            this.chartRawGyroW.Text = "Chart";
            this.chartRawGyroW.Visible = false;
            // 
            // chartRawLinW
            // 
            this.chartRawLinW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea13.AxisX.Interval = 10D;
            chartArea13.AxisX.IntervalOffset = -1D;
            chartArea13.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(208)))), ((int)(((byte)(224)))));
            chartArea13.AxisX.MajorGrid.Enabled = false;
            chartArea13.AxisX.ScrollBar.BackColor = System.Drawing.Color.Silver;
            chartArea13.AxisX.ScrollBar.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            chartArea13.AxisX.Title = "Linear Acceleration";
            chartArea13.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea13.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea13.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea13.AxisY.MajorGrid.Enabled = false;
            chartArea13.AxisY.MajorGrid.LineColor = System.Drawing.SystemColors.InactiveCaptionText;
            chartArea13.AxisY.MajorTickMark.Enabled = false;
            chartArea13.AxisY.Title = "gForce";
            chartArea13.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea13.AxisY.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(134)))), ((int)(((byte)(159)))));
            chartArea13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            chartArea13.BorderColor = System.Drawing.Color.DimGray;
            chartArea13.CursorX.IsUserEnabled = true;
            chartArea13.CursorX.IsUserSelectionEnabled = true;
            chartArea13.Name = "ChartArea1";
            this.chartRawLinW.ChartAreas.Add(chartArea13);
            this.chartRawLinW.Cursor = System.Windows.Forms.Cursors.Cross;
            this.chartRawLinW.Dock = System.Windows.Forms.DockStyle.Fill;
            legend10.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend10.Name = "Legend1";
            this.chartRawLinW.Legends.Add(legend10);
            this.chartRawLinW.Location = new System.Drawing.Point(6, 6);
            this.chartRawLinW.Margin = new System.Windows.Forms.Padding(6);
            this.chartRawLinW.Name = "chartRawLinW";
            series19.ChartArea = "ChartArea1";
            series19.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            series19.Legend = "Legend1";
            series19.Name = "Linear X";
            series19.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series20.ChartArea = "ChartArea1";
            series20.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            series20.Legend = "Legend1";
            series20.Name = "Linear Y";
            series21.ChartArea = "ChartArea1";
            series21.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            series21.Legend = "Legend1";
            series21.Name = "Linear Z";
            series22.ChartArea = "ChartArea1";
            series22.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));
            series22.Legend = "Legend1";
            series22.Name = "Linear Res.";
            this.chartRawLinW.Series.Add(series19);
            this.chartRawLinW.Series.Add(series20);
            this.chartRawLinW.Series.Add(series21);
            this.chartRawLinW.Series.Add(series22);
            this.chartRawLinW.Size = new System.Drawing.Size(710, 1);
            this.chartRawLinW.TabIndex = 8;
            this.chartRawLinW.Text = "Chart";
            this.chartRawLinW.Visible = false;
            // 
            // buttonPanel
            // 
            this.buttonPanel.ColumnCount = 4;
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.buttonPanel.Controls.Add(this.m_button_Clone, 1, 0);
            this.buttonPanel.Controls.Add(this.mUpdateByUSB2, 2, 3);
            this.buttonPanel.Controls.Add(this.m_timeSync, 0, 4);
            this.buttonPanel.Controls.Add(this.btnReset, 2, 2);
            this.buttonPanel.Controls.Add(this.usbUpload_Enable, 2, 0);
            this.buttonPanel.Controls.Add(this.autoUpload_enable, 2, 1);
            this.buttonPanel.Controls.Add(this.mWirelessEnable, 3, 1);
            this.buttonPanel.Controls.Add(this.mbutton_Bind_with_unbind, 0, 0);
            this.buttonPanel.Controls.Add(this.mGFT, 0, 1);
            this.buttonPanel.Location = new System.Drawing.Point(0, 0);
            this.buttonPanel.Margin = new System.Windows.Forms.Padding(12, 6, 12, 6);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.RowCount = 5;
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.buttonPanel.Size = new System.Drawing.Size(1176, 104);
            this.buttonPanel.TabIndex = 3;
            this.buttonPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPanel_Paint);
            // 
            // m_button_Clone
            // 
            this.m_button_Clone.AutoSize = true;
            this.m_button_Clone.Enabled = false;
            this.m_button_Clone.Location = new System.Drawing.Point(300, 6);
            this.m_button_Clone.Margin = new System.Windows.Forms.Padding(6);
            this.m_button_Clone.Name = "m_button_Clone";
            this.m_button_Clone.Size = new System.Drawing.Size(282, 48);
            this.m_button_Clone.TabIndex = 68;
            this.m_button_Clone.Text = "Remove Selected";
            this.m_button_Clone.UseVisualStyleBackColor = true;
            this.m_button_Clone.Click += new System.EventHandler(this.m_button_Clone_Click);
            // 
            // mUpdateByUSB2
            // 
            this.mUpdateByUSB2.AutoSize = true;
            this.mUpdateByUSB2.Location = new System.Drawing.Point(594, 162);
            this.mUpdateByUSB2.Margin = new System.Windows.Forms.Padding(6);
            this.mUpdateByUSB2.Name = "mUpdateByUSB2";
            this.mUpdateByUSB2.Size = new System.Drawing.Size(282, 67);
            this.mUpdateByUSB2.TabIndex = 132;
            this.mUpdateByUSB2.Text = "Update by USB";
            this.mUpdateByUSB2.UseVisualStyleBackColor = true;
            this.mUpdateByUSB2.Visible = false;
            this.mUpdateByUSB2.Click += new System.EventHandler(this.mUpdateByUSB0_Click);
            // 
            // m_timeSync
            // 
            this.m_timeSync.AutoSize = true;
            this.m_timeSync.Location = new System.Drawing.Point(6, 241);
            this.m_timeSync.Margin = new System.Windows.Forms.Padding(6);
            this.m_timeSync.Name = "m_timeSync";
            this.m_timeSync.Size = new System.Drawing.Size(246, 67);
            this.m_timeSync.TabIndex = 78;
            this.m_timeSync.Text = "Time Sync";
            this.m_timeSync.UseVisualStyleBackColor = true;
            this.m_timeSync.Click += new System.EventHandler(this.m_timeSync_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(594, 95);
            this.btnReset.Margin = new System.Windows.Forms.Padding(6);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(272, 44);
            this.btnReset.TabIndex = 158;
            this.btnReset.Text = "Reset GFT by USB";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // usbUpload_Enable
            // 
            this.usbUpload_Enable.AutoSize = true;
            this.usbUpload_Enable.Checked = true;
            this.usbUpload_Enable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.usbUpload_Enable.Location = new System.Drawing.Point(594, 6);
            this.usbUpload_Enable.Margin = new System.Windows.Forms.Padding(6);
            this.usbUpload_Enable.Name = "usbUpload_Enable";
            this.usbUpload_Enable.Size = new System.Drawing.Size(121, 17);
            this.usbUpload_Enable.TabIndex = 73;
            this.usbUpload_Enable.Text = "USB Upload Enable";
            this.usbUpload_Enable.UseVisualStyleBackColor = true;
            this.usbUpload_Enable.Visible = false;
            this.usbUpload_Enable.CheckedChanged += new System.EventHandler(this.usbUpload_Enable_CheckedChanged);
            // 
            // autoUpload_enable
            // 
            this.autoUpload_enable.AutoSize = true;
            this.autoUpload_enable.Checked = true;
            this.autoUpload_enable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoUpload_enable.Location = new System.Drawing.Point(888, 66);
            this.autoUpload_enable.Margin = new System.Windows.Forms.Padding(6);
            this.autoUpload_enable.Name = "autoUpload_enable";
            this.autoUpload_enable.Size = new System.Drawing.Size(121, 17);
            this.autoUpload_enable.TabIndex = 71;
            this.autoUpload_enable.Text = "Auto Upload Enable";
            this.autoUpload_enable.UseVisualStyleBackColor = true;
            this.autoUpload_enable.Visible = false;
            this.autoUpload_enable.CheckedChanged += new System.EventHandler(this.autoUpload_enable_CheckedChanged);
            // 
            // mWirelessEnable
            // 
            this.mWirelessEnable.AutoSize = true;
            this.mWirelessEnable.Enabled = false;
            this.mWirelessEnable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mWirelessEnable.Location = new System.Drawing.Point(6, 95);
            this.mWirelessEnable.Margin = new System.Windows.Forms.Padding(6);
            this.mWirelessEnable.Name = "mWirelessEnable";
            this.mWirelessEnable.Size = new System.Drawing.Size(126, 20);
            this.mWirelessEnable.TabIndex = 130;
            this.mWirelessEnable.Text = "Wireless Enable";
            this.mWirelessEnable.UseVisualStyleBackColor = true;
            this.mWirelessEnable.Visible = false;
            // 
            // mbutton_Bind_with_unbind
            // 
            this.mbutton_Bind_with_unbind.AutoSize = true;
            this.mbutton_Bind_with_unbind.Enabled = false;
            this.mbutton_Bind_with_unbind.Location = new System.Drawing.Point(6, 6);
            this.mbutton_Bind_with_unbind.Margin = new System.Windows.Forms.Padding(6);
            this.mbutton_Bind_with_unbind.Name = "mbutton_Bind_with_unbind";
            this.mbutton_Bind_with_unbind.Size = new System.Drawing.Size(280, 48);
            this.mbutton_Bind_with_unbind.TabIndex = 135;
            this.mbutton_Bind_with_unbind.Text = "Add";
            this.mbutton_Bind_with_unbind.UseVisualStyleBackColor = true;
            this.mbutton_Bind_with_unbind.Click += new System.EventHandler(this.mbutton_Bind_with_unbind_Click);
            // 
            // mGFT
            // 
            this.mGFT.AutoSize = true;
            this.buttonPanel.SetColumnSpan(this.mGFT, 3);
            this.mGFT.Location = new System.Drawing.Point(6, 60);
            this.mGFT.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.mGFT.Name = "mGFT";
            this.mGFT.Size = new System.Drawing.Size(28, 13);
            this.mGFT.TabIndex = 155;
            this.mGFT.Text = "GFT";
            this.mGFT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // wirelessViewerCtl
            // 
            this.wirelessViewerCtl.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.wirelessViewerCtl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wirelessViewerCtl.Location = new System.Drawing.Point(3, 3);
            this.wirelessViewerCtl.Margin = new System.Windows.Forms.Padding(6);
            this.wirelessViewerCtl.Name = "wirelessViewerCtl";
            this.wirelessViewerCtl.node_ID = 0;
            this.wirelessViewerCtl.selectedRow = 0;
            this.wirelessViewerCtl.Size = new System.Drawing.Size(594, 624);
            this.wirelessViewerCtl.TabIndex = 2;
            this.wirelessViewerCtl.OnRowSelected += new WirelessGFTViewer.LightOnChangedEventHandler(this.wirelessViewer_OnRowSelected);
            this.wirelessViewerCtl.OnButtonUploadAll += new WirelessGFTViewer.UploadAllHandler(this.wirelessViewer_OnButtonUploadAll);
            this.wirelessViewerCtl.OnButtonPowerOffAll += new WirelessGFTViewer.PowerOffAllHandler(this.wirelessViewer_OnButtonPowerOffAll);
            this.wirelessViewerCtl.OnButtonEnableProximityAll += new WirelessGFTViewer.EnableProximityAllHandler(this.wirelessViewer_OnButtonEnableProximityAll);
            this.wirelessViewerCtl.OnButtonDisableProximityAll += new WirelessGFTViewer.DisableProximityAllHandler(this.wirelessViewer_OnButtonDisableProximityAll);
            this.wirelessViewerCtl.OnButtonStopRecordingAll += new WirelessGFTViewer.StopRecordingAllHandler(this.wirelessViewer_OnButtonStopRecordingAll);
            this.wirelessViewerCtl.OnButtonStartRecordingAll += new WirelessGFTViewer.StartRecordingAllHandler(this.wirelessViewer_OnButtonStartRecordingAll);
            this.wirelessViewerCtl.OnButtonPowerOnAll += new WirelessGFTViewer.PowerOnAllHandler(this.wirelessViewer_OnButtonPowerOnAll);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "GFTID";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 4;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1334, 653);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(967, 558);
            this.Name = "Form1";
            this.Text = "Sideline Viewer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResizeEnd += new System.EventHandler(this.Form1_SizeChanged);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.dashboard.ResumeLayout(false);
            this.dashboard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamChart)).EndInit();
            this.tableLayoutPanel27.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel28.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActivePct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewerPlayer)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.activityChartPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeatMap)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBatStat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.locationChartPlayer)).EndInit();
            this.consumerPlayerPanel.ResumeLayout(false);
            this.consumerPlayerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartEP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartViewerPlayer)).EndInit();
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tableLayoutPanel29.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.panel111.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.perfLocationChartPlayer)).EndInit();
            this.panel101.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPerfHead)).EndInit();
            this.tableLayoutPanel30.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.perfChartRPE)).EndInit();
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRPEHeatMap)).EndInit();
            this.liveView.ResumeLayout(false);
            this.chartPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeadW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartRawGyroW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartRawLinW)).EndInit();
            this.buttonPanel.ResumeLayout(false);
            this.buttonPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelChart;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelGFT;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.ToolStripStatusLabel m_pStaticMode;
        private System.Windows.Forms.Timer mtimer;
        private System.Windows.Forms.OpenFileDialog openFileDialogFirm;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel liveView;
        private System.Windows.Forms.TableLayoutPanel chartPanel;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.PictureBox pictureBoxHeadW;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartViewer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartRawGyroW;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartRawLinW;
        private System.Windows.Forms.TableLayoutPanel buttonPanel;
        private System.Windows.Forms.Button m_button_Clone;
        private System.Windows.Forms.Button mbutton_Bind_with_unbind;
        private System.Windows.Forms.Button mUpdateByUSB2;
        private System.Windows.Forms.Button m_timeSync;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox simUploads;
        private System.Windows.Forms.CheckBox usbUpload_Enable;
        private System.Windows.Forms.CheckBox autoUpload_enable;
        private System.Windows.Forms.CheckBox mWirelessEnable;
        private System.Windows.Forms.Label mGFT;
        private WirelessGFTViewer.WirelessViewer wirelessViewerCtl;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel dashboard;
        private WirelessGFTViewer.PlayerReview playerReviewer;
        private System.Windows.Forms.DataVisualization.Charting.Chart teamChart;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label HdrRotation;
        private System.Windows.Forms.Label lblTeamRotationAveMed;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label HdrExposure;
        private System.Windows.Forms.Label lblTeamExposure;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label HdrThreshold;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblTeamThreshold;
        private System.Windows.Forms.Label lblTeamThresholdWithin;
        private System.Windows.Forms.Label lblTeamThresholdImpacts;
        private System.Windows.Forms.LinkLabel lnkRotation;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart ActivePct;
        private System.Windows.Forms.DataGridView dataGridViewPlayer;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartViewerPlayer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label HdrResGForce;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblGForceAveMed;
        private System.Windows.Forms.Label lblHighestG;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label HdrResImpacts;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblThresholdTotal;
        private System.Windows.Forms.Label lblThresholdWithinAbove;
        private System.Windows.Forms.Label lblThresholdImpacts;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label HdrResRotation;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblRotationAveMed;
        private System.Windows.Forms.Label lblHighestR;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label HdrResExposure;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblExposure;
        private System.Windows.Forms.DataVisualization.Charting.Chart activityChartPlayer;
        private System.Windows.Forms.PictureBox pbHead;
        private System.Windows.Forms.PictureBox pbHeatMap;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblPlayer;
        private System.Windows.Forms.Label lblDeviceSettings;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.PictureBox pbBatStat;
        private System.Windows.Forms.DataVisualization.Charting.Chart locationChartPlayer;
        private System.Windows.Forms.Label lblRecovery;
        private System.Windows.Forms.TableLayoutPanel consumerPlayerPanel;
        private System.Windows.Forms.DataVisualization.Charting.Chart perfChartViewerPlayer;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Label HdrPerfGForce;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPerfGForceMed;
        private System.Windows.Forms.Label lblPerfHighestG;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Label HdrPerfThreshold;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblPerfThresholdAbove;
        private System.Windows.Forms.Label lblPerfThresholdWithinAbove;
        private System.Windows.Forms.Label lblPerfThresholdImpacts;
        private System.Windows.Forms.Panel panel111;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Label HdrPerfRotation;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblPerfRotationMed;
        private System.Windows.Forms.Label lblPerfHighestR;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Label HdrPerfPlayerLoad;
        private System.Windows.Forms.Label lblPerfPlayerLoad;
        private System.Windows.Forms.PictureBox pbPerfHead;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Label lblPerfPlayer;
        private System.Windows.Forms.Label lblPerfStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Button btnPerfBack;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.DataVisualization.Charting.Chart perfLocationChartPlayer;
        private System.Windows.Forms.Label lblPerfRecovery;
        private System.Windows.Forms.DataVisualization.Charting.Chart perfChartRPE;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataVisualization.Charting.Chart perfChartEP;
        private System.Windows.Forms.PictureBox pbRPEHeatMap;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPerfEPHighest;
        private System.Windows.Forms.Label lblPerfEP;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblPerfRPE;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblPerfPLAve;
        private System.Windows.Forms.Label lblPerfPlayerLoadText;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label HdrGForce;
        private System.Windows.Forms.Label lblTeamGForceAveMed;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblTeamGForce;
        private System.Windows.Forms.LinkLabel lnkGForce;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button powerOffGFTs;
        private System.Windows.Forms.Button UploadAll;
        private System.Windows.Forms.Button powerOnGFTs;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataVisualization.Charting.Chart perfChartPL;
        private System.Windows.Forms.Label lblPerfRotationAve;
        private System.Windows.Forms.Label lblPerfGForceAve;
        private System.Windows.Forms.Label lblPerfEPRating;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.LinkLabel linkRPE;
        private System.Windows.Forms.LinkLabel linkPL;
        private System.Windows.Forms.LinkLabel linkEP;
        private System.Windows.Forms.Label lblPerfPLHighest;
        private System.Windows.Forms.Label lblExpSec;
        private System.Windows.Forms.Button btnEmptyHub;
        private System.Windows.Forms.CheckBox mEraseWholeData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label DongleEmpty;
        private System.Windows.Forms.Label mTryCount;
        private System.Windows.Forms.Button mStopBroadCast;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button mInitBLE;
        private System.Windows.Forms.Button mGetNodeInfo;
        private System.Windows.Forms.Button mGetHubInfo;
        private System.Windows.Forms.Button mConnBle;
        private System.Windows.Forms.ComboBox mBT_Addr;
        private System.Windows.Forms.ComboBox mBT_Name;
    }
}


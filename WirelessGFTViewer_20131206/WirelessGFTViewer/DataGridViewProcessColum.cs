﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;


namespace WirelessGFTViewer
{
    public class DataGridViewProcessColum:DataGridViewImageColumn
    {
//        CellTemplate = new DataGridViewProgressCell();
    }
}

namespace WirelessGFTViewer
{
    class DataGridViewProgressCell : DataGridViewImageCell  
    {
        // Used to make custom cell consistent with a DataGridViewImageCell
        static Image emptyImage;

        static DataGridViewProgressCell()
        {
            emptyImage = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
        }

        public DataGridViewProgressCell()
        {
            this.ValueType = typeof(int);
        }


        // Method required to make the Progress Cell consistent with the default Image Cell. 
        // The default Image Cell assumes an Image as a value, although the value of the Progress Cell is an int.
        protected override object GetFormattedValue(object value,
                            int rowIndex, ref DataGridViewCellStyle cellStyle,
                            TypeConverter valueTypeConverter,
                            TypeConverter formattedValueTypeConverter,
                            DataGridViewDataErrorContexts context)
        {
            return emptyImage;
        }
        
        protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle clipBounds, System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            int progressVal = (int)value;
            Color activeBorder = Color.FromArgb(235, 35, 175, 35);
            float percentage = ((float)progressVal / 100.0f); // Need to convert to float before division; otherwise C# returns int which is 0 for anything but 100%.
            Brush backColorBrush = new SolidBrush(cellStyle.BackColor);
            Brush foreColorBrush = new SolidBrush(cellStyle.ForeColor);

            int yTop = cellBounds.Y + (cellBounds.Height - 18) / 2;
            if (false)
            {
                // Draws the cell grid
                base.Paint(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, (paintParts & ~DataGridViewPaintParts.ContentForeground));
                if (percentage > 0.0)
                {
                    // Draw the progress bar and the text
                    //g.FillRectangle(new SolidBrush(Color.FromArgb(163, 189, 242)), cellBounds.X + 2, cellBounds.Y + 2, Convert.ToInt32((percentage * cellBounds.Width - 4)), cellBounds.Height - 4);
                    g.FillRectangle(new SolidBrush(Color.LimeGreen), cellBounds.X + 2, cellBounds.Y + 2, Convert.ToInt32((percentage * cellBounds.Width - 4)), cellBounds.Height - 4);
                    //g.DrawString(progressVal.ToString() + "%", cellStyle.Font, foreColorBrush, cellBounds.X + 6, cellBounds.Y + 2);
                    g.DrawString(progressVal.ToString() + "%", cellStyle.Font, foreColorBrush, cellBounds.X + cellBounds.Size.Width / 2 - 10, cellBounds.Y + cellBounds.Size.Height / 2 - 6);
                }
                else
                {
                    // draw the text
                    if (this.DataGridView.CurrentRow != null)
                    {
                        if (this.DataGridView.CurrentRow.Index == rowIndex)
                            //g.DrawString(progressVal.ToString() + "%", cellStyle.Font, new SolidBrush(cellStyle.SelectionForeColor), cellBounds.X + 6, cellBounds.Y + 2);
                            g.DrawString(progressVal.ToString() + "%", cellStyle.Font, new SolidBrush(cellStyle.SelectionForeColor), cellBounds.X + cellBounds.Size.Width / 2 - 10, cellBounds.Y + cellBounds.Size.Height / 2 - 6);
                        //else
                        //g.DrawString(progressVal.ToString() + "%", cellStyle.Font, foreColorBrush, cellBounds.X + 6, cellBounds.Y + 2);
                        //      g.DrawString(progressVal.ToString() + "%", cellStyle.Font, foreColorBrush, cellBounds.X+cellBounds.Size.Width / 2, cellBounds.Y + 2);
                    }
                }
            }
            else
            {
                // Draws the cell grid
                base.Paint(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, (paintParts & ~DataGridViewPaintParts.ContentForeground));
                if (percentage > 0.0)
                {
                    // Draw the progress bar and the text
                    g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X, yTop - 2, cellBounds.Width -2, 22);
                    g.FillRectangle(new SolidBrush(Color.White), cellBounds.X + 2, yTop, cellBounds.Width - 6, 18);
                    g.FillRectangle(new SolidBrush(Color.LimeGreen), cellBounds.X + 2, yTop, Convert.ToInt32((percentage * cellBounds.Width - 6)), 18);
                    g.DrawString(progressVal.ToString() + "%", cellStyle.Font, foreColorBrush, cellBounds.X + cellBounds.Size.Width / 2 - 10, cellBounds.Y + cellBounds.Size.Height / 2 - 6);
                }
                else
                {
                    // draw the text
                    g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X, yTop - 2, cellBounds.Width -2, 22);
                    g.FillRectangle(new SolidBrush(Color.White), cellBounds.X + 2, yTop, cellBounds.Width - 6, 18);
                    g.DrawString(progressVal.ToString() + "%", cellStyle.Font, new SolidBrush(cellStyle.SelectionForeColor), cellBounds.X + cellBounds.Size.Width / 2 - 10, cellBounds.Y + cellBounds.Size.Height / 2 - 6);

                }
            }
        }
    }
}

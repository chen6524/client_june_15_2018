﻿//#define ENABLE_DATA_LOGGER
using Microsoft.Win32.SafeHandles;
using Microsoft.VisualBasic;
using System.ComponentModel;

//using ExcelLibrary.SpreadSheet;
//using ExcelLibrary.CompoundDocumentFormat;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Pipes;
using System.Diagnostics;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Collections;
using System.Threading;
using System;
using System.Timers;
using System.Globalization;
using ArtaFlexHidWin;

namespace gForceTrackerWireless
{
    //public clase

    public partial class Form1 : UsbAwareForm
    {
        int ToBytes(string hexLine, out byte[] hexRecord)
        {
            hexRecord = null;

            //            if (hexLine[0] != ':')
            //                return (-1);

            int lineLen = hexLine.Length;

            //            if ((lineLen % 2) != 1)
            //                return (-2);			// Wrong length

            hexRecord = new byte[lineLen / 2];

            string line = hexLine.ToUpper();
            int i, len = 0;
            for (i = 0; i < lineLen - 1; i += 2)
            {
                byte b1 = (byte)line[i];
                byte b0 = (byte)line[i + 1];

                if (('0' <= b1) && (b1 <= '9'))
                    b1 -= (byte)'0';
                else if (('A' <= b1) && (b1 <= 'F'))
                    b1 = (byte)(10 + (b1 - 'A'));
                else
                    return (-2);

                if (('0' <= b0) && (b0 <= '9'))
                    b0 -= (byte)'0';
                else if (('A' <= b0) && (b0 <= 'F'))
                    b0 = (byte)(10 + (b0 - 'A'));
                else
                    return (-2);

                hexRecord[len] = (byte)((b1 << 4) | b0);
                ++len;
            }
            /*
            byte checkSum = 0;
            for (i = 0; i < len; i++)
                checkSum += hexRecord[i];
            checkSum = (byte)~checkSum;

            if (checkSum != 0x00)
                return (-3);
            */

            return len;
        }


        private ushort byteToShort(byte h, byte l)
        {
            return (ushort)(((ushort)h << 8) | l);

        }
        private short byteToSignedShort(byte h, byte l)
        {
            return (short)(((short)h << 8) | l);

        }

        public static double lgDecode(byte h, byte l)
        {
            return (0.012f * (short)(((short)(h << 8 | l)) >> 4));
        }
        
        public static double lgDecodeDn(byte h, byte l, double lgRes)
        {
            //return (lgRes * (short)(((short)(h << 8 | l)) >> 4));
            short d;
            d = (short)((short)(h << 8 | l));

            return (lgRes * d);
        }


        private double AccDecode2(byte c)
        {
            return (ACC_SENSITIVITY_LSB2 * (c - 128));
        }

#if ENABLE_ACC_OUTPUT
        private double gyroDecode(byte h, byte l)                  // Added by Jason Chen for LG test, 2014.05.07
        {
            short lgdata = (short)(h << 8 | l);
            lgdata = (short)(lgdata / 16);
            return (0.012 * lgdata);
        }
#else
        private double gyroDecode(byte h, byte l)
        {
            return (GYRO_SENSITIVITY_LSB * (short)(h << 8 | l));
        }
#endif
        static bool ValidateTime(out DateTime outTime, byte year, byte month, byte day, byte hour, byte minute, byte second, ushort milli)
        {
            string format = "yyyy:MM:dd:HH:mm:ss:fff";
            string myTime;
            myTime = string.Format("{0:d2}", 2000 + year) + ":";
            myTime += string.Format("{0:d2}", month) + ":";
            myTime += string.Format("{0:d2}", day) + ":";
            myTime += string.Format("{0:d2}", hour) + ":";
            myTime += string.Format("{0:d2}", minute) + ":";
            myTime += string.Format("{0:d2}", second) + ":";

            myTime += string.Format("{0:d3}", milli);

            return DateTime.TryParseExact(myTime, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out outTime);

        }

        //DateTime sessionStartTime;
        //DateTime sessionEndTime;
        //int sessionStartTimeF = 0;
        //int sessionEndTimeF = 0;
        public Queue<byte[]> inputReport_Queue = new Queue<byte[]>();      // Added by Jason Chen, 2014.05.29
        internal const byte GFT_STATE_NONE = 0;
        internal const byte GFT_STATE_BOOT = 1;
        internal const byte GFT_STATE_FIRM = 2;
        byte gftWhereAmI = 0;//1: bootldr 2:firm

        public AutoResetEvent oSignalEvent = new AutoResetEvent(false);
        private static Mutex queue_mut = new Mutex();
        private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();

#if true
        
        private int gftStat = 0;
        private bool transferSuccess = false;
        string filenameFirm = "";
        private const int rom_endaddr = 0xefff;

        private byte[] sendCmd(byte cmd, byte subCmd)
        {
           // string result = "failed";
            byte[] outputReportBuffer = new Byte[64 + 1];
            byte[] inputReportBuffer = null;// new Byte[64 + 1];

            try
            {
                outputReportBuffer[0] = 0;// HID_REPORT_ID;
                outputReportBuffer[1] = cmd;
                outputReportBuffer[2] = subCmd;
                transferSuccess = false;

                //if (!MyHid.OutputReports(outputReportBuffer))
                //    return "failed";

                try
                {
                    gForceHid.WriteOutput(ref outputReportBuffer);
                }
                catch (Exception xx)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                    return null;
                }
                //AnonymousPipeClientStream pipeClient = new AnonymousPipeClientStream(PipeDirection.In, pipeHandle);
                //PipeStream pipeClient = new AnonymousPipeClientStream(PipeDirection.In, pipeHandle);
                //using(StreamReader sr = new StreamReader(pipeClient))


                //                    string temp;
                //                    do
                //                    {                        
                //                        temp = sr.ReadLine();
                //                    } 
                //                    while (!temp.StartsWith("SYNC"));
                int timeout = 0;
                while (true)
                {
                    if (oSignalEvent.WaitOne(100))
                    {
                        if (inputReport_Queue.Count > 0)
                        {
                            inputReportBuffer = inputReport_Queue.Dequeue();
                            //if (inputReportBuffer[1] == (byte)(cmd | MHID_CMD_RESPONSE_BIT))
                            //{
                            //}
                            //else
                            //    inputReportBuffer = null;
                        }
                        else
                        {
                            inputReportBuffer = null;
                        }
                        oSignalEvent.Reset();
                        break;
                    }
                    else
                    {
                        //MessageBox.Show("No Response");//caption, buttons);
                        Application.DoEvents();
                        timeout++;
                        if (timeout > 20)
                        {
                            inputReportBuffer = null;
                            break;
                        }
                    }
                    
                }

            }
            catch (Exception ex)
            {
                DisplayException("Send Command", ex);
            }
            return inputReportBuffer;

        }

        private byte[] sendCmd(ref byte[] txbuff)
        {
            // string result = "failed";
            //byte[] outputReportBuffer = new Byte[64 + 1];
            byte[] inputReportBuffer = null;// new Byte[64 + 1];

            try
            {
                //outputReportBuffer[0] = 0;// HID_REPORT_ID;
                //outputReportBuffer[1] = cmd;
                //outputReportBuffer[2] = subCmd;
                transferSuccess = false;

                //if (!MyHid.OutputReports(outputReportBuffer))
                //    return "failed";

                try
                {
                    gForceHid.WriteOutput(ref txbuff);
                }
                catch (Exception xx)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                    return null;
                }

                int timeout = 0;
                Thread.Sleep(10);
                Application.DoEvents();
                while (true)
                {                   
                    if (oSignalEvent.WaitOne(10))
                    {
                        if (inputReport_Queue.Count > 0)
                        {
                            inputReportBuffer = inputReport_Queue.Dequeue();
                            //if (inputReportBuffer[1] == (byte)(cmd | MHID_CMD_RESPONSE_BIT))
                            //{
                            //}
                            //else
                            //    inputReportBuffer = null;
                        }
                        else
                        {
                            inputReportBuffer = null;
                        }
                        oSignalEvent.Reset();
                        break;
                    }
                    else
                    {
                        Application.DoEvents();
                        timeout++;
                        if (timeout > 300)
                        {
                            inputReportBuffer = null;
                            break;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                DisplayException("Send Command", ex);
            }
            return inputReportBuffer;

        }
        private string program()
        {
            //BackgroundWorker worker = sender as BackgroundWorker;
            byte[] outputReportBuffer = new Byte[64 + 1];
            byte[] inputReportBuffer = new Byte[64 + 1];
            ushort ImageLen;
            string OneLineRecord = " ";
            ushort StartAddress, i;
            string temp;
            bool succ = false;
            //int nread = 0;
            //string retstr = null;
            string lastRecord = "";

            StreamReader sw = File.OpenText(filenameFirm);
            while (gForceHid != null)//MyHid.isDeviceAttached)
            {
                lastRecord = OneLineRecord;
                OneLineRecord = sw.ReadLine();
                temp = OneLineRecord.Substring(0, 2);
                if (OneLineRecord.Substring(0, 2) == "S1")
                {
                    temp = OneLineRecord.Substring(2, 2);

                    byte[] RecordLen;
                    int len1 = ToBytes(temp, out RecordLen);

                    ImageLen = (ushort)(RecordLen[0]);
                    temp = OneLineRecord.Substring(2, (ImageLen + 1) * 2);

                    byte[] hexRecord;
                    int len = ToBytes(temp, out hexRecord);
                    StartAddress = (ushort)(hexRecord[2] | (hexRecord[1] << 8));
                    //if ((StartAddress >= 0x1960) && (StartAddress < rom_endaddr + 1))
                    if (StartAddress < rom_endaddr + 1)
                    {

                        if ((StartAddress + ImageLen - 3) > rom_endaddr)
                        {
                            ImageLen = (ushort)(rom_endaddr - StartAddress + 1 + 3);
                        }

                        if (ImageLen - 3 <= 55) // we can have 64-4-1
                        {

                            for (i = 0; i < ImageLen - 3; i++)
                            {
                                outputReportBuffer[i + 5] = hexRecord[i + 3];
                            }

                            outputReportBuffer[1] = (byte)GFT_MHID_CMD.MHID_CMD_PROG_FIRM;
                            outputReportBuffer[2] = (byte)(byte)(ImageLen - 3);
                            outputReportBuffer[3] = (byte)((StartAddress & 0xFF00) >> 8);
                            outputReportBuffer[4] = (byte)(StartAddress & 0x00FF);
                            succ = false;
                            transferSuccess = false;
                            //succ = MyHid.OutputControl(outputReportBuffer);
                            //succ = MyHid.OutputReports(outputReportBuffer);
#if false
                            try
                            {
                                gForceHid.WriteOutput(ref outputReportBuffer);
                                succ = true;
                            }
                            catch (Exception xx)
                            {
                                if (TooltabPage16Exist)
                                    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                                succ = false;
                            }
                            //MyHid.OutputReports(outputReportBuffer);
#endif
                            inputReportBuffer = sendCmd(ref outputReportBuffer);
                            if (inputReportBuffer != null)
                                succ = true;
                            else
                                succ = false;
                            if (succ)
                            {
                                //Thread.Sleep(5);
                                //transferInProgress = true;
#if false
                                //succ = MyHid.inputReports(ref inputReportBuffer, ref nread);
                                if (inputReport_Queue.Count > 0)
                                {
                                    inputReportBuffer = inputReport_Queue.Dequeue();
                                    succ = true;
                                }
                                else
                                    succ = false;
#endif                                
                                if (succ)
                                {
                                    if ((inputReportBuffer[0] == (byte)((int)GFT_MHID_CMD.MHID_CMD_PROG_FIRM | MHID_CMD_RESPONSE_BIT))
                                        && (inputReportBuffer[2] == 0))
                                    {
                                        //;// worker.ReportProgress(StartAddress, ImageLen - 3);
                                        try
                                        {
                                            //if ((m_prog_progress.Value + ImageLen) < m_prog_progress.Maximum)
                                            //    m_prog_progress.Value += ImageLen;
                                            //else
                                            //    m_prog_progress.Value = m_prog_progress.Maximum;
                                        }
                                        catch (Exception cc)
                                        {
                                            //m_Edit_Immediate.AppendText("---" + cc.Message + "\r\n");
                                        }
                                    }
                                    else
                                    {
                                        sw.Close();
                                        MessageBox.Show("Programming Error!");
                                        return "error1";
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("program error2");
                                }
                            }
                            else
                            {
                                //MessageBox.Show("program error1", OneLineRecord);
                                //if (OneLineRecord == "S10B12000000000000000000E2")
                                //    m_Edit_Immediate.AppendText("---This record [" + OneLineRecord + "] don't need to be Programed\r\n");
                                //else
                                //    m_Edit_Immediate.AppendText("---Program Error 1: " + "[" + OneLineRecord + "]\r\n");
                            }
                        }
                        else
                        {
                            sw.Close();
                            MessageBox.Show("program error3");
                            return " error2";
                        }
                    }
                    else
                    {
                        sw.Close();
                        MessageBox.Show("program error 4");

                    }
                }
                else if (OneLineRecord.Substring(0, 2) == "S9")
                {
                    sw.Close();
                    //worker.ReportProgress(100,0);
                    //m_progress.Value = m_progress.Maximum;
                    return "finished";
                }
            }
            return "finished";
        }

        private int getFirmwareSize()
        {
            //BackgroundWorker worker = sender as BackgroundWorker;
            ushort ImageLen;
            string OneLineRecord = " ";
            ushort StartAddress, codeSize;
            string temp;
            string lastRecord = "";

            codeSize = 0;
            StreamReader sw = File.OpenText(filenameFirm);
            while (gForceHid != null)//MyHid.isDeviceAttached)
            {
                lastRecord = OneLineRecord;
                OneLineRecord = sw.ReadLine();
                temp = OneLineRecord.Substring(0, 2);
                if (OneLineRecord.Substring(0, 2) == "S1")
                {
                    temp = OneLineRecord.Substring(2, 2);

                    byte[] RecordLen;
                    int len1 = ToBytes(temp, out RecordLen);

                    ImageLen = (ushort)(RecordLen[0]);
                    temp = OneLineRecord.Substring(2, (ImageLen + 1) * 2);

                    byte[] hexRecord;
                    int len = ToBytes(temp, out hexRecord);
                    StartAddress = (ushort)(hexRecord[2] | (hexRecord[1] << 8));
                    //if ((StartAddress >= 0x1960) && (StartAddress < rom_endaddr + 1))
                    if (StartAddress < rom_endaddr + 1)
                    {
                        if ((StartAddress + ImageLen - 3) > rom_endaddr)
                        {
                            ImageLen = (ushort)(rom_endaddr - StartAddress + 1 + 3);
                        }

                        if (ImageLen - 3 <= 55) // we can have 64-4-1
                        {
                            if (OneLineRecord.Substring(4,4) == "1200")
                            {

                            }
                            else
                            {
                                codeSize += ImageLen;
                            }

                        }
                        else
                        {
                            sw.Close();
                            break;
                        }
                    }
                    else
                    {
                        sw.Close();
                        break;
                    }
                }
                else if (OneLineRecord.Substring(0, 2) == "S9")
                {
                    sw.Close();
                    //worker.ReportProgress(100,0);
                    //m_progress.Value = m_progress.Maximum;
                    break;
                }
            }
            return codeSize;
        }

        int codeSize = 0;
        private void UpdateFirm()
        {
          //  sendCMD_Recv MySendCmd = new sendCMD_Recv(this, null);
            // Initializes the variables to pass to the MessageBox.Show method.
            byte[] outputReportBuffer = new Byte[64 + 1];
            byte[] inputReportBuffer = new Byte[64 + 1];
            outputReportBuffer[0] = 0;
            String firmStatus;
            //int nread = 0;
            string message = "You are going to update gforce tracker firmware. Continue this operation? \r\n";
            message += "If yes please select file in next screen.";
            string caption = "Warning";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            //bool err = false;
            // Displays the MessageBox.

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                // Closes the parent form.
                ;
            }
            else
                return;

            //reboot 
            gftStat = (int)GFT_MHID_CMD.MHID_CMD_START_BOOT;
            if (gForceHid == null)//!MyHid.isDeviceAttached)
            {
                MessageBox.Show("Please reconnect USB cable and try again ");
                return;
            }
            // bool ret = true;

            if (gftWhereAmI == GFT_STATE_FIRM)
            {
                //this.BeginInvoke((MethodInvoker)delegate { mGetInfo_Click(sendCmd((byte)GFT_MHID_CMD.MHID_CMD_START_BOOT, 0), null); });
                if (sendCmd((byte)GFT_MHID_CMD.MHID_CMD_START_BOOT, 0) == null)
                {
                    MessageBox.Show("No Response", caption, buttons);
                    return;
                }
                toolStripStatusLabelGFT.Text = "Booting";
                toolStripStatusLabelGFT.BackColor = System.Drawing.Color.LightGreen;
                //this.Refresh();
            }
            else
            {

                ;//outputReportBuffer[1] = (byte)mhidCmd.MHID_CMD_REBOOT;
                //MyHid.OutputControl(outputReportBuffer);
            }

            //select firm file
            DialogResult fresult = openFileDialogFirm.ShowDialog();
            if (fresult != DialogResult.OK) // Test result.
            {
                MessageBox.Show("Please select a valid file!");
                return;
            }
            filenameFirm = openFileDialogFirm.FileName;


            codeSize = getFirmwareSize();
            //m_prog_progress.Value = 0;
            //m_prog_progress.Maximum = codeSize;
            //m_prog_progress.Visible = true;
            //erease flash

            //  Don't allow another transfer request until this one completes.
            //  Move the focus away from cmdOnce to prevent the focus from 
            //  switching to the next control in the tab order on disabling the button.
            outputReportBuffer[0] = 0;
            outputReportBuffer[1] = (byte)GFT_MHID_CMD.MHID_CMD_ERASE_FIRMWARE;
            //outputReportBuffer[1] = (byte)mhidCmd.MHID_CMD_GET_ACC_DATA_FIRST;
            outputReportBuffer[2] = 0x59; // 'Y'
            outputReportBuffer[3] = 0;
            toolStripStatusLabelGFT.Text = "Erasing Firmware";
            toolStripStatusLabelGFT.BackColor = System.Drawing.Color.LightGreen;
            this.Refresh();

            //MyHid.OutputControl(outputReportBuffer);
            /*err = gForceHid.WriteOutput(ref outputReportBuffer);*/
            //transferInProgress = true;
            Thread.Sleep(200);
            //MyHid.inputReports(ref inputReportBuffer, ref nread);
            //if(inputReport_Queue.Count>0)
            //    inputReportBuffer = inputReport_Queue.Dequeue();
            inputReportBuffer =  sendCmd(ref outputReportBuffer);

            if ((inputReportBuffer != null) && (inputReportBuffer[0] == (byte)((int)GFT_MHID_CMD.MHID_CMD_ERASE_FIRMWARE | MHID_CMD_RESPONSE_BIT))
                && (inputReportBuffer[1] == 0x1)
                && (inputReportBuffer[2] == 'S'))
            {

                toolStripStatusLabelGFT.Text = "Programming Firmware";
                toolStripStatusLabelGFT.BackColor = System.Drawing.Color.LightGreen;
                this.Refresh();
                //program
                //Thread.Sleep(2000);
                //thread_jobs = "FirmUpdate";
                //backgroundWorker.RunWorkerAsync();
                firmStatus = program();
                if (firmStatus == "finished")
                {
                    //firmStatus = "Reboot GFT";
                    toolStripStatusLabelGFT.Text = "Rebooting";
                    toolStripStatusLabelGFT.BackColor = System.Drawing.Color.Green;
                    //reboot
                    outputReportBuffer[1] = (byte)GFT_MHID_CMD.MHID_CMD_REBOOT;
                    //MyHid.OutputControl(outputReportBuffer);
                    gftWhereAmI = GFT_STATE_NONE;
                    try
                    {
                        //err = MyHid.OutputReports(outputReportBuffer);
                        gForceHid.WriteOutput(ref outputReportBuffer);
                    }
                    catch (Exception xx)
                    {
                        string sss = xx.Message;
                        //err = false;
                    }
                    //

                }
                else
                {
                    //firmStatus = "Program firmware failed, try again";
                    toolStripStatusLabelGFT.Text = "Firmware Program Failed - Try Again";
                    toolStripStatusLabelGFT.BackColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                //lblUpdateFirm.Text = "Erase firmware failed, try again";
                toolStripStatusLabelGFT.Text = "Firmware Erase Failed - Try Again";
                toolStripStatusLabelGFT.BackColor = System.Drawing.Color.Red;
            }
        }
#endif
    }


    public class sendCMD_Recv
    {
        Form1 mythis;
        private BuzzHandsetDevice gForceHid = null;
        //private byte[] outReportBuf;
        //private byte[] inReportBuf;
        //private SafePipeHandle safePipeHande;
        private AutoResetEvent oSignalEvent;
        Queue <byte[]> inputReport_Queue;


        public sendCMD_Recv(Form1 mThis, byte[] sendBuff)
        {
            mythis = mThis;
            gForceHid = mThis.gForceHid;
            inputReport_Queue = mThis.inputReport_Queue;
            //safePipeHande = mThis.pipeHandle;
            oSignalEvent = mThis.oSignalEvent;
            
        }

        public byte[] sendCmd(byte cmd, byte subCmd)
        {
            //string result = "failed";
            byte[] outputReportBuffer = new Byte[64 + 1];
            byte[] inputReportBuffer = new Byte[64 + 1];

            try
            {
                outputReportBuffer[0] = 0;// HID_REPORT_ID;
                outputReportBuffer[1] = cmd;
                outputReportBuffer[2] = subCmd;

                //transferSuccess = false;

                //if (!MyHid.OutputReports(outputReportBuffer))
                //    return "failed";

                try
                {
                    gForceHid.WriteOutput(ref outputReportBuffer);
                }
                catch (Exception xx)
                {
                    string ss = xx.Message;
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                    //return "failed";
                }
                //Thread.Sleep(10);

                //AnonymousPipeClientStream pipeClient = new AnonymousPipeClientStream(PipeDirection.In, safePipeHande);
                //PipeStream pipeClient = new AnonymousPipeClientStream(PipeDirection.In, pipeHandle);
                //using (StreamReader sr = new StreamReader(pipeClient))
                {

                  //  string temp;
                    //pipeClient.BeginRead(
                    //do
                   // {
                     //   temp = sr.ReadLine();
                   // }
                    //while (!temp.StartsWith("SYNC"));
                    //while (true)
                    {
                        if (oSignalEvent.WaitOne(5000))
                        {
                            if (inputReport_Queue.Count > 0)
                            {
                                inputReportBuffer = inputReport_Queue.Dequeue();
                                //if (!MyHid.inputReports(ref inputReportBuffer, ref nread))
                                //    return "failed";

                                if (inputReportBuffer[1] == (byte)(cmd | 0x80))
                                {
                                    //    break;
                                    //result = "passed";
                                }
                                else
                                {
                                    inputReportBuffer = null;
                                    //result = "failed";
                                }

                            }
                            else
                                inputReportBuffer = null;
                        }
                        else
                        {

                            inputReportBuffer = null;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string ss = ex.Message;
                //DisplayException("Send Command", ex);

            }
            if (oSignalEvent.WaitOne(5000))
            {
                if (inputReport_Queue.Count > 0)
                {
                    inputReportBuffer = inputReport_Queue.Dequeue();
                    if (inputReportBuffer[1] == (byte)(cmd | 0x80))
                    {
                    }
                    else
                    {
                        inputReportBuffer = null;
                    }
                }
            }
            else
            {
                inputReportBuffer = null;
            }
            return inputReportBuffer;

        }

    }
}

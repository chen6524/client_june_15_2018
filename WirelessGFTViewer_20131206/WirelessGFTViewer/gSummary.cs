﻿
using Microsoft.Win32.SafeHandles;
using Microsoft.VisualBasic;
using System.ComponentModel;
using ExcelLibrary.SpreadSheet;
using ExcelLibrary.CompoundDocumentFormat;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Collections;
using System.Threading;
using System;
using System.Timers;
using System.Globalization;
using ArtaFlexHidWin;

namespace gForceTrackerWireless
{
    public partial class Form1 : UsbAwareForm
    {

        void drawChartReportLocation(List<int> iList, Chart chart1, System.Drawing.Color mColor)
        {

            // remove all previous series
            chart1.Series.Clear();
            chart1.Annotations.Clear();
            var series = chart1.Series.Add("");

            series.Points.DataBindXY(new string[] { "Right", "Left", "Back", "Front", "Top", "Bottom" }, new int[] { iList[1], iList[2], iList[3], iList[4], iList[5], iList[6] });
            //chart1.Series.Add(series);
            series.Color = mColor;// Points[count].Color = System.Drawing.Color.Blue;
            chart1.ChartAreas[0].AxisX.LabelStyle.Angle = 45;

            for (int i = 1; i < 7; i++)
            {
                RectangleAnnotation myLine = new RectangleAnnotation();
                myLine.Text = iList[i].ToString();
                myLine.LineColor = System.Drawing.Color.Transparent;
                myLine.BackColor = System.Drawing.Color.Transparent;
                myLine.AnchorDataPoint = chart1.Series[0].Points[i - 1];
                chart1.Annotations.Add(myLine);
            }

        }
    }

#if false
    class gPerson
    {
        public bool IsActive = true;

        public gPerson(string name)
        {
            this.name = name;
        }

        public gPerson(string name, DateTime birthDate, double height, double weight, string photo, string comments)
        {
            this.name = name;
            this.birthDate = birthDate;
            this.height = height;
            this.weight = weight;
            this.Comments = comments;
            this.Photo = photo;
        }

        public System.Drawing.Image ImageAspect
        {
            get
            {
                return WirelessGFTViewer.Properties.Resources.FrontBottomGreen;
            }
        }
        // Allows tests for properties.
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string name;


        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }

        private DateTime birthDate;

        public double Weight
        {
            get { return this.weight; }
            set { this.weight = value; }
        }

        private double weight;

        public double Height
        {
            get { return this.height; }
            set { this.height = value; }
        }
        private double height;

        public List<gItemCat> getItemCat()
        {
            List<gItemCat> item = new List<gItemCat>();
            if (cat == null)
            {

                item.Add(new gItemCat("Total Session"));
                item.Add(new gItemCat("Total Impacts"));
                item.Add(new gItemCat("Total Whitin Threshold"));
                item.Add(new gItemCat("Total Alarm"));
                foreach (gItemCat gi in item)
                {
                    gi.Parent = this;
                }


            }

            return item;
        }

        // Allow tests for methods

        public List<gItemCat> Cat
        {
            get
            {
                if (this.cat == null)
                    this.cat = getItemCat();
                return this.cat;
            }
        }
        private List<gItemCat> cat;


        // Allows tests for fields.
        public string Photo;
        public string Comments;
        public int userId;

    }
#endif
    class gItemCat
    {
        public bool IsActive = true;

        public gItemCat(string name)
        {
            this.name = name;
        }
        public gItemCat(string name, int count)
        {
            this.name = name;
            this.count = count;
        }

        public System.Drawing.Image ImageAspect
        {
            get
            {
                return WirelessGFTViewer.Properties.Resources.FrontBottomGreen;
            }
        }

        // Allows tests for properties.
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string name;
        private int count;
        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        private object parent;
        public object Parent
        {
            get { return parent; }
            set { parent = value; }
        }
        public List<gImpact> GetImpactList(String userID, DateTime timeStart, DateTime timeEnd)
        {

            List<gImpact> gImpactList = new List<gImpact>();

            SqlCeDataReader myReader = null;
            string mid = "";
            // Open the connection using the connection string.
            using (SqlCeConnection con = new SqlCeConnection(Form1.conString))
            {
                con.Open();
                SqlCeCommand myCommand = new SqlCeCommand("SELECT  IMPACT_TIME, LINEAR_ACC_RESULTANT,ROTATIONAL_ACC_RESULTANT  FROM Events  ", con);
                myCommand.Parameters.AddWithValue("@myUID", mid);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    gImpact im = new gImpact("", (DateTime)myReader["IMPACT_TIME"],
                                            (double)myReader["LINEAR_ACC_RESULTANT"], (double)myReader["ROTATIONAL_ACC_RESULTANT"]);
                    gImpactList.Add(im);

                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
                return gImpactList;
            }
        }
        public List<gImpact> getChild()
        {
            List<gImpact> im;
            im = GetImpactList("", System.DateTime.UtcNow, System.DateTime.UtcNow);
            return im;
        }

    }

    class gImpact
    {


        public gImpact(string name)
        {
            this.name = name;
        }

        public gImpact(string name, DateTime time, double linRes, double rotRes)
        {
            this.name = time.ToShortDateString();

            this.time = time;
            this.linRes = linRes;
            this.rotRes = rotRes;

        }



        public System.Drawing.Image ImageAspect
        {
            get
            {
                return WirelessGFTViewer.Properties.Resources.FrontBottomGreen;
            }
        }
        // Allows tests for properties.
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string name;




        public DateTime BirthDate
        {
            get { return time; }
            set { time = value; }
        }

        private DateTime time;

        public double LinRes
        {
            get { return this.linRes; }
            set { this.linRes = value; }
        }

        private double linRes;

        public double RotRes
        {
            get { return this.rotRes; }
            set { this.rotRes = value; }
        }
        private double rotRes;


    }



}

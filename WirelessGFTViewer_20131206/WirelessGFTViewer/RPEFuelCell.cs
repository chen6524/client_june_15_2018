﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace WirelessGFTViewer
{
    public class RPEFuelCell : DataGridViewImageCell
    {
        // Used to make custom cell consistent with a DataGridViewImageCell
        static Image emptyImage;
        public List<gForceTrackerWireless.Form1.ACTIVITY_DATA> activityData;
        public List<gForceTrackerWireless.Form1.RPE_DATA> rpeData;
        public DateTime firstReceiveTime;
        public int activeTime;
        public int gftType = 0;
        public double rpeVal = 0;
        public double activeVal = 0;

        static RPEFuelCell()
        {
            emptyImage = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
        }

        public RPEFuelCell()
        {
            this.ValueType = typeof(int);
        }

        // Method required to make the Progress Cell consistent with the default Image Cell. 
        // The default Image Cell assumes an Image as a value, although the value of the Progress Cell is an int.
        protected override object GetFormattedValue(object value,
                            int rowIndex, ref DataGridViewCellStyle cellStyle,
                            TypeConverter valueTypeConverter,
                            TypeConverter formattedValueTypeConverter,
                            DataGridViewDataErrorContexts context)
        {
            return emptyImage;
        }

        protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle clipBounds, System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            switch (gftType)
            {
                case 0:
                    PaintActivity(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
                    break;
                case 1:
                    PaintRPE(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
                    break;
            }
        }
        protected void PaintActivity(System.Drawing.Graphics g, System.Drawing.Rectangle clipBounds, System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {

            Color activeColour;
            Color activeBorder;
            double activePct = (activeVal / 100);
            Color fontColour;
            if (activePct < 0.25)
            {
                activeColour = Color.LimeGreen;
                activeBorder = Color.FromArgb(235, 35, 175, 35);
                fontColour = Color.White;
            }
            else if (activePct < 0.6)
            {
                activeColour = Color.Yellow;
                activeBorder = Color.FromArgb(235, 215, 225, 0);
                fontColour = Color.Black;
            }
            else
            {
                activeColour = Color.Red;
                activeBorder = Color.FromArgb(215, 215, 0, 0);
                fontColour = Color.White;
            }
            int yTop = cellBounds.Y + (cellBounds.Height - 16) / 2;
            Brush backColorBrush = new SolidBrush(cellStyle.BackColor);
            Brush foreColorBrush = new SolidBrush(fontColour);

            // Draws the cell grid
            base.Paint(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, (paintParts & ~DataGridViewPaintParts.ContentForeground));
            /*
            g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X, yTop-2, cellBounds.Width, 36);
            g.FillRectangle(new SolidBrush(activeColour), cellBounds.X + 2, yTop, cellBounds.Width - 4, 32);
            g.DrawString((activePct * 100).ToString() + "%", new Font(cellStyle.Font.FontFamily, 8, FontStyle.Bold), foreColorBrush, cellBounds.X + cellBounds.Size.Width / 2 - 11, cellBounds.Y + cellBounds.Size.Height / 2 - 15);
            */
            //yTop += 16;
            g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X + 2, yTop - 2, cellBounds.Width - 4, 20);

            if (activityData != null)
            {


                int activeSeconds = (int)Math.Round((double)activeTime / 3000, 0);
                TimeSpan sesLength = DateTime.UtcNow - firstReceiveTime;
                int totalSeconds = (int)sesLength.TotalSeconds;

                gForceTrackerWireless.Form1.ACTIVITY_DATA lData = new gForceTrackerWireless.Form1.ACTIVITY_DATA();
                lData.activeTime = 0;
                lData.reportTime = firstReceiveTime;

                //using (Graphics g = Graphics.FromImage(pbHeatMap.Image))
                //{
                /*    SolidBrush myBrush = new SolidBrush(Color.White);
                    g.FillRectangle(myBrush, 0, 0, cellBounds.Width, cellBounds.Height);
                    Pen pen = new Pen(Color.Black);
                    g.DrawLine(pen, cellBounds.X + 2, cellBounds.Y+2, cellBounds.Width - 2, 2);
                    g.DrawLine(pen, cellBounds.X + 2, cellBounds.Y - cellBounds.Height - 3, cellBounds.Width - 2, cellBounds.Height - 3);
                    g.DrawLine(pen, cellBounds.X + 2, cellBounds.Y+2, 2, cellBounds.Height - 3);
                    g.DrawLine(pen, cellBounds.X + cellBounds.Width - 2, cellBounds.Y + 2, cellBounds.Width - 2, cellBounds.Height - 3);*/

                TimeSpan t = TimeSpan.FromSeconds(0);
                //int xPos = 0;
                int inc = (int)(totalSeconds / 60) * 60 / 5;
                if (inc < 60) inc = 60;
                /*SolidBrush myBrushB = new SolidBrush(Color.Black);
                for (int i = 0; i <= totalSeconds; i += inc)
                {
                    xPos = 24 + (int)Math.Round(((double)i / totalSeconds * (cellBounds.Width - 48)));
                    g.DrawLine(pen, xPos, cellBounds.Height - 24, xPos, cellBounds.Height - 20);
                    t = TimeSpan.FromSeconds(i);
                    g.DrawString(string.Format("{0:D2}:{1:D2}", t.Hours, t.Minutes), lblRotationAveMed.Font, myBrushB, new Point(xPos - 15, cellBounds.Height - 18));
                    g.DrawString("Fuel Tank", this.label4.Font, myBrushB, new Point(4, 2));

                    g.FillRectangle(myBrushB, 120, 6, 16, 12);
                    g.FillRectangle(myBrushB, 200, 6, 16, 12);
                    g.FillRectangle(myBrushB, 280, 6, 16, 12);

                    g.FillRectangle(new SolidBrush(Color.LimeGreen), 121, 7, 14, 10);
                    g.FillRectangle(new SolidBrush(Color.Yellow), 201, 7, 14, 10);
                    g.FillRectangle(new SolidBrush(Color.Red), 281, 7, 14, 10);

                    g.DrawString("Rest", this.lblRotationAveMed.Font, myBrushB, new Point(135, 6));
                    g.DrawString("Moderate", this.lblRotationAveMed.Font, myBrushB, new Point(215, 6));
                    g.DrawString("Active", this.lblRotationAveMed.Font, myBrushB, new Point(295, 6));
                }*/
                // }

                //ACTIVITY_DATA rData = lpHub.lpDevices[currentNodeID - 1].activeTimeList.Last(); // Most recent activity Data
                if (activityData.Count > 0)
                {
                    gForceTrackerWireless.Form1.ACTIVITY_DATA rData = activityData.Last(); // Most recent activity Data
                    TimeSpan activelength = rData.reportTime - lData.reportTime;
                    int totalTime = (int)activelength.TotalSeconds;

                    System.Drawing.Drawing2D.LinearGradientBrush myGradient;

                    int deltaA;
                    TimeSpan deltaT;
                    //double activePct;
                    Color lastColour = Color.LimeGreen;
                    //Color activeColour = Color.LimeGreen;

                    //g.FillRectangle(new SolidBrush(activeColour), cellBounds.X + 2, yTop, cellBounds.Width - 4, 22);
                    int xPos = cellBounds.X + 4;
                    int xPosTo = cellBounds.X + 4;

                    //Pen pen;
                    //using (Graphics g = Graphics.FromImage(pbHeatMap.Image))
                    //{
                    foreach (gForceTrackerWireless.Form1.ACTIVITY_DATA aData in activityData)
                    {
                        // Determine the colour.. 0-25% Activity = Rest, 25-60% = Moderate, 60%+ = High Activity
                        // What's our active time delta?
                        deltaA = aData.activeTime - lData.activeTime;
                        deltaT = aData.reportTime - lData.reportTime;
                        xPosTo = xPos + (int)Math.Round((deltaT.TotalSeconds / activelength.TotalSeconds) * (cellBounds.Width - 8));
                        if (xPosTo > (cellBounds.X + cellBounds.Width - 3)) xPosTo = (cellBounds.X + cellBounds.Width - 4);
                        activePct = (deltaA / 3000) / deltaT.TotalSeconds;
                        if (activePct < 0.25)
                        {
                            activeColour = Color.LimeGreen;
                        }
                        else if (activePct < 0.6)
                        {
                            activeColour = Color.Yellow;
                        }
                        else
                        {
                            activeColour = Color.Red;
                        }
                        if (xPosTo > xPos)
                        {
                            myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                new Point(xPos - 1, (yTop)),
                                new Point(xPosTo, (yTop)),
                                lastColour, activeColour
                                );
                            //pen = new Pen(myGradient);
                            g.FillRectangle(myGradient, xPos, yTop, xPosTo - xPos, 16);
                            lData = aData;
                            lastColour = activeColour;
                            xPos = xPosTo;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    if (xPosTo < cellBounds.X + cellBounds.Width - 4)
                    {
                        myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                   new Point(xPos - 1, (yTop)),
                                   new Point(cellBounds.X + cellBounds.Width - 2, (yTop)),
                                   lastColour, activeColour
                                   );
                        g.FillRectangle(myGradient, xPos, yTop, (cellBounds.X + cellBounds.Width - xPos) - 4, 16);
                    }
                    //pbHeatMap.Refresh();

                    //}
                }
            }
        }
        protected void PaintRPE(System.Drawing.Graphics g, System.Drawing.Rectangle clipBounds, System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            
            Color activeColour;
            Color activeBorder;
            Color fontColour;
            if (rpeVal <= 2)
            {
                activeColour = Color.LimeGreen;
                activeBorder = Color.FromArgb(235, 35, 175, 35);
                fontColour = Color.White;
            }
            else if (rpeVal <= 3.5)
            {
                activeColour = Color.Yellow;
                activeBorder = Color.FromArgb(235, 215, 225, 0);
                fontColour = Color.Black;
            }
            else
            {
                activeColour = Color.Red;
                activeBorder = Color.FromArgb(215, 215, 0, 0);
                fontColour = Color.White;
            }
            int yTop = cellBounds.Y + (cellBounds.Height - 16) / 2;
            Brush backColorBrush = new SolidBrush(cellStyle.BackColor);
            Brush foreColorBrush = new SolidBrush(fontColour);
            
              // Draws the cell grid
            base.Paint(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, (paintParts & ~DataGridViewPaintParts.ContentForeground));

            g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X + 2, yTop - 2, cellBounds.Width - 4, 20);

            if(rpeData != null) {


                TimeSpan sesLength = DateTime.UtcNow - firstReceiveTime;
                int totalSeconds = (int)sesLength.TotalSeconds;

                gForceTrackerWireless.Form1.RPE_DATA lData = new gForceTrackerWireless.Form1.RPE_DATA();
                lData.rpe = 0;
                lData.report_time = firstReceiveTime;

                TimeSpan t = TimeSpan.FromSeconds(0);
                //int xPos = 0;
                int inc = (int)(totalSeconds / 60) * 60 / 5;
                if (inc < 60) inc = 60;


                if (rpeData.Count > 0)
            {
                if (rpeData.Last().report_time < DateTime.UtcNow.AddSeconds(-15))
                {
                    // If most recent report is more than 15 seconds old, we ned a new one to render our display accurately..
                    gForceTrackerWireless.Form1.RPE_DATA nData = rpeData.Last();
                    nData.report_time = DateTime.UtcNow;
                    nData.explosive_count = 0;
                    nData.player_load = 0;
                    nData.explosive_power = 0;
                    rpeData.Add(nData);
                }

                gForceTrackerWireless.Form1.RPE_DATA rData = rpeData.Last(); // Most recent activity Data
                TimeSpan activelength = rData.report_time - lData.report_time;
                int totalTime = (int)activelength.TotalSeconds;

                System.Drawing.Drawing2D.LinearGradientBrush myGradient;

                TimeSpan deltaT;
                Color lastColour = Color.LimeGreen;

                int xPos = cellBounds.X + 4;
                int xPosTo = cellBounds.X + 4;

                    foreach (gForceTrackerWireless.Form1.RPE_DATA aData in rpeData)
                    {
                        // What's our active time delta?
                        deltaT = aData.report_time - lData.report_time;
                        xPosTo = xPos + (int)Math.Round((deltaT.TotalSeconds / activelength.TotalSeconds) * (cellBounds.Width - 8));
                        if (xPosTo > (cellBounds.X + cellBounds.Width - 3)) xPosTo = (cellBounds.X + cellBounds.Width - 4);

                        if (aData.player_load <= 21)
                        {
                            activeColour = Color.LimeGreen;
                        }
                        else if (aData.player_load <= 35)
                        {
                            activeColour = Color.Yellow;
                        }
                        else
                        {
                            activeColour = Color.Red;
                        }
                        if (xPosTo > xPos)
                        {
                            myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                new Point(xPos - 1, (yTop)),
                                new Point(xPosTo, (yTop)),
                                lastColour, activeColour
                                );
                            //pen = new Pen(myGradient);
                            g.FillRectangle(myGradient, xPos, yTop, xPosTo - xPos, 16);
                            lData = aData;
                            lastColour = activeColour;
                            xPos = xPosTo;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    if (xPosTo < cellBounds.X + cellBounds.Width - 4)
                    {
                        myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                   new Point(xPos - 1, (yTop)),
                                   new Point(cellBounds.X + cellBounds.Width - 2, (yTop)),
                                   lastColour, activeColour
                                   );
                        g.FillRectangle(myGradient, xPos, yTop, (cellBounds.X + cellBounds.Width - xPos) - 4, 16);
                    }
                }
            }
        }
    }
}

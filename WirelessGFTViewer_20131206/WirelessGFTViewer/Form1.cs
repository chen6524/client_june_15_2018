﻿//using Microsoft.Win32.SafeHandles;
//using Microsoft.VisualBasic;
//using ExcelLibrary.SpreadSheet;
//using ExcelLibrary.CompoundDocumentFormat;
//using System.Drawing.Drawing2D;
//using System.Windows.Forms.DataVisualization.Charting;
//using System.Text.RegularExpressions;
//using System.IO;
//using System.Collections;
//using System.Drawing.Imaging;
//using System.Timers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data.SqlServerCe;
using System.Runtime.InteropServices;
using System.Threading;
using System.Globalization;
using ArtaFlexHidWin;
using WirelessGFTViewer.Properties;

using System.IO;                                                    // Added by Jason Chen, 2017.03.31
using System.Reflection;                                            // Added by Jason Chen, 2017.03.31 

using BLE_Interface;

namespace gForceTrackerWireless
{
    //[System.Diagnostics.DebuggerDisplay("Form1")]
    public partial class Form1 : UsbAwareForm
    {
        DateTime buildDate = new FileInfo(Assembly.GetExecutingAssembly().Location).LastWriteTime;                // Added by Jason Chen, 2017.03.31
        #region Wireless_Viewer_Declaration

        public delegate void DeviceList_Refresh(int nodeID);
        public delegate void DeviceList_Refresh_All();
        public bool WirelessViewerOpen = true;

        public const int WM_QUERYENDSESSION = 0x11;
        public bool systemShutdown = false;
#if false
        private BackgroundWorker bwPacketStatus = new BackgroundWorker();
        private BackgroundWorker bwDeviceList = new BackgroundWorker();

        public string PacketStatusAction;
        public string PacketStatusTextToDisplay;
        public string PacketStatusColor;
#endif

        private BuzzHandsetDevice GftWirelessHidHub = null;
        public BuzzHandsetDevice gForceHid = null;
        //public SafePipeHandle pipeHandle;
        //private BuzzHandsetDevice GftWirelessHidHub_temp = null;
        //uploadDataForWirelessProcess processRecvbuff;// = new uploadDataForWirelessProcess(this);
        //List<byte[]> recvQueue = new List<byte[]>();
        bool bind_with_unbind = false;
        bool bind_with_wireless_on = false;                                         // 2014.10.31
        private string last_usb_gid = "123456789ABC";
        private string dongle_mid = string.Empty;                                   // 2014.11.11
        bool new_dongle_plugged = false;                                            // 2014.11.11

        private int EventID = -1;


        private BLE_Interface_Form mBLEInterface;// = new BLE_Interface_Form();
        public AutoResetEvent oSignalEventBle;// = new AutoResetEvent(false);

        int m_OutstandingFrames = 0;
        //bool m_Stop;

        List<String> bleAddr_s;// = new List<String>();
        List<String> bleName_s;// = new List<String>();

        bool USB_OR_BLE = false;
        

        public int getUniqueEventID()
        {
            if (EventID == -1)
            {

                try
                {
                    using (SqlCeConnection mycon = new SqlCeConnection(conString))
                    {
                        mycon.Open();
                        SqlCeCommand cmd = new SqlCeCommand("SELECT COALESCE(MAX(EID),0) as EID FROM EventsWireless", mycon);

                        try
                        {
                            SqlCeDataReader rdr = cmd.ExecuteReader();
                            if (rdr.Read())
                            {
                                EventID = (Int32)rdr["EID"];
                            }
                        }
                        catch (Exception me)
                        {
                            throw me;
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
            return ++EventID;
        }

        public enum GFT_MHID_CMD
        {
            MHID_CMD_GET_ACC_ENTRY,           //0
            MHID_CMD_GET_ACC_ENTRY_CNT,       //1//
            MHID_CMD_GET_ACC_ENTRY_NEXT,      //
            MHID_CMD_GET_ACC_ENTRY_FIRST,
            MHID_CMD_GET_ACC_DATA_MAX,
            MHID_CMD_GET_ACC_DATA_NEXT,
            MHID_CMD_GET_ACC_DATA_FIRST,
            MHID_CMD_GET_ACC_DATA_LAST,
            MHID_CMD_SET_USR_PROFILE,
            MHID_CMD_GET_USR_PROFILE,         //0x9
            MHID_CMD_ERASE_ACC_DATA,          //0xa
            MHID_CMD_ERASE_USR_PROFILE,       //0xb
            MHID_CMD_SET_TIME,                //0xc
            MHID_CMD_START_BOOT,              //0xd
            MHID_CMD_REBOOT,                  //Bootldr & firmwre 0xe
            MHID_CMD_ERASE_FIRMWARE,          //Only used by bootloader  0xf
            MHID_CMD_PROG_FIRM,               //Only used by bootloader 0x10
            MHID_CMD_GET_BATTERY_STAT,	      //abandonded 
            MHID_CMD_GET_ACC_ENTRY_DATA_ALL,  //
            MHID_CMD_GET_ACC_ENTRY_DATA_STOP,
            MHID_CMD_TEST,                    //0x14
            MHID_CMD_CHECKBAT,                //0x15
            MHID_CMD_DEBUG,
            MHID_CMD_GET_GID,                 //0x17
            MHID_CMD_GET_DEV_INFO,            //0x18
            MHID_CMD_GET_TIME,                //0x19
            MHID_CMD_SET_SN,                  //0x20
            MHID_CMD_GET_SN,                  //0x21
            MHID_CMD_RUN_FIRM,                //0x22
            MHID_CMD_LP_UNBIND,               //0x23
            MHID_CMD_CAL_START,               //0x24
            MHID_CMD_CAL_GET                  //0x25
        };


        const byte MSG_HOST_TO_HUB_GET_INFO    = 0x51;
        const byte MSG_HOST_TO_HUB_BIND_MODE   = 0x52;
        const byte MSG_HOST_TO_HUB_NODE_INFO   = 0x53;
        const byte MSG_HOST_TO_HUB_UPLOAD      = 0x54;
        const byte MSG_HOST_TO_HUB_RESET       = 0x55;
        const byte MSG_HOST_TO_HUB_SYNC        = 0x56;
        const byte MSG_HOST_TO_HUB_CLONE       = 0x57;
        const byte MSG_HOST_TO_HUB_UNBIND_MODE = 0x58;
        const byte MSG_HOST_TO_HUB_BCD_CMD     = 0x59;
        const byte MSG_HOST_TO_HUB_NODE_BLE_INFO = 0x5B;

        const byte MSG_HUB_TO_HOST_MODE_SHELF  = 0x0E;
        const byte MSG_HUB_TO_HOST_SHOW_SHELF  = 0x0F;
        const byte MSG_HUB_TO_HOST_NODE_DATA   = 0x10;

        const byte MSG_HUB_TO_HOST_GET_INFO    = 0x11;
        const byte MSG_HUB_TO_HOST_BIND_MODE   = 0x12;
        const byte MSG_HUB_TO_HOST_NODE_INFO   = 0x13;
        const byte MSG_HUB_TO_HOST_UPLOAD      = 0x14;
        const byte MSG_HUB_TO_HOST_RESET       = 0x15;
        const byte MSG_HUB_TO_HOST_SYNC        = 0x16;
        const byte MSG_HUB_TO_HOST_CLONE       = 0x17;
        const byte MSG_HUB_TO_HOST_UNBIND_MODE = 0x18;
        const byte MSG_HUB_TO_HOST_BCD_CMD     = 0x19;
        const byte MSG_HUB_TO_HOST_NODE_BLE_INFO   = 0x1B;

        //message types
        const byte CHUNK_BINDING               = 0;      // no packet transmission
        const byte CHUNK_SHELF_MODE_U          = 1;      // packet size will be  2
        const byte CHUNK_SHELF_MODE_D          = 4;      // packet size will be  5
        const byte CHUNK_POWER_OFF             = 10;     // packet size will be 11
        const byte CHUNK_POWER_ON              = 11;     // packet size will be 12
        const byte CHUNK_IMMEDIATE             = 12;     // packet size will be 13
        const byte CHUNK_HIT_DATA              = 13;     // packet size will be 14

        const byte NUM_NODES = 103;//55;
        const byte MAX_DEVICE_CNT = 103;//63;
        const int IMPACT_DATA_LEN = 573;  //09.11
        const int PERF_DATA_LEN = 291;
        const int ACC_START_DATA_LEN = 39;
        //int error_count = 0;              // 09.11, 10.31

        
        public const double BrIC_CX = 3795.85;
        public const double BrIC_CY = 3234.35;
        public const double BrIC_CZ = 2456.27;


        int gft_node = 0;

        int NUM_UPLOAD_LIMIT = 5;
        int NUM_TIME_SYNC_LIMIT = 2;                                        // Modified from 5 to 2 by jason chen, 2017.03.24, because it infects "Power On" action wirelessly
        bool initiating_time_sync = false;
        bool uploadFlag = false;
        int uploadNum_limit = 0;
        int timeSync_limit = 0;
        //int display_count = 0;

        protected uint m_dwTotalRxPacket;

        protected byte[] m_bSeqNo = new byte[1 + NUM_NODES];
        protected byte m_bSeqErr;

        //int now = 0;//256 - (0x7E - 0x20 + 1);
        //int then = 0;
        //int dif = 0, oldif = 0;

        
        public const int WM_USER = 0x0400;
        public const int WM_TIMER = 0x0113;

        int[] m_wHitCount = new int[NUM_NODES];
        int[] m_dwUploadCount = new int[NUM_NODES];
        byte[] m_bHitSeqNo = new byte[NUM_NODES];

        bool List_or_Table = false;
        bool refreshForList;                                       // 2014.10.30                                        // 2014.10.30
        bool gftGMT_Enable;                                        // 2014.10.30    
        //string[] m_cName = new string[NUM_NODES];

        //bool batFlag = false;

        char[] DELIM_CRLF = new char[] { '\r', '\n' };
        char[] DELIM_OK = new char[] { 'O', 'K' };
        char[] DELIM_OK_CRLF = new char[] { 'O', 'K', '\r', '\n' };
        char[] DELIM_ok_CRLF = new char[] { 'o', 'k', '\r', '\n' };
        char[] DELIM_SPACE = new char[] { ' ' };
        char[] DELIM_ZERO = new char[] { '\0' };
        char[] DELIM_SEMICOMMA = new char[] { ';' };
        char[] DELIM_NOASCII = new char[256 - (0x7E - 0x20 + 1)];
        char[] NONAME = new char[] { ' ' };



        private System.ComponentModel.BackgroundWorker bwDeviceMonitor;

#if ENABLE_DASHBOARD
        public WirelessGFTViewer.PlayerReview wirelessViewer;
#else
        WirelessGFTViewer.WirelessViewer wirelessViewer;
#endif
        //List<byte[]> recvBufferList = new List<byte[]>();
        //Queue<byte[]> recvBufferList = new Queue<byte[]>();
        //SqlCeConnection mycon = new SqlCeConnection(conString);
        //const byte MAX_DEVICE_CNT = 103;//63;

        public enum gLpMsgType
        {
            CHUNK_BINDING = 0,      // no packet transmission
            CHUNK_SHELF_MODE_U = 2,      // packet size will be  2
            CHUNK_SHELF_MODE_D = 5,      // packet size will be  5
            CHUNK_POWER_OFF = 11,     // packet size will be 11
            CHUNK_POWER_ON,    //12,     // packet size will be 12
            CHUNK_IMMEDIATE,   //13,     // packet size will be 13
            CHUNK_HIT_DATA,    //14      // packet size will be 14
            CHUNK_UPLOAD_MORE, //15
            CHUNK_UPLOAD_LAST
        };
        /*
        internal class LP_DATA_PKT
        {
            internal byte   nodeID;
            internal byte   type;
            internal byte   seq;
            internal byte   len;
            internal bool   isLast;
            internal byte[] load = new byte[18];
        }
        */

        public List<IMPACT_DATA> teamImpacts = new List<IMPACT_DATA>();
        private List<IMPACT_DATA> timeline;
        private List<IMPACT_DATA> teamTimeline;

        // Team Tracking data
        public int withinThreshold = 0;
        public int aboveThreshold = 0;
        public double highestG = 0;
        public double highestR = 0;
        public double averageG = 0;
        public int averageR = 0;
        public double medianG = 0;
        public int medianR = 0;
        public double exposure = 0;
        public int totalImpacts = 0;

        public double highestPL = 0;
        public double highestEP = 0;
        public double highestRPE = 0;

        public double averagePL = 0;
        public double averageEP = 0;
        public double averageRPE = 0;

        public double medianPL = 0;
        public double medianEP = 0;
        public double medianRPE = 0;


        public int highestRImpactID = 0;
        public int highestGImpactID = 0;
        public int highestGUID = 0;
        public int highestRUID = 0;
        
        public int highestRPEUID = 0;
        public int highestPLUID = 0;
        public int highestEPUID = 0;


        public WirelessGFTViewer.RxLog RxLog;

        public WirelessGFTViewer.NodeOnSelectedArgs lastNodeSelected;
        public class SyncList<T> : System.ComponentModel.BindingList<T>
        {
            private System.ComponentModel.ISynchronizeInvoke _SyncObject;
            private System.Action<System.ComponentModel.ListChangedEventArgs> _FireEventAction;

            public SyncList()
                : this(null)
            {
            }

            public SyncList(System.ComponentModel.ISynchronizeInvoke syncObject)
            {
                _SyncObject = syncObject;
                _FireEventAction = FireEvent;
            }

            protected override void OnListChanged(System.ComponentModel.ListChangedEventArgs args)
            {
                if (_SyncObject == null)
                {
                    FireEvent(args);
                }
                else
                {
                    _SyncObject.Invoke(_FireEventAction, new object[] { args });
                }
            }

            private void FireEvent(System.ComponentModel.ListChangedEventArgs args)
            {
                base.OnListChanged(args);
            }
        }

        #endregion

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct GFT_PROFILE
        {
            internal byte magic;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            internal byte[] GID;
            internal byte tAlam;
            internal byte tR;
            internal byte AlarmMode;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            internal byte[] name;
            internal int sports;
            internal int level;
            internal int countDay;
            internal int countWeek;
            internal int countMonth;
            internal int countYear;
        };

        internal struct USER_PROFILE
        {
            internal byte magic;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            internal byte[] GID;// = new byte[6];
            internal byte tAlarm;
            internal byte tR;
            internal byte AlarmMode;
            internal byte lpEnable;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            internal byte[] name;// = new byte[20];      // size 20
            internal byte playerNo;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            internal byte[] loc;// = new byte[9];       // size 9
            internal byte pwrMode;
#if true//JASON_PROXIMITY_ENABLE
            internal byte proxEnable;                                 // Added by Jason Chen, 2014.05.20
#endif
            internal byte realXmitEnable;                             // Added by Jason Chen, 2014.03.19    
        }

        internal struct GFT_USR
        {
            internal Int32 uid;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
            internal char[] FNAME;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
            internal char[] LNAME;
            internal DateTime BDAY;
            internal double WEIGHT;
            internal double HEIGHT;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 60)]
            internal char[] email;
            internal int country;
            internal bool male;
        };

        internal struct GFT_DEVICE
        {
            internal byte hwMajor;
            internal byte hwMinor;
            internal byte bootMajor;
            internal byte bootMinor;
            internal byte firmMajor;
            internal byte firmMinor;
        };

        int[] sn = { 0, 0, 0, 0, 0, 0 };                // sn[0] oem number 
        static GFT_PROFILE gftProfile;                 // gft settings
        static GFT_USR curUser;
        static USER_PROFILE userProfile;// = new USER_PROFILE();

        static GFT_DEVICE curGFT;                     // gft info
        //internal const byte GFT_STATE_BOOT = 1;
        //internal const byte GFT_STATE_FIRM = 2;
        //static byte gftWhereAmI = 0;         // 1: bootldr 2:firm
        string GID = "00000000";
        Int32 UID = 0;
        //byte   gftMajor = 0;
        //byte   gftMinor = 0;
        bool isNewUser = false;
        //bool gftPassBoot = true;


        //public Queue<byte[]> HIDinBuffer = new Queue<byte[]>();

        public Form1()
        {
            InitializeComponent();
            this.Text = "Sideline Viewer - " + Application.ProductVersion;
            this.splitContainer1.Panel2Collapsed = true;
            #region Wireless_Viewer_Process

            for (int i = 0; i < (1 + NUM_NODES); i++) m_bSeqNo[i] = 0xFF;
            for (int node = 0; node < NUM_NODES; node++)
            {
                m_wHitCount[node] = 0;
                m_dwUploadCount[node] = 0;
                m_bHitSeqNo[node] = 0;
                //m_cName[node] = string.Format("N-{0:d}", node + 1);
            }

            for (int i = 0; i < 161; i++)
            {
                if (i < 32)
                    DELIM_NOASCII[i] = (char)i;
                else
                    DELIM_NOASCII[i] = (char)(i + 95);
            }

            m_bSeqErr = 0;
            //panelLogin.Parent = tabControl4.Parent;
            //panelLogin.Dock = DockStyle.Fill;
            //panelLogin.Visible = true;

            #endregion
#if ENABLE_DASHBOARD
            this.teamChart.Series.Clear();
            this.splitContainer2.Panel2Collapsed = true;
            this.splitContainer3.Panel2Collapsed = true;
            this.wirelessViewer = this.playerReviewer;
#else
            this.splitContainer2.Panel1Collapsed = true;
            this.wirelessViewer = this.wirelessViewerCtl;
#endif

            this.wirelessViewer.onMnuRemoveFromHubDG += new WirelessGFTViewer.RemoveFromHubDataGridHandler(this.wirelessViewer_OnMnuRemoveFromHub);
            this.wirelessViewer.OnMnuRemoveFromHub += new WirelessGFTViewer.RemoveFromHubHandler(this.wirelessViewer.removeFromHub_Click);

            this.wirelessViewer.OnMnuGetImpactsDG += new WirelessGFTViewer.GetImpactsDataGridHandler(this.wirelessViewer_OnMnuGetImpacts);
            this.wirelessViewer.OnMnuGetImpacts += new WirelessGFTViewer.GetImpactsHandler(this.wirelessViewer.getImpacts_Click);
#if ENABLE_DASHBOARD
            this.wirelessViewer.OnMnuDownloadDG += new WirelessGFTViewer.DownloadDataGridHandler(this.wirelessViewer_OnMnuDownload);
            this.wirelessViewer.OnMnuDownload += new WirelessGFTViewer.DownloadHandler(this.wirelessViewer.Download_Click);
#endif
            this.wirelessViewer.OnMnuPowerOffDG += new WirelessGFTViewer.PowerOffOneDataGridHandler(this.wirelessViewer_OnMnuPowerOff);
            this.wirelessViewer.OnMnuPowerOff += new WirelessGFTViewer.PowerOffOneHandler(this.wirelessViewer.powerOffOne_Click);

            this.wirelessViewer.OnMnuPowerOnDG += new WirelessGFTViewer.PowerOnOneDataGridHandler(this.wirelessViewer_OnMnuPowerOn);
            this.wirelessViewer.OnMnuPowerOn += new WirelessGFTViewer.PowerOnOneHandler(this.wirelessViewer.powerOnOne_Click);

            this.wirelessViewer.OnMnuStopRecordingDG += new WirelessGFTViewer.StopRecordingDataGridHandler(this.wirelessViewer_OnMnuStopRecording);
            this.wirelessViewer.OnMnuStopRecording += new WirelessGFTViewer.StopRecordingHandler(this.wirelessViewer.stopRecording_Click);

            this.wirelessViewer.OnMnuStartRecordingDG += new WirelessGFTViewer.StartRecordingDataGridHandler(this.wirelessViewer_OnMnuStartRecording);
            this.wirelessViewer.OnMnuStartRecording += new WirelessGFTViewer.StartRecordingHandler(this.wirelessViewer.startRecording_Click);

            dataInit();

            //this.ActiveControl = maskedTextBoxLoginPW;
            uiContext = SynchronizationContext.Current;

#if false
            bwPacketStatus.WorkerReportsProgress = true;
            bwPacketStatus.WorkerSupportsCancellation = true;
            bwPacketStatus.DoWork += new DoWorkEventHandler(bwPacketStatus_DoWork);
            bwPacketStatus.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwPacketStatus_RunWorkerCompleted);
            if (!bwPacketStatus.IsBusy) bwPacketStatus.RunWorkerAsync();

            bwDeviceList.WorkerReportsProgress = true;
            bwDeviceList.WorkerSupportsCancellation = true;
            bwDeviceList.DoWork += new DoWorkEventHandler(bwDeviceList_DoWork);
            bwDeviceList.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwDeviceList_RunWorkerCompleted);
            if (!bwDeviceList.IsBusy) bwDeviceList.RunWorkerAsync();
#endif

            this.bwDeviceMonitor = new System.ComponentModel.BackgroundWorker();
            this.bwDeviceMonitor.DoWork += new System.ComponentModel.DoWorkEventHandler(bwDeviceMonitor_DoWork);
            this.bwDeviceMonitor.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(bwDeviceMonitor_RunWorkerCompleted);
            bwDeviceMonitor.RunWorkerAsync();

            gftMsgEvent += new gftMessageHandler(raiseGftMsgEvent);
            
            // Remove any unfinished Downloads
            gDataWSessionRemoveIncomplete();
        }

        private void Form1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if(Control.ModifierKeys == Keys.Control) {
                if(e.KeyCode == Keys.R) {
                    if (this.RxLog == null)                                                                // commented by Jason chen, 2017.03.22
                    {
                        this.RxLog = new WirelessGFTViewer.RxLog();                                        
                        this.RxLog.Show();
                    }
                    else
                    {
                        if (this.RxLog.IsDisposed)
                            this.RxLog = new WirelessGFTViewer.RxLog();                                    // commented by Jason chen, 2017.03.22
                        this.RxLog.Show();                                                                 // commented by Jason chen, 2017.03.22
                    }
                }
            }
        }

        private void bwDeviceMonitor_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Thread.Sleep(60000);
            // Check for devices which aren't responding
            if (this.lpHub != null)
            {
                foreach (WirelessGFTViewer.LP_DEVICE dev in this.lpHub.lpDevices)
                {
                    if (dev.valid == true)
                    {
                        if (dev.mode >= 1 && dev.lastPingTime < DateTime.UtcNow.AddMinutes(-3))
                        {
                            this.BeginInvoke(new DeviceList_Refresh(this.lpHub.dgvLpPopulateView), dev.nodeID); //this.lpHub.updateDGView(dev.nodeID);//
                        }
                    }
                }
            }

        }

        private void bwDeviceMonitor_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && !bwDeviceMonitor.IsBusy) bwDeviceMonitor.RunWorkerAsync();
        }
        private void raiseGftMsgEvent(object sender, gftMsgArgs e)
        {
            switch (e.getMsg)
            {
                case wirelessGftMsg.MSG_DOWNLOAD_INTERRUPTED:
                    //MessageBox.Show(e.getString);
                    threadedPopup(e.getString);
                    break;
            }
        }
        
        public int gftBattery(int mode, int acd0, int acd1, int fuel0, int fuel1, int pct) {
            int value = 0;
            switch (mode)
            {
                case 0:
                    value = acd0 * 256 + acd1;
                    break;
                case 1:
                    value = fuel0 * 256 + fuel1;
                    break;
                case 2:
                    value = pct;
                    break;
            }

            return gftBattery(mode, value);
        }
        public int gftBattery(int mode, int value)
        {
            int percent = 0;
            switch(mode) {
                case 0:
                    if (value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_100PECENT)
                        percent = 100;
                    else if ((value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_90PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_100PECENT))
                        percent = 90;
                    else if ((value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_80PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_90PECENT))
                        percent = 80;
                    else if ((value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_70PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_80PECENT))
                        percent = 70;
                    else if ((value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_60PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_70PECENT))
                        percent = 60;
                    else if ((value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_50PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_60PECENT))
                        percent = 50;
                    else if ((value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_40PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_50PECENT))
                        percent = 40;
                    else if ((value >= WirelessGFTViewer.LP_HUB.BATT_LEVEL_30PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_40PECENT))
                        percent = 30;
                    else if ((value > WirelessGFTViewer.LP_HUB.BATT_LEVEL_20PECENT) && (value < WirelessGFTViewer.LP_HUB.BATT_LEVEL_30PECENT))
                        percent = 20;
                    else if ((value > WirelessGFTViewer.LP_HUB.BATT_LEVEL_10PECENT) && (value <= WirelessGFTViewer.LP_HUB.BATT_LEVEL_20PECENT))
                        percent = 10;
                    else
                        percent = 0;
                    break;
                case 1:
                    if (value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_100PECENT)
                        percent = 100;
                    else if ((value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_90PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_100PECENT))
                        percent = 90;
                    else if ((value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_80PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_90PECENT))
                        percent = 80;
                    else if ((value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_70PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_80PECENT))
                        percent = 70;
                    else if ((value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_60PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_70PECENT))
                        percent = 60;
                    else if ((value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_50PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_60PECENT))
                        percent = 50;
                    else if ((value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_40PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_50PECENT))
                        percent = 40;
                    else if ((value >= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_30PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_40PECENT))
                        percent = 30;
                    else if ((value > WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_20PECENT) && (value < WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_30PECENT))
                        percent = 20;
                    else if ((value > WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_10PECENT) && (value <= WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_20PECENT))
                        percent = 10;
                    else
                        percent = 0;
                    break;
                case 2:
                    // Whoopie.. We don't do anything
                    percent = value;
                    break;
            }
            // Offset, GFT should power off at 7 percent battery
            percent = (int)(((float)(percent >= 7 ? percent : 7) - 7) / 93 * 100);
            return percent;
        }

        private void threadedPopup(string message)
        {
            Thread newPopupThread = new Thread(new ThreadStart(() =>
                {
                    SynchronizationContext.SetSynchronizationContext(
                        new System.Windows.Threading.DispatcherSynchronizationContext(
                            System.Windows.Threading.Dispatcher.CurrentDispatcher));
                    WirelessGFTViewer.wGftMessageBox popup = new WirelessGFTViewer.wGftMessageBox(message, "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    popup.FormClosed += (s, e) =>
                        System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvokeShutdown(System.Windows.Threading.DispatcherPriority.Background);
                    popup.Show();
                    System.Windows.Threading.Dispatcher.Run();
                }));
            newPopupThread.SetApartmentState(ApartmentState.STA);
            newPopupThread.IsBackground = true;
            newPopupThread.Start();
        }

        public void calcTeamStats()
        {
            List<double> gForce = new List<double>();
            List<double> rotR = new List<double>();
            List<double> rpe = new List<double>();
            List<double> pl = new List<double>();
            List<double> ep = new List<double>();
            string highestGLnkText = "";
            string highestRLnkText = "";
            string highestPLLnkText = "";
            string highestEPLnkText = "";
            string highestRPELnkText = "";
            string highestEPSecondsText = "0 Seconds";
            highestG = 0;
            highestR = 0;
            highestEP = 0;
            highestRPE = 0;
            highestPL = 0;
            int activeTime = 0;
            int activeNodes = 0;
            double totalPlayerLoad = 0;
            double averagePlayerLoad = 0;
            //IMPACT_DATA iData;
            int perfNodes = 0;
            int nodeID = 0;
            for (nodeID = 1; nodeID < NUM_NODES; nodeID++)
            {
                activeTime += lpHub.lpDevices[nodeID - 1].activeTime;
                if (lpHub.lpDevices[nodeID - 1].valid)
                {
                    activeNodes++;
                    foreach(IMPACT_DATA impData in Args[nodeID -1].recvFIFO) {
                        if (impData.linR >= this.highestG)
                        {
                            this.highestG = impData.linR;
                            this.highestGImpactID = impData.eid;
                            highestGLnkText = NodeProfile[nodeID - 1].playerNum + " - " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                            this.highestGUID = impData.UID;
                        }
                        if (impData.rotR >= this.highestR)
                        {
                            this.highestR = impData.rotR;
                            this.highestRImpactID = impData.eid;
                            this.highestRUID = impData.UID;
                            highestRLnkText = NodeProfile[nodeID - 1].playerNum + " - " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                        }
                        gForce.Add(impData.linR);
                        rotR.Add(impData.rotR);
                    }


                    if (lpHub.lpDevices[nodeID - 1].rpeList.Count() > 0)
                    {
                        perfNodes++;
                        rpe.Add(lpHub.lpDevices[nodeID - 1].rpeList.Last().rpe);
                        //totalPlayerLoad += lpHub.lpDevices[nodeID - 1].cumulativeRPE;
                        
                       // foreach (RPE_DATA rData in lpHub.lpDevices[nodeID - 1].rpeList)
                        RPE_DATA rData = lpHub.lpDevices[nodeID - 1].rpeList.Last();
                        {
                            if (rData.rpe >= this.highestRPE)
                            {
                                this.highestRPE = rData.rpe;
                                highestRPELnkText = NodeProfile[nodeID - 1].playerNum + " - " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                                this.highestRPEUID = nodeID;
                            }
                            if (rData.player_load_raw >= this.highestPL)
                            {
                                this.highestPL = rData.player_load_raw;
                                highestPLLnkText = NodeProfile[nodeID - 1].playerNum + " - " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                                this.highestPLUID = nodeID;
                            }
                            if (rData.explosive_count_raw >= this.highestEP)
                            {
                                this.highestEP = rData.explosive_count_raw;
                                highestEPLnkText = NodeProfile[nodeID - 1].playerNum + " - " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                                this.highestEPUID = nodeID;
                                highestEPSecondsText = ((double)rData.explosive_power_raw / 100).ToString() + " Seconds";
                            }
                            //rpe.Add(rData.rpe);
                            pl.Add(rData.player_load);
                            ep.Add(rData.explosive_count);
                        }
                    }

                }
            }
            if (activeNodes > 0)
            {
                averagePlayerLoad = Math.Round(totalPlayerLoad / activeNodes, 2);
            }
            this.averageRPE = averagePlayerLoad;

            gForce.Sort();
            rotR.Sort();

            if (perfNodes > 0)
            {
                this.averageRPE = Math.Round(rpe.Sum() / perfNodes, 1);
                this.averageEP = Math.Round(ep.Sum() / perfNodes, 1);
                this.averagePL = Math.Round(pl.Sum() / 60 / perfNodes, 1);
            }
            this.highestPL = Math.Round(highestPL / 60, 1);

            /*rpe.Sort();
            pl.Sort();
            ep.Sort();
            if (rpe.Count > 0)
            {
                this.averageEP = Math.Round(ep.Average(), 1);
                this.averagePL = Math.Round(pl.Average(), 1);
                this.averageRPE = Math.Round(rpe.Average(), 1);

                if (rpe.Count % 2 == 0)
                {
                    this.medianEP = Math.Round((ep[(ep.Count / 2)] + ep[(ep.Count / 2) - 1]) / 2, 1);
                    this.medianPL = Math.Round((pl[(pl.Count / 2)] + pl[(pl.Count / 2) - 1]) / 2, 1);
                    this.medianRPE = Math.Round((rpe[(rpe.Count / 2)] + rpe[(rpe.Count / 2) - 1]) / 2, 1);
                }
                else
                {
                    this.medianEP = Math.Round(ep[(int)((ep.Count / 2) - 0.5)], 1);
                    this.medianPL = Math.Round(pl[(int)((pl.Count / 2) - 0.5)], 1);
                    this.medianRPE = Math.Round(rpe[(int)((rpe.Count / 2) - 0.5)], 1);
                }
            }*/
            
            if (highestRPELnkText != "")
            {
                linkRPE.Text = highestRPELnkText;
                linkRPE.Visible = true;
            }
            else
            {
                linkRPE.Visible = false;
            }
            if (highestPLLnkText != "")
            {
                linkPL.Text = highestPLLnkText;
                linkPL.Visible = true;
            }
            else
            {
                linkPL.Visible = false;
            }
            if (highestEPLnkText != "")
            {
                linkEP.Text = highestEPLnkText;
                linkEP.Visible = true;
            }
            else
            {
                linkEP.Visible = false;
            }
            lblExpSec.Text = highestEPSecondsText;

            this.totalImpacts = gForce.Count();
            //this.highestG = gForce.Max();
            //this.highestR = rotR.Max();
            if (this.totalImpacts > 0)
            {
                this.averageG = Math.Round(gForce.Average(), 1);
                this.averageR = (int)Math.Round(rotR.Average(), 0);
                // Get Median 
                if (this.totalImpacts % 2 == 0)
                {
                    this.medianG = Math.Round((gForce[(this.totalImpacts / 2)] + gForce[(this.totalImpacts / 2) - 1]) / 2, 1);
                    this.medianR = (int)Math.Round((rotR[(this.totalImpacts / 2)] + rotR[(this.totalImpacts / 2) - 1]) / 2, 0);
                }
                else
                {
                    this.medianG = gForce[(int)((this.totalImpacts / 2) - 0.5)];
                    this.medianR = (int)Math.Round(rotR[(int)((this.totalImpacts / 2) - 0.5)], 0);
                }
                if (activeTime > 0) this.exposure = Math.Round((double)this.totalImpacts / ((double)activeTime / 3000 / 60), 1);
            }
            else
            {
                this.averageG = 0;
                this.averageR = 0;
            }
            
            if (highestGLnkText != "")
            {
                lnkGForce.Text = highestGLnkText;
                lnkGForce.Visible = true;
            }
            else
            {
                lnkGForce.Visible = false;
            }
            if (highestRLnkText != "")
            {
                lnkRotation.Text = highestRLnkText;
                lnkRotation.Visible = true;
            }
            else
            {
                lnkRotation.Visible = false;
            }


            // RPE
            tileImage(this.tableLayoutPanel25, WirelessGFTViewer.Properties.Resources.burn, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, highestRPE.ToString()); // averageRPE.ToString()
            //this.lblTeamRPE.Text = "Average Accumulation";
            //this.lblTeamRPEAveMed.Text = ""; // "Average: " + averageRPE + ", Median: " + medianRPE;

            // Explosive Power
            tileImage(this.tableLayoutPanel16, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, highestEP.ToString()); //Math.Round(averageEP/1000,1).ToString()+"s"
            //this.lblTeamEP.Text = "Average Accumulation";
            //this.lblTeamEPAveMed.Text = ""; // "Average: " + averageEP + ", Median: " + medianEP;

            //this.lblTeamThresholdImpacts.Text = (withinThreshold + aboveThreshold).ToString();
            tileImage(this.tableLayoutPanel6, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, (withinThreshold + aboveThreshold).ToString());
            this.lblTeamThresholdWithin.Text = "Within: " + withinThreshold;
            this.lblTeamThreshold.Text = "Above: " + aboveThreshold + " (" + (totalImpacts > 0 ? Math.Round((double)aboveThreshold / totalImpacts * 100).ToString() : "0") + "%)";
            //this.lblTeamThreshold.Text = totalImpacts + " Impacts Total, " + (totalImpacts > 0 ? Math.Round((double)aboveThreshold / totalImpacts * 100).ToString() : "0") + "% Above Threshold";

            //this.lblTeamGForce.Text = highestG.ToString();
            tileImage(this.tableLayoutPanel5, WirelessGFTViewer.Properties.Resources.impact, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, Math.Round(highestG, 1).ToString());
            this.lblTeamGForceAveMed.Text = "Average: " + averageG + "g, Median: " + medianG + "g";

            //this.lblTeamRotation.Text = highestR.ToString();
            //this.rotationTile.Image
            //this.tableLayoutPanel7.BackgroundImageLayout = ImageLayout.Center;
            //this.tableLayoutPanel7.BackgroundImage = tileImage(WirelessGFTViewer.Properties.Resources.rotation, Color.White, Color.Green, 4, this.HdrRotation.Font, highestR.ToString());
            tileImage(this.tableLayoutPanel7, WirelessGFTViewer.Properties.Resources.rotation, Color.White, Color.Green, 4, this.lblTeamGForce.Font, highestR.ToString());
            this.lblTeamRotationAveMed.Text = "Average: " + averageR + "°/s, Median: " + medianR + "°/s";

            //this.lblTeamExposure.Text = exposure.ToString();
            if (true)
            {
                this.HdrExposure.Text = "Player Load";
                this.lblExposure.Text = "Highest";
                tileImage(this.tableLayoutPanel8, WirelessGFTViewer.Properties.Resources.burn, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, highestPL.ToString()); //averagePL.ToString()
            }
            else
            {
                tileImage(this.tableLayoutPanel8, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, exposure.ToString());
            }
        }

        public void DisposeHub()
        {
            if (GftWirelessHidHub != null)
                GftWirelessHidHub.Dispose();
            GftWirelessHidHub = null;
            UpdateNodeControlEnabled();
        }

        string Hub_productStr = "";                                         // Added by Jason Chen, 2017.06.23
        private void CheckIfWirelessHidHubPresent()
        {
            if (WirelessViewerOpen)
            {

                BuzzHandsetDevice GftWirelessHidHub_temp = BuzzHandsetDevice.FindBuzzHandset(0x1cd1, 0x0403);	// look for the device on the USB bus
                if (GftWirelessHidHub_temp != null)  //  did we find it?
                {
                    Hub_productStr = GftWirelessHidHub_temp.productString;
                    if (GftWirelessHidHub_temp.mFileHandleValid)
                    {
                        // Yes! So wire into its events and update the UI
                        if (GftWirelessHidHub == null)
                        {
                            GftWirelessHidHub = GftWirelessHidHub_temp;
                            GftWirelessHidHub.OnRxPacketReceived += new HIDInputEventHandler(WirelessHidHub_OnRxPacketReceived);
                            UpdateNodeControlEnabled();
                        }
                        else
                        {
                            //GftWirelessHidHub_temp.Dispose();
                        }
                    }
                }
                else
                {
                    if (GftWirelessHidHub != null)
                        GftWirelessHidHub.Dispose();
                    GftWirelessHidHub = GftWirelessHidHub_temp;
                    UpdateNodeControlEnabled();
                }
            }
        }

        private void WirelessHidHub_OnRxPacketReceived(object sender, HIDInportEventArgs args)
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new HIDInputEventHandler(WirelessHidHub_OnRxPacketReceived), new object[] { sender, args });
                }
                else
                {
                    if (GftWirelessHidHub != null)
                    {
                        //BuzzInputReport myReportIn = GftWirelessHidHub.getInputReport;
                        BuzzInputReport myReportIn = args.myInputReport;
                        byte[] rx_bufff = new byte[myReportIn.BufferLength - 1];
                        Array.Copy(myReportIn.Buffer, 1, rx_bufff, 0, myReportIn.BufferLength - 1);

                        //HIDinBuffer.Enqueue(rx_bufff);                        
                        RxPacketProcess(ref rx_bufff, false);
                    }
                }
            }
            catch (Exception cc)
            {
                string ccc = cc.Message;
                //MessageBox.Show(ccc);
                try
                {
                    //if (tabControl4.TabPages.Contains(tabPage16))
                        //m_Edit_Immediate.AppendText("++++" + ccc + "\r\n");
                }
                catch (Exception xx)
                {
                    //MessageBox.Show(xx.Message);
                }
                
            }
        }


        private void UpdateNodeControlEnabled()
        {
            bool bEnabled = false;
            if (GftWirelessHidHub != null)
            {
                bEnabled = true;
            }
            //if (GftWirelessHidHub == null)
#if false
            {
                int countC = tableLayoutPanel8.Controls.Count;

                if (countC != 0)
                {
                    for (int i = 0; i < countC; i++)
                    {
                        tableLayoutPanel8.Controls[i].Enabled = bEnabled;
                    }
                }
            }
#endif

            if (bEnabled)
            {
                //m_Button_hubInfo.Enabled = true;
                //m_button_NodeInfo.Enabled = true;
                //m_button_Bind.Enabled     = true;
                //m_button_Clone.Enabled    = true;
                //m_button_Sync.Enabled     = true;
                //m_button_Upload.Enabled = true;
                //m_button_Reset.Enabled    = true;

                //this.Text = "gForceTrackerViewer";// +" ( Hid HUB Connected )";
                lpHub = new WirelessGFTViewer.LP_HUB(wirelessViewer.dataGridView);//, imageList1);
                wirelessViewer.setHub(ref lpHub);
                wirelessViewer.setProfile(ref NodeProfile);
                toolStripStatus.Text = Hub_productStr + " Connected";                         // Added by Jason Chen, 2017.06.23
              //toolStripStatus.Text = "HUB Connected";
                toolStripStatus.BackColor = Color.Lime;

                //getGftWirelessAll();

                //DeleteAllUsersData();

                //NodeControlInit();

                ArgsInit();

                NodeProfileInit();

                Wireless_dataInit();

                //this.BeginInvoke((MethodInvoker)delegate { m_button_NodeInfo_Click(this, null); });     //moved into RxPacketProcess()//  Commented by Jason Chen, 2017.03.16
                
#if true
                byte[] DataBuf = new byte[2];
                DataBuf[0] = MSG_HUB_TO_HOST_RESET;
                DataBuf[1] = 1;
                RxPacketProcess(ref DataBuf, false);                
                Thread.Sleep(10);

                SetTimer(this.Handle, 0x600, 10000, null);                                                       // Added by Jason Chen for checking nodes populated successful or not, 2017.06.15
#endif

#if false
                if (Settings.Default.DisableToolPage)
                {
                    if (tabControl4.TabPages.Contains(tabPage16))
                    {
                        tabControl4.TabPages.Remove(tabPage16);
                        tabControl4.ItemSize = new Size(92, tabControl4.ItemSize.Height);
                    }
                }
                else
                {
                    if (!(tabControl4.TabPages.Contains(tabPage16)))
                    {
                        tabControl4.TabPages.Add(tabPage16);
                        tabControl4.ItemSize = new Size(60, tabControl4.ItemSize.Height);
                    }
                    getGftWirelessAll();
#if false
                    if (gftWirelessView.Rows.Count != 0)
                    {
                        DataGridViewRow cur_row = gftWirelessView.Rows[0];                               // Added by Jason Chen, 2014.04.30

                        string nodeIDs = cur_row.Cells[1].Value.ToString();

                        mEmailAddress.Text = "GForceTracker@artaflex.com";
                        mSerialNum.Text = cur_row.Cells[0].Value.ToString();

                        m_Node.Text = cur_row.Cells[1].Value.ToString();
                        mPlayerNum.Text = cur_row.Cells[2].Value.ToString();
                        mRecThreshold.Text = cur_row.Cells[4].Value.ToString();
                        mAlarmThreshold.Text = cur_row.Cells[5].Value.ToString();

                        mFirstName.Text = cur_row.Cells[7].Value.ToString();
                        mLastName.Text = cur_row.Cells[8].Value.ToString();

                        int AlarmMode = Convert.ToInt16(cur_row.Cells[9].Value.ToString());
                        mAlarmEnable.Checked = ((AlarmMode & 0x80) == 0) ? false : true;
                        mAudio.Checked = ((AlarmMode & 0x10) == 0) ? false : true;
                        mVisual.Checked = ((AlarmMode & 0x20) == 0) ? false : true;
                        mInterlock.Checked = ((AlarmMode & 0x40) == 0) ? false : true;
                        mTestMode.Checked = ((AlarmMode & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01

                        mProximityEnable.Checked = ((AlarmMode & 0x02) == 0x02) ? true : false;    // Added by Jason Chen, 2015.05.21
                        mFieldDataEnable.Checked = ((AlarmMode & 0x04) == 0x04) ? true : false;         // Added by Jason Chen, 2015.05.21                        
                    }
#endif
                }
#endif
            }
            else 
            {
                if(!mBLEInterface.m_bConnected)
                {
                    //this.BeginInvoke((MethodInvoker)delegate { mInitBLE_Click(this, null); });     //moved into RxPacketProcess()//  Commented by Jason Chen, 2017.03.16
                    //m_Button_hubInfo.Enabled = false;
                    //m_button_NodeInfo.Enabled = false;
                    //m_button_Bind.Enabled = false;
                    m_button_Clone.Enabled = false;
                    //m_button_Sync.Enabled = false;
                    //m_button_Upload.Enabled = false;
                    //m_button_Reset.Enabled = false;

                    //this.Text = "gForceTrackerViewer";// +" ( Hid HUB Disconnected )";

                    toolStripStatus.Text = "HUB/BLE not Connected";
                    toolStripStatus.BackColor = Color.Red;

                    //DeleteAllUsersData();
#if false
                int rowCount = gftWirelessView.Rows.Count;
                for (int index = 0; index < rowCount; index++)
                {
                    gftWirelessView.Rows.RemoveAt(0);
                }
#endif

                    if (Args != null)
                        Args.Clear();

                    if (NodeProfile != null)
                    {
                        //NodeProfile.Clear();           // 09.11
                    }

                    //if (NodeControl != null)
                    //    NodeControl.Clear();

                    //controlPanel.Controls.Clear();

                    if (lpHub != null)
                    {
                        lpHub.lpDevices.Clear(); // 2014.10.01
                        lpHub.dgvLpPopulateView(); // 2014.10.01
                    }
                    lpHub = null;

#if ENABLE_DASHBOARD
                    teamChart.Series.Clear();
#else
                chartViewer.Series.Clear();
#endif
                    chartRawLinW.Series.Clear();
                    chartRawGyroW.Series.Clear();

                    new_dongle_plugged = false;

                    //rowCount = dataGridView3.Rows.Count;
                    //for (int index = 0; index < rowCount; index++) dataGridView3.Rows.RemoveAt(0);        
                }       
            }
        }

        protected override void OnDeviceArrived(EventArgs args)
        {
            if (WirelessViewerOpen)
            {
                base.OnDeviceArrived(args);
                if (GftWirelessHidHub == null)
                    CheckIfWirelessHidHubPresent();
                if (gForceHid == null)
                    CheckIfGFTHidHubPresent();
            }
        }

        /// <summary>
        /// Called when a new device is removed
        /// </summary>
#if true  // Added by Jason Chen
        protected override void OnDeviceRemoved(EventArgs args)
        {
            base.OnDeviceRemoved(args);
            if (GftWirelessHidHub != null)
                CheckIfWirelessHidHubPresent();
            if (gForceHid != null)
                CheckIfGFTHidHubPresent();
        }
#endif

        #region  old define

        internal const byte MY_HID_REPORT_LENGTH = 64;
        internal const string APPLICATION_STRING = "gForce Firmware";

        internal const double ACC_SENSITIVITY_LSB = 0.244;
        internal const double ACC_SENSITIVITY_LSB2 = (200d / 128d);
        internal const double GYRO_SENSITIVITY_LSB = 0.07; //70mdps/digit
        internal const byte MHID_CMD_RESPONSE_BIT = 0x80;

        //internal const byte  ENABLE_DATA_LOGGER = 0x1; 
#if false
        public enum mhidCmd
        {
            MHID_CMD_GET_ACC_ENTRY = 0,
            MHID_CMD_GET_ACC_ENTRY_CNT,
            MHID_CMD_GET_ACC_ENTRY_NEXT,
            MHID_CMD_GET_ACC_ENTRY_FIRST,
            MHID_CMD_GET_ACC_DATA_MAX,
            MHID_CMD_GET_ACC_DATA_NEXT,
            MHID_CMD_GET_ACC_DATA_FIRST,
            MHID_CMD_GET_ACC_DATA_LAST,
            MHID_CMD_SET_USR_PROFILE,
            MHID_CMD_GET_USR_PROFILE,
            MHID_CMD_ERASE_ACC_DATA,
            MHID_CMD_ERASE_USR_PROFILE,
            MHID_CMD_SET_TIME,
            MHID_CMD_START_BOOT,
            MHID_CMD_REBOOT,           //Bootldr & firmwre
            MHID_CMD_ERASE_FIRMWARE,   //Only used by bootloader
            MHID_CMD_PROG_FIRM,        //Only used by bootloader
            MHID_CMD_GET_BATTERY_STAT, //abandoned
            MHID_CMD_GET_ACC_ENTRY_DATA_ALL,
            MHID_CMD_GET_ACC_ENTRY_DATA_STOP,
            MHID_CMD_TEST,
            MHID_CMD_CHECKBAT,
            MHID_CMD_DEBUG,
            MHID_CMD_GET_GID,
            MHID_CMD_GET_DEV_INFO, //bootldr & firmware bootldr id: 0x1E firm:0x2D
            MHID_CMD_GET_TIME,
            MHID_CMD_SET_SN,
            MHID_CMD_GET_SN,
            MHID_CMD_RUN_FIRM    //bootldr
        };

        internal const byte HID_INT_OUT_EP_SIZE = 64;
        internal const byte HID_INT_IN_EP_SIZE = 41;

        [StructLayout(LayoutKind.Sequential)]
        public struct mhid_cmd
        {
            byte reportId;
            byte cmd;
            byte subCmd;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 41)]
            byte[] data;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct mhid_boot_cmd
        {
            byte reportId;
            byte cmd;
            byte subCmd;//or count for firmware data
            byte addrH;
            byte addrL;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 37)]
            byte[] data;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct mhid_rpt
        {
            byte reportId;
            byte h;
            byte cnt;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 39)]
            byte[] data;
        };

        internal const byte ACC_ENTRY_MAGIC = 0x48;
        internal const byte ACC_ENTRY_START = 0x49;
        internal const byte ACC_ENTRY_END = 0x4A;

        enum update_firmware_state
        {
            UPDATE_FIRM_STATE_START,
            UPDATE_FIRM_STATE_WAIT,
            UPDATE_FIRM_STATE_RD,
            UPDATE_FIRM_STATE_RD_WAIT,
            UPDATE_FIRM_STATE_IDL,
            UPDATE_FIRM_STATE_ERR,
            UPDATE_FIRM_STATE_TX
        };
#endif
        #endregion
        #region generic hid
        SynchronizationContext uiContext;
        //private int gftBattVol = 0;
        //private int gftStat = 0;
        //private int usbStat = 0;
        //private bool BootloaderMode = false;
        //private IntPtr deviceNotificationHandle;
        //private Boolean exclusiveAccess;
        //private FileStream fileStreamDeviceData;
        //private SafeFileHandle hidHandle;
        //private String hidUsage;
        //private bool bUploadInProgress = false;
        //private bool hasUploadData = false;
        //private String myDevicePathName;
        //private Boolean transferInProgress = false;
        //private bool transferSuccess = false;
        //private Debugging MyDebugging = new Debugging(); //  For viewing results of API calls via Debug.Write.
        //private DeviceManagement MyDeviceManagement = new DeviceManagement(); 
        //private Hid MyHid = new Hid(MY_VENDOR_ID, MY_PRODUCT_ID, MY_PRODUCT_BOOT_ID);

        //private static System.Timers.Timer tmrContinuousDataCollect;
        //private bool BackgroundWorkerThreadBusy = false;
        //private string thread_jobs = "NONE";

        //private const int rom_endaddr = 0xefff;
        //const byte FIRMWARE_UPDATE  =   0x01;
        //internal FrmMain FrmMy; 

        //  This delegate has the same parameters as AccessForm.
        //  Used in accessing the application's form from a different thread.

        private delegate void MarshalToForm(String action, String textToAdd, System.Drawing.Color color);

        ///  <summary>
        ///  Uses a series of API calls to locate a HID-class device
        ///  by its Vendor ID and Product ID.
        ///  </summary>
        ///          
        ///  <returns>
        ///   True if the device is detected, False if not detected.
        ///  </returns>
        internal const UInt16 MY_PRODUCT_BOOT_ID = 0x0401;
        internal const UInt16 MY_PRODUCT_ID      = 0x0402;
        internal const UInt16 WIRELESS_PID       = 0x0403;
        internal const UInt16 MY_VENDOR_ID       = 0x1CD1;


        ///  <summary>
        ///  Performs various application-specific functions that
        ///  involve accessing the application's form.
        ///  </summary>
        ///  
        ///  <param name="action"> a String that names the action to perform on the form</param>
        ///  <param name="formText"> text that the form displays or the code uses for 
        ///  another purpose. Actions that don't use text ignore this parameter. </param>
        private void AccessForm(String action, String formText, System.Drawing.Color color)
        {
            try
            {
                //  Select an action to perform on the form:
                switch (action)
                {
                    case "updateBatt":
                        //progressBarBattery.Value = gftBattVol;
                        break;
                    case "userinputForUpload":
                        int mSessionID = SessionID;
                        //backgroundWorkerT.RunWorkerAsync(mSessionID);
                        break;
                    case "AddItemToListBox":
                        //lblUpdateFirm.Items.Add(formText);
                        break;
                    case "autoDraw":
                        //curSessionID = gDataGetSessionLastSID();
                        //loadFileAuto(this, null);
                        break;
                    case "toolStripStatusLabelGFT":
                        toolStripStatusLabelGFT.Text = formText;
                        toolStripStatusLabelGFT.BackColor = color;
                        break;
                    case "toolStripStatusLabelChart":
                        toolStripStatusLabelChart.Text = formText;
                        toolStripStatusLabelChart.BackColor = color;
                        //  If it's a single transfer, re-enable the command button.
                        // if ( cmdContinuous.Text == "Continuous" ) 
                        // { 
                        //      cmdOnce.Enabled = true; 
                        // }                         
                        break;
                    case "ScrollToBottomOfListBox":
                        //lblUpdateFirm.SelectedIndex = lblUpdateFirm.Items.Count - 1;
                        break;
                    case "txtBoxTabUsrTotalRecords":
                        //txtBoxTabUsrTotalRecords.Text = formText;
                        break;
                    case "txtBoxTabChartTotalRecords":
                        //txtBoxTabChartTotalRecords.Text = formText;
                        break;
                    case "txtBoxTabChartROA":
                        //txtBoxTabChartROA.Text = formText;
                        break;
                    case "toolStripStatus":                                 //09.11
                        toolStripStatus.BackColor = color;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                DisplayException(this.Name, ex);
                MessageBox.Show("AccessForm()");
            }
        }


        ///  <summary>
        ///  Perform startup operations.
        ///  </summary>
        ///  

        //bool TooltabPage16Exist = false;                                            // commented by Jason Chen, 2017.03.28

        private void frmMain_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            //if (Application.OpenForms[0] != null)
            //{
            //    Application.OpenForms[0].SendToBack();
            //    Application.OpenForms[0].Hide();
            //}
            if (Application.OpenForms[Application.OpenForms.Count-1] != null)
            {
                Application.OpenForms[Application.OpenForms.Count - 1].BringToFront();
            }
            //int NUM_UPLOAD_LIMIT = Convert.ToInt16(m_NumUpload.Text);
            NUM_UPLOAD_LIMIT = Convert.ToInt16(simUploads.Text);
            

            //labelLoginUser.Location = new Point((panelLogin.Width) / 2 - (labelLoginUser.Width) / 2, (panelLogin.Height) / 2 - (labelLoginUser.Height) / 2);
            //pictureBox2.Location = new Point(labelLoginUser.Location.X, labelLoginUser.Location.Y - pictureBox2.Height);

            SqlCeConnection mycon = new SqlCeConnection(conString);

            try
            {
                mycon.Open();
                if (mycon.State != ConnectionState.Open)
                    MessageBox.Show("My Database fails to be connected?");
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
                Close();
                return;
            }


            mBLEInterface = new BLE_Interface_Form();
            oSignalEventBle = new AutoResetEvent(false);

            m_OutstandingFrames = 0;

            bleAddr_s = new List<String>();
            bleName_s = new List<String>();

            USB_OR_BLE = false;


            if (!mBLEInterface.IsBLERadioExist())
            {
                //mReceivedBytes.AppendText("Bluetooth radio is not present or not functioning.  Please plug in the dongle and try again.\r\n");
            }
            //Could need to add kernel32.lib user32.lib advapi32.lib into Linker inputs option!
            // This application supports Microsoft on Win8				 
            if (!mBLEInterface.IsOSWin8(true))
            {
                // mReceivedBytes.AppendText("The application just can be running in Windows 8 or later.\r\n");
            }

            if (mBLEInterface.GetDeviceList(out bleAddr_s, out bleName_s) > 0)
            {
                //foreach (String addr in bleAddr_s)
                //int index = 0;
                foreach (String bleAddr in bleName_s)
                {
                    //if (bleAddr.Substring(0, 9) == "BLEBridge")
                    {
                        mBT_Name.Items.Add(bleAddr);
                    }
                }
                foreach (String bleAddr1 in bleAddr_s)
                {
                    //if (bleAddr.Substring(0, 9) == "BLEBridge")
                    {
                        mBT_Addr.Items.Add(bleAddr1);
                    }
                }

                mBT_Addr.SelectedIndex = 0;
            }

            // this.AlamEnableChanged

            //DeleteAllUsersData();            
            CheckIfGFTHidHubPresent();
            CheckIfWirelessHidHubPresent();
            

            //enableToolPageToolStripMenuItem.Checked = !(Settings.Default.DisableToolPage); // removed by Jared Stock, 2014.06.25

            gftGMT_Enable = true;// Settings.Default.GFT_GMT_Enable;
            autoUpload_enable.Checked = Settings.Default.AUTO_UPLOAD_Enable;
            refreshForList = Settings.Default.ReFresh_for_List;
            usbUpload_Enable.Checked = Settings.Default.usbUpload_Enable;
            //mEnableRawData.Checked = Settings.Default.rawDataEnable;

            //processRecvbuff = new uploadDataForWirelessProcess(this);

#if false
            if (Settings.Default.DisableToolPage)
            {
                if (tabControl4.TabPages.Contains(tabPage16))
                {
                    tabControl4.TabPages.Remove(tabPage16);
                    tabControl4.ItemSize = new Size(92, tabControl4.ItemSize.Height);
                }
            }
            else
            {
                if (!(tabControl4.TabPages.Contains(tabPage16)))
                {
                    tabControl4.TabPages.Add(tabPage16);
                    tabControl4.ItemSize = new Size(60, tabControl4.ItemSize.Height);
                }
            }

            TooltabPage16Exist = tabControl4.TabPages.Contains(tabPage16);
#endif
            mGFT.Location = new Point(this.Size.Width - mGFT.Size.Width - 20, mGFT.Location.Y);

            //pipeServer = new AnonymousPipeServerStream(PipeDirection.Out, HandleInheritability.None);
            //pipeHandle = pipeServer.ClientSafePipeHandle;

            //mNotesBox.AppendText("Please unplug USB and Power off,\r\n Power On to register the GFT when a new  gft is bound!");
        }


        ///  <summary>
        ///  Initialize the elements on the form.
        ///  </summary>

        ///  <summary>
        ///  Enables accessing a form's controls from another thread 
        ///  </summary>
        ///  
        ///  <param name="action"> a String that names the action to perform on the form </param>
        ///  <param name="textToDisplay"> text that the form displays or the code uses for 
        ///  another purpose. Actions that don't use text ignore this parameter.  </param>

#if false
        private void MyMarshalToForm(String action, String textToDisplay, String color)
        {
            PacketStatusAction = action;
            PacketStatusTextToDisplay = textToDisplay;
            PacketStatusColor = color;
        }
#else
        private void MyMarshalToForm(String action, String textToDisplay, System.Drawing.Color color)    // 2014.10.27
        {
            object[] args = { action, textToDisplay, color };
            MarshalToForm MarshalToFormDelegate = null;

            //  The AccessForm routine contains the code that accesses the form.
            MarshalToFormDelegate = new MarshalToForm(AccessForm);

            //  Execute AccessForm, passing the parameters in args.
            base.Invoke(MarshalToFormDelegate, args);
            //base.BeginInvoke(MarshalToFormDelegate, args);                     // 2014.10.27
        }
#endif
        private void MyMarshalToFormBW(String action, String textToDisplay, System.Drawing.Color color)
        {
            object[] args = { action, textToDisplay, color };
            MarshalToForm MarshalToFormDelegate = null;

            //  The AccessForm routine contains the code that accesses the form.
            MarshalToFormDelegate = new MarshalToForm(AccessForm);

            //  Execute AccessForm, passing the parameters in args.
            base.Invoke(MarshalToFormDelegate, args);
        }

        ///  <param name="moduleName"> the module where the exception occurred. </param>
        ///  <param name="e"> the exception </param>

        internal static void DisplayException(String moduleName, Exception e)
        {
            String message = null;
            String caption = null;

            //  Create an error message.
            message = "Exception: " + e.Message + "\r\n" + "Module: " + moduleName + "\r\n" + "Method: " + e.TargetSite.Name;
            caption = "Unexpected Exception";
            MessageBox.Show(message, caption, MessageBoxButtons.OK);
            Debug.Write(message);
        }

        #endregion
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        private string gidToString()
        {
            return string.Format("{0:X2}", gftProfile.GID[0]) +
                   string.Format("{0:X2}", gftProfile.GID[1]) +
                   string.Format("{0:X2}", gftProfile.GID[2]) +
                   string.Format("{0:X2}", gftProfile.GID[3]) +
                   string.Format("{0:X2}", gftProfile.GID[4]) +
                   string.Format("{0:X2}", gftProfile.GID[5]);

        }

        private string gidToString(byte[] mgid)
        {
            return string.Format("{0:X2}", mgid[0]) +
                   string.Format("{0:X2}", mgid[1]) +
                   string.Format("{0:X2}", mgid[2]) +
                   string.Format("{0:X2}", mgid[3]) +
                   string.Format("{0:X2}", mgid[4]) +
                   string.Format("{0:X2}", mgid[5]);
        }


        internal const byte PROFILE_ALARM_EN_MSK = 0x80;
        internal const byte PROFILE_ALARM_AU_MSK = 0x10;
        internal const byte PROFILE_ALARM_VI_MSK = 0x20;
        internal const byte PROFILE_ALARM_LOCK_MSK = 0x40;    //return to play interlock
        internal const byte PROFILE_ALARM_ON_MSK = 0x01;

        PaintEventArgs myPaintEvent = null;
        private String hitPosition = "NONE";
        private String hitPositionColor = "blue";

        public enum Title
        {
            King,
            Sir
        };

        private void myUserTabSelected()
        {
            //gDataGetUsers();
            //tabControl4.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            loginCheck();
        }


        private void loginCheck()
        {
            //if (maskedTextBoxLoginPW.Text != "gft")
            //{
            //    MessageBox.Show("Invalid password!", "GFT login");
            //    return;
           // }

            //panelLogin.Visible = false;
            myUserTabSelected();
            Thread.Sleep(50);
            //tabControl4.SelectTab(tabPage18);
            //scanUSB();

            //this.BeginInvoke((MethodInvoker)delegate { mGetUserProfile_Click(this, null); });                        
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //tabControl4.Visible = false;
            //panelLogin.Visible = true;
           // maskedTextBoxLoginPW.Text = "";
        }

        public DateTime FirstDayOfWeek()
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            var fdow = currentCulture.DateTimeFormat.FirstDayOfWeek;
            var offset = System.DateTime.UtcNow.DayOfWeek - fdow < 0 ? 7 : 0;
            var numberOfDaysSinceBeginningOfTheWeek = System.DateTime.UtcNow.DayOfWeek + offset - fdow;

            return new DateTime(System.DateTime.UtcNow.Year, System.DateTime.UtcNow.Month, System.DateTime.UtcNow.Day).AddDays(-numberOfDaysSinceBeginningOfTheWeek);
        }

        public static List<string> GetCountryList()
        {
            RegionInfo country = new RegionInfo(new CultureInfo("en-US", false).LCID);
            List<string> countryNames = new List<string>();
            foreach (CultureInfo cul in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                country = new RegionInfo(new CultureInfo(cul.Name, false).LCID);
                countryNames.Add(country.EnglishName.ToString());
            }
            //countryNames.OrderBy(names => names).Distinct();
            IEnumerable<string> nameAdded = countryNames.OrderBy(names => names).Distinct();
            List<string> countryList = new List<string>();
            countryList.Add("Please select");
            foreach (string item in nameAdded)
            {
                countryList.Add(item);
            }
            return countryList;
        }

#if false
        public static List<string> GetCountryList()
        {
            List<string> countryList = new List<string>();
            countryList.Add("CA Canada");
            countryList.Add("US United States");

            return countryList;
        }
#endif
        public static List<string> GetSportsList()
        {
            List<string> sportList = new List<string>();
            sportList.Add("Please select");
            sportList.Add("Hockey");
            sportList.Add("Football");
            sportList.Add("Basketball");
            return sportList;
        }

        public static List<string> GetSportsLevelList()
        {
            List<string> sportList = new List<string>();
            sportList.Add("Please select");
            sportList.Add("Pro");
            sportList.Add("Elite");
            sportList.Add("Competitive");
            sportList.Add("Recreational");
            return sportList;
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if false
            try
            {
                System.Net.IPHostEntry i = System.Net.Dns.GetHostEntry("www.gforcetracker.com");
            }
            catch
            {
                return;
            }
#else
            bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            if (!bb)
            {
                MessageBox.Show("Internet connection is not available");
                return;
            }

#endif

            Help.ShowHelp(this, "http://www.gforcetracker.com/");
        }



        private void maskedTextBoxLoginPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return) loginCheck();
        }

#if false
        private void DoUserInputWork(object sender, DoWorkEventArgs e)
        {
            uiContext.Send(_ =>
            {
                formUploadInput uploadForm = new formUploadInput();

                if (uploadForm.ShowDialog(myMainFrmInstance) == DialogResult.OK)
                {
                    gDataUpdateSession((int)e.Argument, uploadForm.exerciseLog, uploadForm.exerciseType);
                    loadFileAuto(myMainFrmInstance, null);
                    uploadForm.exerciseType = 'G';
                    uploadForm.exerciseLog = "Notes:";
                    hasUploadData = false;
                }
            }, null);

           
        }
#endif
        public enum mft_cmd
        {
            MFT_CMD_NULL,
            MFT_CMD_FLASH,
            MFT_CMD_LED,
            MFT_CMD_RTC,
            MFT_CMD_GYRO,
            MFT_CMD_ACCE,
            MFT_CMD_BUZZ,
            MFT_CMD_TORCH_LED,
            MFT_CMD_DEFAULT_SET,
            MFT_CMD_PROX
        };
        
        private void wirelessViewer_OnButtonUploadAll(object sender, EventArgs args)
        {

            if (MessageBox.Show(new Form { TopMost = true }, "Are you sure you want to Download Impact and Session Data from all devices?\n\nNo more data will be recorded after each device begins downloading. After the download is complete, each device will be automatically powered off.",
                                    "Download Sessions from All Devices",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes)
            {

                int nodeID;

                uploadFlag = !uploadFlag;
                for (nodeID = 1; nodeID < NUM_NODES; nodeID++)
                {
                    if (lpHub.lpDevices[nodeID-1].valid)
                    {
                        if (lpHub.lpDevices[nodeID - 1].mode == 0)
                        {
                            // Device is off, power it on 
                            lpHub.lpDevices[nodeID - 1].powerOnFlag = true;
                            lpHub.lpDevices[nodeID - 1].powerOnSent = false;
                            //NodeControl[nodeID - 1].setChkPowerOn(true);
                            cmdProcess(nodeID, USB_OR_BLE);
                        }
                        NodeProfile[nodeID - 1].uploadFlag = uploadFlag;

                        lpHub.lpDevices[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;
    #if USE_ORIGINAL_REFRESH
                        lpHub.dgvLpPopulateView(nodeID); // 2014.10.01
    #else
                        this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), nodeID);
    #endif
                       // lpHub.lpDevices[nodeID-1].updateDgv = true;

                        NodeProfile[nodeID - 1].deleteFlag = false;                          // Added by Jason Chen 2014.02.18
                        NodeProfile[nodeID - 1].uploadNum_Flag = 0;                          // Added by Jason Chen 2014.02.18
                        NodeProfile[nodeID - 1].upload_req_flag = false;                     // Added by Jason Chen 2014.03.18

                        lpHub.lpDevices[nodeID - 1].upldFlag_num = NodeProfile[nodeID - 1].uploadNum_Flag;
                        lpHub.lpDevices[nodeID - 1].progressValue = 0;
                    }
                }
                /*if(uploadFlag)
                    lpHub.dgv.Columns[6].Name = "GeForce (Upload Enbaled)";
                else
                    lpHub.dgv.Columns[6].Name = "GeForce";*/

            }
        }

        private void wirelessViewer_OnButtonDevicesWithData(object sender, EventArgs args)
        {
            if (NodeProfile.Count > 0)
            {
                for (int nodeID = 1; nodeID < NUM_NODES; nodeID++)
                {
                    if (lpHub.lpDevices[nodeID - 1].valid && NodeProfile[nodeID - 1].GID != null)
                    {
                        if (NodeProfile[nodeID - 1].newDataOnDevice) //NodeProfile[nodeID - 1].newDataOnDevice  || NodeProfile[nodeID - 1].impactNum > 0
                        {
                            if (lpHub.lpDevices[nodeID - 1].mode == 0)
                            {
                                // Device is off, power it on 
                                lpHub.lpDevices[nodeID - 1].powerOnFlag = true;
                                lpHub.lpDevices[nodeID - 1].powerOnSent = false;
                                //NodeControl[nodeID - 1].setChkPowerOn(true);
                                cmdProcess(nodeID, USB_OR_BLE);
                            }
                            NodeProfile[nodeID - 1].uploadFlag = uploadFlag;

                            lpHub.lpDevices[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;

                            this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), nodeID);


                            NodeProfile[nodeID - 1].deleteFlag = false;                          // Added by Jason Chen 2014.02.18
                            NodeProfile[nodeID - 1].uploadNum_Flag = 0;                          // Added by Jason Chen 2014.02.18
                            NodeProfile[nodeID - 1].upload_req_flag = false;                     // Added by Jason Chen 2014.03.18

                            lpHub.lpDevices[nodeID - 1].upldFlag_num = NodeProfile[nodeID - 1].uploadNum_Flag;
                            lpHub.lpDevices[nodeID - 1].progressValue = 0;
                        }
                    }
                }

            }
        }


        private void wirelessViewer_OnButtonPowerOffAll(object sender, EventArgs args)
        {
            if (MessageBox.Show(new Form { TopMost = true }, "Are you sure you want to Power Off All Devices?",
                                    "Power off All GFTs",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.powerOnGFTs.BackColor = this.powerOffGFTs.BackColor;
                foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
                {
                    string nodeIDs;
                    if (cur_row != null)
                        nodeIDs = cur_row.Cells[0].Value.ToString();
                    else
                        return;
                    int nodeID;

                    nodeID = Convert.ToInt16(nodeIDs);
                    lpHub.lpDevices[nodeID - 1].powerOffFlag = true;
                    lpHub.lpDevices[nodeID - 1].powerOffSent = false;
                    //NodeControl[nodeID - 1].setChkPowerOff(true);
                    lpHub.lpDevices[nodeID - 1].keepPowerOnFlag = false; // This flag is referenced to determine if GFT should be automatically powered on if it's seen off
                    cmdProcess(nodeID, USB_OR_BLE);
                    //mSendMsgCMD_Queue((byte)nodeID, 0x16);//22
                }
            }

            if (!USB_OR_BLE)
            {
                byte[] txBuff = new byte[65];
                txBuff[1] = 0x5A;
                txBuff[2] = 0x00;
                txBuff[3] = 0;

                GftWirelessHidHub.WriteOutput(ref txBuff);
            }
            else
            {
                byte[] txBuff = new byte[20];
                txBuff[0] = 0x5A;
                txBuff[1] = 0x00;
                txBuff[2] = 0;
                mBLEInterface.SendData(ref txBuff, 20);
            }
        }


        private void wirelessViewer_OnButtonPowerOnAll(object sender, EventArgs args)
        {

            if (MessageBox.Show(new Form { TopMost = true }, "Are you sure you want to Power On All Devices?\n\nAny devices which are manually powered off will be automatically powered on again. To Power them off, you will need to use the interface to do so.",
                                    "Power on All GFTs",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.powerOnGFTs.BackColor = Color.Green;

#if true
                foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
                {
                    string nodeIDs;
                    if (cur_row != null)
                        nodeIDs = cur_row.Cells[0].Value.ToString();
                    else
                        return;
                    int nodeID;

                    nodeID = Convert.ToInt16(nodeIDs);
                    lpHub.lpDevices[nodeID - 1].powerOnFlag = true;
                    lpHub.lpDevices[nodeID - 1].powerOnSent = false;
                    lpHub.lpDevices[nodeID - 1].keepPowerOnFlag = true; // This flag is referenced to determine if GFT should be automatically powered on if it's seen off
                    //NodeControl[nodeID - 1].setChkPowerOn(true);
                    cmdProcess(nodeID, USB_OR_BLE);
                    //mSendMsgCMD_Queue((byte)nodeID, 0x16);//22
                }
#else
                foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
                {
                    string nodeIDs;
                    if (cur_row != null)
                        nodeIDs = cur_row.Cells[0].Value.ToString();
                    else
                        return;
                    int nodeID;

                    nodeID = Convert.ToInt16(nodeIDs);
                    lpHub.lpDevices[nodeID - 1].powerOnFlag = true;
                    lpHub.lpDevices[nodeID - 1].powerOnSent = false;
                    lpHub.lpDevices[nodeID - 1].keepPowerOnFlag = true; // This flag is referenced to determine if GFT should be automatically powered on if it's seen off
                    //NodeControl[nodeID - 1].setChkPowerOn(true);
                    cmdProcess(nodeID);
                    //mSendMsgCMD_Queue((byte)nodeID, 0x16);//22
                }

                byte[] txBuff = new byte[65];
                txBuff[1] = 0x5A;
                txBuff[2] = 0xFF;
                txBuff[3] = 0;

                if (RxLog != null)// && RxLog.isCaptureBackChannel(0xFF, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                {
                    var txtBuff = /*DateTime.Now + */"==>BC Broadcast CMD " + txBuff[1] + " msg ";                 // Modified by Jason chen, 2017.06.15
                    RxLog.logText(txtBuff);
                }
                GftWirelessHidHub.WriteOutput(ref txBuff);
#endif
            }
        }

        private void wirelessViewer_OnButtonStopRecordingAll(object sender, EventArgs args)
        {
            if (MessageBox.Show(new Form { TopMost = true }, "Are you sure you want to Stop Recording on all Devices?\n\nAll devices will stop recording data.",
                                    "Stop All GFTs Recording",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes)
            {
                foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
                {
                    string nodeIDs;
                    if (cur_row != null)
                        nodeIDs = cur_row.Cells[0].Value.ToString();
                    else
                        return;
                    int nodeID;

                    nodeID = Convert.ToInt16(nodeIDs);
                    lpHub.lpDevices[nodeID - 1].stopRecordingFlag = true;
                    lpHub.lpDevices[nodeID - 1].startRecordingFlag = false;
                    cmdProcess(nodeID, USB_OR_BLE);
                }
            }

        }
        private void wirelessViewer_OnButtonStartRecordingAll(object sender, EventArgs args)
        {
            if (MessageBox.Show(new Form { TopMost = true }, "Are you sure you want to Start Recording on all Devices?\n\nAny device which is currently in \"Not Recording\" mode will begin recording data.",
                                    "Start All GFTs Recording",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes)
            {
                foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
                {
                    string nodeIDs;
                    if (cur_row != null)
                        nodeIDs = cur_row.Cells[0].Value.ToString();
                    else
                        return;
                    int nodeID;

                    nodeID = Convert.ToInt16(nodeIDs);
                    lpHub.lpDevices[nodeID - 1].startRecordingFlag = true;
                    lpHub.lpDevices[nodeID - 1].stopRecordingFlag = false;
                    cmdProcess(nodeID, USB_OR_BLE);
                }
            }
        }

        private void wirelessViewer_OnMnuRemoveFromHub(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            Int16 nodeID = (byte)args.NodeID;
            if (lpHub.lpDevices[nodeID - 1].mode == 0)
            {
                // The device appears to be off. Just delete binding information from the hub

                if ((GftWirelessHidHub == null)&&(!mBLEInterface.m_bConnected)) return;    // BLE added
                if (nodeID > 0)
                {
                    DialogResult removeFromHub = MessageBox.Show(new Form { TopMost = true }, "Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT is not Powered On.\n\nIf you choose to remove it now, the Wireless functionality on the device will not be disabled.\n\nAre you sure you want to remove the GFT from the Hub?",
                                                                 "GFT is Off, remove from Hub?",
                                                                 MessageBoxButtons.YesNo);
                    if (removeFromHub == DialogResult.Yes)
                    {
                        if (GftWirelessHidHub != null)
                        {
                            byte[] txBuff = new byte[65];
                            txBuff[0] = 0x00;
                            txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                            txBuff[2] = (byte)nodeID;
                            GftWirelessHidHub.WriteOutput(ref txBuff);                    // remove binding info of dongle
                        }
                        else if(mBLEInterface.m_bConnected)                               // BLE added
                        {
                            byte[] txBuff = new byte[20];
                            txBuff[0] = MSG_HOST_TO_HUB_UNBIND_MODE;
                            txBuff[1] = (byte)nodeID;
                            mBLEInterface.SendData(ref txBuff, 20);
                        }
                        else
                        {
                            return;
                        }
                        MessageBox.Show(new Form { TopMost = true }, "Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT has been removed from the Hub");

                        if (nodeID >= 1 && nodeID <= NUM_NODES)
                        {
                            //ResetNodeControls(nodeID);
                            NodeProfile[nodeID - 1].GID = null;
                        }
                        if (lpHub != null)
                        {

                            int row = getRowNum(nodeID);                           // 2014.10.30
                            if (row >= 0)
                                wirelessViewer.dataGridView.Rows.RemoveAt(row);
                        }

                        string m_NodeID = nodeID.ToString();
                        string queryInsert = "DELETE FROM gftWireless WHERE (nodeID = @nodeID) and (MID = @dongle_mid OR MID = '' OR MID = '0000000000')";   // changed, 2014.11.11

                        using (SqlCeConnection mycon = new SqlCeConnection(conString))
                        {
                            try
                            {
                                mycon.Open();
                                using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                                {
                                    com.Parameters.AddWithValue("@nodeID", nodeID);
                                    com.Parameters.AddWithValue("@dongle_mid", dongle_mid);      // 2014.11.11

                                    com.ExecuteNonQuery();
                                    getGftWirelessAll();
                                }
                            }
                            catch (Exception er)
                            {
                            }
                            try
                            {
                                mycon.Close();
                            }
                            catch (Exception er)
                            {
                                throw er;
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "Please connect the device by USB and use the \"Unbind\" button.");
            }
        }
        private void wirelessViewer_OnMnuGetImpacts(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            lpHub.lpDevices[args.NodeID - 1].getImpactsFlag = true;
            lpHub.lpDevices[args.NodeID - 1].getImpactsSent = false;
            //NodeControl[args.NodeID - 1].setChkGetImpacts(true);
            cmdProcess(args.NodeID, USB_OR_BLE);
        }
        private void wirelessViewer_OnMnuDownload(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            wirelessViewer.dataGridView.Rows[args.selectedRow].Cells[14].Value = true;
            bool checkStatus = true;
            NodeProfile[args.NodeID - 1].uploadFlag = checkStatus;
            lpHub.lpDevices[args.NodeID - 1].upldFlag = NodeProfile[args.NodeID - 1].uploadFlag;

            NodeProfile[args.NodeID - 1].deleteFlag = false;                          // Added by Jason Chen 09.11
            NodeProfile[args.NodeID - 1].uploadNum_Flag = 0;                          // Added by Jason Chen 09.11
            NodeProfile[args.NodeID - 1].upload_req_flag = false;                     // Added by Jason Chen 09.11

            lpHub.lpDevices[args.NodeID - 1].upldFlag_num = NodeProfile[args.NodeID - 1].uploadNum_Flag;// Added by Jason Chen 09.11
            lpHub.lpDevices[args.NodeID - 1].progressValue = 0;                                         // Added by Jason Chen 09.11
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(args.NodeID); // 2014.10.01
#else
            this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), args.NodeID);
#endif
        }
        private void wirelessViewer_OnMnuPowerOff(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            lpHub.lpDevices[args.NodeID - 1].powerOffFlag = true;
            lpHub.lpDevices[args.NodeID - 1].powerOffSent = false;
            //NodeControl[args.NodeID - 1].setChkPowerOff(true);
            lpHub.lpDevices[args.NodeID - 1].keepPowerOnFlag = false;
            this.powerOnGFTs.BackColor = Color.LightGreen;
            cmdProcess(args.NodeID, USB_OR_BLE);
        }
        private void wirelessViewer_OnMnuPowerOn(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            lpHub.lpDevices[args.NodeID - 1].powerOnFlag = true;
            lpHub.lpDevices[args.NodeID - 1].powerOnSent = false;
            //NodeControl[args.NodeID - 1].setChkPowerOn(true);
            lpHub.lpDevices[args.NodeID - 1].keepPowerOnFlag = true;
            cmdProcess(args.NodeID, USB_OR_BLE);
        }
        private void wirelessViewer_OnMnuStopRecording(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            lpHub.lpDevices[args.NodeID - 1].stopRecordingFlag = true;
            lpHub.lpDevices[args.NodeID - 1].startRecordingFlag = false;
           // NodeControl[args.NodeID - 1].setChkStopRecording(true);
            cmdProcess(args.NodeID, USB_OR_BLE);
        }
        private void wirelessViewer_OnMnuStartRecording(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            lpHub.lpDevices[args.NodeID - 1].stopRecordingFlag = false;
            lpHub.lpDevices[args.NodeID - 1].startRecordingFlag = true;
            // NodeControl[args.NodeID - 1].setChkStopRecording(true);
            cmdProcess(args.NodeID, USB_OR_BLE);
        }

        public void wirelessViewer_OnNewData(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            if (splitContainer3.Panel1Collapsed)
            {
                switch (NodeProfile[args.NodeID - 1].gftType)
                {
                    case 0:
                        splitContainer4.Panel2Collapsed = true;
                        mWirelessViewer_OnRowSelectedByList(sender, args, null, dataGridViewPlayer, chartViewerPlayer); //
                        break;
                    case 1:
                        splitContainer4.Panel1Collapsed = true;
                        mWirelessViewer_OnRowSelectedByList(sender, args, null, dataGridViewPlayer, perfChartViewerPlayer); //
                        break;
                }
                //mWirelessViewer_OnRowSelectedByList(sender, args, null/*dataGridView1*/, dataGridViewPlayer, chartViewerPlayer); //
            }
            else
            {
                mWirelessViewer_OnRowSelectedByList(sender, args, null/*dataGridView1*/, dataGridView3, teamChart); //
            }
        }

        public void wirelessViewer_OnRowSelected(object sender, WirelessGFTViewer.NodeOnSelectedArgs args)
        {
            lastNodeSelected = args;
#if false
            if (refreshForList.Checked)
            {

                dataGetImpactByList(currentNodeID, dataGridView3);
                drawChartByList(currentNodeID, NodeProfile[currentNodeID - 1].alarmThres, chartViewer);

                if (Args[currentNodeID - 1].recvFIFO.Count > 0)  // Fixed by Jason Chen, 2014.01.10
                    Impacts_MouseToFirst(sender, dataGridView3, chartRawLinW, chartRawGyroW, pictureBoxHeadW, "EventsWireless");
            }
            //#else
            else
            {
                curSessionID = gDataGetSessionLastSID(currentNodeID);
                dataGetImpact(curSessionID, dataGridView3, "EventsWireless");
                drawChart(curSessionID, gDataGetSessionSidThreshold(curSessionID, "SessionWireless"), chartViewer, "EventsWireless");
            }
            //#endif

            //chartRawLinW.Series.Clear();
            //chartRawGyroW.Series.Clear();

            LastSessionID = curSessionID;
            getGftWireless(currentNodeID);    //setup NodeProfile[currentNodeID-1].alarmThres Value
#endif
            // Enable / Disable the Unbind button
            if ((byte)args.NodeID == gft_node)
            {
                m_button_Clone.Enabled = true;
                this.splitContainer1.Panel2Collapsed = false;
            }
            else
            {
                m_button_Clone.Enabled = false;
                this.splitContainer1.Panel2Collapsed = true;
            }

            // Update proximity checkbox with the selected GFT's proximity status
            if ((NodeProfile[args.NodeID - 1].AlarmMode & 0x02) == 0x02)
            {
                // Proximity is enabled
                //mProximityEnable.Checked = true;
            }
            else
            {
                //mProximityEnable.Checked = false;
            }

#if ENABLE_DASHBOARD
            if (args.selectedColumn != 14) // Changes from 5 to 1 by Jared Stock, 2014.06.25 // Changed from 1 to != 10 by Jared Stock, 2014.07.07
#else
            if (args.selectedColumn != 10) // Changes from 5 to 1 by Jared Stock, 2014.06.25 // Changed from 1 to != 10 by Jared Stock, 2014.07.07
#endif
            {
                refreshForList = true;
                if (args.NodeID == 0)
                    return;

                int impact = 0;
                if (args.selectedColumn == 4)
                    impact = lpHub.lpDevices[args.NodeID - 1].highestGImpact;
                else if (args.selectedColumn == 5)
                    impact = lpHub.lpDevices[args.NodeID - 1].highestRotImpact;
                else if (args.selectedColumn == 6)
                    impact = lpHub.lpDevices[args.NodeID - 1].lastAlarmImpact;

                if (Args[args.NodeID - 1].recvFIFO.Count <= impact) impact = 0; // something went wrong...
                List_or_Table = true;
#if true//USE_OF_LIST
#if ENABLE_DASHBOARD
                splitContainer3.Panel1Collapsed = true; // Collapse Panel 1(Team View) this will expand Panel 2(Player View)
                //lpHub.lpDevices[args.NodeID - 1].reviewRed = false;
                //lpHub.lpDevices[args.NodeID - 1].reviewYellow = false;
                lpHub.dgvLpPopulateView(args.NodeID);
                //wirelessViewer.dataGridView.Refresh();

                switch (NodeProfile[args.NodeID - 1].gftType)
                {
                    case 0:
                        splitContainer4.Panel2Collapsed = true;
                        mWirelessViewer_OnRowSelectedByList(sender, args, null/*dataGridView1*/, dataGridViewPlayer, chartViewerPlayer, impact);
                        break;
                    case 1:
                        splitContainer4.Panel1Collapsed = true;
                        //mWirelessViewer_OnRowSelectedByList(sender, args, null, dataGridViewPlayer, perfChartViewerPlayer); //
                        mWirelessViewer_OnRowSelectedByList(sender, args, null/*dataGridView1*/, new DataGridView(), perfChartViewerPlayer, impact);
                        break;
                }
                
#else
                mWirelessViewer_OnRowSelectedByList(sender, args, null/*dataGridView1*/, dataGridView3, chartViewer, impact); //
#endif
#else
                //mWirelessViewer_OnRowSelected(sender, args, null/*dataGridView1*/, dataGridView3, chartViewer);

#endif
                //tabControl4.SelectTab(tabPage17);                     // Removed by Jared Stock, 2014.06.25
                //getGftWireless(args.NodeID);                           // Removed by Jason Chen, 2014.05.15
                //UpdateNodeControlLayout(args.NodeID);

                //NodeControl[args.NodeID - 1].PlayerNumber = NodeProfile[args.NodeID - 1].playerNum;
                //NodeControl[args.NodeID - 1].UserName = NodeProfile[args.NodeID - 1].firstName + " " + NodeProfile[args.NodeID - 1].lastName;

                if (args.selectedColumn == 1) // Moved to this block by Jared Stock, 2014.07.07
                {
                    //byte msg = (byte)(0xC0 | args.NodeID);
                    mSendMsgCMD_Queue((byte)args.NodeID, 0xC0, USB_OR_BLE);
                }
            }
            else if (args.selectedColumn == 2)
            {
                refreshForList = false;
                currentNodeID = args.NodeID;             // Current UID (Node ID

                curSessionID = gDataGetSessionLastSID(currentNodeID);
                dataGetImpact(curSessionID, dataGridView3, "EventsWireless");
                drawChart(curSessionID, gDataGetSessionSidThreshold(curSessionID, "SessionWireless"), chartViewer, "EventsWireless");

                chartRawLinW.Series.Clear();
                chartRawGyroW.Series.Clear();

               // Impacts_MouseToFirst(sender, dataGridView3, chartRawLinW, chartRawGyroW, pictureBoxHeadW, "EventsWireless");

                LastSessionID = curSessionID;
                getGftWireless(currentNodeID);    //setup NodeProfile[currentNodeID-1].alarmThres Value

               // tabControl4.SelectTab(tabPage17);   // Removed by Jared Stock, 2014.06.25
                //getGftWireless(args.NodeID);                           // Removed by Jason Chen, 2014.05.15
               // UpdateNodeControlLayout(args.NodeID);
            }

#if ENABLE_DASHBOARD
            else if (/*(args.selectedColumn == 6) || (args.selectedColumn == 7) || */(args.selectedColumn == 14))
#else
            else if (/*(args.selectedColumn == 6) || (args.selectedColumn == 7) || */(args.selectedColumn == 10))
#endif
            {
                if (args.NodeID == 0)
                    return;

                bool checkStatus = Convert.ToBoolean(wirelessViewer.dataGridView.Rows[args.selectedRow].Cells[args.selectedColumn].Value);
                NodeProfile[args.NodeID - 1].uploadFlag = checkStatus;
                lpHub.lpDevices[args.NodeID - 1].upldFlag = NodeProfile[args.NodeID - 1].uploadFlag;

                NodeProfile[args.NodeID - 1].deleteFlag = false;                          // Added by Jason Chen 09.11
                NodeProfile[args.NodeID - 1].uploadNum_Flag = 0;                          // Added by Jason Chen 09.11
                NodeProfile[args.NodeID - 1].upload_req_flag = false;                     // Added by Jason Chen 09.11

                lpHub.lpDevices[args.NodeID - 1].upldFlag_num = NodeProfile[args.NodeID - 1].uploadNum_Flag;// Added by Jason Chen 09.11
                lpHub.lpDevices[args.NodeID - 1].progressValue = 0;                                         // Added by Jason Chen 09.11
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(args.NodeID); // 2014.10.01
#else
                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), args.NodeID);
#endif
 
               // lpHub.dgvLpPopulateView(args.NodeID); // 2014.10.01
               // lpHub.lpDevices[args.NodeID-1].updateDgv = true;

 
            }
            else if (args.selectedColumn == 1)
            {
                //byte msg = (byte)(0xC0 | args.NodeID);
                mSendMsgCMD_Queue((byte)args.NodeID, 0xC0, USB_OR_BLE);
            }
            else
                return;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool CloseWindow = false;
            bool DataOnDevices = false;
            int nodeID;
            string dataMessage = "";
            string batMessage = "";
            if(!systemShutdown) {
                if (NodeProfile.Count > 0)
                {
                    for (nodeID = 1; nodeID < NUM_NODES; nodeID++)
                    {
                        if(NodeProfile[nodeID - 1].GID != null) {
                            if (NodeProfile[nodeID - 1].newDataOnDevice) //NodeProfile[nodeID - 1].newDataOnDevice  || NodeProfile[nodeID - 1].impactNum > 0
                            {
                                DataOnDevices = true;
                                dataMessage += "\r\n#" + NodeProfile[nodeID - 1].playerNum + " - " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                            }

                            if (NodeProfile[nodeID - 1].bat_level < 40)
                            {
                                batMessage += "\r\n#" + NodeProfile[nodeID - 1].playerNum + " - " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT is at " + NodeProfile[nodeID - 1].bat_level + "% Battery and should be Charged.";
                            }
                        }
                    }
                }
                if (DataOnDevices)
                {
                    switch (MessageBox.Show(new Form { TopMost = true },"There is still data on some devices which has not been retrieved.\nYou may retrieve the data later by plugging the device in with the USB cable, or you may retrieve it wirelessly now.\n\nDo you wish to retreive the data wirelessly?\nDevices:" + dataMessage,
                                        "Data Still On Devices",
                                        MessageBoxButtons.YesNoCancel,
                                        MessageBoxIcon.Information))
                    {
                        case DialogResult.Yes:
                            e.Cancel = true;
                            wirelessViewer_OnButtonDevicesWithData(this, null);
                            break;
                        case DialogResult.No:
                            CloseWindow = true;
                            break;
                        case DialogResult.Cancel:
                            e.Cancel = true;
                            break;

                    }
                }
                if (batMessage != "")
                {
                    MessageBox.Show(new Form { TopMost = true }, "Please ensure that you charge the following devices before your next Game or Practice." + batMessage,
                                        "Device Battery Warning",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                }
            }
            if (CloseWindow)
            {

                // DeleteAllUsersData();

                if (GftWirelessHidHub != null)
                {
                    //GftWirelessHidHub.OnRxPacketReceived -= new HIDInputEventHandler(WirelessHidHub_OnRxPacketReceived); 
                    GftWirelessHidHub.OnRxPacketReceived -= WirelessHidHub_OnRxPacketReceived;
                    Thread.Sleep(50);
                    GftWirelessHidHub.Dispose();
                    //GftWirelessHidHub.mFileHandleValid;
                }
                //Thread.Sleep(50);
                //if (mycon.State != ConnectionState.Closed)
                //{
                //    try
                //    {
                //        mycon.Close();
                //    }
                //    catch (Exception me)
                //    {
                //        Console.WriteLine(me.ToString());
                //    }
                //}

                //Console.WriteLine(e.ToString());

                // Remove any unfinished Downloads
                gDataWSessionRemoveIncomplete();
            }
        }

#if false
        private void tabControl4_DrawItem1(object sender, DrawItemEventArgs e)
        {
            TabPage CurrentTab = tabControl4.TabPages[e.Index];
            Rectangle ItemRect = tabControl4.GetTabRect(e.Index);
            SolidBrush FillBrush = new SolidBrush(Color.Red);
            SolidBrush TextBrush = new SolidBrush(Color.White);
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            //If we are currently painting the Selected TabItem we'll
            //change the brush colors and inflate the rectangle.
            if (System.Convert.ToBoolean(e.State & DrawItemState.Selected))
            {
                FillBrush.Color = Color.White;
                TextBrush.Color = Color.Red;
                ItemRect.Inflate(2, 2);
            }

            //Set up rotation for left and right aligned tabs
            if (tabControl4.Alignment == TabAlignment.Left || tabControl4.Alignment == TabAlignment.Right)
            {
                float RotateAngle = 90;
                if (tabControl4.Alignment == TabAlignment.Left)
                    RotateAngle = 270;
                PointF cp = new PointF(ItemRect.Left + (ItemRect.Width / 2), ItemRect.Top + (ItemRect.Height / 2));
                e.Graphics.TranslateTransform(cp.X, cp.Y);
                e.Graphics.RotateTransform(RotateAngle);
                ItemRect = new Rectangle(-(ItemRect.Height / 2), -(ItemRect.Width / 2), ItemRect.Height, ItemRect.Width);
            }

            //Next we'll paint the TabItem with our Fill Brush
            e.Graphics.FillRectangle(FillBrush, ItemRect);

            //Now draw the text.
            e.Graphics.DrawString(CurrentTab.Text, e.Font, TextBrush, (RectangleF)ItemRect, sf);

            //Reset any Graphics rotation
            e.Graphics.ResetTransform();

            //Finally, we should Dispose of our brushes.
            FillBrush.Dispose();
            TextBrush.Dispose();
        }

        private void tabControl4_DrawItem(object sender, DrawItemEventArgs e)
        {

            Graphics g = e.Graphics;
            Brush _textBrush;

            System.Drawing.Image image;

            try
            {
                //if (tabControl4.TabPages.Contains(tabPage16))
                if (TooltabPage16Exist)
                {
                    if (e.Index == 0)
                        image = imageList1.Images[2];
                    else if (e.Index == 1)
                        image = imageList1.Images[14];
                    else if (e.Index == 2)
                        image = imageList1.Images[11];
                    else
                        return;
                }
                else
                {
                    if (e.Index == 0) image = imageList1.Images[2];
                    else if (e.Index == 1) image = imageList1.Images[14];
                    else
                        return;
                }


                // Get the item from the collection.
                TabPage _tabPage = tabControl4.TabPages[e.Index];

                // Get the real bounds for the tab rectangle.
                Rectangle _tabBounds = tabControl4.GetTabRect(e.Index);

                int buttom;
                //if (tabControl4.TabPages.Contains(tabPage16)) 
                if (TooltabPage16Exist)
                    buttom = 3 * _tabBounds.Height;
                else buttom = 2 * _tabBounds.Height;


                int height = tabControl4.Size.Height - buttom;
                Rectangle rect0 = new Rectangle(_tabBounds.X, buttom, e.Bounds.Size.Width + 3, height);
                g.FillRectangle(Brushes.Silver, rect0);

                buttom = tabControl4.Bottom;
                Rectangle rect1 = new Rectangle(_tabBounds.X + 5, _tabBounds.Y + (_tabBounds.Height / 2) - 24 + 10, 32, 32);
                if (e.State == DrawItemState.Selected)
                {
                    // Draw a different background color, and don't paint a focus rectangle.
                    _textBrush = new SolidBrush(Color.Red);
                    g.FillRectangle(Brushes.LightBlue, e.Bounds);
                }
                else
                {
                    _textBrush = new SolidBrush(e.ForeColor);
                    g.FillRectangle(Brushes.AliceBlue, e.Bounds);
                    //e.DrawBackground();
                }

                // Use our own font.
                Font _tabFont = new Font("Arial", (float)15.0, FontStyle.Regular, GraphicsUnit.Pixel);

                // Draw string. Center the text.
                StringFormat _stringFlags = new StringFormat();
                _stringFlags.Alignment = StringAlignment.Center;
                _stringFlags.LineAlignment = StringAlignment.Center;

                g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
                g.DrawImage(image, rect1);

                //gForce Picture
                //System.Drawing.Image image1;
                //image = wirelessViewer.pictureBox2.Image;
                //Rectangle rect2 = new Rectangle(_tabBounds.X + 30, buttom - 190, 131, 67);
                //g.DrawImage(image, rect2);

                //image = buttonBattery.Image;
                //image = buttonBattery.Image;
                //rect = new Rectangle(_tabBounds.X + 30, buttom - 90, 41, 62);
                //g.DrawImage(image, rect);

                _textBrush = new SolidBrush(Color.Black);

                _stringFlags.Alignment = StringAlignment.Near;
                _stringFlags.LineAlignment = StringAlignment.Center;
                Rectangle rect3 = new Rectangle(_tabBounds.X + 30, buttom - 215, 200, 30);
                g.DrawString("          ", _tabFont, _textBrush, rect3, new StringFormat(_stringFlags));

                //Font _tabFont1 = new Font("Arial", (float)13.0, FontStyle.Regular, GraphicsUnit.Pixel);
                //rect = new Rectangle(_tabBounds.X + 30, buttom - 115, 200, 30);
                //g.DrawString(labelGftVersion.Text, _tabFont1, _textBrush, rect, new StringFormat(_stringFlags));
            }
            catch (Exception xxx)
            {
                string er = xxx.ToString();
                if (TooltabPage16Exist)
                {
                    //m_Edit_Immediate.AppendText(er + "\r\n");
                }
                return;
            }
        }

        private void tabControl4_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rect0;
            Rectangle rect4;

            //if (tabControl4.TabPages.Contains(tabPage16))
            if(TooltabPage16Exist)
            {
                rect0 = tabControl4.GetTabRect(0);
                rect4 = tabControl4.GetTabRect(2);
            }
            else
            {
                rect0 = tabControl4.GetTabRect(0);
                rect4 = tabControl4.GetTabRect(1);
            }

            Point point = e.Location;

            if ((point.X > rect0.X && point.X < rect0.X + rect0.Width) && (point.Y > rect0.Y && point.Y < rect4.Y + rect4.Height))
            {
                tabControl4.Cursor = Cursors.Cross;// Hand;
            }
        }
#endif
        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            //labelLoginUser.Location = new Point((panelLogin.Width) / 2 - (labelLoginUser.Width) / 2, (panelLogin.Height) / 2 - (labelLoginUser.Height) / 2);
            //pictureBox2.Location = new Point(labelLoginUser.Location.X, labelLoginUser.Location.Y - pictureBox2.Height);

            //mGFT.Location = new Point(this.Size.Width - mGFT.Size.Width-20, mGFT.Location.Y);
#if false
            bool visible_t = m_prog_progress.Visible;
            m_prog_progress.Visible = true;
            int group_width = groupBox6.Size.Width;
            group_width = group_width - m_prog_progress.Location.X;
            Size size = new Size();
            size.Height = m_prog_progress.Size.Height;
            size.Width = group_width - 10;
            m_prog_progress.Size = size;
            m_prog_progress.Visible = visible_t;
#endif
#if ENABLE_DASHBOARD
            if (splitContainer3.Panel1Collapsed)
            {
                switch(NodeProfile[lastNodeSelected.NodeID - 1].gftType) {
                    case 0:
                        splitContainer4.Panel2Collapsed = true;
                        mWirelessViewer_OnRowSelectedByList(sender, lastNodeSelected, null, dataGridViewPlayer, chartViewerPlayer); //
                        break;
                    case 1:
                        splitContainer4.Panel1Collapsed = true;
                        mWirelessViewer_OnRowSelectedByList(sender, lastNodeSelected, null, dataGridViewPlayer, perfChartViewerPlayer); //
                        break;
                }
            }
#endif
        }

        private void chartViewer_MouseMove(object sender, MouseEventArgs e)
        {
#if ENABLE_DASHBOARD
            if (splitContainer3.Panel1Collapsed)
            {
                switch (NodeProfile[lastNodeSelected.NodeID - 1].gftType)
                {
                    case 0:
                        splitContainer4.Panel2Collapsed = true;
                        Impacts_MouseMove(sender, e, dataGridViewPlayer, chartRawLinW, chartRawGyroW, pbHead, "EventsWireless");
                        break;
                    case 1:
                        splitContainer4.Panel1Collapsed = true;
                        Impacts_MouseMove(sender, e, new DataGridView(), chartRawLinW, chartRawGyroW, pbPerfHead, "EventsWireless");
                        break;
                }
            }
            else
            {
                Impacts_MouseMove(sender, e, dataGridView3, chartRawLinW, chartRawGyroW, pbHead, "EventsWireless");
            }
#else
            Impacts_MouseMove(sender, e, dataGridView3, chartRawLinW, chartRawGyroW, pictureBoxHeadW, "EventsWireless");
#endif
        }

        private void teamChart_MouseMove(object sender, MouseEventArgs e)
        {
#if ENABLE_DASHBOARD
            TeamImpacts_MouseMove(sender, e, dataGridViewPlayer, chartRawLinW, chartRawGyroW, pbHead, "EventsWireless");
#endif
        }



        private void teamChart_mouseClick(object sender, MouseEventArgs e)
        {
#if ENABLE_DASHBOARD
            TeamImpacts_Click(sender, e, dataGridViewPlayer, chartRawLinW, chartRawGyroW, pbHead, "EventsWireless");

#endif
        }



        public void mtimer_Disable()
        {
            this.mtimer.Enabled = false;
        }
        public void show_device_receiving(int node)
        {
            lpHub.lpDevices[node - 1].lastReceiveTime = DateTime.UtcNow;
            //lpHub.updateDGView(node - 1);  // 2014.10.01
           // lpHub.lpDevices[node - 1].updateDgv = true;
            //wirelessViewer.show_device_receiving(node);
        }
        private void mtimer_Tick(object sender, EventArgs e)
        {
            if(gForceHid != null)
                this.BeginInvoke((MethodInvoker)delegate { mBatLVL_Click(sender, e); });//m_button_NodeInfo_Click(this, null); });                      
        }

        private void gftGMT_Enable_CheckStateChanged(object sender, EventArgs e)
        {
            Settings.Default.GFT_GMT_Enable = gftGMT_Enable;
            Settings.Default.Save();
        }

        private void autoUpload_enable_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.AUTO_UPLOAD_Enable = autoUpload_enable.Checked;
            Settings.Default.Save();

            //m_NumUpload.Enabled = !autoUpload_enable.Checked;
            NUM_UPLOAD_LIMIT = Convert.ToInt16(simUploads.Text);            // 2014.10.31
        }

        private void refreshForList_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.ReFresh_for_List = refreshForList;
            Settings.Default.Save();
        }

        private void usbUpload_Enable_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.usbUpload_Enable = usbUpload_Enable.Checked;
            Settings.Default.Save();
        }

       // private void m_NumUpload_ValueChanged(object sender, EventArgs e)
       // {
            //NUM_UPLOAD_LIMIT = Convert.ToInt16(m_NumUpload.Text);
       // }


        private void m_button_Upload_Click(object sender, EventArgs e)
        {
            DeleteAllUsersData();
        }

        private void m_Button_hubInfo_Click(object sender, EventArgs e)
        {
            if (GftWirelessHidHub == null) return;

            byte[] txBuff = new byte[65];
            //txBuff[0] = 0;
            txBuff[1] = MSG_HOST_TO_HUB_GET_INFO;
            txBuff[2] = 0;
            GftWirelessHidHub.WriteOutput(ref txBuff);

            SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            //Thread.Sleep(10);
        }

        private void m_button_NodeInfo_Click(object sender, EventArgs e)
        {
            if (GftWirelessHidHub == null) return;

            byte[] txBuff = new byte[65];
            txBuff[1] = MSG_HOST_TO_HUB_NODE_INFO;
            txBuff[2] = 0;
            GftWirelessHidHub.WriteOutput(ref txBuff);

            SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            //Thread.Sleep(10);
        }

        private void m_button_Reset_Click(object sender, EventArgs e)
        {
            if (GftWirelessHidHub == null) return;

            byte[] txBuff = new byte[65];
            txBuff[1] = MSG_HOST_TO_HUB_RESET;
            txBuff[2] = 0;
            GftWirelessHidHub.WriteOutput(ref txBuff);
            //Thread.Sleep(10);
        }

        private void m_button_ResetBLE_Click(object sender, EventArgs e)
        {
            if (!mBLEInterface.m_bConnected) return;

            byte[] txBuff = new byte[20];
            txBuff[0] = MSG_HOST_TO_HUB_RESET;
            txBuff[1] = 0;

            m_receivedBytes = 0;
            //mBLEInterface.Data_SetDescriptors(1, 0);
            //mBLEInterface.SendCommand(1, 0);         //(byte)NumFramesBeforeAck);

            mBLEInterface.SendData(ref txBuff, 20);

            int wait_count = 0;
            for (;;)
            {

                if (!oSignalEventBle.WaitOne(10))
                {
                    //ods("Ack failed\n");
                    wait_count++;
                    if (wait_count > 100)
                    {
                        wait_count = 1000;
                        break;
                    }
                    else
                        Application.DoEvents();
                }
                else
                {
                    oSignalEventBle.Reset();
                    break;
                }
            }
            if (wait_count == 1000)
            {
                ;// mReceivedBytes.AppendText("Data Sending fail.....\r\n");
            }

            //GftWirelessHidHub.WriteOutput(ref txBuff);
            //Thread.Sleep(10);
        }
        private void m_button_Sync_Click(object sender, EventArgs e)
        {
#if false
            if (GftWirelessHidHub == null) return;

            DataGridViewRow cur_row = gftWirelessView.CurrentRow;
            string nodeIDs;
            if (cur_row != null)
                nodeIDs = cur_row.Cells[1].Value.ToString();
            else
                return;

            byte[] txBuff = new byte[65];
            //            txBuff[1] = MSG_HOST_TO_HUB_SYNC;
            //            txBuff[2] = 0;
            //            GftWirelessHidHub.WriteOutput(ref txBuff);

            txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
            //txBuff[2] = (byte)Convert.ToInt16(nodeIDs);
            txBuff[2] = (byte)Convert.ToInt16(mUnbindNum.Text);
            GftWirelessHidHub.WriteOutput(ref txBuff);
            SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));

            //Thread.Sleep(10);
#endif
        }

        private void m_button_Bind_Click(object sender, EventArgs e)
        {

            //setDeviceForWireless(); // Enable wireless on the device                             // 2014.10.31  removed
            if (gft_node > 0)
            {
                //this.BeginInvoke((MethodInvoker)delegate { mUnbind_Click(this, null); });
                //Thread.Sleep(1000);
            }
#if false
            if (GftWirelessHidHub == null) return;

            byte[] txBuff = new byte[65];
            txBuff[1] = MSG_HOST_TO_HUB_BIND_MODE;
            txBuff[2] = 0;
            GftWirelessHidHub.WriteOutput(ref txBuff);
            SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            //Thread.Sleep(10);
#else
            if (GftWirelessHidHub != null)
            {
                byte[] txBuff = new byte[65];
                txBuff[1] = MSG_HOST_TO_HUB_BIND_MODE;
                txBuff[2] = 0;
                GftWirelessHidHub.WriteOutput(ref txBuff);
                SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            }
            else if(mBLEInterface.m_bConnected)                      // BLE added
            {
                byte[] txBuff = new byte[20];
                txBuff[0] = MSG_HOST_TO_HUB_BIND_MODE;
                txBuff[1] = 0;
                mBLEInterface.SendData(ref txBuff, 20);

                SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            }
            
            //Thread.Sleep(10);
#endif
        }


        private void mSendMsgCMD(byte nodeID, byte Msg)
        {
            //int numSend;
            if (GftWirelessHidHub == null) return;

            byte[] txBuff = new byte[65];
            txBuff[1] = MSG_HOST_TO_HUB_BCD_CMD;
            txBuff[2] = nodeID;
            txBuff[3] = Msg;
            // Commented by Jason Chen, 2017.03.28
            //System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
            //if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
            //{
            //    var txtBuff = DateTime.Now + " BC: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Sent";
            //    RxLog.logText(txtBuff);
            //}
            //if (NodeProfile[(int)nodeID - 1].usbMode == 0x81)
            {
                try
                {
//#if false            //2014.10.28         
                    if (mEraseWholeData.Checked)
                    {
                        GftWirelessHidHub.WriteOutput(ref txBuff);
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////
                        // Added by Jason Chen, 2017.03.28
                        System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
                        if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            var txtBuff = /*DateTime.Now*/"<<========BC: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Sent";
                            RxLog.logText(txtBuff);
                        }
                    }
                    // Added by Jason Chen, 2017.03.28
                    //////////////////////////////////////////////////
                    else
                    {
                        // 2017.04.04, added for testing
                        if (txBuff[3] != 191)   //0xEE                         // 09.11 for testing
                        {
                            GftWirelessHidHub.WriteOutput(ref txBuff);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            // Added by Jason Chen, 2017.03.28
                            //System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
                            if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = /*DateTime.Now*/"<<========BC: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Sent";
                                RxLog.logText(txtBuff);
                            }
                            // Added by Jason Chen, 2017.03.28
                            //////////////////////////////////////////////////
                        }
                        else
                        {
                            //NodeProfile[nodeID - 1].deleteFlag = false;        // 09.11 for testing
                            //NodeProfile[nodeID - 1].deleteFlag = true;         // 2014.10.27 for testing
                            NodeProfile[nodeID - 1].uploadFlag = false;        // 09.11 for testing
                            NodeProfile[nodeID - 1].uploadNum_Flag = 0;        // 09.11 for testing
                            lpHub.lpDevices[nodeID - 1].upldFlag_num = 0;      // 09.11 for testing
                            lpHub.lpDevices[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;// 09.11 for testing

                            txBuff[3] = 22;                                    // 09.11 , power off for testing
                            GftWirelessHidHub.WriteOutput(ref txBuff);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            // Added by Jason Chen, 2017.03.28
                            //System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
                            if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = /*DateTime.Now*/"<<============================BC: node " + nodeID + " : cmd 22 Sent, Don't erase data for Testing";
                                RxLog.logText(txtBuff);
                            }
                            // Added by Jason Chen, 2017.03.28
                            //////////////////////////////////////////////////
                        }
                    }
//#endif     
#if false
                    else
                    {
                        ///////////////////////////////////////////////////////                          // Added by Jason Chen, 2014.05.12
                        txBuff[3] = 21;                // reboot gft                    
                        NodeProfile[nodeID - 1].deleteFlag = false;
                        NodeProfile[nodeID - 1].uploadFlag = false;
                        lpHub.[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;
                        lpHub.dgvLpPopulateView(nodeID);
                        GftWirelessHidHub.WriteOutput(ref txBuff);
                        ///////////////////////////////////////////////////////                          // Added by Jason Chen, 2014.05.12 for testing
                    }
#endif
                    if ((txBuff[3] == 16) || (txBuff[3] == 15))
                        SetTimer(this.Handle, 0x160 + nodeID, 5000, null);                    //new TimerEventHandler(timerCallBack));
                    //else
                    //{
                        //if ((txBuff[3] >= 193) && (txBuff[3] <= 255))
                        //{ }
                        //else
                        //    SetTimer(this.Handle, 0x120 + nodeID, 3000, null);                //new TimerEventHandler(timerCallBack));
                    //}
                }
                catch (Exception xx)
                {
                    if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        var txtBuff = DateTime.Now + " BC: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " " + xx.Message;
                        RxLog.logText(txtBuff);
                    }
                }
            }
        }

        private void mSendMsgBleCMD(byte nodeID, byte Msg)
        {
            if (!mBLEInterface.m_bConnected) return;

            byte[] txBuff = new byte[20];
            txBuff[0] = MSG_HOST_TO_HUB_BCD_CMD;
            txBuff[1] = nodeID;
            txBuff[2] = Msg;
            // Commented by Jason Chen, 2017.03.28
            //System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
            //if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
            //{
            //    var txtBuff = DateTime.Now + " BC: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Sent";
            //    RxLog.logText(txtBuff);
            //}
            //if (NodeProfile[(int)nodeID - 1].usbMode == 0x81)
            {
                try
                {
                    //#if false            //2014.10.28         
                    if (mEraseWholeData.Checked)
                    {
                        mBLEInterface.SendData(ref txBuff, 20);
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////
                        // Added by Jason Chen, 2017.03.28
                        System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
                        if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            var txtBuff = /*DateTime.Now*/"<<========BC: node " + nodeID + " cmd " + txBuff[0] + " msg " + txBuff[2] + " Sent";
                            RxLog.logText(txtBuff);
                        }
                    }
                    // Added by Jason Chen, 2017.03.28
                    //////////////////////////////////////////////////
                    else
                    {
                        // 2017.04.04, added for testing
                        if (txBuff[2] != 191)   //0xEE                         // 09.11 for testing
                        {
                            mBLEInterface.SendData(ref txBuff, 20);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            // Added by Jason Chen, 2017.03.28
                            //System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
                            if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = /*DateTime.Now*/"<<========BC: node " + nodeID + " cmd " + txBuff[0] + " msg " + txBuff[2] + " Sent";
                                RxLog.logText(txtBuff);
                            }
                            // Added by Jason Chen, 2017.03.28
                            //////////////////////////////////////////////////
                        }
                        else
                        {
                            //NodeProfile[nodeID - 1].deleteFlag = false;        // 09.11 for testing
                            //NodeProfile[nodeID - 1].deleteFlag = true;         // 2014.10.27 for testing
                            NodeProfile[nodeID - 1].uploadFlag = false;        // 09.11 for testing
                            NodeProfile[nodeID - 1].uploadNum_Flag = 0;        // 09.11 for testing
                            lpHub.lpDevices[nodeID - 1].upldFlag_num = 0;      // 09.11 for testing
                            lpHub.lpDevices[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;// 09.11 for testing

                            txBuff[2] = 22;                                    // 09.11 , power off for testing
                            mBLEInterface.SendData(ref txBuff, 20);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            // Added by Jason Chen, 2017.03.28
                            //System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);
                            if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = /*DateTime.Now*/"<<============================BC: node " + nodeID + " : cmd 22 Sent, Don't erase data for Testing";
                                RxLog.logText(txtBuff);
                            }
                            // Added by Jason Chen, 2017.03.28
                            //////////////////////////////////////////////////
                        }
                    }
                    //#endif     
#if false
                    else
                    {
                        ///////////////////////////////////////////////////////                          // Added by Jason Chen, 2014.05.12
                        txBuff[3] = 21;                // reboot gft                    
                        NodeProfile[nodeID - 1].deleteFlag = false;
                        NodeProfile[nodeID - 1].uploadFlag = false;
                        lpHub.[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;
                        lpHub.dgvLpPopulateView(nodeID);
                        GftWirelessHidHub.WriteOutput(ref txBuff);
                        ///////////////////////////////////////////////////////                          // Added by Jason Chen, 2014.05.12 for testing
                    }
#endif
                    if ((txBuff[2] == 16) || (txBuff[2] == 15))
                        SetTimer(this.Handle, 0x160 + nodeID, 5000, null);                    //new TimerEventHandler(timerCallBack));
                                                                                              //else
                                                                                              //{
                                                                                              //if ((txBuff[3] >= 193) && (txBuff[3] <= 255))
                                                                                              //{ }
                                                                                              //else
                                                                                              //    SetTimer(this.Handle, 0x120 + nodeID, 3000, null);                //new TimerEventHandler(timerCallBack));
                                                                                              //}
                }
                catch (Exception xx)
                {
                    if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        var txtBuff = DateTime.Now + " BC: node " + nodeID + " cmd " + txBuff[0] + " msg " + txBuff[2] + " " + xx.Message;
                        RxLog.logText(txtBuff);
                    }
                }
            }
        }
        private bool mSendMsgCMD_Queue(byte nodeID, byte Msg, bool USB_or_BLE, byte BCMsgType = 0)
        {
            bool returnval = true;
            //int numSend;
            if ((GftWirelessHidHub == null)&&(!mBLEInterface.m_bConnected))
                return false;

            byte[] txBuff = new byte[65];
            txBuff[1] = MSG_HOST_TO_HUB_BCD_CMD;
            txBuff[2] = nodeID;
            txBuff[3] = Msg;

            byte[] txBuffBle = new byte[20];
            txBuffBle[0] = MSG_HOST_TO_HUB_BCD_CMD;
            txBuffBle[1] = nodeID;
            txBuffBle[2] = Msg;

            System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);

            if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
            {
                var txtBuff = /*DateTime.Now + */"==>BC Queued: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Trying";              // Modified by Jason chen, 2017.03.28
                RxLog.logText(txtBuff);                 
            }
            if (NodeProfile[(int)nodeID - 1].usbMode == 0x81)
            {
                try
                {
                    if (txBuff[3] != 191)   //0xEE
                    {
                        if (!USB_or_BLE)
                            GftWirelessHidHub.WriteOutput(ref txBuff);
                        else
                            mBLEInterface.SendData(ref txBuffBle, 20);
                    }
                    else
                    {
                        ///////////////////////////////////////////////////////                          // Added by Jason Chen, 2014.05.12
                        txBuff[3] = 21;                // reboot gft                    
                        txBuffBle[2] = 21;
                        NodeProfile[nodeID - 1].deleteFlag = false;
                        NodeProfile[nodeID - 1].uploadFlag = false;
                        lpHub.lpDevices[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;
#if USE_ORIGINAL_REFRESH
                        lpHub.dgvLpPopulateView(nodeID); // 2014.10.01
#else
                        this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), nodeID);
#endif
                        // lpHub.lpDevices[nodeID-1].updateDgv = true;
                        if (!USB_or_BLE)
                            GftWirelessHidHub.WriteOutput(ref txBuff);
                        else
                            mBLEInterface.SendData(ref txBuffBle, 20);
                        ///////////////////////////////////////////////////////                          // Added by Jason Chen, 2014.05.12 for testing
                    }

                    if (!USB_or_BLE)
                    {
                        if ((txBuff[3] == 16) || (txBuff[3] == 15))
                            SetTimer(this.Handle, 0x160 + nodeID, 5000, null);                //new TimerEventHandler(timerCallBack));
                    }
                    else
                    {
                        if ((txBuff[2] == 16) || (txBuff[2] == 15))
                            SetTimer(this.Handle, 0x160 + nodeID, 5000, null);                //new TimerEventHandler(timerCallBack));
                    }

                    //else
                    //{
                    //if ((txBuff[3] >= 193) && (txBuff[3] <= 255))
                    //{ }
                    //else
                    //    SetTimer(this.Handle, 0x120 + nodeID, 3000, null);                //new TimerEventHandler(timerCallBack));
                    //}
                }
                catch (Exception xx)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                }
            }
            else
            {
                byte[] cmd_buf = new byte[2];
                cmd_buf[0] = Msg;
                if (NodeProfile[(int)nodeID - 1].Back_Command_buf.Count == 0)
                //if (!NodeProfile[(int)nodeID - 1].Back_Command_buf.Contains(cmd_buf))
                {
                    if (BCMsgType == 0xD2)
                    {
                        // Request to start time sync
                        // We need to send this immediately, cannot queue it.
                        mSendMsgData(nodeID, Msg, USB_or_BLE);
                        return true;
                    }
                    else
                    {
                        if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            var txtBuff = DateTime.Now + " BC Queued: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Put into Queue\r\n";              // Added by Jason chen, 2017.03.28
                            RxLog.logText(txtBuff);
                        }

                        NodeProfile[(int)nodeID - 1].Back_Command_buf.Enqueue(cmd_buf);
                        //if (TooltabPage16Exist)
                            //m_Edit_Immediate.AppendText("\r\n---GFT " + nodeID.ToString("D2") + " Radio off, the CMD " + Msg.ToString() + " enter Command Queue----\r\n\r\n");

                        //    SetTimer(this.Handle, 0xB00 + nodeID, 15000, null);                //new TimerEventHandler(timerCallBack));
                    }
                }
                else
                {

                    if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        var txtBuff = DateTime.Now + " BC Queued: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " No Empty Queue";              // Added by Jason chen, 2017.03.28
                        RxLog.logText(txtBuff);
                    }

                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText("\r\n---GFT " + nodeID.ToString("D2") + " Queue No Empty----\r\n\r\n");
                    returnval = false;
                }
            }
            return returnval;
        }

        private void mSendMsgData(byte nodeID, byte Msg, bool USB_or_BLE)
        {
            //int numSend;
            if ((GftWirelessHidHub == null)&&(!mBLEInterface.m_bConnected))
                return;

            byte[] txBuff = new byte[65];
            txBuff[1] = MSG_HOST_TO_HUB_BCD_CMD;
            txBuff[2] = nodeID;
            txBuff[3] = Msg;

            byte[] txBuffBle = new byte[20];
            txBuffBle[0] = MSG_HOST_TO_HUB_BCD_CMD;
            txBuffBle[1] = nodeID;
            txBuffBle[2] = Msg;

            System.Diagnostics.Debug.WriteLine("node: " + txBuff[2] + ", cmd: " + txBuff[1] + ", msg: " + txBuff[3]);


            //if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))    // Commented by Jason chen, 2017.03.28
            //{
                //var txtBuff = DateTime.Now + " BC: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Trying";
                //RxLog.logText(txtBuff);
            //}
            try
            {
                if (!USB_or_BLE)
                    GftWirelessHidHub.WriteOutput(ref txBuff);
                else
                    mBLEInterface.SendData(ref txBuffBle, 20);

                //SetTimer(this.Handle, 0x120 + nodeID, 3000, null);                //new TimerEventHandler(timerCallBack));
                if (RxLog != null && RxLog.isCaptureBackChannel(nodeID, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                {
                    var txtBuff = /*DateTime.Now + */" BC: node " + nodeID + " cmd " + txBuff[1] + " msg " + txBuff[3] + " Sent <========= Node " + nodeID;    // NModified by Jason chen, 2017.03.28
                    RxLog.logText(txtBuff);
                }
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //{
                    //m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                //}
            }
        }




        private void m_timeSync_Click(object sender, EventArgs e)
        {
#if false
            if ((DateTime.UtcNow.Second >= 0) && (DateTime.UtcNow.Second <= 50))
            {
                if (GftWirelessHidHub == null) return;

                mTimeStatus.Text = "Time Sync ...";

                DataGridViewRow cur_row = gftWirelessView.CurrentRow;
                string nodeIDs;
                if (cur_row != null)
                    nodeIDs = cur_row.Cells[1].Value.ToString();
                else
                    return;

                byte[] txBuff = new byte[2];
                txBuff[0] = (byte)Convert.ToInt16(nodeIDs/*m_Node.Text*/);
                txBuff[1] = (byte)Convert.ToInt16(m_bcdData.Text);


                mSendMsgCMD_Queue(txBuff[0], 210);                   // Intialize a time synchronization CMD, 2014.04.28
            }
#endif
        }

        private void m_eraseData_Click(object sender, EventArgs e)
        {
#if false
            byte[] txBuff = new byte[2];
            DataGridViewRow cur_row = gftWirelessView.CurrentRow;
            string nodeIDs;
            if (cur_row != null)
                nodeIDs = cur_row.Cells[1].Value.ToString();
            else
                return;
          
            txBuff[0] = (byte)Convert.ToInt16(nodeIDs);

            if ((gft_node > 0) && (gft_node == txBuff[0]))
            {
                if (gForceHid == null)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                    return;
                }

                byte[] txBuff1 = new byte[65];
                txBuff1[0] = 0x00;
                txBuff1[1] = (byte)GFT_MHID_CMD.MHID_CMD_ERASE_ACC_DATA;

                try
                {
                    gForceHid.WriteOutput(ref txBuff1);
                }
                catch (Exception xx)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                }                   
            }
            else
            {
                txBuff[1] = 191;                                       //Command
                mSendMsgCMD_Queue(txBuff[0], txBuff[1]);
            }
#endif
        }

        private void wirelessViewer_OnButtonEnableProximityAll(object sender, EventArgs e)
        {
            foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
            {
                string nodeIDs;
                if (cur_row != null)
                    nodeIDs = cur_row.Cells[0].Value.ToString();
                else
                    return;

                mSendMsgCMD_Queue((byte)Convert.ToInt16(nodeIDs), 149, USB_OR_BLE);
            }
        }
        private void wirelessViewer_OnButtonDisableProximityAll(object sender, EventArgs e)
        {
            foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
            {
                string nodeIDs;
                if (cur_row != null)
                    nodeIDs = cur_row.Cells[0].Value.ToString();
                else
                    return;

                mSendMsgCMD_Queue((byte)Convert.ToInt16(nodeIDs), 148, USB_OR_BLE);
            }
        }

        private void mProxmityEnable_CheckedChanged(object sender, EventArgs e)
        {
            if (GftWirelessHidHub == null) return;

            //if(mProximityEnable.Checked == mProximityStatus.Checked) return;

            DataGridViewRow cur_row = wirelessViewer.dataGridView.CurrentRow;
            string nodeIDs;
            if (cur_row != null)
                if (cur_row.Cells[0].Value != null)
                    nodeIDs = cur_row.Cells[0].Value.ToString();
                else
                    return;
            else
                return;

            byte[] txBuff = new byte[2];
            txBuff[0] = (byte)Convert.ToInt16(nodeIDs);
            /*if (mProximityEnable.Checked)
                txBuff[1] = 149;
            else
                txBuff[1] = 148;
            mSendMsgCMD_Queue(txBuff[0], txBuff[1]);*/
        }

        private void mPowerOff_Click_old(object sender, EventArgs e)
        {
#if false
            DataGridViewRow cur_row = gftWirelessView.CurrentRow;
            string nodeIDs;
            if (cur_row != null)
                nodeIDs = cur_row.Cells[1].Value.ToString();
            else
                return;
            int nodeID;

            nodeID = Convert.ToInt16(nodeIDs);
            mSendMsgCMD_Queue((byte)nodeID, 0x16);//22
#endif
        }
        private void mPowerOff_Click(object sender, EventArgs e)
        {

            foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
            {
                string nodeIDs;
                if (cur_row != null)
                    nodeIDs = cur_row.Cells[0].Value.ToString();
                else
                    return;
                int nodeID;

                nodeID = Convert.ToInt16(nodeIDs);
                lpHub.lpDevices[nodeID - 1].keepPowerOnFlag = false; // This flag is referenced to determine if GFT should be automatically powered on if it's seen off
                this.powerOnGFTs.BackColor = Color.LightGreen;
                //mSendMsgCMD_Queue((byte)nodeID, 0x16);//22
                queuePowerOff((byte)nodeID);
            }

        }
        private bool queuePowerOff(int nodeID) {
            NodeProfile[nodeID - 1].Back_Command_buf.Clear();
            if (mSendMsgCMD_Queue((byte)nodeID, 0x16, USB_OR_BLE))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void GftRxPacketProcess(ref byte[] buff)
        {
            int len = 0;
            if ((buff[0] & 0x80) == 0x80)
            {
                int CMD_RESP = buff[0] & 0x7F;

                string textInfo = "---USB";
                if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_LP_UNBIND)
                {
                    len = 2;
                    textInfo += "-Unbind:";

                    if (bind_with_unbind)
                    {
                        bind_with_unbind = false;
#if false
                        Thread.Sleep(750);
                        this.BeginInvoke((MethodInvoker)delegate { m_button_Bind_Click(this, null); });
#else
                        if (userProfile.lpEnable == 0) //userProfile.proxEnable == 0 ||
                        {
                            setDeviceForWireless(); // Enable wireless on the device                                 // 2014.10.31
                            bind_with_wireless_on = true;                                                               // 2014.10.31
                        }
                        else
                        {
                            bind_with_wireless_on = false;                                                            // 2014.10.31
                            Thread.Sleep(300);
                            this.BeginInvoke((MethodInvoker)delegate { m_button_Bind_Click(this, null); });           // 2014.10.31
                            return;                                                                                   // 2014.10.31
                        }
#endif
                    }
                    else
                    {

                        setDeviceForWirelessOff(); // Device unbound, disable wireless
                    }
                }
                else if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_REBOOT)
                {
                    len = 10;
                    textInfo += "-REBOOT:";
                }
                else if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_CHECKBAT)
                {
                    len = 4;
                    textInfo += "-BatLVL:";
                    if (gft_node > 0)
                    {
                        float batLvl = 0;

                        if (curGFT.firmMajor >= 4)
                        {
                            batLvl = gftBattery(2, buff[4]);
                            //batLvl = (int)(((float)(buff[4] >= 7 ? buff[4] : 7) - 7) / 93 * WirelessGFTViewer.LP_HUB.F_BATT_LEVEL_100PECENT);
                        }
                        else
                        {
                            batLvl = gftBattery(1, (int)((buff[3] * 256 + buff[2] - 45) / 0.625f));
                            //batLvl = buff[3] * 256 + buff[2] - 45;
                            //batLvl /= 0.625f;
                        }
                        NodeProfile[gft_node - 1].bat_level = (int)batLvl;
                        NodeProfile[gft_node - 1].max17047Exist = true;
                        lpHub.lpDevices[gft_node-1].max17047Exist = true;
                        lpHub.lpDevices[gft_node - 1].battLevel = (int)batLvl;
 
#if USER_ORIGINAL_REFRESH
                        lpHub.dgvLpPopulateView(gft_node); // 2014.10.01
#else
                        this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), gft_node);
#endif
 
                       // lpHub.dgvLpPopulateView(gft_node); // 2014.10.01
                       // lpHub.lpDevices[gft_node-1].updateDgv = true;
 
                    }                  
                }
                else if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_ERASE_ACC_DATA)
                {
                    len = 4;
                    textInfo += "-FlashErase:";
                }
                else if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_GET_DEV_INFO)
                {
                    len = 7;
                    textInfo += "-GftInfo:";

                    mWirelessEnable.Enabled = true;

                    if ((byte)buff[6] == 0x1E)
                    {
                        m_pStaticMode.Text/*mGFT.Text*/ = "GFT Connected" + "(" + string.Format("{0:x2}:{1:x2}Boot:{2:x2}:{3:x2}", buff[4], buff[5], buff[2], buff[3]).ToUpper() + ")";
                        m_pStaticMode.BackColor = Color.LightSkyBlue;
                        curGFT.bootMajor = (byte)buff[2];
                        curGFT.bootMinor = (byte)buff[3];
                        curGFT.hwMajor = (byte)buff[4];
                        curGFT.hwMinor = (byte)buff[5];
                        gftWhereAmI = GFT_STATE_BOOT;

                        //mUnbind.Visible = false;
                    }
                    else if ((byte)buff[6] == 0x2D)
                    {
                        toolStripStatusLabelGFT.Text = "";
                        toolStripStatusLabelGFT.BackColor = Color.Gray;

                        m_pStaticMode.Text = "GFT Connected" + "(" + string.Format("{0:x2}:{1:x2}Firm:{2:x2}:{3:x2}", buff[4], buff[5], buff[2], buff[3]).ToUpper() + ")";
                        m_pStaticMode.BackColor = Color.Lime;
                        curGFT.firmMajor = (byte)buff[2];
                        curGFT.firmMinor = (byte)buff[3];
                        curGFT.hwMajor = (byte)buff[4];
                        curGFT.hwMinor = (byte)buff[5];
                        gftWhereAmI = GFT_STATE_FIRM;

                        //pipeServer.DisposeLocalCopyOfClientHandle();
                        //pipeServer.Close();
                        this.BeginInvoke((MethodInvoker)delegate { mGetUserProfile_Click(this, null); });

                        //m_prog_progress.Visible = false;
                        //m_prog_progress.Value = 0;

                        //mUnbind.Visible = true;
                    }
                    m_pStaticMode.TextAlign = ContentAlignment.MiddleRight;//new Point(this.Size.Width - mGFT.Size.Width - 20, mGFT.Location.Y);          
                }
                else if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_GET_USR_PROFILE)
                {                    
                    len = 48;
                    textInfo  = "---USR-Profile:";
                    if (gftWhereAmI == GFT_STATE_FIRM)
                    {
                        bool deviceRegistered = true;
                        int len1 = Marshal.SizeOf(userProfile);

                        IntPtr ptr = Marshal.AllocHGlobal(len1);
                        Marshal.Copy(buff, 2, ptr, len1);
                        userProfile = (USER_PROFILE)Marshal.PtrToStructure(ptr, userProfile.GetType());
                        Marshal.FreeHGlobal(ptr);

                        string myGID = gidToString(userProfile.GID);

                        string mSerialNum = myGID;

                        //mSerialNum.Text = myGID;

                        if ((GftWirelessHidHub != null)||(mBLEInterface.m_bConnected))     // BLE Added
                        {
                            last_usb_gid = myGID;
                            gft_node = getGftWireless(myGID);
                        }
                        else
                            gft_node = 0;

                       // if (gft_node == 5)
                        //    Thread.Sleep(0);
                        string myName = System.Text.Encoding.ASCII.GetString(userProfile.name);
                        myName = getFirstBeforeZero(DELIM_ZERO, myName);

                        if (myName.Trim() == "" || myName.Trim().ToUpper() == "GFT") deviceRegistered = false; // Device name indicates it has not been registered or does not contain a valid profile

                        int size_t = myName.Split(DELIM_SPACE, StringSplitOptions.RemoveEmptyEntries).Length;
                        string[] fullName = new string[size_t];
                        fullName = myName.Split(DELIM_SPACE, StringSplitOptions.RemoveEmptyEntries);

                        bool mTestMode = (userProfile.pwrMode != 0) ? true : false;
                        mWirelessEnable.Checked = (userProfile.lpEnable != 0) ? true : false;
                        bool mProximityStatus = (userProfile.proxEnable != 0) ? true : false;
                        //mProximityEnable.Checked = (userProfile.proxEnable != 0) ? true : false;           // Added by Jason Chen, 2014.06.03
                        bool mFieldDataEnable = (userProfile.realXmitEnable != 0) ? true : false;
                        bool mEnableXmit = (userProfile.realXmitEnable != 0) ? true : false;            // Added by Jason Chen, 2014.06.03
                        bool mAlarmEnable = ((userProfile.AlarmMode & 0x80) == 0) ? false : true;
                        bool mAudio = ((userProfile.AlarmMode & 0x10) == 0) ? false : true;
                        bool mVisual = ((userProfile.AlarmMode & 0x20) == 0) ? false : true;
                        bool mInterlock = ((userProfile.AlarmMode & 0x40) == 0) ? false : true;
                        mTestMode = ((userProfile.AlarmMode & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01


                        string mFirstName = fullName[0];
                        string mLastName;
  
                        if (fullName.Length > 1)
 
                            mLastName = fullName[1];
 
                           // lastName = fullName[1]; //mLastName.Text
 
                        else
 
                            mLastName = "";
                        string mPlayerNum = userProfile.playerNo.ToString();
                        string mRecThreshold = userProfile.tR.ToString();
                        string mAlarmThreshold = userProfile.tAlarm.ToString();
 
 

                        //////////////////////////////////////////////////////////////////////////////////////            // 2014.10.31
                        if (bind_with_wireless_on)
                        {
                            if (mWirelessEnable.Checked)                                                                  // 2014.10.31
                            {
                                bind_with_wireless_on = false;                                                            // 2014.10.31
                                Thread.Sleep(300);
                                this.BeginInvoke((MethodInvoker)delegate { m_button_Bind_Click(this, null); });           // 2014.10.31
                                return;                                                                                   // 2014.10.31
                            }
                            else
                            {
                                setDeviceForWireless(); // Enable wireless on the device                                  // 2014.10.31
                                bind_with_wireless_on = true;                                                             // 2014.10.31
                            }
                        }
                        //////////////////////////////////////////////////////////////////////////////////////            // 2014.10.31

                        if (gft_node > 0)
                        {
                            NodeProfile[gft_node - 1].register = true;
                            NodeProfile[gft_node - 1].usbMode = 0x81;
                            NodeProfile[gft_node - 1].alarmThres = userProfile.tAlarm;
                            NodeProfile[gft_node - 1].playerNum = userProfile.playerNo;
                            NodeProfile[gft_node - 1].recThres = userProfile.tR;
 
                            NodeProfile[gft_node - 1].firstName = mFirstName;// fullName[0];
                            NodeProfile[gft_node - 1].lastName = mLastName;// fullName[1];
 
                          //  NodeProfile[gft_node - 1].firstName = firstName; // mFirstName.Text;// fullName[0];
                          //  NodeProfile[gft_node - 1].lastName = lastName; // mLastName.Text;// fullName[1];
 
                            NodeProfile[gft_node - 1].AlarmMode = userProfile.AlarmMode;

                            if (mProximityStatus)
                                NodeProfile[gft_node - 1].AlarmMode |= 0x02;
                            else
                                NodeProfile[gft_node - 1].AlarmMode &= 0xFD;
                            if (mFieldDataEnable)
                                NodeProfile[gft_node - 1].AlarmMode |= 0x04;
                            else
                                NodeProfile[gft_node - 1].AlarmMode &= 0xFB;
                            if (mTestMode)
                                NodeProfile[gft_node - 1].AlarmMode |= 0x08;
                            else
                                NodeProfile[gft_node - 1].AlarmMode &= 0xF7;

                            NodeProfile[gft_node - 1].iLocation[0] = BitConverter.ToInt16(new byte[2] { userProfile.loc[0], userProfile.loc[1] }, 0);
                            NodeProfile[gft_node - 1].iLocation[1] = BitConverter.ToInt16(new byte[2] { userProfile.loc[2], userProfile.loc[3] }, 0);
                            NodeProfile[gft_node - 1].iLocation[2] = BitConverter.ToInt16(new byte[2] { userProfile.loc[4], userProfile.loc[5] }, 0);
                            NodeProfile[gft_node - 1].iLocation[3] = BitConverter.ToInt16(new byte[2] { userProfile.loc[6], userProfile.loc[7] }, 0);

                            setGftWirelessNode(NodeProfile[gft_node - 1], false);     // Update database

                            lpHub.lpDevices[gft_node - 1].live = true;
                            lpHub.lpDevices[gft_node - 1].alarm = ((userProfile.AlarmMode & 0x01) == 0x01) ? true : false;
                            lpHub.lpDevices[gft_node - 1].playerNumer = userProfile.playerNo;
                            lpHub.lpDevices[gft_node - 1].name = myName.ToCharArray();
                            lpHub.lpDevices[gft_node - 1].mode = 0x81;


                            //lpHub.lpDevices[gft_node- 1].battLevel = 
 
#if USE_ORIGINAL_REFRESH
                            lpHub.dgvLpPopulateView(gft_node); // 2014.10.01
#else
                            //this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), gft_node);
                            lpHub.dgvLpPopulateView(gft_node); // 2014.10.31
#endif
 
                           // lpHub.dgvLpPopulateView(gft_node); // 2014.10.01
                           // lpHub.lpDevices[gft_node-1].updateDgv = true;
 

                            mtimer.Enabled = true;

                            //////////////////////////////////////////////////////////////////////////
                            int row = getRowNum(gft_node);                         // 2014.10.30
                            if (row >= 0)
                            {
                                wirelessViewer.dataGridView.FirstDisplayedCell = null; // 2014.10.30
                                wirelessViewer.dataGridView.ClearSelection();          // 2014.10.30
                                wirelessViewer.dataGridView.Rows[row].Selected = true; // 2014.10.30   
                                //m_button_Clone.Enabled = true;
                            }
                            //////////////////////////////////////////////////////////////////////////

                            //mUnbind.Enabled = false;
                            //onDeviceProfileReceived();
                        }
                        //mUnbind.Enabled = false;
                        onDeviceProfileReceived(deviceRegistered);
                    }
                }
                else if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_START_BOOT)
                {
                    len = 10;
                    textInfo = "-StartBoot:";

                }
                else
                {
                    textInfo += "-NoHidCMD";
                }

                if (CMD_RESP != (int)GFT_MHID_CMD.MHID_CMD_CHECKBAT)
                {
                    string temp = textInfo;
                    for (int jj = 0; jj < len; jj++)
                    {
                        textInfo += string.Format("{0:x2} ", buff[jj]).ToUpper();     // start from buff[4]
                        if ((jj+1)%22 == 0)
                        {
                            textInfo +="\r\n"+temp;//---USR-Profile:";
                        }
                    }
                    //m_Edit_RxPcket.AppendText(textInfo + "\r\n");
                    if (CMD_RESP == (int)GFT_MHID_CMD.MHID_CMD_GET_USR_PROFILE)
                    {
                       // m_Edit_RxPcket.AppendText("\r\n");
                    }
                }

            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (gForceHid != null)
            {
                byte[] txBuff = new byte[65];
                txBuff[0] = 0x00; 
                txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_REBOOT;

                try
                {
                    gForceHid.WriteOutput(ref txBuff);                             // remove binding info of GFT by GFT USB
                }
                catch (Exception xx)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                }
            }
        }

        private void m_button_Clone_Click(object sender, EventArgs e)               // Function Changed to Unbin cmd
        {

            if ((GftWirelessHidHub == null) && (!mBLEInterface.m_bConnected))     // BLE added
                return;


            if (wirelessViewer.dataGridView.SelectedRows.Count != 1) return;                       //2014.10.30 added
            
            //DataGridViewRow cur_row = gftWirelessView.CurrentRow;
            //DataGridViewRow cur_row = wirelessViewer.dataGridView.CurrentRow;                    //2014.10.30 removed            
            DataGridViewRow cur_row = wirelessViewer.dataGridView.SelectedRows[0];                 //2014.10.30 added
            string nodeIDs;
            if (cur_row != null)
                nodeIDs = cur_row.Cells[0].Value.ToString();
            else
                return;

            byte nodeID = (byte)Convert.ToInt16(nodeIDs);
#if false
            if (((gft_node > 0) && (gft_node == nodeID))||((gft_node == 0)&&(gForceHid != null)))
            {
                if (gForceHid == null)
                {
                    if (TooltabPage16Exist)
                        m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                    return;
                }
                setDeviceForWirelessOff();

                byte[] txBuff = new byte[65];
                txBuff[0] = 0x00;
                //txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_REBOOT;
                txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_LP_UNBIND;

                try
                {
                    gForceHid.WriteOutput(ref txBuff);                             // remove binding info of GFT by GFT USB
                }
                catch (Exception xx)
                {
                    if (TooltabPage16Exist)
                        m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                }

                if (GftWirelessHidHub == null) return;
                if (gft_node > 0)
                {
                    txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                    txBuff[2] = (byte)gft_node;
                    GftWirelessHidHub.WriteOutput(ref txBuff);                    // remove binding info of dongle
                    MessageBox.Show("Player " + NodeProfile[gft_node - 1].playerNum + " " + NodeProfile[gft_node - 1].firstName + " " + NodeProfile[gft_node - 1].lastName + "'s GFT has been unbound by USB.");
                }
                else
                {
                }
                
            }
            else if (gft_node > 0 && gForceHid != null){
                setDeviceForWirelessOff();
                byte[] txBuff = new byte[65];
                txBuff[0] = 0x00;
                //txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_REBOOT;
                txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_LP_UNBIND;

                try
                {
                    gForceHid.WriteOutput(ref txBuff);                             // remove binding info of GFT by GFT USB
                }
                catch (Exception xx)
                {
                    if (TooltabPage16Exist)
                        m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                }

                if (GftWirelessHidHub == null) return;
                if (gft_node > 0)
                {
                    txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                    txBuff[2] = (byte)gft_node;
                    GftWirelessHidHub.WriteOutput(ref txBuff);                    // remove binding info of dongle
                    MessageBox.Show("Player " + NodeProfile[gft_node - 1].playerNum + " " + NodeProfile[gft_node - 1].firstName + " " + NodeProfile[gft_node - 1].lastName + "'s GFT has been unbound by USB.");
                }
            }
            else
            {

                if (lpHub.lpDevices[nodeID - 1].mode == 0)
                {
                    // The device appears to be off. Just delete binding information from the hub

                    if (GftWirelessHidHub == null) return;
                    if (nodeID > 0)
                    {
                        DialogResult removeFromHub = MessageBox.Show("Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT is not Powered On.\n\nIf you choose to remove it now, the Wireless functionality on the device will not be disabled.\n\nAre you sure you want to remove the GFT from the Hub?",
                                                                     "GFT is Off, remove from Hub?",
                                                                     MessageBoxButtons.YesNo);
                        if (removeFromHub == DialogResult.Yes)
                        {
                            byte[] txBuff = new byte[65];
                            txBuff[0] = 0x00;
                            txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                            txBuff[2] = (byte)nodeID;
                            GftWirelessHidHub.WriteOutput(ref txBuff);                    // remove binding info of dongle
                        }
                        MessageBox.Show("Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT has been removed from the Hub");
                    }
                }
                else
                {
                    DialogResult removeFromHub = MessageBox.Show("Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT is not connected by USB.\n\nIt may also take some time before the device is ready to receive the unbind command. It will still be visible in the list during this time.\n\nIf you choose to remove it now, the Wireless functionality on the device will not be disabled.\n\nAre you sure you want to remove the GFT from the Hub?",
                                                                     "GFT is Off, remove from Hub?",
                                                                     MessageBoxButtons.YesNo);
                    if (removeFromHub == DialogResult.Yes)
                    {
                        byte[] txBuff = new byte[65];
                        //mSendMsgCMD(nodeID, 0x11);
                        //mSendMsgCMD_Queue((byte)nodeID, 0x11);//17
                        NodeProfile[nodeID - 1].unbindFlag = true;
                        Thread.Sleep(10);
                        cmdProcess(nodeID);
                        MessageBox.Show("Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT will be told to unbind when it is ready to receive the command.");
                    }
                }
            }
#else
            if (((gft_node > 0) && (gft_node == nodeID)) || ((gft_node == 0) && (gForceHid != null)))
            {
                setDeviceForWirelessOff();
                byte[] txBuff = new byte[65];
                txBuff[0] = 0x00;
                txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_LP_UNBIND;
                if (gForceHid == null)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                    return;
                }
                try
                {
                    gForceHid.WriteOutput(ref txBuff); // remove binding info of GFT by GFT USB
                }
                catch (Exception xx)
                {
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                }


                if ((GftWirelessHidHub == null) && (!mBLEInterface.m_bConnected))     // BLE added
                    return;
                if (gft_node > 0)
                {
                    if (GftWirelessHidHub != null)
                    {
                        txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                        txBuff[2] = (byte)gft_node;
                        GftWirelessHidHub.WriteOutput(ref txBuff);                    // remove binding info of dongle
                    }
                    else if (mBLEInterface.m_bConnected)                              // BLE Added
                    {
                        txBuff[0] = MSG_HOST_TO_HUB_UNBIND_MODE;
                        txBuff[1] = (byte)gft_node;
                        mBLEInterface.SendData(ref txBuff, 20);
                    }
                    MessageBox.Show(new Form { TopMost = true }, "Player " + NodeProfile[gft_node - 1].playerNum + " " + NodeProfile[gft_node - 1].firstName + " " + NodeProfile[gft_node - 1].lastName + "'s GFT has been unbound by USB.");
                    
                    deviceOnUnbind();
                }
            }


#endif


        }

        void unbindAnyway()
        {
            if (gForceHid == null)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                return;
            }

            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            //txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_REBOOT;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_LP_UNBIND;

            try
            {
                gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
            }


            if ((GftWirelessHidHub == null)&&(!mBLEInterface.m_bConnected))
                return;

            if (gft_node > 0)
            {

                //byte[] txBuff = new byte[65];
                //            txBuff[1] = MSG_HOST_TO_HUB_SYNC;
                //            txBuff[2] = 0;
                //            GftWirelessHidHub.WriteOutput(ref txBuff);

                if (GftWirelessHidHub != null)
                {
                    txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                    //txBuff[2] = (byte)Convert.ToInt16(nodeIDs);
                    txBuff[2] = (byte)gft_node;
                    GftWirelessHidHub.WriteOutput(ref txBuff);
                }
                else if (mBLEInterface.m_bConnected)
                {
                    txBuff[0] = MSG_HOST_TO_HUB_UNBIND_MODE;
                    //txBuff[2] = (byte)Convert.ToInt16(nodeIDs);
                    txBuff[1] = (byte)gft_node;
                    mBLEInterface.SendData(ref txBuff, 20);
                }
                //SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            }
        }

        private void mUnbind_Click(object sender, EventArgs e)
        {
            unbindAnyway();
        }

        //AnonymousPipeServerStream pipeServer;// = new AnonymousPipeServerStream(PipeDirection.Out, HandleInheritability.Inheritable);
        //string pipeHandle;// = string.Empty;
        
        private void CheckIfGFTHidHubPresent()                              // Added by Jason Chen for recgnizing GFT usb plugging, 2014.05.27
        {
            mtimer.Enabled = false;
            BuzzHandsetDevice GftWirelessHidHub_temp = BuzzHandsetDevice.FindBuzzHandset(0x1cd1, 0x0402);	// look for the device on the USB bus
            if (GftWirelessHidHub_temp != null)  //  did we find it?
            {
                if (GftWirelessHidHub_temp.mFileHandleValid)
                {
                    // Yes! So wire into its events and update the UI
                    if (gForceHid == null)
                    {
                        gForceHid = GftWirelessHidHub_temp;
                        gForceHid.OnRxPacketReceived += new HIDInputEventHandler(gForceHid_OnRxPacketReceived);
                        
                        mGFT.Text = "USB: One GFT Connected";
                        mGFT.BackColor = Color.GreenYellow;
                        //mbutton_Bind_with_unbind.Enabled = true; // Added by Jared Stock, 2014.06.25
                        //m_button_Clone.Text = "Unbind by USB";
                        //btnReset.Visible = true;
                        //mProximityEnable.Enabled = true; // Added by Jared Stock, 2014.07.09
                        //m_button_Clone.Enabled = true; // Added by Jared Stock, 2014.06.25

                        //m_button_Bind.Enabled = true; // Added by Jared Stock, 2014.06.25
                        //mbutton_Bind_with_unbind.Enabled = true;
                        //m_button_Clone.Enabled = true; // Added by Jared Stock, 2014.06.25

                        //UpdateNodeControlEnabled();
                        
                        //this.BeginInvoke((MethodInvoker)delegate { mGetUserProfile_Click(this, null); });    

                        //var server = new Named
                        //pipeServer.DisposeLocalCopyOfClientHandle();

                      //  AnonymousPipeClientStream ss = new AnonymousPipeClientStream(PipeDirection.In, pipeHandle);

                    }
                    else
                    {
                        gForceHid.Dispose();
                        //Thread.Sleep(10);
                        //gForceHid = GftWirelessHidHub_temp;
                        gForceHid.OnRxPacketReceived += new HIDInputEventHandler(gForceHid_OnRxPacketReceived);

                        mGFT.Text = "USB: One GFT Connected";
                        mGFT.BackColor = Color.GreenYellow;
                        //mbutton_Bind_with_unbind.Enabled = true; // Added by Jared Stock, 2014.06.25
                        //mProximityEnable.Enabled = true; // Added by Jared Stock, 2014.07.09
                        //m_button_Clone.Text = "Unbind by USB";
                        //m_button_Clone.Enabled = true;
                        //btnReset.Visible = true;
                        //m_button_Clone.Enabled = true; // Added by Jared Stock, 2014.06.25

                        //GftWirelessHidHub_temp.Dispose();                        
                    }
                    this.BeginInvoke((MethodInvoker)delegate { mGetInfo_Click(this, null); });
                }
            }
            else
            {
                if (gForceHid != null)
                    gForceHid.Dispose();
                gForceHid = GftWirelessHidHub_temp;
                //UpdateNodeControlEnabled();
                mGFT.Text = "USB: No GFT Connected ";
                //mbutton_Bind_with_unbind.Enabled = false; // Added by Jared Stock, 2014.06.25
                //mProximityEnable.Enabled = false; // Added by Jared Stock, 2014.07.09
                //m_button_Clone.Text = "Unbind Selected";
                //m_button_Clone.Enabled = false;
                //btnReset.Visible = false;
                m_button_Clone.Enabled = false; // Added by Jared Stock, 2014.06.25
                mGFT.BackColor = Color.GreenYellow;

                mWirelessEnable.Checked = false;
                mWirelessEnable.Enabled = false;
                gft_node = 0;

                m_pStaticMode.Text = "";//GFT Connected" + "(" + string.Format("{0:x2}:{1:x2}Firm:{2:x2}:{3:x2}", buff[4], buff[5], buff[2], buff[3]).ToUpper() + ")";
                m_pStaticMode.BackColor = Color.Lime;
                //pipeServer.DisposeLocalCopyOfClientHandle();
                //pipeServer.Close();

                mtimer.Enabled = false;
                //mUnbind.Visible = false;

                //wirelessViewer.dataGridView.FirstDisplayedCell = null; // 2014.10.30
                //wirelessViewer.dataGridView.ClearSelection();          // 2014.10.30

                m_button_Clone.Enabled = false;                           // 2014.10.30
                mbutton_Bind_with_unbind.Enabled = false;                 // 2014.10.30
                this.splitContainer1.Panel2Collapsed = true;
            }
        }

        private bool checkCanBindDevices()
        {
            if ((GftWirelessHidHub != null)||(mBLEInterface.m_bConnected))
            {
                int nodeCount = 0;
                foreach (NODE_PROFILE nodeProfile in NodeProfile)
                {
                    if (nodeProfile.GID != null)
                    {
                        nodeCount++;
                    }

                }
                if (nodeCount < NUM_NODES) return true;
                else return false;
                return true;
            }
            else return false;
        }

        private void gForceHid_OnRxPacketReceived(object sender, HIDInportEventArgs args)
        {
            try
            {
                
                if (InvokeRequired)
                {
                    Invoke(new HIDInputEventHandler(gForceHid_OnRxPacketReceived), new object[] { sender, args });
                }
                else
                {
                    if (gForceHid != null)
                    {
                        //BuzzInputReport myReportIn = GftWirelessHidHub.getInputReport;
                        BuzzInputReport myReportIn = args.myInputReport;
                        byte[] rx_bufff = new byte[myReportIn.BufferLength - 1];
                        Array.Copy(myReportIn.Buffer, 1, rx_bufff, 0, myReportIn.BufferLength - 1);

                        //HIDinBuffer.Enqueue(rx_bufff);  
                        //if (rx_bufff[0] == ((byte)GFT_MHID_CMD.MHID_CMD_START_BOOT | MHID_CMD_RESPONSE_BIT))
                        if (gftWhereAmI == GFT_STATE_BOOT)
                        {

                            //Thread.Sleep(200);
                            inputReport_Queue.Enqueue(rx_bufff);
                            //Thread.Sleep(1000);

                            oSignalEvent.Set();

                            //Thread.Sleep(1);
                            //StreamWriter sw = new StreamWriter(pipeServer);
                            //sw.AutoFlush = true;
                            //sw.WriteLine("SYNC");
                            //pipeServer.WaitForPipeDrain();
                            //sw.WriteLine("SYNC");
                        }
                        else
                        {
                            if (rx_bufff[0] == ((byte)GFT_MHID_CMD.MHID_CMD_START_BOOT | MHID_CMD_RESPONSE_BIT))
                            {
                                byte[] boot_buf = new byte[myReportIn.BufferLength - 1];
                                Array.Copy(myReportIn.Buffer, 1, boot_buf, 0, myReportIn.BufferLength - 1);
                                inputReport_Queue.Enqueue(boot_buf);
                                oSignalEvent.Set();
                            }
                            GftRxPacketProcess(ref rx_bufff);
                        }
                        //m_Edit_RxPacket.AppendText(rx_bufff.ToString() + "\r\n");
                    }
                }
            }
            catch (Exception cc)
            {
#if false
                string ccc = cc.Message;
                try
                {
                    if (tabControl4.TabPages.Contains(tabPage16))
                    {
                        //m_Edit_Immediate.AppendText("gForceHid--" + ccc + "\r\n");
                    }
                }
                catch (Exception xx) {MessageBox.Show(xx.Message);}
#endif
            }
        }

        private void mGetInfo_Click(object sender, EventArgs e)
        {
            if (gForceHid == null)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                return;
            }
            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            //txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_REBOOT;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_GET_DEV_INFO;

            try
            {
                gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "--4\r\n");
            }                   

        }

        private void mGetUserProfile_Click(object sender, EventArgs e)
        {
            if (gForceHid == null)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                return;
            }

            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            //txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_REBOOT;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_GET_USR_PROFILE;

            try
            {
                gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "--3\r\n");
            }                   
        }

        static byte[] GetBytesHex(string str)
        {
            byte[] bytes = GetBytes(str);
            byte[] retbytes = new byte[str.Length/2];
            byte byte_h = 0;
//          byte byte_l = 0;

            for (int i = 0; i < 6; i++)
            {
                if ((bytes[i*4 + 2] >= 0x30) && (bytes[i*4 + 2] <= 0x39))
                    retbytes[i] = (byte)(bytes[i*4+2] - 0x30);
                else
                    retbytes[i] = (byte)(bytes[i*4+2] - 0x37);

                if((bytes[i*4] >= 0x30)&&(bytes[i*4] <= 0x39))
                    byte_h = (byte)(bytes[i*4] - 0x30);
                else
                    byte_h = (byte)(bytes[i*4] - 0x37);

                retbytes[i] =(byte)(retbytes[i]+(byte_h<<4));
            }
            return retbytes;
        }

        static byte[] GetBytesName(string str)
        {
            byte[] name = new byte[20];
            int len = str.Length;
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            for (int i = 0; i < len; i++)
            {
                name[i] = bytes[i * 2];
            }
            for (int j = len; j < 20; j++)
                name[j] = 0x00;
            
            return name;
        }

        private void mUpdateByUSB0_Click(object sender, EventArgs e)
        {/*
            if (gForceHid == null)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                return;
            }

            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_SET_USR_PROFILE;
            txBuff[2] = 0x00;
            userProfile.GID = GetBytesHex("123456789ABC");// GetBytesHex(mSerialNum.Text);
            userProfile.tAlarm = 60;// (byte)Convert.ToInt16(mAlarmThreshold.Text);
            userProfile.tR = 10;// (byte)Convert.ToInt16(mRecThreshold.Text);
            userProfile.AlarmMode = 0;// (byte)((mAlarmEnable.Checked) ? 0x80 : 0x00);
            //if (mAudio.Checked)
            //    userProfile.AlarmMode |= 0x10;
            //else
                userProfile.AlarmMode &= 0xEF;

            //if(mVisual.Checked)
            //    userProfile.AlarmMode |= 0x20;
            //else
                userProfile.AlarmMode &= 0xDF; ;

            //if(mInterlock.Checked)
            //    userProfile.AlarmMode |= 0x40;
            //else
                userProfile.AlarmMode &= 0xBF;

            userProfile.lpEnable = (byte)Convert.ToInt16(mWirelessEnable.Checked);
            userProfile.name = GetBytesName("Artaflex INC");// GetBytesName(mFirstName.Text + " " + mLastName.Text);
            userProfile.playerNo = 255;// (byte)Convert.ToInt16(mPlayerNum.Text);
            userProfile.pwrMode = 0;// (byte)Convert.ToInt16(mTestMode.Checked);
            userProfile.proxEnable = 0;// (byte)Convert.ToInt16(mProximityStatus.Checked);
            userProfile.realXmitEnable = 0;// (byte)Convert.ToInt16(mFieldDataEnable.Checked);
            //userProfile.magic = 0;

            int size_t = Marshal.SizeOf(userProfile);
            IntPtr ptr = Marshal.AllocHGlobal(size_t);
            Marshal.StructureToPtr(userProfile, ptr, true);
            Marshal.Copy(ptr, txBuff, 3, size_t);

            try
            {
                gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "--1\r\n");
            }*/
        }

        private void mBatLVL_Click(object sender, EventArgs e)
        {
            if (gForceHid == null)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                return;
            }

            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_CHECKBAT;

            try
            {
                if((mtimer.Enabled)&&(gForceHid!=null))
                    gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "--2\r\n");
            }                   


        }

        private void mEraseData_Click(object sender, EventArgs e)
        {
            if (gForceHid == null)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                return;
            }

            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_ERASE_ACC_DATA;

            try
            {
                gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
            }                   
        }

        private void btnUpdateFirm_Click(object sender, EventArgs e)
        {
            UpdateFirm();
        }

        private void GFT_ReBoot_Click(object sender, EventArgs e)
        {
            
            if (gForceHid == null)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---No GFT Connected!\r\n");
                return;
            }

            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_START_BOOT;
            txBuff[2] = 0;

            try
            {
                gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
            }
             
            //oSignalEvent.Set();
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
#if false
            bool visible_t = m_prog_progress.Visible;
            m_prog_progress.Visible = true;
            int group_width = groupBox6.Size.Width;
            group_width = group_width - m_prog_progress.Location.X;
            Size size = new Size();
            size.Height = m_prog_progress.Size.Height;
            size.Width = group_width - 10;
            m_prog_progress.Size = size;
            m_prog_progress.Visible = visible_t;
#endif
        }

         
        private void mbutton_Bind_with_unbind_Click(object sender, EventArgs e)
        {
            if ((gForceHid != null)||(mBLEInterface.m_bConnected))        // BLE added
            {
                unbindAnyway();
                bind_with_unbind = true;
            }
            else
            {
                this.BeginInvoke((MethodInvoker)delegate { m_button_Bind_Click(this, null); });
            }       
        }
        private void setDeviceForWireless()
        {
            // We can trigger this function from GftRxPacketProcess when CMD_RESP = MHID_CMD_GET_USR_PROFILE
            // Which means we must trigger the get profile routine when the device connects
            

            if (gForceHid != null)
            {
                if (userProfile.lpEnable == 0 || userProfile.realXmitEnable == 0) //userProfile.proxEnable == 0 ||
                {
                    byte[] txBuff = new byte[65];
                    txBuff[0] = 0x00;
                    txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_SET_USR_PROFILE;
                    txBuff[2] = 0x00;

                    /*if (mProximityEnable.Checked)
                    {
                        userProfile.proxEnable = (byte)Convert.ToInt16(true); // Enable Proximity
                    }
                    else
                    {
                        userProfile.proxEnable = (byte)Convert.ToInt16(false); // Disable Proximity
                    }*/
                    userProfile.lpEnable = (byte)Convert.ToInt16(true); // Enable Wireless on Device
                    userProfile.realXmitEnable = (byte)Convert.ToInt16(true); // Enable Wireless Data transfer
                    int size_t = Marshal.SizeOf(userProfile);
                    IntPtr ptr = Marshal.AllocHGlobal(size_t);
                    Marshal.StructureToPtr(userProfile, ptr, true);
                    Marshal.Copy(ptr, txBuff, 3, size_t);

                    try
                    {
                        gForceHid.WriteOutput(ref txBuff);
                    }
                    catch (Exception xx)
                    {
                        //if (TooltabPage16Exist)
                        //    m_Edit_Immediate.AppendText(xx.Message + "--1\r\n");
                    }
                }
            }


        }

        private void updateSettings()
        {
            if (gForceHid != null)
            {
                if (userProfile.lpEnable == 1) // || userProfile.proxEnable == 1
                {
                    byte[] txBuff = new byte[65];
                    txBuff[0] = 0x00;
                    txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_SET_USR_PROFILE;
                    txBuff[2] = 0x00;
                    /*if (mProximityEnable.Checked)
                    {
                        userProfile.proxEnable = (byte)Convert.ToInt16(true); // Enable Proximity
                    }
                    else
                    {
                        userProfile.proxEnable = (byte)Convert.ToInt16(false); // Disable Proximity
                    }*/
                    userProfile.lpEnable = (byte)Convert.ToInt16(false); // Disable Wireless on Device
                    userProfile.realXmitEnable = (byte)Convert.ToInt16(false); // Disable Wireless Data transfer
                    int size_t = Marshal.SizeOf(userProfile);
                    IntPtr ptr = Marshal.AllocHGlobal(size_t);
                    Marshal.StructureToPtr(userProfile, ptr, true);
                    Marshal.Copy(ptr, txBuff, 3, size_t);

                    try
                    {
                        gForceHid.WriteOutput(ref txBuff);
                    }
                    catch (Exception xx)
                    {
                        //if (TooltabPage16Exist)
                        //    m_Edit_Immediate.AppendText(xx.Message + "--1\r\n");
                    }
                }
            }
        }

        private void setDeviceForWirelessOff()
        {
     
            if (gForceHid != null)
            {
                if (userProfile.lpEnable == 1) // || userProfile.proxEnable == 1
                {
                    byte[] txBuff = new byte[65];
                    txBuff[0] = 0x00;
                    txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_SET_USR_PROFILE;
                    txBuff[2] = 0x00;
                    /*if (mProximityEnable.Checked)
                    {
                        userProfile.proxEnable = (byte)Convert.ToInt16(true); // Enable Proximity
                    }
                    else
                    {
                        userProfile.proxEnable = (byte)Convert.ToInt16(false); // Disable Proximity
                    }*/
                    userProfile.lpEnable = (byte)Convert.ToInt16(false); // Disable Wireless on Device
                    userProfile.realXmitEnable = (byte)Convert.ToInt16(false); // Disable Wireless Data transfer
                    int size_t = Marshal.SizeOf(userProfile);
                    IntPtr ptr = Marshal.AllocHGlobal(size_t);
                    Marshal.StructureToPtr(userProfile, ptr, true);
                    Marshal.Copy(ptr, txBuff, 3, size_t);

                    try
                    {
                        gForceHid.WriteOutput(ref txBuff);
                    }
                    catch (Exception xx)
                    {
                        //if (TooltabPage16Exist)
                        //    m_Edit_Immediate.AppendText(xx.Message + "--1\r\n");
                    }
                }
            }


        }

        public void onDeviceProfileReceived(bool deviceRegistered)
        {
            // Enable / Disable the Unbind button
            //DataGridViewRow cur_row = wirelessViewer.dataGridView.CurrentRow;

            string nodeIDs;
            if (wirelessViewer.dataGridView.SelectedRows.Count == 1)                     //2014.10.30 added
            {

                //DataGridViewRow cur_row = gftWirelessView.CurrentRow;
                //DataGridViewRow cur_row = wirelessViewer.dataGridView.CurrentRow;                    //2014.10.30 removed            
                DataGridViewRow cur_row = wirelessViewer.dataGridView.SelectedRows[0];                 //2014.10.30 added                
                if (cur_row != null)
                {
                    nodeIDs = cur_row.Cells[0].Value.ToString();
                    byte nodeID = (byte)Convert.ToInt16(nodeIDs);
                    if (nodeID == gft_node)
                    {
                        m_button_Clone.Enabled = true;
                        mbutton_Bind_with_unbind.Enabled = false;                               // 2014.10.31

                        this.splitContainer1.Panel2Collapsed = false;
                        return;                                                                 // 2014.10.31
                    }
                    else
                    {
                        m_button_Clone.Enabled = false;
                        this.splitContainer1.Panel2Collapsed = true;
                    }
                }
            }
            // Enable Bind button if device is not bound
            bool enableBind = true;
            foreach (DataGridViewRow cur_row2 in wirelessViewer.dataGridView.Rows)
            {
                if (cur_row2 != null) 
                {
                    nodeIDs = cur_row2.Cells[0].Value.ToString();
                    int nodeID;
                    nodeID = Convert.ToInt16(nodeIDs);
                    if (nodeID == gft_node)
                    {
                        enableBind = false;
                        break;                                                                // 2014.10.31
                    }
                 }
            }
            if (enableBind && deviceRegistered)
            {
                if ((GftWirelessHidHub == null)&&(!mBLEInterface.m_bConnected))                   // Added BLE regoniztion
                {
                    MessageBox.Show(new Form { TopMost = true }, "Please plug in your GFT Wireless Hub or Connected BLE Module");
                } else if (checkCanBindDevices())
                {
                    mbutton_Bind_with_unbind.Enabled = true;
                    this.splitContainer1.Panel2Collapsed = false;
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Your GFT Wireless Hub is Full. You will need to remove a device before you may add a new one.");
                }
            }
            else if (!deviceRegistered)
            {
                MessageBox.Show(new Form { TopMost = true }, "Your GFT does not appear to be registered. Please register your device before use.");
            }
            else mbutton_Bind_with_unbind.Enabled = false;
        }

        public void deviceOnBind()
        {
            mbutton_Bind_with_unbind.Enabled = false; // Disable the bind button when a device has been bound
            byte[] txBuff = new byte[65];
            txBuff[0] = 0x00;
            txBuff[1] = (byte)GFT_MHID_CMD.MHID_CMD_GET_USR_PROFILE;

            try
            {
                gForceHid.WriteOutput(ref txBuff);
            }
            catch (Exception xx)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
            }
        }
        public void deviceOnUnbind()
        {
            m_button_Clone.Enabled = false; // Disable the unbind button when device has been unbound
        }

        public void deviceOnConnect(int nodeIDs)
        {
            // This needs to be triggered when the device first comes online

            // Enable Proximity Alarm by Wireless
            /*
            if (GftWirelessHidHub != null)
            {
                if (mProximityEnable.Checked == true)
                {
                    if ((NodeProfile[nodeIDs - 1].AlarmMode & 0x02) == 0x02)
                    {
                        // Proximity is enabled
                    }
                }
                else
                {
                    if ((NodeProfile[nodeIDs - 1].AlarmMode & 0x02) != 0x02)
                    {
                        // Proximity is disabled
                    }
                }

                //if (mProximityEnable.Checked == mProximityStatus.Checked) return;
                byte[] txBuff = new byte[2];
                txBuff[0] = (byte)Convert.ToInt16(nodeIDs);
                txBuff[1] = 149;
                mSendMsgCMD_Queue(txBuff[0], txBuff[1]);
            }
            */
            

            if (lpHub.lpDevices[nodeIDs - 1].timeSynced == 0 && lpHub.lpDevices[nodeIDs - 1].mode != 0)
            {
                Thread.Sleep(500); // Sleep and then sync time
                lpHub.lpDevices[nodeIDs - 1].timeSynced = 1;
                /*// Check Device time
                // If time not accurate, synchronize the clock
                if ((DateTime.Now.Second >= 0) && (DateTime.Now.Second <= 60))
                {
                    if (GftWirelessHidHub == null) return;
                    byte[] txBuff = new byte[2];
                    txBuff[0] = (byte)Convert.ToInt16(nodeIDs);
                    txBuff[1] = (byte)Convert.ToInt16(m_bcdData.Text);
                    mSendMsgCMD_Queue(txBuff[0], 210);                   // Intialize a time synchronization CMD, 2014.04.28
                }
                // Get node info...
         
                byte[] txBuff2 = new byte[65];
                txBuff2[1] = MSG_HOST_TO_HUB_NODE_INFO;
                txBuff2[2] = 0;
                GftWirelessHidHub.WriteOutput(ref txBuff2);*/
                cmdProcess(nodeIDs,USB_OR_BLE);
            }
        }

        private void simUploads_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            e.Handled = !Char.IsDigit(e.KeyChar) && e.KeyChar != Delete;
        }
        private void simUploads_TextChanged(object sender, EventArgs e)
        {
            if (simUploads.Text != "")
            {
                NUM_UPLOAD_LIMIT = Convert.ToInt16(simUploads.Text);
            }
            else
            {
                NUM_UPLOAD_LIMIT = 0;
            }
        }

        private void chartViewer_Click(object sender, EventArgs e)
        {

        }

        private void buttonPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void liveView_Paint(object sender, PaintEventArgs e)
        {

        }

        private void splitContainer2_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            calcTeamStats();
            this.splitContainer3.Panel2Collapsed = true;
            wirelessViewer.dataGridView.Refresh();
            //drawTeamChartByList(teamChart);
            wirelessViewer_OnNewData(this, lastNodeSelected);
            //this.BeginInvoke(new DeviceList_Refresh_All(lpHub.dgvLpUpdateView));
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            //Panel panel = (Panel)sender;
            ControlPaint.DrawBorder(e.Graphics, this.panel1.ClientRectangle, Color.DarkBlue, ButtonBorderStyle.Inset); //FromArgb(255, 55, 55, 55)
        }

        private void lnkHighestG_Click(object sender, MouseEventArgs e)
        {
            lpHub.lpDevices[highestGUID].selectedImpact = highestGImpactID; // Set the impact that should be selected
            wirelessViewer_OnRowSelected(this, new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, highestGUID + 1)); // Node ID is expected to be +1
        }

        private void lnkHighestR_Click(object sender, MouseEventArgs e)
        {
            lpHub.lpDevices[highestRUID].selectedImpact = highestRImpactID; // Set the impact that should be selected
            wirelessViewer_OnRowSelected(this, new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, highestRUID + 1)); // Node ID is expected to be +1
        }

        private void playerReviewer_Load(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void linkPL_Click(object sender, EventArgs e)
        {
            wirelessViewer_OnRowSelected(this, new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, highestPLUID)); // Node ID is expected to be +1
        }

        private void linkEP_Click(object sender, EventArgs e)
        {
            wirelessViewer_OnRowSelected(this, new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, highestEPUID)); // Node ID is expected to be +1
        }

        private void linkRPE_Click(object sender, EventArgs e)
        {
            wirelessViewer_OnRowSelected(this, new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, highestRPEUID)); // Node ID is expected to be +1
        }

        private void btnEmptyHub_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show(new Form { TopMost = true }, "Are you sure you want to remove all devices from the Hub?\n\nAfter doing so, all Devices will have to be added to the hub manually.",
                                    "Remove All Devices From Hub",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes)
            {
                foreach (DataGridViewRow cur_row in wirelessViewer.dataGridView.Rows)
                {
                    string nodeIDs;
                    if (cur_row != null)
                        nodeIDs = cur_row.Cells[0].Value.ToString();
                    else
                        continue;
                    int nodeID;

                    nodeID = Convert.ToInt16(nodeIDs);


                    ///
                    if (GftWirelessHidHub == null) return;
                    if (nodeID > 0)
                    {
                        if (true)
                        {
                            if (!USB_OR_BLE)
                            {
                                byte[] txBuff = new byte[65];
                                txBuff[0] = 0x00;
                                txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                                txBuff[2] = (byte)nodeID;
                                GftWirelessHidHub.WriteOutput(ref txBuff);                    // remove binding info of dongle                                                                                              //MessageBox.Show(new Form { TopMost = true }, "Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT has been removed from the Hub");
                            }
                            else
                            {
                                byte[] txBuff = new byte[20];
                                txBuff[0] = MSG_HOST_TO_HUB_UNBIND_MODE;
                                txBuff[1] = (byte)nodeID;
                                mBLEInterface.SendData(ref txBuff, 20);                      // remove binding info of dongle
                                                                                             //MessageBox.Show(new Form { TopMost = true }, "Player " + NodeProfile[nodeID - 1].playerNum + " " + NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName + "'s GFT has been removed from the Hub");
                            }

                            if (nodeID >= 1 && nodeID <= NUM_NODES)
                            {
                                //ResetNodeControls(nodeID);
                                NodeProfile[nodeID - 1].GID = null;
                            }
                            if (lpHub != null)
                            {

                                int row = getRowNum(nodeID);                           // 2014.10.30
                                if (row >= 0)
                                    wirelessViewer.dataGridView.Rows.RemoveAt(row);
                            }

                            string m_NodeID = nodeID.ToString();
                            string queryInsert = "DELETE FROM gftWireless WHERE (nodeID = @nodeID) and (MID = @dongle_mid OR MID = '' OR MID = '0000000000')";   // changed, 2014.11.11

                            using (SqlCeConnection mycon = new SqlCeConnection(conString))
                            {
                                try
                                {
                                    mycon.Open();
                                    using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                                    {
                                        com.Parameters.AddWithValue("@nodeID", nodeID);
                                        com.Parameters.AddWithValue("@dongle_mid", dongle_mid);      // 2014.11.11

                                        com.ExecuteNonQuery();
                                        getGftWirelessAll();
                                    }
                                }
                                catch (Exception er)
                                {
                                }
                                try
                                {
                                    mycon.Close();
                                }
                                catch (Exception er)
                                {
                                    //throw er;
                                }
                            }
                        }
                    }
                    ///
                }
            }
        }

        private void mStopBroadCast_Click(object sender, EventArgs e)
        {
            byte[] txBuff = new byte[65];

            if (RxLog != null )//&& RxLog.isCaptureBackChannel(0xFF, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
            {
                var txtBuff = /*DateTime.Now + */"==>BC Broadcast CMD " + txBuff[1] + " msg ";                 // Modified by Jason chen, 2017.06.15
                RxLog.logText(txtBuff);
            }
            if (!USB_OR_BLE)
            {
                txBuff[1] = 0x5A;
                txBuff[2] = 0x00;
                txBuff[3] = 0;
                GftWirelessHidHub.WriteOutput(ref txBuff);
            }
            else
            {
                txBuff[0] = 0x5A;
                txBuff[1] = 0x00;
                txBuff[2] = 0;
                mBLEInterface.SendData(ref txBuff, 20);
            }
        }

        bool firstStart = true;
        private void mInitBLE_Click(object sender, EventArgs e)
        {
            //BLE_InitOnClicked(this, new BLEControlOnClickedArgs(BLE_number));
            if (mBT_Addr.Text.Length == 12)
            {
                if (mBLEInterface.InitTest(mBT_Addr.Text))
                {
                    mBLEInterface.OnBLEAckReceived += new BLE_Interface.BLE_ReceiveEventHandler(BLE_OnBLEAckReceived);
                    mBLEInterface.OnBLEDataReceived += new BLE_Interface.BLE_ReceiveEventHandler(BLE_OnBLEDataReceived);

                    if (firstStart)
                    {
                        firstStart = false;
                        lpHub = new WirelessGFTViewer.LP_HUB(wirelessViewer.dataGridView);//, imageList1);
                        wirelessViewer.setHub(ref lpHub);
                        wirelessViewer.setProfile(ref NodeProfile);
                        toolStripStatus.Text = "BLE" + " Connected";                         // Added by Jason Chen, 2017.06.23
                                                                                             //toolStripStatus.Text = "HUB Connected";
                        toolStripStatus.BackColor = Color.Lime;


                        ArgsInit();
                        NodeProfileInit();
                        Wireless_dataInit();

                        //this.BeginInvoke((MethodInvoker)delegate { m_button_NodeInfo_Click(this, null); });     //moved into RxPacketProcess()//  Commented by Jason Chen, 2017.03.16
#if true
                        byte[] DataBuf = new byte[2];
                        DataBuf[0] = MSG_HUB_TO_HOST_RESET;
                        DataBuf[1] = 1;
                        RxPacketProcess(ref DataBuf, true);
                        Thread.Sleep(10);
                        SetTimer(this.Handle, 0x600, 10000, null);                                                       // Added by Jason Chen for checking nodes populated successful or not, 2017.06.15
#endif
                    }

                    mInitBLE.Enabled = false;

                }

            }
        }


        int m_receivedBytes = 0;
        int now = 0;//256 - (0x7E - 0x20 + 1);
        int then = 0;
        int dif = 0, oldif = 0;

        //const byte CHUNK_BINDING = 0;      // no packet transmission
        //const byte CHUNK_SHELF_MODE_U = 1;      // packet size will be  2
        //const byte CHUNK_SHELF_MODE_D = 4;      // packet size will be  5
        //const byte CHUNK_POWER_OFF = 10;     // packet size will be 11
        //const byte CHUNK_POWER_ON = 11;     // packet size will be 12
        //const byte CHUNK_IMMEDIATE = 12;     // packet size will be 13
        //const byte CHUNK_HIT_DATA = 13;     // packet size will be 14

        bool FrameReceived(BLE_ReceiveEventArgs args)
        {
            //BTW_GATT_VALUE pValue = new BTW_GATT_VALUE();
            //byte[] inBuffer = args.inBuffer;
            byte[] buff = args.inBuffer;
            int lenX = args.length;


            RxPacketProcess(ref buff, true);

#if false
            m_receivedBytes += lenX;



            int len, node, plen;
            byte seqn, mlen;

            mlen = (byte)(buff[4] & 0xF);
            seqn = (byte)(buff[4] >> 4);
            node = buff[1];
            plen = buff[3];
#if false
            string textInfo = string.Empty;
            string Bat_msg = string.Empty;
            now = Environment.TickCount;// &Int32.MaxValue;                
            dif = now - then;
            if ((dif > 999) || (dif < 0))
            {
                dif = 999;
                if (oldif != 999)
                    textInfo += "\r\n";
            }

            textInfo += string.Format("{0:d3}", dif);
            textInfo = string.Empty;
            textInfo += string.Format("Node{0:d2}--", node);

            if (plen == CHUNK_IMMEDIATE + 1)
            {
                for (int jj = 0; jj < mlen + 1; jj++)
                    textInfo += string.Format("{0:x2} ", buff[jj + 4]).ToUpper();     // start from buff[4]

                //mReceivedBytes.AppendText(textInfo + "\r\n");
                //if (mlen < CHUNK_IMMEDIATE)
                //    mReceivedBytes.AppendText("\r\n");
            }
            else
            {
                //String Str = Encoding.ASCII.GetString(buff);
                //MyMarshalToForm("RECV", Str + "\r\n");
            }
            //MyMarshalToForm("RECV_BYTES", m_receivedBytes.ToString());

            if ((m_receivedBytes % 100) == 0)
                Application.DoEvents();
#else
            //string temp;
            //int node;
            plen = buff[3];
            if (buff[0] == MSG_HUB_TO_HOST_NODE_DATA)
            {                
                if (plen == CHUNK_IMMEDIATE + 1)
                {
                    node = buff[1];
                    NodeDataProcess(ref buff);                // Main Process 
                }
            }

#endif
#endif
            return true;
        }

        void BLE_OnBLEAckReceived(Object sender, BLE_ReceiveEventArgs args)
        {
            m_OutstandingFrames++;
            oSignalEventBle.Set();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            oSignalEventBle.Close();
            mBLEInterface.Close();
        }

        private void mGetHubInfo_Click(object sender, EventArgs e)
        {
            if (!mBLEInterface.m_bConnected) return;

            byte[] txBuff = new byte[20];
            //txBuff[0] = 0;
            txBuff[0] = MSG_HOST_TO_HUB_GET_INFO;
            txBuff[1] = 0xEE;

            m_receivedBytes = 0;
            //mBLEInterface.Data_SetDescriptors(1, 0);
            //mBLEInterface.SendCommand(1, 0);         //(byte)NumFramesBeforeAck);

            mBLEInterface.SendData(ref txBuff, 20);

            int wait_count = 0;
            for (;;)
            {

                if (!oSignalEventBle.WaitOne(10))
                {
                    //ods("Ack failed\n");
                    wait_count++;
                    if (wait_count > 500)
                    {
                        wait_count = 1000;
                        break;
                    }
                    else
                        Application.DoEvents();
                }
                else
                {
                    oSignalEventBle.Reset();
                    break;
                }
            }
            if (wait_count == 1000)
            {
                ;// mReceivedBytes.AppendText("Data Sending fail.....\r\n");
            }

            //GftWirelesdsHidHub.WriteOutput(ref txBuff);

            SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            //Thread.Sleep(10);
        }

        private void mGetNodeInfo_Click(object sender, EventArgs e)
        {
            if (!mBLEInterface.m_bConnected) return;

            byte[] txBuff = new byte[20];
            txBuff[0] = MSG_HOST_TO_HUB_NODE_BLE_INFO;
            txBuff[1] = 0x55;


            m_receivedBytes = 0;
            //mBLEInterface.Data_SetDescriptors(1, 0);
            //mBLEInterface.SendCommand(1, 0);         //(byte)NumFramesBeforeAck);

            mBLEInterface.SendData(ref txBuff, 20);

            int wait_count = 0;
            for (;;)
            {

                if (!oSignalEventBle.WaitOne(10))
                {
                    //ods("Ack failed\n");
                    wait_count++;
                    if (wait_count > 100)
                    {
                        wait_count = 1000;
                        break;
                    }
                    else
                        Application.DoEvents();
                }
                else
                {
                    oSignalEventBle.Reset();
                    break;
                }
            }
            if (wait_count == 1000)
            {
                ;// mReceivedBytes.AppendText("Data Sending fail.....\r\n");
            }

            //GftWirelessHidHub.WriteOutput(ref txBuff);

            SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
            //Thread.Sleep(10);
        }

        private void mConnBle_Click(object sender, EventArgs e)
        {

            if(!USB_OR_BLE)
            {
                if (GftWirelessHidHub == null) return;

                byte[] txBuff = new byte[65];
                txBuff[1] = MSG_HOST_TO_HUB_UPLOAD;
                txBuff[2] = (byte)Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value);// (byte)Convert.ToInt16(mNodeID.Text);
                txBuff[3] = 0;                                                   // 0 -> Whole Packet, 1 -> Second Packet

                GftWirelessHidHub.WriteOutput(ref txBuff);
            }
            else
            {
                if (!mBLEInterface.m_bConnected)
                    return;

                //mBLEInterface.Data_SetDescriptors(1, 0);
                //mBLEInterface.SendCommand(1, 0);

                byte[] txBuff = new byte[20];

                txBuff[0] = MSG_HOST_TO_HUB_UPLOAD;
                txBuff[1] = (byte)Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value);// (byte)Convert.ToInt16(mNodeID.Text);
                txBuff[2] = 0;

                mBLEInterface.SendData(ref txBuff, 20);
            }
        }

        private void mBT_Addr_SelectedIndexChanged(object sender, EventArgs e)
        {
            mBT_Name.SelectedIndex = mBT_Addr.SelectedIndex;
        }

        void BLE_OnBLEDataReceived(Object sender, BLE_ReceiveEventArgs args)
        {
            if (InvokeRequired)
            {
                Invoke(new BLE_ReceiveEventHandler(BLE_OnBLEDataReceived), new Object[] { sender, args });
            }
            else
            {
                FrameReceived(args);
            }
        }




#if false
        private void bwDeviceList_DoWork(object sender, DoWorkEventArgs e)
        {
            if(lpHub != null) lpHub.dgvLpUpdateView();
            Thread.Sleep(1000);
        }

        private void bwDeviceList_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Thread.Sleep(5000); // Sleep for 5 seconds between updates
            if (!bwDeviceList.IsBusy) bwDeviceList.RunWorkerAsync();
        }

        private void bwPacketStatus_DoWork(object sender, DoWorkEventArgs e)
        {
            if (base.Created && PacketStatusAction != "") MyMarshalToFormBW(PacketStatusAction, PacketStatusTextToDisplay, System.Drawing.Color.FromName(PacketStatusColor)); // System.Drawing.Color.
            PacketStatusAction = "";
            Thread.Sleep(50);
        }
        private void bwPacketStatus_RunWorkerCompleted(object sneder, RunWorkerCompletedEventArgs e)
        {
            if (!bwPacketStatus.IsBusy) bwPacketStatus.RunWorkerAsync();
        }
#endif
    }
}

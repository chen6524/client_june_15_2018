﻿using Microsoft.Win32.SafeHandles;
using Microsoft.VisualBasic;
using System.ComponentModel;
//using ExcelLibrary.SpreadSheet;
//using ExcelLibrary.CompoundDocumentFormat;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Collections;
using System.Threading;
using System;
using System.Timers;
using System.Globalization;
using ArtaFlexHidWin;


namespace gForceTrackerWireless
{

    public partial class Form1 : UsbAwareForm
    {
        Int32 SessionID = 0;
        //Int32 EventID = 0;
        Int32 curSessionID = 0;
        //bool viewClickFlag = false;
        //bool cellClickFlag = false;
        public static string conString = WirelessGFTViewer.Properties.Settings.Default.gForceTrackerConnectionString;

        public struct SESSION_DATA    // Session Database Table
        {
            public Int32 SID;
            public int UID;
            public DateTime Date;     //or count for firmware data
            public DateTime OnTime;
            public DateTime OffTime;
            public Int32    Duration;
            public Int32    Impact;
            public Int32    Alarm;
            public byte     WTHR;
            public Int32    WTH;
            public Int32    Threshold;
            public Int32    sports;
        };

        public struct ACTIVITY_DATA
        {
            public DateTime reportTime;
            public Int32 activeTime;
        }
        public struct RPE_DATA
        {
            public UInt32 player_load;
            public UInt32 explosive_count;
            public UInt32 explosive_power;
            public double rpe;
            public DateTime report_time;
            public UInt32 player_load_raw;
            public UInt32 explosive_count_raw;
            public UInt32 explosive_power_raw;
            public bool first_after_on;
        }
        public struct FG_DATA
        {
            public int x_start;
            public int x_end;
            public DateTime d_start;
            public DateTime d_end;
            public uint player_load;
        }

        public struct IMPACT_DATA     // Event Database Table
        {
            public Int32     eid;
            public Int32     sid;
            public DateTime  time;
            public Int32     millisecond;
            public double    millisort;
            public Double    linX;
            public Double    linY;
            public Double    linZ;
            public Double    linR;
            public Double    thresholdG;
            public Double    thresholdR;

            public int[] buckets;
            public int exp_count;
            public int exp_power;

            public Double azimuth;
            public Double elevation;

            public Double iBrIC;

            public Double iMaxRotX; 
            public Double iMaxRotY; 
            public Double iMaxRotZ; 

            public Double rotX;
            public Double rotY;
            public Double rotZ;
            public Double rotR;

            public Double hic;
            public Double gsi;
            public Double pcs;

            public byte locationId;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataX;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataZ;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataX;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataZ;


            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataX_raw;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataY_raw;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataZ_raw;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataX_raw;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataY_raw;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataZ_raw;

            public int UID;
        };
        
        private void dataInit()
        {
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;

            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
#if false
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select MAX(EID) from Events", con);

                    using (myReader = myCommand.ExecuteReader())
                    {
                        if (myReader.Read())
                        {
                            //SessionID = Convert.ToInt32(myReader["column1"]);
                            EventID = myReader.GetInt32(0);
                        }
                    }
                    myCommand = new SqlCeCommand("select MAX(SID) from Session", con);
                    using (myReader = myCommand.ExecuteReader())
                    {
                        if (myReader.Read())
                        {

                            //SessionID = Convert.ToInt32(myReader["column1"]);
                            SessionID = myReader.GetInt32(0);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
#endif
        }

#if false
        //Jason
        private void mDataGridView1_SelectionChanged(Object sender, int index, DataGridView dataGridView1, DataGridView dataGridView2, Chart chart1)
        {
            /*
            if (viewClickFlag)
            {
                viewClickFlag = false;
                return;
            }
            DataGridView view = sender as DataGridView;
            DataGridViewSelectedRowCollection dddd = view.SelectedRows;
            Int32 index_t = 0;
            
            if (dddd.Count > 0)
            {
                cellClickFlag = true;
                //index_t = (Int32)dataGridView1.Rows[index].Cells[0].Value;
                index_t = (Int32)view.SelectedRows[0].Cells[0].Value;
                //UID = (Int32)dataGridView1.Rows[index].Cells[1].Value;
                UID = (Int32)view.SelectedRows[0].Cells[1].Value;

                curSessionID = index_t;
                dataGetImpact(curSessionID, dataGridView2);
                drawChart(curSessionID, gDataGetSessionSidThreshold(curSessionID), chart1);
            }*/
        }
#endif
        private void mWirelessViewer_OnRowSelected(Object sender, WirelessGFTViewer.NodeOnSelectedArgs args, DataGridView wirelessViewer, DataGridView dataGridView3, Chart chartViewer)
        {
            //Int32 index = 0;
            if (args.selectedRow >= 0)
            {
                if ((args.NodeID <= 0) || (args.NodeID > NUM_NODES))
                    return;

                currentNodeID = args.NodeID;             // Current UID (Node ID)
                curSessionID  = gDataGetSessionLastSID(currentNodeID);
                LastSessionID = curSessionID;

                if (curSessionID > 0)
                {
                    dataGetImpact(curSessionID, dataGridView3, "EventsWireless");
                    drawChart(curSessionID, gDataGetSessionSidThreshold(curSessionID, "SessionWireless"), chartViewer, "EventsWireless");

                    chartRawLinW.Series.Clear();
                    chartRawGyroW.Series.Clear();

                    Impacts_MouseToFirst(sender,dataGridView3, chartRawLinW, chartRawGyroW, pictureBoxHeadW, "EventsWireless");

                }
                //else
                {

                    //dataGridView3.Rows.Clear();
                }
            }
        }

        private void mDataGridView1_CellMouseClick(Object sender, DataGridViewCellMouseEventArgs e, DataGridView dataGridView1, DataGridView dataGridView2, Chart chart1)
        {
            //if (cellClickFlag)
            //{
            //    cellClickFlag = true;
            //    return;
            //}

            Int32 index = 0;
            if (e.RowIndex >= 0)
            {
                index = (Int32)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                UID = (Int32)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
                curSessionID = index;

                dataGetImpact(curSessionID, dataGridView2);
                drawChart(curSessionID, gDataGetSessionSidThreshold(curSessionID), chart1);
            }
#if false
            
            System.Text.StringBuilder messageBoxCS = new System.Text.StringBuilder();
            messageBoxCS.AppendFormat("{0} = {1}", "ColumnIndex", e.ColumnIndex);
            messageBoxCS.AppendLine();
            messageBoxCS.AppendFormat("{0} = {1}", "RowIndex", e.RowIndex);
            messageBoxCS.AppendLine();
            messageBoxCS.AppendFormat("{0} = {1}", "Button", e.Button);
            messageBoxCS.AppendLine();
            messageBoxCS.AppendFormat("{0} = {1}", "Clicks", e.Clicks);
            messageBoxCS.AppendLine();
            messageBoxCS.AppendFormat("{0} = {1}", "X", e.X);
            messageBoxCS.AppendLine();
            messageBoxCS.AppendFormat("{0} = {1}", "Y", e.Y);
            messageBoxCS.AppendLine();
            messageBoxCS.AppendFormat("{0} = {1}", "Delta", e.Delta);
            messageBoxCS.AppendLine();
            messageBoxCS.AppendFormat("{0} = {1}", "Location", e.Location);
            messageBoxCS.AppendLine();
            MessageBox.Show(messageBoxCS.ToString(), "CellMouseClick Event");
#endif
        }

        private void gDataInsertSession(SESSION_DATA sessionData)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
#if true
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT Session ON", con))
                    {
                        com.ExecuteNonQuery();
                        string query = "INSERT INTO Session (SID,UID,SPORTS,SDATE,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH_RANGE,WTH,THRESHOLD,ExerciseType )" +
                            "VALUES(@SID,@UID,@SPORTS,@SDATE,@ON_TIME,@OFF_TIME,@DURATION,@IMPACT,@ALARM,@WTH_RANGE,@WTH,@THRESHOLD,@ExerciseType )";
                        com.CommandText = query;
                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@DURATION", sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        num = com.ExecuteNonQuery();

                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string dd = me.ToString();
                throw;
            }
        }
        private void gDataDeleteSessionID(uploadDataForWirelessProcess.SESSION_DATA sessionData, string SessionTable, string dongle_mid, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
#if true
                    using (SqlCeCommand com = new SqlCeCommand())
                    {
                        com.Connection = con;
                        string query = "DELETE FROM " + SessionTable + " WHERE UID = @UID AND GID = @GID AND SDATE = @SDATE AND OFF_TIME = @OFF_TIME AND MID = @MID";

                        com.CommandText = query;
                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@GID", sessionData.GID);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@MID", dongle_mid);
                        num = com.ExecuteNonQuery();
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string dd = me.ToString();
                throw;
            }
        }
        private void gDataInsertSession(uploadDataForWirelessProcess.SESSION_DATA sessionData, string SessionTable, string dongle_mid, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
#if true
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    {
                        com.ExecuteNonQuery();

                        string query = "INSERT INTO " + SessionTable + " (SID,UID,GID,SPORTS,NAME,SDATE,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH_RANGE,WTH,THRESHOLD,ExerciseType,ACTIVE, ERASED, MID,ROT_THRESHOLD,GFT_FIRMWARE,GFT_SOFTWARE,iLOCATION0,iLOCATION1,iLOCATION2,iLOCATION3,MNT_INSIDE,UDATE,BAT_STATUS )" +
                            "VALUES(@SID,@UID,@GID,@SPORTS,@NAME,@SDATE,@ON_TIME,@OFF_TIME,@DURATION,@IMPACT,@ALARM,@WTH_RANGE,@WTH,@THRESHOLD,@ExerciseType,@ACTIVE,@ERASED,@MID,@ROT_THRESHOLD,@GFT_FIRMWARE,@GFT_SOFTWARE,@iLOCATION0,@iLOCATION1,@iLOCATION2,@iLOCATION3,@MNT_INSIDE,@UDATE,@BAT_STATUS )";

                        com.CommandText = query;
                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@GID", sessionData.GID);
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@NAME", sessionData.name);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@DURATION", sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ACTIVE", sessionData.activeTime);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        com.Parameters.AddWithValue("@ERASED", "N");
                        com.Parameters.AddWithValue("@MID", dongle_mid);
                        com.Parameters.AddWithValue("@ROT_THRESHOLD", "2000");
                        com.Parameters.AddWithValue("@GFT_FIRMWARE", sessionData.fwVersion);
                        com.Parameters.AddWithValue("@GFT_SOFTWARE", sessionData.swVersion);
                        com.Parameters.AddWithValue("@iLOCATION0", sessionData.iLocation[0]);
                        com.Parameters.AddWithValue("@iLOCATION1", sessionData.iLocation[1]);
                        com.Parameters.AddWithValue("@iLOCATION2", sessionData.iLocation[2]);
                        com.Parameters.AddWithValue("@iLOCATION3", sessionData.iLocation[3]);
                        com.Parameters.AddWithValue("@MNT_INSIDE", sessionData.iMntInside);
                        com.Parameters.AddWithValue("@UDATE", DateTime.UtcNow);
                        com.Parameters.AddWithValue("@BAT_STATUS", NodeProfile[nodeID - 1].bat_level);
                        num = com.ExecuteNonQuery();

                        //mythis.wirelessViewer.DebugDataBoxDisplay(" Session Insert OK -->" + (sessionData.SID).ToString("x") + "\r\n");
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string dd = me.ToString();
                throw;
            }
        }
        private void gDataInsertSession(SESSION_DATA sessionData, string SessionTable)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
#if true
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    {
                        com.ExecuteNonQuery();




                        string query = "INSERT INTO " + SessionTable + " (SID,UID,SPORTS,SDATE,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH_RANGE,WTH,THRESHOLD,ExerciseType )" +
                            "VALUES(@SID,@UID,@SPORTS,@SDATE,@ON_TIME,@OFF_TIME,@DURATION,@IMPACT,@ALARM,@WTH_RANGE,@WTH,@THRESHOLD,@ExerciseType )";

                        com.CommandText = query;
                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@DURATION", sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        num = com.ExecuteNonQuery();

                        //wirelessViewer.DebugDataBoxDisplay(" Session Insert OK -->" + (sessionData.SID).ToString("x") + "\r\n");
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string dd = me.ToString();
                throw;
            }
        }

        private void gDataWUpdateSession(uploadDataForWirelessProcess.SESSION_DATA sessionData, string SessionTable, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    //SID=@SID,UID=@SID,
                    //,SDATE=@SDATE,ON_TIME=@ON_TIME,@DURATION
                    string query = "UPDATE SessionWireless SET GID=@GID,SPORTS=@SPORTS,NAME=@NAME,OFF_TIME=@OFF_TIME,DURATION=DATEDIFF(minute, ON_TIME,@OFF_TIME),IMPACT=@IMPACT,ALARM=@ALARM,WTH_RANGE=@WTH_RANGE,WTH=@WTH,THRESHOLD=@THRESHOLD,ExerciseType=@ExerciseType,ACTIVE=@ACTIVE, ROT_THRESHOLD=@ROT_THRESHOLD,GFT_FIRMWARE=@GFT_FIRMWARE,GFT_SOFTWARE=@GFT_SOFTWARE,iLOCATION0=@iLOCATION0,iLOCATION1=@iLOCATION1,iLOCATION2=@iLOCATION2,iLOCATION3=@iLOCATION3,MNT_INSIDE=@MNT_INSIDE,UDATE=@UDATE,BAT_STATUS=@BAT_STATUS"
                                 + " WHERE (UID = @UID) AND (SID = @SID)";

#if true
                    //using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        //com.ExecuteNonQuery();
                        //com.CommandText = query;

                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@GID", sessionData.GID);        // Added by jason Chen according Jerad's requirement, 2014.06.16
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@NAME", sessionData.name);
                        //com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        //com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        //com.Parameters.AddWithValue("@DURATION", sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        com.Parameters.AddWithValue("@ACTIVE", sessionData.activeTime);

                        com.Parameters.AddWithValue("@ROT_THRESHOLD", "2000");
                        com.Parameters.AddWithValue("@GFT_FIRMWARE", sessionData.fwVersion);
                        com.Parameters.AddWithValue("@GFT_SOFTWARE", sessionData.swVersion);
                        com.Parameters.AddWithValue("@iLOCATION0", sessionData.iLocation[0]);
                        com.Parameters.AddWithValue("@iLOCATION1", sessionData.iLocation[1]);
                        com.Parameters.AddWithValue("@iLOCATION2", sessionData.iLocation[2]);
                        com.Parameters.AddWithValue("@iLOCATION3", sessionData.iLocation[3]);
                        com.Parameters.AddWithValue("@MNT_INSIDE", sessionData.iMntInside);
                        com.Parameters.AddWithValue("@UDATE", DateTime.UtcNow);
                        //if (!mythis.NodeProfile[nodeID - 1].max17047Exist) com.Parameters.AddWithValue("@BAT_STATUS", Math.Round((float)mythis.NodeProfile[nodeID - 1].bat_level / WirelessGFTViewer.LP_HUB.BATT_LEVEL_100PECENT * 100));
                        com.Parameters.AddWithValue("@BAT_STATUS", Math.Round((float)NodeProfile[nodeID - 1].bat_level / WirelessGFTViewer.LP_HUB.BATT_LEVEL_100PECENT * 100));

                        num = com.ExecuteNonQuery();

                        //wirelessViewer.DebugDataBoxDisplay("Session Update OK!....\r\n");
                        //mythis.wirelessViewer.DebugDataBoxDisplay(" Session Update OK -->" + (sessionData.SID).ToString("x") + "\r\n");
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                //string dd = me.ToString();
                //throw;
                throw me;
                //mythis.wirelessViewer.DebugDataBoxDisplay("Session Update" + me.Message + "\r\n");
            }
        }
        private void gDataUpdateSession(int sessionID, string log, char type)
        {
            string queryUpdate = "UPDATE Session SET ExerciseType=@ExerciseType, NOTES=@NOTES " +
                                  "WHERE SID = @SID ";
            string exerciseType;
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    if (type == 'G')
                        exerciseType = "Game";
                    else
                        exerciseType = "Practice";
                    //int uID = 1;
#if true
                    using (SqlCeCommand com = new SqlCeCommand(queryUpdate, con))
                    {
                        com.Parameters.AddWithValue("@SID", sessionID);
                        com.Parameters.AddWithValue("@ExerciseType", exerciseType);
                        com.Parameters.AddWithValue("@NOTES", log); //usrPrivate.FNAME);
                        //clear user input 
                        com.ExecuteNonQuery();
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string ddd = me.ToString();
                throw;
            }
        }

        public enum DateSessionQueryMode
        {
            DateSessionQueryModeTime,
            DateSessionQueryModeAll,
        };

        private int gDataGetSession(DateSessionQueryMode mode, DateTime start, DateTime end, DataGridView view)
        {
            int ret = 0;
#if false
            var buttonCol = new DataGridViewButtonColumn();
            buttonCol.Name = "DataGridViewColLog";
            buttonCol.HeaderText = "Log";
            buttonCol.Text = "Button Text";
#endif
            // Open the connection using the connection string.
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();

                    string myQuery;
                    string queryTime = "SELECT  SID,UID,ExerciseType,SDATE,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH,THRESHOLD,NOTES   FROM Session WHERE ( UID = '" + curUser.uid + "') AND (SDATE >= @mStart ) AND (SDATE <= @mEnd)";
                    string queryAll = "SELECT  SID,UID,ExerciseType,SDATE,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH,THRESHOLD,NOTES  FROM Session";
                    if (mode == DateSessionQueryMode.DateSessionQueryModeTime)
                        myQuery = queryTime;
                    else
                        myQuery = queryAll;
                    using (SqlCeDataAdapter adp = new SqlCeDataAdapter(myQuery, con))
                    {
                        if (mode == DateSessionQueryMode.DateSessionQueryModeTime)
                        {
                            adp.SelectCommand.Parameters.AddWithValue("mStart", start);
                            adp.SelectCommand.Parameters.AddWithValue("mEnd", end);
                        }

                        // Use DataAdapter to fill DataTable
                        DataTable t = new DataTable();
                        adp.Fill(t);
                        ret = t.Rows.Count;
                        // Render data onto the screen
                        view.DataSource = t;
                        //dataGridView1.Columns.Remove("GFTID");
                        //dataGridView1.Columns.Add(
                        //dataGridView1.Columns[1].DefaultCellStyle.Format = "##:##:##:##";

                        view.Columns[0].HeaderText = "Session ID number";

                        view.Columns[1].Visible = true;
                        view.Columns[1].HeaderText = "UID";
                        view.Columns[1].Width = 35;

                        view.Columns[2].HeaderText = "Session Type";
                        view.Columns[3].DefaultCellStyle.Format = "dd.MMMM.yyyy";
                        view.Columns[3].HeaderText = "Date";
                        view.Columns[4].DefaultCellStyle.Format = "HH:mm:ss";
                        view.Columns[4].HeaderText = "Power On";
                        view.Columns[5].DefaultCellStyle.Format = "HH:mm:ss";
                        view.Columns[5].HeaderText = "Power Off";
                        //dataGridView1.Columns[5].DefaultCellStyle.Format = "HH:mm:ss";
                        view.Columns[6].HeaderText = "Duration (Minutes)";
                        view.Columns[7].HeaderText = "Number of Impacts";
                        view.Columns[8].HeaderText = "Number of Alarms";

                        view.Columns[9].HeaderText = "Number Within Threshold";
                        view.Columns[9].Width = 120;
                        view.Columns[10].HeaderText = "Alarm Threshold";
                        view.Columns[11].HeaderText = "Log";
                        view.Sort(view.Columns[0], ListSortDirection.Descending);
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);
                }
#if false
                view.Columns.Add(buttonCol);

                foreach (DataGridViewRow row in view.Rows)
                {
                    DataGridViewButtonCell button =(DataGridViewButtonCell) row.Cells["DataGridViewColLog"] ;
                    button.Value = "abc" ;
                }
#endif

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return ret;
        }

        private Int32 gDataGetSessionLastSID()
        {
            Int32 mSessionID = 0;
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select MAX(SID) from SessionWireless", con);
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        mSessionID = myReader.GetInt32(0);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }

            return mSessionID;
        }

        private Int32 gDataGetSessionSidThreshold(Int32 sid)
        {
            Int32 threshold = 0;
       
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select THRESHOLD from Session WHERE SID LIKE @mySID", con);

                    myCommand.Parameters.Add("@mySID", SqlDbType.Int).Value = sid;
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        threshold = myReader.GetInt32(0);

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return threshold;
        }

        private Int32 gDataGetSessionSidThreshold(Int32 sid, string tableString)
        {
            Int32 threshold = 0;
       
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select THRESHOLD from " + tableString + " WHERE SID LIKE @mySID", con);

                    myCommand.Parameters.Add("@mySID", SqlDbType.Int).Value = sid;
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        threshold = myReader.GetInt32(0);

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return threshold;
        }

        private void dataGetImpact(Int32 mSessionID, DataGridView dataGridView2)
        {
           SqlCeConnection con;

            using (con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    //int num = 5;
                    //int uID = 1;
                    using (SqlCeDataAdapter adp = new SqlCeDataAdapter("SELECT  EID,IMPACT_TIME,IMPACT_TIME,LINEAR_ACC_RESULTANT,HIC,GSI,LINEAR_ACC_X,LINEAR_ACC_Y,LINEAR_ACC_Z," +
                        "ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z,ROTATIONAL_ACC_RESULTANT, AZIMUTH,ELEVATION FROM Events WHERE SID  = @myID", con))
                    {
                        adp.SelectCommand.Parameters.AddWithValue("myID", mSessionID);
                        // Use DataAdapter to fill DataTable
                        DataTable t = new DataTable();
                        adp.Fill(t);
                        dataGridView2.EnableHeadersVisualStyles = false;

                        // Render data onto the screen
                        dataGridView2.DataSource = t;

                        dataGridView2.Columns[5].HeaderCell.Style.ForeColor = System.Drawing.Color.Green;
                        dataGridView2.Columns[6].HeaderCell.Style.ForeColor = System.Drawing.Color.Orange;
                        dataGridView2.Columns[7].HeaderCell.Style.ForeColor = System.Drawing.Color.Red;
                        dataGridView2.Columns[8].HeaderCell.Style.ForeColor = System.Drawing.Color.DarkCyan;

                        dataGridView2.Columns[0].HeaderText = "Impact ID Number";
                        dataGridView2.Columns[1].DefaultCellStyle.Format = "dd.MMMM.yyyy";
                        dataGridView2.Columns[1].HeaderText = "Impact Date";
                        dataGridView2.Columns[2].DefaultCellStyle.Format = "HH:mm:ss";
                        dataGridView2.Columns[2].HeaderText = "Impact Time";
                        dataGridView2.Columns[3].HeaderText = "gForce";
                        dataGridView2.Columns[4].HeaderText = "HIC";
                        dataGridView2.Columns[5].HeaderText = "GSI";
                        //dataGridView2.Columns[5].HeaderText = "wPCS";
                        /*
                         * seriesX.Color = System.Drawing.Color.LimeGreen;
                           seriesY.Color = System.Drawing.Color.Moccasin;
                           seriesZ.Color = System.Drawing.Color.Red;
                           seriesV.Color = System.Drawing.Color.Cyan;
                         * */
                        //barCellStyle = new DataGridViewCellStyle();
                        //barCellStyle.ForeColor = System.Drawing.Color.LimeGreen; 
                        //dataGridView2.Columns[5].DefaultCellStyle.ApplyStyle(barCellStyle);
                        dataGridView2.Columns[6].HeaderText = "Linear X";

                        //barCellStyle.ForeColor = System.Drawing.Color.Orange;
                        //dataGridView2.Columns[6].DefaultCellStyle.ApplyStyle(barCellStyle);                    
                        dataGridView2.Columns[7].HeaderText = "Linear Y";
                        //barCellStyle.ForeColor = System.Drawing.Color.Red;
                        //dataGridView2.Columns[7].DefaultCellStyle.ApplyStyle(barCellStyle);
                        dataGridView2.Columns[8].HeaderText = "Linear Z";
                        //barCellStyle.ForeColor = System.Drawing.Color.DarkCyan;
                        //dataGridView2.Columns[8].DefaultCellStyle.ApplyStyle(barCellStyle);

                        //dataGridView2.Columns[8].HeaderText = "gForce";

                        dataGridView2.Columns[9].HeaderText = "Rotation X";
                        dataGridView2.Columns[10].HeaderText = "Rotation Y";
                        dataGridView2.Columns[11].HeaderText = "Rotation Z";
                        dataGridView2.Columns[12].HeaderText = "Rotation Resultant";

                        dataGridView2.Columns[13].HeaderText = "Azimuth";
                        dataGridView2.Columns[14].HeaderText = "Elevation";
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }

        }

        private void dataGetImpact(Int32 mSessionID, DataGridView dataGridView2, string EventsTable)
        {
            SqlCeConnection con;

            using (con = new SqlCeConnection(conString))
            {
                // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                //int num = 5;
                //int uID = 1;
                try
                {
                    con.Open();
                    using (SqlCeDataAdapter adp = new SqlCeDataAdapter("SELECT  EID,SID,IMPACT_TIME,LINEAR_ACC_RESULTANT,HIC,GSI,LINEAR_ACC_X,LINEAR_ACC_Y,LINEAR_ACC_Z," +
                      //"ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z,ROTATIONAL_ACC_RESULTANT, AZIMUTH,ELEVATION FROM " + EventsTable + " WHERE SID  = @myID" + " order by IMPACT_TIME", con))
                        "ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z,ROTATIONAL_ACC_RESULTANT, AZIMUTH,ELEVATION FROM " + EventsTable + " WHERE SID  = @myID", con))
                    {
                        adp.SelectCommand.Parameters.AddWithValue("myID", mSessionID);
                     // Use DataAdapter to fill DataTable

                        //DataSet tt = new DataSet();
                        DataTable tt = new DataTable();

                        DataTable t = new DataTable();
                                                                       
                        adp.FillSchema(t,SchemaType.Source);

                        if (gftGMT_Enable)
                            t.Columns["IMPACT_TIME"].DateTimeMode = DataSetDateTime.Local;

                        adp.Fill(tt);                        

                        if (gftGMT_Enable)
                            foreach (DataRow idata in tt.Rows)
                            {
                                DateTime impactTime = (DateTime)idata[2];
                                impactTime = impactTime.ToLocalTime();
                                idata[2] = impactTime.ToString();
                                t.Rows.Add(idata[0], idata[1], idata[2], idata[3], idata[4], idata[5], idata[6], idata[7], idata[8], idata[9], idata[10], idata[11], idata[12], idata[13], idata[14]);
                            }
                        else
                            t = tt;

                        dataGridView2.EnableHeadersVisualStyles = false;

                        // Render data onto the screen
                        //dataGridView2.Columns[0].
                        
                        dataGridView2.DataSource = t;                      
                        
                        dataGridView2.Columns[6].HeaderCell.Style.ForeColor = System.Drawing.Color.LimeGreen;
                        dataGridView2.Columns[7].HeaderCell.Style.ForeColor = System.Drawing.Color.Orange;
                        dataGridView2.Columns[8].HeaderCell.Style.ForeColor = System.Drawing.Color.Red;
                        dataGridView2.Columns[9].HeaderCell.Style.ForeColor = System.Drawing.Color.DarkCyan;

                        dataGridView2.Columns[0].HeaderText = "Impact ID";
                        dataGridView2.Columns[0].DefaultCellStyle.Format = "X8";
                        dataGridView2.Columns[0].Width = 65;
                        dataGridView2.Columns[1].HeaderText = "Session ID";
                        dataGridView2.Columns[1].Width = 65;
                        dataGridView2.Columns[1].DefaultCellStyle.Format = "X8";
                        dataGridView2.Columns[1].Visible = true;
                        //dataGridView2.Columns[2].DefaultCellStyle.Format = "dd.MMMM.yyyy";
                        //dataGridView2.Columns[2].HeaderText = "Impact Date";

                        dataGridView2.Columns[2].DefaultCellStyle.Format = "dd.MMMM.yyyy HH:mm:ss";                        
                        dataGridView2.Columns[2].HeaderText = "Impact Time";
                        dataGridView2.Columns[2].Width = 115;


                        dataGridView2.Columns[3].HeaderText = "gForce";
                        dataGridView2.Columns[3].Width = 75;
                        //dataGridView2.Columns[4].Width = 50;
                        dataGridView2.Columns[4].HeaderText = "HIC";
                        //dataGridView2.Columns[5].Width = 50;
                        dataGridView2.Columns[5].HeaderText = "GSI";
                        //dataGridView2.Columns[6].Width = 50;
                        //dataGridView2.Columns[5].HeaderText = "wPCS";
                        /*
                         * seriesX.Color = System.Drawing.Color.LimeGreen;
                           seriesY.Color = System.Drawing.Color.Moccasin;
                           seriesZ.Color = System.Drawing.Color.Red;
                           seriesV.Color = System.Drawing.Color.Cyan;
                         * */
                        //barCellStyle = new DataGridViewCellStyle();
                        //barCellStyle.ForeColor = System.Drawing.Color.LimeGreen; 
                        //dataGridView2.Columns[5].DefaultCellStyle.ApplyStyle(barCellStyle);
                        dataGridView2.Columns[6].HeaderText = "Linear X";
                        dataGridView2.Columns[6].Width = 55;

                        //barCellStyle.ForeColor = System.Drawing.Color.Orange;
                        //dataGridView2.Columns[6].DefaultCellStyle.ApplyStyle(barCellStyle);                    
                        dataGridView2.Columns[7].HeaderText = "Linear Y";
                        dataGridView2.Columns[7].Width = 55;
                        //barCellStyle.ForeColor = System.Drawing.Color.Red;
                        //dataGridView2.Columns[7].DefaultCellStyle.ApplyStyle(barCellStyle);
                        dataGridView2.Columns[8].HeaderText = "Linear Z";
                        dataGridView2.Columns[8].Width = 55;
                        //barCellStyle.ForeColor = System.Drawing.Color.DarkCyan;
                        //dataGridView2.Columns[8].DefaultCellStyle.ApplyStyle(barCellStyle);

                        //dataGridView2.Columns[8].HeaderText = "gForce";

                        dataGridView2.Columns[9].HeaderText = "Rotation X";
                        dataGridView2.Columns[9].Width = 65;
                        dataGridView2.Columns[10].HeaderText = "Rotation Y";
                        dataGridView2.Columns[10].Width = 65;
                        dataGridView2.Columns[11].HeaderText = "Rotation Z";
                        dataGridView2.Columns[11].Width = 65;
                        dataGridView2.Columns[12].HeaderText = "Rotation Resultant";
                        //dataGridView2.Columns[13].Width = 65;

                        dataGridView2.Columns[13].HeaderText = "Azimuth";
                        //dataGridView2.Columns[14].Width = 55;
                        dataGridView2.Columns[14].HeaderText = "Elevation";
                        //dataGridView2.Columns[15].Width = 55;

                        for (int i = 0; i < dataGridView2.Columns.Count; i++)
                            dataGridView2.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
                catch (SqlException er)
                {
                    MessageBox.Show(er.Message);
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }

        }


        internal struct IMPACT_SUMMARY_RESULT
        {
            internal List<int> linear;
            internal List<int> hic;
            internal List<int> gsi;
            internal List<int> location;

        };
        internal struct IMPACT_CNT_SUMMARY
        {
            internal int cnt;
            internal double average;
            internal double hicAve;
            internal double gsiAve;

        };

        //get the impact count and average
        private IMPACT_CNT_SUMMARY dataGetImpactCnt(DateTime start, DateTime end)
        {

            double sum = 0;
            double sumHic = 0;
            double sumGsi = 0;
            int cnt = 0;
            double read;
            SqlCeCommand myCommand;
            IMPACT_CNT_SUMMARY su = new IMPACT_CNT_SUMMARY();

            SqlCeDataReader myReader = null;
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
                myCommand = new SqlCeCommand("SELECT Events.SID,Events.IMPACT_TIME,Events.LINEAR_ACC_RESULTANT, Events.HIC, Events.GSI, Session.UID  FROM Events INNER JOIN Session ON Events.SID = Session.SID  WHERE (UID = @mUID) AND (SDATE >=@mstart) AND (SDATE <= @mend) ", con);
                myCommand.Parameters.AddWithValue("mUID", curUser.uid);
                myCommand.Parameters.AddWithValue("mstart", start);
                myCommand.Parameters.AddWithValue("mend", end);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    //linear
                    read = (double)myReader["LINEAR_ACC_RESULTANT"];
                    cnt++;
                    sum += read;
                    read = (double)myReader["HIC"];
                    sumHic += read;
                    read = (double)myReader["GSI"];
                    sumGsi += read;
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            su.cnt = cnt;
            if (cnt > 0)
            {
                su.average = Math.Round(sum / cnt, 2);
                su.hicAve = Math.Round(sumHic / cnt, 2);
                su.gsiAve = Math.Round(sumGsi / cnt, 2);
            }
            else
            {
                su.average = 0;
                su.hicAve = 0;
                su.gsiAve = 0;
            }
            return su;
        }


        private IMPACT_SUMMARY_RESULT dataGetImpact(DateTime start, DateTime end, int mode)
        {
            //sort the impact
            List<int> iList = new List<int>();
            List<int> iListHic = new List<int>();
            List<int> iListGsi = new List<int>();
            List<int> iListLocation = new List<int>();
            int i;
            double azimuth = 0, elevation = 0;
            double sum = 0;
            double read;
            SqlCeCommand myCommand;
            IMPACT_SUMMARY_RESULT su = new IMPACT_SUMMARY_RESULT();
            for (i = 0; i < 10; i++)
            {

                iList.Add(0);
                iListHic.Add(0);
                iListGsi.Add(0);
                iListLocation.Add(0);
            }
            su.linear = iList;
            su.hic = iListHic;
            su.gsi = iListGsi;
            su.location = iListLocation;

            SqlCeDataReader myReader = null;
            //int cnt =0;
            i = 0;
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
                if (mode == 1)
                    myCommand = new SqlCeCommand("SELECT Events.SID,Events.IMPACT_TIME,Events.LINEAR_ACC_RESULTANT, Events.HIC, Events.GSI,Events.AZIMUTH, Events.ELEVATION,  Session.UID,Session.THRESHOLD  FROM Events INNER JOIN Session ON Events.SID = Session.SID  WHERE (UID = @mUID) AND (SDATE >=@mstart) AND (SDATE <= @mend) ", con);
                else if (mode == 2)
                    myCommand = new SqlCeCommand("SELECT Events.SID,Events.IMPACT_TIME,Events.LINEAR_ACC_RESULTANT, Events.HIC, Events.GSI,Events.AZIMUTH, Events.ELEVATION,  Session.UID, Session.THRESHOLD  FROM Events INNER JOIN Session ON Events.SID = Session.SID  WHERE (UID = @mUID) AND (SDATE >=@mstart) AND (SDATE <= @mend) AND(LINEAR_ACC_RESULTANT >= THRESHOLD*0.9)  AND(LINEAR_ACC_RESULTANT<=THRESHOLD)", con);
                else
                    myCommand = new SqlCeCommand("SELECT Events.SID,Events.IMPACT_TIME,Events.LINEAR_ACC_RESULTANT, Events.HIC, Events.GSI,Events.AZIMUTH, Events.ELEVATION,  Session.UID, Session.THRESHOLD  FROM Events INNER JOIN Session ON Events.SID = Session.SID  WHERE (UID = @mUID) AND (SDATE >=@mstart) AND (SDATE <= @mend) AND(LINEAR_ACC_RESULTANT>=THRESHOLD) ", con);
                myCommand.Parameters.AddWithValue("mUID", curUser.uid);
                myCommand.Parameters.AddWithValue("mstart", start);
                myCommand.Parameters.AddWithValue("mend", end);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    //linear
                    read = (double)myReader["LINEAR_ACC_RESULTANT"];
                    azimuth = (double)myReader["AZIMUTH"];
                    elevation = (double)myReader["ELEVATION"];
                    i = gCalcPosition(azimuth, elevation);
                    iListLocation[i]++;
                    iList[0]++;
                    sum += read;
                    if (read <= 25)
                        iList[1]++;
                    else if ((read > 25) && (read <= 50))
                        iList[2]++;
                    else if ((read > 50) && (read <= 75))
                        iList[3]++;
                    else if ((read > 75) && (read <= 100))
                        iList[4]++;
                    else if ((read > 100) && (read <= 125))
                        iList[5]++;
                    else if ((read > 125) && (read <= 150))
                        iList[6]++;
                    else if ((read > 150) && (read <= 175))
                        iList[7]++;
                    else if ((read > 175) && (read <= 200))
                        iList[8]++;
                    else if ((read > 200) && (read <= 225))
                        iList[9]++;


                    //HIC
                    read = (double)myReader["HIC"];
                    if ((double)myReader["HIC"] <= 100)
                        iListHic[1]++;
                    else if ((read > 100) && (read <= 150))
                        iListHic[2]++;
                    else if ((read > 150) && (read <= 200))
                        iListHic[3]++;
                    else if ((read > 200) && (read <= 250))
                        iListHic[4]++;
                    else if ((read > 250) && (read <= 300))
                        iListHic[5]++;
                    else if ((read > 300) && (read <= 350))
                        iListHic[6]++;
                    else if ((read > 350) && (read <= 400))
                        iListHic[7]++;
                    else if ((read > 400) && (read <= 450))
                        iListHic[8]++;
                    else if (read > 450)
                        iListHic[9]++;

                    //GSI
                    read = (double)myReader["GSI"];
                    if (read <= 100)
                        iListGsi[1]++;
                    else if ((read > 100) && (read <= 150))
                        iListGsi[2]++;
                    else if ((read > 150) && (read <= 200))
                        iListGsi[3]++;
                    else if ((read > 200) && (read <= 250))
                        iListGsi[4]++;
                    else if ((read > 250) && (read <= 300))
                        iListGsi[5]++;
                    else if ((read > 300) && (read <= 350))
                        iListGsi[6]++;
                    else if ((read > 350) && (read <= 400))
                        iListGsi[7]++;
                    else if ((read > 400) && (read <= 450))
                        iListGsi[8]++;
                    else if (read > 450)
                        iListGsi[9]++;
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
                //con.Dispose();
            }
            return su;
        }


        private void gDataInsertImpact(IMPACT_DATA iData)
        {
            // Retrieve the connection string from the settings file.
            // Open the connection using the connection string.
            try
            {
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT Events ON", con))
                    {
                        com.ExecuteNonQuery();
                        string query = "INSERT INTO Events (EID,SID,IMPACT_TIME,LINEAR_ACC_X,LINEAR_ACC_Y," +
                                       " LINEAR_ACC_Z,LINEAR_ACC_RESULTANT,AZIMUTH,ELEVATION,ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z," +
                                       " ROTATIONAL_ACC_RESULTANT,HIC,GSI,PCS,LOCATION_ID,DATAX,DATAY,DATAZ,RDATAX,RDATAY,RDATAZ,millisecond)" +
                                       "VALUES(@EID,@SID,@IMPACT_TIME,@LINEAR_ACC_X,@LINEAR_ACC_Y,@LINEAR_ACC_Z,@LINEAR_ACC_RESULTANT," +
                                       "@AZIMUTH, @ELEVATION, " +
                                       "@ROTATIONAL_ACC_X,@ROTATIONAL_ACC_Y,@ROTATIONAL_ACC_Z,@ROTATIONAL_ACC_RESULTANT,@HIC,@GSI,@PCS,@LOCATION_ID,@dataX,@dataY,@dataZ,@rdataX,@rdataY,@rdataZ,@millisecond)";
                        com.CommandText = query;
                        com.Parameters.AddWithValue("@EID", iData.eid);
                        com.Parameters.AddWithValue("@SID", iData.sid);
                        com.Parameters.AddWithValue("@IMPACT_TIME", iData.time);
                        com.Parameters.AddWithValue("@LINEAR_ACC_X", iData.linX);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Y", iData.linY);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Z", iData.linZ);
                        com.Parameters.AddWithValue("@LINEAR_ACC_RESULTANT", iData.linR);

                        com.Parameters.AddWithValue("@AZIMUTH", iData.azimuth);
                        com.Parameters.AddWithValue("@ELEVATION", iData.elevation);

                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_X", iData.rotX);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Y", iData.rotY);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Z", iData.rotZ);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_RESULTANT", iData.rotR);

                        com.Parameters.AddWithValue("@HIC", iData.hic);
                        com.Parameters.AddWithValue("@GSI", iData.gsi);
                        com.Parameters.AddWithValue("@PCS", iData.pcs);
                        com.Parameters.AddWithValue("@LOCATION_ID", iData.locationId);

                        com.Parameters.AddWithValue("@dataX", iData.dataX);
                        com.Parameters.AddWithValue("@dataY", iData.dataY);
                        com.Parameters.AddWithValue("@dataZ", iData.dataZ);

                        com.Parameters.AddWithValue("@rdataX", iData.rdataX);
                        com.Parameters.AddWithValue("@rdataY", iData.rdataY);
                        com.Parameters.AddWithValue("@rdataZ", iData.rdataZ);
                        com.Parameters.AddWithValue("@millisecond", iData.millisecond);
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string ddd = me.ToString();
                throw;
            }
        }

        private void gDataInsertImpact(IMPACT_DATA iData, string EventsTable)
        {
            System.Diagnostics.Debug.WriteLine("Writing Impact to Database: " + iData.sid + "::" + iData.eid);
            // Retrieve the connection string from the settings file.
            // Open the connection using the connection string.
            try
            {
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + EventsTable + " ON", con))
                    {
                        com.ExecuteNonQuery();
                        string query = "INSERT INTO " + EventsTable + " (EID,SID,IMPACT_TIME,LINEAR_ACC_X,LINEAR_ACC_Y," +
                                       " LINEAR_ACC_Z,LINEAR_ACC_RESULTANT,AZIMUTH,ELEVATION,ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z," +
                                       " ROTATIONAL_ACC_RESULTANT,HIC,GSI,PCS,LOCATION_ID,DATAX,DATAY,DATAZ,RDATAX,RDATAY,RDATAZ,millisecond)" +
                                       "VALUES(@EID,@SID,@IMPACT_TIME,@LINEAR_ACC_X,@LINEAR_ACC_Y,@LINEAR_ACC_Z,@LINEAR_ACC_RESULTANT," +
                                       "@AZIMUTH, @ELEVATION, " +
                                       "@ROTATIONAL_ACC_X,@ROTATIONAL_ACC_Y,@ROTATIONAL_ACC_Z,@ROTATIONAL_ACC_RESULTANT,@HIC,@GSI,@PCS,@LOCATION_ID,@dataX,@dataY,@dataZ,@rdataX,@rdataY,@rdataZ,@millisecond)";
                        com.CommandText = query;
                        com.Parameters.AddWithValue("@EID", iData.eid);
                        com.Parameters.AddWithValue("@SID", iData.sid);
                        com.Parameters.AddWithValue("@IMPACT_TIME", iData.time);
                        com.Parameters.AddWithValue("@LINEAR_ACC_X", iData.linX);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Y", iData.linY);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Z", iData.linZ);
                        com.Parameters.AddWithValue("@LINEAR_ACC_RESULTANT", iData.linR);

                        com.Parameters.AddWithValue("@AZIMUTH", iData.azimuth);
                        com.Parameters.AddWithValue("@ELEVATION", iData.elevation);

                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_X", iData.rotX);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Y", iData.rotY);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Z", iData.rotZ);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_RESULTANT", iData.rotR);

                        com.Parameters.AddWithValue("@HIC", iData.hic);
                        com.Parameters.AddWithValue("@GSI", iData.gsi);
                        com.Parameters.AddWithValue("@PCS", iData.pcs);
                        com.Parameters.AddWithValue("@LOCATION_ID", iData.locationId);

                        com.Parameters.AddWithValue("@dataX", iData.dataX);
                        com.Parameters.AddWithValue("@dataY", iData.dataY);
                        com.Parameters.AddWithValue("@dataZ", iData.dataZ);

                        com.Parameters.AddWithValue("@rdataX", iData.rdataX);
                        com.Parameters.AddWithValue("@rdataY", iData.rdataY);
                        com.Parameters.AddWithValue("@rdataZ", iData.rdataZ);
                        com.Parameters.AddWithValue("@millisecond", iData.millisecond);
                        num = com.ExecuteNonQuery();

                        //wirelessViewer.DebugDataBoxDisplay(" Event Insert     OK -->" + (iData.eid).ToString("x") + "\r\n");
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }
        private void gDataGetLinData(Int32 myEID)
        {

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
#if false             
                SqlCeCommand command = new SqlCeCommand("SELECT  dataX FROM Events WHERE ", con);
                byte[] buffer = (byte[])command.ExecuteScalar();
                con.Close();
                return buffer;
#endif
                SqlCeCommand cmd = new SqlCeCommand("Select * from Events WHERE EID LIKE @myEID", con);
                cmd.Parameters.Add("@myEID", SqlDbType.Int).Value = myEID;

                SqlCeDataReader rdr = cmd.ExecuteReader();
                rdr.Read();
                byte[] dataX = (byte[])rdr["dataX"];
                byte[] dataY = (byte[])rdr["dataY"];
                byte[] dataZ = (byte[])rdr["dataZ"];
            }
        }

        //get user and gft info for database
        private bool gDataGetUsr()
        {
            bool ret = false;
#if false
            if (mycon.State != ConnectionState.Open)
                return false;
#else
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();

#endif
                SqlCeCommand cmd = new SqlCeCommand("SELECT gft.UID,gft.GID,gft.CNT_SET_YEAR,gft.CNT_SET_MONTH,gft.CNT_SET_WEEK,gft.CNT_SET_DAY, gft.SPORTS, gft.LEVEL," +
                                                           "gUser.EMAIL,gUser.FNAME, gUser.LNAME, gUser.BDAY, gUser.WEIGHT, gUser.HEIGHT, gUser.Country,gUser.Male " +
                                                           "FROM gft INNER JOIN gUser ON gft.UID = gUser.UID WHERE (GID = @mGID) ", con);
                cmd.Parameters.AddWithValue("@mGID", gidToString());
                try
                {
                    SqlCeDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        curUser.uid = (Int32)rdr["UID"];
                        curUser.email = ((string)rdr["EMAIL"]).ToCharArray();
                        curUser.BDAY = (DateTime)rdr["BDAY"];
                        curUser.FNAME = ((string)rdr["FNAME"]).ToCharArray();
                        curUser.LNAME = ((string)rdr["LNAME"]).ToCharArray();
                        curUser.WEIGHT = (double)rdr["WEIGHT"];
                        curUser.HEIGHT = (double)rdr["HEIGHT"];

                        curUser.country = (int)rdr["Country"];
                        curUser.male = (bool)rdr["Male"];
                        gftProfile.sports = (int)rdr["SPORTS"];
                        gftProfile.level = (int)rdr["LEVEL"];

                        gftProfile.countYear = (int)rdr["CNT_SET_YEAR"];
                        gftProfile.countMonth = (int)rdr["CNT_SET_MONTH"];
                        gftProfile.countWeek = (int)rdr["CNT_SET_WEEK"];
                        gftProfile.countDay = (int)rdr["CNT_SET_DAY"];

                        ret = true;

                    }
                    else
                        ret = false;
                }
                catch (Exception me)
                {
                    Console.WriteLine("gDataGetUser " + me.ToString());
                    throw;
                }
            }
            return ret;
        }

        private bool gDataSetGft()
        {
            bool ret = false;
            string myGID = gidToString();
            string queryUpdate = "UPDATE gft SET DATE = @DATE,GFT_REC_THRES=@GFT_REC_THRES,GFT_ALARM_THRES=@GFT_ALARM_THRES,CNT_SET_YEAR=@CNT_SET_YEAR," +
                                  "CNT_SET_MONTH=@CNT_SET_MONTH,CNT_SET_WEEK=@CNT_SET_WEEK,CNT_SET_DAY=@CNT_SET_DAY,SPORTS=@SPORTS,LEVEL=@LEVEL WHERE GID = @GID ";
            string queryInsert = "INSERT gft (GID,UID,DATE,GFT_REC_THRES,GFT_ALARM_THRES,CNT_SET_YEAR,CNT_SET_MONTH," +
                                 "CNT_SET_WEEK,CNT_SET_DAY,SPORTS,LEVEL) VALUES(@GID,@UID,@DATE,@GFT_REC_THRES,@GFT_ALARM_THRES," +
                                 "@CNT_SET_YEAR,@CNT_SET_MONTH,@CNT_SET_WEEK,@CNT_SET_DAY,@SPORTS,@LEVEL)";
#if false
            if (mycon.State != ConnectionState.Open)
                return false;
#else
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();

#endif
                if (isNewUser)  //new user
                {
                    using (SqlCeCommand com = new SqlCeCommand(queryInsert, con))
                    {
                        com.Parameters.AddWithValue("@GID", myGID);
                        com.Parameters.AddWithValue("@UID", curUser.uid);
                        com.Parameters.AddWithValue("@DATE", System.DateTime.UtcNow); //usrPrivate.FNAME);
                        com.Parameters.AddWithValue("@GFT_REC_THRES", gftProfile.tR); //usrPrivate.LNAME);

                        com.Parameters.AddWithValue("@GFT_ALARM_THRES", gftProfile.tAlam);

                        com.Parameters.AddWithValue("@SPORTS", gftProfile.sports);

                        com.Parameters.AddWithValue("@LEVEL", gftProfile.level);

                        com.Parameters.AddWithValue("@CNT_SET_YEAR", gftProfile.countYear);
                        com.Parameters.AddWithValue("@CNT_SET_MONTH", gftProfile.countMonth);
                        com.Parameters.AddWithValue("@CNT_SET_WEEK", gftProfile.countWeek);
                        com.Parameters.AddWithValue("@CNT_SET_DAY", gftProfile.countDay);

                        com.ExecuteNonQuery();
                    }
                }
                else //update user
                {
                    using (SqlCeCommand com = new SqlCeCommand(queryUpdate, con))
                    {
                        com.Parameters.AddWithValue("@GID", myGID);
                        com.Parameters.AddWithValue("@DATE", System.DateTime.UtcNow); //usrPrivate.FNAME);
                        com.Parameters.AddWithValue("@GFT_REC_THRES", gftProfile.tR); //usrPrivate.LNAME);
                        com.Parameters.AddWithValue("@GFT_ALARM_THRES", gftProfile.tAlam);
                        com.Parameters.AddWithValue("@SPORTS", gftProfile.sports);
                        com.Parameters.AddWithValue("@LEVEL", gftProfile.level);

                        com.Parameters.AddWithValue("@CNT_SET_YEAR", gftProfile.countYear);
                        com.Parameters.AddWithValue("@CNT_SET_MONTH", gftProfile.countMonth);
                        com.Parameters.AddWithValue("@CNT_SET_WEEK", gftProfile.countWeek);
                        com.Parameters.AddWithValue("@CNT_SET_DAY", gftProfile.countDay);
                        com.ExecuteNonQuery();
                    }

                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return ret;
        }


        private Int32 gDataGetLastUID()
        {
            Int32 mUID = 0;
#if false
            if (mycon.State != ConnectionState.Open)
                return -1;
#else
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();

#endif
                try
                {
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select MAX(UID) from gUser", con);
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        mUID = myReader.GetInt32(0);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return mUID;
        }
        private bool gDataGetUsrByEmail(string email)
        {
            bool ret = false;
#if false
            if (mycon.State != ConnectionState.Open)
                return false;
#else
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();

#endif
                SqlCeCommand cmd = new SqlCeCommand("SELECT  UID,EMAIL, FNAME,  LNAME,  BDAY,  WEIGHT,  HEIGHT,  Country, Male " +
                    "FROM   gUser  WHERE (EMAIL = @mEmail) ", con);
                cmd.Parameters.AddWithValue("@mEmail", email);
                try
                {
                    SqlCeDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        curUser.uid = (Int32)rdr["UID"];
                        ret = true;

                        curUser.email = ((string)rdr["EMAIL"]).ToCharArray();
                        curUser.BDAY = (DateTime)rdr["BDAY"];
                        curUser.FNAME = ((string)rdr["FNAME"]).ToCharArray();
                        curUser.LNAME = ((string)rdr["LNAME"]).ToCharArray();
                        curUser.WEIGHT = (double)rdr["WEIGHT"];
                        curUser.HEIGHT = (double)rdr["HEIGHT"];

                        curUser.country = (int)rdr["Country"];
                        curUser.male = (bool)rdr["Male"];
                    }
                    else
                        ret = false;
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                    throw;
                }
            }
            return ret;
        }
#if false
        internal List<gPerson> GetUsrList(String name)
        {

            List<gPerson> gUserList = new List<gPerson>();
            SqlCeDataReader myReader = null;
            string myGID = tbUserGID.Text;
            // Open the connection using the connection string.
#if true
            if (mycon.State != ConnectionState.Open)
                return null;
#else
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
            }
#endif
            SqlCeCommand myCommand = new SqlCeCommand("SELECT  FNAME, LNAME, BDAY, WEIGHT,HEIGHT  FROM gUser ", mycon);
            myCommand.Parameters.AddWithValue("@FNAME", name);
            myReader = myCommand.ExecuteReader();
            while (myReader.Read())
            {
                gPerson p = new gPerson((string)myReader["FNAME"] + " " + (string)myReader["LNAME"], (DateTime)myReader["BDAY"],
                                        (double)myReader["HEIGHT"], (double)myReader["WEIGHT"], "A", "B");
                gUserList.Add(p);

            }
            /*
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }*/
            return gUserList;
            //}

        }
#endif

        //ToDo need change it to be more efficent
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

    }
#if false
    public class DataGridViewRolloverCell : DataGridViewTextBoxCell
    {
        protected override void Paint(
            Graphics graphics,
            Rectangle clipBounds,
            Rectangle cellBounds,
            int rowIndex,
            DataGridViewElementStates cellState,
            object value,
            object formattedValue,
            string errorText,
            DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            // Call the base class method to paint the default cell appearance.
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState,
                value, formattedValue, errorText, cellStyle,
                advancedBorderStyle, paintParts);

            // Retrieve the client location of the mouse pointer.
            Point cursorPosition =
            this.DataGridView.PointToClient(System.Windows.Forms.Cursor.Position);

            // If the mouse pointer is over the current cell, draw a custom border.
            if (cellBounds.Contains(cursorPosition))
            {
                Rectangle newRect = new Rectangle(cellBounds.X + 1,
                    cellBounds.Y + 1, cellBounds.Width - 4,
                    cellBounds.Height - 4);
                graphics.DrawRectangle(Pens.Red, newRect);
            }
        }

        // Force the cell to repaint itself when the mouse pointer enters it.
        protected override void OnMouseEnter(int rowIndex)
        {
            this.DataGridView.InvalidateCell(this);
        }

        // Force the cell to repaint itself when the mouse pointer leaves it.
        protected override void OnMouseLeave(int rowIndex)
        {
            this.DataGridView.InvalidateCell(this);
        }

    }

    public class DataGridViewRolloverCellColumn : DataGridViewColumn
    {
        public DataGridViewRolloverCellColumn()
        {
            this.CellTemplate = new DataGridViewRolloverCell();
        }
    }
#endif
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace WirelessGFTViewer
{
    public class FuelCell : DataGridViewImageCell
    {
        // Used to make custom cell consistent with a DataGridViewImageCell
        static Image emptyImage;
        public List<gForceTrackerWireless.Form1.ACTIVITY_DATA> activityData;
        public List<gForceTrackerWireless.Form1.RPE_DATA> rpeData;
        public int gftType = 0; // 0 = impacts, 1 = performance
        public DateTime firstReceiveTime;
        public int activeTime;

        static FuelCell()
        {
            emptyImage = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
        }

        public FuelCell()
        {
            this.ValueType = typeof(int);
        }

        // Method required to make the Progress Cell consistent with the default Image Cell. 
        // The default Image Cell assumes an Image as a value, although the value of the Progress Cell is an int.
        protected override object GetFormattedValue(object value,
                            int rowIndex, ref DataGridViewCellStyle cellStyle,
                            TypeConverter valueTypeConverter,
                            TypeConverter formattedValueTypeConverter,
                            DataGridViewDataErrorContexts context)
        {
            return emptyImage;
        }

        protected void PaintActivity(System.Drawing.Graphics g, System.Drawing.Rectangle clipBounds, System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {

            Color activeColour;
            Color activeBorder;
            double activePct = ((double)((int)value) / 100);
            Color fontColour;
            if (activePct < 0.25)
            {
                activeColour = Color.LimeGreen;
                activeBorder = Color.FromArgb(235, 35, 175, 35);
                fontColour = Color.White;
            }
            else if (activePct < 0.6)
            {
                activeColour = Color.Yellow;
                activeBorder = Color.FromArgb(235, 215, 225, 0);
                fontColour = Color.Black;
            }
            else
            {
                activeColour = Color.Red;
                activeBorder = Color.FromArgb(215, 215, 0, 0);
                fontColour = Color.White;
            }
            int yTop = cellBounds.Y + (cellBounds.Height - 32) / 2;
            Brush backColorBrush = new SolidBrush(cellStyle.BackColor);
            Brush foreColorBrush = new SolidBrush(fontColour);
            // Draws the cell grid
            base.Paint(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, (paintParts & ~DataGridViewPaintParts.ContentForeground));

            g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X, yTop - 2, cellBounds.Width, 36);
            g.FillRectangle(new SolidBrush(activeColour), cellBounds.X + 2, yTop, cellBounds.Width - 4, 32);
            g.DrawString((activePct * 100).ToString() + "%", new Font(cellStyle.Font.FontFamily, 8, FontStyle.Bold), foreColorBrush, cellBounds.X + cellBounds.Size.Width / 2 - 11, cellBounds.Y + cellBounds.Size.Height / 2 - 15);

            yTop += 16;
            // Draw Fuel Tank
            /*if (pbHeatMap.Image == null)
            {
                pbHeatMap.Image = new Bitmap(pbHeatMap.Width, pbHeatMap.Height);
            }*/

            //int yTop = cellBounds.Y + (cellBounds.Height - 22) / 2;
            //Brush backColorBrush = new SolidBrush(cellStyle.BackColor);
            //Brush foreColorBrush = new SolidBrush(fontColour);
            //base.Paint(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, (paintParts & ~DataGridViewPaintParts.ContentForeground));

            g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X, yTop - 2, cellBounds.Width, 20);

            if (activityData != null)
            {


                int activeSeconds = (int)Math.Round((double)activeTime / 3000, 0);
                TimeSpan sesLength = DateTime.UtcNow - firstReceiveTime;
                int totalSeconds = (int)sesLength.TotalSeconds;

                gForceTrackerWireless.Form1.ACTIVITY_DATA lData = new gForceTrackerWireless.Form1.ACTIVITY_DATA();
                lData.activeTime = 0;
                lData.reportTime = firstReceiveTime;

                TimeSpan t = TimeSpan.FromSeconds(0);
                int inc = (int)(totalSeconds / 60) * 60 / 5;
                if (inc < 60) inc = 60;

                if (activityData.Count > 0)
                {
                    gForceTrackerWireless.Form1.ACTIVITY_DATA rData = activityData.Last(); // Most recent activity Data
                    TimeSpan activelength = rData.reportTime - lData.reportTime;
                    int totalTime = (int)activelength.TotalSeconds;

                    System.Drawing.Drawing2D.LinearGradientBrush myGradient;

                    int deltaA;
                    TimeSpan deltaT;
                    Color lastColour = Color.LimeGreen;

                    int xPos = cellBounds.X + 2;
                    int xPosTo = cellBounds.X + 2;

                    foreach (gForceTrackerWireless.Form1.ACTIVITY_DATA aData in activityData)
                    {
                        // Determine the colour.. 0-25% Activity = Rest, 25-60% = Moderate, 60%+ = High Activity
                        // What's our active time delta?
                        deltaA = aData.activeTime - lData.activeTime;
                        deltaT = aData.reportTime - lData.reportTime;
                        xPosTo = xPos + (int)Math.Round((deltaT.TotalSeconds / activelength.TotalSeconds) * (cellBounds.Width - 6));
                        if (xPosTo > (cellBounds.X + cellBounds.Width - 3)) xPosTo = (cellBounds.X + cellBounds.Width - 3);
                        activePct = (deltaA / 3000) / deltaT.TotalSeconds;
                        if (activePct < 0.25)
                        {
                            activeColour = Color.LimeGreen;
                        }
                        else if (activePct < 0.6)
                        {
                            activeColour = Color.Yellow;
                        }
                        else
                        {
                            activeColour = Color.Red;
                        }
                        if (xPosTo > xPos)
                        {
                            myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                new Point(xPos - 1, (yTop)),
                                new Point(xPosTo, (yTop)),
                                lastColour, activeColour
                                );
                            //pen = new Pen(myGradient);
                            g.FillRectangle(myGradient, xPos, yTop, xPosTo - xPos, 16);
                            lData = aData;
                            lastColour = activeColour;
                            xPos = xPosTo;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    if (xPosTo < cellBounds.X + cellBounds.Width - 2)
                    {
                        myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                   new Point(xPos - 1, (yTop)),
                                   new Point(cellBounds.X + cellBounds.Width - 2, (yTop)),
                                   lastColour, activeColour
                                   );
                        g.FillRectangle(myGradient, xPos, yTop, (cellBounds.X + cellBounds.Width - xPos) - 2, 16);
                    }
                }
            }
        }
        
        protected void PaintRPE(System.Drawing.Graphics g, System.Drawing.Rectangle clipBounds, System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {

            Color activeColour;
            Color activeBorder;
            Color fontColour;
            if ((double)value <= 2)
            {
                activeColour = Color.LimeGreen;
                activeBorder = Color.FromArgb(235, 35, 175, 35);
                fontColour = Color.White;
            }
            else if ((double)value <= 3.5)
            {
                activeColour = Color.Yellow;
                activeBorder = Color.FromArgb(235, 215, 225, 0);
                fontColour = Color.Black;
            }
            else
            {
                activeColour = Color.Red;
                activeBorder = Color.FromArgb(215, 215, 0, 0);
                fontColour = Color.White;
            }
            int yTop = cellBounds.Y + (cellBounds.Height - 32) / 2;
            Brush backColorBrush = new SolidBrush(cellStyle.BackColor);
            Brush foreColorBrush = new SolidBrush(fontColour);
            // Draws the cell grid
            base.Paint(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, (paintParts & ~DataGridViewPaintParts.ContentForeground));

            g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X, yTop - 2, cellBounds.Width, 36);
            g.FillRectangle(new SolidBrush(activeColour), cellBounds.X + 2, yTop, cellBounds.Width - 4, 32);
            g.DrawString(((double)value).ToString() + "", new Font(cellStyle.Font.FontFamily, 8, FontStyle.Bold), foreColorBrush, cellBounds.X + cellBounds.Size.Width / 2 - 11, cellBounds.Y + cellBounds.Size.Height / 2 - 15);

            yTop += 16;

            g.FillRectangle(new SolidBrush(activeBorder), cellBounds.X, yTop - 2, cellBounds.Width, 20);

            if (rpeData != null)
            {


                int activeSeconds = (int)Math.Round((double)activeTime / 3000, 0);
                TimeSpan sesLength = DateTime.UtcNow - firstReceiveTime;
                int totalSeconds = (int)sesLength.TotalSeconds;

                gForceTrackerWireless.Form1.RPE_DATA lData = new gForceTrackerWireless.Form1.RPE_DATA();
                lData.rpe = 0;
                lData.report_time = firstReceiveTime;

                TimeSpan t = TimeSpan.FromSeconds(0);
                //int xPos = 0;
                int inc = (int)(totalSeconds / 60) * 60 / 5;
                if (inc < 60) inc = 60;


                //ACTIVITY_DATA rData = lpHub.lpDevices[currentNodeID - 1].activeTimeList.Last(); // Most recent activity Data
                if (rpeData.Count > 0)
                {
                    gForceTrackerWireless.Form1.RPE_DATA rData = rpeData.Last(); // Most recent activity Data
                    TimeSpan activelength = rData.report_time - lData.report_time;
                    int totalTime = (int)activelength.TotalSeconds;

                    System.Drawing.Drawing2D.LinearGradientBrush myGradient;

                    int deltaA;
                    TimeSpan deltaT;
                    //double activePct;
                    Color lastColour = Color.LimeGreen;
                    //Color activeColour = Color.LimeGreen;
                    int xPos = cellBounds.X + 2;
                    int xPosTo = cellBounds.X + 2;


                    foreach (gForceTrackerWireless.Form1.RPE_DATA aData in rpeData)
                    {
                        // Determine the colour.. 0-25% Activity = Rest, 25-60% = Moderate, 60%+ = High Activity
                        // What's our active time delta?
                        deltaT = aData.report_time - lData.report_time;
                        xPosTo = xPos + (int)Math.Round((deltaT.TotalSeconds / activelength.TotalSeconds) * (cellBounds.Width - 6));
                        if (xPosTo > (cellBounds.X + cellBounds.Width - 3)) xPosTo = (cellBounds.X + cellBounds.Width - 3);
                        if ((double)value < 2)
                        {
                            activeColour = Color.LimeGreen;
                        }
                        else if ((double)value < 3.5)
                        {
                            activeColour = Color.Yellow;
                        }
                        else
                        {
                            activeColour = Color.Red;
                        }
                        if (xPosTo > xPos)
                        {
                            myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                new Point(xPos - 1, (yTop)),
                                new Point(xPosTo, (yTop)),
                                lastColour, activeColour
                                );
                            //pen = new Pen(myGradient);
                            g.FillRectangle(myGradient, xPos, yTop, xPosTo - xPos, 16);
                            lData = aData;
                            lastColour = activeColour;
                            xPos = xPosTo;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    if (xPosTo < cellBounds.X + cellBounds.Width - 2)
                    {
                        myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                   new Point(xPos - 1, (yTop)),
                                   new Point(cellBounds.X + cellBounds.Width - 2, (yTop)),
                                   lastColour, activeColour
                                   );
                        g.FillRectangle(myGradient, xPos, yTop, (cellBounds.X + cellBounds.Width - xPos) - 2, 16);
                    }
                }
            }
        }

        protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle clipBounds, System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            switch (gftType)
            {
                case 0:
                    PaintActivity(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
                    break;
                case 1:
                    PaintRPE(g, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
                    break;
            }
        }
    }
}

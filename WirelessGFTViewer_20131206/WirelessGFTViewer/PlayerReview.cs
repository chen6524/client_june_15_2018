﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gForceTrackerWireless;
using WirelessGFTViewer.Properties;
using System.Threading;

namespace WirelessGFTViewer
{
    public partial class PlayerReview : UserControl
    {
        public event LightOnChangedEventHandler OnRowSelected;
        public event UploadAllHandler OnButtonUploadAll;
        public event RemoveFromHubHandler OnMnuRemoveFromHub;
        public event RemoveFromHubDataGridHandler onMnuRemoveFromHubDG;
        public event GetImpactsHandler OnMnuGetImpacts;
        public event GetImpactsDataGridHandler OnMnuGetImpactsDG;
        public event DownloadHandler OnMnuDownload;
        public event DownloadDataGridHandler OnMnuDownloadDG;
        public event PowerOffOneHandler OnMnuPowerOff;
        public event PowerOffOneDataGridHandler OnMnuPowerOffDG;
        public event StopRecordingHandler OnMnuStopRecording;
        public event StopRecordingDataGridHandler OnMnuStopRecordingDG;
        public event StartRecordingHandler OnMnuStartRecording;
        public event StartRecordingDataGridHandler OnMnuStartRecordingDG;
        public event PowerOffAllHandler OnButtonPowerOffAll;
        public event EnableProximityAllHandler OnButtonEnableProximityAll;
        public event DisableProximityAllHandler OnButtonDisableProximityAll;
        public event StopRecordingAllHandler OnButtonStopRecordingAll;
        public event StartRecordingAllHandler OnButtonStartRecordingAll;

        public event PowerOnOneHandler OnMnuPowerOn;
        public event PowerOnOneDataGridHandler OnMnuPowerOnDG;
        public event PowerOnAllHandler OnButtonPowerOnAll;

        private LP_HUB lpHub;
        private List<Form1.NODE_PROFILE> NodeProfile;
        public bool uploadFlag = false;
        private NodeOnSelectedArgs mnuRightClickRow = null;

        public PlayerReview()
        {
            InitializeComponent();
            dgvLpInit();
        }

        public DataGridView dataGridView
        {
            get
            {
                return dgvViewer;
            }
        }

        public void setHub(ref LP_HUB lpHub) {
            this.lpHub = lpHub;
        }

        public void setProfile(ref List<Form1.NODE_PROFILE> NodeProfile)
        {
            this.NodeProfile = NodeProfile;
        }

        private void dgvViewer_Paint(object sender, PaintEventArgs e)
        {
            if (dgvViewer.ColumnHeadersHeight < 50)
            {
                dgvViewer.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dgvViewer.ColumnHeadersHeight = 50;
            }
            drawHeaders("Impacts", 3, 4, e);

            drawHeaders("Performance", 7, 4, e);
            drawHeaders("Device", 11, 2, e);
           
        }
        private void dgvViewer_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column == dgvViewer.Columns[3])
            {
                e.Handled = true;
                e.SortResult = dgvCompare(dgvViewer[17, e.RowIndex1].Value, dgvViewer[17, e.RowIndex2].Value);
            }
            else if (e.Column == dgvViewer.Columns[4])
            {
                e.Handled = true;
                e.SortResult = dgvCompare(dgvViewer[18, e.RowIndex1].Value, dgvViewer[18, e.RowIndex2].Value);
            }
        }
        private int dgvCompare(object o1, object o2)
        {
            return -Convert.ToDecimal(o1).CompareTo(Convert.ToDecimal(o2));
        }
        private void drawHeaders(string groupHeader, int offset, int cols, PaintEventArgs e)
        {
            // Paint the Performance Grouped Headers
            DataGridViewCell hc = dgvViewer.Columns[offset].HeaderCell;
            Rectangle hcRct = dgvViewer.GetCellDisplayRectangle(hc.ColumnIndex, -1, true);
            int multiHeaderWidth = 0;
            int c = 0;
            for (int i = 0; i < cols; i++)
            {
                while (dgvViewer.Columns.Count > hc.ColumnIndex + c && dgvViewer.Columns[hc.ColumnIndex + c].Visible == false) c++; // We actually only want x number of visible columns
                if (dgvViewer.Columns.Count > hc.ColumnIndex + c)
                {
                    multiHeaderWidth += dgvViewer.Columns[hc.ColumnIndex + c].Width;
                }
                c++;
            }
            
            Rectangle headRct = new Rectangle(hcRct.Left, 2, multiHeaderWidth, dgvViewer.ColumnHeadersHeight);
            headRct.Height -= 1;
            headRct.Width -= 1;

            SizeF sz = e.Graphics.MeasureString(groupHeader, dgvViewer.ColumnHeadersDefaultCellStyle.Font);
            int headerTop = Convert.ToInt32((sz.Height / 2));
            e.Graphics.FillRectangle(new SolidBrush(dgvViewer.ColumnHeadersDefaultCellStyle.BackColor), headRct);
            e.Graphics.DrawString(groupHeader, dgvViewer.ColumnHeadersDefaultCellStyle.Font, Brushes.Black, hcRct.Left + 2 + (headRct.Width - sz.Width) / 2, 0);
            //dgvViewer.GetCellDisplayRectangle

            // Draw Sub Headers (Our original Columns Headers..)
            int subHeaderTop = Convert.ToInt32(headRct.Height - sz.Height);
            c = 0;
            for (int i = 0; i < cols; i++)
            {
                while (dgvViewer.Columns.Count > hc.ColumnIndex + c && dgvViewer.Columns[hc.ColumnIndex + c].Visible == false) c++; // We actually only want x number of visible columns
                if (dgvViewer.Columns.Count > hc.ColumnIndex + c)
                {
                    Bitmap bmp = null;
                    switch (dgvViewer.Columns[hc.ColumnIndex + c].Name)
                    {
                        case "Threshold Impacts":
                        case "Explosive Power":
                            bmp = Resources.alarmsSmall;
                            break;
                        case "Total Impacts":
                            bmp = Resources.impactSmall;
                            break;
                        case "Activity":
                        case "Player Load":
                        case "Exertion":
                            bmp = Resources.activitySmall;
                            break;

                        case "Highest gForce":
                            bmp = Resources.impactSmall;
                            break;
                        case "Highest Rotation":
                            bmp = Resources.rotationSmall;
                            break;
                    }
                    Rectangle hcRcti = dgvViewer.GetCellDisplayRectangle(hc.ColumnIndex + c, -1, true);
                    if (bmp != null)
                    {
                        Graphics g = Graphics.FromImage(bmp);
                        g.Dispose();
                        System.Drawing.Imaging.ImageAttributes attr = new System.Drawing.Imaging.ImageAttributes();
                        Rectangle dstRect = new Rectangle(hcRcti.X + hcRcti.Width/2 - bmp.Width/2, hcRcti.Y + hcRcti.Height / 2 - bmp.Height / 2, bmp.Width, bmp.Height);
                        e.Graphics.DrawImage(bmp, dstRect, 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, attr);
                    }
                    sz = e.Graphics.MeasureString(dgvViewer.Columns[hc.ColumnIndex + c].HeaderText, dgvViewer.ColumnHeadersDefaultCellStyle.Font);
                    if (sz.Width > hcRct.Width && false)
                    {
                        // Try Splitting the text..
                        string[] headerText = dgvViewer.Columns[hc.ColumnIndex + c].HeaderText.Split(' ');
                        if (headerText.Count() > 1)
                        {
                            int splitSubHeaderTop = 0;
                            for (int l = 0; l < headerText.Count(); l++)
                            {
                                splitSubHeaderTop = Convert.ToInt32(headRct.Height - sz.Height * (headerText.Count() - l));
                                sz = e.Graphics.MeasureString(headerText[l], dgvViewer.ColumnHeadersDefaultCellStyle.Font);
                                //e.Graphics.DrawString(headerText[l], dgvViewer.ColumnHeadersDefaultCellStyle.Font, Brushes.Black, hcRcti.Left + 2 + (hcRcti.Width - sz.Width) / 2, splitSubHeaderTop);
                            }
                        }
                    }
                    else
                    {
                        StringFormat sf = new StringFormat();
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Center;
                        SolidBrush b = new SolidBrush(Color.Black);
                        Pen p = new Pen(Color.White, 2);
                        p.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;
                        System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
                        gp.AddString(dgvViewer.Columns[hc.ColumnIndex + c].HeaderText, dgvViewer.ColumnHeadersDefaultCellStyle.Font.FontFamily, (int)dgvViewer.ColumnHeadersDefaultCellStyle.Font.Style, dgvViewer.ColumnHeadersDefaultCellStyle.Font.Size, hcRcti, sf);
                        e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        e.Graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

                        e.Graphics.DrawPath(p, gp);
                        e.Graphics.FillPath(b, gp);

                        gp.Dispose();
                        sf.Dispose();
                        b.Dispose();
                        p.Dispose();
                        //e.Graphics.DrawString(dgvViewer.Columns[hc.ColumnIndex + c].HeaderText, dgvViewer.ColumnHeadersDefaultCellStyle.Font, Brushes.Black, hcRcti.Left + 2 + (hcRcti.Width - sz.Width) / 2, hcRcti.Y + (hcRcti.Height - sz.Height)/2); // subHeaderTop
                    }
                }
                c++;
            }
        }
        private void dgvLpInit()
        {
            dgvViewer.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvViewer.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvViewer.RowTemplate.MinimumHeight = 25;
            dgvViewer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgvViewer.RowsDefaultCellStyle.BackColor = System.Drawing.Color.White;
            dgvViewer.GridColor = Color.FromArgb(195,195,195);
            dgvViewer.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            dgvViewer.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            dgvViewer.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.Single;
            dgvViewer.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            dgvViewer.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(226, 228, 255);
            dgvViewer.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(242, 245, 169);
            //dgvViewer.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(255,255,255);
            //dgvViewer.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(225,225,225);
            dgvViewer.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
            dgvViewer.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
            dgvViewer.ColumnCount = 19;

            dgvViewer.Columns[0].Name = " ";// Number";
            dgvViewer.Columns[0].Width = 0;
            dgvViewer.Columns[0].Visible= false;
            dgvViewer.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

            //dgvViewer.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvViewer.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[1].Name = "";//#
            dgvViewer.Columns[1].ToolTipText = "Review Status"; //Player Number
            dgvViewer.Columns[1].Width = 10;
            dgvViewer.Columns[1].Visible = false;
            dgvViewer.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

            dgvViewer.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;
            dgvViewer.Columns[2].Name = "    Name";
            dgvViewer.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvViewer.Columns[2].Width = 300;
            dgvViewer.Columns[2].Frozen = true;
            dgvViewer.Columns[2].MinimumWidth = 200;
            dgvViewer.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;

            dgvViewer.Columns[7].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[7].Name = "Last 5 Minutes Activity";
            dgvViewer.Columns[7].Width = 30;
            dgvViewer.Columns[7].MinimumWidth = 30;
            dgvViewer.Columns[7].Visible = true;
            dgvViewer.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;


            DataGridViewCellStyle style1 = new DataGridViewCellStyle();
            style1.Font = new Font(dgvViewer.DefaultCellStyle.Font.FontFamily, 9, FontStyle.Bold);
            style1.Alignment = DataGridViewContentAlignment.MiddleCenter;


            dgvViewer.Columns[8].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            if (true)
            {
                dgvViewer.Columns[8].Name = "Player Load";//Time
            }
            else
            {
                dgvViewer.Columns[8].Name = "Active";//Time
            }
            dgvViewer.Columns[8].Width = 60;
            dgvViewer.Columns[8].MinimumWidth = 60;
            dgvViewer.Columns[8].DefaultCellStyle = style1;
            dgvViewer.Columns[8].SortMode = DataGridViewColumnSortMode.Automatic;


            dgvViewer.Columns[9].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[9].Name = "Explosive Power";
            dgvViewer.Columns[9].Width = 60;
            dgvViewer.Columns[9].MinimumWidth = 60;
            dgvViewer.Columns[9].DefaultCellStyle = style1;
            dgvViewer.Columns[9].SortMode = DataGridViewColumnSortMode.Automatic;
            dgvViewer.Columns[9].Visible = true;


            dgvViewer.Columns[10].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvViewer.Columns[10].DefaultCellStyle.BackColor = Color.Black;
            dgvViewer.Columns[10].Name = "Exertion";
            //dgvViewer.Columns[10].ToolTipText = "Alarm Condition";
            dgvViewer.Columns[10].Width = 30;
            dgvViewer.Columns[10].MinimumWidth = 30;
            dgvViewer.Columns[10].SortMode = DataGridViewColumnSortMode.Automatic;
            dgvViewer.Columns[10].Visible = true;


            dgvViewer.Columns[3].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[3].Name = "Highest gForce";
            dgvViewer.Columns[3].Width = 60;
            dgvViewer.Columns[3].MinimumWidth = 60;
            dgvViewer.Columns[3].DefaultCellStyle = style1;
            dgvViewer.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;


            dgvViewer.Columns[4].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[4].Name = "Highest Rotation";
            dgvViewer.Columns[4].Width = 60;
            dgvViewer.Columns[4].MinimumWidth = 60;
            dgvViewer.Columns[4].DefaultCellStyle = style1;
            dgvViewer.Columns[4].SortMode = DataGridViewColumnSortMode.Automatic;


            dgvViewer.Columns[5].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[5].Name = "Threshold Impacts";
            dgvViewer.Columns[5].Width = 60;
            dgvViewer.Columns[5].MinimumWidth = 60;
            dgvViewer.Columns[5].DefaultCellStyle = style1;
            dgvViewer.Columns[5].SortMode = DataGridViewColumnSortMode.Automatic;

            dgvViewer.Columns[6].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[6].Name = "Total Impacts";
            dgvViewer.Columns[6].Width = 60;
            dgvViewer.Columns[6].MinimumWidth = 60;
            dgvViewer.Columns[6].DefaultCellStyle = style1;
            dgvViewer.Columns[6].SortMode = DataGridViewColumnSortMode.Automatic;


            /*dgvViewer.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[8].Name = "Start Time";
            dgvViewer.Columns[8].Width = 95;
            dgvViewer.Columns[8].Visible = false;
            dgvViewer.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;*/

            /*dgvViewer.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[9].Name = "Time Span";
            dgvViewer.Columns[9].Width = 105;
            dgvViewer.Columns[9].Visible = false;
            dgvViewer.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;*/


            dgvViewer.Columns[11].HeaderCell = new playerReviewCellHeader();
            dgvViewer.Columns[11].Name = "Update";//Last
            dgvViewer.Columns[11].Width = 60;
            dgvViewer.Columns[11].MinimumWidth = 60;
            //dgvViewer.Columns[11].DefaultCellStyle = style1; 
            dgvViewer.Columns[11].SortMode = DataGridViewColumnSortMode.Automatic;

            dgvViewer.Columns[12].HeaderCell = new playerReviewCellHeader();
            //dgvViewer.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft; 
            dgvViewer.Columns[12].Name = "Status";
            dgvViewer.Columns[12].Width = 120;
            dgvViewer.Columns[12].MinimumWidth = 120;
            dgvViewer.Columns[12].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvViewer.Columns[12].Visible = true;


            dgvViewer.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[13].Name = "Bat";
            dgvViewer.Columns[13].ToolTipText = "Battery Level";
            dgvViewer.Columns[13].Width = 40;
            dgvViewer.Columns[13].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvViewer.Columns[13].Visible = false;

            dgvViewer.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[14].ToolTipText = "Click to Download Impacts";
            dgvViewer.Columns[14].Name = "Download";
            dgvViewer.Columns[14].Width = 60;
            dgvViewer.Columns[14].MinimumWidth = 60;
            dgvViewer.Columns[14].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvViewer.Columns[14].Visible = false;


            dgvViewer.Columns[15].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[15].Name = "Fuel";
            dgvViewer.Columns[15].Width = 60;
            dgvViewer.Columns[15].MinimumWidth = 60;
            dgvViewer.Columns[15].Visible = false;
            dgvViewer.Columns[15].SortMode = DataGridViewColumnSortMode.NotSortable;


            dgvViewer.Columns[16].Name = " "; // Values: Powered on = 1, Powered off = 9
            dgvViewer.Columns[16].Width = 0;
            dgvViewer.Columns[16].Visible = false;
            dgvViewer.Columns[16].SortMode = DataGridViewColumnSortMode.NotSortable;

            dgvViewer.Columns[17].Name = "Highest GForce - Raw Value";
            dgvViewer.Columns[17].Width = 0;
            dgvViewer.Columns[17].Visible = false;
            dgvViewer.Columns[17].SortMode = DataGridViewColumnSortMode.NotSortable;

            dgvViewer.Columns[18].Name = "Highest Rotation - Raw Value";
            dgvViewer.Columns[18].Width = 0;
            dgvViewer.Columns[18].Visible = false;
            dgvViewer.Columns[18].SortMode = DataGridViewColumnSortMode.NotSortable;

            //dgvViewer.
            //dgvViewer.AllowUserToDeleteRows = false;
            //dgvViewer.AllowUserToAddRows = false;
            //dgvViewer.ReadOnly = true;
            //dgvViewer.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            //dgvViewer.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            //dgvViewer.RowTemplate.Height = 30;
        }

        private int m_selectedRow;
        private int m_NodeID;

        public int selectedRow
        {
            get
            {
                return m_selectedRow;
            }
            set
            {
                if (m_selectedRow != value)
                {
                    m_selectedRow = value;
                }
            }
        }

        public int node_ID
        {
            get
            {
                return m_NodeID;
            }
            set
            {
                if (m_NodeID != value)
                {
                    m_NodeID = value;
                }
            }

        }


        public void show_device_receiving(int node) {
            foreach (DataGridViewRow cur_row in dataGridView.Rows)
            {
                if (cur_row != null)
                    if (node == Convert.ToInt16(cur_row.Cells[0].Value.ToString()))
                    {
                        string test = cur_row.Cells[1].Value.ToString();
                        cur_row.Cells[3].Value = "Receiving...";
                    }
                else
                    return;

            }
        }
#if false
        public void DebugCmdBoxDisplay(string textInfo)
        {
            //if ((m_DebugCmdBox.Visible) && (textInfo != null))
            //    m_DebugCmdBox.AppendText(textInfo);
        }


        public void DebugCmdBoxClear()
        {
            //m_DebugCmdBox.Clear();
            //m_DebugDataBox.Clear();
        }
#endif
#if false
        public void DebugDataBoxDisplay(string textInfo)
        {
            //if ((m_DebugDataBox.Visible) && (textInfo != null))
            //    m_DebugDataBox.AppendText(textInfo);
        }
#endif

        private void dgvViewer_MouseClick(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu mnuRightClick = new ContextMenu();

                int currentMouseOverRow = dgvViewer.HitTest(e.X, e.Y).RowIndex;
                int currentMouseOverCol = dgvViewer.HitTest(e.X, e.Y).ColumnIndex;
                if (currentMouseOverRow >= 0)
                {
                    if(dgvViewer.CurrentCell != null) dgvViewer.Rows[dgvViewer.CurrentCell.RowIndex].Selected = false;
                    dgvViewer.Rows[currentMouseOverRow].Selected = true;
                    dgvViewer.CurrentCell = dgvViewer[currentMouseOverCol, currentMouseOverRow];
                    MenuItem RemoveFromHub = new MenuItem("Remove From Hub");
                    //MenuItem StopRecording = new MenuItem("Stop Recording Impacts");
                    //MenuItem StartRecording = new MenuItem("Start Recording Impacts");
                    //MenuItem GetImpacts = new MenuItem("View All Impacts");
                    MenuItem PowerOff = new MenuItem("Power Off");
                    MenuItem PowerOn = new MenuItem("Power On");
                    MenuItem Download = new MenuItem("Download Data");
                    int thisNodeID = Convert.ToInt16(dgvViewer.Rows[currentMouseOverRow].Cells[0].Value.ToString());

                    /*if (lpHub.lpDevices[thisNodeID - 1].startRecordingFlag)
                    {
                        StartRecording.Enabled = false;
                        StartRecording.Checked = true;
                        StartRecording.Text += ": Waiting for response from GFT";
                    }
                    if (lpHub.lpDevices[thisNodeID - 1].stopRecordingFlag)
                    {
                        StopRecording.Enabled = false;
                        StopRecording.Checked = true;
                        StopRecording.Text += ": Waiting for response from GFT";
                    }*/
                    if (lpHub.lpDevices[thisNodeID - 1].powerOffSent == false)
                    {
                        PowerOff.Enabled = false;
                        PowerOff.Checked = true;
                        PowerOff.Text += ": Waiting for response from GFT";
                    }

                    if (lpHub.lpDevices[thisNodeID - 1].powerOnSent == false)
                    {
                        PowerOn.Enabled = false;
                        PowerOn.Checked = true;
                        PowerOn.Text += ": Waiting for response from GFT";
                    }
                    //if (lpHub.lpDevices[thisNodeID - 1].getImpactsSent == false)
                    //{
                    //    GetImpacts.Enabled = false;
                    //    GetImpacts.Checked = true;
                    //    GetImpacts.Text += ": Waiting for response from GFT";
                    //}
                    if (lpHub.lpDevices[thisNodeID - 1].upldFlag == true)
                    {
                        Download.Enabled = false;
                        Download.Checked = true;
                        Download.Text += ": Waiting for response from GFT";
                    }

                    mnuRightClickRow = new NodeOnSelectedArgs(currentMouseOverRow, currentMouseOverCol, thisNodeID);
                    RemoveFromHub.Click += new System.EventHandler(OnMnuRemoveFromHub);
                    //StopRecording.Click += new System.EventHandler(OnMnuStopRecording);
                    //StartRecording.Click += new System.EventHandler(OnMnuStartRecording);
                    //GetImpacts.Click += new System.EventHandler(OnMnuGetImpacts);
                    Download.Click += new System.EventHandler(OnMnuDownload);
                    PowerOff.Click += new System.EventHandler(OnMnuPowerOff);
                    PowerOn.Click += new System.EventHandler(OnMnuPowerOn);


                    if (lpHub.lpDevices[thisNodeID - 1].mode == 0)
                    {
                        mnuRightClick.MenuItems.Add(PowerOn);
                        mnuRightClick.MenuItems.Add(RemoveFromHub);
                    }
                    else
                    {
                        //mnuRightClick.MenuItems.Add(GetImpacts);

                        /*if (lpHub.lpDevices[thisNodeID - 1].stopRecording)
                            mnuRightClick.MenuItems.Add(StartRecording);
                        else
                            mnuRightClick.MenuItems.Add(StopRecording);*/

                        mnuRightClick.MenuItems.Add(Download);
                        mnuRightClick.MenuItems.Add(PowerOff);
                    }
                    mnuRightClick.Show(dgvViewer, new Point(e.X, e.Y));
                }
            } else {
                return;
            }
        }

        private void dgvViewer_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            UInt32 row = (UInt32)(e.RowIndex);
            if ((row == 0xffffffff) || (e.RowIndex > 62) || (e.RowIndex < 0)) 
                return;


                //string sss = (dgvViewer.Rows[e.RowIndex].Cells[0].Value).ToString();
                int n_id = 0;
                try
                {
                    n_id = Convert.ToInt32(dgvViewer.Rows[e.RowIndex].Cells[0].Value);
                    //n_id = Convert.ToInt32(e.RowIndex + 1);
                }
                catch (Exception ee)
                {
                    string temp = ee.Message;
                    return;
                }
                if (n_id == 0)
                    return;
                node_ID = n_id;

                //else node_ID = 9;
                //UID = (Int32)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
                if (e.ColumnIndex == 14)
                {
                    if (Convert.ToBoolean(dgvViewer.Rows[e.RowIndex].Cells[14].Value) == false)
                        dgvViewer.Rows[e.RowIndex].Cells[14].Value = true;
                    else
                        dgvViewer.Rows[e.RowIndex].Cells[14].Value = false;
                }


                if (OnRowSelected != null)
                {
                    if (e.Button == MouseButtons.Left)  OnRowSelected(this, new NodeOnSelectedArgs(e.RowIndex, e.ColumnIndex, node_ID));
                }
        }

        private void dgvViewer_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            int n_id = 0;
            try
            {
                if (e.RowIndex >= 0)
                {
                    n_id = Convert.ToInt32(dgvViewer.Rows[e.RowIndex].Cells[0].Value);
                    //n_id = Convert.ToInt32(e.RowIndex + 1);
                }
            }
            catch (Exception ee)
            {
                string temp = ee.Message;
            }


            if (n_id == 0)
                dgvViewer.Cursor = Cursors.Default;
            else
                dgvViewer.Cursor = Cursors.Hand;
        }

        private void dgvViewer_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
#if false
            int n_id = 0;
            if (e.ColumnIndex == 6)
            {

                for (int i = 0; i < 63; i++)
                {
                    try
                    {
                        n_id = Convert.ToInt32(dgvViewer.Rows[i].Cells[0].Value);
                    }
                    catch (Exception ee)
                    {
                        string temp = ee.Message;
                    }
                    if (n_id != 0)
                    {
                        DataGridViewCell cellImpact = new DataGridViewTextBoxCell();
                        cellImpact.Value = "0.00";
                        dgvViewer.Rows[i].Cells[6] = cellImpact;
                    }
                }
            }
#else
            if (e.ColumnIndex == 14)

            {
                if (OnButtonUploadAll != null)
                {
                    OnButtonUploadAll(this, null);
                }
            }
#endif
        }

        public void removeFromHub_Click(Object sender, EventArgs e)
        {
            if (onMnuRemoveFromHubDG != null)
                onMnuRemoveFromHubDG(this, mnuRightClickRow);
        }

        public void Download_Click(Object sender, EventArgs e)
        {
            if (OnMnuDownloadDG != null)
                OnMnuDownloadDG(this, mnuRightClickRow);
        }
        public void getImpacts_Click(Object sender, EventArgs e)
        {
            if (OnMnuGetImpactsDG != null)
                OnMnuGetImpactsDG(this, mnuRightClickRow);
        }

        public void powerOffOne_Click(Object sender, EventArgs e)
        {
            if (OnMnuPowerOffDG != null)
                OnMnuPowerOffDG(this, mnuRightClickRow);
        }

        public void powerOnOne_Click(Object sender, EventArgs e)
        {
            if (OnMnuPowerOnDG != null)
                OnMnuPowerOnDG(this, mnuRightClickRow);
        }

        public void stopRecording_Click(Object sender, EventArgs e)
        {
            if (OnMnuStopRecordingDG != null)
                OnMnuStopRecordingDG(this, mnuRightClickRow);
        }

        public void startRecording_Click(Object sender, EventArgs e)
        {
            if (OnMnuStartRecordingDG != null)
                OnMnuStartRecordingDG(this, mnuRightClickRow);
        }

        private void UploadAll_Click(object sender, EventArgs e)
        {
            if (OnButtonUploadAll != null)
            {
                OnButtonUploadAll(this, null);
            }
        }

        private void powerOffGFTs_Click(object sender, EventArgs e)
        {
            if (OnButtonPowerOffAll != null)
            {
                OnButtonPowerOffAll(this, null);
            }
        }
        private void powerOnGFTs_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("hey");
            if (OnButtonPowerOnAll != null)
            {
                OnButtonPowerOnAll(this, null);
            }
        }


        private void stopRecordingGFTs_Click(object sender, EventArgs e)
        {
            if (OnButtonStopRecordingAll != null)
            {
                OnButtonStopRecordingAll(this, null);
            }
        }

        private void startRecordingGFTs_Click(object sender, EventArgs e)
        {
            if (OnButtonStartRecordingAll != null)
            {
                OnButtonStartRecordingAll(this, null);
            }
        }
        private void proximityDisableAll_Click(object sender, EventArgs e)
        {
            if (OnButtonDisableProximityAll != null)
            {
                OnButtonDisableProximityAll(this, null);
            }
        }
        private void proximityEnableAll_Click(object sender, EventArgs e)
        {
            if (OnButtonEnableProximityAll != null)
            {
                OnButtonEnableProximityAll(this, null);
            }
        }
    }

}


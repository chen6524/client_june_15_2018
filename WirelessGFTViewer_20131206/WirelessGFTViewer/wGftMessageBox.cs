﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

namespace WirelessGFTViewer
{
    public partial class wGftMessageBox : Form
    {
        private string text2;
        private string productName;
        private MessageBoxButtons buttons;

        private MessageBoxIcon iconicon;
        private Button MyButtonOK;
        private Button MyButtonYes;
        private Button MyButtonNo;
        private Button MyButtonCancel;

        public wGftMessageBox(string text, string productName, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            Load += wGftMessageBox_Load;
            InitializeComponent();
            this.text2 = text;
            this.productName = productName;
            this.buttons = buttons;
            this.iconicon = icon;
        }
        public void wGftMessageBox_Load(System.Object sender, System.EventArgs e)
        {
            //Dim MyIcon As New Drawing.Icon(Me.iconicon)
            //Dim MyRect As New Rectangle(New Point(20, 20), New Drawing.Size(New Point(64, 64)))
            //Me.Graphics.DrawIcon()
            //Dim MyGraphics As Graphics = Graphics.FromHwnd(Me.Handle)
            //MyGraphics.DrawIcon(MyIcon, MyRect)
            int padding = 20;
            Debug.WriteLine("Loading");
            this.Text = productName;
            Label MyLabel = new Label();
            var _with1 = MyLabel;
            _with1.AutoSize = true;
            _with1.Location = new Point(padding, padding);
            _with1.Text = this.text2;
            this.pnlText.Controls.Add(MyLabel);
            this.Width = MyLabel.Width + (padding * 3);
            this.Height = MyLabel.Height + 25 + (padding * 5);
            if (buttons == MessageBoxButtons.OK)
            {
                MyButtonOK = new Button();
                var _with2 = MyButtonOK;
                _with2.Size = new System.Drawing.Size(new Point(120, 25));
                _with2.Location = new Point(this.pnlButtons.Width - 120 - (padding), (int)((this.pnlButtons.Height - 25)/2));//this.pnlButtons.Height - 25 - (padding * 3)
                _with2.Text = "OK";
                MyButtonOK.Click += onButtonOKClick;
                this.pnlButtons.Controls.Add(MyButtonOK);
            }
            else if (buttons == MessageBoxButtons.YesNo)
            {
                MyButtonYes = new Button();
                MyButtonNo = new Button();
                var _with3 = MyButtonYes;
                _with3.Size = new System.Drawing.Size(new Point(120, 25));
                _with3.Location = new Point(this.pnlButtons.Width - 120 - (padding * 2) - 120, (int)((this.pnlButtons.Height - 25) / 2)); //this.pnlButtons.Height - 25 - (padding * 3)
                _with3.Text = "Yes";
                var _with4 = MyButtonNo;
                _with4.Size = new System.Drawing.Size(new Point(120, 25));
                _with4.Location = new Point(this.pnlButtons.Width - 120 - (padding * 2), (int)((this.pnlButtons.Height - 25) / 2)); //this.pnlButtons.Height - 25 - (padding * 3)
                _with4.Text = "No";
                MyButtonYes.Click += onButtonYesClick;
                MyButtonNo.Click += onButtonNoClick;
                this.pnlButtons.Controls.Add(MyButtonYes);
                this.pnlButtons.Controls.Add(MyButtonNo);

            }
        }

        private void onButtonOKClick(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void onButtonYesClick(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void onButtonNoClick(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}

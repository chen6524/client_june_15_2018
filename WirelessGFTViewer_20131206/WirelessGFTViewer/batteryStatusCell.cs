﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


namespace WirelessGFTViewer
{
    public class batteryStatusColumn : DataGridViewTextBoxColumn
    {
        private Image imageValue;
        private Size imageSize;

        private batteryStatusColumn()
        {
            this.CellTemplate = new playerCell();
        }
        public override object Clone()
        {
            batteryStatusColumn c = base.Clone() as batteryStatusColumn;
            c.imageValue = this.imageValue;
            c.imageSize = this.imageSize;
            return c;
        }
        public Image Image
        {
            get { return this.imageValue; }
            set
            {
                if (this.Image != value)
                {
                    this.imageValue = value;
                    this.imageSize = value.Size;

                    if (this.InheritedStyle != null)
                    {
                        Padding inheritedPadding = this.InheritedStyle.Padding;
                        this.DefaultCellStyle.Padding = new Padding(imageSize.Width,
                     inheritedPadding.Top, inheritedPadding.Right,
                     inheritedPadding.Bottom);
                    }
                }
            }
        }
        private simplePlayerCell TextAndImageCellTemplate
        {
            get { return this.CellTemplate as simplePlayerCell; }
        }
        internal Size ImageSize
        {
            get { return imageSize; }
        }
    }


    public class batteryStatusCell : DataGridViewTextBoxCell
    {
        private Image imageValue;
        private Size imageSize;

        public int mode = 0;
        public string playerName = "";
        public string playerNumber = "";
        public string gftStatus = "";
        public string commandStatus = "";
        public int battery_pct = 0;

        public bool reviewRed = false;
        public bool reviewYellow = false;

        private Rectangle cell;

        private int cellX = 0;
        private int cellY = 0;
        private int cellWidth = 0;
        private int cellHeight = 0;

        public override object Clone()
        {
            batteryStatusCell c = base.Clone() as batteryStatusCell;
            c.imageValue = this.imageValue;
            c.imageSize = this.imageSize;
            return c;
        }

        private Rectangle setCellBounds(Rectangle cellBounds, string part)
        {
            switch (part)
            {
                /*case "alert":
                    cellBounds.X = this.cell.X;
                    cellBounds.Y = this.cell.Y;
                    cellBounds.Width = 10;
                    cellBounds.Height = this.cell.Height;
                    break;
                case "playerName":
                    cellBounds.X = this.cell.X + 10 + 30;
                    cellBounds.Y = this.cell.Y;
                    cellBounds.Width = this.cell.Width - 30;// - 46 - 30 - 100; // 10
                    cellBounds.Height = 25;
                    break;
                case "playerNumber":
                    cellBounds.X = this.cell.X + 10;
                    cellBounds.Y = this.cell.Y;// + 13;
                    cellBounds.Width = 30;
                    cellBounds.Height = 25;//25;
                    break;*/
                case "gftBattery":
                    cellBounds.X = this.cell.X + this.cell.Width - 36; //this.cell.X + 30 +10;
                    cellBounds.Y = this.cell.Y;// + 20;
                    cellBounds.Width = 36;
                    cellBounds.Height = 25;
                    break;
                case "gftStatus":
                    cellBounds.X = this.cell.X; //this.cell.X + 30 + 10;//this.cell.X + 30 + 36 + 10;
                    cellBounds.Y = this.cell.Y;// + 13;
                    cellBounds.Width = cell.Width - 36;//cell.Width - 30 - 36 - 10;
                    cellBounds.Height = 25;//25;
                    break;
                case "gftCommands":
                    cellBounds.X = this.cell.X;//this.cell.X + 10;
                    cellBounds.Y = this.cell.Y +25;//+ 12 + 17;
                    cellBounds.Width = cell.Width - 36;//this.cell.Width - 10;
                    cellBounds.Height = 0;//this.cell.Height - 15; //25 - 
                    break;
            }
            return cellBounds;
        }

        public Image Image
        {
            get
            {
                if (this.OwningColumn == null ||
            this.OwningPlayerColumn == null)
                {

                    return imageValue;
                }
                else if (this.imageValue != null)
                {
                    return this.imageValue;
                }
                else
                {
                    return this.OwningPlayerColumn.Image;
                }
            }
            set
            {
                if (this.imageValue != value)
                {
                    this.imageValue = value;
                    this.imageSize = value.Size;

                    //Padding inheritedPadding = this.InheritedStyle.Padding;
                    //this.Style.Padding = new Padding(imageSize.Width,
                    //inheritedPadding.Top, inheritedPadding.Right,
                    //inheritedPadding.Bottom);
                }
            }
        }
        protected override void Paint(Graphics graphics, Rectangle clipBounds,
        Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState,
        object value, object formattedValue, string errorText,
        DataGridViewCellStyle cellStyle,
        DataGridViewAdvancedBorderStyle advancedBorderStyle,
        DataGridViewPaintParts paintParts)
        {
            // Copy our Cell Bounds
            this.cell = new Rectangle(cellBounds.X, cellBounds.Y, cellBounds.Width, cellBounds.Height);


            DataGridViewCellStyle style1 = new DataGridViewCellStyle();
            DataGridViewAdvancedBorderStyle bStyle1 = new DataGridViewAdvancedBorderStyle(); //advancedBorderStyle

            // Paint the Alert Area

            if (this.reviewRed)
            {

                style1.BackColor = System.Drawing.Color.FromArgb(255, 255, 0, 0);
                style1.SelectionBackColor = System.Drawing.Color.FromArgb(255, 255, 0, 0);
            }
            else if (this.reviewYellow)
            {

                style1.BackColor = System.Drawing.Color.FromArgb(255, 255, 255, 0);
                style1.SelectionBackColor = System.Drawing.Color.FromArgb(255, 255, 255, 0);
            }
            else
            {
                style1.BackColor = cellStyle.BackColor;
                style1.SelectionBackColor = cellStyle.SelectionBackColor;
            }
            style1.Font = new Font(cellStyle.Font.FontFamily, 9, FontStyle.Regular);
            /*
            base.Paint(graphics, clipBounds, this.setCellBounds(cellBounds, "alert"), rowIndex, cellState,
               "", "", errorText, style1,
               advancedBorderStyle, paintParts);
            */
            // Paint Player Name
            /*
            if (this.playerName.Length > 25)
            {
                style1.Font = new Font(cellStyle.Font.FontFamily, 4, FontStyle.Regular);
            }
            else if (this.playerName.Length > 20)
            {
                style1.Font = new Font(cellStyle.Font.FontFamily, 5, FontStyle.Regular);
            }
            else if (this.playerName.Length > 17)
            {
                style1.Font = new Font(cellStyle.Font.FontFamily, 6, FontStyle.Regular);
            }
            else if (this.playerName.Length > 15)
            {
                style1.Font = new Font(cellStyle.Font.FontFamily, 7, FontStyle.Regular);
            }
            else
            {
                style1.Font = new Font(cellStyle.Font.FontFamily, 9, FontStyle.Regular);
            }
             * */
            style1.Alignment = DataGridViewContentAlignment.MiddleLeft;

            style1.BackColor = cellStyle.BackColor;
            style1.SelectionBackColor = cellStyle.SelectionBackColor;
            //style1.SelectionForeColor = cellStyle.SelectionForeColor;

            bStyle1.Top = advancedBorderStyle.Top;
            bStyle1.Left = advancedBorderStyle.Left;
            bStyle1.Bottom = advancedBorderStyle.Bottom; //DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Right = advancedBorderStyle.Right;
            /*
            base.Paint(graphics, clipBounds, this.setCellBounds(cellBounds, "playerName"), rowIndex, cellState,
               this.playerName, this.playerName, errorText, style1,
               bStyle1, paintParts);

            // Paint Player Number
            bStyle1.Top = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Left = advancedBorderStyle.Left;
            bStyle1.Bottom = advancedBorderStyle.Bottom; //DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Right = DataGridViewAdvancedCellBorderStyle.None;
            style1.Font = new Font(cellStyle.Font.FontFamily, 7, FontStyle.Bold);
            style1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            base.Paint(graphics, clipBounds, this.setCellBounds(cellBounds, "playerNumber"), rowIndex, cellState,
               this.playerNumber, this.playerNumber, errorText, style1,
               bStyle1, paintParts);
            */
            // Paint GFT Status
            bStyle1.Top = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Left = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Right = advancedBorderStyle.Right;
            if (this.gftStatus.Length > 15)
            {
                style1.Font = new Font(cellStyle.Font.FontFamily, 6, FontStyle.Regular);
            }
            else
            {
                style1.Font = new Font(cellStyle.Font.FontFamily, 7, FontStyle.Regular);
            }
            base.Paint(graphics, clipBounds, this.setCellBounds(cellBounds, "gftStatus"), rowIndex, cellState,
               this.gftStatus, this.gftStatus, errorText, style1,
               bStyle1, paintParts);
            
            // Paint Command Status
            bStyle1.Top = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Left = advancedBorderStyle.Left;
            bStyle1.Bottom = advancedBorderStyle.Bottom;
            bStyle1.Right = advancedBorderStyle.Right;
            style1.Font = new Font(cellStyle.Font.FontFamily, 7, FontStyle.Regular);
            base.Paint(graphics, clipBounds, this.setCellBounds(cellBounds, "gftCommands"), rowIndex, cellState,
               this.commandStatus, this.commandStatus, errorText, style1,
               bStyle1, paintParts);
            
            // Paint the area the image will go..
            
            bStyle1.Top = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Left = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            bStyle1.Right = DataGridViewAdvancedCellBorderStyle.None;
            //bStyle1.Right = advancedBorderStyle.Right;
            bStyle1.Bottom = advancedBorderStyle.Bottom;
            base.Paint(graphics, clipBounds, this.setCellBounds(cellBounds, "gftBattery"), rowIndex, cellState,
               "", "", errorText, style1,
               bStyle1, paintParts);
            if (this.Image != null)
            {
               // Draw the image clipped to the cell.
                cellBounds = this.setCellBounds(cellBounds, "gftBattery");
                Bitmap bmp = new Bitmap(this.Image);
                Graphics g = Graphics.FromImage(bmp);
                g.FillRectangle(new SolidBrush(style1.SelectionBackColor), cellBounds);
                g.Dispose();
                System.Drawing.Imaging.ImageAttributes attr = new System.Drawing.Imaging.ImageAttributes();
                Rectangle dstRect = new Rectangle(cellBounds.X, cellBounds.Y, bmp.Width, bmp.Height);
                graphics.DrawImage(bmp, dstRect, 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, attr);
                if (false && this.mode != 0x81)
                {
                    // Don't show percentage on battery if device is charging
                    style1.BackColor = Color.FromArgb(0, 255, 255, 255);
                    style1.SelectionBackColor = Color.FromArgb(0, 255, 255, 255);
                    dstRect = new Rectangle(cellBounds.X + 1, cellBounds.Y - 2, bmp.Width - 1, bmp.Height + 2);
                    style1.Font = new Font(cellStyle.Font.FontFamily, 7, FontStyle.Bold);
                    style1.ForeColor = Color.FromArgb(255, 255, 255, 255);
                    base.Paint(graphics, clipBounds, dstRect, rowIndex, cellState, battery_pct.ToString() + "%", battery_pct.ToString() + "%", errorText, style1, bStyle1, paintParts);
                }
            }
        }

        private batteryStatusColumn OwningPlayerColumn
        {
            get { return this.OwningColumn as batteryStatusColumn; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace gForceTrackerWireless
{
    class hitImage
    {
        private int top = 0;
        private int bottom = 0;
        private int left = 0;
        private int right = 0;
        private int azOffset = 0;
        private int elOffset = 0;
        private double azCutoff = 0.0;
        private double elCutoff = 0.0;
        private int azRange = 0;
        private int elRange = 0;
        private int azChoke = 0;
        private int elChoke = 0;
        private int radius = 10;
        private Color green = Color.LimeGreen;
        private Color yellow = Color.Yellow;
        private Color red = Color.Red;
        private double alphaFactor = 0.25;
        private Bitmap image;

        private double azStart = 0;
        private double azEnd = 0;
        private int elStart = 0;
        private int elEnd = 0;
        private int dheight = 0;
        private int dwidth = 0;
        private int centerX = 0;
        private int centerY = 0;
        private double factorX = 0;
        private double factorY = 0;

        public hitImage(int top, int bottom, int left, int right, int azOffset, int elOffset, double azCutoff, double elCutoff, int azRange, int elRange, int azChoke, int elChoke, Bitmap image)
        {
            this.top = top;
            this.bottom = bottom;
            this.left = left;
            this.right = right;
            this.azOffset = azOffset;
            this.elOffset = elOffset;
            this.azCutoff = azCutoff;
            this.elCutoff = elCutoff;
            this.azRange = azRange;
            this.elRange = elRange;
            this.azChoke = azChoke;
            this.elChoke = elChoke;
            this.image = image;

            this.azStart = this.azOffset - (this.azRange / 2);
            this.azEnd = this.azOffset + (this.azRange / 2);
            this.elStart = this.elOffset - (this.elRange / 2);
            this.elEnd = this.elOffset + (this.elRange / 2);
            this.dheight = this.image.Height - this.top - this.bottom;
            this.dwidth = this.image.Width - this.left - this.right;
            this.centerX = this.left + (this.dwidth / 2);
            this.centerY = this.top + (this.dheight / 2);
            this.factorX = this.dwidth / 90 * this.azCutoff;
            this.factorY = this.dheight / 90 * this.elCutoff;

        }
        public Bitmap render(List<Form1.IMPACT_DATA> xy, Form1.IMPACT_DATA hxy = new Form1.IMPACT_DATA(), double alphaFactor = 1)
        {
            SolidBrush brush = new SolidBrush(this.green);
            Graphics g = Graphics.FromImage(this.image);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            foreach (Form1.IMPACT_DATA impData in xy)
            {
                drawPoint(impData, ref this.image, ref brush, ref g, alphaFactor);
            }
            if (hxy.linR > 0 || hxy.rotR > 0) // Do we want to highlight an impact?
            {
                drawPoint(hxy, ref this.image, ref brush, ref g, 1, true);
            }
            return this.image;
        }

        public Bitmap render(Form1.IMPACT_DATA xy)
        {
            List<Form1.IMPACT_DATA> xyList = new List<Form1.IMPACT_DATA>();
            xyList.Add(xy);
            return render(xyList);
        }

        private void drawPoint(Form1.IMPACT_DATA xy, ref Bitmap image, ref SolidBrush brush, ref Graphics g, double alphaFactor, bool outline = false)
        {
            double az = xy.azimuth;
            double el = 0 - xy.elevation;
            // Adjust Azimuth for FRONT Views
            if (this.azOffset >= 0 && this.azOffset <= 45 && az >= 225)
            {
                az = az - 360;
            }
            else if (this.azOffset >= -45 && this.azOffset < 0 && az <= 135)
            {
                az = az + 360;
            }
            if ((az >= this.azStart && az <= azEnd) && (el >= this.elStart && el <= this.elEnd))
            {
                // adjust az/el to the current view port
                az = az - this.azOffset;
                el = el - this.elOffset;

                // Choke off points that are near or past the edges (not exact..)
                if (az > this.azChoke / 2) az = this.azChoke / 2 + Math.Sqrt(Math.Abs(az) - this.azChoke / 2);
                else if (az < 0 - this.azChoke / 2) az = 0 - this.azChoke / 2 - Math.Sqrt(Math.Abs(az) - this.azChoke / 2);

                if (el > this.elChoke / 2) el = this.elChoke / 2 + Math.Sqrt(Math.Abs(el) - this.elChoke / 2);
                else if (el < 0 - this.elChoke / 2) el = 0 - this.elChoke / 2 - Math.Sqrt(Math.Abs(el) - this.elChoke / 2);


                // General warping to account for facial features
                // Make it an oval...
                double x = az / (this.azRange / 2);
                double y = el / (this.elRange / 3);

                // Account for a chin
                bool isChin = false;
                bool isBackNeck = false;
                bool isCenterChin = false;
                if (this.azOffset < 0 && this.azOffset > -180) // Left, Front-Left, Back-Left, Front-Left-Top etc..
                {
                    if (az > 0 && el > 0) isChin = true;
                }
                else if (this.azOffset > 0 && this.azOffset < 180) // Right, Front-Right, Back-Right, Front-Right-Top etc..
                {
                    if (az < 0 && el > 0) isChin = true;
                }
                else if (this.azOffset == 0 && this.elOffset < 0) // Front-Top
                {
                    if (el > 0) isCenterChin = true;
                }
                else if (this.azOffset > 90 && this.azOffset < 270) // Right, Front-Right, Back-Right, Front-Right-Top etc..
                {
                    if (az < 0 && el > 0) isBackNeck = true;
                }

                double yCirc = y * Math.Sqrt(1 - 0.5 * Math.Pow(x, 2));
                double xCirc = x;
                if (!isChin)
                {
                    if (isBackNeck)
                    {
                        xCirc = x * Math.Sqrt(1 - 0.25 * Math.Pow(y, 2));
                    } 
                    else if (isCenterChin)
                    {
                        xCirc = x * Math.Sqrt(1 - 0.5 * (y * 1.5));
                    }
                    else
                    {
                        xCirc = x * Math.Sqrt(1 - 0.5 * Math.Pow(y, 2));
                    }
                }
                // Need to check for invalid values for x..

                x = xCirc * (this.azRange / 2) * factorX;
                y = yCirc * (this.elRange / 3) * factorY;


                // Draw outline around point?
                if (outline)
                {
                    brush.Color = Color.Black;
                    g.DrawEllipse(new Pen(brush, 4), new Rectangle((int)Math.Round(this.centerX - x - (this.radius + 2)), (int)Math.Round(this.centerY + y - (this.radius + 2)), (this.radius + 2) * 2, (this.radius + 2) * 2));
                    //g.FillEllipse(brush, new Rectangle((int)Math.Round(this.centerX - x - (this.radius + 2)), (int)Math.Round(this.centerY + y - (this.radius + 2)), (this.radius + 2) * 2, (this.radius + 2) * 2));
                }

                if (xy.linR >= xy.thresholdG || xy.rotR >= xy.thresholdR)
                {
                    // red
                    brush.Color = this.red;
                }
                else if (xy.linR >= xy.thresholdG * 0.9 || xy.rotR >= xy.thresholdR * 0.9)
                {
                    // yellow;
                    brush.Color = this.yellow;
                }
                else
                {
                    // green;
                    brush.Color = this.green;
                }

                // Calculate Alpha
                double gForcePCT = xy.linR / xy.thresholdG;
                double rotRPCT = xy.rotR / xy.thresholdR;
                double alarmPCT = gForcePCT > rotRPCT ? gForcePCT : rotRPCT;
                int alpha = alarmPCT > 1 ? (int)Math.Round(255 * alphaFactor) : (int)Math.Round(alarmPCT * 255 * alphaFactor);

                brush.Color = Color.FromArgb(alpha, brush.Color);
                // Draw the circle..
                g.FillEllipse(brush, new Rectangle((int)Math.Round(this.centerX - x - this.radius), (int)Math.Round(this.centerY + y - this.radius), this.radius * 2, this.radius * 2));
            }
            else
            {
                // Impact is not in visible area
            }
        }
    }
}

﻿namespace WirelessGFTViewer
{
    partial class RxLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RxLog));
            this.txtPacketLog = new System.Windows.Forms.TextBox();
            this.chkCapture = new System.Windows.Forms.CheckBox();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.chkSelDevice = new System.Windows.Forms.CheckBox();
            this.chkPacketType = new System.Windows.Forms.CheckBox();
            this.chkBackChannel = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtPacketLog
            // 
            this.txtPacketLog.Location = new System.Drawing.Point(13, 13);
            this.txtPacketLog.MaximumSize = new System.Drawing.Size(1064, 325);
            this.txtPacketLog.MinimumSize = new System.Drawing.Size(1064, 325);
            this.txtPacketLog.Multiline = true;
            this.txtPacketLog.Name = "txtPacketLog";
            this.txtPacketLog.ReadOnly = true;
            this.txtPacketLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPacketLog.Size = new System.Drawing.Size(1064, 325);
            this.txtPacketLog.TabIndex = 0;
            // 
            // chkCapture
            // 
            this.chkCapture.AutoSize = true;
            this.chkCapture.Location = new System.Drawing.Point(13, 350);
            this.chkCapture.Name = "chkCapture";
            this.chkCapture.Size = new System.Drawing.Size(130, 17);
            this.chkCapture.TabIndex = 1;
            this.chkCapture.Text = "Capture Raw Packets";
            this.chkCapture.UseVisualStyleBackColor = true;
            // 
            // btnClearLog
            // 
            this.btnClearLog.Location = new System.Drawing.Point(1002, 344);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(75, 23);
            this.btnClearLog.TabIndex = 2;
            this.btnClearLog.Text = "Clear Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // chkSelDevice
            // 
            this.chkSelDevice.AutoSize = true;
            this.chkSelDevice.Location = new System.Drawing.Point(150, 350);
            this.chkSelDevice.Name = "chkSelDevice";
            this.chkSelDevice.Size = new System.Drawing.Size(169, 17);
            this.chkSelDevice.TabIndex = 3;
            this.chkSelDevice.Text = "Capture Only Selected Device";
            this.chkSelDevice.UseVisualStyleBackColor = true;
            // 
            // chkPacketType
            // 
            this.chkPacketType.AutoSize = true;
            this.chkPacketType.Location = new System.Drawing.Point(326, 350);
            this.chkPacketType.Name = "chkPacketType";
            this.chkPacketType.Size = new System.Drawing.Size(132, 17);
            this.chkPacketType.TabIndex = 4;
            this.chkPacketType.Text = "Capture Packet Types";
            this.chkPacketType.UseVisualStyleBackColor = true;
            // 
            // chkBackChannel
            // 
            this.chkBackChannel.AutoSize = true;
            this.chkBackChannel.Location = new System.Drawing.Point(465, 350);
            this.chkBackChannel.Name = "chkBackChannel";
            this.chkBackChannel.Size = new System.Drawing.Size(133, 17);
            this.chkBackChannel.TabIndex = 5;
            this.chkBackChannel.Text = "Capture Back Channel";
            this.chkBackChannel.UseVisualStyleBackColor = true;
            // 
            // RxLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 379);
            this.Controls.Add(this.chkBackChannel);
            this.Controls.Add(this.chkPacketType);
            this.Controls.Add(this.chkSelDevice);
            this.Controls.Add(this.btnClearLog);
            this.Controls.Add(this.chkCapture);
            this.Controls.Add(this.txtPacketLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1105, 418);
            this.MinimumSize = new System.Drawing.Size(1105, 418);
            this.Name = "RxLog";
            this.Text = "RxLog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPacketLog;
        private System.Windows.Forms.CheckBox chkCapture;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.CheckBox chkSelDevice;
        private System.Windows.Forms.CheckBox chkPacketType;
        private System.Windows.Forms.CheckBox chkBackChannel;
    }
}
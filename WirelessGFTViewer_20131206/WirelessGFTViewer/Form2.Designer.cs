﻿namespace WirelessGFTViewer
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_button_Clone = new System.Windows.Forms.Button();
            this.m_button_Bind = new System.Windows.Forms.Button();
            this.gftWirelessView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gftWirelessView)).BeginInit();
            this.SuspendLayout();
            // 
            // m_button_Clone
            // 
            this.m_button_Clone.AutoSize = true;
            this.m_button_Clone.Location = new System.Drawing.Point(98, 135);
            this.m_button_Clone.Name = "m_button_Clone";
            this.m_button_Clone.Size = new System.Drawing.Size(88, 24);
            this.m_button_Clone.TabIndex = 70;
            this.m_button_Clone.Text = "Unbind";
            this.m_button_Clone.UseVisualStyleBackColor = true;
            // 
            // m_button_Bind
            // 
            this.m_button_Bind.AutoSize = true;
            this.m_button_Bind.Location = new System.Drawing.Point(98, 104);
            this.m_button_Bind.Name = "m_button_Bind";
            this.m_button_Bind.Size = new System.Drawing.Size(88, 26);
            this.m_button_Bind.TabIndex = 69;
            this.m_button_Bind.Text = "Bind";
            this.m_button_Bind.UseVisualStyleBackColor = true;
            // 
            // gftWirelessView
            // 
            this.gftWirelessView.AllowUserToAddRows = false;
            this.gftWirelessView.AllowUserToDeleteRows = false;
            this.gftWirelessView.AllowUserToOrderColumns = true;
            this.gftWirelessView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gftWirelessView.BackgroundColor = System.Drawing.Color.Silver;
            this.gftWirelessView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gftWirelessView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gftWirelessView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gftWirelessView.Location = new System.Drawing.Point(333, 12);
            this.gftWirelessView.MultiSelect = false;
            this.gftWirelessView.Name = "gftWirelessView";
            this.gftWirelessView.ReadOnly = true;
            this.gftWirelessView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gftWirelessView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gftWirelessView.RowHeadersWidth = 30;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            this.gftWirelessView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.gftWirelessView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gftWirelessView.RowTemplate.Height = 30;
            this.gftWirelessView.RowTemplate.ReadOnly = true;
            this.gftWirelessView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gftWirelessView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.gftWirelessView.Size = new System.Drawing.Size(308, 387);
            this.gftWirelessView.TabIndex = 71;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 262);
            this.Controls.Add(this.gftWirelessView);
            this.Controls.Add(this.m_button_Clone);
            this.Controls.Add(this.m_button_Bind);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.gftWirelessView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_button_Clone;
        private System.Windows.Forms.Button m_button_Bind;
        private System.Windows.Forms.DataGridView gftWirelessView;
    }
}
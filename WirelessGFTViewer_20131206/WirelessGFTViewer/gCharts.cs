﻿using Microsoft.Win32.SafeHandles;
using Microsoft.VisualBasic;
using System.ComponentModel;
//using ExcelLibrary.SpreadSheet;
//using ExcelLibrary.CompoundDocumentFormat;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Collections;
using System.Threading;
using System;
using System.Timers;
using System.Globalization;
using ArtaFlexHidWin;


namespace gForceTrackerWireless
{

    public partial class Form1 : UsbAwareForm
    {
        private void loadFileAuto(object sender, TabControlEventArgs e)
        {
            //string Chosen_File = "";
            Int32 maxSID = gDataGetSessionLastSID();
            //if (bUploadInProgress) return;
            // populate datagrid
            //gDataGetSession(DateSessionQueryMode.DateSessionQueryModeAll, DateTime.Now, DateTime.Now, dataGridView1);

            //dataGetImpact(maxSID, dataGridView2);
            //dataGetImpact(maxSID, dataGridView3);
            //if (logFileName == null)
            //{
            // logFileName = "default.csv";
            //chart1.Hide();
            //chart2.Hide();
            //lblChartTime.Hide();
            // return;
            //}
            //chart1.Show();
            //chart2.Show();
            //lblChartTime.Show();
            //Chosen_File = logFileName;
            if (curSessionID == 0)
                curSessionID = maxSID;

            //drawChart(maxSID, gDataGetSessionSidThreshold(maxSID), chartRaw1);
            //DrawImpactChart(0, chartRawLin, chartRawGyro);
            //chartRaw1.Show();
            //chartRawLin.Show();

            //drawChart(maxSID, gDataGetSessionSidThreshold(maxSID), chartViewer);
            //DrawImpactChart(0, chartRawLinW, chartRawGyroW);
            //chartViewer.Show();
            //chartRawLinW.Show();
        }

        void drawHitCountArrow(List<int> location, PictureBox picute)
        {
            //return;
            System.Drawing.Graphics g;
            Pen p;
            g = myPaintEvent.Graphics;
            //top
            switch (hitPosition)
            {
                case "TOP":
                    p = new Pen(System.Drawing.Color.Red, 3);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);
                    p.DashStyle = DashStyle.Solid;
                    g.DrawLine(p, new PointF(170, 5), new PointF(170, 50));
                    break;

                //left
                case "LEFT":
                    p = new Pen(System.Drawing.Color.Blue, 3);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);
                    //p.DashStyle = DashStyle.DashDot;
                    p.DashStyle = DashStyle.Solid;
                    g.DrawLine(p, new PointF(290, 110), new PointF(233, 110));
                    break;
                case "RIGHT":
                    //right
                    p = new Pen(System.Drawing.Color.Blue, 3);
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);
                    //p.DashStyle = DashStyle.DashDot;
                    g.DrawLine(p, new PointF(41, 110), new PointF(100, 110));
                    break;
                case "FRONT":

                    //front
                    p = new Pen(System.Drawing.Color.Blue, 3);
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);
                    //p.DashStyle = DashStyle.DashDot;
                    g.DrawLine(p, new PointF(77, 200), new PointF(105, 173));
                    break;
                case "BACK":

                    //back
                    p = new Pen(System.Drawing.Color.Blue, 3);
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);
                    //p.DashStyle = DashStyle.DashDot;
                    g.DrawLine(p, new PointF(260, 35), new PointF(224, 72));
                    break;
                default:
                    break;

            }
        }
        void drawHitArrow(PictureBox pictureBoxHead, IMPACT_DATA impData, List<IMPACT_DATA> impList)
        {
            hitImage image;
            switch (hitPosition)
            {
                case "TOP":
                    if (impData.azimuth > 90 && impData.azimuth < 270)
                        image = new hitImage(66, 104, 75, 75, 180, -45, 0.65, 0.65, 150, 180, 135, 135, WirelessGFTViewer.Properties.Resources.BACK_TOP);
                    else
                        image = new hitImage(57, 75, 73, 73, 0, -35, 0.65, 0.65, 150, 180, 135, 135, WirelessGFTViewer.Properties.Resources.FRONT_TOP);
                    break;
                case "LEFT":
                    image = new hitImage(80, 100, 68, 52, 270, 0, 0.6, 0.7, 180, 180, 180, 135, WirelessGFTViewer.Properties.Resources.LEFT);
                    break;
                case "RIGHT":
                    image = new hitImage(80, 100, 52, 68, 90, 0, 0.6, 0.7, 180, 180, 135, 135, WirelessGFTViewer.Properties.Resources.RIGHT);
                    break;
                case "BACK":
                    image = new hitImage(70, 100, 85, 85, 180, 0, 0.65, 0.65, 150, 180, 135, 135, WirelessGFTViewer.Properties.Resources.BACK);
                    break;
                case "BOTTOM":
                    if (impData.azimuth > 45 && impData.azimuth < 135)
                        image = new hitImage(80, 100, 52, 68, 90, 0, 0.6, 0.7, 180, 180, 135, 135, WirelessGFTViewer.Properties.Resources.RIGHT);
                    else if (impData.azimuth >= 135  && impData.azimuth < 235)
                        image = new hitImage(70, 100, 85, 85, 180, 0, 0.65, 0.65, 150, 180, 135, 135, WirelessGFTViewer.Properties.Resources.BACK);
                    else if (impData.azimuth >= 235 && impData.azimuth < 315)
                        image = new hitImage(80, 100, 68, 52, 270, 0, 0.6, 0.7, 180, 180, 135, 135, WirelessGFTViewer.Properties.Resources.LEFT);
                    else
                        image = new hitImage(75, 90, 90, 90, 0, 0, 0.7, 0.7, 150, 180, 135, 125, WirelessGFTViewer.Properties.Resources.FRONT);
                    break;
                case "FRONT":
                default:
                    image = new hitImage(75, 90, 90, 90, 0, 0, 0.7, 0.7, 150, 180, 135, 125, WirelessGFTViewer.Properties.Resources.FRONT);
                    break;
            }
            pictureBoxHead.Image = image.render(impList, impData, 0.8);
            System.Diagnostics.Debug.WriteLine(hitPosition);
        }
        void drawHitArrow(PictureBox pictureBoxHead)
        {
            //return;
            //System.Drawing.Graphics g;
            //Pen p;
            //g = myPaintEvent.Graphics;
            //top
            switch (hitPosition)
            {
                case "TOP":
                    // p = new Pen(System.Drawing.Color.Red, 3);
                    //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    //p.StartCap = LineCap.Round;
                    // p.EndCap = LineCap.ArrowAnchor;
                    //p.CustomEndCap = new AdjustableArrowCap(3, 4);
                    // p.DashStyle = DashStyle.Solid;
                    // g.DrawLine(p, new PointF(170, 5), new PointF(170, 50));

                    // pictureBoxHead.Image = System.Drawing.Image.FromFile("dummyheadFrontTop.jpg");
                    if (hitPositionColor == "red")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontTopRed;
                    else if (hitPositionColor == "green")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontTopGreen;
                    else
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontTopYellow;
                    break;

                //left
                case "LEFT":
                    // p = new Pen(System.Drawing.Color.Blue, 3);
                    // g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    // p.StartCap = LineCap.Round;
                    // p.EndCap = LineCap.ArrowAnchor;
                    //  p.CustomEndCap = new AdjustableArrowCap(3, 4);
                    //p.DashStyle = DashStyle.DashDot;
                    // p.DashStyle = DashStyle.Solid;
                    // g.DrawLine(p, new PointF(290, 110), new PointF(233, 110));
                    //pictureBoxHead.Image = System.Drawing.Image.FromFile("dummyheadSideLeft.jpg");
                    //System.Drawing.Bitmap bitmap1 = WirelessGFTViewer.Properties.Resources.dummyheadFrontBottom;
                    if (hitPositionColor == "red")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.LeftLeftRed;
                    else if (hitPositionColor == "green")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.LeftLeftGreen;
                    else
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.LeftLeftYellow;
                    break;
                case "RIGHT":
                    //right
#if false
                    p = new Pen(System.Drawing.Color.Blue, 3);
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);

#endif
                    //p.DashStyle = DashStyle.DashDot;
                    //g.DrawLine(p, new PointF(41, 110), new PointF(100, 110));
                    // pictureBoxHead.Image = System.Drawing.Image.FromFile("dummyheadSideRight.jpg");
                    if (hitPositionColor == "red")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.RightRightRed;
                    else if (hitPositionColor == "green")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.RightRightGreen;
                    else
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.RightRightYellow;
                    break;
                case "FRONT":

                    //front
#if false
                    p = new Pen(System.Drawing.Color.Blue, 3);
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);
#endif
                    //p.DashStyle = DashStyle.DashDot;
                    //g.DrawLine(p, new PointF(77, 200), new PointF(105, 173));
                    //pictureBoxHead.Image = System.Drawing.Image.FromFile("dummyheadFrontFront.jpg");
                    if (hitPositionColor == "red")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontFrontRed;
                    else if (hitPositionColor == "green")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontFrontGreen;
                    else
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontFrontYellow;
                    break;
                case "BACK":

                    //back
#if false
                    p = new Pen(System.Drawing.Color.Blue, 3);
                    p.StartCap = LineCap.Round;
                    p.EndCap = LineCap.ArrowAnchor;
                    p.CustomEndCap = new AdjustableArrowCap(3, 4);
#endif
                    //p.DashStyle = DashStyle.DashDot;

                    //g.DrawLine(p, new PointF(260, 35), new PointF(224, 72));
                    // pictureBoxHead.Image = System.Drawing.Image.FromFile("dummyheadSideBack.jpg");
                    if (hitPositionColor == "red")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.LeftBackRed;
                    else if (hitPositionColor == "green")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.LeftBackGreen;
                    else
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.LeftBackYellow;
                    break;
                default:
                    //pictureBoxHead.Image = System.Drawing.Image.FromFile("dummyheadFrontBottom.jpg");
                    if (hitPositionColor == "red")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontBottomRed;
                    else if (hitPositionColor == "green")
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontBottomGreen;
                    else
                        pictureBoxHead.Image = WirelessGFTViewer.Properties.Resources.FrontBottomYellow;
                    break;


            }
        }


        void drawChart(Int32 mySessionID, int mThreshold, Chart chart1)
        {


            int countOverThreshold = 0;
            //int elements = 100;
            int count = 0;
            double linRes = 0;
            DateTime baseDate;
            DateTime tmpTime;
            int millisecond = 0;
            //const string f = "C:\\00\\g-force\\test.csv";
            // creates 100 random X points
#if false
            Random r = new Random();
            List<double> xValues = new List<double>();
            double currentX = 0;
            for (int i = 0; i < elements; i++)
            {
                xValues.Add(currentX);
                currentX = currentX + r.Next(1, 100);
            }

            // creates 100 random Y values
            List<double> yValues = new List<double>();
            for (int i = 0; i < elements; i++)
            {
                yValues.Add(r.Next(0, 20));
            }
#endif

            // remove all previous series
            chart1.Series.Clear();

            var series = chart1.Series.Add("");
            series.ChartType = SeriesChartType.Column;
            series.XValueType = ChartValueType.Auto;
            //chart1.Titles.Add("Shock Peak Point");
            SqlCeConnection con;
            using (con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select IMPACT_TIME,LINEAR_ACC_RESULTANT,millisecond  from Events where SID = @myID", con);
                    myCommand.Parameters.Add("@myID", SqlDbType.Int).Value = mySessionID;
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {

                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        baseDate = myReader.GetDateTime(0);
                        linRes = myReader.GetDouble(1);
                        millisecond = myReader.GetInt32(2);
                        tmpTime = new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, baseDate.Hour, baseDate.Minute, baseDate.Second, millisecond);

                        // var xDate = baseDate.AddSeconds(1);

                        var xDate = count;// baseDate.AddSeconds(0);

                        series.Points.AddXY(xDate, linRes);
                        if (linRes >= mThreshold)
                        {
                            series.Points[count].Color = System.Drawing.Color.Red;
                            countOverThreshold++;

                        }
                        else if (linRes >= mThreshold * 0.9)
                        {
                            series.Points[count].Color = System.Drawing.Color.Yellow;

                        }
                        else
                            series.Points[count].Color = System.Drawing.Color.LimeGreen;
                        series.Points[count].AxisLabel = tmpTime.ToString("HH:mm:ss.fff");

                        // chart1.Series["Test1"].Points.AddXY
                        //             (myDateTime.Ticks, Convert.ToInt32(match.Groups[11].Value, 10));
                        count++;
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }


                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }


            // show an X label every 3 Minute

            chart1.ChartAreas[0].AxisX.ScaleView.MinSize = 4;
            // chart1.ChartAreas[0].AxisX.ScaleView.MinSizeType = DateTimeIntervalType.Hours;
            chart1.ChartAreas[0].AxisX.Interval = 0;
            chart1.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Number;
            //txtBoxTabChartTotalRecords.Text = Convert.ToString(count);
            //txtBoxTabChartROA.Text = Convert.ToString(countOverThreshold);
            // chart1.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
            //pictureBox2.Hide()
            //drawHitArrow();

        }

        void drawChart(Int32 mySessionID, int mThreshold, Chart chart1, string EventsTable)
        {


            int countOverThreshold = 0;
            //int elements = 100;
            int count     = 0;
            double linRes = 0;
            DateTime baseDate;
            DateTime tmpTime;
            int millisecond = 0;
            //const string f = "C:\\00\\g-force\\test.csv";
            // creates 100 random X points
#if false
            Random r = new Random();
            List<double> xValues = new List<double>();
            double currentX = 0;
            for (int i = 0; i < elements; i++)
            {
                xValues.Add(currentX);
                currentX = currentX + r.Next(1, 100);
            }

            // creates 100 random Y values
            List<double> yValues = new List<double>();
            for (int i = 0; i < elements; i++)
            {
                yValues.Add(r.Next(0, 20));
            }
#endif
            // remove all previous series
            chart1.Series.Clear();

            var series = chart1.Series.Add("");
            series.ChartType  = SeriesChartType.Column;
            series.XValueType = ChartValueType.Auto;
          //chart1.Titles.Add("Shock Peak Point");

            SqlCeConnection con;
            using (con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    //SqlCeCommand myCommand = new SqlCeCommand("select IMPACT_TIME,LINEAR_ACC_RESULTANT,millisecond  from " + EventsTable + " where SID = @myID", mycon);
                    SqlCeCommand myCommand = new SqlCeCommand("select IMPACT_TIME,LINEAR_ACC_RESULTANT,millisecond  from " + EventsTable + " where SID=@myID" + " order by IMPACT_TIME", con);
                    myCommand.Parameters.Add("@myID", SqlDbType.Int).Value = mySessionID;
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {

                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        linRes = myReader.GetDouble(1);
                        baseDate = myReader.GetDateTime(0);
                        int millis = baseDate.Millisecond;
                        millisecond = myReader.GetInt32(2);
                        tmpTime = new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, baseDate.Hour, baseDate.Minute, baseDate.Second, millisecond);

                        if (gftGMT_Enable)
                            tmpTime = tmpTime.ToLocalTime();

                        var xDate = count;
                        series.Points.AddXY(xDate, linRes);
                        if (linRes >= mThreshold)
                        {
                            series.Points[count].Color = System.Drawing.Color.Red;
                            countOverThreshold++;
                        }
                        else if (linRes >= mThreshold * 0.9)
                        {
                            series.Points[count].Color = System.Drawing.Color.Yellow;

                        }
                        else
                            series.Points[count].Color = System.Drawing.Color.LimeGreen;
                        series.Points[count].AxisLabel = tmpTime.ToString("HH:mm:ss.fff");

                        // chart1.Series["Test1"].Points.AddXY
                        //             (myDateTime.Ticks, Convert.ToInt32(match.Groups[11].Value, 10));
                        count++;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }


                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }

            // show an X label every 3 Minute

            chart1.ChartAreas[0].AxisX.ScaleView.MinSize = 4;         
            chart1.ChartAreas[0].AxisX.Interval = 0;
            chart1.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Number;

          //chart1.ChartAreas[0].AxisX.ScaleView.MinSizeType = DateTimeIntervalType.Hours;
          //txtBoxTabChartTotalRecords.Text = Convert.ToString(count);
          //txtBoxTabChartROA.Text = Convert.ToString(countOverThreshold);
          //chart1.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
          //pictureBox2.Hide()
          //drawHitArrow();
        }

#if false
        private void Impacts_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e, DataGridView dataGridView2, Chart chartReportLin, Chart chartReportGyro, PictureBox pictureBoxHead)
        {
            Chart chart1 = (Chart)sender;
            // Call HitTest
            HitTestResult result = chart1.HitTest(e.X, e.Y);

            //var series = chart2.Series.Add("X");
            // Reset Data Point Attributes
            foreach (DataPoint point in chart1.Series[0].Points)
            {
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 1;
            }

            // If the mouse if over a data point
            if (result.ChartElementType == ChartElementType.DataPoint)
            {
                int lineStart;
                // Find selected data point
                DataPoint point = chart1.Series[0].Points[result.PointIndex];

                // Change the appearance of the data point
                point.BackSecondaryColor = Color.White;
                point.BackHatchStyle = ChartHatchStyle.Percent25;
                point.BorderWidth = 2;
                //lblChartTime.Text = point.AxisLabel.ToString();
                lineStart = (int)point.XValue;
                DrawImpactChart(lineStart, chartReportLin, chartReportGyro);
                if (dataGridView2.RowCount > lineStart)
                {
                    dataGridView2.CurrentCell = dataGridView2[0, lineStart];
                    dataGridView2.Rows[lineStart].Selected = true;
                }

                drawHitArrow(pictureBoxHead);

                toolStripStatusLabelGFT.Text = "Node " + currentNodeID.ToString("d2");
                toolStripStatusLabelGFT.BackColor = point.Color;
                SetTimer(this.Handle, 0x900 + currentNodeID, 500, null);

            }//over a data point



        }//end mouse move function
#endif

        private void Impacts_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e,
                                       DataGridView dataGridView2, Chart chartReportLin, Chart chartReportGyro,
                                       PictureBox pictureBoxHead, string eventsTable)
        {
            Chart chart1 = (Chart)sender;
            // Call HitTest
            HitTestResult result = chart1.HitTest(e.X, e.Y);

            //var series = chart2.Series.Add("X");
            // Reset Data Point Attributes

            if (!(result.ChartElementType == ChartElementType.DataPoint))
                return;


            foreach (DataPoint point in chart1.Series[1].Points)
            {
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 2;
            }

            foreach (DataPoint point in chart1.Series[3].Points)
            {
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 2;
            }

            // If the mouse if over a data point
            if (result.ChartElementType == ChartElementType.DataPoint && lpHub != null)
            {
                if (chart1.Series[1].Points.Count() > result.PointIndex)
                {
                    // Get the impact EID
                    lpHub.lpDevices[currentNodeID - 1].selectedImpact = timeline[result.PointIndex].eid;
                    // Get the "real" x value, excluding the spacer entries
                    int rX = 0;
                    int i = 0;
                    foreach (IMPACT_DATA impData in timeline)
                    {
                        if (i >= result.PointIndex) break;
                        if (impData.eid > 0) rX++;
                        i++;
                    }
                    //UpdateNodeControlLayout(currentNodeID);

                    //lpHub.lpDevices[currentNodeID - 1].selectedImpact = Args[currentNodeID - 1].recvFIFO[result.PointIndex].eid;

                    int lineStart;
                    // Find selected data point
                    DataPoint point = chart1.Series[1].Points[result.PointIndex];

                    // Change the appearance of the data point
                    point.BackSecondaryColor = Color.White;
                    point.BackHatchStyle = ChartHatchStyle.Percent25;
                    point.BorderWidth = 2;

                    DataPoint pointR = chart1.Series[3].Points[result.PointIndex];
                    // Change the appearance of the data point
                    pointR.BackSecondaryColor = Color.White;
                    pointR.BackHatchStyle = ChartHatchStyle.Percent25;
                    pointR.BorderWidth = 2;
                    //lblChartTime.Text = point.AxisLabel.ToString();
                    lineStart = rX;// (int)point.XValue;


                    //#if true //USE_OF_LIST                                                          //Dispay Gyro, Accelrometer Data, 2014.01.09
                    //if(List_or_Table)
                    DrawImpactChartByList(currentNodeID, lineStart, chartReportLin, chartReportGyro);
                    //#else
                    //else
                    // DrawImpactChart(lineStart, chartReportLin, chartReportGyro, eventsTable);
                    //#endif
                    if (dataGridView2.RowCount > lineStart)
                    {
                        dataGridView2.CurrentCell = dataGridView2[2, lineStart];
                        dataGridView2.Rows[lineStart].Selected = true;
                    }

                    //drawHitArrow(pictureBoxHead);
                    drawHitArrow(pictureBoxHead, timeline[result.PointIndex], Args[currentNodeID - 1].recvFIFO);
                    //toolStripStatusLabelGFT.Text = "Node " + currentNodeID.ToString("d2");
                    //toolStripStatusLabelGFT.BackColor = point.Color;
                    //SetTimer(this.Handle, 0x900+currentNodeID, 500, null);
                }
                

            }//over a data point

        }//end mouse move function



        private void TeamImpacts_Click(object sender, System.Windows.Forms.MouseEventArgs e,
                                       DataGridView dataGridView2, Chart chartReportLin, Chart chartReportGyro,
                                       PictureBox pictureBoxHead, string eventsTable)
        {
            Chart chart1 = (Chart)sender;
            // Call HitTest
            HitTestResult result = chart1.HitTest(e.X, e.Y);
            if (!(result.ChartElementType == ChartElementType.DataPoint))
                return;

            // If the mouse if over a data point
            if (result.ChartElementType == ChartElementType.DataPoint && lpHub != null)
            {
                
                DataPoint point = chart1.Series[0].Points[result.PointIndex]; // PointIndex should be the same as the teamImpacts index
                //lpHub.lpDevices[teamImpacts[result.PointIndex].UID].selectedImpact = teamImpacts[result.PointIndex].eid; // Set the impact that should be selected
                //wirelessViewer_OnRowSelected(this, new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, teamImpacts[result.PointIndex].UID+1)); // Node ID is expected to be +1
                lpHub.lpDevices[teamTimeline[result.PointIndex].UID].selectedImpact = teamTimeline[result.PointIndex].eid;
                wirelessViewer_OnRowSelected(this, new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, teamTimeline[result.PointIndex].UID + 1)); // Node ID is expected to be +1
            }//over a data point
        }

        private void TeamImpacts_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e,
                                       DataGridView dataGridView2, Chart chartReportLin, Chart chartReportGyro,
                                       PictureBox pictureBoxHead, string eventsTable)
        {
            Chart chart1 = (Chart)sender;
            // Call HitTest
            HitTestResult result = chart1.HitTest(e.X, e.Y);

            //var series = chart2.Series.Add("X");
            // Reset Data Point Attributes

            if (!(result.ChartElementType == ChartElementType.DataPoint))
                return;


            foreach (DataPoint point in chart1.Series[1].Points)
            {
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 2;
            }

            foreach (DataPoint point in chart1.Series[3].Points)
            {
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 2;
            }

            // If the mouse if over a data point
            if (result.ChartElementType == ChartElementType.DataPoint)
            {
                //UpdateNodeControlLayout(currentNodeID);

                int lineStart;
                // Find selected data point
                DataPoint point = chart1.Series[1].Points[result.PointIndex];

                // Change the appearance of the data point
                point.BackSecondaryColor = Color.White;
                //point.BackSecondaryColor = Color.FromArgb(100,255,255,255);
                point.BackHatchStyle = ChartHatchStyle.Percent25;
                point.BorderWidth = 2;

                DataPoint pointR = chart1.Series[3].Points[result.PointIndex];
                // Change the appearance of the data point
                pointR.BackSecondaryColor = Color.White;
                pointR.BackHatchStyle = ChartHatchStyle.Percent25;
                pointR.BorderWidth = 2;
                //lblChartTime.Text = point.AxisLabel.ToString();
                lineStart = (int)point.XValue;


                //#if true //USE_OF_LIST                                                          //Dispay Gyro, Accelrometer Data, 2014.01.09
                //if (List_or_Table)
                    //DrawImpactChartByList(currentNodeID, lineStart, chartReportLin, chartReportGyro);
                //#else
                //else
                    //DrawImpactChart(lineStart, chartReportLin, chartReportGyro, eventsTable);
                //#endif
                if (dataGridView2.RowCount > lineStart)
                {
                    dataGridView2.CurrentCell = dataGridView2[2, lineStart];
                    dataGridView2.Rows[lineStart].Selected = true;
                }

                //drawHitArrow(pictureBoxHead);

                //toolStripStatusLabelGFT.Text = "Node " + currentNodeID.ToString("d2");
                //toolStripStatusLabelGFT.BackColor = point.Color;
                //SetTimer(this.Handle, 0x900+currentNodeID, 500, null);

            }//over a data point

        }//end mouse move function

        private void DrawImpactChart(int lineStart, Chart chartRawLin, Chart chartRawGyro)
        {

            double[] myRes = new double[3];
            double[] my40 = new double[40];
            double[] maxPoint = new double[4];
            double[] myPoint = new double[3];
            //double myDouble;
            //double tDouble = 0;
            double azimuth = 0;
            double elevation = 0;
            int count = 0;
            byte[] dataX;
            byte[] dataY;
            byte[] dataZ;

            byte[] rdataX;
            byte[] rdataY;
            byte[] rdataZ;
            double mDataX, mDataY, mDataZ;
            byte i;
            double[] mTmpPoint = new double[3];
            double linearR = 0;
            double linearAlarmT = 0;
            //const string f = "C:\\00\\g-force\\test.csv";
            chartRawLin.Series.Clear();

            var seriesX = chartRawLin.Series.Add("Linear X");
            var seriesY = chartRawLin.Series.Add("Linear Y");
            var seriesZ = chartRawLin.Series.Add("Linear Z");
            var seriesV = chartRawLin.Series.Add("Linear Res.");
            seriesX.ChartType = SeriesChartType.Line;
            seriesX.XValueType = ChartValueType.Auto;
            seriesY.ChartType = SeriesChartType.Line;
            seriesY.XValueType = ChartValueType.Auto;
            seriesZ.ChartType = SeriesChartType.Line;
            seriesZ.XValueType = ChartValueType.Auto;
            seriesV.ChartType = SeriesChartType.Line;
            seriesV.XValueType = ChartValueType.Auto;
            seriesX.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            seriesY.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            seriesZ.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            seriesV.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));

            //gyro
            chartRawGyro.Series.Clear();

            var seriesRX = chartRawGyro.Series.Add("Rot. X");
            var seriesRY = chartRawGyro.Series.Add("Rot. Y");
            var seriesRZ = chartRawGyro.Series.Add("Rot. Z");
            var seriesRV = chartRawGyro.Series.Add("Rot. Res.");
            seriesRX.ChartType = SeriesChartType.Line;
            seriesRX.XValueType = ChartValueType.Auto;
            seriesRY.ChartType = SeriesChartType.Line;
            seriesRY.XValueType = ChartValueType.Auto;
            seriesRZ.ChartType = SeriesChartType.Line;
            seriesRZ.XValueType = ChartValueType.Auto;
            seriesRV.ChartType = SeriesChartType.Line;
            seriesRV.XValueType = ChartValueType.Auto;
            seriesRX.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            seriesRY.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            seriesRZ.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            seriesRV.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select LINEAR_ACC_RESULTANT, AZIMUTH,ELEVATION,dataX,dataY,dataZ,rdataX,rdataY,rdataZ from Events where SID = @myID", con);
                    myCommand.Parameters.Add("@myID", SqlDbType.Int).Value = curSessionID; //gDataGetSessionLastSID();
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        if (count == lineStart)
                        {
                            dataX = (byte[])myReader["dataX"];
                            dataY = (byte[])myReader["dataY"];
                            dataZ = (byte[])myReader["dataZ"];

                            rdataX = (byte[])myReader["rdataX"];
                            rdataY = (byte[])myReader["rdataY"];
                            rdataZ = (byte[])myReader["rdataZ"];

                            azimuth = (double)myReader["AZIMUTH"];
                            elevation = (double)myReader["ELEVATION"];
                            linearR = (double)myReader["LINEAR_ACC_RESULTANT"];
                            linearAlarmT = gDataGetSessionSidThreshold(curSessionID);
                            if (linearR >= linearAlarmT)
                                hitPositionColor = "red";
                            else if (linearR >= linearAlarmT * 0.9)
                                hitPositionColor = "yellow";
                            else
                                hitPositionColor = "green";
                            for (i = 0; i < 120; i++)
                            {
                                mDataX = AccDecode(dataX[i]);
                                mDataY = AccDecode(dataY[i]);
                                mDataZ = AccDecode(dataZ[i]);
                                seriesX.Points.AddXY(i, mDataX);
                                seriesY.Points.AddXY(i, mDataY);
                                seriesZ.Points.AddXY(i, mDataZ);
                                seriesV.Points.AddXY(i, Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ));
                                seriesX.Points[i].AxisLabel = Convert.ToString(i / 3);
                            }
                            for (i = 0; i < 32; i++)
                            {
                                mDataX = gyroDecode(rdataX[i * 2], rdataX[i * 2 + 1]);
                                mDataY = gyroDecode(rdataY[i * 2], rdataY[i * 2 + 1]);
                                mDataZ = gyroDecode(rdataZ[i * 2], rdataZ[i * 2 + 1]);
                                seriesRX.Points.AddXY(i, mDataX);
                                seriesRY.Points.AddXY(i, mDataY);
                                seriesRZ.Points.AddXY(i, mDataZ);
                                seriesRV.Points.AddXY(i, Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ));
                                seriesRX.Points[i].AxisLabel = Convert.ToString(i);

                            }
                            break;
                        }
                        count++;
                    }

                }

                catch (Exception f)
                {
                    Console.WriteLine(f.ToString());
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }


            chartRawLin.ChartAreas[0].AxisX.ScaleView.MinSize = 3;

            chartRawLin.ChartAreas[0].AxisX.Interval = 10;
            chartRawLin.ChartAreas[0].AxisX.IntervalOffset = -1;
            // chart2.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Number;

            //private double[] getAzmuth(Double[] myPoint)

            gCalcPosition(azimuth, elevation);


        }

        private void DrawImpactChart(int lineStart, Chart chartRawLin, Chart chartRawGyro, string EventsTable)
        {

            double[] myRes = new double[3];
            double[] my40 = new double[40];
            double[] maxPoint = new double[4];
            double[] myPoint = new double[3];
            //double myDouble;
            //double tDouble = 0;
            double azimuth = 0;
            double elevation = 0;
            int count = 0;
            byte[] dataX;
            byte[] dataY;
            byte[] dataZ;

            byte[] rdataX;
            byte[] rdataY;
            byte[] rdataZ;
            double mDataX, mDataY, mDataZ;
            byte i;
            double[] mTmpPoint = new double[3];
            double linearR = 0;
            double linearAlarmT = 0;
            //const string f = "C:\\00\\g-force\\test.csv";
            chartRawLin.Series.Clear();

            var seriesX = chartRawLin.Series.Add("Linear X");
            var seriesY = chartRawLin.Series.Add("Linear Y");
            var seriesZ = chartRawLin.Series.Add("Linear Z");
            var seriesV = chartRawLin.Series.Add("Linear Res.");
            seriesX.ChartType = SeriesChartType.Line;
            seriesX.XValueType = ChartValueType.Auto;
            seriesY.ChartType = SeriesChartType.Line;
            seriesY.XValueType = ChartValueType.Auto;
            seriesZ.ChartType = SeriesChartType.Line;
            seriesZ.XValueType = ChartValueType.Auto;
            seriesV.ChartType = SeriesChartType.Line;
            seriesV.XValueType = ChartValueType.Auto;
            seriesX.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            seriesY.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            seriesZ.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            seriesV.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));

            //gyro
            chartRawGyro.Series.Clear();

            var seriesRX = chartRawGyro.Series.Add("Rot. X");
            var seriesRY = chartRawGyro.Series.Add("Rot. Y");
            var seriesRZ = chartRawGyro.Series.Add("Rot. Z");
            var seriesRV = chartRawGyro.Series.Add("Rot. Res.");
            seriesRX.ChartType = SeriesChartType.Line;
            seriesRX.XValueType = ChartValueType.Auto;
            seriesRY.ChartType = SeriesChartType.Line;
            seriesRY.XValueType = ChartValueType.Auto;
            seriesRZ.ChartType = SeriesChartType.Line;
            seriesRZ.XValueType = ChartValueType.Auto;
            seriesRV.ChartType = SeriesChartType.Line;
            seriesRV.XValueType = ChartValueType.Auto;
            seriesRX.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            seriesRY.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            seriesRZ.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            seriesRV.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select LINEAR_ACC_RESULTANT, AZIMUTH,ELEVATION,dataX,dataY,dataZ,rdataX,rdataY,rdataZ from " + EventsTable + " where SID = @myID", con);
                    //myCommand.Parameters.Add("@myID", SqlDbType.Int).Value = curSessionID;
                    myCommand.Parameters.Add("@myID", SqlDbType.Int).Value = LastSessionID;// gDataGetSessionLastSID(currentNodeID); //gDataGetSessionLastSID();
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        if (count == lineStart)
                        {
                            dataX = (byte[])myReader["dataX"];
                            dataY = (byte[])myReader["dataY"];
                            dataZ = (byte[])myReader["dataZ"];

                            rdataX = (byte[])myReader["rdataX"];
                            rdataY = (byte[])myReader["rdataY"];
                            rdataZ = (byte[])myReader["rdataZ"];

                            azimuth = (double)myReader["AZIMUTH"];
                            elevation = (double)myReader["ELEVATION"];
                            linearR = (double)myReader["LINEAR_ACC_RESULTANT"];

                            //linearAlarmT = gDataGetSessionSidThreshold(curSessionID, "SessionWireless");
                            linearAlarmT = NodeProfile[currentNodeID - 1].alarmThres;

                            if (linearR >= linearAlarmT)
                                hitPositionColor = "red";
                            else if (linearR >= linearAlarmT * 0.9)
                                hitPositionColor = "yellow";
                            else
                                hitPositionColor = "green";
                            for (i = 0; i < 120; i++)
                            {
                                mDataX = AccDecode(dataX[i]);
                                mDataY = AccDecode(dataY[i]);
                                mDataZ = AccDecode(dataZ[i]);
                                seriesX.Points.AddXY(i, mDataX);
                                seriesY.Points.AddXY(i, mDataY);
                                seriesZ.Points.AddXY(i, mDataZ);
                                seriesV.Points.AddXY(i, Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ));
                                seriesX.Points[i].AxisLabel = Convert.ToString(i / 3);
                            }
                            for (i = 0; i < 32; i++)
                            {
                                mDataX = gyroDecode(rdataX[i * 2], rdataX[i * 2 + 1]);
                                mDataY = gyroDecode(rdataY[i * 2], rdataY[i * 2 + 1]);
                                mDataZ = gyroDecode(rdataZ[i * 2], rdataZ[i * 2 + 1]);
                                seriesRX.Points.AddXY(i, mDataX);
                                seriesRY.Points.AddXY(i, mDataY);
                                seriesRZ.Points.AddXY(i, mDataZ);
                                seriesRV.Points.AddXY(i, Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ));
                                seriesRX.Points[i].AxisLabel = Convert.ToString(i);

                            }
                            break;
                        }
                        count++;
                    }

                }

                catch (Exception f)
                {
                    Console.WriteLine(f.ToString());
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }


            chartRawLin.ChartAreas[0].AxisX.ScaleView.MinSize = 3;

            chartRawLin.ChartAreas[0].AxisX.Interval = 10;
            chartRawLin.ChartAreas[0].AxisX.IntervalOffset = -1;
            // chart2.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Number;

            //private double[] getAzmuth(Double[] myPoint)

            gCalcPosition(azimuth, elevation);


        }



#if false
        void gGetRawData(int lineStart)
        {
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
                try
                {
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select LINEAR_ACC_RESULTANT, AZIMUTH,ELEVATION,dataX,dataY,dataZ from Events where SID = @myID", con);
                    myCommand.Parameters.Add("@myID", SqlDbType.Int).Value = curSessionID;
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        if (count == lineStart)
                        {

                            dataX = (byte[])myReader["dataX"];
                            dataY = (byte[])myReader["dataY"];
                            dataZ = (byte[])myReader["dataZ"];
                            azimuth = (double)myReader["AZIMUTH"];
                            elevation = (double)myReader["ELEVATION"];
                            linearR = (double)myReader["LINEAR_ACC_RESULTANT"];
                            linearAlarmT = gDataGetSessionSidThreshold(curSessionID);
                            if (linearR >= linearAlarmT)
                                hitPositionColor = "red";
                            else if (linearR >= linearAlarmT * 0.9)
                                hitPositionColor = "yellow";
                            else
                                hitPositionColor = "green";
                            for (i = 0; i < 120; i++)
                            {
                                mDataX = AccDecode(dataX[i]);
                                mDataY = AccDecode(dataY[i]);
                                mDataZ = AccDecode(dataZ[i]);
                                seriesX.Points.AddXY(i, mDataX);
                                seriesY.Points.AddXY(i, mDataY);
                                seriesZ.Points.AddXY(i, mDataZ);
                                seriesV.Points.AddXY(i, Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ));
                                seriesX.Points[i].AxisLabel = Convert.ToString(i / 3);
                            }
                            break;
                        }
                        count++;
                    }

                }

                catch (Exception f)
                {
                    Console.WriteLine(f.ToString());
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }


        }
#endif
        //string GFT_J4  = "C9406000"; //back outside
        //string GFT_J6  = "88406000"; //back inside
        //string GFT_J1  = "21406000"; //Top outside
        //string GFT_J12 = "01406000";//top inside middle
        //string GFT_J9  = "1B406000"; //top inside side
        //string GFT_J13 = "02406000"; //right jaw outside
        //string GFT_J3 = "54416000"; //right jaw inside
        //string GFT_J7 = "7C416000";  // left jaw inside
        //string GFT_J8 = "4B416000";   // in dummy head

        string GFT_G2 = "5D2388000000"; //LEFT visor
        string GFT_G18 = "424060000000"; //right visor
        string GFT_G28 = "3C4060000000"; //top
        string GFT_G11 = "A62688000000";//BACK

        int gCalcPosition(double azimuth, double elevation)
        {
            
            int index = 0;
            GID = "";// tbUserGID.Text;
                /* Old hit position code
                // Modified by Harry, 20130524, for Helmet
                if (elevation >= 40)
                    hitPosition = "RIGHT";
                else if (elevation <= -40)
                    hitPosition = "LEFT";
                else if (azimuth <= 225 && azimuth >= 135)
                    hitPosition = "BOTTOM";
                else if (azimuth <= 45 || azimuth >= 315)
                    hitPosition = "TOP";
                else if (azimuth >= 45 && azimuth <= 135)
                    hitPosition = "FRONT";
                else //
                    hitPosition = "BACK";
                */
            
            // updated to be identical to cloud code for hitposition as of Sept 10, 2014
            //
            //    else if (elevation <= -45)
            //        hitPosition = "BOTTOM"; // No longer consider "bottom" impacts, just assign them based on azimuth
            if (elevation >= 45)
                    hitPosition = "TOP";
                else if (azimuth <= 225 && azimuth >= 135)
                    hitPosition = "BACK";
                else if (azimuth <= 45 || azimuth >= 315)
                    hitPosition = "FRONT";
                else if (azimuth >= 45 && azimuth <= 135)
                    hitPosition = "RIGHT";
                else
                    hitPosition = "LEFT";

            
            //get location index
            if (hitPosition == "RIGHT")
                index = 1;
            else if (hitPosition == "LEFT")
                index = 2;
            else if (hitPosition == "BACK")
                index = 3;
            else if (hitPosition == "FRONT")
                index = 4;
            else if (hitPosition == "TOP")
                index = 5;
            else if (hitPosition == "BOTTOM")
                index = 6;
            return index;
        }

    }
}
﻿namespace WirelessGFTViewer
{
    partial class WirelessViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvViewer = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.UploadAll = new System.Windows.Forms.Button();
            this.powerOffGFTs = new System.Windows.Forms.Button();
            this.startRecordingGFTs = new System.Windows.Forms.Button();
            this.stopRecordingGFTs = new System.Windows.Forms.Button();
            this.proximityDisableAll = new System.Windows.Forms.Button();
            this.proximityEnableAll = new System.Windows.Forms.Button();
            this.powerOnGFTs = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewer)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dgvViewer, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(600, 500);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dgvViewer
            // 
            this.dgvViewer.AllowUserToAddRows = false;
            this.dgvViewer.AllowUserToDeleteRows = false;
            this.dgvViewer.AllowUserToOrderColumns = true;
            this.dgvViewer.AllowUserToResizeRows = false;
            this.dgvViewer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvViewer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.dgvViewer.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvViewer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvViewer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvViewer.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvViewer.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvViewer.GridColor = System.Drawing.SystemColors.Control;
            this.dgvViewer.Location = new System.Drawing.Point(3, 3);
            this.dgvViewer.Name = "dgvViewer";
            this.dgvViewer.ReadOnly = true;
            this.dgvViewer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvViewer.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvViewer.RowHeadersVisible = false;
            this.dgvViewer.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvViewer.RowTemplate.DividerHeight = 10;
            this.dgvViewer.RowTemplate.Height = 96;
            this.dgvViewer.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvViewer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvViewer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvViewer.Size = new System.Drawing.Size(594, 461);
            this.dgvViewer.TabIndex = 52;
            this.dgvViewer.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvViewer_CellMouseClick);
            this.dgvViewer.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvViewer_CellMouseEnter);
            this.dgvViewer.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvViewer_ColumnHeaderMouseClick);
            this.dgvViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvViewer_MouseClick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.UploadAll, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.proximityDisableAll, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.proximityEnableAll, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.stopRecordingGFTs, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.startRecordingGFTs, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.powerOffGFTs, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.powerOnGFTs, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 470);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(594, 27);
            this.tableLayoutPanel2.TabIndex = 55;
            // 
            // UploadAll
            // 
            this.UploadAll.Location = new System.Drawing.Point(3, 3);
            this.UploadAll.Name = "UploadAll";
            this.UploadAll.Size = new System.Drawing.Size(112, 23);
            this.UploadAll.TabIndex = 57;
            this.UploadAll.Text = "Download All";
            this.UploadAll.UseVisualStyleBackColor = true;
            this.UploadAll.Click += new System.EventHandler(this.UploadAll_Click);
            // 
            // powerOffGFTs
            // 
            this.powerOffGFTs.Location = new System.Drawing.Point(239, 3);
            this.powerOffGFTs.Name = "powerOffGFTs";
            this.powerOffGFTs.Size = new System.Drawing.Size(112, 23);
            this.powerOffGFTs.TabIndex = 58;
            this.powerOffGFTs.Text = "Power Off All";
            this.powerOffGFTs.UseVisualStyleBackColor = true;
            this.powerOffGFTs.Click += new System.EventHandler(this.powerOffGFTs_Click);
            // 
            // startRecordingGFTs
            // 
            this.startRecordingGFTs.Location = new System.Drawing.Point(357, 3);
            this.startRecordingGFTs.Name = "startRecordingGFTs";
            this.startRecordingGFTs.Size = new System.Drawing.Size(112, 23);
            this.startRecordingGFTs.TabIndex = 62;
            this.startRecordingGFTs.Text = "Start Recording All";
            this.startRecordingGFTs.UseVisualStyleBackColor = true;
            this.startRecordingGFTs.Click += new System.EventHandler(this.startRecordingGFTs_Click);
            // 
            // stopRecordingGFTs
            // 
            this.stopRecordingGFTs.Location = new System.Drawing.Point(475, 3);
            this.stopRecordingGFTs.Name = "stopRecordingGFTs";
            this.stopRecordingGFTs.Size = new System.Drawing.Size(116, 23);
            this.stopRecordingGFTs.TabIndex = 61;
            this.stopRecordingGFTs.Text = "Stop Recording All";
            this.stopRecordingGFTs.UseVisualStyleBackColor = true;
            this.stopRecordingGFTs.Click += new System.EventHandler(this.stopRecordingGFTs_Click);
            // 
            // proximityDisableAll
            // 
            this.proximityDisableAll.Location = new System.Drawing.Point(357, 32);
            this.proximityDisableAll.Name = "proximityDisableAll";
            this.proximityDisableAll.Size = new System.Drawing.Size(112, 14);
            this.proximityDisableAll.TabIndex = 59;
            this.proximityDisableAll.Text = "Turn Off Proximity(All)";
            this.proximityDisableAll.UseVisualStyleBackColor = true;
            this.proximityDisableAll.Visible = false;
            this.proximityDisableAll.Click += new System.EventHandler(this.proximityDisableAll_Click);
            // 
            // proximityEnableAll
            // 
            this.proximityEnableAll.Location = new System.Drawing.Point(239, 32);
            this.proximityEnableAll.Name = "proximityEnableAll";
            this.proximityEnableAll.Size = new System.Drawing.Size(112, 14);
            this.proximityEnableAll.TabIndex = 60;
            this.proximityEnableAll.Text = "Turn On Proximity(All)";
            this.proximityEnableAll.UseVisualStyleBackColor = true;
            this.proximityEnableAll.Visible = false;
            this.proximityEnableAll.Click += new System.EventHandler(this.proximityEnableAll_Click);
            // 
            // powerOnGFTs
            // 
            this.powerOnGFTs.Location = new System.Drawing.Point(121, 3);
            this.powerOnGFTs.Name = "powerOnGFTs";
            this.powerOnGFTs.Size = new System.Drawing.Size(112, 23);
            this.powerOnGFTs.TabIndex = 63;
            this.powerOnGFTs.Text = "Power On All";
            this.powerOnGFTs.UseVisualStyleBackColor = true;
            this.powerOnGFTs.Click += new System.EventHandler(this.powerOnGFTs_Click);
            // 
            // WirelessViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "WirelessViewer";
            this.Size = new System.Drawing.Size(600, 500);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewer)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dgvViewer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button UploadAll;
        private System.Windows.Forms.Button powerOffGFTs;
        private System.Windows.Forms.Button proximityEnableAll;
        private System.Windows.Forms.Button proximityDisableAll;
        private System.Windows.Forms.Button startRecordingGFTs;
        private System.Windows.Forms.Button stopRecordingGFTs;
        private System.Windows.Forms.Button powerOnGFTs;


    }
}

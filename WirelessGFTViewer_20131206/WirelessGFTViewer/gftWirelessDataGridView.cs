﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;
//using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using ArtaFlexHidWin;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.ComponentModel;
using WirelessGFTViewer.Properties;
//using System.Diagnostics;
//using System.Globalization;

namespace gForceTrackerWireless
{
    public partial class Form1 : UsbAwareForm
    {
        int currentNodeID = 0;
        Int32 WirelessSessionID = 0;
        Int32 LastSessionID = 0;
        Int32 WirelessEventID = 0;

        public WirelessGFTViewer.LP_HUB lpHub = null;
        //List<WirelessGFTViewer.NodeControl> NodeControl = new List<WirelessGFTViewer.NodeControl>();        

        public class procArgs
        {
            public Object sender;
            public bool nodeLive = false;
            public bool impactFlag = false;
            public bool mHitFlag = false;
            public byte nodeID;
            public int recv_count = 0;
            public int msg_len;
            public int msg_packet_num;
            
            public bool rec128 = false;
            public bool recStatus = false;
            public bool recSyncTime = false;
            public bool recUploadStatus = false;
            public int status_recv_count = 0;

            public byte[] recvbuff;// = new byte[4000];
            public byte[] lastRecvbuff;
            public string recvbuffstr = "";
            public byte[] sumbuff;// = new byte[4000];
            public int EventID;
#if true //USE_OF_LIST
            public List<IMPACT_DATA> recvFIFO = new List<IMPACT_DATA>();
            public DateTime firstOn = DateTime.MinValue;

    #if NO_IMPACT_DATA_TABLE
    #else
            public List<IMPACT_DATA_TABLE> recvFIFOTable = new List<IMPACT_DATA_TABLE>();
    #endif
#endif
            //public Queue<byte[]> recvQueue = new Queue<byte[]>();
        }

        public List<procArgs> Args = new List<procArgs>();

        public class NODE_PROFILE
        {
            public byte CRC_CheckSum8;
            public ushort previousCountNum;
            public string GID;
            public int nodeID;
            public int playerNum;
            public int recThres;
            public int alarmThres;
            public float m_Tmp;                                                // Added by Jason Chen, 2017.03.29
            public string firstName;
            public string lastName;
            public string UserName;                                          // Added by Jason Chen, 2017.03.29, Whole User Name , includes Firstname and Lastname
            public int EventID;
            public int SessionID;
            public uploadDataForWirelessProcess.SESSION_DATA sData;
            public int recvSessionID;
            public int ALARM;
            public int WTH;
            public int IMPACT;
            public int bat_level;
            public int bat_levelfuel;
            public bool max17047Exist;                                      // Added by Jason Chen, 2014.04.16            
            public byte AlarmMode;

            public int impactNum;                                           //bat_currentfuel;
            public int impactCount;
            public int sessionNum = -1;                                     // Default -1 so we know it hasn't been retrieved
            public int sessionCount;
            public int entryNum;
            public byte usbMode;
            public int  uploadNum_Flag;
            public bool deleteFlag;
            public bool uploadFlag;
            public bool upload_req_flag;
            public bool newDataOnDevice = false;
            public bool syncTimeFlag = false;                                // Notice, 2017.03.23
            //public bool syncTimeFlag = true;                               // Notice, 2017.03.28
            public DateTime syncTimeSentAt = DateTime.MinValue;
            public bool unbindFlag = false;

            public bool recvSessionStarted = false;
            public bool recvSessionClosed = true;

            public double[] iLocation = new double[4];
            public byte mntLoc;
            public float mountCoefficient;

            public int processValue;                                          // 2014.10.29 added
            public bool register = false;

            public byte[] gftSummary_buff;
            public byte[] last_gftSummary_buff;

            public Queue<byte[]> Back_Command_buf = new Queue<byte[]>();      // Added by Jason Chen, 2014.05.16

            public int gftType = 0; // 0 = Regular impact GFT, 1 = Performance

            public Double[] qua = new double[4];
            public bool mntInside;
            public void initQ(bool inside)
            {
                double[] q = iLocation;
                this.qua[0] = q[0] / 1000f;
                this.qua[1] = q[1] / 1000f;
                this.qua[2] = q[2] / 1000f;
                this.qua[3] = q[3] / 1000f;
                this.mntInside = inside;
            }
        }

        public List<NODE_PROFILE> NodeProfile = new List<NODE_PROFILE>();

        public List<int> SessionIDs = new List<int>();

        //NODE_PROFILE nodeProfile = new NODE_PROFILE();

        private void NodeProfileInit()
        {
#if false                                                         //09.11     
            NODE_PROFILE nodeprofile;

            for (byte i = 0; i < MAX_DEVICE_CNT; i++)
            {
                nodeprofile = new NODE_PROFILE();
                NodeProfile.Add(nodeprofile);
            }
#else                                                             //09.11
            if (NodeProfile != null)                              //09.11
            {                                                     //09.11
                if (NodeProfile.Count == 0)                       //09.11
                {                                                 //09.11  
                    NODE_PROFILE nodeprofile;                     //09.11

                    for (byte i = 0; i < MAX_DEVICE_CNT; i++)     //09.11
                    {                                             //09.11
                        nodeprofile = new NODE_PROFILE();         //09.11 
                        NodeProfile.Add(nodeprofile);             //09.11
                    }                                             //09.11
                }                                                 //09.11 
            }                                                     //09.11
#endif
        }

        private void ArgsInit()
        {
            procArgs args;
            for (byte i = 0; i < MAX_DEVICE_CNT; i++)
            {
                args = new procArgs();
                args.nodeID = (byte)(i + 1);
                args.nodeLive = false;
                //args.EventID = (i + 1) << 24;
                args.EventID = getUniqueEventID();
                Args.Add(args);                
            }
        }

#if false
        private void NodeControlInit()
        {
            WirelessGFTViewer.NodeControl nodeControl;
            controlPanel.Controls.Clear();
            for (byte i = 0; i < MAX_DEVICE_CNT; i++)
            {
                nodeControl = new WirelessGFTViewer.NodeControl();
                nodeControl.NodeNumber = i + 1;
                nodeControl.NodeControlClicked += new WirelessGFTViewer.NodeControlOnClickedEventHandler(nodeControl_NodeControlClicked);
                nodeControl.NodeControlLastSessionRestored += new WirelessGFTViewer.NodeControlOnRestoreLastSession(nodeControl_NodeControlLastSessionRestored);
                nodeControl.powerOffClicked += new WirelessGFTViewer.powerOffClickedEventHandler(nodeControl_powerOffClicked);
                nodeControl.getImpactsClicked += new WirelessGFTViewer.getImpactsClickedEventHandler(nodeControl_getImpactsClicked);
                NodeControl.Add(nodeControl);
                //flowLayoutPanel.Controls.Add(nodeControl);
            }
            controlPanel.Controls.Add(NodeControl[0]);
            controlPanel.Dock = DockStyle.Fill;
            NodeControl[0].Dock = DockStyle.Fill;
        }
#endif

        private void gftWirelessViewInit1()
        {
        }

        private void gftWirelessViewInit()
        {

        }

        private void Wireless_dataInit()
        {
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select MAX(EID) from EventsWireless", con);

                    using (myReader = myCommand.ExecuteReader())
                    {
                        if (myReader.Read())
                        {
                            //SessionID = Convert.ToInt32(myReader["column1"]);
                            //int fieldCount = myReader.FieldCount;
                            if(!myReader.IsDBNull(0))
                                WirelessEventID = myReader.GetInt32(0);
                        }
                    }
                    myCommand = new SqlCeCommand("select MAX(SID) from SessionWireless", con);
                    using (myReader = myCommand.ExecuteReader())
                    {
                        if (myReader.Read())
                        {

                            //SessionID = Convert.ToInt32(myReader["column1"]);
                            //int fieldCount = myReader.FieldCount;
                            if (!myReader.IsDBNull(0))
                                WirelessSessionID = myReader.GetInt32(0);
                        }
                    }
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.ToString());
                    //wirelessViewer.DebugDataBoxDisplay(e.Message + "\r\n");
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                   // wirelessViewer.DebugDataBoxDisplay(me.ToString() + "\r\n");
                }
            }
        }

        private void Wireless_dataInit(int nodeID)
        {
#if USE_OF_LIST  
            if((NodeProfile[nodeID - 1].SessionID & 0xFF000000) == 0x0000)
              NodeProfile[nodeID - 1].SessionID = (nodeID << 24) | 0x0001;
            if((NodeProfile[nodeID - 1].EventID & 0xFF000000) == 0x0000)
              NodeProfile[nodeID - 1].EventID = (nodeID << 24) | 0x0000;
#else
            int tempID = 0;
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);

            //if (nodeID == 1)
            //    Thread.Sleep(0);

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {                
                SqlCeDataReader myReader = null;
                try
                {
                    con.Open();
                    SqlCeCommand myCommand = new SqlCeCommand("select MAX(SID) from SessionWireless where (UID = @mUID AND MID = @MID)", con);
                    myCommand.Parameters.AddWithValue("@mUID", nodeID);
                    myCommand.Parameters.AddWithValue("@MID", dongle_mid);
                    using (myReader = myCommand.ExecuteReader())
                    {
                        if (myReader.Read())
                        {

                            //SessionID = Convert.ToInt32(myReader["column1"]);
                            //int fieldCount = myReader.FieldCount;
                            if (!myReader.IsDBNull(0))
                            {
                                tempID = WirelessSessionID = myReader.GetInt32(0);
                                tempID &= 0x00FFFFF;
                                //tempID++;
                                NodeProfile[nodeID - 1].SessionID = (nodeID << 24) | tempID;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    string temp = e.Message;
                    //wirelessViewer.DebugDataBoxDisplay("No record in SessionWireless table\r\n");
                    NodeProfile[nodeID - 1].SessionID = (nodeID << 24) | 0x0001;
                }
                /*if (NodeProfile[nodeID - 1].sData.OnTime == DateTime.MinValue) // This is a new Session
                {
                    NodeProfile[nodeID - 1].SessionID = gDataGetSessionLastSID() + 1;
                    uploadDataForWirelessProcess.SESSION_DATA sData = new uploadDataForWirelessProcess.SESSION_DATA();
                    bool uniqueSID = false;
                    while (!uniqueSID)
                    {
                        myReader = null;
                        try
                        {
                            if(con.State.ToString() != "Open") con.Open();
                            SqlCeCommand myCommand = new SqlCeCommand("SELECT COUNT(*) as cnt from SessionWireless where (SID = @SID@)", con);
                            myCommand.Parameters.AddWithValue("@SID@", NodeProfile[nodeID - 1].SessionID);
                            using (myReader = myCommand.ExecuteReader())
                            {
                                if (myReader.Read())
                                {
                                    if (myReader.GetInt32(0) == 0) uniqueSID = true;
                                    else
                                    {
                                        NodeProfile[nodeID - 1].SessionID++;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            string temp = e.Message;
                            NodeProfile[nodeID - 1].SessionID++;
                        }
                    }
                    sData.OnTime = DateTime.UtcNow;
                    sData.OffTime = sData.OnTime;
                    sData.Duration = 0;
                    sData.activeTime = 0;
                    sData.Date = sData.OnTime;
                    sData.name = NodeProfile[nodeID - 1].firstName;
                    sData.SID = NodeProfile[nodeID - 1].SessionID;
                    sData.Threshold = NodeProfile[nodeID - 1].alarmThres;
                    sData.GID = NodeProfile[nodeID - 1].GID;
                    sData.UID = nodeID;
                    sData.Impact = 0;
                    sData.Alarm = 0;
                    sData.WTH = 0;
                    sData.iLocation = NodeProfile[nodeID - 1].iLocation;
                    sData.iMntInside = NodeProfile[nodeID - 1].mntInside;
                    sData.swVersion = Application.ProductVersion;
                    sData.fwVersion = ""; // Need to store this
                    if (sData.GID == null) sData.GID = "123456789ABC";

                    gDataInsertSession(sData, "SessionWireless", dongle_mid, nodeID);     // Insert sData into Session table
                    // Session has been inserted, check that it has a unique ID, if not, change it
                    uniqueSID = false;
                    Random rnd = new Random();
                    while (!uniqueSID)
                    {
                        myReader = null;
                        try
                        {
                            if (con.State.ToString() != "Open") con.Open();
                            SqlCeCommand myCommand = new SqlCeCommand("SELECT COUNT(*) as cnt from SessionWireless where (SID = @SID@)", con);
                            myCommand.Parameters.AddWithValue("@SID@", NodeProfile[nodeID - 1].SessionID);
                            using (myReader = myCommand.ExecuteReader())
                            {
                                if (myReader.Read())
                                {
                                    if (myReader.GetInt32(0) <= 1) uniqueSID = true;
                                    else
                                    {
                                        NodeProfile[nodeID - 1].SessionID++;
                                        if (rnd.Next(0, nodeID + 10) > nodeID + 10 / 1.25) NodeProfile[nodeID - 1].SessionID += nodeID; // Sometimes tick it up by the nodeID, to avoid a possible race condition
                                        sData.SID = NodeProfile[nodeID - 1].SessionID;
                                        gDataDeleteSessionID(sData, "SessionWireless", dongle_mid, nodeID);
                                        gDataInsertSession(sData, "SessionWireless", dongle_mid, nodeID);
                                    }
                                }
                            }
                        }
                        catch (SqlCeException e)
                        {
                        }
                    }

                    NodeProfile[nodeID - 1].sData = sData;
                }*/
                /*try
                {
                    SqlCeCommand myCommand = new SqlCeCommand("select MAX(EID) from EventsWireless where (SID = @mSID) and (UID = @mUID)", con);   //WHERE (UID = @nodeID) AND (SID = @mSID)"
                    tempID = NodeProfile[nodeID - 1].SessionID;// -1;
                    myCommand.Parameters.AddWithValue("@mSID", tempID);
                    myCommand.Parameters.AddWithValue("@mUID", nodeID);

                    using (myReader = myCommand.ExecuteReader())
                    {
                        if (myReader.Read())
                        {
                            //SessionID = Convert.ToInt32(myReader["column1"]);
                            //int fieldCount = myReader.FieldCount;
                            if (!myReader.IsDBNull(0))
                            {
                                tempID = WirelessEventID = myReader.GetInt32(0);
                                tempID &= 0x00FFFFFF;
                                NodeProfile[nodeID - 1].EventID = (nodeID << 24) | tempID;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    string temp = e.Message;
                    //wirelessViewer.DebugDataBoxDisplay("No record in EventWireless table\r\n");
                    NodeProfile[nodeID - 1].EventID = (nodeID << 24) | 0x0000;
                }*/
                NodeProfile[nodeID - 1].EventID = getUniqueEventID();

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
#endif
        }

        private void mEnableAdd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void getGftWirelessAll1()
        {

        }

        private void getGftWirelessAll()
        {

        }

        private void getGftWirelessAll(int nodeID)
        {

        }

        private DataTable getGftUserNum()
        {
            DataTable t;
            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                try
                {
                    mycon.Open();                   
                    using (SqlCeDataAdapter adp = new SqlCeDataAdapter("SELECT GID, nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,LOCATION,FNAME,LNAME FROM gftWireless", mycon))
                    {
                        t = new DataTable();
                        adp.Fill(t);
                        //gftWirelessView.DataSource = t;
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);
                    //Close();
                    return null;
                    //throw er;
                }
                try
                {
                    mycon.Close();
                }
                catch (Exception er)
                {
                    throw er;
                }
                return t;
            }
            //return 0;
        }

        private int getGftWireless(string myGID)
        {
            int ret = 0;

            if (myGID.Length != 12) return ret;

            //if (mycon.State != ConnectionState.Open)
            //    return false;

            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                try
                {
                    mycon.Open();
                    //SqlCeCommand cmd = new SqlCeCommand("SELECT GID,nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,LOCATION,FNAME,LNAME,iLocation0,iLocation1,iLocation2,iLocation3 FROM gftWireless WHERE (GID = @mGID) ", mycon);
                    SqlCeCommand cmd = new SqlCeCommand("SELECT GID,nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,LOCATION,FNAME,LNAME,iLocation0,iLocation1,iLocation2,iLocation3,coefficient FROM gftWireless WHERE (GID = @mGID) and (MID = @dongle_mid OR MID = '' OR MID = '0000000000') ", mycon);   // 2014.11.11
                    cmd.Parameters.AddWithValue("@mGID", myGID);
                    cmd.Parameters.AddWithValue("@dongle_mid", dongle_mid);    // 2014.11.11
              
                    SqlCeDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        int nodeId = (Int32)rdr["nodeID"];
                        if (nodeId >= 1 && nodeId <= NUM_NODES)
                        {
                            NodeProfile[nodeId - 1].GID = (string)rdr["GID"];
                            NodeProfile[nodeId - 1].nodeID = nodeId;
                            NodeProfile[nodeId - 1].playerNum = (Int32)rdr["PlayerNumber"];
                            NodeProfile[nodeId - 1].recThres = (Int32)rdr["REC_THRES"];
                            NodeProfile[nodeId - 1].alarmThres = (Int32)rdr["ALARM_THRES"];
                            NodeProfile[nodeId - 1].firstName = (string)rdr["FNAME"];
                            NodeProfile[nodeId - 1].lastName = (string)rdr["LNAME"];
                            NodeProfile[nodeId - 1].mntLoc = (byte)((int)rdr["LOCATION"]);
                            NodeProfile[nodeId - 1].iLocation[0] = (Int32)rdr["iLocation0"];
                            NodeProfile[nodeId - 1].iLocation[1] = (Int32)rdr["iLocation1"];
                            NodeProfile[nodeId - 1].iLocation[2] = (Int32)rdr["iLocation2"];
                            NodeProfile[nodeId - 1].iLocation[3] = (Int32)rdr["iLocation3"];
                            NodeProfile[nodeId - 1].mountCoefficient = (float)(double)rdr["coefficient"];
                            if (NodeProfile[nodeId - 1].mountCoefficient < 0.1) NodeProfile[nodeId - 1].mountCoefficient = (float)1.00;
                            ret = nodeId;
                        }
                    }
                    else
                        ret = 0;
                }
                catch (Exception me)
                {
                    //if (TooltabPage16Exist)
                    //{
                        //m_Edit_Immediate.AppendText("Check Database " + me.Message + "\r\n");
                    //}
                }
                try
                {
                    mycon.Close();
                }
                catch (Exception er)
                {
                    throw er;
                }
            }
            return ret;
        }

        private bool getGftWireless(int node_id)
        {
            bool ret = false;

            if (node_id > MAX_DEVICE_CNT/*63*/ || node_id < 1) return ret;                     //2017.06.20

            //if (mycon.State != ConnectionState.Open)
            //    return false;

            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                try
                {
                    mycon.Open();
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);
                }
                //SqlCeCommand cmd = new SqlCeCommand("SELECT GID,nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,LOCATION,FNAME,LNAME,iLocation0,iLocation1,iLocation2,iLocation3 FROM gftWireless WHERE (nodeID = @nodeID) ", mycon);
                SqlCeCommand cmd = new SqlCeCommand("SELECT GID,nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,LOCATION,FNAME,LNAME,iLocation0,iLocation1,iLocation2,iLocation3,coefficient FROM gftWireless WHERE (nodeID = @nodeID) and (MID = @dongle_mid OR MID = '' OR MID = '0000000000')", mycon);         // 2014.11.11

                cmd.Parameters.AddWithValue("@nodeID", node_id);
                cmd.Parameters.AddWithValue("@dongle_mid", dongle_mid);    // 2014.11.11
                try
                {
                    SqlCeDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        NodeProfile[node_id - 1].GID = (string)rdr["GID"];
                        NodeProfile[node_id - 1].nodeID = (Int32)rdr["nodeID"];
                        NodeProfile[node_id - 1].playerNum = (Int32)rdr["PlayerNumber"];
                        NodeProfile[node_id - 1].recThres = (Int32)rdr["REC_THRES"];
                        NodeProfile[node_id - 1].alarmThres = (Int32)rdr["ALARM_THRES"];
                        NodeProfile[node_id - 1].firstName = (string)rdr["FNAME"];
                        NodeProfile[node_id - 1].lastName = (string)rdr["LNAME"];
                        NodeProfile[node_id - 1].mntLoc = (byte) ((int)rdr["LOCATION"]);
                        NodeProfile[node_id - 1].iLocation[0] = (Int32)rdr["iLocation0"];
                        NodeProfile[node_id - 1].iLocation[1] = (Int32)rdr["iLocation1"];
                        NodeProfile[node_id - 1].iLocation[2] = (Int32)rdr["iLocation2"];
                        NodeProfile[node_id - 1].iLocation[3] = (Int32)rdr["iLocation3"];
                        NodeProfile[node_id - 1].mountCoefficient = (float)(double)rdr["coefficient"];
                        if (NodeProfile[node_id - 1].mountCoefficient < 0.1) NodeProfile[node_id - 1].mountCoefficient = (float)1.00;
                        ret = true;
                    }
                    else
                        ret = false;
                }
                catch (Exception me)
                {
                    //if (TooltabPage16Exist)
                    //{
                        //m_Edit_Immediate.AppendText(me.Message + "\r\n");
                    //}
                }

                try
                {
                    mycon.Close();
                }
                catch (Exception er)
                {
                    throw er;
                }
            }
            return ret;
        }

        //private bool getSessionWireless(int mySession, int node_id)
        private bool getSessionWireless(int node_id)
        {
            bool ret = false;

            if (node_id > MAX_DEVICE_CNT || node_id < 1) return ret;

            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                try
                {
                    mycon.Open();
                    SqlCeCommand cmd = new SqlCeCommand("SELECT SID,UID,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH,THRESHOLD FROM SessionWireless WHERE (UID = @nodeID) AND (SID = @mSID)", mycon);
                    cmd.Parameters.AddWithValue("@nodeID", node_id);
                    int mySession = NodeProfile[node_id - 1].SessionID;
                    cmd.Parameters.AddWithValue("@mSID", mySession);

                    SqlCeDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        NodeProfile[node_id - 1].IMPACT = (Int32)rdr["IMPACT"];
                        NodeProfile[node_id - 1].WTH = (Int32)rdr["WTH"];
                        NodeProfile[node_id - 1].ALARM = (int)rdr["ALARM"];
                        ret = true;
                    }
                    else
                        ret = false;
                }
                catch (Exception me)
                {
                    //if (TooltabPage16Exist)
                    //{
                        //m_Edit_Immediate.AppendText(me.Message + "\r\n");
                    //}
                }

                try
                {
                    mycon.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return ret;
        }

        private bool setGftWireless(bool iNewUser)
        {

            bool ret = false;
#if false
            int nodeID, playerNum, recThres, alarmThres;
            string tName;

            DataGridViewRow cur_row = gftWirelessView.CurrentRow;
            string myGID = cur_row.Cells[0].Value.ToString();

            if (myGID.Length != 12)
            {
                MessageBox.Show("GID format not correct!");
                return ret;
            }

            string temp = cur_row.Cells[1].Value.ToString();

            if (temp == "") nodeID = 0;
            else nodeID = Convert.ToInt32(cur_row.Cells[1].Value);

            temp = cur_row.Cells[2].Value.ToString();
            if (temp == "") playerNum = 0;
            else playerNum = Convert.ToInt32(cur_row.Cells[2].Value);

            temp = cur_row.Cells[4].Value.ToString();
            if (temp == "") recThres = 0;
            else recThres = Convert.ToInt32(cur_row.Cells[4].Value);

            temp = cur_row.Cells[5].Value.ToString();
            if (temp == "") alarmThres = 0;
            else alarmThres = Convert.ToInt32(cur_row.Cells[5].Value);

            temp = (string)(cur_row.Cells[7].Value);
            if (temp == "") tName = "FirstName1";
            else tName = temp;

            string queryUpdate = "UPDATE gftWireless SET GID=@GID,PlayerNumber = @PlayerNumber, DATE = @DATE," +
                                                "REC_THRES=@REC_THRES,ALARM_THRES=@ALARM_THRES," +
                                                "FNAME = @FNAME, LNAME = @LNAME WHERE nodeID = @nodeID "; // ,LOCATION = @LOCATION
            /*, " +
                                                "iLocation0 = @iLocation0, iLocation1=@iLocation1, iLocation2=@iLocation2, iLocation3=@iLocation3" +
                                                "
             ,iLocation0,iLocation1,iLocation2,iLocation3
             * ,@iLocation0,@iLocation1,@iLocation2,@iLocation3
             */

            string queryInsert = "INSERT gftWireless (GID,nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,FNAME,LNAME)" +
                                 "VALUES(@GID,@nodeID,@PlayerNumber,@DATE,@REC_THRES,@ALARM_THRES,@FNAME,@LNAME)";//,@LOCATION // LOCATION

            //if (mycon.State != ConnectionState.Open)
            //    return false;
            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                try
                {
                    mycon.Open();
                    if (iNewUser)  //new user
                    {
                        using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                        {
                            com.Parameters.AddWithValue("@GID", myGID);
                            com.Parameters.AddWithValue("@nodeID", nodeID);
                            com.Parameters.AddWithValue("@PlayerNumber", playerNum);
                            com.Parameters.AddWithValue("@DATE", System.DateTime.UtcNow);
                            com.Parameters.AddWithValue("@REC_THRES", recThres);
                            com.Parameters.AddWithValue("@ALARM_THRES", alarmThres);
                            /*com.Parameters.AddWithValue("@LOCATION", 0);*/
                            com.Parameters.AddWithValue("@FNAME", tName);
                            com.Parameters.AddWithValue("@LNAME", "LastName");
                          /*  com.Parameters.AddWithValue("@iLocation0", 0);
                            com.Parameters.AddWithValue("@iLocation1", 0);
                            com.Parameters.AddWithValue("@iLocation2", 0);
                            com.Parameters.AddWithValue("@iLocation3", 0);*/
                            try
                            {
                                com.ExecuteNonQuery();
                            }
                            catch (SqlCeException ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                    else //update user
                    {
                        using (SqlCeCommand com = new SqlCeCommand(queryUpdate, mycon))
                        {
                            com.Parameters.AddWithValue("@GID", myGID);
                            com.Parameters.AddWithValue("@nodeID", nodeID);
                            com.Parameters.AddWithValue("@PlayerNumber", playerNum);
                            com.Parameters.AddWithValue("@DATE", System.DateTime.UtcNow);
                            com.Parameters.AddWithValue("@REC_THRES", recThres);
                            com.Parameters.AddWithValue("@ALARM_THRES", alarmThres);
                           /* com.Parameters.AddWithValue("@LOCATION", 0);*/
                            com.Parameters.AddWithValue("@FNAME", tName);
                            com.Parameters.AddWithValue("@LNAME", "LastName1");
                           /* com.Parameters.AddWithValue("@iLocation0", 0);
                            com.Parameters.AddWithValue("@iLocation1", 0);
                            com.Parameters.AddWithValue("@iLocation2", 0);
                            com.Parameters.AddWithValue("@iLocation3", 0);*/
                            com.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);
                }
                try
                {
                    mycon.Close();
                }
                catch (Exception er)
                {
                    throw er;
                }
            }

            getGftWirelessAll();
#endif
            return ret;
        }

        private bool setGftWirelessNode(NODE_PROFILE nodeProfile, bool iNewUser)
        {
            bool ret = false;
            int nodeID, playerNum, recThres, alarmThres, alarmMode;
            string firstName, lastName;

            string myGID = nodeProfile.GID;
            nodeID = nodeProfile.nodeID;

            if ((myGID == null)||(myGID.Length != 12))
            {
                //wirelessViewer.DebugDataBoxDisplay("  GID format not correct! \r\n");
                myGID = "123456789ABC";
                //myGID = last_usb_gid; // Use the GID from the last USB connected GFT
                //return ret;
            }

            
            playerNum = nodeProfile.playerNum;
            recThres = nodeProfile.recThres;
            if (recThres == 0)
                recThres = 10;
            alarmThres = nodeProfile.alarmThres;
            if (alarmThres == 0)
                alarmThres = 60;
            firstName = nodeProfile.firstName;
            if (firstName == null)
                firstName = "        "; //"FirstName";
            lastName = nodeProfile.lastName;
            if (lastName == null)
                lastName  = "        ";// "LastName";
            alarmMode = nodeProfile.AlarmMode;
            if (alarmMode == 0)
                alarmMode = 0xF1;
            /*
            lpHub.lpDevices[nodeID - 1].live = true;
            lpHub.lpDevices[nodeID - 1].alarm = false;
            lpHub.lpDevices[nodeID - 1].playerNumer = nodeProfile.playerNum;
            lpHub.lpDevices[nodeID - 1].name = (firstName + " " + lastName).ToCharArray();
            lpHub.lpDevices[nodeID - 1].mode = 0x81; */

            string queryUpdate = "UPDATE gftWireless SET GID=@GID,PlayerNumber = @PlayerNumber, DATE = @DATE," +
                                                "REC_THRES=@REC_THRES,ALARM_THRES=@ALARM_THRES,LOCATION = @LOCATION," +
                                                "FNAME = @FNAME, LNAME = @LNAME, ALARMMODE=@ALARMMODE, " +
                                                "iLocation0 = @iLocation0, iLocation1=@iLocation1, iLocation2=@iLocation2, iLocation3=@iLocation3, coefficient=@coefficient" +
                                                " WHERE (nodeID = @nodeID) and (MID = @dongle_mid OR MID = '' OR MID = '0000000000') ";

            //string queryInsert = "INSERT gftWireless (GID,nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,LOCATION,FNAME,LNAME,ALARMMODE,iLocation0,iLocation1,iLocation2,iLocation3)" +
            //                     "VALUES(@GID,@nodeID,@PlayerNumber,@DATE,@REC_THRES,@ALARM_THRES,@LOCATION,@FNAME,@LNAME,@ALARMMODE,@iLocation0,@iLocation1,@iLocation2,@iLocation3)";
            string queryInsert = "INSERT gftWireless (GID,MID,nodeID,PlayerNumber,DATE,REC_THRES,ALARM_THRES,LOCATION,FNAME,LNAME,ALARMMODE,iLocation0,iLocation1,iLocation2,iLocation3,coefficient)" +
                                 "VALUES(@GID,@dongle_mid,@nodeID,@PlayerNumber,@DATE,@REC_THRES,@ALARM_THRES,@LOCATION,@FNAME,@LNAME,@ALARMMODE,@iLocation0,@iLocation1,@iLocation2,@iLocation3,@coefficient)";    //  changed, 2014.11.11



//            if (mycon.State != ConnectionState.Open)
//                return false;
            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                try
                {
                    mycon.Open();

                    if (iNewUser)  //new user
                    {
                        using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                        {
                            com.Parameters.AddWithValue("@GID", myGID);
                            com.Parameters.AddWithValue("@dongle_mid", dongle_mid);                               // 2014.11.11
                            com.Parameters.AddWithValue("@nodeID", nodeID);
                            com.Parameters.AddWithValue("@PlayerNumber", playerNum);
                            com.Parameters.AddWithValue("@DATE", System.DateTime.UtcNow);
                            com.Parameters.AddWithValue("@REC_THRES", recThres);
                            com.Parameters.AddWithValue("@ALARM_THRES", alarmThres);
                            com.Parameters.AddWithValue("@LOCATION", nodeProfile.mntLoc);
                            com.Parameters.AddWithValue("@FNAME", firstName);
                            com.Parameters.AddWithValue("@LNAME", lastName);
                            com.Parameters.AddWithValue("@ALARMMODE", alarmMode);
                            com.Parameters.AddWithValue("@iLocation0", nodeProfile.iLocation[0]);
                            com.Parameters.AddWithValue("@iLocation1", nodeProfile.iLocation[1]);
                            com.Parameters.AddWithValue("@iLocation2", nodeProfile.iLocation[2]);
                            com.Parameters.AddWithValue("@iLocation3", nodeProfile.iLocation[3]);
                            com.Parameters.AddWithValue("@coefficient", nodeProfile.mountCoefficient);
                            try
                            {
                                com.ExecuteNonQuery();
                            }
                            catch (SqlCeException ex)
                            {
                                //wirelessViewer.DebugDataBoxDisplay(ex.Message + "\r\n");
                                //if (TooltabPage16Exist)
                                //{
                                    //m_Edit_Immediate.AppendText(ex.Message + "\r\n");
                                //}
                            }
                        }
                    }
                    else //update user
                    {
                        using (SqlCeCommand com = new SqlCeCommand(queryUpdate, mycon))
                        {
                            com.Parameters.AddWithValue("@GID", myGID);
                            com.Parameters.AddWithValue("@dongle_mid", dongle_mid);                               // 2014.11.11
                            com.Parameters.AddWithValue("@nodeID", nodeID);
                            com.Parameters.AddWithValue("@PlayerNumber", playerNum);
                            com.Parameters.AddWithValue("@DATE", System.DateTime.UtcNow);
                            com.Parameters.AddWithValue("@REC_THRES", recThres);
                            com.Parameters.AddWithValue("@ALARM_THRES", alarmThres);
                            com.Parameters.AddWithValue("@LOCATION", nodeProfile.mntLoc);
                            com.Parameters.AddWithValue("@FNAME", firstName);
                            com.Parameters.AddWithValue("@LNAME", lastName);
                            com.Parameters.AddWithValue("@ALARMMODE", alarmMode);
                            com.Parameters.AddWithValue("@iLocation0", nodeProfile.iLocation[0]);
                            com.Parameters.AddWithValue("@iLocation1", nodeProfile.iLocation[1]);
                            com.Parameters.AddWithValue("@iLocation2", nodeProfile.iLocation[2]);
                            com.Parameters.AddWithValue("@iLocation3", nodeProfile.iLocation[3]);
                            com.Parameters.AddWithValue("@coefficient", nodeProfile.mountCoefficient);
                            try
                            {
                                com.ExecuteNonQuery();
                            }
                            catch (SqlCeException ex)
                            {
                                //wirelessViewer.DebugDataBoxDisplay(ex.Message + "\r\n");
                                //if (TooltabPage16Exist)
                                //{
                                    //m_Edit_Immediate.AppendText(ex.Message + "\r\n");
                                //}
                            }
                        }
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);
                }
                try
                {
                    mycon.Close();
                }
                catch (Exception er)
                {
                    throw er;
                }
            }

            //if(tabControl4.TabPages.Contains(tabPage16))
            //if (TooltabPage16Exist)
            //{
            //    getGftWirelessAll(nodeID);
            //}
            if (iNewUser) deviceOnBind(); // Commands to trigger on new device bind
            return ret;
        }

        private void mUpdate_Click(object sender, EventArgs e)
        {
            setGftWireless(false);
        }

        private void mAdd_Click(object sender, EventArgs e)
        {
            setGftWireless(true);
        }

        private void gftWirelessView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //if (TooltabPage16Exist)
            //    m_Edit_Immediate.AppendText(" Cell Format wrong\r\n");
            MessageBox.Show("Must be digits","Cell Format wrong");
        }

        private void gftWirelessView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void mDelete_Click(object sender, EventArgs e)
        {
            

        }

        private void DeleteAllUsersData()
        {
#if USE_OF_LIST
#else
#if true
            int myUID = 0;
            int FirstSID = 0;
            int rowCount = 0;
            //int rowCount = gftWirelessView.Rows.Count;
            DataTable t = getGftUserNum();
            if(t != null)
               rowCount = t.Rows.Count;

            //DataGridViewRow cur_row = gftWirelessView.CurrentRow;           
            for (int i = 0; i < rowCount; i++)
            {
                try
                {
                    //myUID = (int)(t.Rows[i].Cells[1].Value);
                    myUID = (int)(t.Rows[i].Field<int>(1));
                }
                catch (Exception er)
                {
                    string temp = er.Message;
                    continue;
                }
                for (; ; )
                {
                    FirstSID = gDataGetSessionFirstSID(myUID);
                    if (FirstSID == 0)
                        break;
                    mDelete_EventRecord(FirstSID);
                    mDelete_SessionRecord(FirstSID);
                } 
            }
#endif
#endif
        }

        private Int32 gDataGetSessionLastSID(int uid)
        {
            Int32 mSessionID = 0;

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select MAX(SID) from SessionWireless WHERE ( UID = '" + uid + "')", con);
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        mSessionID = myReader.GetInt32(0);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }

            return mSessionID;
        }

        private Int32 gDataGetSessionFirstSID(int uid)
        {
            Int32 mSessionID = 0;

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand("select MIN(SID) from SessionWireless WHERE ( UID = '" + uid + "')", con);
                    myReader = myCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        mSessionID = myReader.GetInt32(0);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return mSessionID;
        }

        private bool mDelete_SessionRecord(int SessionID)
        {
            string queryInsert = "DELETE FROM SessionWireless WHERE SID = @SID";

            //if (mycon.State != ConnectionState.Open) return false;

            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                
                bool ret = false;
                try
                {
                    mycon.Open();
                    using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                    {
                        com.Parameters.AddWithValue("@SID", SessionID);

                        com.ExecuteNonQuery();
                        //if (TooltabPage16Exist)
                        //    m_Edit_Immediate.AppendText("SessionWireless " + SessionID.ToString("X8") + " Deleted!\r\n");
                        ret=true;
                    }
                }
                catch (Exception er)
                {
                    string temp = er.Message;
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText("SessionWireless " + SessionID.ToString("X8") + " Delete Fail!\r\n");
                    ret=false;
                }
                try
                {
                    mycon.Close();
                }
                catch(Exception ee)
                {
                    throw ee;
                }
                return ret;
            }
        }

        private bool mDelete_EventRecord(int SessionID)
        {
            string queryInsert = "DELETE FROM EventsWireless WHERE SID = @SID";

            //if (mycon.State != ConnectionState.Open) return false;
            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                

                bool ret = false;
                try
                {
                    mycon.Open();
                    using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                    {
                        com.Parameters.AddWithValue("@SID", SessionID);

                        com.ExecuteNonQuery();
                        //if (TooltabPage16Exist)
                        //    m_Edit_Immediate.AppendText("EventsWireless Session " + SessionID.ToString("X8") + " Deleted!\r\n");
                        ret = true;
                    }
                }
                catch (Exception er)
                {
                    string temo = er.Message;
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText("EventsWireless Session " + SessionID.ToString("X8") + " Delete Fail!\r\n");
                    ret = false;
                }
                try
                {
                    mycon.Close();
                }
                catch (Exception me)
                {
                    throw me;
                }
                return ret;
            }
        }

        private bool mDelete_gftWireless(int node)
        {

            //int rowCount = gftWirelessView.Rows.Count;
            //for (int index = 0; index < rowCount; index++)
            //{
            //    gftWirelessView.Rows.RemoveAt(0);
            //}

            //gftWirelessView.

            //string queryInsert = "DELETE FROM gftWireless WHERE nodeID = @nodeID";
            string queryInsert = "DELETE FROM gftWireless WHERE (nodeID = @nodeID) and (MID = @dongle_mid OR MID = '' OR MID = '0000000000')";    // changed, 2014.11.11

            //if (mycon.State != ConnectionState.Open) return;

            using (SqlCeConnection mycon = new SqlCeConnection(conString))
            {
                bool ret = false;
                try
                {
                    mycon.Open();
                    using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                    {
                        com.Parameters.AddWithValue("@nodeID", node);
                        com.Parameters.AddWithValue("@dongle_mid", dongle_mid);

                        com.ExecuteNonQuery();
                        getGftWirelessAll();
                        ret = true;
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);
                    ret = false;
                }
                try
                {
                    mycon.Close();
                }
                catch (Exception er)
                {
                    throw er;
                }
                return ret;
            }
        }

        #region Wireless_Viewer_process


        private delegate void TimerEventHandler(IntPtr hHandle, int msg, int idEvent, int dwTime);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool KillTimer(IntPtr hHandle, UInt32 uIDEvent);

        [DllImport("user32.dll", SetLastError = true)]
        static extern UInt32 SetTimer(IntPtr hHandle, Int32 uIDEvent, UInt32 uElapse, TimerEventHandler tcb);

        //private void TimerEventHandler(uint timer_id)
        private void timerCallBack(IntPtr hHandle, int msg, int timer_id, int dwTime)
        {
            if (timer_id == 0x100)
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("Timer ID " + timer_id.ToString("X4") + " Response Timeout....\r\n");
                //wirelessViewer.DebugDataBoxDisplay("Timer ID 0x160 "+timer_id.ToString("X2")+" Response Timeout....\r\n");
            }
            else if(timer_id >= (0x120 + 1) && timer_id <= (0x120 + NUM_NODES))
            {
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("Timer ID 0x120 " + (timer_id - 0x120).ToString("D3") + " Response Timeout....\r\n");

            }
            else if (timer_id >= (0x160 + 1) && timer_id <= (0x160 + NUM_NODES))
            {
                NodeProfile[timer_id - 0x160 - 1].uploadNum_Flag = 0;
                lpHub.lpDevices[timer_id - 0x160 - 1].upldFlag_num = NodeProfile[timer_id - 0x160 - 1].uploadNum_Flag;
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n---Node " + (timer_id - 0x160).ToString("D3") + " Upload CMD, Timeout....\r\n");
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(timer_id - 0x160); // 2014.10.01
#else
                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), timer_id - 0x160);
#endif
            }
            else if (timer_id == 0x250)
            {
                m_pStaticMode.Text = "   ";
            }
            else if (timer_id >= (0x300 + 1) && timer_id <= (0x300 + NUM_NODES))// && NodeControl.Count > 0)
            {
                    //NodeControl[timer_id - 0x300 - 1].m_Upld_Count = 0;
                    //NodeControl[timer_id - 0x300 - 1].m_Upld_Status = " -- ";
            }
            else if (timer_id >= (0x400 + 1) && timer_id <= (0x400 + NUM_NODES))
            {
                //NodeControl[timer_id - 0x400 - 1].m_Hit = false;

                //lpHub.lpDevices[(int)(timer_id - 0x400 - 1)].alarm = false;
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(timer_id - 0x400 - 1);
#else
                //this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), timer_id - 0x400 - 1);
                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), timer_id - 0x400);    // Modified, 2014.10.26
#endif
            }
            else if (timer_id >= (0x500 + 1) && timer_id <= (0x500 + NUM_NODES))
            {
                //NodeControl[timer_id - 0x500 - 1].m_Upld_Status = "--";
            }
            else if (timer_id == 0x600)                                        //  Added by Jason Chen, the timer event istoggled, which means that node population fail, need re-activate the action.
            {                                                                  //  2017.06.15
                mTryCount.Text = "Re Population happended!";
                this.BeginInvoke((MethodInvoker)delegate { m_Button_hubInfo_Click(this, null); });                 // added by Jason Chen, 2017.06.15
            }
            else if (timer_id >= (0x600 + 1) && timer_id <= (0x600 + NUM_NODES))
            {
                //if (NodeControl[timer_id - 0x600 - 1].m_Imm)
                //    NodeControl[timer_id - 0x600 - 1].m_Imm = false;                
            }
            else if (timer_id >= (0x700 + 1) && timer_id <= (0x700 + NUM_NODES))
            {
                Args[timer_id - 0x700 - 1].mHitFlag = false;
                Args[timer_id - 0x700 - 1].recv_count = 0;
                Args[timer_id - 0x700 - 1].recvbuffstr = "";

                string temp = " -->Hit Data transmission broken!....";
                temp = "Node " + (timer_id - 0x700).ToString("d2") + temp;
                //wirelessViewer.DebugDataBoxDisplay(temp + "\r\n");
                toolStripStatusLabelChart.Text = temp;
            }
            else if (timer_id >= (0x800 + 1) && timer_id <= (0x800 + NUM_NODES))
            {
                //NodeControl[timer_id - 0x800 - 1].m_PWR = false;
                lpHub.lpDevices[timer_id - 0x800 - 1].live = false;
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(timer_id - 0x800); // 2014.10.01
#else
                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), timer_id - 0x800);
#endif
            }
            else if (timer_id >= (0x900 + 1) && timer_id <= (0x900 + NUM_NODES))
            {
              //toolStripStatusLabelGFT.BackColor = Color.Transparent;
                toolStripStatusLabelChart.BackColor = Color.Transparent;
              //toolStripStatusLabelGFT.Text = "";
                toolStripStatusLabelChart.Text = "Online";
                //NodeControl[timer_id - 0x900 - 1].DataActive = false;
            }
            //else if (timer_id >= (0xA00 + 1) && timer_id <= (0xA00 + NUM_NODES))
           // {
                //mTimeStatus.Text = "";
                //mNameStatus.Text = "";
           // }
            else if (timer_id >= (0xB00 + 1) && timer_id <= (0xB00 + NUM_NODES))
            {
                if (NodeProfile[timer_id - 0xB00 - 1].Back_Command_buf.Count > 0)
                {
                    NodeProfile[timer_id - 0xB00 - 1].Back_Command_buf.Clear();
                    //if (TooltabPage16Exist)
                    //    m_Edit_Immediate.AppendText("---Node " + (timer_id - 0xB00).ToString("D2") + " Queue command Timeout, the command cancelled!\r\n");
                }
            }

            KillTimer(this.Handle, (uint)timer_id);
        }

#if true
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_TIMER:
                    //uint timer_id = (uint) (m.WParam);
                    try
                    {
                        this.Invoke(new TimerEventHandler(timerCallBack), new Object[] { this.Handle, WM_TIMER, (int)(m.WParam), 0 });
                    }
                    catch (Exception xx)
                    {
                        //if (TooltabPage16Exist)
                        //    m_Edit_Immediate.AppendText(xx.Message + "\r\n");
                    }
                    break;
                case WM_QUERYENDSESSION:
                    systemShutdown = true;
                    break;

            }
            base.WndProc(ref m);
        }
#endif
        private void gDataWReOpenSession(int sesID, string SessionTable, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    int num = 5;
                    string query = "UPDATE " + SessionTable + " SET OFF_TIME=ON_TIME,DURATION=0,IMPACT=0,ALARM=0,WTH_RANGE=0,WTH=0"
                                 + " WHERE (UID = @UID) AND (SID = @SID)";

                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@SID", sesID);
                        com.Parameters.AddWithValue("@UID", nodeID);
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                System.Diagnostics.Debug.WriteLine(me.ToString());
            }
        }
        private void gDataWUpdateSession(SESSION_DATA sessionData, string SessionTable)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    try
                    {
                        con.Open();
                    }
                    catch (Exception er)
                    {
                        MessageBox.Show(er.Message);
                    }

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    //SID=@SID,UID=@SID,
                    string query = "UPDATE SessionWireless SET SPORTS=@SPORTS,SDATE=@SDATE,ON_TIME=@ON_TIME,OFF_TIME=@OFF_TIME,DURATION=@DURATION,IMPACT=@IMPACT,ALARM=@ALARM,WTH_RANGE=@WTH_RANGE,WTH=@WTH,THRESHOLD=@THRESHOLD,ExerciseType=@ExerciseType"
                                 + " WHERE (UID = @UID) AND (SID = @SID)";

#if true
                    //using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        //com.ExecuteNonQuery();
                        //com.CommandText = query;

                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@DURATION", sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        num = com.ExecuteNonQuery();

                        //wirelessViewer.DebugDataBoxDisplay("Session Update OK!....\r\n");
                        //wirelessViewer.DebugDataBoxDisplay(" Session Update OK -->" + (sessionData.SID).ToString("x") + "\r\n");
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                //string dd = me.ToString();
                //throw;

                //wirelessViewer.DebugDataBoxDisplay("Session Update" + me.Message + "\r\n");
            }
        }

#if false
        public void uploadDataForWireless(byte[] recvBuff, int nodeID)
        {
            DateTime outTime;
            //          bool hasData = false;

            IMPACT_DATA iData = new IMPACT_DATA(); //impactData;            // Event
            SESSION_DATA sData = new SESSION_DATA();// sessionData;           // Session

            double[] azmuthElev = new double[2];
            //hold max point
            double[] myPoint = new double[3];
            int maxIndex = 0;
            iData.dataX = new Byte[120];
            iData.dataY = new Byte[120];
            iData.dataZ = new Byte[120];
            iData.rdataX = new Byte[64];
            iData.rdataY = new Byte[64];
            iData.rdataZ = new Byte[64];

            double x, y, z, v;
            //double xx, yy, zz;// vv;
            uint j = 0, k = 0;//, i=0;
            byte c = 0;
            double mLinMax = 0;
            TimeSpan sessionLen;
            sData.Impact = 0;
            sData.WTH = 0;
            sData.Alarm = 0;
            ushort myShort = 0;
            sData.sports = 0;
            double[] my40 = new double[40];
            try
            {
                //byte[] outputReportBuffer = new Byte[64 + 1];
                //byte[] inputReportBuffer = new Byte[64 + 1];


                //MyMarshalToForm("toolStripStatusLabelGFT", "Downloading Data by Wireless", System.Drawing.Color.Red);

                if (recvBuff[0] == 0x48)//for (j = 0; j < 11; j++)
                {

                    //save to database
                    //session

                    sessionStartTimeF = 1;
                    //sessionEndTimeF = 0;
                    if (ValidateTime(out outTime, recvBuff[1], recvBuff[2],
                                                  recvBuff[3], recvBuff[4],
                                                  recvBuff[5], recvBuff[6], myShort))
                        sData.OnTime = outTime;
                    else
                    {
                        //MessageBox.Show("Upload data has invalid Start date time");
                        wirelessViewer.DebugDataBoxDisplay("Upload data has invalid Start date time.\r\n");
                        return;
                    }

                    sData.Impact++;
                    //mLinMax = (double)(Math.Sqrt(byteToShort((byte)recvBuff[13], (byte)recvBuff[14])) * (ACC_SENSITIVITY_LSB * 4));
                    mLinMax = (double)(Math.Sqrt(byteToShort((byte)recvBuff[13], (byte)recvBuff[14])) * ACC_SENSITIVITY_LSB2);

                    //if (mLinMax >= gftProfile.tAlam)
                    //    sData.Alarm++;
                    //if (mLinMax >= (gftProfile.tAlam) * 0.9)
                    //    sData.WTH++;

                    if (mLinMax >= NodeProfile[nodeID - 1].alarmThres)
                        sData.Alarm++;
                    if (mLinMax >= (NodeProfile[nodeID - 1].alarmThres) * 0.9)
                        sData.WTH++;


                    //impact
                    int iData_sid = recvBuff[11];
                    iData.sid = WirelessSessionID + 1;
                    iData.sid = NodeProfile[nodeID - 1].SessionID;

                    WirelessEventID++;
                    iData.eid = WirelessEventID;
                    iData.eid = ++(NodeProfile[nodeID - 1].EventID);

                    myShort = byteToShort(recvBuff[7], recvBuff[8]);
                    if (myShort >= 1000)  // myShort =1000 is invalid data
                        myShort = 999;

                    if (ValidateTime(out outTime, recvBuff[1], recvBuff[2], recvBuff[3],
                                                  recvBuff[4], recvBuff[5], recvBuff[6],
                                                  myShort))
                        iData.time = outTime;
                    else
                    {
                        //MessageBox.Show("Upload data has invalid date time");
                        wirelessViewer.DebugDataBoxDisplay("Upload data has invalid date time.\r\n");
                        return;
                    }

                    iData.millisecond = myShort;
                    iData.rotX = (short)gyroDecode(recvBuff[15], recvBuff[16]);
                    iData.rotY = (short)gyroDecode(recvBuff[17], recvBuff[18]);
                    iData.rotZ = (short)gyroDecode(recvBuff[19], recvBuff[20]);

                    iData.rotR = (short)Math.Sqrt(iData.rotX * iData.rotX + iData.rotY * iData.rotY + iData.rotZ * iData.rotZ);
                    maxIndex = (byte)recvBuff[12];

                    k = 0;
                    //i = 0;
                    mLinMax = 0;
                    //xx = yy = zz = 0;
                    for (j = 1; j <= 6; j++)//if (j < 7)    // 1 ~ 6 for Acclerometer data points, 6 * 20 = 120 points
                    {
                        for (c = 0; c < 20; c++)
                        {
                            //builder.Length = 0;
                            x = AccDecode((byte)recvBuff[c * 3 + 21 + (j - 1) * 60]);
                            y = AccDecode((byte)recvBuff[c * 3 + 22 + (j - 1) * 60]);
                            z = AccDecode((byte)recvBuff[c * 3 + 23 + (j - 1) * 60]);
                            v = System.Math.Sqrt(x * x + y * y + z * z);
                            /*
                            if (mLinMax < v)
                            {
                                mLinMax = v;
                                xx = x; yy = y; zz = z;
                            }*/

                            if (k == maxIndex)
                            {
                                iData.linX = Math.Round(x, 2);
                                iData.linY = Math.Round(y, 2);
                                iData.linZ = Math.Round(z, 2);
                                iData.linR = Math.Round(v, 2);
                            }

                            iData.dataX[c + (j - 1) * 20] = (byte)recvBuff[c * 3 + 21 + (j - 1) * 60];
                            iData.dataY[c + (j - 1) * 20] = (byte)recvBuff[c * 3 + 22 + (j - 1) * 60];
                            iData.dataZ[c + (j - 1) * 20] = (byte)recvBuff[c * 3 + 23 + (j - 1) * 60];

                            if (k % 3 == 0)
                            {
                                my40[k / 3] = v;
                            }
                            k++;
                        }
                    }
                    /*
                    iData.linX = Math.Round(xx, 2);
                    iData.linY = Math.Round(yy, 2);
                    iData.linZ = Math.Round(zz, 2);
                    iData.linR = Math.Round(mLinMax, 2);*/

                    for (j = 7; j <= 10; j++)  //else    // 7 ~ 10 for gyroscope data points, 4 * 8 = 32 points, 6 bytes each point
                    {
                        for (c = 0; c < 8; c++)
                        {
                            // iData.rdataX  //for rotation data
                            iData.rdataX[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 381 + (j - 7) * 48];
                            iData.rdataX[c * 2 + (j - 7) * 16 + 1] = recvBuff[c * 6 + 382 + (j - 7) * 48];

                            iData.rdataY[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 383 + (j - 7) * 48];
                            iData.rdataY[c * 2 + (j - 7) * 16 + 1] = recvBuff[c * 6 + 384 + (j - 7) * 48];

                            iData.rdataZ[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 385 + (j - 7) * 48];
                            iData.rdataZ[c * 2 + (j - 7) * 16 + 1] = recvBuff[c * 6 + 386 + (j - 7) * 48];
                        }
                    }

                    iData.gsi = Math.Round(GSI(my40), 1);
                    iData.hic = Math.Round(HIC15(my40), 1);

                    myPoint[0] = iData.linX;
                    myPoint[1] = iData.linY;
                    myPoint[2] = iData.linZ;

                    //if ((tbUserGID.Text == GFT_J13) || (tbUserGID.Text == GFT_J3) || (tbUserGID.Text == GFT_J7))
                    //    azmuthElev = getAzmuthJaw(myPoint);
                    //else if (tbUserGID.Text == GFT_J12)
                        azmuthElev = getAzmuthYasZ(myPoint);
                    //else
                    //    azmuthElev = getAzmuthBack(myPoint);

                    iData.azimuth = Math.Round(azmuthElev[0], 2);
                    iData.elevation = Math.Round(azmuthElev[1], 2);

                    //save impact to database
                    gDataInsertImpact(iData, "EventsWireless");     // Insert iData into Event table
                    lpHub.lpDevices[nodeID - 1].temperature += 1;
                    lpHub.dgvLpPopulateView(nodeID);
                    //readySaveImpact = false;

                    // Begin to End this session
                    //if (sessionEndTimeF == 0)
                    {
                        //sessionEndTimeF = 1;
                        myShort = 120 / 3;

                        if (ValidateTime(out outTime, recvBuff[1], recvBuff[2],
                                                      recvBuff[3], recvBuff[4],
                                                      recvBuff[5], recvBuff[6], myShort))
                            sessionEndTime = outTime;
                        else
                        {
                            //MessageBox.Show("Upload data has invalid End date time");
                            wirelessViewer.DebugDataBoxDisplay("Upload data has invalid End date time.\r\n");
                            return;
                        }

                        if ((sData.Impact > 0) && (sessionStartTimeF == 1))
                        {
                            WirelessSessionID++;
                            sData.WTH -= sData.Alarm;
                            //session time 
                            sData.OffTime = sessionEndTime;
                            sData.Date = sData.OnTime;
                            sessionLen = (sData.OffTime - sData.OnTime);
                            sData.Duration = (int)sessionLen.TotalMinutes;

                            //sData.UID = curUser.uid;
                            sData.UID = nodeID;
                            //sData.Threshold = gftProfile.tAlam;
                            sData.Threshold = NodeProfile[nodeID - 1].alarmThres;

                            //sData.SID = SessionID;
                            //iData.sid = recvBuff[12];

                            sData.SID = WirelessSessionID;
                            sData.SID = NodeProfile[nodeID - 1].SessionID;

                            //needRecordSession = false;
                            //if (getSessionWireless(NodeProfile[nodeID - 1].SessionID, nodeID))
                            if (getSessionWireless(nodeID))
                            {
                                sData.WTH += NodeProfile[nodeID - 1].WTH;
                                sData.Impact += NodeProfile[nodeID - 1].IMPACT;
                                sData.Alarm  += NodeProfile[nodeID - 1].ALARM;
                                gDataWUpdateSession(sData, "SessionWireless");
                            }
                            else
                            {
                                gDataInsertSession(sData, "SessionWireless");     // Insert sData into Session table
                            }

                            if (tabControl4.SelectedIndex == 1)
                            {
                                /*
                                currentNodeID = nodeID;             // Current UID (Node ID)
                                curSessionID = NodeProfile[nodeID - 1].SessionID;

                                dataGetImpact(curSessionID, dataGridView3, "EventsWireless");
                                drawChart(curSessionID, gDataGetSessionSidThreshold(curSessionID, "SessionWireless"), chartViewer, "EventsWireless");

                                //chartRawLinW.Series.Clear();
                                //chartRawGyroW.Series.Clear();

                                LastSessionID = curSessionID;*/
                            }

                            //hasData = true;
                            sData.Impact = 0;
                            sData.Alarm = 0;
                            sData.WTH = 0;
                            sessionStartTimeF = 0;
                            //MyMarshalToForm("autoDraw", null, System.Drawing.Color.LightGray);// refresh table
                        }
                    }

                }//j
            }
            catch (Exception ex)
            {
                DisplayException(this.Name, ex);
                MessageBox.Show("uploadData()");
            }
        }

      
        public void processRecv(byte nodeID, byte[] ImpactPacket, int packetLen)
        {
            if (Args[nodeID - 1].nodeID == nodeID)
            {
                Args[nodeID - 1].impactFlag = false;
                Args[nodeID - 1].mHitFlag = false;
                Args[nodeID - 1].recv_count = 0;
                Args[nodeID - 1].recvbuffstr = "";

                if (packetLen == IMPACT_DATA_LEN)
                {
                    uploadDataForWireless(ImpactPacket, nodeID);
                }
            }
        }
#endif
        private void PacketProcess(int node)
        {
            int bat_level = 0;
            Int16 bat_current = 0;

          //string fullNameStr = NodeControl[node - 1].UserName;
            string fullNameStr = NodeProfile[node - 1].UserName;
            int size_t = fullNameStr.Split(DELIM_SPACE, StringSplitOptions.RemoveEmptyEntries).Length;
            string[] fullName = new string[size_t];

            fullName = fullNameStr.Split(DELIM_SPACE, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                NodeProfile[node - 1].firstName = fullName[0];
                NodeProfile[node - 1].lastName = fullName[1];
            }
            catch (Exception dd)
            {
                string ddd = dd.Message;
            }

            NodeProfile[node - 1].GID = gidToString(lpHub.lpDevices[node - 1].GID);
            if (Args[node - 1].recvbuff[26] != 0)
                NodeProfile[node - 1].playerNum = Args[node - 1].recvbuff[26];
            
            NodeProfile[node - 1].bat_level = gftBattery(Args[node - 1].recvbuff[35], Args[node - 1].recvbuff[27], Args[node - 1].recvbuff[28], Args[node - 1].recvbuff[29], Args[node - 1].recvbuff[30], Args[node - 1].recvbuff[47]);
            lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

            bat_current = (Int16)((Args[node - 1].recvbuff[31] << 8) | Args[node - 1].recvbuff[32]);
            //NodeProfile[node - 1].bat_currentfuel = bat_current;
            NodeProfile[node - 1].impactNum = bat_current;
            NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[33];                                 // Added by Jason chen, 2014.04.16

            NodeProfile[node - 1].alarmThres = Args[node - 1].recvbuff[34];                              // Added by Jason chen, 2014.04.15

            NodeProfile[node - 1].max17047Exist = (Args[node - 1].recvbuff[35] == 1) ? true : false;     // Added by Jason chen, 2014.04.16
            lpHub.lpDevices[node - 1].max17047Exist = NodeProfile[node - 1].max17047Exist;

            NodeProfile[node - 1].recThres = Args[node - 1].recvbuff[36];                                // Added by Jason chen, 2014.04.29
            NodeProfile[node - 1].AlarmMode = Args[node - 1].recvbuff[37];                               // Added by Jason chen, 2014.04.30

            NodeProfile[node - 1].iLocation[0] = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[38], Args[node - 1].recvbuff[39] }, 0);
            NodeProfile[node - 1].iLocation[1] = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[40], Args[node - 1].recvbuff[41] }, 0);
            NodeProfile[node - 1].iLocation[2] = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[42], Args[node - 1].recvbuff[43] }, 0);
            NodeProfile[node - 1].iLocation[3] = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[44], Args[node - 1].recvbuff[45] }, 0);
            NodeProfile[node - 1].mountCoefficient = ((float)Convert.ToInt16(Args[node - 1].recvbuff[48]))/100;
            if (NodeProfile[node - 1].mountCoefficient < 0.01) NodeProfile[node - 1].mountCoefficient = 1;
            NodeProfile[node - 1].mntLoc = Args[node - 1].recvbuff[46];


            //mFieldDataEnable.Checked = ((Args[node - 1].recvbuff[37] & 0x01) == 0x01) ? true : false;

            // Added by Jason Chen, 2014.04.30   
#if false                 
            mAlarmEnable.Checked = ((NodeProfile[node - 1].AlarmMode & 0x80) == 0) ? false : true;
            mAudio.Checked = ((NodeProfile[node - 1].AlarmMode & 0x10) == 0) ? false : true;
            mVisual.Checked = ((NodeProfile[node - 1].AlarmMode & 0x20) == 0) ? false : true;
            mInterlock.Checked = ((NodeProfile[node - 1].AlarmMode & 0x40) == 0) ? false : true;            
            mTestMode.Checked = ((NodeProfile[node - 1].AlarmMode & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2014.05.01

            mFieldDataEnable.Checked = ((NodeProfile[node - 1].AlarmMode & 0x04) == 0x04) ? true : false;        // Added by Jason Chen, 2015.05.21
            mProximityStatus.Checked = ((NodeProfile[node - 1].AlarmMode & 0x02) == 0x02) ? true : false;        // Added by Jason Chen, 2015.05.21
#endif
            //////////////////////////////////////////////////////////////// 
        }

        private void PowerOnProcess(int node)
        {

            if (/*(NodeControl[node - 1].m_PWR) &&*/ (lpHub.lpDevices[node - 1].live))
            {
                      
            }

            if (getGftWireless(node)) // Update
            {
                PacketProcess(node);  // it will update firstName and lastName of NodeProfile by NodeControl.UserName
                if (NodeProfile[node - 1].firstName != null)
                {
                    setGftWirelessNode(NodeProfile[node - 1], false);
                    lpHub.lpDevices[node - 1].playerNumer = NodeProfile[node - 1].playerNum;
                }
            }
            else//new NodeID
            {
                PacketProcess(node);
              //if (NodeControl[node - 1].UserName != null)
                if (NodeProfile[node - 1].UserName != null)
                {


                    NodeProfile[node - 1].nodeID = node;
                    NodeProfile[node - 1].playerNum = Args[node - 1].recvbuff[26];

                    setGftWirelessNode(NodeProfile[node - 1], true);
                    lpHub.lpDevices[node - 1].playerNumer = NodeProfile[node - 1].playerNum;
                }
            }
        }

        private int getRowNum(int nodeID)       // 09.11 -->   bool --> int
        {
            int i;
            int rowCount = wirelessViewer.dataGridView.Rows.Count;
            for (i = 0; i < rowCount; i++)
            {
                int cellValue = Convert.ToInt32(wirelessViewer.dataGridView.Rows[i].Cells[0].Value);
                if (cellValue == nodeID)
                {
                    return i;
                }
            }
            return -1;
        }

        private void BindingProcess(byte node)                                    // 2014.10.31, new function
        {
            lpHub.lpDevices[node - 1].nodeID = (byte)node;
            lpHub.lpDevices[node - 1].valid = true;
            string userName = System.Text.Encoding.ASCII.GetString(userProfile.name);
            userName = getFirstBeforeZero(DELIM_ZERO, userName);

            int size_t = userName.Split(DELIM_SPACE, StringSplitOptions.RemoveEmptyEntries).Length;
            string[] fullName = new string[size_t];
            fullName = userName.Split(DELIM_SPACE, StringSplitOptions.RemoveEmptyEntries);
            bool deviceRegistered = true;
            if (userName.Trim() != "" || userName.Trim().ToUpper() != "GFT") deviceRegistered = false;

            NodeProfile[node - 1].register = true;
            if (getGftWireless(node))
            {
                NodeProfile[node - 1].nodeID = node;
                NodeProfile[node - 1].GID = gidToString(userProfile.GID);
                NodeProfile[node - 1].AlarmMode = userProfile.AlarmMode;
                NodeProfile[node - 1].playerNum = userProfile.playerNo;
                NodeProfile[node - 1].recThres = userProfile.tR;
                NodeProfile[node - 1].alarmThres = userProfile.tAlarm;

                NodeProfile[node - 1].firstName = fullName[0];
                string mLastName;
                if (fullName.Length > 1)
                    mLastName = fullName[1];
                else
                    mLastName = "";
                NodeProfile[node - 1].lastName = mLastName;

                //lpHub.lpDevices[node - 1].name = (NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName).ToCharArray();
              //NodeControl[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                NodeProfile[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;

                lpHub.lpDevices[node - 1].playerNumer = NodeProfile[node - 1].playerNum;
              //NodeControl[node - 1].PlayerNumber = NodeProfile[node - 1].playerNum;
                setGftWirelessNode(NodeProfile[node - 1], false);
            }
            else
            {
                NodeProfile[node - 1].nodeID = node;
                NodeProfile[node - 1].GID = gidToString(userProfile.GID);
                NodeProfile[node - 1].AlarmMode = userProfile.AlarmMode;
                NodeProfile[node - 1].playerNum = userProfile.playerNo;
                NodeProfile[node - 1].recThres = userProfile.tR;
                NodeProfile[node - 1].alarmThres = userProfile.tAlarm;

                NodeProfile[node - 1].firstName = fullName[0];
                string mLastName;
                if (fullName.Length > 1)
                    mLastName = fullName[1];
                else
                    mLastName = "";
                NodeProfile[node - 1].lastName = mLastName;
                setGftWirelessNode(NodeProfile[node - 1], true);
            }

            Args[node - 1].nodeID = node;
            Args[node - 1].nodeLive = true;
            Args[node - 1].recvbuff = new byte[574];

          //if (node >= 1 && node <= NUM_NODES) NodeControl[node - 1].Enabled = lpHub.lpDevices[node - 1].valid;

            if (getRowNum(node) < 0)
            {
                DataGridViewRow myDGVR = new DataGridViewRow();
                DataGridViewCell cellID = new DataGridViewTextBoxCell();
                cellID.Value = node;
                myDGVR.Cells.Add(cellID);
                myDGVR.Height = 25;
                wirelessViewer.dataGridView.Rows.Add(myDGVR);
                wirelessViewer.dataGridView.Sort(wirelessViewer.dataGridView.Columns[0], ListSortDirection.Ascending);
            }
            lpHub.lpDevices[node - 1].live = true;
            lpHub.lpDevices[node - 1].alarm = ((userProfile.AlarmMode & 0x01) == 0x01) ? true : false;
            lpHub.lpDevices[node - 1].playerNumer = userProfile.playerNo;
            lpHub.lpDevices[node - 1].name = (NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName).ToCharArray();
            lpHub.lpDevices[node - 1].mode = 0x81;

            lpHub.dgvLpPopulateView(node); // 2014.10.31
            mtimer.Enabled = true;
            gft_node = node;

            int row = getRowNum(gft_node);                         // 2014.10.30
            if (row >= 0)
            {
                wirelessViewer.dataGridView.FirstDisplayedCell = null; // 2014.10.30
                wirelessViewer.dataGridView.ClearSelection();          // 2014.10.30
                wirelessViewer.dataGridView.Rows[row].Selected = true; // 2014.10.30   
                //m_button_Clone.Enabled = true;
            }
            onDeviceProfileReceived(deviceRegistered);
            //////////////////////////////////////////////////////////////////////////            // 2014.10.30
        }

        private void ClearAllLocalDataBase()                                                                // 2014.10.31
        {
            int myUID = 0;
            int FirstSID = 0;
            int rowCount = 0;
            //int rowCount = gftWirelessView.Rows.Count;
            DataTable t = getGftUserNum();
            if (t != null)
                rowCount = t.Rows.Count;

            //DataGridViewRow cur_row = gftWirelessView.CurrentRow;           
            for (int i = 0; i < rowCount; i++)
            {
                try
                {
                    //myUID = (int)(t.Rows[i].Cells[1].Value);
                    myUID = (int)(t.Rows[i].Field<int>(1));
                }
                catch (Exception er)
                {
                    string temp = er.Message;
                    continue;
                }
                for (; ; )
                {
                    FirstSID = gDataGetSessionFirstSID(myUID);
                    if (FirstSID == 0)
                        break;
                    mDelete_EventRecord(FirstSID);                                      // Maybe don't need to erase the database, 2014.10.14
                    mDelete_SessionRecord(FirstSID);                                    // Maybe don't need to erase the database, 2014.10.14
                }

                NodeProfile[myUID - 1].register = false;
                mDelete_gftWireless(myUID);
            }
        }

        private void RxPacketProcess(ref byte[] buff, bool USB_or_BLE)
        {
            //System.Diagnostics.Debug.Write("Node Data");
            //for (int i = 0; i < buff.Length; i++) System.Diagnostics.Debug.Write("\t" + buff[i].ToString());
            //System.Diagnostics.Debug.WriteLine("");

            string temp;
            int node;

            USB_OR_BLE = USB_or_BLE;
#if false
            byte[] buff;
            if (HIDinBuffer.Count > 0)
                buff = HIDinBuffer.Dequeue();
            else
                return;
#endif
            //node = buff[1];
            //if (node < 1 || node > NUM_NODES)
            //    return;

            if (buff[0] == MSG_HUB_TO_HOST_MODE_SHELF)
            {
                m_pStaticMode.Text = string.Format("{0:d3}", buff[1]);
                SetTimer(this.Handle, 0x250, 500, null);//new TimerEventHandler(timerCallBack));
            }
            else if (buff[0] == MSG_HUB_TO_HOST_SHOW_SHELF)
            {
                node = buff[2];

                string textInfo = DateTime.UtcNow.ToLongTimeString() + " - ";

                if (buff[1] == 1)
                    textInfo += "Add: ";
                else if (buff[1] == 2)
                    textInfo += "Get: ";
                else if (buff[1] == 3)
                    textInfo += "Rem: ";
                else
                    textInfo += "???: ";

                textInfo += string.Format("{0:x2}{1:x2}{2:x2} - ", buff[2], buff[3], buff[4]);
                if (node > 64 - 5) node = 64 - 5;

                temp = "";
                for (int i = 0; i <= node; i++)
                {
                    temp += string.Format("{0:x2} ", buff[i + 5]);
                    if (((i + 1) % 16) == 0)
                        temp += "\r\n                       ";
                }
                textInfo += temp + "\r\n";
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo);
                //wirelessViewer.DebugDataBoxDisplay(textInfo);
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(node); // 2014.10.01
#else
                //this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);    // 2014.10.26 removed
#endif

                //lpHub.dgvLpPopulateView(node); // 2014.10.01
                //lpHub.lpDevices[node-1].updateDgv = true;

            }
            else if (buff[0] == MSG_HUB_TO_HOST_NODE_DATA)
            {
                node = buff[1];
                //int plen = buff[3];
                //if (plen != (CHUNK_IMMEDIATE + 1))
                    NodeDataProcess(ref buff, USB_or_BLE);        // Main Process 
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(node); // 2014.10.01
#else
                //this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);     // 2014.10.26 removed
#endif
                //this.BeginInvoke((MethodInvoker)delegate { deviceOnConnect(node); }); 
            }
            else if (buff[0] == MSG_HUB_TO_HOST_UPLOAD)
            {
                // new process by Jason Chen, Oct. 26, 2017
                KillTimer(this.Handle, 0x100);

                node = (int)buff[1];
                string textInfo;

                //= DateTime.UtcNow.ToLongTimeString() + " - Upld - ";
                //if (buff[1] == 0) textInfo += "OK: Entering Upload Mode.\r\n";
                //else if (buff[1] == 2) textInfo += "Error: Already in Upload mode.\r\n";
                //else if (buff[1] == 4) textInfo += "Error: no Shelf node present.\r\n";
                //else if (buff[1] == 3) textInfo += "Error: Bind mode active.\r\n";
                //else if (buff[1] == 5) textInfo += "Error: Sync mode active...\r\n";
                //else if (buff[1] == 1)
                if (buff[2] == 0)
                {
                    textInfo = "\r\n" + DateTime.Now.ToLongTimeString() + " - UserProfile - ";
                    textInfo += "Node" + node.ToString("d3") + string.Format("    GID = 0x{0:x2}{1:x2}{2:x2}{3:x2}{4:x2}{5:x2}, tAlarm = {6:d3}, PlayerNum = {7:d3}", buff[3], buff[4], buff[5], buff[6], buff[7], buff[8], buff[9], buff[10]);

                    byte[] name1 = new byte[7];
                    Array.Copy(buff, 11, name1, 0, 6);
                    name1[6] = 0;
                    string name0_s = System.Text.Encoding.ASCII.GetString(name1);
                    name0_s = name0_s.Substring(0, name0_s.IndexOf("\0"));
                    textInfo += ", Player Name = " + name0_s;// +"\r\n";
                }
                else if (buff[2] == 1)
                {
                    byte[] name1 = new byte[15];
                    Array.Copy(buff, 3, name1, 0, 15);
                    name1[14] = 0;
                    string name1_s = System.Text.Encoding.ASCII.GetString(name1);
                    name1_s = name1_s.Substring(0, name1_s.IndexOf("\0"));

                    textInfo = /*"                                                   Player Name1 = " + */ name1_s;// + "\r\n\r\n"; ;
                    //textInfo += "Node" + node.ToString("d3") + string.Format("    GID = 0x{0:x2}{1:x2}{2:x2}{3:x2}{4:x2}{5:x2}, tAlarm = {6:d3}\r\n", buff[2], buff[3], buff[4], buff[5], buff[6], buff[7], buff[8]);
                }
                else     // for tR, Loc[0]~Loc[8], Mnloc, Added by Jason Chen, 2018.09.25
                {                    
                    //textInfo = "             and tR, Loc, mntLoc" + "\r\n\r\n"; ;
                    byte[] name1 = new byte[15];
                    Array.Copy(buff, 3, name1, 0, 11);

                    textInfo = ", tR = " + name1[0].ToString("d2");
                    string name01 = string.Format("{0:x2}{1:x2}{2:x2}{3:x2}{4:x2}{5:x2}{6:x2}{7:x2}{8:x2}", name1[1], name1[2], name1[3], name1[4], name1[5], name1[6], name1[7], name1[8], name1[9], name1[10]);
                    name01 = name01.ToUpper();
                    string name02 = string.Format("{0:x2}", name1[10]);
                    name02 = name02.ToUpper();
                    textInfo += ", Loc = 0x" + name01 + ", mntLoc = 0x" + name02 + "\r\n";
                }

                if (buff[2] == 0)
                {
                    if (!USB_or_BLE)
                    {
                        byte[] txBuff = new byte[65];
                        txBuff[1] = MSG_HOST_TO_HUB_UPLOAD;
                        txBuff[2] = (byte)Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value);// (byte)Convert.ToInt16(mNodeID.Text);
                        txBuff[3] = 1;
                        GftWirelessHidHub.WriteOutput(ref txBuff);
                        //m_Edit_RxPacket.AppendText(textInfo);
                    }
                    else
                    {
                        byte[] txBuff = new byte[20];
                        txBuff[0] = MSG_HOST_TO_HUB_UPLOAD;
                        txBuff[1] = (byte)Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value);// (byte)Convert.ToInt16(mNodeID.Text);
                        txBuff[2] = 1;
                        mBLEInterface.SendData(ref txBuff, 20);
                    }

                    if (RxLog != null)//&& RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        RxLog.logText2(textInfo);
                    }
                }
                else if (buff[2] == 1)                                  // for Getting tR, Loc[0]~Loc[8], Mnloc CMD ( Third packet, Added by Jason Chen, 2018.09.25
                {
                    if (!USB_or_BLE)
                    {
                        byte[] txBuff = new byte[65];
                        txBuff[1] = MSG_HOST_TO_HUB_UPLOAD;
                        txBuff[2] = (byte)Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value);// (byte)Convert.ToInt16(mNodeID.Text);
                        txBuff[3] = 2;                          // <----- Third Packet Command
                        GftWirelessHidHub.WriteOutput(ref txBuff);
                        //m_Edit_RxPacket.AppendText(textInfo);
                    }
                    else
                    {
                        byte[] txBuff = new byte[20];
                        txBuff[0] = MSG_HOST_TO_HUB_UPLOAD;
                        txBuff[1] = (byte)Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value);// (byte)Convert.ToInt16(mNodeID.Text);
                        txBuff[2] = 2;                          // <----- Third Packet Command
                        mBLEInterface.SendData(ref txBuff, 20);
                    }
                    if (RxLog != null)//&& RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        RxLog.logText2(textInfo);
                    }
                }
                else
                {
                    //m_Edit_RxPacket.AppendText(textInfo + "\r\n");
                    if (RxLog != null)//&& RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        RxLog.logText2(textInfo);
                    }
                }

                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo);


#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(node); // 2014.10.01
#else
                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);
#endif
            }
            else if (buff[0] == MSG_HUB_TO_HOST_BCD_CMD)  // Added by Jason, 20130828
            {
                //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140211
                node = buff[1];

                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("---" + (display_count++).ToString("D4") + " Hub got BCD CMD " + buff[2].ToString() + " To GFT " + node.ToString("D2") + "----\r\n");
                //if (display_count > 9999)
                //    display_count = 0;
                          
                //KillTimer(this.Handle, 0x100);

                //m_Edit_Immediate.AppendText("BCD Data Got it\r\n");
                //wirelessViewer.DebugDataBoxDisplay("BCD Data Got it\r\n");
            }
            else if (buff[0] == MSG_HUB_TO_HOST_NODE_INFO)
            {
                if (dongle_mid.Trim() != "" && dongle_mid != "00000000") // Don't accept node info if we don't have a valid MID
                {
                    DongleEmpty.Text = dongle_mid;
                    KillTimer(this.Handle, 0x600);              // Successfully get the nodes populated command, so don't need to reactivat the action, 2017.06.15

                    int j = 0;
                    byte nodeID;
                    byte[] MID = new byte[4];

                    KillTimer(this.Handle, 0x100);

                    string textInfo = string.Empty;
                    if (buff[1] == 0)   // First Group
                        textInfo = DateTime.UtcNow.ToLongTimeString() + " - Node Info\r\n";

                    for (int n = (1 + buff[1] * 7); n <= (7 + buff[1] * 7); n++)
                    {
                        if (buff[2 + j + 1] == 0)  // node ID , node = 0 ---> No node exists
                        {
                            j += 8;
                            continue;
                        }
                        textInfo += string.Format("   {0:x2}:", n);
                        for (byte i = 0; i < 8; i++)
                        {
                            textInfo += string.Format("{0:x2} ", buff[i + 2 + j]);
                        }

                        nodeID = buff[j + 3];

                        Array.Copy(buff, j + 4, MID, 0, 4);

                         //if (nodeID == 7)
                        //      Thread.Sleep(0);
                        lpHub.lpDevices[nodeID - 1].nodeID = nodeID;
                        lpHub.lpDevices[nodeID - 1].valid = true;
                        lpHub.lpDevices[nodeID - 1].nodePwrMode = buff[j + 2];
                        lpHub.lpDevices[nodeID - 1].MID = MID;


                        if (getGftWireless(nodeID))
                        {
                            if (NodeProfile[nodeID - 1].firstName != null)
                            {
                                //m_cName[nodeID - 1] = NodeProfile[nodeID - 1].firstName;
                                //NodeControl[nodeID - 1].UserName = NodeProfile[nodeID - 1].firstName;
                                lpHub.lpDevices[nodeID - 1].name = (NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName).ToCharArray();
                                //NodeControl[nodeID - 1].UserName = NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                                NodeProfile[nodeID - 1].UserName = NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                            }

                            lpHub.lpDevices[nodeID - 1].playerNumer = NodeProfile[nodeID - 1].playerNum;
                            //NodeControl[nodeID - 1].PlayerNumber = NodeProfile[nodeID - 1].playerNum;
                            NodeProfile[nodeID - 1].register = true;
                        }
                        else
                        {
                            NodeProfile[nodeID - 1].nodeID = nodeID;
                            //Thread.Sleep(500);  // Commented out 09.28.2014
                            //lpHub.lpDevices[nodeID - 1].playerNumer = nodeID;           // 2014.10.31
                            //NodeControl[nodeID - 1].PlayerNumber = nodeID;              // 2014.10.31
                            NodeProfile[nodeID - 1].register = false;                   // 2014.11.11  for multple dongle recognization                        
                            NodeProfile[nodeID - 1].Back_Command_buf.Clear();           // 2014.11.11
                            NodeProfile[nodeID - 1].firstName = "      ";               // 2014.11.11
                            NodeProfile[nodeID - 1].lastName = "      "; ;              // 2014.11.11


                            setGftWirelessNode(NodeProfile[nodeID - 1], true);
                        }

                        Args[nodeID - 1].nodeID = nodeID;
                        Args[nodeID - 1].nodeLive = true;
                        Args[nodeID - 1].recvbuff = new byte[574];

                        //if (nodeID >= 1 && nodeID <= NUM_NODES)
                        //    NodeControl[nodeID - 1].Enabled = lpHub.lpDevices[nodeID - 1].valid;

                        j += 8;
                        textInfo += "\r\n";

                        if (getRowNum(nodeID) < 0)                                          // 09.11 
                        {
                            DataGridViewRow myDGVR = new DataGridViewRow();                // Added by Jason Chen, 2014.05.14
                            DataGridViewCell cellID = new DataGridViewTextBoxCell();
                            cellID.Value = nodeID;
                            myDGVR.Cells.Add(cellID);
#if ENABLE_DASHBOARD
                            myDGVR.Height = 25;
#endif
                            wirelessViewer.dataGridView.Rows.Add(myDGVR);

                            wirelessViewer.dataGridView.Sort(wirelessViewer.dataGridView.Columns[0], ListSortDirection.Ascending);
                        }

                        if (new_dongle_plugged)                                // 2014.11.11
                        {                                                      // 2014.11.11
                            NodeProfile[nodeID - 1].processValue = 0;          // 2014.11.11
                            NodeProfile[nodeID - 1].deleteFlag = false;        // 2014.11.11
                            NodeProfile[nodeID - 1].uploadFlag = false;        // 2014.11.11
                            NodeProfile[nodeID - 1].upload_req_flag = false;   // 2014.11.11
                            NodeProfile[nodeID - 1].uploadNum_Flag = 0;        // 2014.11.11
                        }                                                      // 2014.11.11

                        int row = getRowNum(nodeID);                                                      // 09.11
                        if (row >= 0)                                                                     // 09.11
                        {
                            lpHub.lpDevices[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;    // 09.11
                            //if (lpHub.lpDevices[nodeID - 1].upldFlag)                                     // 2014.11.11, Changed //09.11
                            {                                                                             // 09.11
                                //if (!NodeControl[nodeID - 1].Enabled)                                   // 09.11
                                //    NodeControl[nodeID - 1].Enabled = true;                             // 09.11

                                //NodeControl[nodeID - 1].m_PWR = true;                                   // 09.11
                                //NodeControl[nodeID - 1].m_Usb = false;                                  // 09.11

                                lpHub.lpDevices[nodeID - 1].live = false;                               //2014.10.29 modified  // 09.11
                                lpHub.lpDevices[nodeID - 1].mode = 0;                                   //2014.10.29 modified  // 09.11----> go to Power On mode;
                                lpHub.lpDevices[nodeID - 1].charging = false;

                                lpHub.lpDevices[nodeID - 1].progressValue = NodeProfile[nodeID - 1].processValue;   // 2014.10.29 added
                                /*if (NodeProfile[nodeID - 1].sData.OnTime > DateTime.MinValue)
                                {
                                    NodeProfile[nodeID - 1].sData.OffTime = DateTime.UtcNow;
                                    NodeProfile[nodeID - 1].sData.Duration = (int)(NodeProfile[nodeID - 1].sData.OffTime - NodeProfile[nodeID - 1].sData.OnTime).TotalMinutes;
                                    NodeProfile[nodeID - 1].sData.GID = NodeProfile[nodeID - 1].GID;
                                    gDataWUpdateSession(NodeProfile[nodeID - 1].sData, "SessionWireless", nodeID); // Save the off time
                                    NodeProfile[nodeID - 1].sData = new uploadDataForWirelessProcess.SESSION_DATA();
                                }*/
                            }
                        }
#if USE_ORIGINAL_REFRESH
                    lpHub.dgvLpPopulateView(nodeID); // 2014.10.01
#else
                        this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), nodeID);
                        //this.Invoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), nodeID);                    

#endif
                        Application.DoEvents();                          //2014.10.29
                        Thread.Sleep(0);
                    }

                    if (textInfo.Length != 0)
                    {
                        //if (TooltabPage16Exist)
                        //m_Edit_Immediate.AppendText(textInfo);
                        //wirelessViewer.DebugCmdBoxDisplay(textInfo);   2014.10.28
                    }
                }
                else
                    DongleEmpty.Text = "Invalid Dongle Mid" + dongle_mid;
            }
            else if (buff[0] == MSG_HUB_TO_HOST_NODE_BLE_INFO)
            {
                if (dongle_mid.Trim() != "" && dongle_mid != "00000000") // Don't accept node info if we don't have a valid MID
                {
                    DongleEmpty.Text = dongle_mid;
                    KillTimer(this.Handle, 0x600);              // Successfully get the nodes populated command, so don't need to reactivat the action, 2017.06.15

                    int j = 0;
                    byte nodeID;
                    byte[] MID = new byte[4];

                    KillTimer(this.Handle, 0x100);

                    string textInfo = string.Empty;
                    if (buff[1] == 0)   // First Group
                        textInfo = DateTime.UtcNow.ToLongTimeString() + " - Node Info\r\n";

                    for (int n = (1 + buff[1] * 2); n <= (2 + buff[1] * 2); n++)
                    {
                        if (buff[2 + j + 1] == 0)  // node ID , node = 0 ---> No node exists
                        {
                            j += 8;
                            continue;
                        }
                        textInfo += string.Format("   {0:x2}:", n);
                        for (byte i = 0; i < 8; i++)
                        {
                            textInfo += string.Format("{0:x2} ", buff[i + 2 + j]);
                        }

                        nodeID = buff[j + 3];

                        Array.Copy(buff, j + 4, MID, 0, 4);

                        //if (nodeID == 7)
                        //    Thread.Sleep(0);
                        lpHub.lpDevices[nodeID - 1].nodeID = nodeID;
                        lpHub.lpDevices[nodeID - 1].valid = true;
                        lpHub.lpDevices[nodeID - 1].nodePwrMode = buff[j + 2];
                        lpHub.lpDevices[nodeID - 1].MID = MID;


                        if (getGftWireless(nodeID))
                        {
                            if (NodeProfile[nodeID - 1].firstName != null)
                            {
                                //m_cName[nodeID - 1] = NodeProfile[nodeID - 1].firstName;
                                //NodeControl[nodeID - 1].UserName = NodeProfile[nodeID - 1].firstName;
                                lpHub.lpDevices[nodeID - 1].name = (NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName).ToCharArray();
                                //NodeControl[nodeID - 1].UserName = NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                                NodeProfile[nodeID - 1].UserName = NodeProfile[nodeID - 1].firstName + " " + NodeProfile[nodeID - 1].lastName;
                            }

                            lpHub.lpDevices[nodeID - 1].playerNumer = NodeProfile[nodeID - 1].playerNum;
                            //NodeControl[nodeID - 1].PlayerNumber = NodeProfile[nodeID - 1].playerNum;
                            NodeProfile[nodeID - 1].register = true;
                        }
                        else
                        {
                            NodeProfile[nodeID - 1].nodeID = nodeID;
                            //Thread.Sleep(500);  // Commented out 09.28.2014
                            //lpHub.lpDevices[nodeID - 1].playerNumer = nodeID;           // 2014.10.31
                            //NodeControl[nodeID - 1].PlayerNumber = nodeID;              // 2014.10.31
                            NodeProfile[nodeID - 1].register = false;                   // 2014.11.11  for multple dongle recognization                        
                            NodeProfile[nodeID - 1].Back_Command_buf.Clear();           // 2014.11.11
                            NodeProfile[nodeID - 1].firstName = "      ";               // 2014.11.11
                            NodeProfile[nodeID - 1].lastName = "      "; ;              // 2014.11.11


                            setGftWirelessNode(NodeProfile[nodeID - 1], true);
                        }

                        Args[nodeID - 1].nodeID = nodeID;
                        Args[nodeID - 1].nodeLive = true;
                        Args[nodeID - 1].recvbuff = new byte[574];

                        //if (nodeID >= 1 && nodeID <= NUM_NODES)
                        //    NodeControl[nodeID - 1].Enabled = lpHub.lpDevices[nodeID - 1].valid;

                        j += 8;
                        textInfo += "\r\n";

                        if (getRowNum(nodeID) < 0)                                          // 09.11 
                        {
                            DataGridViewRow myDGVR = new DataGridViewRow();                // Added by Jason Chen, 2014.05.14
                            DataGridViewCell cellID = new DataGridViewTextBoxCell();
                            cellID.Value = nodeID;
                            myDGVR.Cells.Add(cellID);
#if ENABLE_DASHBOARD
                            myDGVR.Height = 25;
#endif
                            wirelessViewer.dataGridView.Rows.Add(myDGVR);

                            wirelessViewer.dataGridView.Sort(wirelessViewer.dataGridView.Columns[0], ListSortDirection.Ascending);
                        }

                        if (new_dongle_plugged)                                // 2014.11.11
                        {                                                      // 2014.11.11
                            NodeProfile[nodeID - 1].processValue = 0;          // 2014.11.11
                            NodeProfile[nodeID - 1].deleteFlag = false;        // 2014.11.11
                            NodeProfile[nodeID - 1].uploadFlag = false;        // 2014.11.11
                            NodeProfile[nodeID - 1].upload_req_flag = false;   // 2014.11.11
                            NodeProfile[nodeID - 1].uploadNum_Flag = 0;        // 2014.11.11
                        }                                                      // 2014.11.11

                        int row = getRowNum(nodeID);                                                      // 09.11
                        if (row >= 0)                                                                     // 09.11
                        {
                            lpHub.lpDevices[nodeID - 1].upldFlag = NodeProfile[nodeID - 1].uploadFlag;    // 09.11
                            //if (lpHub.lpDevices[nodeID - 1].upldFlag)                                     // 2014.11.11, Changed //09.11
                            {                                                                             // 09.11
                                //if (!NodeControl[nodeID - 1].Enabled)                                   // 09.11
                                //    NodeControl[nodeID - 1].Enabled = true;                             // 09.11

                                //NodeControl[nodeID - 1].m_PWR = true;                                   // 09.11
                                //NodeControl[nodeID - 1].m_Usb = false;                                  // 09.11

                                lpHub.lpDevices[nodeID - 1].live = false;                               //2014.10.29 modified  // 09.11
                                lpHub.lpDevices[nodeID - 1].mode = 0;                                   //2014.10.29 modified  // 09.11----> go to Power On mode;
                                lpHub.lpDevices[nodeID - 1].charging = false;

                                lpHub.lpDevices[nodeID - 1].progressValue = NodeProfile[nodeID - 1].processValue;   // 2014.10.29 added
                                /*if (NodeProfile[nodeID - 1].sData.OnTime > DateTime.MinValue)
                                {
                                    NodeProfile[nodeID - 1].sData.OffTime = DateTime.UtcNow;
                                    NodeProfile[nodeID - 1].sData.Duration = (int)(NodeProfile[nodeID - 1].sData.OffTime - NodeProfile[nodeID - 1].sData.OnTime).TotalMinutes;
                                    NodeProfile[nodeID - 1].sData.GID = NodeProfile[nodeID - 1].GID;
                                    gDataWUpdateSession(NodeProfile[nodeID - 1].sData, "SessionWireless", nodeID); // Save the off time
                                    NodeProfile[nodeID - 1].sData = new uploadDataForWirelessProcess.SESSION_DATA();
                                }*/
                            }
                        }
#if USE_ORIGINAL_REFRESH
                    lpHub.dgvLpPopulateView(nodeID); // 2014.10.01
#else
                        this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), nodeID);
                        //this.Invoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), nodeID);                    

#endif
                        Application.DoEvents();                          //2014.10.29
                        Thread.Sleep(0);
                    }

                    if (textInfo.Length != 0)
                    {
                        //if (TooltabPage16Exist)
                        //m_Edit_Immediate.AppendText(textInfo);
                        //wirelessViewer.DebugCmdBoxDisplay(textInfo);   2014.10.28
                    }
                }
                else
                    DongleEmpty.Text = "Invalid Dongle Mid" + dongle_mid;
            }
            else if (buff[0] == MSG_HUB_TO_HOST_GET_INFO)
            {

                KillTimer(this.Handle, 0x100);

                string textInfo = DateTime.UtcNow.ToLongTimeString() + " - Hub Info\r\n";

                //if (TooltabPage16Exist)
                    //m_Edit_Immediate.AppendText(textInfo);
                //wirelessViewer.DebugCmdBoxDisplay(textInfo); 2014.10.28

                textInfo = string.Format("    sopIdx={0:x2}   chBase={1:x2}  chHop={2:x2}  hubSeed={3:x2}{4:x2}  maxNode={5:d2}  BCD={6:x2}\r\n",
                                              buff[1], buff[2], buff[3], buff[4], buff[5], buff[9], buff[10]);

                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo);
                //wirelessViewer.DebugCmdBoxDisplay(textInfo);   2014.10.28

                lpHub.sopIdx = buff[1];
                lpHub.chBase = buff[2];
                lpHub.chHop = buff[3];
                lpHub.hubSeed = (short)(buff[4] * 256 + buff[5]);
                lpHub.maxNodes = buff[9];
                lpHub.BCD = buff[10];

                dongle_mid = string.Format("{0:x2}{1:x2}{2:x2}{3:x4}", lpHub.sopIdx, lpHub.chBase, lpHub.chHop, lpHub.hubSeed);  //2014.11.11

                if ((Settings.Default.sopIdx != lpHub.sopIdx) ||
                   (Settings.Default.chBase != lpHub.chBase) ||
                   (Settings.Default.chHop != lpHub.chHop) ||
                   (Settings.Default.hubSeed != lpHub.hubSeed))
                {
                    //ClearAllLocalDataBase();
                    Settings.Default.sopIdx = lpHub.sopIdx;
                    Settings.Default.chBase = lpHub.chBase;
                    Settings.Default.chHop = lpHub.chHop;
                    Settings.Default.hubSeed = lpHub.hubSeed;
                    Settings.Default.Save();

                    new_dongle_plugged = true;                    // 2014.11.11
                }
                else
                {
                    //ClearAllLocalDataBase();
                }


                if (!USB_or_BLE)
                {
                    for (int i = 0; i < 65/*myHidReportIn.RptByteLen*/; i++)
                    {
                        buff[i] = buff[i + 18];
                        if (buff[i] == 0x00) break;
                    }

                    string buildtime = System.Text.Encoding.ASCII.GetString(buff);
                    textInfo = string.Format("    chnMin={0:x2}  chnMax={1:x2}  curCh={2:x2}  upldMod={3:x2}       build={4:s\r\n}",
                                                  buff[11], buff[12], buff[13], buff[14], buildtime);
                    textInfo = textInfo.TrimEnd(DELIM_ZERO);

                    lpHub.chnMin = buff[11];
                    lpHub.chnMax = buff[12];
                    lpHub.curCh = buff[13];
                    lpHub.upldMod = buff[14];
                }

                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo);
                //wirelessViewer.DebugCmdBoxDisplay(textInfo);  2014.10.28

                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText("\r\n");
                //wirelessViewer.DebugCmdBoxDisplay("\r\n");   2014.10.28
                if (!USB_or_BLE)
                    this.BeginInvoke((MethodInvoker)delegate { m_button_NodeInfo_Click(this, null); });       // 2014.10.31, added
                else
                    this.BeginInvoke((MethodInvoker)delegate { mGetNodeInfo_Click(this, null); });             // 2017.09.25, added
            }
            else if (buff[0] == MSG_HUB_TO_HOST_RESET)
            {
                KillTimer(this.Handle, 0x100);

                string textInfo = DateTime.UtcNow.ToLongTimeString() + " - Reset - ";
                if (buff[1] == 0)
                {
                    //m_Edit_Immediate.Clear();
                    //m_Button_hubInfo.Text = "Hub Info";
                    //m_button_NodeInfo.Text = "Mode Info";
                    textInfo += "OK...\r\n";
                }
                else if (buff[1] == 1) textInfo += "PnP...\r\n";
                else if (buff[1] == 0xFF) textInfo += "Nok But...\r\n";
                else textInfo += "???...\r\n";

                //if ((buff[1] == 0) || (buff[1] == 1))
                //{
                //    for (int i = 1; i <= NUM_NODES; i++) ResetNodeControls(i);
                //}
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo + "\r\n");

                if(!USB_or_BLE)
                    this.BeginInvoke((MethodInvoker)delegate { m_Button_hubInfo_Click(this, null); });                 // 2014.10.31, added
                else
                    this.BeginInvoke((MethodInvoker)delegate { mGetHubInfo_Click(this, null); });                 // 2017.09.25, added

                
            }
            else if (buff[0] == MSG_HUB_TO_HOST_SYNC)
            {
                KillTimer(this.Handle, 0x100);
                string textInfo = DateTime.UtcNow.ToLongTimeString() + " - Sync - ";
                if (buff[1] == 0) textInfo += "Ok: Entering Sync Mode, wait (15+15) sec...\r\n";
                else if (buff[1] == 2) textInfo += "Error: Already in Sync mode.\r\n";
                else if (buff[1] == 4) textInfo += "Error: Shelf node present.\r\n";
                else if (buff[1] == 3) textInfo += "Error: Bind mode active.\r\n";
                else if (buff[1] == 5) textInfo += "Error: Upload mode active...\r\n";
                else textInfo += "???...\r\n";

                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo + "\r\n");
            }
            else if (buff[0] == MSG_HUB_TO_HOST_BIND_MODE)
            {
                KillTimer(this.Handle, 0x100);
                node = buff[2];
                string textInfo = DateTime.UtcNow.ToLongTimeString() + " - Bind - ";


                if (buff[1] == 0xff) textInfo += "Error: Nok But...\r\n";
                else if (buff[1] == 2) textInfo += "Error: Already in Bind mode...\r\n";
                else if (buff[1] == 0) textInfo += "Ok: Entering Bind mode...\r\n";
                else if (buff[1] == 3) textInfo += "Error: Upload mode active...\r\n";
                else if (buff[1] == 4) textInfo += "Error: Shelf node present...\r\n";
                else if (buff[1] == 5) textInfo += "Error: Sync mode active...\r\n";
                else if (buff[1] == 1)
                {
                    textInfo += string.Format("Bound with Node {0:d2}...\r\n", node);
                    textInfo += "              Please unplug USB and Power off --> Power On to register the GFT!!!\r\n";
                    //if (node >= 1 && node <= NUM_NODES)
                    //{
                    //    ResetNodeControls(node);
                    //}

                    //////////////////////////////////////////////////////////////////////////              // 2014.10.31           
                    bind_with_unbind = false;                                                               // 2014.10.31           
                    bind_with_wireless_on = false;                                                          // 2014.10.31
                    BindingProcess((byte)node);                                                             // 2014.10.31
                    //////////////////////////////////////////////////////////////////////////              // 2014.10.31           

                    //this.BeginInvoke((MethodInvoker)delegate { m_button_NodeInfo_Click(this, null); });     // Adde by Jason Chen, 20140305                    
                }
                else
                    textInfo += "???...\r\n";

                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo + "\r\n");
                //wirelessViewer.DebugDataBoxDisplay(textInfo + "\r\n");
            }
            else if (buff[0] == MSG_HUB_TO_HOST_UNBIND_MODE)
            {
                KillTimer(this.Handle, 0x100);
                node = buff[1];
                string textInfo = DateTime.UtcNow.ToLongTimeString() + " - UnBind - ";


                if (buff[1] == 0xff) textInfo += "Error: Nok But...\r\n";
                //else if (buff[1] == 2) textInfo += "Error: Already in Bind mode...\r\n";
                //else if (buff[1] == 0) textInfo += "Ok: Entering Bind mode...\r\n";
                //else if (buff[1] == 3) textInfo += "Error: Upload mode active...\r\n";
                //else if (buff[1] == 4) textInfo += "Error: Shelf node present...\r\n";
                //else if (buff[1] == 5) textInfo += "Error: Sync mode active...\r\n";
                else /*if (buff[1] == 1)*/
                {
                    textInfo += string.Format("Unbound with Node {0:d2}...\r\n", node);
                    if (node >= 1 && node <= NUM_NODES)
                    {
                        //ResetNodeControls(node);
                        NodeProfile[node - 1].GID = null;
                    }
#if false
                    int rowCount = gftWirelessView.Rows.Count;
                    for (int index = 0; index < rowCount; index++)
                    {
                        gftWirelessView.Rows.RemoveAt(0);
                    }
#endif
                    if (lpHub != null)
                    {
#if false
                        lpHub.lpDevices.Clear(); // 2014.10.01
                        lpHub.dgvLpPopulateView(); // 2014.10.01
#else
                        int row = getRowNum(gft_node);                           // 2014.10.30
                        //wirelessViewer.dataGridView.FirstDisplayedCell = null; // 2014.10.30
                        //wirelessViewer.dataGridView.ClearSelection();          // 2014.10.30
                        if(row >= 0)
                            wirelessViewer.dataGridView.Rows.RemoveAt(row);
#endif
                    }
                    //lpHub = new WirelessGFTViewer.LP_HUB(wirelessViewer.dataGridView);//, imageList1);
                    //wirelessViewer.setHub(ref lpHub);
                    //wirelessViewer.setProfile(ref NodeProfile);

#if true
                    //DataGridViewRow cur_row = gftWirelessView.CurrentRow;
                    string m_NodeID = node.ToString();

                    //string queryInsert = "DELETE FROM gftWireless WHERE nodeID = @nodeID";
                    string queryInsert = "DELETE FROM gftWireless WHERE (nodeID = @nodeID) and (MID = @dongle_mid OR MID = '' OR MID = '0000000000')";   // changed, 2014.11.11

                    //if (mycon.State != ConnectionState.Open) return;

                    using (SqlCeConnection mycon = new SqlCeConnection(conString))
                    {
                        try
                        {
                            mycon.Open();
                            using (SqlCeCommand com = new SqlCeCommand(queryInsert, mycon))
                            {
                                com.Parameters.AddWithValue("@nodeID", node);
                                com.Parameters.AddWithValue("@dongle_mid", dongle_mid);      // 2014.11.11

                                com.ExecuteNonQuery();
                                getGftWirelessAll();
                            }
                        }
                        catch (Exception er)
                        {
                            //MessageBox.Show(er.Message);
                        }
                        try
                        {
                            mycon.Close();
                        }
                        catch (Exception er)
                        {
                            throw er;
                        }
                    }
#endif

                    //this.BeginInvoke((MethodInvoker)delegate { m_button_NodeInfo_Click(this, null); });     // Adde by Jason Chen, 20140305
                                                                                                              // removed, 2014.10.30 

                }
                //else
                //    textInfo += "???...\r\n";
                //if (TooltabPage16Exist)
                //    m_Edit_Immediate.AppendText(textInfo + "\r\n");
                //wirelessViewer.DebugDataBoxDisplay(textInfo + "\r\n");
            }
        }


        private string getFirstBeforeZero(char[] token, string response)
        {
            foreach (string s in response.Split(token, StringSplitOptions.RemoveEmptyEntries))
            {
                if (s.Length != 0)
                    return s;//.Substring(token.Length);
            }
            return null;
        }

#if false
        private void UpdateNodeControlLayout(int NodeID)
        {
           // if (tabControl4.SelectedIndex != 1)// removed by Jared Stock, 2014.06.25
            //    return;// removed by Jared Stock, 2014.06.25

            if(controlPanel.Contains(NodeControl[NodeID-1]))
                return;

            controlPanel.Controls.Clear();

            controlPanel.Controls.Add(NodeControl[NodeID - 1]);
            NodeControl[NodeID - 1].Dock = DockStyle.Fill;
        }
#endif

#if false
        private void ResetNodeControls(int node)
        {
            NodeControl[node - 1].PlayerNumber = 0;
            NodeControl[node - 1].UserName = "";
            NodeControl[node - 1].m_PWR = false;
            NodeControl[node - 1].m_Hit = false;
            NodeControl[node - 1].m_Usb = false;
            NodeControl[node - 1].m_Imm = false;
            NodeControl[node - 1].m_Tmp = 0;
            NodeControl[node - 1].m_Upld_Count = 0;
            //NodeControl[node - 1].m_Upld_Status = "--";

            m_bSeqNo[node] = 0xFF;

            m_wHitCount[node - 1] = 0;
            m_dwUploadCount[node - 1] = 0;
            m_bHitSeqNo[node - 1] = 0;
            //m_cName[node - 1] = string.Format("N-{0:d}", node);
        }
#endif
        #endregion
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32.SafeHandles;

namespace WirelessGFTViewer
{
    public partial class RxLog : Form
    {
        public RxLog()
        {
            InitializeComponent();
        }

        public bool isCapture()
        {
            return this.chkCapture.Checked;
        }
        public bool isCapture(int node, int selnode)
        {
            if (!this.chkCapture.Checked) return false;
            if (this.chkSelDevice.Checked)
            {
                if (node == selnode) return true;
                else return false;
            }
            else
            {
                return true; // capture all devices
            }
        }
        public bool isCaptureType(int node, int selnode)
        {
            if (!this.chkPacketType.Checked) return false;
            if (this.chkSelDevice.Checked)
            {
                if (node == selnode) return true;
                else return false;
            }
            else
            {
                return true; // capture all devices
            }
        }

        public bool isCaptureBackChannel(int node, int selnode)
        {
            if (!this.chkBackChannel.Checked) return false;
            if (this.chkSelDevice.Checked)
            {
                if (node == selnode) return true;
                else return false;
            }
            else
            {
                return true; // capture all devices
            }
        }
        public void logText(string text)
        {
            if(!this.IsDisposed)                                                        // Added by Jason Chen, 2017.03.22
                this.txtPacketLog.AppendText("\r\n" + text);
        }

        public void logText2(string text)
        {
            if (!this.IsDisposed)                                                        // Added by Jason Chen, 2017.03.22
                this.txtPacketLog.AppendText(text);
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            this.txtPacketLog.Clear();

        }
    }
}

﻿using ArtaFlexHidWin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Globalization;
using System.Linq;
//using System.Data.SqlClient;
//using System.Data;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace gForceTrackerWireless
{
    public enum wirelessGftMsg
    {
        MSG_DOWNLOAD_INTERRUPTED
    }
    public class gftMsgArgs : EventArgs
    {

        private wirelessGftMsg msg;
        private int msgInt = 0;
        private string msgString = "";
        private string msgStringTitle = "";

        public gftMsgArgs(wirelessGftMsg m, string mString = "", string mStringTitle = "")
        {
            this.msg = m;
            this.msgString = mString;
            this.msgStringTitle = mStringTitle;
        }
        public gftMsgArgs(wirelessGftMsg m, int mInt = 0)
        {
            this.msg = m;
            this.msgInt = mInt;
        }
        public wirelessGftMsg getMsg
        {
            get { return this.msg; }
        }
        public int getInt
        {
            get { return this.msgInt; }
        }
        public string getString
        {
            get { return this.msgString; }
        }
    }

    public partial class Form1 : UsbAwareForm
    {

        static DateTime lastRefresh = DateTime.MinValue;
        public delegate void gftMessageHandler(object sender, gftMsgArgs e);
        public event gftMessageHandler gftMsgEvent;
        public delegate void ImpactPacketProcess(procArgs args);

        public DateTime lastRebootSentAt = DateTime.MinValue;

        //Impacts_MouseMove(sender, e, dataGridView3, chartRawLinW, chartRawGyroW, pictureBoxHeadW, "EventsWireless");
        private ushort CalcCRC16(ref byte[] data_p, ushort length, ushort crc_prev, bool cFlag)
        {
            ushort temp = 0;
            ushort crc = crc_prev;// 0xFFFF;
            ushort POLY = 0x8408;
            //byte[] bytes = GetBytesFromHexString(strInput);
            for (int j = 0; j < length; j++)
            {
                crc = (ushort)(crc ^ data_p[j]);
                for (int i = 0; i < 8; i++)
                {
                    if ((crc & 0x0001) == 1)
                        crc = (ushort)((crc >> 1) ^ POLY);//0x1021);
                    else
                        crc >>= 1;
                }
            }
            if (cFlag)
            {
                crc = (ushort)~crc;
                temp = crc;
                crc = (ushort)((crc << 8) | (temp >> 8 & 0xFF));
            }
            return crc;
        }

        private byte CalcCRC8(ref byte[] data_p, byte length, byte preCRC)                       // Added by Jason Chen, 2016.03.29
        {

            byte i, j;
            byte data = preCRC;

            for (j = 0; j < length; j++)
            {
                data = (byte)(data ^ data_p[j]);
                for (i = 0; i < 8; i++)
                {
                    if ((data & 0x80) != 0)
                    {
                        data <<= 1;
                        data ^= 0x07;        // Poly = 0x07;
                    }
                    else
                    {
                        data <<= 1;
                    }
                }
            }

            return data;
        }

        public bool raiseGftMsg(object sender, gftMsgArgs e)
        {
            if (gftMsgEvent != null)
            {
                gftMsgEvent(sender, e);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Impacts_MouseToFirst(Object sender,
                               DataGridView dataGridView2, Chart chartReportLin, Chart chartReportGyro,
                               PictureBox pictureBoxHead, string eventsTable, int impact = 0, Chart chartViewer = null)
        {
            if (chartViewer == null) chartViewer = this.chartViewer;
           // Chart chart1 = (Chart)sender;
            // Call HitTest
            //HitTestResult result = chart1.HitTest(e.X, e.Y);

            //var series = chart2.Series.Add("X");
            // Reset Data Point Attributes

           // if (!(result.ChartElementType == ChartElementType.DataPoint))
           //     return;


            foreach (DataPoint point in chartViewer.Series[1].Points)
            {
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 2;
            }
            foreach (DataPoint point in chartViewer.Series[3].Points)
            {
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 2;
            }

            if (impact == 0)
            {
                // No Impact specified, go to the last selected one if set
                int i = 0;
                foreach(IMPACT_DATA iData in Args[currentNodeID-1].recvFIFO) {
                    if (iData.eid == lpHub.lpDevices[currentNodeID - 1].selectedImpact)
                    {
                        impact = i;
                    }
                    i++;
                }
            }

            // If the mouse if over a data point
            //if (result.ChartElementType == ChartElementType.DataPoint)
            {
                //UpdateNodeControlLayout(currentNodeID);

                int lineStart;
                // Find selected data point

                if (chartViewer.Series[1].Points.Count() > impact)
                {
                    // We now have "spacer" impacts which need to be skipped..

                    int e = 0;
                    int i = 0;
                    for (i = 0; i < timeline.Count; i++)
                    {
                        if (timeline[i].eid > 0)
                            e++;
                        if (e > impact) break;
                    }
                    DataPoint point = chartViewer.Series[1].Points[i];



                    // Change the appearance of the data point
                    point.BackSecondaryColor = Color.White;
                    point.BackHatchStyle = ChartHatchStyle.Percent25;
                    point.BorderWidth = 2;
                    DataPoint pointR = chartViewer.Series[3].Points[i];
                    // Change the appearance of the data point
                    pointR.BackSecondaryColor = Color.White;
                    pointR.BackHatchStyle = ChartHatchStyle.Percent25;
                    pointR.BorderWidth = 2;
                    //lblChartTime.Text = point.AxisLabel.ToString();
                    lineStart = impact;// (int)point.XValue;

                    if (List_or_Table)                // Added by Jason Chen, 2014.01.10
                        //#if USE_OF_LIST
                        DrawImpactChartByList(currentNodeID, lineStart, chartReportLin, chartReportGyro);
                    //else
                    //#else
                    //DrawImpactChart(lineStart, chartReportLin, chartReportGyro, eventsTable);
                    //#endif
                    if (dataGridView2.RowCount > lineStart)
                    {
                        dataGridView2.CurrentCell = dataGridView2[2, lineStart];
                        dataGridView2.Rows[lineStart].Selected = true;
                    }

                    //drawHitArrow(pictureBoxHead);
                    drawHitArrow(pictureBoxHead, timeline[i], Args[currentNodeID - 1].recvFIFO);

                    //toolStripStatusLabelGFT.Text = "Node " + currentNodeID.ToString("d2");
                    //toolStripStatusLabelGFT.BackColor = point.Color;
                    //SetTimer(this.Handle, 0x900+currentNodeID, 500, null);
                }

            }//over a data point
        }//end mouse move function

#if true //USE_OF_LIST
        public class IMPACT_DATA_TABLE
        {

            private Int32 EID;
            private Int32 SID;
            private DateTime Time;
            private Double LinR;
            private Double Hic;
            private Double Gsi;
            private Double LinX;
            private Double LinY;
            private Double LinZ;
            private Double RotX;
            private Double RotY;
            private Double RotZ;
            private Double RotR;
            private Double Azimuth;
            private Double Elevation;
            private Double ThresholdG;
            private Double ThresholdR;

            public Int32 eid
            {
                get{return EID;}
                set{EID = value;}
            }

            public Int32 sid
            {
                get { return SID; }
                set { SID = value; }
            }
            
            public DateTime time
            {
                get{ return Time; }
                set{ Time = value;}
            }
            
            public Double linR
            {
                get { return LinR; }
                set { LinR = value; }
            }
            
            public Double hic
            {
                get { return Hic; }
                set { Hic = value; }
            }
            
            public Double gsi
            {
                get { return Gsi; }
                set { Gsi = value; }
            }

            public Double linX
            {
                get { return LinX; }
                set { LinX = value; }
            }
            
            public Double linY
            {
                get { return LinY; }
                set { LinY = value; }
            }
            
            public Double linZ
            {
                get { return LinZ; }
                set { LinZ = value; }
            }

            public Double rotX
            {
                get { return RotX; }
                set { RotX = value; }
            }
            
            public Double rotY
            {
                get { return RotY; }
                set { RotY = value; }
            }

            public Double rotZ
            {
                get { return RotZ; }
                set { RotZ = value; }
            }

            public Double rotR
            {
                get { return RotR; }
                set { RotR = value; }
            }

            public Double azimuth
            {
                get { return Azimuth; }
                set { Azimuth = value; }
            }

            public Double elevation
            {
                get { return Elevation; }
                set { Elevation = value; }
            }
            public Double thresholdR
            {
                get { return ThresholdR; }
                set { ThresholdR = value; }
            }
            public Double thresholdG
            {
                get { return ThresholdG; }
                set { ThresholdG = value; }
            }
        }

        private DataTable ConvertListToDataTable(List<IMPACT_DATA> list)
        {

            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 15;

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            //int k = 0;
            foreach (var idata in list)
            {
                if(gftGMT_Enable)
                table.Rows.Add((idata.eid).ToString("X8"),idata.sid,idata.time.ToLocalTime()/*JasonAdded, 2014.0109*/,idata.linR,idata.hic,idata.gsi,
                               idata.linX,idata.linY,idata.linZ,idata.rotX,idata.rotY,idata.rotZ,
                               idata.rotR,idata.azimuth,idata.elevation);
                else
                    table.Rows.Add((idata.eid).ToString("X8"), idata.sid, idata.time, idata.linR, idata.hic, idata.gsi,
                               idata.linX, idata.linY, idata.linZ, idata.rotX, idata.rotY, idata.rotZ,
                               idata.rotR, idata.azimuth, idata.elevation);
            }

            return table;
        }



        private void dataGetImpactByList(Int32 nodeID, DataGridView dataGridView2)
        {
            //DateTime baseDate,tmpTime;
            //int millisecond;//, i=0;
            try
            {
                dataGridView2.EnableHeadersVisualStyles = false;
                dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                /*                      
                foreach (var myReader in Args[nodeID-1].recvFIFO)
                {
                    baseDate = myReader.time;
                    int millis = baseDate.Millisecond;
                    millisecond = myReader.millisecond;
                    tmpTime = new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, baseDate.Hour, baseDate.Minute, baseDate.Second, millisecond);

                    tmpTime = tmpTime.ToLocalTime();     // For displaying local time

                    Args[nodeID-1].recvFIFO[i].time.Year = tmpTime.Year;

                    //baseDate.Year, baseDate.Month, baseDate.Day, baseDate.Hour, baseDate.Minute, baseDate.Second, millisecond)
                */



               // Args[nodeID - 1].recvFIFO[0].

                // Render data onto the screen
                dataGridView2.DataSource = ConvertListToDataTable(Args[nodeID - 1].recvFIFO);
                dataGridView2.RowsDefaultCellStyle.BackColor = System.Drawing.Color.White;
                dataGridView2.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(228)))), ((int)(((byte)(255)))));
                dataGridView2.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(245)))), ((int)(((byte)(169)))));
                dataGridView2.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));

                dataGridView2.Columns[0].HeaderText = "Impact ID";
                dataGridView2.Columns[0].DefaultCellStyle.Format = "X8";
                dataGridView2.Columns[0].MinimumWidth = 65;
                dataGridView2.Columns[0].Visible = false;

                dataGridView2.Columns[1].HeaderText = "Session ID";
                dataGridView2.Columns[1].MinimumWidth = 65;
                dataGridView2.Columns[1].DefaultCellStyle.Format = "X8";
                dataGridView2.Columns[1].Visible = false;

                dataGridView2.Columns[2].DefaultCellStyle.Format = "dd.MMMM.yyyy HH:mm:ss";
                dataGridView2.Columns[2].HeaderText = "Date / Impact Time";
                dataGridView2.Columns[2].MinimumWidth = 120;

                dataGridView2.Columns[3].HeaderText = "gForce";
                dataGridView2.Columns[3].MinimumWidth = 55;

                dataGridView2.Columns[4].HeaderText = "HIC";
                dataGridView2.Columns[4].MinimumWidth = 40;
                dataGridView2.Columns[4].Visible = false;
                dataGridView2.Columns[5].HeaderText = "GSI";
                dataGridView2.Columns[5].MinimumWidth = 40;
                dataGridView2.Columns[5].Visible = false;
                dataGridView2.Columns[6].HeaderText = "Lin. X";
                dataGridView2.Columns[6].MinimumWidth = 45;
                dataGridView2.Columns[7].HeaderText = "Lin. Y";
                dataGridView2.Columns[7].MinimumWidth = 45;
                dataGridView2.Columns[8].HeaderText = "Lin. Z";
                dataGridView2.Columns[8].MinimumWidth = 45;

                dataGridView2.Columns[9].HeaderText = "Rot. X";
                dataGridView2.Columns[9].MinimumWidth = 45;
                dataGridView2.Columns[10].HeaderText = "Rot. Y";
                dataGridView2.Columns[10].MinimumWidth = 45;
                dataGridView2.Columns[11].HeaderText = "Rot. Z";
                dataGridView2.Columns[11].MinimumWidth = 45;
                dataGridView2.Columns[12].HeaderText = "R. Res.";
                dataGridView2.Columns[6].MinimumWidth = 45;
                dataGridView2.Columns[13].HeaderText = "Azimuth";
                dataGridView2.Columns[6].MinimumWidth = 40;
                dataGridView2.Columns[14].HeaderText = "Elevation";
                dataGridView2.Columns[6].MinimumWidth = 50;

                //dataGridView2.Columns[6].HeaderCell.Style.ForeColor = System.Drawing.Color.LimeGreen;
                //dataGridView2.Columns[7].HeaderCell.Style.ForeColor = System.Drawing.Color.Orange;
                //dataGridView2.Columns[8].HeaderCell.Style.ForeColor = System.Drawing.Color.Red;
                //dataGridView2.Columns[9].HeaderCell.Style.ForeColor = System.Drawing.Color.DarkCyan;

                for (int i = 0; i < dataGridView2.Columns.Count; i++)
                    dataGridView2.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }
        public void CreateYAxis(Chart chart, ChartArea area, Series series, float axisOffset, float labelsSize)
        {
            // Create new chart area for original series
            ChartArea areaSeries = chart.ChartAreas.Add("ChartArea_" + series.Name);
            //areaSeries.AxisY.Maximum = 50000;
            areaSeries.AxisY.Minimum = 0;
            areaSeries.AxisY.Maximum = 10;
            areaSeries.BackColor = Color.Transparent;
            areaSeries.BorderColor = Color.Transparent;
            areaSeries.Position.FromRectangleF(area.Position.ToRectangleF());
            areaSeries.InnerPlotPosition.FromRectangleF(area.InnerPlotPosition.ToRectangleF());
            areaSeries.AxisX.MajorGrid.Enabled = false;
            areaSeries.AxisX.MajorTickMark.Enabled = false;
            areaSeries.AxisX.LabelStyle.Enabled = false;
            areaSeries.AxisY.MajorGrid.Enabled = false;
            areaSeries.AxisY.MajorTickMark.Enabled = false;
            areaSeries.AxisY.LabelStyle.Enabled = false;
            areaSeries.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;


            series.ChartArea = areaSeries.Name;

            // Create new chart area for axis
            ChartArea areaAxis = chart.ChartAreas.Add("AxisY_" + series.ChartArea);
            areaAxis.BackColor = Color.Transparent;
            areaAxis.BorderColor = Color.Transparent;
            areaAxis.Position.FromRectangleF(chart.ChartAreas[series.ChartArea].Position.ToRectangleF());
            areaAxis.InnerPlotPosition.FromRectangleF(chart.ChartAreas[series.ChartArea].InnerPlotPosition.ToRectangleF());

            // Create a copy of specified series
            Series seriesCopy = chart.Series.Add(series.Name + "_Copy");
            seriesCopy.ChartType = series.ChartType;
            foreach (DataPoint point in series.Points)
            {
                seriesCopy.Points.AddXY(point.XValue, point.YValues[0]);
            }

            // Hide copied series
            seriesCopy.IsVisibleInLegend = false;
            seriesCopy.Color = Color.Transparent;
            seriesCopy.BorderColor = Color.Transparent;
            seriesCopy.ChartArea = areaAxis.Name;

            // Disable drid lines & tickmarks
            areaAxis.AxisY.Title = series.Name;
            areaAxis.AxisY.TitleFont = chart.ChartAreas.First().AxisY.TitleFont;
            areaAxis.AxisY.TitleForeColor = series.Color;
            series.Label = "";
            areaAxis.AxisX.LineWidth = 0;
            areaAxis.AxisX.MajorGrid.Enabled = false;
            areaAxis.AxisX.MajorTickMark.Enabled = false;
            areaAxis.AxisX.LabelStyle.Enabled = false;
            areaAxis.AxisY.MajorGrid.Enabled = false;
            areaAxis.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;
            areaAxis.AxisY.LabelStyle.Font = area.AxisY.LabelStyle.Font;

            // Adjust area position
            areaAxis.Position.X -= axisOffset;
            areaAxis.InnerPlotPosition.X += labelsSize;

        }
        void drawRPEChartByList(Int32 nodeID, Chart chart1)
        {
            Int32 diff = 0;
            Double cumulativeRPE = 0;
            double cumulativePL = 0;
            UInt32 lastPL = 0;
            DateTime lastTime = DateTime.MinValue;
            double rpe = 0;
            chart1.Series.Clear();
            chart1.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Minutes;
            int epMax = 0;
            int borderWidth = 1;
            try
            {
                //chart1.ChartAreas.First().AxisY2.Enabled = AxisEnabled.True;
                var series = chart1.Series.Add("");
                chart1.ChartAreas[0].AxisX.MinorGrid.LineWidth = 0;
                series.Color = System.Drawing.Color.FromArgb(255, 124, 181, 236);
                series.BorderWidth = borderWidth;

                series.ChartType = SeriesChartType.Line;
                series.XValueType = ChartValueType.Auto;
                /*var seriesPL = chart1.Series.Add("Player Load");
                seriesPL.Color = System.Drawing.Color.FromArgb(255, 247, 163, 92);
                seriesPL.BorderWidth = 2;
                seriesPL.ChartType = SeriesChartType.Line;
                seriesPL.XValueType = ChartValueType.Auto;
                seriesPL.LegendText = "Player Load";*/
                var seriesEP = chart1.Series.Add("Explosive Power");
                seriesEP.Color = System.Drawing.Color.FromArgb(255, 247, 163, 92);
                seriesEP.BorderWidth = borderWidth;
                seriesEP.ChartType = SeriesChartType.Line;
                seriesEP.XValueType = ChartValueType.Auto;
                seriesEP.YValueType = ChartValueType.Auto;
                seriesEP.LegendText = "Explosive Power";

                // Get impact timeline so we can synchronize the charts
                timeline = getTimeline(nodeID);
                //timeline.Sort((x, y) => x.time.CompareTo(y.time));
                
                if (timeline.Count() < 40)
                {
                    series["PixelPointWidth"] = "1";
                    seriesEP["PixelPointWidth"] = "1";
                }
                else
                {
                    borderWidth = 0;
                }

               /* var seriesG = chart1.Series.Add("gForce");
                seriesG.Color = System.Drawing.Color.FromArgb(255, 247, 163, 92);
                seriesG.Label = "gForce";
                seriesG.BorderWidth = 2;
                //seriesG.YAxisType = AxisType.Secondary;
                var seriesR = chart1.Series.Add("Rotation");
                seriesR.Color = System.Drawing.Color.FromArgb(255, 128, 133, 233);
                seriesR.Label = "Rotation";
                seriesR.BorderWidth = 2;
                seriesG.ChartType = SeriesChartType.Line;
                seriesG.XValueType = ChartValueType.Auto;
                seriesG.LegendText = "gForce";
                seriesR.LegendText = "Rotation";
                seriesR.ChartType = SeriesChartType.Line;
                seriesR.XValueType = ChartValueType.Auto;*/
                List<RPE_DATA> rpeData = new List<RPE_DATA>(lpHub.lpDevices[nodeID -1].rpeList);
                int count = 0;
                int rCount = 0;
                RPE_DATA rdata;
                DateTime lastMinute = DateTime.MinValue;
                DateTime thisMinute;
                DateTime rpeMinute;
                bool repeatMinute;
                foreach(var myReader in timeline)
                //foreach (RPE_DATA rdata in rpeData)
                {
                    thisMinute = myReader.time.AddSeconds(-1 * myReader.time.Second).AddTicks(-1 * myReader.time.Ticks); // flatten the datetime to the minute
                    if (thisMinute == lastMinute)
                    {
                        // This isn't the first timeline entry for this minute.
                        repeatMinute = true;

                    }
                    else
                    {
                        repeatMinute = false;
                    }
                    lastMinute = thisMinute;
                    int r = 0; // define r in this scope so we can still access it after the loop
                    for (r = 0; r < 6; r++) // There should be 6 RPE reports per minute, the timeline is 1/minute + extra when there are multiple impacts in a minute
                    {
                        rdata = rpeData[rCount + r];
                        if (lastTime < DateTime.UtcNow.AddDays(-2))
                        {
                            lastTime = rdata.report_time;
                        }
                        else
                        {
                            rpeMinute = rdata.report_time.AddSeconds(-1 * rdata.report_time.Second).AddTicks(-1 * rdata.report_time.Ticks); // flatten the datetime to the minute
                            if (rpeMinute != thisMinute)
                            {
                                r--; // tick r down and break.
                                break;
                            }
                            System.Diagnostics.Debug.WriteLine("RPE CHART: Y Value: " + (rdata.player_load).ToString());
                            DateTime tmpTime = rdata.report_time;
                            if (gftGMT_Enable)
                                tmpTime = tmpTime.ToLocalTime();     // For displaying local time

                            /*seriesPL.Points.AddXY(count, cumulativePL);*/
                            if (rdata.explosive_power > epMax) epMax = (int)(rdata.explosive_power);
                            if (!repeatMinute)
                            {
                                cumulativePL += Math.Round((double)rdata.player_load / 60, 2);
                                seriesEP.Points.AddXY(count, Math.Round((double)rdata.explosive_power / 1000, 1));//datetime.ToString("HH:mm")); //
                                series.Points.AddXY(count, rdata.rpe); //(rdata.rpe - lastPL) / (double)((rdata.report_time - lastTime).Seconds / 60.00)
                            }
                            else
                            {
                                // this minute is being repeated
                                seriesEP.Points.AddXY(count, 0);
                                series.Points.AddXY(count, 0);
                            }
                            series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");

                            //series.Points[count].AxisLabel = "hey";

                            // Do we have a matching R or G entry to show?
                            /*double highestG = 0;
                            double highestR = 0;
                            foreach(gForceTrackerWireless.Form1.IMPACT_DATA iData in Args[nodeID - 1].recvFIFO) {
                                if (iData.time >= rdata.report_time.AddSeconds(-10) && iData.time < rdata.report_time)
                                {
                                    if (iData.linR > highestG) highestG = iData.linR;
                                    if (iData.rotR > highestR) highestR = iData.rotR;
                                }
                                if (iData.time > rdata.report_time.AddSeconds(10)) break;
                            }
                            seriesG.Points.AddXY(count, highestG);
                            seriesR.Points.AddXY(count, highestR);*/
                            //seriesG.Points[count].AxisLabel = rdata.report_time.ToString("HH:mm");

                            count++;
                        }
                    }
                    if (!repeatMinute) rCount += r;
                    
                }
                while (chart1.Series.Count > 2)
                {
                    chart1.Series.RemoveAt(2);
                }
                while (chart1.ChartAreas.Count > 1)
                {
                    chart1.ChartAreas.RemoveAt(1);
                }
                chart1.ChartAreas.First().Position = new ElementPosition(15, 10, 90, 85);
                chart1.ChartAreas.First().InnerPlotPosition = new ElementPosition(2, 0, 90, 90);
                /*CreateYAxis(chart1, chart1.ChartAreas.First(), seriesPL, 13, 8);*/
                CreateYAxis(chart1, chart1.ChartAreas.First(), seriesEP, 13, 8);
                //chart1.ChartAreas.Last().AxisY.Minimum = 0;
                //chart1.ChartAreas.Last().AxisY.Maximum = 10;
                //chart1.ChartAreas[2].AxisY.Maximum = 50000;
                //chart1.ChartAreas.Last().AxisY.LabelStyle.Format = "mm:ss";
                /*CreateYAxis(chart1, chart1.ChartAreas.First(), seriesG, 13, 8);
                CreateYAxis(chart1, chart1.ChartAreas.First(), seriesR, 22, 8);*/

            //    chart1.ChartAreas.Last().AxisY.CustomLabels.Clear();
                int max = 10;// (int)chart1.ChartAreas.Last().AxisY.Maximum;
                chart1.ChartAreas.Last().AxisY.Maximum = max;
                chart1.ChartAreas.Last().AxisY.Minimum = 0;
                int interval = 2;// (int)Math.Round((decimal)epMax / 5);// (int)chart1.ChartAreas.Last().AxisY.Interval;
                if (interval < 1) interval = 1;
                try
                {
                    for (int i = 0; i <= max; i += interval)
                    {
                        CustomLabel cl = new CustomLabel();
                        cl.FromPosition = i - interval / 2;
                        cl.ToPosition = i + interval / 2;
                        cl.Text = i + " s";
                        //TimeSpan time = TimeSpan.FromMilliseconds(i);
                        //cl.Text = time.ToString(@"hh\:mm\:ss");
                        chart1.ChartAreas.Last().AxisY.CustomLabels.Add(cl);
                    }
                }
                catch (Exception e)
                {
                }

                chart1.ChartAreas[0].AxisY.Minimum = 0;
                chart1.ChartAreas[0].AxisY.Maximum = 10;
                //this.lblPerfPlayerLoad.Text = Math.Round(cumulativePL, 2).ToString();
            }
            catch (Exception e)
            {
            }
        }

        void drawEPChartByList(Int32 nodeID, Chart chart1)
        {
            DateTime lastTime = DateTime.MinValue;
            DateTime firstTime = DateTime.MinValue;
            chart1.Series.Clear();
            int borderWidth = 1;
            //chart1.Annotations.Clear();
            try
            {
                chart1.ChartAreas.FirstOrDefault().CursorX.IsUserSelectionEnabled = true;
                chart1.ChartAreas.FirstOrDefault().AxisX.ScaleView.Zoomable = true;
                chart1.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
                int highestEP = 0;
                var series = chart1.Series.Add("");

                // Get impact timeline so we can synchronize the charts
                timeline = getTimeline(nodeID);
                //timeline.Sort((x, y) => x.time.CompareTo(y.time));

                if (timeline.Count() < 40)
                {
                    series["PixelPointWidth"] = "1";
                }
                else
                {
                    borderWidth = 0;
                }
                series.Color = System.Drawing.Color.FromArgb(255, 124, 181, 236);
                series.BorderWidth = borderWidth;

                series.ChartType = SeriesChartType.Column;
                series.XValueType = ChartValueType.Auto;
                series.AxisLabel = "Explosive Power";
                /*List<RPE_DATA> rpeData = new List<RPE_DATA>(lpHub.lpDevices[nodeID - 1].rpeList);

                List<RPE_DATA> rpeTimeline = new List<RPE_DATA>();
                // Add "Missing" Entries
                TimeSpan deltaT;
                foreach (RPE_DATA rdata2 in rpeData)
                {
                    if (firstTime < DateTime.UtcNow.AddDays(-2))
                    {
                        firstTime = rdata2.report_time;
                    }
                    if (lastTime > DateTime.UtcNow.AddDays(-2))
                    {
                        deltaT = rdata2.report_time - lastTime;
                        for (int i = 1; i < Math.Round(deltaT.TotalSeconds); i+=10)
                        {
                            RPE_DATA nData = new RPE_DATA();
                            nData.explosive_count = 0;
                            nData.explosive_count_raw = 0;
                            nData.explosive_power = 0;
                            nData.explosive_power_raw = 0;
                            nData.player_load = 0;
                            nData.player_load_raw = 0;
                            nData.rpe = 0;
                            nData.report_time = lastTime.AddSeconds(i);
                            rpeTimeline.Add(nData);
                        }
                    }
                    rpeTimeline.Add(rdata2);
                    lastTime = rdata2.report_time;
                }
                if (lastTime < timeline.Last().time)
                {
                    deltaT = timeline.Last().time - rpeTimeline.Last().report_time;
                    for (int i = 1; i < Math.Round(deltaT.TotalSeconds); i += 10)
                    {
                        RPE_DATA nData = new RPE_DATA();
                        nData.explosive_count = 0;
                        nData.explosive_count_raw = 0;
                        nData.explosive_power = 0;
                        nData.explosive_power_raw = 0;
                        nData.player_load = 0;
                        nData.player_load_raw = 0;
                        nData.rpe = 0;
                        nData.report_time = lastTime.AddSeconds(i);
                        rpeTimeline.Add(nData);
                    }
                }*/
                List<RPE_DATA> rpeTimeline = getRPETimeline(nodeID, timeline);
                /*if (firstTime > DateTime.UtcNow.AddDays(-2) && Args[nodeID - 1].firstOn > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = firstTime - Args[nodeID - 1].firstOn;
                    for (int i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        RPE_DATA nData = new RPE_DATA();
                        nData.explosive_count = 0;
                        nData.explosive_count_raw = 0;
                        nData.explosive_power = 0;
                        nData.explosive_power_raw = 0;
                        nData.player_load = 0;
                        nData.player_load_raw = 0;
                        nData.rpe = 0;
                        nData.report_time = Args[nodeID - 1].firstOn.AddMinutes(i);
                        rpeTimeline.Add(nData);
                    }
                }
                if (lastTime > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = DateTime.UtcNow - lastTime;
                    int i = 0;
                    for (i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        RPE_DATA nData = new RPE_DATA();
                        nData.explosive_count = 0;
                        nData.explosive_count_raw = 0;
                        nData.explosive_power = 0;
                        nData.explosive_power_raw = 0;
                        nData.player_load = 0;
                        nData.player_load_raw = 0;
                        nData.rpe = 0;
                        nData.report_time = lastTime.AddMinutes(i);
                        rpeTimeline.Add(nData);
                    }
                    if (rpeTimeline.Count < 60)
                    {
                        int deltaI = 60 - rpeTimeline.Count;
                        for (int i2 = 0; i2 < deltaI; i2++)
                        {
                            RPE_DATA nData = new RPE_DATA();
                            nData.explosive_count = 0;
                            nData.explosive_count_raw = 0;
                            nData.explosive_power = 0;
                            nData.explosive_power_raw = 0;
                            nData.player_load = 0;
                            nData.player_load_raw = 0;
                            nData.rpe = 0;
                            nData.report_time = lastTime.AddMinutes(i + i2);
                            rpeTimeline.Add(nData);
                        }
                    }
                }*/

                rpeTimeline.Sort((x, y) => x.report_time.CompareTo(y.report_time));
                int count = 0;
                int rCount = 0;
                RPE_DATA rdata;
                DateTime lastMinute = DateTime.MinValue;
                DateTime thisMinute;
                DateTime rpeMinute;
                bool repeatMinute;
                foreach(var myReader in timeline)
                //foreach (RPE_DATA rdata in rpeData)
                {
                    thisMinute = myReader.time.AddSeconds(-1 * myReader.time.Second).AddMilliseconds(-1 * myReader.time.Millisecond); // flatten the datetime to the minute
                    System.Diagnostics.Debug.WriteLine(thisMinute.ToString("HH:mm"));
                    if (thisMinute.ToString("HH:mm") == lastMinute.ToString("HH:mm"))
                    {
                        // This isn't the first timeline entry for this minute.
                        repeatMinute = true;

                    }
                    else
                    {
                        repeatMinute = false;
                    }
                    lastMinute = thisMinute;
                    int r = 0; // define r in this scope so we can still access it after the loop
                    for (r = 0; r < 6; r++) // There should be 6 RPE reports per minute, the timeline is 1/minute + extra when there are multiple impacts in a minute
                    {

                        rdata = rpeTimeline.Last();
                        rpeMinute = rdata.report_time.AddSeconds(-1 * rdata.report_time.Second).AddMilliseconds(-1 * rdata.report_time.Millisecond);

                        while (rCount < rpeTimeline.Count) //
                        {
                            if (rpeTimeline.Count > rCount + r)
                            {
                                rdata = rpeTimeline[rCount + r];
                                rpeMinute = rdata.report_time.AddSeconds(-1 * rdata.report_time.Second).AddMilliseconds(-1 * rdata.report_time.Millisecond); // flatten the datetime to the minute
                                if (rpeMinute.ToString("HH:mm") == thisMinute.ToString("HH:mm") || rpeMinute >= thisMinute) break;
                            }
                            rCount++;
                        }
                    //foreach (RPE_DATA rdata in rpeTimeline)
                   // {
                        if (lastTime < DateTime.UtcNow.AddDays(-2))
                        {
                            lastTime = rdata.report_time;
                        }
                        else
                        {

                            DateTime tmpTime = rdata.report_time;
                            if (gftGMT_Enable)
                                tmpTime = tmpTime.ToLocalTime();     // For displaying local time
                            if (rpeMinute.ToString("HH:mm") != thisMinute.ToString("HH:mm"))
                            {
                                //if (rpeMinute < thisMinute) break; // we are behind..
                                for (int ri = 0; ri < 6 - r; ri++)
                                {
                                    series.Points.AddXY(count, 0);
                                    series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                    count++;
                                }
                                r--; // tick r down and break.
                                break;
                            }
                            if (rdata.explosive_count > 0 && !repeatMinute)
                            {
                                int showCnt = 0;
                                //showCnt = (int)Math.Ceiling((double)rdata.explosive_count / 5);
                                showCnt = (int)rdata.explosive_count;
                                //for (int i = 0; i < showCnt; i++)
                                {
                                    series.Points.AddXY(count, Math.Round((double)rdata.explosive_power / 100, 3));
                                    if (rdata.explosive_power > highestEP) highestEP = (int)rdata.explosive_power;
                                    series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                    series.Points[count].ToolTip = "Explosive Count: " + rdata.explosive_count.ToString() + "\nExplosive Power: " + Math.Round((double)rdata.explosive_power / 100, 1) + "s\nExertion: " + rdata.rpe + "\nPlayer Load: " + Math.Round((double)rdata.player_load / 60, 1);
                                    /*TextAnnotation myText = new TextAnnotation();
                                    myText.Name = count.ToString();
                                    myText.Text = rdata.explosive_count.ToString();
                                    myText.AxisX = chart1.ChartAreas[0].AxisX;
                                    myText.AxisY = chart1.ChartAreas[0].AxisY;
                                    myText.AnchorDataPoint = series.Points[count];
                                    myText.AnchorAlignment = ContentAlignment.TopCenter;
                                    myText.AnchorOffsetX = 0;
                                    myText.AnchorOffsetY = -1;
                                    myText.AllowAnchorMoving = false;
                                    myText.BringToFront();
                                    chart1.Annotations.Add(myText);*/
                                    count++;
                                }
                            }
                            else if (repeatMinute)
                            {
                                series.Points.AddXY(count, 0);
                                series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                count++;
                            }
                            else
                            {
                                series.Points.AddXY(count, 0);
                                series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                count++;
                            }
                        
                        }

                    }
                    rCount += r + 1;
                }
                chart1.ChartAreas[0].AxisX.Interval = Math.Ceiling((double)series.Points.Count() / 10);
                chart1.ChartAreas.FirstOrDefault().AxisY.Minimum = 0;
                chart1.ChartAreas.FirstOrDefault().AxisY.Maximum = (double)Math.Round((double)highestEP / 100, 1) + 0.2;
                chart1.ChartAreas.FirstOrDefault().AxisY2.Minimum = chart1.ChartAreas.FirstOrDefault().AxisY.Minimum;
                chart1.ChartAreas.FirstOrDefault().AxisY2.Maximum = chart1.ChartAreas.FirstOrDefault().AxisY.Maximum;
            }
            catch (Exception e)
            {
            }
        }

        void drawPLChartByList(Int32 nodeID, Chart chart1)
        {
            DateTime lastTime = DateTime.MinValue;
            DateTime firstTime = DateTime.MinValue;
            chart1.Series.Clear();
            try
            {
                chart1.ChartAreas.FirstOrDefault().CursorX.IsUserSelectionEnabled = true;
                chart1.ChartAreas.FirstOrDefault().AxisX.ScaleView.Zoomable = true;

                int borderWidth = 1;
                int highestPL = 0;
                chart1.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
                var series = chart1.Series.Add("");
                series.Color = System.Drawing.Color.FromArgb(255, 124, 181, 236);

                // Get impact timeline so we can synchronize the charts
                timeline = getTimeline(nodeID);
                //timeline.Sort((x, y) => x.time.CompareTo(y.time));

                if (timeline.Count() < 40)
                {
                    series["PixelPointWidth"] = "1";
                }
                else
                {
                    borderWidth = 0;
                }
                series.BorderWidth = borderWidth;

                series.ChartType = SeriesChartType.Column;
                series.XValueType = ChartValueType.Auto;
                series.AxisLabel = "Player Load";
                //series.YAxisType = AxisType.Secondary;
                /*List<RPE_DATA> rpeData = new List<RPE_DATA>(lpHub.lpDevices[nodeID - 1].rpeList);

                List<RPE_DATA> rpeTimeline = new List<RPE_DATA>(rpeData);
                // Add "Missing" Entries
                TimeSpan deltaT;
                
                foreach (RPE_DATA rdata2 in rpeData)
                {
                    if (firstTime < DateTime.UtcNow.AddDays(-2))
                    {
                        firstTime = rdata2.report_time;
                    }
                    if (lastTime > DateTime.UtcNow.AddDays(-2))
                    {
                        deltaT = rdata2.report_time - lastTime;
                        for (int i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                        {
                            RPE_DATA nData = new RPE_DATA();
                            nData.explosive_count = 0;
                            nData.explosive_count_raw = 0;
                            nData.explosive_power = 0;
                            nData.explosive_power_raw = 0;
                            nData.player_load = 0;
                            nData.player_load_raw = 0;
                            nData.rpe = 0;
                            nData.report_time = lastTime.AddMinutes(i);
                            rpeTimeline.Add(nData);
                        }
                    }
                    lastTime = rdata2.report_time;
                }*/

                List<RPE_DATA> rpeTimeline = getRPETimeline(nodeID, timeline);
                /*
                if (firstTime > DateTime.UtcNow.AddDays(-2) && Args[nodeID - 1].firstOn > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = firstTime - Args[nodeID - 1].firstOn;
                    for (int i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        RPE_DATA nData = new RPE_DATA();
                        nData.explosive_count = 0;
                        nData.explosive_count_raw = 0;
                        nData.explosive_power = 0;
                        nData.explosive_power_raw = 0;
                        nData.player_load = 0;
                        nData.player_load_raw = 0;
                        nData.rpe = 0;
                        nData.report_time = Args[nodeID - 1].firstOn.AddMinutes(i);
                        rpeTimeline.Add(nData);
                    }
                }
                if (lastTime > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = DateTime.UtcNow - lastTime;
                    int i = 0;
                    for (i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        RPE_DATA nData = new RPE_DATA();
                        nData.explosive_count = 0;
                        nData.explosive_count_raw = 0;
                        nData.explosive_power = 0;
                        nData.explosive_power_raw = 0;
                        nData.player_load = 0;
                        nData.player_load_raw = 0;
                        nData.rpe = 0;
                        nData.report_time = lastTime.AddMinutes(i);
                        rpeTimeline.Add(nData);
                    }
                    if (rpeTimeline.Count < 60)
                    {
                        int deltaI = 60 - rpeTimeline.Count;
                        for (int i2 = 0; i2 < deltaI; i2++)
                        {
                            RPE_DATA nData = new RPE_DATA();
                            nData.explosive_count = 0;
                            nData.explosive_count_raw = 0;
                            nData.explosive_power = 0;
                            nData.explosive_power_raw = 0;
                            nData.player_load = 0;
                            nData.player_load_raw = 0;
                            nData.rpe = 0;
                            nData.report_time = lastTime.AddMinutes(i + i2);
                            rpeTimeline.Add(nData);
                        }
                    }
                }*/

                //rpeTimeline.Sort((x, y) => x.report_time.CompareTo(y.report_time));
                int count = 0;
                int rCount = 0;
                RPE_DATA rdata;
                DateTime lastMinute = DateTime.MinValue;
                DateTime thisMinute;
                DateTime rpeMinute;
                bool repeatMinute;
                foreach (var myReader in timeline)
                //foreach (RPE_DATA rdata in rpeData)
                {
                    thisMinute = myReader.time.AddSeconds(-1 * myReader.time.Second).AddMilliseconds(-1 * myReader.time.Millisecond); // flatten the datetime to the minute
                    if (thisMinute.ToString("HH:mm") == lastMinute.ToString("HH:mm"))
                    {
                        // This isn't the first timeline entry for this minute.
                        repeatMinute = true;

                    }
                    else
                    {
                        repeatMinute = false;
                    }
                    lastMinute = thisMinute;
                    int r = 0; // define r in this scope so we can still access it after the loop
                    for (r = 0; r < 6; r++) // There should be 6 RPE reports per minute, the timeline is 1/minute + extra when there are multiple impacts in a minute
                    {
                        rdata = rpeTimeline.Last();
                        rpeMinute = rdata.report_time.AddSeconds(-1 * rdata.report_time.Second).AddMilliseconds(-1 * rdata.report_time.Millisecond);

                        while (rCount < rpeTimeline.Count) //
                        {
                            if (rpeTimeline.Count > rCount + r)
                            {
                                rdata = rpeTimeline[rCount + r];
                                rpeMinute = rdata.report_time.AddSeconds(-1 * rdata.report_time.Second).AddMilliseconds(-1 * rdata.report_time.Millisecond); // flatten the datetime to the minute
                                if (rpeMinute.ToString("HH:mm") == thisMinute.ToString("HH:mm") || rpeMinute >= thisMinute) break;
                            }
                            rCount++;
                        }
                        //foreach (RPE_DATA rdata in rpeTimeline)
                        //{
                        if (lastTime < DateTime.UtcNow.AddDays(-2))
                        {
                            lastTime = rdata.report_time;
                        }
                        else
                        {
                            DateTime tmpTime = rdata.report_time;
                            if (gftGMT_Enable)
                                tmpTime = tmpTime.ToLocalTime();     // For displaying local time
                            if (rpeMinute.ToString("HH:mm") != thisMinute.ToString("HH:mm"))
                            {
                                //if (rpeMinute < thisMinute) break; // we are behind..
                                for (int ri = 0; ri < 6 - r; ri++)
                                {
                                    series.Points.AddXY(count, 0);
                                    series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                    count++;
                                }
                                r--; // tick r down and break.
                                break;
                            }
                            if (rdata.player_load > 0 && !repeatMinute) //player_load_raw
                            {
                                //int showCnt = 0;
                                //showCnt = (int)Math.Ceiling((double)rdata.explosive_count / 5);
                                //showCnt = (int)rdata.explosive_count;
                                //for (int i = 0; i < showCnt; i++)
                                {
                                    series.Points.AddXY(count, Math.Round((double)rdata.player_load_raw / 60, 3));
                                    if (rdata.player_load_raw > highestPL) highestPL = (int)rdata.player_load_raw;
                                    series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                    series.Points[count].ToolTip = "Player Load: " + Math.Round((double)rdata.player_load_raw / 60, 1) + " (+" + Math.Round((double)rdata.player_load / 60, 1) + ")\nExplosive Power: " + Math.Round((double)rdata.explosive_power / 1000, 1) + "s\nExertion: " + rdata.rpe;
                                    count++;
                                }
                            }
                            else if (!repeatMinute)
                            {
                                series.Points.AddXY(count, 0);
                                series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                count++;
                            }
                            else
                            {
                                series.Points.AddXY(count, 0);
                                series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");
                                count++;
                            }

                        }

                    }
                    rCount += r + 1;
                }
                /*foreach (TextAnnotation ta in chart1.Annotations)
                {
                    ta.Anch
                }*/
                chart1.ChartAreas[0].AxisX.Interval = Math.Ceiling((double)series.Points.Count()/10);
                chart1.ChartAreas.FirstOrDefault().AxisY.Minimum = 0;
                chart1.ChartAreas.FirstOrDefault().AxisY.Maximum = (int)Math.Ceiling((double)highestPL / 60) + 1;
                chart1.ChartAreas.FirstOrDefault().AxisY2.Minimum = chart1.ChartAreas.FirstOrDefault().AxisY.Minimum;
                chart1.ChartAreas.FirstOrDefault().AxisY2.Maximum = chart1.ChartAreas.FirstOrDefault().AxisY.Maximum;

            }
            catch (Exception e)
            {
            }
        }
        List<RPE_DATA> getRPETimeline(int nodeID, List<IMPACT_DATA> timeLine)
        {
            DateTime lastTime = DateTime.MinValue;
            DateTime firstTime = DateTime.MinValue;
            List<RPE_DATA> rpeTimeline = new List<RPE_DATA>();
            try
            {
                List<RPE_DATA> rpeData = new List<RPE_DATA>(lpHub.lpDevices[nodeID - 1].rpeList);
                TimeSpan deltaT;
                foreach (RPE_DATA rdata2 in rpeData)
                {
                    if (firstTime < DateTime.UtcNow.AddDays(-2))
                    {
                        firstTime = rdata2.report_time;
                    }
                    if (lastTime > DateTime.UtcNow.AddDays(-2))
                    {
                        deltaT = rdata2.report_time - lastTime;
                        for (int i = 1; i < Math.Round(deltaT.TotalSeconds); i += 10)
                        {
                            RPE_DATA nData = new RPE_DATA();
                            nData.explosive_count = 0;
                            nData.explosive_count_raw = 0;
                            nData.explosive_power = 0;
                            nData.explosive_power_raw = 0;
                            nData.player_load = 0;
                            nData.player_load_raw = 0;
                            nData.rpe = 0;
                            nData.report_time = lastTime.AddSeconds(i);
                            rpeTimeline.Add(nData);
                        }
                    }
                    rpeTimeline.Add(rdata2);
                    lastTime = rdata2.report_time;
                }
                if (lastTime < timeline.Last().time)
                {
                    deltaT = timeline.Last().time - rpeTimeline.Last().report_time;
                    for (int i = 1; i < Math.Round(deltaT.TotalSeconds); i += 10)
                    {
                        RPE_DATA nData = new RPE_DATA();
                        nData.explosive_count = 0;
                        nData.explosive_count_raw = 0;
                        nData.explosive_power = 0;
                        nData.explosive_power_raw = 0;
                        nData.player_load = 0;
                        nData.player_load_raw = 0;
                        nData.rpe = 0;
                        nData.report_time = lastTime.AddSeconds(i);
                        rpeTimeline.Add(nData);
                    }
                }
                rpeTimeline.Sort((x, y) => x.report_time.CompareTo(y.report_time));
            }
            catch (Exception e)
            {
            }
            return rpeTimeline;
        }
        List<IMPACT_DATA> getTimeline(int nodeID)
        {
            // Returns the timeline to be used on the player view
            // Applies to EC, PL and Fuel Gauge

            // Parse to a timeline
            List<IMPACT_DATA> impData = new List<IMPACT_DATA>(Args[nodeID - 1].recvFIFO);
            try
            {
                // Sort it
                impData.Sort((x, y) => x.millisort.CompareTo(y.millisort));
                //impData.Sort((x, y) => x.time.CompareTo(y.time));
                timeline = new List<IMPACT_DATA>(impData);
                // Add "Missing" Entries
                DateTime lastTime = DateTime.MinValue;
                DateTime firstTime = DateTime.MinValue;
                TimeSpan deltaT;
                foreach (IMPACT_DATA imp in impData)
                {
                    if (firstTime < DateTime.UtcNow.AddDays(-2))
                    {
                        firstTime = imp.time;
                    }
                    if (lastTime > DateTime.UtcNow.AddDays(-2))
                    {
                        deltaT = imp.time - lastTime;
                        for (int i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                        {
                            IMPACT_DATA nData = new IMPACT_DATA();
                            nData.eid = 0;
                            nData.UID = 0;
                            nData.thresholdG = 100;
                            nData.thresholdR = 2000;
                            nData.linR = 0;
                            nData.rotR = 0;
                            nData.time = lastTime.AddMinutes(i);
                            nData.millisecond = 0;
                            nData.millisort = 0;
                            TimeSpan secondsSince2k = nData.time - DateTime.Parse("2000-01-01");
                            nData.millisort = secondsSince2k.TotalSeconds * 1000;
                            timeline.Add(nData);
                        }
                    }
                    lastTime = imp.time;
                }
                if (firstTime > DateTime.UtcNow.AddDays(-2) && Args[nodeID - 1].firstOn > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = firstTime - Args[nodeID - 1].firstOn;
                    for (int i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        IMPACT_DATA nData = new IMPACT_DATA();
                        nData.eid = 0;
                        nData.UID = 0;
                        nData.thresholdG = 100;
                        nData.thresholdR = 2000;
                        nData.linR = 0;
                        nData.rotR = 0;
                        nData.time = Args[nodeID - 1].firstOn.AddMinutes(i);
                        nData.millisecond = 0;
                        TimeSpan secondsSince2k = nData.time - DateTime.Parse("2000-01-01");
                        nData.millisort = secondsSince2k.TotalSeconds * 1000;
                        timeline.Add(nData);
                    }
                }
                if (lastTime > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = DateTime.UtcNow - lastTime;
                    int i = 0;
                    for (i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        IMPACT_DATA nData = new IMPACT_DATA();
                        nData.eid = 0;
                        nData.UID = 0;
                        nData.thresholdG = 100;
                        nData.thresholdR = 2000;
                        nData.linR = 0;
                        nData.rotR = 0;
                        nData.time = lastTime.AddMinutes(i);
                        nData.millisecond = 0;
                        TimeSpan secondsSince2k = nData.time - DateTime.Parse("2000-01-01");
                        nData.millisort = secondsSince2k.TotalSeconds * 1000;
                        timeline.Add(nData);
                    }
                    if (timeline.Count < 60)
                    {
                        int deltaI = 60 - timeline.Count;
                        for (int i2 = 0; i2 < deltaI; i2++)
                        {
                            IMPACT_DATA nData = new IMPACT_DATA();
                            nData.eid = 0;
                            nData.UID = 0;
                            nData.thresholdG = 100;
                            nData.thresholdR = 2000;
                            nData.linR = 0;
                            nData.rotR = 0;
                            nData.time = lastTime.AddMinutes(i + i2);
                            nData.millisecond = 0;
                            TimeSpan secondsSince2k = nData.time - DateTime.Parse("2000-01-01");
                            nData.millisort = secondsSince2k.TotalSeconds * 1000;
                            timeline.Add(nData);
                        }
                    }
                    
                }
                if (timeline.Count == 0 && lpHub.lpDevices[currentNodeID - 1].rpeList.Count > 0)
                {
                    int deltaI = 60 - timeline.Count;
                    int i = 0;
                    lastTime = lpHub.lpDevices[currentNodeID - 1].rpeList[0].report_time;
                    for (int i2 = 0; i2 < deltaI; i2++)
                    {
                        IMPACT_DATA nData = new IMPACT_DATA();
                        nData.eid = 0;
                        nData.UID = 0;
                        nData.thresholdG = 100;
                        nData.thresholdR = 2000;
                        nData.linR = 0;
                        nData.rotR = 0;
                        nData.time = lastTime.AddMinutes(i + i2);
                        nData.millisecond = 0;
                        TimeSpan secondsSince2k = nData.time - DateTime.Parse("2000-01-01");
                        nData.millisort = secondsSince2k.TotalSeconds * 1000;
                        timeline.Add(nData);
                    }
                }
                // Sort it again
                timeline.Sort((x, y) => x.millisort.CompareTo(y.millisort));
            }
            catch (Exception ex)
            {
            }
            return timeline;
        }
        void drawChartByList(Int32 nodeID, int mThreshold, Chart chart1)
        {
            int countOverThreshold = 0;
            //int elements = 100;
            int count = 0;
            double linRes = 0;
            double rotRes = 0;
            DateTime baseDate;
            DateTime tmpTime;
            int millisecond = 0;
            //const string f = "C:\\00\\g-force\\test.csv";
            // creates 100 random X points
            // remove all previous series

            chart1.Series.Clear();

            var wSeries = chart1.Series.Add("");
            
            wSeries.ChartType = SeriesChartType.StackedColumn;
            var series = chart1.Series.Add("");
            series.ChartType = SeriesChartType.StackedColumn;
            series.XValueType = ChartValueType.Auto;

            var wSeriesR = chart1.Series.Add("");
            wSeriesR.ChartType = SeriesChartType.StackedColumn;
            var seriesR = chart1.Series.Add("");
            seriesR.ChartType = SeriesChartType.StackedColumn;
            //chart1.Titles.Add("Shock Peak Point");
            try
            {
                chart1.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
                timeline = getTimeline(nodeID);   
                //timeline.Sort((x, y) => x.time.CompareTo(y.time));
                int borderWidth = 2;
                if (timeline.Count() < 40)
                {
                    wSeries["PixelPointWidth"] = "14";
                    series["PixelPointWidth"] = "14";
                    wSeriesR["PixelPointWidth"] = "14";
                    seriesR["PixelPointWidth"] = "14";
                }
                else
                {
                    borderWidth = 0;
                }

                foreach (var myReader in timeline)
                {

                    linRes = myReader.linR;
                    rotRes = myReader.rotR;
                    baseDate = myReader.time;
                    int millis = baseDate.Millisecond;
                    millisecond = myReader.millisecond;
                    tmpTime = new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, baseDate.Hour, baseDate.Minute, baseDate.Second, millisecond);

                    if (gftGMT_Enable)
                        tmpTime = tmpTime.ToLocalTime();     // For displaying local time

                    var xDate = count;
                    if (linRes == 0 && rotRes == 0)
                    {
                        // Add an "empty" point
                        series.Points.AddXY(xDate, linRes);
                        seriesR.Points.AddXY(xDate, -rotRes / 20);


                        series.Points[count].BorderWidth = 0;
                        seriesR.Points[count].BorderWidth = 0;

                        wSeries.Points.AddXY(xDate, 0);
                        wSeries.Points[count].Color = System.Drawing.Color.White;
                        wSeriesR.Points.AddXY(xDate, -0);
                        wSeriesR.Points[count].Color = System.Drawing.Color.White;


                        wSeries.Points[count].BorderWidth = 0;
                        wSeriesR.Points[count].BorderWidth = 0;
                    }
                    else
                    {

                        // Make a gap near 0
                        wSeries.Points.AddXY(xDate, 2);
                        wSeries.Points[count].Color = System.Drawing.Color.White;
                        if (borderWidth > 0)
                        {
                            wSeries.Points[count].BorderWidth = borderWidth;
                            wSeries.Points[count].BorderColor = System.Drawing.Color.White;
                        }
                        // Make a gap near 0
                        wSeriesR.Points.AddXY(xDate, -2);
                        wSeriesR.Points[count].Color = System.Drawing.Color.White;
                        if (borderWidth > 0)
                        {
                            wSeriesR.Points[count].BorderWidth = borderWidth;
                            wSeriesR.Points[count].BorderColor = System.Drawing.Color.White;
                        }
                        series.Points.AddXY(xDate, linRes);
                        if (linRes >= mThreshold)
                        {
                            series.Points[count].Color = System.Drawing.Color.Red;
                            if (borderWidth > 0)
                            {
                                series.Points[count].BorderWidth = borderWidth;
                                series.Points[count].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);
                            }
                            countOverThreshold++;
                        }
                        else if (linRes >= mThreshold * 0.9)
                        {
                            series.Points[count].Color = System.Drawing.Color.Yellow;
                            if (borderWidth > 0)
                            {
                                series.Points[count].BorderWidth = borderWidth;
                                series.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 215, 225, 0);
                            }

                        }
                        else
                        {
                            series.Points[count].Color = System.Drawing.Color.LimeGreen;
                            if (borderWidth > 0)
                            {
                                series.Points[count].BorderWidth = borderWidth;
                                series.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);
                            }
                        }

                        seriesR.Points.AddXY(xDate, -rotRes / 20);
                        if (rotRes >= 2000)
                        {
                            //seriesR.Points[count].Color = System.Drawing.Color.FromArgb(255, 235, 0, 0); //System.Drawing.Color.Red;
                            seriesR.Points[count].Color = System.Drawing.Color.Red;
                            if (borderWidth > 0)
                            {
                                seriesR.Points[count].BorderWidth = borderWidth;
                                seriesR.Points[count].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);
                            }
                        }
                        else if (rotRes >= 2000 * 0.9)
                        {
                            //seriesR.Points[count].Color = System.Drawing.Color.FromArgb(255, 235, 245, 0); //System.Drawing.Color.Yellow;
                            seriesR.Points[count].Color = System.Drawing.Color.Yellow;
                            if (borderWidth > 0)
                            {
                                seriesR.Points[count].BorderWidth = borderWidth;
                                seriesR.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 215, 225, 0);
                            }
                        }
                        else
                        {
                            //seriesR.Points[count].Color = System.Drawing.Color.FromArgb(255, 45, 185, 45); //System.Drawing.Color.LimeGreen;
                            seriesR.Points[count].Color = System.Drawing.Color.LimeGreen;
                            if (borderWidth > 0)
                            {
                                seriesR.Points[count].BorderWidth = borderWidth;
                                seriesR.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);
                            }
                        }
                        series.Points[count].ToolTip = "gForce: " + myReader.linR + "g\nRotation: " + myReader.rotR + "°/s"; //#" + NodeProfile[myReader.UID].playerNum + " " + NodeProfile[myReader.UID].firstName + " " + NodeProfile[myReader.UID].lastName + "\n
                        seriesR.Points[count].ToolTip = series.Points[count].ToolTip;
                    }
                    series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");//:ss.fff
                    //seriesR.Points[count].BorderWidth = 0;
                    seriesR.Points[count].AxisLabel = tmpTime.ToString("HH:mm");//:ss.fff


                    // chart1.Series["Test1"].Points.AddXY
                    //             (myDateTime.Ticks, Convert.ToInt32(match.Groups[11].Value, 10));
                    count++;
                }

                //chart1.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
                chart1.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount;
                chart1.ChartAreas[0].AxisY2.IntervalAutoMode = IntervalAutoMode.FixedCount;
                chart1.ChartAreas[0].AxisY.Interval = 50;
                chart1.ChartAreas[0].AxisY2.Interval = 50;
                chart1.ChartAreas[0].AxisX.Interval = Math.Ceiling((double)series.Points.Count() / 10);
                chart1.ChartAreas[0].AxisY.CustomLabels.Clear();
                chart1.ChartAreas[0].AxisY2.CustomLabels.Clear();
                chart1.ChartAreas[0].AxisY.LabelStyle.Angle = 0;
                chart1.ChartAreas[0].AxisY2.LabelStyle.Angle = 0;
                chart1.ChartAreas[0].AxisY.Maximum = 200;
                chart1.ChartAreas[0].AxisY.Minimum = -200;
                chart1.ChartAreas.FirstOrDefault().AxisY2.Minimum = chart1.ChartAreas.FirstOrDefault().AxisY.Minimum;
                chart1.ChartAreas.FirstOrDefault().AxisY2.Maximum = chart1.ChartAreas.FirstOrDefault().AxisY.Maximum;

                chart1.ChartAreas[0].AxisY.Title = "Rotation     |        gForce";
                chart1.ChartAreas[0].AxisY.TitleAlignment = System.Drawing.StringAlignment.Center;
                int rIndex = 0;
                for (double i = chart1.ChartAreas[0].AxisY.Minimum; i <= chart1.ChartAreas[0].AxisY.Maximum; i += 50) //chart1.ChartAreas[0].AxisY.Interval
                {
                    string lbltext = "";
                    if (i < 0)
                        lbltext = (i * -20) + "°/s";
                    else if (i > 0)
                        lbltext = i + "g";
                    else
                        lbltext = "0";
                    chart1.ChartAreas[0].AxisY.CustomLabels.Add(i - 0.5, i + 0.5, lbltext);
                    chart1.ChartAreas[0].AxisY2.CustomLabels.Add(i - 0.5, i + 0.5, lbltext);
                    rIndex++;
                }
                /*chart1.Annotations.Clear();
                if (chart1.Annotations.Count() == 0)
                {
                    HorizontalLineAnnotation origin = new HorizontalLineAnnotation();
                    origin.AxisX = chart1.ChartAreas[0].AxisX;
                    origin.AxisX = chart1.ChartAreas[0].AxisY;
                    origin.IsSizeAlwaysRelative = false;
                    origin.AnchorY = chart1.ChartAreas[0].AxisY.ValueToPosition(0);// chart1.Series[0].Points.Average(p => p.XValue);
                    origin.IsInfinitive = true;
                    origin.ClipToChartArea = chart1.ChartAreas[0].Name;
                    origin.LineColor = Color.Black;
                    origin.LineWidth = 1;
                    chart1.Annotations.Add(origin);
                }*/
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }


        void drawTeamChartByList(Chart chart1)
        {
            int countOverThreshold = 0;
            //int elements = 100;
            int count = 0;
            double linRes = 0;
            double rotRes = 0;
            DateTime baseDate;
            DateTime tmpTime;
            int millisecond = 0;
            //const string f = "C:\\00\\g-force\\test.csv";
            // creates 100 random X points
            // remove all previous series

            chart1.Series.Clear();
            //chart1.Titles.Clear();
            //chart1.Titles.Add("Team At A Glance");
            //Font titleFont = new Font(chart1.Titles[0].Font.FontFamily, 12, FontStyle.Bold);
            //chart1.Titles[0].Font = titleFont;

            var wSeries = chart1.Series.Add("");
            var series = chart1.Series.Add("");
            var wSeriesR = chart1.Series.Add("");
            var seriesR = chart1.Series.Add("");
            wSeries.ChartType = SeriesChartType.StackedColumn;
            series.ChartType = SeriesChartType.StackedColumn;
            series.XValueType = ChartValueType.Auto;
            seriesR.ChartType = SeriesChartType.StackedColumn;
            wSeriesR.ChartType = SeriesChartType.StackedColumn;
            //chart1.Titles.Add("Shock Peak Point");
            try
            {
                // Parse to a timeline
                List<IMPACT_DATA> impData = new List<IMPACT_DATA>(teamImpacts);
                // Sort it
                impData.Sort((x, y) => x.millisecond.CompareTo(y.millisecond));
                impData.Sort((x, y) => x.time.CompareTo(y.time));
                //List<gForceTracker.Sideline_Viewer.IMPACT_DATA> 

                teamTimeline = new List<IMPACT_DATA>(impData);
                // Add "Missing" Entries
                DateTime lastTime = DateTime.MinValue;
                DateTime firstTime = DateTime.MinValue;
                DateTime firstOnTime = DateTime.MinValue;
                foreach (procArgs args in Args)
                {
                    if (args.firstOn > DateTime.MinValue && (args.firstOn < firstOnTime || firstOnTime == DateTime.MinValue))
                    {
                        firstOnTime = args.firstOn;
                    }
                }
                TimeSpan deltaT;
                foreach (IMPACT_DATA imp in impData)
                {
                    if (lastTime > DateTime.UtcNow.AddDays(-2))
                    {
                        if (firstTime < DateTime.UtcNow.AddDays(-2))
                        {
                            firstTime = imp.time;
                        }
                        deltaT = imp.time - lastTime;
                        for (int i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                        {
                            IMPACT_DATA nData = new IMPACT_DATA();
                            nData.eid = 0;
                            nData.UID = 0;
                            nData.thresholdG = 100;
                            nData.thresholdR = 2000;
                            nData.linR = 0;
                            nData.rotR = 0;
                            nData.time = lastTime.AddMinutes(i);
                            nData.millisecond = 0;
                            teamTimeline.Add(nData);
                        }
                    }
                    lastTime = imp.time;
                }
                if (firstTime > DateTime.UtcNow.AddDays(-2) && firstOnTime > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = firstTime - firstOnTime;
                    for (int i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        IMPACT_DATA nData = new IMPACT_DATA();
                        nData.eid = 0;
                        nData.UID = 0;
                        nData.thresholdG = 100;
                        nData.thresholdR = 2000;
                        nData.linR = 0;
                        nData.rotR = 0;
                        nData.time = firstOnTime.AddMinutes(i);
                        nData.millisecond = 0;
                        teamTimeline.Add(nData);
                    }
                }
                if (lastTime > DateTime.UtcNow.AddDays(-2))
                {
                    deltaT = DateTime.UtcNow - lastTime;
                    int i = 0;
                    for (i = 1; i < Math.Round(deltaT.TotalMinutes); i++)
                    {
                        IMPACT_DATA nData = new IMPACT_DATA();
                        nData.eid = 0;
                        nData.UID = 0;
                        nData.thresholdG = 100;
                        nData.thresholdR = 2000;
                        nData.linR = 0;
                        nData.rotR = 0;
                        nData.time = lastTime.AddMinutes(i);
                        nData.millisecond = 0;
                        teamTimeline.Add(nData);
                    }
                    if (teamTimeline.Count < 60)
                    {
                        int deltaI = 60 - teamTimeline.Count;
                        for (int i2 = 0; i2 < deltaI; i2++)
                        {
                            IMPACT_DATA nData = new IMPACT_DATA();
                            nData.eid = 0;
                            nData.UID = 0;
                            nData.thresholdG = 100;
                            nData.thresholdR = 2000;
                            nData.linR = 0;
                            nData.rotR = 0;
                            nData.time = lastTime.AddMinutes(i+i2);
                            nData.millisecond = 0;
                            teamTimeline.Add(nData);
                        }
                    }
                }
                // Sort it again
                teamTimeline.Sort((x, y) => x.millisecond.CompareTo(y.millisecond));
                teamTimeline.Sort((x, y) => x.time.CompareTo(y.time));

                int borderWidth = 2;
                if (teamTimeline.Count < 50)
                {
                    wSeries["PixelPointWidth"] = "15";
                    series["PixelPointWidth"] = "15";
                    wSeriesR["PixelPointWidth"] = "15";
                    seriesR["PixelPointWidth"] = "15";
                }
                else
                {
                    borderWidth = 0;
                }
                foreach (var myReader in teamTimeline)
                {

                    linRes = myReader.linR;
                    rotRes = myReader.rotR;
                    if (linRes >= 0.9 * myReader.thresholdG || rotRes >= 0.9 * myReader.thresholdR || (linRes == 0 && rotRes == 0))
                    {
                        baseDate = myReader.time;
                        int millis = baseDate.Millisecond;
                        millisecond = myReader.millisecond;
                        tmpTime = new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, baseDate.Hour, baseDate.Minute, baseDate.Second, millisecond);

                        if (gftGMT_Enable)
                            tmpTime = tmpTime.ToLocalTime();     // For displaying local time

                        var xDate = count;
                        if (linRes == 0 && rotRes == 0)
                        {
                            wSeries.Points.AddXY(xDate, 0);
                            wSeries.Points[count].Color = System.Drawing.Color.White;

                            wSeriesR.Points.AddXY(xDate, 0);
                            wSeriesR.Points[count].Color = System.Drawing.Color.White;

                            series.Points.AddXY(xDate, linRes);
                            seriesR.Points.AddXY(xDate, -rotRes / 20);
                        }
                        else
                        {
                            // Make a gap near 0
                            wSeries.Points.AddXY(xDate, 2);
                            wSeries.Points[count].Color = System.Drawing.Color.White;
                            if (borderWidth > 0)
                            {
                                wSeries.Points[count].BorderWidth = borderWidth;
                                wSeries.Points[count].BorderColor = System.Drawing.Color.White;
                            }
                            // Make a gap near 0
                            wSeriesR.Points.AddXY(xDate, -2);
                            wSeriesR.Points[count].Color = System.Drawing.Color.White;
                            if (borderWidth > 0)
                            {
                                wSeriesR.Points[count].BorderWidth = borderWidth;
                                wSeriesR.Points[count].BorderColor = System.Drawing.Color.White;
                            }

                            series.Points.AddXY(xDate, linRes);
                            if (linRes >= myReader.thresholdG)
                            {
                                series.Points[count].Color = System.Drawing.Color.Red;
                                if (borderWidth > 0)
                                {
                                    series.Points[count].BorderWidth = borderWidth;
                                    series.Points[count].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);
                                }
                                countOverThreshold++;
                            }
                            else if (linRes >= myReader.thresholdG * 0.9)
                            {
                                series.Points[count].Color = System.Drawing.Color.Yellow;
                                if (borderWidth > 0)
                                {
                                    series.Points[count].BorderWidth = borderWidth;
                                    series.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 215, 225, 0);
                                }

                            }
                            else
                            {
                                series.Points[count].Color = System.Drawing.Color.LimeGreen;
                                if (borderWidth > 0)
                                {
                                    series.Points[count].BorderWidth = borderWidth;
                                    series.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);
                                }
                            }

                            series.Points[count].ToolTip = "#" + NodeProfile[myReader.UID].playerNum + " " + NodeProfile[myReader.UID].firstName + " " + NodeProfile[myReader.UID].lastName + "\ngForce: " + myReader.linR + "g\nRotation: " + myReader.rotR + "°/s";

                            seriesR.Points.AddXY(xDate, -rotRes / 20);
                            if (rotRes >= myReader.thresholdR)
                            {
                                //seriesR.Points[count].Color = System.Drawing.Color.FromArgb(255, 235, 0, 0); //System.Drawing.Color.Red;
                                seriesR.Points[count].Color = System.Drawing.Color.Red;
                                if (borderWidth > 0)
                                {
                                    seriesR.Points[count].BorderWidth = borderWidth;
                                    seriesR.Points[count].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);
                                }
                            }
                            else if (rotRes >= myReader.thresholdR * 0.9)
                            {
                                //seriesR.Points[count].Color = System.Drawing.Color.FromArgb(255, 235, 245, 0); //System.Drawing.Color.Yellow;
                                seriesR.Points[count].Color = System.Drawing.Color.Yellow;
                                if (borderWidth > 0)
                                {
                                    seriesR.Points[count].BorderWidth = borderWidth;
                                    seriesR.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 215, 225, 0);
                                }
                            }
                            else
                            {
                                //seriesR.Points[count].Color = System.Drawing.Color.FromArgb(255, 45, 185, 45); //System.Drawing.Color.LimeGreen;
                                seriesR.Points[count].Color = System.Drawing.Color.LimeGreen;
                                if (borderWidth > 0)
                                {
                                    seriesR.Points[count].BorderWidth = borderWidth;
                                    seriesR.Points[count].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);
                                }
                            }
                            seriesR.Points[count].ToolTip = series.Points[count].ToolTip;
                        }
                        series.Points[count].AxisLabel = tmpTime.ToString("HH:mm");//:ss.fff
                        seriesR.Points[count].AxisLabel = tmpTime.ToString("HH:mm");//:ss.fff


                        // chart1.Series["Test1"].Points.AddXY
                        //             (myDateTime.Ticks, Convert.ToInt32(match.Groups[11].Value, 10));
                        count++;
                    }
                }

                //chart1.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
                chart1.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount;
                chart1.ChartAreas[0].AxisY.Interval = 50;
                chart1.ChartAreas[0].AxisX.Interval = Math.Ceiling((double)series.Points.Count() / 10);
                chart1.ChartAreas[0].AxisY.CustomLabels.Clear();
                chart1.ChartAreas[0].AxisY.LabelStyle.Angle = 0;
                chart1.ChartAreas[0].AxisY.Maximum = 200;
                chart1.ChartAreas[0].AxisY.Minimum = -200;
                chart1.ChartAreas[0].AxisY.Title = "Rotation     |        gForce";
                chart1.ChartAreas[0].AxisY.TitleAlignment = System.Drawing.StringAlignment.Center;
                int rIndex = 0;
                for (double i = chart1.ChartAreas[0].AxisY.Minimum; i <= chart1.ChartAreas[0].AxisY.Maximum; i += 50) //chart1.ChartAreas[0].AxisY.Interval
                {
                    string lbltext = "";
                    if (i < 0)
                        lbltext = (i * -20) + "°/s";
                    else if (i > 0)
                        lbltext = i + "g";
                    else
                        lbltext = "0";
                    chart1.ChartAreas[0].AxisY.CustomLabels.Add(i - 0.5, i + 0.5, lbltext);
                    rIndex++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        private double AccDecode(byte c)
        {
            //if((curGFT.hwMajor ==0x1) && (curGFT.hwMinor == 0))
            return (ACC_SENSITIVITY_LSB2 * (c - 128));
            //return (ACC_SENSITIVITY_LSB * (c * 4 - 512));
        }
        private void DrawImpactChartByList(Int32 nodeID, int lineStart, Chart chartRawLin, Chart chartRawGyro)
        {

            /*double[] myRes = new double[3];
            double[] my40 = new double[40];
            double[] maxPoint = new double[4];
            double[] myPoint = new double[3];*/
            //double myDouble;
            //double tDouble = 0;
            double azimuth = 0;
            double elevation = 0;
            int count = 0;
            /*byte[] dataX;
            byte[] dataY;
            byte[] dataZ;

            byte[] rdataX;
            byte[] rdataY;
            byte[] rdataZ;
            double mDataX, mDataY, mDataZ;
            byte i;
            double[] mTmpPoint = new double[3];
            double linearR = 0;
            double linearAlarmT = 0;
            //const string f = "C:\\00\\g-force\\test.csv";
            chartRawLin.Series.Clear();

            var seriesX = chartRawLin.Series.Add("Linear X");
            var seriesY = chartRawLin.Series.Add("Linear Y");
            var seriesZ = chartRawLin.Series.Add("Linear Z");
            var seriesV = chartRawLin.Series.Add("Linear Res.");
            seriesX.ChartType = SeriesChartType.Line;
            seriesX.XValueType = ChartValueType.Auto;
            seriesY.ChartType = SeriesChartType.Line;
            seriesY.XValueType = ChartValueType.Auto;
            seriesZ.ChartType = SeriesChartType.Line;
            seriesZ.XValueType = ChartValueType.Auto;
            seriesV.ChartType = SeriesChartType.Line;
            seriesV.XValueType = ChartValueType.Auto;
            seriesX.BorderWidth = 2;
            seriesY.BorderWidth = 2;
            seriesZ.BorderWidth = 2;
            seriesV.BorderWidth = 2;
            seriesX.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            seriesY.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            seriesZ.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            seriesV.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));
            seriesX.ToolTip = "Linear X: " + "#VAL{N2}";
            seriesY.ToolTip = "Linear Y: " + "#VAL{N2}";
            seriesZ.ToolTip = "Linear Z: " + "#VAL{N2}";
            seriesV.ToolTip = "Linear Res.: " + "#VAL{N2}";

            //gyro
            chartRawGyro.Series.Clear();

            var seriesRX = chartRawGyro.Series.Add("Rot. X");
            var seriesRY = chartRawGyro.Series.Add("Rot. Y");
            var seriesRZ = chartRawGyro.Series.Add("Rot. Z");
            var seriesRV = chartRawGyro.Series.Add("Rot. Res.");
            seriesRX.ChartType = SeriesChartType.Line;
            seriesRX.XValueType = ChartValueType.Auto;
            seriesRY.ChartType = SeriesChartType.Line;
            seriesRY.XValueType = ChartValueType.Auto;
            seriesRZ.ChartType = SeriesChartType.Line;
            seriesRZ.XValueType = ChartValueType.Auto;
            seriesRV.ChartType = SeriesChartType.Line;
            seriesRV.XValueType = ChartValueType.Auto;
            seriesRX.BorderWidth = 2;
            seriesRY.BorderWidth = 2;
            seriesRZ.BorderWidth = 2;
            seriesRV.BorderWidth = 2;
            seriesRX.Color = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(114)))), ((int)(((byte)(167)))));
            seriesRY.Color = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(70)))), ((int)(((byte)(67)))));
            seriesRZ.Color = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            seriesRV.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(105)))), ((int)(((byte)(155)))));
            seriesRX.ToolTip = "Rot. X: " + "#VAL{N2}";
            seriesRY.ToolTip = "Rot. Y: " + "#VAL{N2}";
            seriesRZ.ToolTip = "Rot. Z: " + "#VAL{N2}";
            seriesRV.ToolTip = "Rot. Res.: " + "#VAL{N2}";*/

            try
            {
                foreach (var myReader in Args[nodeID - 1].recvFIFO)
                {
                    if (count == lineStart)
                    {
                        /*dataX = myReader.dataX;                        
                        dataY = myReader.dataY;
                        dataZ = myReader.dataZ;

                        rdataX = myReader.rdataX;
                        rdataY = myReader.rdataY;
                        rdataZ = myReader.rdataZ;*/

                        azimuth = myReader.azimuth;
                        elevation = myReader.elevation;
                        /*linearR = myReader.linR;

                        //linearAlarmT = gDataGetSessionSidThreshold(curSessionID, "SessionWireless");
                        linearAlarmT = NodeProfile[currentNodeID - 1].alarmThres;

                        if (linearR >= linearAlarmT)
                            hitPositionColor = "red";
                        else if (linearR >= linearAlarmT * 0.9)
                            hitPositionColor = "yellow";
                        else
                            hitPositionColor = "green";
                        for (i = 0; i < 120; i++)
                        {
                            mDataX = AccDecode(dataX[i]);
                            mDataY = AccDecode(dataY[i]);
                            mDataZ = AccDecode(dataZ[i]);
                            seriesX.Points.AddXY(i, mDataX);
                            seriesY.Points.AddXY(i, mDataY);
                            seriesZ.Points.AddXY(i, mDataZ);
                            seriesV.Points.AddXY(i, Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ));
                            seriesX.Points[i].AxisLabel = Convert.ToString(i / 3);
                        }
                        for (i = 0; i < 32; i++)
                        {
                            mDataX = gyroDecode(rdataX[i * 2], rdataX[i * 2 + 1]);
                            mDataY = gyroDecode(rdataY[i * 2], rdataY[i * 2 + 1]);
                            mDataZ = gyroDecode(rdataZ[i * 2], rdataZ[i * 2 + 1]);
                            seriesRX.Points.AddXY(i, mDataX);
                            seriesRY.Points.AddXY(i, mDataY);
                            seriesRZ.Points.AddXY(i, mDataZ);
                            seriesRV.Points.AddXY(i, Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ));
                            seriesRX.Points[i].AxisLabel = Convert.ToString(i);

                        }*/
                        break;
                    }
                    count++;
                }

            }
            catch (Exception f)
            {
                Console.WriteLine(f.ToString());
            }
            /*chartRawLin.ChartAreas[0].AxisX.ScaleView.MinSize = 10;
            chartRawLin.ChartAreas[0].AxisX.Interval = 10;
            chartRawLin.ChartAreas[0].AxisX.IntervalOffset = 0;
            chartRawGyro.ChartAreas[0].AxisX.ScaleView.MinSize = 10;
            chartRawGyro.ChartAreas[0].AxisX.Interval = 10;
            chartRawGyro.ChartAreas[0].AxisX.IntervalOffset = 0;*/

            gCalcPosition(azimuth, elevation);
        }

        private void mWirelessViewer_OnRowSelectedByList(Object sender, WirelessGFTViewer.NodeOnSelectedArgs args, DataGridView wirelessViewer, DataGridView dataGridView3, Chart chartViewer, int impact = 0)
        {
#if ENABLE_DASHBOARD

            if (splitContainer3.Panel1Collapsed) // Panel 1 is Team View, Panel 2 is Player View
            {
                //Int32 index = 0;
                if (args.selectedRow >= 0)
                {
                    if ((args.NodeID <= 0) || (args.NodeID > NUM_NODES))
                        return;

                    if (splitContainer4.Panel1Collapsed)
                    {
                        currentNodeID = args.NodeID;             // Current UID (Node ID)
                        //curSessionID = gDataGetSessionLastSID(currentNodeID);
                        //LastSessionID = curSessionID; //this.NodeProfile[currentNodeID-1]

                        this.lblPerfPlayer.Text = "#" + this.NodeProfile[currentNodeID - 1].playerNum + " " + this.NodeProfile[currentNodeID - 1].firstName + " " + this.NodeProfile[currentNodeID - 1].lastName;
                        
                        TimeSpan recovery = DateTime.UtcNow - lpHub.lpDevices[currentNodeID - 1].lastThresholdTime;
                        string recoveryText = "No Threshold Impacts";
                        if (recovery.TotalDays < 100)
                        {
                            if (recovery.Hours > 0)
                            {
                                recoveryText = recovery.Hours.ToString() + " Hour" + (recovery.Hours > 1 ? "s" : "");

                                if (recovery.Minutes > 0)
                                {
                                    recoveryText += ", " + recovery.Minutes.ToString() + " Minute" + (recovery.Minutes > 1 ? "s" : "");
                                }
                                recoveryText += " Since last Threshold Impact";
                            }
                            else if (recovery.Minutes > 0)
                            {
                                recoveryText = recovery.Minutes.ToString() + " Minute" + (recovery.Minutes > 1 ? "s" : "");

                                if (recovery.Seconds > 0)
                                {
                                    recoveryText += ", " + recovery.Seconds.ToString() + " Second" + (recovery.Seconds > 1 ? "s" : "");
                                }
                                recoveryText += " Since last Threshold Impact";
                            }
                            else if (recovery.Seconds > 0)
                            {
                                recoveryText = recovery.Seconds.ToString() + " Second" + (recovery.Seconds > 1 ? "s" : "");
                                recoveryText += " Since last Threshold Impact";
                            }
                        }
                        this.lblPerfRecovery.Text = recoveryText;

                        string deviceSettings = "Record: " + NodeProfile[currentNodeID - 1].recThres.ToString() + "g, Threshold: " + NodeProfile[currentNodeID - 1].alarmThres.ToString() + "g / 2000°/s";
                        lblDeviceSettings.Text = deviceSettings;

                        this.lblPerfStatus.Text = lpHub.lpDevices[currentNodeID - 1].status + lpHub.lpDevices[currentNodeID - 1].commandStatus.Replace("\n", ", ") +", " + deviceSettings;
                        double averageG = 0;
                        double averageR = 0;
                        double averageEP = 0;
                        double highestEP = 0;
                        double averagePL = 0;
                        double highestPL = 0;

                        List<double> gForce = new List<double>();
                        List<double> rotR = new List<double>();
                        List<double> pl = new List<double>();
                        List<double> ep = new List<double>();
                        List<int> ecnt = new List<int>();
                        foreach (IMPACT_DATA impData in Args[currentNodeID - 1].recvFIFO)
                        {
                            gForce.Add(impData.linR);
                            rotR.Add(impData.rotR);
                        }
                        foreach (RPE_DATA rData in lpHub.lpDevices[currentNodeID - 1].rpeList)
                        {
                            //rpe.Add(rData.rpe);
                            pl.Add(rData.player_load);
                            ep.Add(rData.explosive_power);
                            ecnt.Add((int)rData.explosive_count);
                        }

                        if (pl.Count() > 0)
                        {
                            //this.averageRPE = Math.Round(rpe.Sum() / perfNodes, 1);
                            this.averageEP = Math.Round(ep.Sum() / ep.Count(), 1);
                            this.averagePL = Math.Round(pl.Sum() / 60 / pl.Count(), 1);
                        }

                        //rpe.Sort();
                        pl.Sort();
                        ep.Sort();
                        if (pl.Count > 0)
                        {
                            //this.averageEP = Math.Round(ep.Average(), 1);
                            //this.averagePL = Math.Round(pl.Average(), 1);
                            //this.averageRPE = Math.Round(rpe.Average(), 1);
                            this.highestPL = Math.Round(pl.Max() / 60, 1);
                            this.highestEP = ep.Max();
                            if (pl.Count % 2 == 0)
                            {
                                this.medianEP = Math.Round(((double)ep[(ep.Count / 2)] + ep[(ep.Count / 2) - 1]) / 2, 1);
                                this.medianPL = Math.Round(((double)pl[(pl.Count / 2)] + pl[(pl.Count / 2) - 1]) / 2, 1);
                                //this.medianRPE = Math.Round((rpe[(rpe.Count / 2)] + rpe[(rpe.Count / 2) - 1]) / 2, 1);
                            }
                            else
                            {
                                this.medianEP = Math.Round((double)ep[(int)((ep.Count / 2) - 0.5)], 1);
                                this.medianPL = Math.Round((double)pl[(int)((pl.Count / 2) - 0.5)], 1);
                                //this.medianRPE = Math.Round(rpe[(int)((rpe.Count / 2) - 0.5)], 1);
                            }
                            medianPL = Math.Round(medianPL / 60, 1);
                        }

                        gForce.Sort();
                        rotR.Sort();
                        if (gForce.Count > 0 && rotR.Count > 0)
                        {
                            averageG = Math.Round(gForce.Average(), 1);
                            averageR = Math.Round(rotR.Average(), 1);
                            // Get Median 
                            if (lpHub.lpDevices[currentNodeID - 1].impacts > 0)
                            {
                                if (lpHub.lpDevices[currentNodeID - 1].impacts % 2 == 0)
                                {
                                    medianG = Math.Round((gForce[(lpHub.lpDevices[currentNodeID - 1].impacts / 2)] + gForce[(lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 1]) / 2, 1);
                                    medianR = (int)Math.Round((rotR[(lpHub.lpDevices[currentNodeID - 1].impacts / 2)] + rotR[(lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 1]) / 2, 0);
                                }
                                else
                                {
                                    medianG = gForce[(int)((lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 0.5)];
                                    medianR = (int)Math.Round(rotR[(int)((lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 0.5)], 0);
                                }
                            }
                        }
                        int activeMinutes = (int)Math.Round((double)lpHub.lpDevices[currentNodeID - 1].activeTime / 3000 / 60, 0);
                        if (lpHub.lpDevices[currentNodeID - 1].activeTime > 0)
                        {
                            this.lblPerfPlayerLoad.Visible = false;
                            tileImage(this.tableLayoutPanel22, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, Math.Round((double)lpHub.lpDevices[currentNodeID - 1].impacts / ((double)lpHub.lpDevices[currentNodeID - 1].activeTime / 3000 / 60), 1).ToString());
                        }
                        tileImage(this.tableLayoutPanel19, WirelessGFTViewer.Properties.Resources.impact, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, Math.Round(lpHub.lpDevices[currentNodeID - 1].gForce, 1).ToString());

                        tileImage(this.tableLayoutPanel21, WirelessGFTViewer.Properties.Resources.rotation, Color.White, Color.Green, 4, this.lblTeamGForce.Font, Math.Round(lpHub.lpDevices[currentNodeID - 1].Rotation, 0).ToString());
                        this.lblPerfThresholdImpacts.Text = lpHub.lpDevices[currentNodeID - 1].thresholdImpacts.ToString();

                        this.lblPerfThresholdWithinAbove.Text = "Wtihin: " + lpHub.lpDevices[currentNodeID - 1].withinThreshold;
                        this.lblPerfThresholdAbove.Text = "Above: " + lpHub.lpDevices[currentNodeID - 1].aboveThreshold + " (" + (lpHub.lpDevices[currentNodeID - 1].impacts > 0 ? Math.Round((double)lpHub.lpDevices[currentNodeID - 1].aboveThreshold / lpHub.lpDevices[currentNodeID - 1].impacts * 100).ToString() : "0") + "%)";
                        tileImage(this.tableLayoutPanel20, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, (lpHub.lpDevices[currentNodeID - 1].withinThreshold + lpHub.lpDevices[currentNodeID - 1].aboveThreshold).ToString());


                        // Performance Metrics - Player

                        //this.lblPerfPlayerLoad.Visible = true;
                        // Performance Player Load // lblPerfPlayerLoadText
                        tileImage(this.tableLayoutPanel22, WirelessGFTViewer.Properties.Resources.burn, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, (Math.Round(pl.Sum()/60,1)).ToString());
                        //this.lblPerfPlayerLoadText.Text = "Average: " + this.averagePL + ", Median: " + this.medianPL + "";
                        this.lblPerfPLAve.Text = "Average: " + this.averagePL + " PL/min";
                        this.lblPerfPLHighest.Text = "Highest: " + this.highestPL + " PL/min";

                        // Performance Explosive Power // lblPerfEPAveMed
                       // this.lblPerfEPAveMed.Text = "Average: " + Math.Round(this.averageEP/100,1) + "s, Median: " + Math.Round(this.medianEP/100,1) + "s";
                        int epSum = (int)ep.Sum();


                        this.lblPerfEPRating.Text = "Total: " + (Math.Round((double)epSum / 100, 2)).ToString() + " sec";
                        this.lblPerfEPHighest.Text = "Highest: " + Math.Round((double)this.highestEP / 100, 2) + " sec"; // "Total Time: " + (Math.Round((double)epSum / 1000, 2)).ToString() + "s";
 
                        int epCunt = (int)ecnt.Sum();
                        //tileImage(this.tableLayoutPanel17, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, (Math.Round((double)epSum/100,1)).ToString() +"s");
                        tileImage(this.tableLayoutPanel17, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, epCunt.ToString());

                        //tileImage(this.tableLayoutPanel17, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, (Math.Round((double)epSum/100,1)).ToString() +"s");

                        // Performance RPE //lblPerfRPEAveMed
                        //this.lblPerfRPEAveMed.Text = "";// "Average: " + averageRPE + ", Median: " + medianRPE + "";
                        tileImage(this.tableLayoutPanel26, WirelessGFTViewer.Properties.Resources.burn, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, ((lpHub.lpDevices[currentNodeID - 1].rpeList.Count()  > 0 ? lpHub.lpDevices[currentNodeID - 1].rpeList.Last().rpe : 0)).ToString());
                        

                        this.lblPerfGForceAve.Text = "Average: " + averageG + "g";
                        this.lblPerfGForceMed.Text = "Median: " + medianG + "g";
                        this.lblPerfRotationAve.Text = "Average: " + averageR + "°/s";
                        this.lblPerfRotationMed.Text = "Median: " + medianR + "°/s";

                        int list_count = Args[currentNodeID - 1].recvFIFO.Count;
                        int list_rpe = lpHub.lpDevices[currentNodeID - 1].rpeList.Count;

                        // Pie Charts

                        Font lblFont = new Font(perfLocationChartPlayer.Font.FontFamily, 9, FontStyle.Bold);

                        // Draw Impact Location Pie Chart
                        int impactLeft = 0;
                        int impactRight = 0;
                        int impactTop = 0;
                        int impactBottom = 0;
                        int impactFront = 0;
                        int impactBack = 0;
                        foreach (IMPACT_DATA iData in Args[currentNodeID - 1].recvFIFO)
                        {
                            /* No longer consider bottom impacts, simply assign based on azimuth
                            else if (iData.elevation <= -45)
                                impactBottom++; // = "BOTTOM";*/
                            if (iData.elevation >= 45)
                                impactTop++; // = "TOP";
                            else if (iData.azimuth <= 225 && iData.azimuth >= 135)
                                impactBack++;// = "BACK";
                            else if (iData.azimuth <= 45 || iData.azimuth >= 315)
                                impactFront++;// = "FRONT";
                            else if (iData.azimuth >= 45 && iData.azimuth <= 135)
                                impactRight++;// = "RIGHT";
                            else
                                impactLeft++;// = "LEFT";
                        }
                        try
                        {
                            perfLocationChartPlayer.Series.Clear();
                            Series sLocations = perfLocationChartPlayer.Series.Add("");
                            sLocations.LabelForeColor = System.Drawing.Color.White;
                            sLocations.Font = lblFont;
                            sLocations.ChartType = SeriesChartType.Pie;

                            sLocations.Points.AddXY(0, impactRight);
                            if (impactRight > 0) sLocations.Points[0].AxisLabel = impactRight.ToString();
                            sLocations.Points[0].LegendText = "Right";//: " + impactRight.ToString();
                            sLocations.Points[0].Color = System.Drawing.Color.Red;
                            sLocations.Points[0].BorderWidth = 2;
                            sLocations.Points[0].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);

                            sLocations.Points.AddXY(0, impactLeft);
                            if (impactLeft > 0) sLocations.Points[1].AxisLabel = impactLeft.ToString();
                            sLocations.Points[1].LegendText = "Left";//: " + impactLeft.ToString();
                            sLocations.Points[1].Color = System.Drawing.Color.FromArgb(235, 95, 125, 0);
                            sLocations.Points[1].BorderWidth = 2;
                            sLocations.Points[1].BorderColor = System.Drawing.Color.FromArgb(235, 85, 105, 0);

                            sLocations.Points.AddXY(0, impactTop);
                            if (impactTop > 0) sLocations.Points[2].AxisLabel = impactTop.ToString();
                            sLocations.Points[2].LegendText = "Top";//: " + impactTop.ToString();
                            sLocations.Points[2].Color = System.Drawing.Color.LimeGreen;
                            sLocations.Points[2].BorderWidth = 2;
                            sLocations.Points[2].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);

                            sLocations.Points.AddXY(0, impactBottom);
                            if (impactBottom > 0) sLocations.Points[3].AxisLabel = impactBottom.ToString();
                            sLocations.Points[3].LegendText = "Bottom";//: " + impactBottom.ToString();
                            sLocations.Points[3].Color = System.Drawing.Color.FromArgb(235, 105, 85, 255);
                            sLocations.Points[3].BorderWidth = 2;
                            sLocations.Points[3].BorderColor = System.Drawing.Color.FromArgb(235, 95, 75, 235);

                            sLocations.Points.AddXY(0, impactFront);
                            if (impactFront > 0) sLocations.Points[4].AxisLabel = impactFront.ToString();
                            sLocations.Points[4].LegendText = "Front";//: " + impactFront.ToString();
                            sLocations.Points[4].Color = System.Drawing.Color.FromArgb(235, 0, 155, 155);
                            sLocations.Points[4].BorderWidth = 2;
                            sLocations.Points[4].BorderColor = System.Drawing.Color.FromArgb(235, 0, 135, 135);

                            sLocations.Points.AddXY(0, impactBack);
                            if (impactBack > 0) sLocations.Points[5].AxisLabel = impactBack.ToString();
                            sLocations.Points[5].LegendText = "Back";//: " + impactBack.ToString();
                            sLocations.Points[5].Color = System.Drawing.Color.FromArgb(235, 255, 175, 0);
                            sLocations.Points[5].BorderWidth = 2;
                            sLocations.Points[5].BorderColor = System.Drawing.Color.FromArgb(235, 235, 155, 0);
                        }
                        catch (Exception e)
                        {
                        }

                        if (list_count > 0)
                        {
                            //dataGetImpactByList(currentNodeID, dataGridView3);
                            //drawChartByList(currentNodeID, 60/*gDataGetSessionSidThreshold(curSessionID, "SessionWireless")*/, chartViewer);
                            drawChartByList(currentNodeID, NodeProfile[currentNodeID - 1].alarmThres, chartViewer);

                            //chartRawLinW.Series.Clear();
                            //chartRawGyroW.Series.Clear();

                            Impacts_MouseToFirst(sender, new DataGridView(), chartRawLinW, chartRawGyroW, pbPerfHead, "EventsWireless", impact, chartViewer);

                        }
                        else
                        {
                            // Added by Jason Chen for Show, 2014.01.09
                            chartViewer.Series.Clear();
                            chartRawLinW.Series.Clear();
                            chartRawGyroW.Series.Clear();

                            //int rowCount = dataGridView3.Rows.Count;
                            //for (int index = 0; index < rowCount; index++) dataGridView3.Rows.RemoveAt(0);

                        }
                        if (list_rpe > 0)
                        {
                            //drawRPEChartByList(currentNodeID, perfChartRPE);
                            perfChartRPE.Series.Clear();
                            drawEPChartByList(currentNodeID, perfChartEP);
                            drawPLChartByList(currentNodeID, perfChartPL);


                            int restActivity = 0;
                            int moderateActivity = 0;
                            int highActivity = 0;
                            int veryHighActivity = 0;
                            
                            try
                            {
                                // Fuel Gauge
                                List<IMPACT_DATA> timeline = getTimeline(currentNodeID);

                                List<RPE_DATA> rpeTimeline = getRPETimeline(currentNodeID, timeline);
                                // Calculate fuel gauge and compensate for impacts on timeline to keep visually synchronized
                                List<FG_DATA> fgData = new List<FG_DATA>();
                                int x_cur = 0;
                                int x_new = 0;
                                DateTime lTime = DateTime.MinValue;
                                int iCnt = 0;
                                int imp = 0;
                                foreach (RPE_DATA aData in rpeTimeline) //lpHub.lpDevices[currentNodeID - 1].rpeList
                                {
                                    // Compensate for any missed report packets, for now, they are "rest". We should not actually have any missing, as we have a compensation mechanism when the packets are received.
                                    if (lTime < aData.report_time.AddSeconds(-11) && lTime > DateTime.MinValue) // Our resolution for this list is 1 = 1 second, the gaps should be no more than 10 seconds, we add 1 second as a buffer
                                    {
                                        int addSecs = 10;
                                        while(lTime.AddSeconds(addSecs) < aData.report_time.AddSeconds(-11)) {
                                            x_new = x_cur + 10;
                                            fgData.Add(new FG_DATA { x_start = x_cur, x_end = x_new, d_start = lTime.AddSeconds(addSecs), d_end = lTime.AddSeconds(addSecs + 10), player_load = 20 });
                                            x_cur = x_new;
                                            addSecs += 10;
                                        }
                                    }
                                    // Count through impacts to see if any occur during this RPE report
                                    for (imp = 0; iCnt + imp < timeline.Count; imp++)
                                    {
                                        if (timeline[iCnt + imp].time < aData.report_time || timeline[iCnt + imp].time >= aData.report_time.AddSeconds(10))
                                        {
                                            // Break the loop, we don't have any impacts here.
                                            break;
                                        }
                                    }
                                    iCnt += imp;
                                    x_new = x_cur + 10 + (imp * 6 * 10); // add 6 periods of 10 seconds for each "impact" that occurred - impacts take up 1 minute of space on the impacts timeline...
                                    fgData.Add(new FG_DATA { x_start = x_cur, x_end = x_new, d_start = aData.report_time, d_end = aData.report_time.AddSeconds(10), player_load = aData.player_load });
                                    lTime = aData.report_time;
                                    x_cur = x_new;
                                }

                                int xOffset = (int)Math.Round(pbRPEHeatMap.Width * 0.05) + 10;
                                int xOffsetRight = (int)Math.Round(pbRPEHeatMap.Width * 0.0025) * 20 - 25;

                                // Ok, we now have an entry for every 10 seconds, but that's probably way too many entries. Lets get our width and find out what a more appropriate time period would be?
                                int maxEntries = (int)Math.Round((double)(pbRPEHeatMap.Width - 50 - xOffset - xOffsetRight) / 3);

                                int modVal = (int)Math.Ceiling((double)fgData.Count / maxEntries);

                                // Ok, so lets work with this..
                                int thisCnt = 0;
                                uint thisPL = 0;
                                int thisX = 0;
                                int cCnt = 0;
                                DateTime thisd = DateTime.MinValue;
                                List<FG_DATA> fgData2 = new List<FG_DATA>();
                                foreach (FG_DATA fData in fgData)
                                {
                                    if (thisCnt == 0)
                                    {
                                        thisPL = 0;
                                        thisX = fData.x_start;
                                        thisd = fData.d_start;
                                    }
                                    if(fData.player_load > thisPL) thisPL = fData.player_load; //thisPL += fData.player_load; // take the maximum found in the set
                                    thisCnt++;
                                    cCnt++;
                                    if (thisCnt % modVal == 0 || cCnt == fgData.Count)
                                    {
                                        fgData2.Add(new FG_DATA { x_start = thisX, x_end = fData.x_end, d_start = thisd, d_end = fData.d_end, player_load = thisPL }); // (uint)Math.Round((double)thisPL / thisCnt)
                                        thisCnt = 0;
                                    }
                                }


                                // Total length = x_new

                                // Draw Fuel Tank
                                if (pbRPEHeatMap.Image == null || pbRPEHeatMap.Image.Width != pbRPEHeatMap.Width || pbRPEHeatMap.Image.Height != pbRPEHeatMap.Height)
                                {
                                    pbRPEHeatMap.Image = new Bitmap(pbRPEHeatMap.Width, pbRPEHeatMap.Height);
                                }

                                RPE_DATA lData = new RPE_DATA();
                                lData.rpe = 0;
                                lData.report_time = rpeTimeline.First().report_time; //lpHub.lpDevices[currentNodeID - 1].rpeList


                                TimeSpan sesLength = DateTime.UtcNow - lData.report_time;
                                int totalSeconds = x_new; // (int)sesLength.TotalSeconds;

                                using (Graphics g = Graphics.FromImage(pbRPEHeatMap.Image))
                                {
                                    SolidBrush myBrush = new SolidBrush(Color.White);
                                    g.FillRectangle(myBrush, 00, 0, pbRPEHeatMap.Width, pbRPEHeatMap.Height);
                                    Pen pen = new Pen(Color.Black);
                                    g.DrawLine(pen, xOffset + 24, 24, pbRPEHeatMap.Width - 24 - xOffsetRight, 24);
                                    g.DrawLine(pen, xOffset + 24, pbRPEHeatMap.Height - 25, pbRPEHeatMap.Width - 24 - xOffsetRight, pbRPEHeatMap.Height - 25);
                                    g.DrawLine(pen, xOffset + 24, 24, xOffset + 24, pbRPEHeatMap.Height - 25);
                                    g.DrawLine(pen, pbRPEHeatMap.Width - 24 - xOffsetRight, 24, pbRPEHeatMap.Width - 24 - xOffsetRight, pbRPEHeatMap.Height - 25);

                                    //TimeSpan t = TimeSpan.FromSeconds(0);
                                    int xPos = xOffset;
                                    int inc = (int)(totalSeconds / 60) * 60 / 11;
                                    if (inc < 60) inc = 60;
                                    SolidBrush myBrushB = new SolidBrush(Color.Black);
                                    for (int i = 0; i <= totalSeconds; i += inc)
                                    {
                                        /*xPos = xOffset + 24 + (int)Math.Round(((double)i / totalSeconds * (pbRPEHeatMap.Width - 48)));
                                        g.DrawLine(pen, xPos, pbRPEHeatMap.Height - 24, xPos, pbRPEHeatMap.Height - 20);
                                        //t = TimeSpan.FromSeconds(i);
                                        g.DrawString(string.Format("{0:D2}:{1:D2}", lData.report_time.AddSeconds(i).ToLocalTime().Hour, lData.report_time.AddSeconds(i).Minute), lblRotationAveMed.Font, myBrushB, new Point(xPos - 15, pbRPEHeatMap.Height - 18));
                                        g.DrawString("Fuel Tank", this.label4.Font, myBrushB, new Point(4, 2));*/

                                        g.FillRectangle(myBrushB, 120, 6, 16, 12);
                                        g.FillRectangle(myBrushB, 200, 6, 16, 12);
                                        g.FillRectangle(myBrushB, 280, 6, 16, 12);

                                        g.FillRectangle(new SolidBrush(Color.LimeGreen), 121, 7, 14, 10);
                                        g.FillRectangle(new SolidBrush(Color.Yellow), 201, 7, 14, 10);
                                        g.FillRectangle(new SolidBrush(Color.Red), 281, 7, 14, 10);

                                        g.DrawString("Rest", this.lblRotationAveMed.Font, myBrushB, new Point(135, 6));
                                        g.DrawString("Moderate", this.lblRotationAveMed.Font, myBrushB, new Point(215, 6));
                                        g.DrawString("Active", this.lblRotationAveMed.Font, myBrushB, new Point(295, 6));
                                    }
                                }

                                //ACTIVITY_DATA rData = lpHub.lpDevices[currentNodeID - 1].activeTimeList.Last(); // Most recent activity Data
                                if (rpeTimeline.Count > 0) //lpHub.lpDevices[currentNodeID - 1].rpeList
                                {
                                    RPE_DATA rData = rpeTimeline.Last(); // Most recent activity Data //lpHub.lpDevices[currentNodeID - 1].rpeList
                                    TimeSpan activelength = rData.report_time - lData.report_time;
                                    int totalTime = x_new; //(int)activelength.TotalSeconds;

                                    System.Drawing.Drawing2D.LinearGradientBrush myGradient;

                                    double deltaA;
                                    int deltaT;//TimeSpan deltaT;
                                    double activePct;
                                    Color lastColour = Color.LimeGreen;
                                    Color activeColour = Color.LimeGreen;

                                    double xPos = xOffset + 25;
                                    double xPosTo = xPos;

                                    //Pen pen;
                                    Pen pen = new Pen(Color.Black);
                                    SolidBrush myBrushB = new SolidBrush(Color.Black);
                                    modVal = (int)Math.Ceiling((double)fgData2.Count / 11);
                                    cCnt = 0;
                                    using (Graphics g = Graphics.FromImage(pbRPEHeatMap.Image))
                                    {
                                        foreach(FG_DATA fData in fgData2)
                                        //foreach (RPE_DATA aData in lpHub.lpDevices[currentNodeID - 1].rpeList)
                                        {
                                            if (cCnt % modVal == 0)
                                            {
                                                g.DrawLine(pen, (int)xPos, pbRPEHeatMap.Height - 24, (int)xPos, pbRPEHeatMap.Height - 20);
                                                g.DrawString(string.Format("{0:D2}:{1:D2}", fData.d_start.ToLocalTime().Hour, fData.d_start.ToLocalTime().Minute), lblRotationAveMed.Font, myBrushB, new Point((int)xPos - 15, pbRPEHeatMap.Height - 18));
                                            }
                                            cCnt++;
                                            // Determine the colour.. 0-2 = Rest, 2-3.5 = Moderate, 3.5+ = High Activity
                                            // What's our active time delta?
                                            //deltaA = aData.rpe - lData.rpe;
                                            deltaT = fData.x_end - fData.x_start;// fData.report_time - lData.report_time;
                                            xPosTo = xPos + (((double)deltaT / totalTime) * (pbRPEHeatMap.Width - 50 - xOffset - xOffsetRight)); //(int)Math.Round((deltaT.TotalSeconds / activelength.TotalSeconds) * (pbRPEHeatMap.Width - 50));
                                            //System.Diagnostics.Debug.WriteLine(((double)deltaT / totalTime) * (pbRPEHeatMap.Width - 50 - xOffset - xOffsetRight));
                                            if (xPosTo > (pbRPEHeatMap.Width - 25 - xOffsetRight)) xPosTo = (pbRPEHeatMap.Width - 25 - xOffsetRight);
                                            //activePct = (deltaA / 3000) / deltaT.TotalSeconds;
                                            if (fData.player_load <= 21)
                                            {
                                                activeColour = Color.LimeGreen;
                                                restActivity += deltaT;// (int)deltaT.TotalSeconds;
                                            }
                                            else if (fData.player_load < 35)
                                            {
                                                activeColour = Color.Yellow;
                                                moderateActivity += deltaT;// (int)deltaT.TotalSeconds;
                                            }
                                            else
                                            {
                                                activeColour = Color.Red;
                                                if (fData.player_load < 40)
                                                    highActivity += deltaT;// (int)deltaT.TotalSeconds;
                                                else
                                                    veryHighActivity += deltaT;// (int)deltaT.TotalSeconds;
                                            }
                                            if (xPosTo > xPos)
                                            {
                                                System.Diagnostics.Debug.WriteLine(cCnt.ToString() + " " + fData.d_start.ToLocalTime().ToString() + " Fuel Gauge xPos: " + xPos.ToString() + ", xPosTo: " + xPosTo.ToString() + " Max Pos: " + (pbRPEHeatMap.Width - 25 - xOffsetRight).ToString() + ", Player Load: " + fData.player_load.ToString());
                                                //System.Diagnostics.Debug.WriteLine(cCnt.ToString() + " " + fData.d_start.ToLocalTime().ToString() + " Fuel Gauge xPos: " + xPos.ToString() + ", xPosTo: " + xPosTo.ToString() + " Max Pos: " + (pbRPEHeatMap.Width - 25 - xOffsetRight).ToString() + ", Player Load: " +fData.player_load.ToString());
                                                myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                                    new Point((int)xPos - 1, (this.pbRPEHeatMap.Height - 25)),
                                                    new Point((int)xPosTo, (this.pbRPEHeatMap.Height - 25)),
                                                    lastColour, activeColour
                                                    );
                                                //pen = new Pen(myGradient);
                                                g.FillRectangle(myGradient, (int)xPos, 25, (int)xPosTo - (int)xPos, (pbRPEHeatMap.Height - 50));
                                                //lData = aData;
                                                lastColour = activeColour;
                                                xPos = xPosTo;
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                        if (xPosTo < pbRPEHeatMap.Width - 24 - xOffsetRight)
                                        {
                                            myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                                       new Point((int)xPos - 1, (this.pbRPEHeatMap.Height - 25)),
                                                       new Point(pbRPEHeatMap.Width - 24, (this.pbRPEHeatMap.Height - 25)),
                                                       lastColour, activeColour
                                                       );
                                            g.FillRectangle(myGradient, (int)xPos, 25, pbRPEHeatMap.Width - (int)xPos - 24 - xOffsetRight, (pbRPEHeatMap.Height - 50));
                                        }
                                        pbRPEHeatMap.Refresh();
                                        /*foreach (FG_DATA fData in fgData)
                                        {
                                            System.Diagnostics.Debug.WriteLine(cCnt.ToString() + " " + fData.d_start.ToLocalTime().ToString() + " Fuel Gauge xPos: " + xPos.ToString() + ", xPosTo: " + xPosTo.ToString() + " Max Pos: " + (pbRPEHeatMap.Width - 25 - xOffsetRight).ToString() + ", Player Load: " + fData.player_load.ToString());
                                        }*/
                                    }
                                }
                                if (list_count > 0)
                                {
                                    if(NodeProfile[currentNodeID - 1].gftType == 0) dataGetImpactByList(currentNodeID, dataGridView3);
                                    //drawChartByList(currentNodeID, 60/*gDataGetSessionSidThreshold(curSessionID, "SessionWireless")*/, chartViewer);
                                    drawChartByList(currentNodeID, NodeProfile[currentNodeID - 1].alarmThres, chartViewer);

                                    //chartRawLinW.Series.Clear();
                                    //chartRawGyroW.Series.Clear();

                                    Impacts_MouseToFirst(sender, dataGridView3, chartRawLinW, chartRawGyroW, pbHead, "EventsWireless", impact, chartViewer);

                                }
                                else
                                {
                                    // Added by Jason Chen for Show, 2014.01.09
                                    chartViewer.Series.Clear();
                                    chartRawLinW.Series.Clear();
                                    chartRawGyroW.Series.Clear();

                                    int rowCount = dataGridView3.Rows.Count;
                                    for (int index = 0; index < rowCount; index++) dataGridView3.Rows.RemoveAt(0);

                                }
                            }
                            catch (Exception e)
                            {
                            }
                            if (false)
                            {
                                try
                                {
                                    /*
                                int restActivity = 0;
                                int moderateActivity = 0;
                                int highActivity = 0;
                                int veryHighActivity = 0;*/
                                    // Draw Activity Pie Chart
                                    int activeSeconds = moderateActivity + highActivity + veryHighActivity;
                                    TimeSpan sesLength = DateTime.UtcNow - lpHub.lpDevices[currentNodeID - 1].firstReceiveTime;
                                    int totalSeconds = (int)restActivity + moderateActivity + highActivity + veryHighActivity;

                                    perfChartRPE.Series.Clear();
                                    perfChartRPE.Legends.Clear();
                                    perfChartRPE.Legends.Add(new Legend("Legend"));
                                    perfChartRPE.Legends["Legend"].DockedToChartArea = perfChartRPE.ChartAreas.FirstOrDefault().Name;
                                    perfChartRPE.Legends["Legend"].Docking = Docking.Bottom;
                                    perfChartRPE.Legends["Legend"].IsDockedInsideChartArea = false;
                                    perfChartRPE.Legends["Legend"].Alignment = StringAlignment.Center;
                                    var series = perfChartRPE.Series.Add("");
                                    series.LabelForeColor = System.Drawing.Color.White;
                                    series.Font = lblFont;
                                    series.ChartType = SeriesChartType.Pie;
                                    double activePercent = 0;
                                    try
                                    {
                                        activePercent = Math.Round((double)(highActivity + veryHighActivity) / totalSeconds * 100, 1);
                                        series.Points.AddXY(0, (int)Math.Round(activePercent));//
                                        series.Points[0].Color = System.Drawing.Color.Red;
                                        series.Points[0].BorderWidth = 2;
                                        series.Points[0].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);
                                        series.Points[0].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(activeSeconds).ToString();
                                        series.Points[0].LegendText = "Active";

                                        activePercent = Math.Round((double)(moderateActivity) / totalSeconds * 100, 1);
                                        series.Points.AddXY(1, (int)Math.Round(activePercent));
                                        series.Points[1].Color = System.Drawing.Color.Yellow;
                                        series.Points[1].BorderWidth = 2;
                                        series.Points[1].BorderColor = System.Drawing.Color.FromArgb(215, 255, 255, 35);
                                        series.Points[1].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(totalSeconds - activeSeconds).ToString();
                                        series.Points[1].LegendText = "Moderate";

                                        activePercent = Math.Round((double)(restActivity) / totalSeconds * 100, 1);
                                        series.Points.AddXY(1, (int)Math.Round(activePercent));
                                        series.Points[2].Color = System.Drawing.Color.LimeGreen;
                                        series.Points[2].BorderWidth = 2;
                                        series.Points[2].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);
                                        series.Points[2].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(totalSeconds - activeSeconds).ToString();
                                        series.Points[2].LegendText = "Rest";
                                    }
                                    catch (Exception e)
                                    {
                                        System.Diagnostics.Debug.WriteLine("error");
                                    }
                                }
                                catch (Exception e)
                                {
                                }
                            }

                        }
                        else
                        {
                            perfChartRPE.Series.Clear();
                        }
                    }
                    else
                    {
                        currentNodeID = args.NodeID;             // Current UID (Node ID)
                        //curSessionID = gDataGetSessionLastSID(currentNodeID);
                        //LastSessionID = curSessionID; //this.NodeProfile[currentNodeID-1]

                        this.lblPlayer.Text = "#" + this.NodeProfile[currentNodeID - 1].playerNum + " " + this.NodeProfile[currentNodeID - 1].firstName + " " + this.NodeProfile[currentNodeID - 1].lastName;
                        this.lblStatus.Text = lpHub.lpDevices[currentNodeID - 1].status + lpHub.lpDevices[currentNodeID - 1].commandStatus.Replace("\n", ", ");
                        TimeSpan recovery = DateTime.UtcNow - lpHub.lpDevices[currentNodeID - 1].lastThresholdTime;
                        string recoveryText = "No Threshold Impacts";
                        if (recovery.TotalDays < 100)
                        {
                            if (recovery.Hours > 0)
                            {
                                recoveryText = recovery.Hours.ToString() + " Hour" + (recovery.Hours > 1 ? "s" : "");

                                if (recovery.Minutes > 0)
                                {
                                    recoveryText += ", " + recovery.Minutes.ToString() + " Minute" + (recovery.Minutes > 1 ? "s" : "");
                                }
                                recoveryText += " Since last Threshold Impact";
                            }
                            else if (recovery.Minutes > 0)
                            {
                                recoveryText = recovery.Minutes.ToString() + " Minute" + (recovery.Minutes > 1 ? "s" : "");

                                if (recovery.Seconds > 0)
                                {
                                    recoveryText += ", " + recovery.Seconds.ToString() + " Second" + (recovery.Seconds > 1 ? "s" : "");
                                }
                                recoveryText += " Since last Threshold Impact";
                            }
                            else if (recovery.Seconds > 0)
                            {
                                recoveryText = recovery.Seconds.ToString() + " Second" + (recovery.Seconds > 1 ? "s" : "");
                                recoveryText += " Since last Threshold Impact";
                            }
                        }
                        this.lblRecovery.Text = recoveryText;

                        string deviceSettings = "Record: " + NodeProfile[currentNodeID - 1].recThres.ToString() + "g, Threshold: " + NodeProfile[currentNodeID - 1].alarmThres.ToString() + "g / 2000°/s";
                        lblDeviceSettings.Text = deviceSettings;
                        double averageG = 0;
                        double averageR = 0;

                        List<double> gForce = new List<double>();
                        List<double> rotR = new List<double>();
                        foreach (IMPACT_DATA impData in Args[currentNodeID - 1].recvFIFO)
                        {
                            gForce.Add(impData.linR);
                            rotR.Add(impData.rotR);
                        }

                        gForce.Sort();
                        rotR.Sort();
                        if (gForce.Count > 0 && rotR.Count > 0)
                        {
                            averageG = Math.Round(gForce.Average(), 1);
                            averageR = Math.Round(rotR.Average(), 1);
                            // Get Median 
                            if (lpHub.lpDevices[currentNodeID - 1].impacts > 0)
                            {
                                if (lpHub.lpDevices[currentNodeID - 1].impacts % 2 == 0)
                                {
                                    medianG = Math.Round((gForce[(lpHub.lpDevices[currentNodeID - 1].impacts / 2)] + gForce[(lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 1]) / 2, 1);
                                    medianR = (int)Math.Round((rotR[(lpHub.lpDevices[currentNodeID - 1].impacts / 2)] + rotR[(lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 1]) / 2, 0);
                                }
                                else
                                {
                                    medianG = gForce[(int)((lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 0.5)];
                                    medianR = (int)Math.Round(rotR[(int)((lpHub.lpDevices[currentNodeID - 1].impacts / 2) - 0.5)], 0);
                                }
                            }
                        }



                        int activeMinutes = (int)Math.Round((double)lpHub.lpDevices[currentNodeID - 1].activeTime / 3000 / 60, 0);
                        if (lpHub.lpDevices[currentNodeID - 1].activeTime > 0)
                        {
                            //this.lblExposure.Text = "Exposure: " + lpHub.lpDevices[currentNodeID - 1].impacts + " Impacts in " + activeMinutes.ToString() + " Minute" + (activeMinutes != 1 ? "s" : "") + " (" + Math.Round((double)lpHub.lpDevices[currentNodeID - 1].impacts / (lpHub.lpDevices[currentNodeID - 1].activeTime / 3000 / 60), 1).ToString() + " Hits/Min)";
                            //this.lblExposure.Text = Math.Round((double)lpHub.lpDevices[currentNodeID - 1].impacts / ((double)lpHub.lpDevices[currentNodeID - 1].activeTime / 3000 / 60), 1).ToString();
                            tileImage(this.tableLayoutPanel14, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, Math.Round((double)lpHub.lpDevices[currentNodeID - 1].impacts / ((double)lpHub.lpDevices[currentNodeID - 1].activeTime / 3000 / 60), 1).ToString());
                        }
                        //this.lblHighestG.Text = Math.Round(lpHub.lpDevices[currentNodeID - 1].gForce, 1).ToString();
                        tileImage(this.tableLayoutPanel12, WirelessGFTViewer.Properties.Resources.impact, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, Math.Round(lpHub.lpDevices[currentNodeID - 1].gForce, 1).ToString());
                        //this.lblHighestR.Text = Math.Round(lpHub.lpDevices[currentNodeID - 1].Rotation, 1).ToString();
                        tileImage(this.tableLayoutPanel13, WirelessGFTViewer.Properties.Resources.rotation, Color.White, Color.Green, 4, this.lblTeamGForce.Font, Math.Round(lpHub.lpDevices[currentNodeID - 1].Rotation, 0).ToString());
                        this.lblThresholdImpacts.Text = lpHub.lpDevices[currentNodeID - 1].thresholdImpacts.ToString();

                        //this.lblThresholdImpacts.Text = (lpHub.lpDevices[currentNodeID - 1].withinThreshold + lpHub.lpDevices[currentNodeID - 1].aboveThreshold).ToString();
                        this.lblThresholdWithinAbove.Text = lpHub.lpDevices[currentNodeID - 1].withinThreshold + " Wtihin, " + lpHub.lpDevices[currentNodeID - 1].aboveThreshold + " Above";
                        this.lblThresholdTotal.Text = lpHub.lpDevices[currentNodeID - 1].impacts + " Impacts Total, " + (lpHub.lpDevices[currentNodeID - 1].impacts > 0 ? Math.Round((double)lpHub.lpDevices[currentNodeID - 1].aboveThreshold / lpHub.lpDevices[currentNodeID - 1].impacts * 100).ToString() : "0") + "% Above Threshold";
                        tileImage(this.tableLayoutPanel9, WirelessGFTViewer.Properties.Resources.hit, Color.White, Color.FromArgb(255, 195, 0, 0), 4, this.lblTeamGForce.Font, (lpHub.lpDevices[currentNodeID - 1].withinThreshold + lpHub.lpDevices[currentNodeID - 1].aboveThreshold).ToString());

                        this.lblGForceAveMed.Text = "Average: " + averageG + "g, Median: " + medianG + "g";
                        this.lblRotationAveMed.Text = "Average: " + averageR + "°/s, Median: " + medianR + "°/s";

                        pbBatStat.Image = lpHub.lpDevices[currentNodeID - 1].batImg;

                        int list_count = Args[currentNodeID - 1].recvFIFO.Count;


                        // Pie Charts

                        Font lblFont = new Font(locationChartPlayer.Font.FontFamily, 9, FontStyle.Bold);

                        // Draw Impact Location Pie Chart
                        int impactLeft = 0;
                        int impactRight = 0;
                        int impactTop = 0;
                        int impactBottom = 0;
                        int impactFront = 0;
                        int impactBack = 0;
                        foreach (IMPACT_DATA iData in Args[currentNodeID - 1].recvFIFO)
                        {
                            /* No longer consider bottom impacts, simply assign based on azimuth
                            else if (iData.elevation <= -45)
                                impactBottom++; // = "BOTTOM";*/
                            if (iData.elevation >= 45)
                                impactTop++; // = "TOP";
                            else if (iData.azimuth <= 225 && iData.azimuth >= 135)
                                impactBack++;// = "BACK";
                            else if (iData.azimuth <= 45 || iData.azimuth >= 315)
                                impactFront++;// = "FRONT";
                            else if (iData.azimuth >= 45 && iData.azimuth <= 135)
                                impactRight++;// = "RIGHT";
                            else
                                impactLeft++;// = "LEFT";
                        }
                        try
                        {
                            locationChartPlayer.Series.Clear();
                            Series sLocations = locationChartPlayer.Series.Add("");
                            sLocations.LabelForeColor = System.Drawing.Color.White;
                            sLocations.Font = lblFont;
                            sLocations.ChartType = SeriesChartType.Pie;

                            sLocations.Points.AddXY(0, impactRight);
                            if (impactRight > 0) sLocations.Points[0].AxisLabel = impactRight.ToString();
                            sLocations.Points[0].LegendText = "Right";//: " + impactRight.ToString();
                            sLocations.Points[0].Color = System.Drawing.Color.Red;
                            sLocations.Points[0].BorderWidth = 2;
                            sLocations.Points[0].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);

                            sLocations.Points.AddXY(0, impactLeft);
                            if (impactLeft > 0) sLocations.Points[1].AxisLabel = impactLeft.ToString();
                            sLocations.Points[1].LegendText = "Left";//: " + impactLeft.ToString();
                            sLocations.Points[1].Color = System.Drawing.Color.FromArgb(235, 95, 125, 0);
                            sLocations.Points[1].BorderWidth = 2;
                            sLocations.Points[1].BorderColor = System.Drawing.Color.FromArgb(235, 85, 105, 0);

                            sLocations.Points.AddXY(0, impactTop);
                            if (impactTop > 0) sLocations.Points[2].AxisLabel = impactTop.ToString();
                            sLocations.Points[2].LegendText = "Top";//: " + impactTop.ToString();
                            sLocations.Points[2].Color = System.Drawing.Color.LimeGreen;
                            sLocations.Points[2].BorderWidth = 2;
                            sLocations.Points[2].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);

                            sLocations.Points.AddXY(0, impactBottom);
                            if (impactBottom > 0) sLocations.Points[3].AxisLabel = impactBottom.ToString();
                            sLocations.Points[3].LegendText = "Bottom";//: " + impactBottom.ToString();
                            sLocations.Points[3].Color = System.Drawing.Color.FromArgb(235, 105, 85, 255);
                            sLocations.Points[3].BorderWidth = 2;
                            sLocations.Points[3].BorderColor = System.Drawing.Color.FromArgb(235, 95, 75, 235);

                            sLocations.Points.AddXY(0, impactFront);
                            if (impactFront > 0) sLocations.Points[4].AxisLabel = impactFront.ToString();
                            sLocations.Points[4].LegendText = "Front";//: " + impactFront.ToString();
                            sLocations.Points[4].Color = System.Drawing.Color.FromArgb(235, 0, 155, 155);
                            sLocations.Points[4].BorderWidth = 2;
                            sLocations.Points[4].BorderColor = System.Drawing.Color.FromArgb(235, 0, 135, 135);

                            sLocations.Points.AddXY(0, impactBack);
                            if (impactBack > 0) sLocations.Points[5].AxisLabel = impactBack.ToString();
                            sLocations.Points[5].LegendText = "Back";//: " + impactBack.ToString();
                            sLocations.Points[5].Color = System.Drawing.Color.FromArgb(235, 255, 175, 0);
                            sLocations.Points[5].BorderWidth = 2;
                            sLocations.Points[5].BorderColor = System.Drawing.Color.FromArgb(235, 235, 155, 0);
                        }
                        catch (Exception e)
                        {
                        }

                        // Draw Activity Pie Chart
                        int activeSeconds = (int)Math.Round((double)lpHub.lpDevices[currentNodeID - 1].activeTime / 3000, 0);
                        TimeSpan sesLength = DateTime.UtcNow - lpHub.lpDevices[currentNodeID - 1].firstReceiveTime;
                        int totalSeconds = (int)sesLength.TotalSeconds;

                        activityChartPlayer.Series.Clear();
                        var series = activityChartPlayer.Series.Add("");
                        series.LabelForeColor = System.Drawing.Color.White;
                        series.Font = lblFont;
                        series.ChartType = SeriesChartType.Pie;
                        double activePercent = 0;
                        try
                        {
                            activePercent = Math.Round((double)activeSeconds / totalSeconds * 100, 1);
                            series.Points.AddXY(0, (int)Math.Round(activePercent));//
                            series.Points[0].Color = System.Drawing.Color.Red;
                            series.Points[0].BorderWidth = 2;
                            series.Points[0].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);
                            series.Points[0].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(activeSeconds).ToString();
                            series.Points[0].LegendText = "Active Time";
                            activePercent = Math.Round((double)(totalSeconds - activeSeconds) / totalSeconds * 100, 1);
                            series.Points.AddXY(1, (int)Math.Round(activePercent));
                            series.Points[1].Color = System.Drawing.Color.LimeGreen;
                            series.Points[1].BorderWidth = 2;
                            series.Points[1].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);
                            series.Points[1].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(totalSeconds - activeSeconds).ToString();
                            series.Points[1].LegendText = "Rest Time";
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Debug.WriteLine("error");
                        }

                        // Fuel Gauge
                        int restActivity = 0;
                        int moderateActivity = 0;
                        int highActivity = 0;
                        int veryHighActivity = 0;

                        // Draw Fuel Tank
                        if (pbHeatMap.Image == null || pbHeatMap.Image.Width != pbHeatMap.Width || pbHeatMap.Image.Height != pbHeatMap.Height)
                        {
                            pbHeatMap.Image = new Bitmap(pbHeatMap.Width, pbHeatMap.Height);
                        }

                        ACTIVITY_DATA lData = new ACTIVITY_DATA();
                        lData.activeTime = 0;
                        lData.reportTime = lpHub.lpDevices[currentNodeID - 1].firstReceiveTime;

                        using (Graphics g = Graphics.FromImage(pbHeatMap.Image))
                        {
                            SolidBrush myBrush = new SolidBrush(Color.White);
                            g.FillRectangle(myBrush, 0, 0, pbHeatMap.Width, pbHeatMap.Height);
                            Pen pen = new Pen(Color.Black);
                            g.DrawLine(pen, 24, 24, pbHeatMap.Width - 24, 24);
                            g.DrawLine(pen, 24, pbHeatMap.Height - 25, pbHeatMap.Width - 24, pbHeatMap.Height - 25);
                            g.DrawLine(pen, 24, 24, 24, pbHeatMap.Height - 25);
                            g.DrawLine(pen, pbHeatMap.Width - 24, 24, pbHeatMap.Width - 24, pbHeatMap.Height - 25);

                            TimeSpan t = TimeSpan.FromSeconds(0);
                            int xPos = 0;
                            int inc = (int)(totalSeconds / 60) * 60 / 5;
                            if (inc < 60) inc = 60;
                            SolidBrush myBrushB = new SolidBrush(Color.Black);
                            for (int i = 0; i <= totalSeconds; i += inc)
                            {
                                xPos = 24 + (int)Math.Round(((double)i / totalSeconds * (pbHeatMap.Width - 48)));
                                g.DrawLine(pen, xPos, pbHeatMap.Height - 24, xPos, pbHeatMap.Height - 20);
                                t = TimeSpan.FromSeconds(i);
                                g.DrawString(string.Format("{0:D2}:{1:D2}", t.Hours, t.Minutes), lblRotationAveMed.Font, myBrushB, new Point(xPos - 15, pbHeatMap.Height - 18));
                                g.DrawString("Fuel Tank", this.label4.Font, myBrushB, new Point(4, 2));

                                g.FillRectangle(myBrushB, 120, 6, 16, 12);
                                g.FillRectangle(myBrushB, 200, 6, 16, 12);
                                g.FillRectangle(myBrushB, 280, 6, 16, 12);

                                g.FillRectangle(new SolidBrush(Color.LimeGreen), 121, 7, 14, 10);
                                g.FillRectangle(new SolidBrush(Color.Yellow), 201, 7, 14, 10);
                                g.FillRectangle(new SolidBrush(Color.Red), 281, 7, 14, 10);

                                g.DrawString("Rest", this.lblRotationAveMed.Font, myBrushB, new Point(135, 6));
                                g.DrawString("Moderate", this.lblRotationAveMed.Font, myBrushB, new Point(215, 6));
                                g.DrawString("Active", this.lblRotationAveMed.Font, myBrushB, new Point(295, 6));
                            }
                        }

                        //ACTIVITY_DATA rData = lpHub.lpDevices[currentNodeID - 1].activeTimeList.Last(); // Most recent activity Data
                        if (lpHub.lpDevices[currentNodeID - 1].activeTimeList.Count > 0)
                        {
                            ACTIVITY_DATA rData = lpHub.lpDevices[currentNodeID - 1].activeTimeList.Last(); // Most recent activity Data
                            TimeSpan activelength = rData.reportTime - lData.reportTime;
                            int totalTime = (int)activelength.TotalSeconds;

                            System.Drawing.Drawing2D.LinearGradientBrush myGradient;

                            int deltaA;
                            TimeSpan deltaT;
                            double activePct;
                            Color lastColour = Color.LimeGreen;
                            Color activeColour = Color.LimeGreen;

                            int xPos = 25;
                            int xPosTo = 25;

                            //Pen pen;
                            using (Graphics g = Graphics.FromImage(pbHeatMap.Image))
                            {
                                foreach (ACTIVITY_DATA aData in lpHub.lpDevices[currentNodeID - 1].activeTimeList)
                                {
                                    // Determine the colour.. 0-25% Activity = Rest, 25-60% = Moderate, 60%+ = High Activity
                                    // What's our active time delta?
                                    deltaA = aData.activeTime - lData.activeTime;
                                    deltaT = aData.reportTime - lData.reportTime;
                                    xPosTo = xPos + (int)Math.Round((deltaT.TotalSeconds / activelength.TotalSeconds) * (pbHeatMap.Width - 50));
                                    if (xPosTo > (pbHeatMap.Width - 25)) xPosTo = (pbHeatMap.Width - 25);
                                    activePct = (deltaA / 3000) / deltaT.TotalSeconds;
                                    if (activePct < 0.25)
                                    {
                                        activeColour = Color.LimeGreen;
                                        restActivity += (int)deltaT.TotalSeconds;
                                    }
                                    else if (activePct < 0.6)
                                    {
                                        activeColour = Color.Yellow;
                                        moderateActivity += (int)deltaT.TotalSeconds;
                                    }
                                    else
                                    {
                                        activeColour = Color.Red;
                                        if (activePct < 0.8)
                                            highActivity += (int)deltaT.TotalSeconds;
                                        else
                                            veryHighActivity += (int)deltaT.TotalSeconds;
                                    }
                                    if (xPosTo > xPos)
                                    {
                                        myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                            new Point(xPos - 1, (this.pbHeatMap.Height - 25)),
                                            new Point(xPosTo, (this.pbHeatMap.Height - 25)),
                                            lastColour, activeColour
                                            );
                                        //pen = new Pen(myGradient);
                                        g.FillRectangle(myGradient, xPos, 25, xPosTo - xPos, (pbHeatMap.Height - 50));
                                        lData = aData;
                                        lastColour = activeColour;
                                        xPos = xPosTo;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                if (xPosTo < pbHeatMap.Width - 24)
                                {
                                    myGradient = new System.Drawing.Drawing2D.LinearGradientBrush(
                                               new Point(xPos - 1, (this.pbHeatMap.Height - 25)),
                                               new Point(pbHeatMap.Width - 24, (this.pbHeatMap.Height - 25)),
                                               lastColour, activeColour
                                               );
                                    g.FillRectangle(myGradient, xPos, 25, pbHeatMap.Width - xPos - 24, (pbHeatMap.Height - 50));
                                }
                                pbHeatMap.Refresh();

                            }
                        }
                        if (list_count > 0)
                        {
                            dataGetImpactByList(currentNodeID, dataGridView3);
                            //drawChartByList(currentNodeID, 60/*gDataGetSessionSidThreshold(curSessionID, "SessionWireless")*/, chartViewer);
                            drawChartByList(currentNodeID, NodeProfile[currentNodeID - 1].alarmThres, chartViewer);

                            //chartRawLinW.Series.Clear();
                            //chartRawGyroW.Series.Clear();

                            Impacts_MouseToFirst(sender, dataGridView3, chartRawLinW, chartRawGyroW, pbHead, "EventsWireless", impact, chartViewer);

                        }
                        else
                        {
                            // Added by Jason Chen for Show, 2014.01.09
                            chartViewer.Series.Clear();
                            chartRawLinW.Series.Clear();
                            chartRawGyroW.Series.Clear();

                            int rowCount = dataGridView3.Rows.Count;
                            for (int index = 0; index < rowCount; index++) dataGridView3.Rows.RemoveAt(0);

                        }

                        // Draw Fuel Gauge Pie Chart

                        ActivePct.Series.Clear();
                        var aPSeries = ActivePct.Series.Add("");
                        aPSeries.LabelForeColor = System.Drawing.Color.White;
                        aPSeries.Font = lblFont;
                        aPSeries.ChartType = SeriesChartType.Pie;
                        try
                        {
                            activePercent = Math.Round((double)restActivity / totalSeconds * 100, 1);
                            aPSeries.Points.AddXY(0, (int)Math.Round(activePercent));//
                            aPSeries.Points[0].Color = System.Drawing.Color.LimeGreen;
                            aPSeries.Points[0].BorderWidth = 2;
                            aPSeries.Points[0].BorderColor = System.Drawing.Color.FromArgb(235, 35, 175, 35);
                            aPSeries.Points[0].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(activeSeconds).ToString();
                            aPSeries.Points[0].LegendText = "Low";

                            activePercent = Math.Round((double)moderateActivity / totalSeconds * 100, 1);
                            aPSeries.Points.AddXY(1, (int)Math.Round(activePercent));
                            aPSeries.Points[1].Color = System.Drawing.Color.Yellow;
                            aPSeries.Points[1].BorderWidth = 2;
                            aPSeries.Points[1].BorderColor = System.Drawing.Color.FromArgb(235, 215, 225, 0);
                            aPSeries.Points[1].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(totalSeconds - activeSeconds).ToString();
                            aPSeries.Points[1].LabelForeColor = Color.Black;
                            aPSeries.Points[1].LegendText = "Medium";

                            activePercent = Math.Round((double)highActivity / totalSeconds * 100, 1);
                            aPSeries.Points.AddXY(1, (int)Math.Round(activePercent));
                            aPSeries.Points[2].Color = System.Drawing.Color.Orange;
                            aPSeries.Points[2].BorderWidth = 2;
                            aPSeries.Points[2].BorderColor = System.Drawing.Color.FromArgb(235, 235, 155, 0);
                            aPSeries.Points[2].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(totalSeconds - activeSeconds).ToString();
                            aPSeries.Points[2].LabelForeColor = Color.Black;
                            aPSeries.Points[2].LegendText = "High";

                            activePercent = Math.Round((double)veryHighActivity / totalSeconds * 100, 1);
                            aPSeries.Points.AddXY(1, (int)Math.Round(activePercent));
                            aPSeries.Points[3].Color = System.Drawing.Color.Red;
                            aPSeries.Points[3].BorderWidth = 2;
                            aPSeries.Points[3].BorderColor = System.Drawing.Color.FromArgb(215, 215, 0, 0);
                            aPSeries.Points[3].AxisLabel = activePercent.ToString() + "%";// TimeSpan.FromSeconds(totalSeconds - activeSeconds).ToString();
                            aPSeries.Points[3].LegendText = "Highest";
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Debug.WriteLine("error");
                        }
                    }
                }
            }
            else
            {
                drawTeamChartByList(chartViewer);
                calcTeamStats();
            }
#else
            //Int32 index = 0;
            if (args.selectedRow >= 0)
            {
                if ((args.NodeID <= 0) || (args.NodeID > NUM_NODES))
                    return;

                currentNodeID = args.NodeID;             // Current UID (Node ID)
                //curSessionID = gDataGetSessionLastSID(currentNodeID);
                //LastSessionID = curSessionID;

                int list_count = Args[currentNodeID - 1].recvFIFO.Count;
                if (list_count > 0)
                {
                    dataGetImpactByList(currentNodeID, dataGridView3);
                    //drawChartByList(currentNodeID, 60/*gDataGetSessionSidThreshold(curSessionID, "SessionWireless")*/, chartViewer);
                    drawChartByList(currentNodeID, NodeProfile[currentNodeID - 1].alarmThres, chartViewer);

                    chartRawLinW.Series.Clear();
                    chartRawGyroW.Series.Clear();

                    Impacts_MouseToFirst(sender, dataGridView3, chartRawLinW, chartRawGyroW, pictureBoxHeadW, "EventsWireless", impact);

                }
                else
                {
                    // Added by Jason Chen for Show, 2014.01.09
                    chartViewer.Series.Clear();
                    chartRawLinW.Series.Clear();
                    chartRawGyroW.Series.Clear();

                    int rowCount = dataGridView3.Rows.Count;
                    for (int index = 0; index < rowCount; index++) dataGridView3.Rows.RemoveAt(0);

                }
            }
#endif
        }
#endif

        private void tileImage(TableLayoutPanel tbl, Image graphic, Color fillColor, Color strokeColor, int strokeWidth, Font fontStyle, string text)
        {
            if (tbl.Width > 0 && tbl.Height > 0)
            {
                Bitmap bg = new Bitmap(tbl.Width, tbl.Height);

                Rectangle src = new Rectangle(0, 0, graphic.Width, graphic.Height);
                Rectangle dst = new Rectangle(bg.Width - graphic.Width - 10, (int)(bg.Height - graphic.Height) / 2 + 5, graphic.Width, graphic.Height);
                Rectangle r = new Rectangle(bg.Width - graphic.Width - 20, (int)(bg.Height - graphic.Height) / 2 + 7, graphic.Width + 20, graphic.Height);

                Graphics g = Graphics.FromImage(bg);
                g.DrawImage(graphic, dst, src, GraphicsUnit.Pixel);
                //Graphics g = Graphics.FromImage(graphic);
                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                SolidBrush b = new SolidBrush(fillColor);
                Pen p = new Pen(strokeColor, strokeWidth);
                p.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;
                System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
                gp.AddString(text, fontStyle.FontFamily, (int)fontStyle.Style, fontStyle.Size, r, sf);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

                g.DrawPath(p, gp);
                g.FillPath(b, gp);

                gp.Dispose();
                b.Dispose();
                p.Dispose();
                fontStyle.Dispose();
                sf.Dispose();
                g.Dispose();
                tbl.BackgroundImageLayout = ImageLayout.Center;
                tbl.BackgroundImage = bg;
                //return graphic;
            }
        }

        private int getUploadNodeNum()
        {
            int i, uploadNum=0;
            for(i=0;i<NUM_NODES;i++)
                if (NodeProfile[i].uploadNum_Flag == 1 || lpHub.lpDevices[i].mode == 4) uploadNum++;//=NodeProfile[i].uploadNum_Flag;
                
            return uploadNum;
        }

        private void uploadProcess(int node, bool USB_or_BLE)
        {
            if (autoUpload_enable.Checked)
            {
                if (NodeProfile[node - 1].usbMode == 0x81)
                {
#if true
                    if ((usbUpload_Enable.Checked) && (NodeProfile[node - 1].uploadFlag))
                    //if (usbUpload_Enable.Checked)
                    {// Only download if there is data AND we have received the calibration data
                        if ((NodeProfile[node - 1].impactNum > 0 || NodeProfile[node - 1].sessionNum > 0) && (NodeProfile[node - 1].iLocation[0] != 0 || NodeProfile[node - 1].iLocation[1] != 0 || NodeProfile[node - 1].iLocation[2] != 0 || NodeProfile[node - 1].iLocation[3] != 0))
                        {
                            if (!NodeProfile[node - 1].deleteFlag)
                            {
                                NodeProfile[node - 1].upload_req_flag = true;                               // Added by Jason Chen, 201403.18

                                uploadNum_limit = getUploadNodeNum();
                                if (uploadNum_limit < NUM_UPLOAD_LIMIT)
                                {
                                    NodeProfile[node - 1].recvSessionStarted = false;
                                    NodeProfile[node - 1].recvSessionClosed = true;
                                    NodeProfile[node - 1].uploadNum_Flag = 1;
                                    NodeProfile[node - 1].impactCount = 0;
                                    NodeProfile[node - 1].sessionCount = 0;
                                    Args[node - 1].mHitFlag = false;
                                    lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;

                                    Thread.Sleep(75);
                                    if (!USB_or_BLE)
                                        mSendMsgCMD((byte)node, 15); // request upload shole impact data
                                    else
                                        mSendMsgBleCMD((byte)node, 15);     // BLE Added
                                }
                                else
                                {

                                    if (NodeProfile[node - 1].uploadNum_Flag == 1)    // Re-Upload again
                                    {
                                        if (uploadNum_limit <= NUM_UPLOAD_LIMIT)
                                        {
                                            //NodeProfile[node - 1].uplolaNum_Flag = 0;
                                            //lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uplolaNum_Flag;

                                            NodeProfile[node - 1].recvSessionStarted = false;
                                            NodeProfile[node - 1].recvSessionClosed  = true;
                                            NodeProfile[node - 1].impactCount = 0;
                                            NodeProfile[node - 1].sessionCount = 0;

                                            Thread.Sleep(75);
                                            Args[node - 1].mHitFlag = false;
                                            if (!USB_or_BLE)
                                                mSendMsgCMD((byte)node, 15); // request upload shole impact data
                                            else
                                                mSendMsgBleCMD((byte)node, 15);     // BLE Added
                                        }
                                        else
                                        {
                                            NodeProfile[node - 1].uploadNum_Flag = 0;
                                            lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                        }
                                    }

                                }
                            }
                            else
                            {
                                //if (TooltabPage16Exist)
                                //{
                                    //m_Edit_Immediate.AppendText("Send a delete CMD to Node " + node.ToString("D2") + " again!" + "\r\n");
                                //}

                                NodeProfile[node - 1].deleteFlag = true;
                                if (!USB_or_BLE)
                                    mSendMsgCMD((byte)node, 191);         // Delete GFT data, 0xEE
                                else
                                    mSendMsgBleCMD((byte)node, 191);     // BLE Added
                                lpHub.lpDevices[node - 1].keepPowerOnFlag = false;

                                lpHub.lpDevices[node - 1].mode = 3;     // Erase GFT Flash
                                NodeProfile[node - 1].uploadNum_Flag = 0;
                                lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                gDataWSessionMarkErased(NodeProfile[node - 1].GID); // 1

                            }
                        }
                        else
                        {
                            NodeProfile[node - 1].deleteFlag = false;
                            NodeProfile[node - 1].recvSessionID = 0;
                            NodeProfile[node - 1].uploadNum_Flag = 0;
                            lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;

                            NodeProfile[node - 1].upload_req_flag = false;                               // Added by Jason Chen, 201403.18
                        }
                    }
                    else
                    {
                        if(NodeProfile[node - 1].deleteFlag == true)
                        {
                            if (!USB_or_BLE)
                                mSendMsgCMD((byte)node, 191);  // Delete GFT data, 0xBF
                            else
                                mSendMsgBleCMD((byte)node, 191);
                            lpHub.lpDevices[node - 1].keepPowerOnFlag = false;

                            lpHub.lpDevices[node - 1].mode = 3;     // Erase GFT Flash
                            NodeProfile[node - 1].uploadNum_Flag = 0;
                            lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                            gDataWSessionMarkErased(NodeProfile[node - 1].GID); // 2
                        }
                    }
#endif
                }
                else
                {
                    if (NodeProfile[node - 1].impactNum == 0 && NodeProfile[node - 1].sessionNum == 0 && NodeProfile[node - 1].uploadFlag)
                    {
                        NodeProfile[node - 1].deleteFlag = false;
                        NodeProfile[node - 1].recvSessionID = 0;

                        NodeProfile[node - 1].uploadFlag = false;
                        NodeProfile[node - 1].uploadNum_Flag = 0;
                        lpHub.lpDevices[node - 1].upldFlag = NodeProfile[node - 1].uploadFlag;

                        NodeProfile[node - 1].upload_req_flag = false;                                  // Added by Jason Chen, 201403.18
                        //mSendMsg((byte)node, 18);                                                       // recover gft long sleep period, 2014.04.10

                        lpHub.lpDevices[node - 1].alarm = false;

                        NodeProfile[node - 1].deleteFlag = true;
                        if (!USB_or_BLE)
                            mSendMsgCMD((byte)node, 191);  // Delete GFT data, 0xBF
                        else
                            mSendMsgBleCMD((byte)node, 191);
                        lpHub.lpDevices[node - 1].keepPowerOnFlag = false;

                        lpHub.lpDevices[node - 1].mode = 3;     // Erase GFT Flash
                        NodeProfile[node - 1].uploadNum_Flag = 0;
                        lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                        gDataWSessionMarkErased(NodeProfile[node - 1].GID); // Mark the stored session data as erased on the device - this allows the upload routine to capture it. // 3
                    }
                    else
                    {

                        if (!NodeProfile[node - 1].deleteFlag)
                        {
                            if (NodeProfile[node - 1].uploadFlag)
                            {
                                NodeProfile[node - 1].upload_req_flag = true;                               // Added by Jason Chen, 201403.18

                                uploadNum_limit = getUploadNodeNum();
                                if (uploadNum_limit < NUM_UPLOAD_LIMIT)
                                {
                                    NodeProfile[node - 1].recvSessionStarted = false;
                                    NodeProfile[node - 1].recvSessionClosed = true;
                                    NodeProfile[node - 1].uploadNum_Flag = 1;
                                    NodeProfile[node - 1].impactCount = 0;
                                    NodeProfile[node - 1].sessionCount = 0;
                                    Args[node - 1].mHitFlag = false;
                                    lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;

                                    Thread.Sleep(75);
                                    if (!USB_or_BLE)
                                        mSendMsgCMD((byte)node, 16); // request upload shole impact data
                                    else
                                        mSendMsgBleCMD((byte)node, 16);
                                }
                                else
                                {
                                    if (NodeProfile[node - 1].uploadNum_Flag == 1)    // Re-Upload again
                                    {

                                        //NodeProfile[node - 1].uplolaNum_Flag = 0;
                                        //lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uplolaNum_Flag;
                                        if (uploadNum_limit <= NUM_UPLOAD_LIMIT)
                                        {
                                            NodeProfile[node - 1].impactCount = 0;
                                            NodeProfile[node - 1].sessionCount = 0;
                                            Args[node - 1].mHitFlag = false;

                                            if (NodeProfile[node - 1].uploadFlag)
                                            {
                                                NodeProfile[node - 1].recvSessionStarted = false;
                                                NodeProfile[node - 1].recvSessionClosed = true;
                                                NodeProfile[node - 1].impactCount = 0;
                                                NodeProfile[node - 1].sessionCount = 0;
                                                Thread.Sleep(75);
                                                if (!USB_or_BLE)
                                                    mSendMsgCMD((byte)node, 16); // request upload whole impact data
                                                else
                                                    mSendMsgBleCMD((byte)node, 16);
                                            }
                                            else
                                            {
                                                NodeProfile[node - 1].uploadNum_Flag = 0;
                                                lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                            }
                                        }
                                        else
                                        {
                                            NodeProfile[node - 1].uploadNum_Flag = 0;
                                            lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                        }

                                    }
                                    else
                                    {
                                        if (!USB_or_BLE)
                                            mSendMsgCMD((byte)node, 19);               // make gft to prepare upload, 2014.04.09 (short sleep period)
                                        else
                                            mSendMsgBleCMD((byte)node, 19);
                                    }
                                }
                            }
                            else
                            {
                                //mSendMsg((byte)node, 18);                                                       // recover gft long sleep period, 2014.04.10
                            }
                        }
                        else
                        {
                            //if (TooltabPage16Exist)
                            //{
                                //m_Edit_Immediate.AppendText("Send a delete CMD to Node " + node.ToString("D2") + " again!" + "\r\n");
                            //}

                            NodeProfile[node - 1].deleteFlag = true;
                            if (!USB_or_BLE)
                                mSendMsgCMD((byte)node, 191); // Delete GFT data, 0xBF
                            else
                                mSendMsgBleCMD((byte)node, 191);
                            lpHub.lpDevices[node - 1].keepPowerOnFlag = false;

                            lpHub.lpDevices[node - 1].mode = 3;     // Erase GFT Flash
                            NodeProfile[node - 1].uploadNum_Flag = 0;
                            lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                            gDataWSessionMarkErased(NodeProfile[node - 1].GID); // 4
                        }
                    }
                }
            }
        }
        private void gDataWSessionRemoveIncomplete()
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    int num = 5;
                    con.Open();
                    string query = "DELETE FROM EventsWireless WHERE SID IN (SELECT SID FROM SessionWireless WHERE ERASED='N')"; //IMPACT > 0 AND 
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        num = com.ExecuteNonQuery();
                    }
                    query = "DELETE FROM SessionWireless WHERE ERASED='N'"; //IMPACT > 0 AND 
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                Console.WriteLine(me.ToString());
            }
        }
        

        private void gDataWSessionRemoveIncomplete(string gid)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    int num = 5;
                    con.Open();
                    string query = "DELETE FROM EventsWireless WHERE SID IN (SELECT SID FROM SessionWireless WHERE ERASED='N' AND GID = @GID)"; // IMPACT > 0 
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        num = com.ExecuteNonQuery();
                    }
                    query = "DELETE FROM SessionWireless WHERE ERASED='N' AND GID = @GID";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                Console.WriteLine(me.ToString());
            }
        }
        public static void gDataWSessionMarkErased(string gid)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    int num = 5;
                    string query = "UPDATE SessionWireless SET ERASED='Y'"
                                 + " WHERE (GID = @GID)";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);        // Added by jason Chen according Jerad's requirement, 2014.06.16
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
                //return; // Don't remove any for now.. Testing purposes
                // Remove any sessions less than 5 minutes that do not have any impacts for this device
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    int num = 5;
                    // Verify no impacts in session by cross referencing the EventsWireless table. This is safer. Even if the session isn't properly post processed, we will not lose impacts
                     string query = "DELETE FROM SessionWireless WHERE SID NOT IN (SELECT SID FROM EventsWireless WHERE GID = @GID) AND IMPACT = 0 AND DURATION < 5 AND GID = @GID";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                Console.WriteLine(me.ToString());
            }
        }
        public static void gDataWPerfSessionMarkErased(string gid)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    int num = 5;
                    string query = "UPDATE PerfSessionWireless SET ERASED='Y'"
                                 + " WHERE (GID = @GID)";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);        // Added by jason Chen according Jerad's requirement, 2014.06.16
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    int num = 5;
                    // Verify no impacts in session by cross referencing the EventsWireless table. This is safer. Even if the session isn't properly post processed, we will not lose impacts
                    string query = "DELETE FROM PerfSessionWireless WHERE SID NOT IN (SELECT SID FROM PerfWireless WHERE GID = @GID) AND IMPACT = 0 AND DURATION < 5 AND GID = @GID";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                Console.WriteLine(me.ToString());
            }
        }
        private void PowerOffProcess(int node)
        {

            if (getGftWireless(node)) // Update
            {
                //NodeProfile[node - 1].GID = gidToString(mgid);
                //if ((NodeControl[node - 1].m_PWR) && (lpHub.lpDevices[node - 1].live))
                //{

                //}


                lpHub.lpDevices[node - 1].playerNumer = NodeProfile[node - 1].playerNum;
            }
            else//new NodeID
            {
                NodeProfile[node - 1].nodeID = node;
                NodeProfile[node - 1].playerNum = 0;
                NodeProfile[node - 1].recThres = 10;
                NodeProfile[node - 1].alarmThres = 60;
                NodeProfile[node - 1].firstName = "No name";
                NodeProfile[node - 1].lastName = "GForceTracker";
                setGftWirelessNode(NodeProfile[node - 1], true);

                lpHub.lpDevices[node - 1].playerNumer = NodeProfile[node - 1].playerNum;
            }
        }

        private void sendSyncTime(int node)
        {
            if (initiating_time_sync == false && timeSync_limit < NUM_TIME_SYNC_LIMIT && uploadNum_limit == 0) // Don't try to time sync while other devics are downloading, or too many are syncing - could cause too much hub traffic
            {
                initiating_time_sync = true;
                //return;
                System.Diagnostics.Debug.WriteLine("Sync Time " + node);
                if ((DateTime.UtcNow.Second >= 0) && (DateTime.UtcNow.Second <= 60))
                {
                    if (mSendMsgCMD_Queue((byte)node, 0xD2, USB_OR_BLE))
                    {
                        timeSync_limit++;
                        NodeProfile[node - 1].syncTimeSentAt = DateTime.UtcNow;
                        NodeProfile[node - 1].syncTimeFlag = false;
                    }
                }
                initiating_time_sync = false;
            }
        }

        private void sendPowerOff(int node)
        {
            System.Diagnostics.Debug.WriteLine("Power Off " + node);
            if (queuePowerOff(node))
            {
                lpHub.lpDevices[node - 1].powerOffFlag = false;//22
                lpHub.lpDevices[node - 1].keepPowerOnFlag = false;

                //NodeControl[node - 1].setChkPowerOff(false);
            }
        }
        private void sendPowerOn(int node)
        {
            System.Diagnostics.Debug.WriteLine("Power On " + node);
            if (mSendMsgCMD_Queue((byte)node, 23, USB_OR_BLE)) //0x17
            {
                lpHub.lpDevices[node - 1].powerOnFlag = false;//22

                //NodeControl[node - 1].setChkPowerOff(false);
            }
        }
        private void sendGetImpacts(int node)
        {
            System.Diagnostics.Debug.WriteLine("View Impacts " + node);
            if (mSendMsgCMD_Queue((byte)node, 0xC0, USB_OR_BLE))
            {
                lpHub.lpDevices[node - 1].getImpactsFlag = false; // get all impacts, do not download them...
                //NodeControl[node - 1].setChkGetImpacts(false);
            }
        }
        private void sendStopRecording(int node)
        {
            if (lpHub.lpDevices[node - 1].stopRecording)
            {
                lpHub.lpDevices[node - 1].stopRecordingFlag = false;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Stop Recording " + node);
                mSendMsgCMD_Queue((byte)node, 0x90, USB_OR_BLE);
            }
        }
        private void sendStartRecording(int node)
        {
            if (lpHub.lpDevices[node - 1].stopRecording)
            {
                System.Diagnostics.Debug.WriteLine("Start Recording " + node);
                mSendMsgCMD_Queue((byte)node, 0x91, USB_OR_BLE);
            }
            else
            {
                lpHub.lpDevices[node - 1].startRecordingFlag = false;
            }
        }

        private void sendUnbind(int node)
        {
            System.Diagnostics.Debug.WriteLine("Unbind " + node);
            if (mSendMsgCMD_Queue((byte)node, 0x11, USB_OR_BLE))
            {
                NodeProfile[node - 1].unbindFlag = false;
            }
        }
        private void cmdProcess(int node, bool USB_or_BLE)
        {
            System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Device Mode " + lpHub.lpDevices[node - 1].mode);
            if (lpHub.lpDevices[node - 1].mode > 0)
            {
                if (timeSync_limit <= 50)
                {
                    uploadProcess(node, USB_or_BLE); // First process upload requests
                }
                bool dontSyncNow = false;
                    if (NodeProfile[node - 1].syncTimeSentAt > DateTime.MinValue)
                    {
                        if (NodeProfile[node - 1].syncTimeSentAt < DateTime.UtcNow.AddSeconds(-5))
                        {

                            if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = DateTime.Now + " Found Time Sync for node " + node + " has Expired!!!! Re-queuing!";
                                RxLog.logText(txtBuff);
                            }
                            // Sync time timed out..
                            NodeProfile[node - 1].syncTimeSentAt = DateTime.MinValue;
                            timeSync_limit--;
                            NodeProfile[node - 1].syncTimeFlag = true; // Re-queue..
                            dontSyncNow = true; // Don't send the sync request now.. Wait til next packet received.
                        }
                    }
                    if (NodeProfile[node - 1].syncTimeFlag && timeSync_limit < NUM_TIME_SYNC_LIMIT && uploadNum_limit == 0 && !dontSyncNow && lastRebootSentAt.AddSeconds(20) < DateTime.UtcNow)
                    {
                        sendSyncTime(node);
                    }
                    else
                    {

                       // if (timeSync_limit <= 0)
                        //{
                            if (lpHub.lpDevices[node - 1].getImpactsFlag) sendGetImpacts(node);
                        //}
                        // These commands can send any time.
                        if (lpHub.lpDevices[node - 1].stopRecordingFlag) sendStopRecording(node);
                        if (lpHub.lpDevices[node - 1].startRecordingFlag) sendStartRecording(node);
                        if (NodeProfile[node - 1].unbindFlag) sendUnbind(node);
                        if (lpHub.lpDevices[node - 1].powerOffFlag) sendPowerOff(node); // this flag must be unset when the hub sees the device power off
                    }
            }
            else
            {
                //if (timeSync_limit <= 0)
                //{
                    if (lpHub.lpDevices[node - 1].keepPowerOnFlag && !lpHub.lpDevices[node - 1].powerOnFlag) // If we aren't already trying to turn it on..
                    {
                        lpHub.lpDevices[node - 1].powerOnFlag = true;
                        lpHub.lpDevices[node - 1].powerOnSent = false;
                    }

                    if (lpHub.lpDevices[node - 1].powerOnFlag) sendPowerOn(node); // this flag must be unset when the hub sees the device power on
                //}
            }
            //lpHub.updateDGView(node); // Update the device status
        }

        private void NodeDataProcess(ref byte[] buff, bool USB_or_BLE)
        {
            //System.Diagnostics.Debug.Write("NodeDataProcess: " .ToString());
            //for (int i = 0; i < buff.Length; i++) System.Diagnostics.Debug.Write(" " + buff[i].ToString());
            //System.Diagnostics.Debug.WriteLine("");
            int len, node, plen;
            //string temp;
            byte seqn, mlen;

            byte report_flag = 0;
            

            mlen = (byte)(buff[4] & 0xF);
            seqn = (byte)(buff[4] >> 4);
            node = buff[1];
            if ((buff[5] == 0x55 || buff[5] == 0xAA)) // && seqn == 0
            {

            }

            if (lpHub != null)
                lpHub.lpDevices[node - 1].lastPingTime = System.DateTime.UtcNow;

            //UpdateNodeControlLayout(node);

            plen = buff[3];
            if (RxLog != null && RxLog.isCapture(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
            {
                var txtBuff = DateTime.Now + " Rx:";
                //for (int i = 0; i < plen+5; i++) 
                txtBuff += BitConverter.ToString(buff.Take(13).ToArray()).Replace('-','\t') + "   Node" + node.ToString("D2");
                RxLog.logText(txtBuff);
            }
            /////////////////////////////////////////////////////////////                               // 2014.10.31
            if (lpHub != null)
            {
                if (!lpHub.lpDevices[node - 1].valid)                                                       // 2014.10.31
                {
                    if ((plen == (CHUNK_HIT_DATA + 1)) || (plen == (CHUNK_IMMEDIATE + 1)))
                        lastRebootSentAt = DateTime.UtcNow;
                    if (!USB_or_BLE)
                        ;// mSendMsgCMD((byte)node, 21);                                                        // 2014.10.31, re-boot GFT, Stop the wrong Uploading
                    else
                        ;// mSendMsgBleCMD((byte)node, 21);
                     this.BeginInvoke((MethodInvoker)delegate { m_button_NodeInfo_Click(this, null); });     // 2014.10.31, Adde by Jason Chen, 2014.10.31
                    return;                                                                                 // 2014.10.31
                }
                /////////////////////////////////////////////////////////////// 2014.10.31
                else                                                                                        // Added by Jason Chen, 2017.03.21
#if true
                /////////////////////////////////////////////////////////////                               // 2014.10.31
                if (!NodeProfile[node - 1].register && lpHub != null && lpHub.lpDevices.Count > 0)   // 2017.03.16 Added check to see if the Hub has loaded devices yet.
                {
                    if (plen != (CHUNK_POWER_ON + 1) && plen != (CHUNK_POWER_OFF + 1))                                                       // 2014.10.31
                    {
                        if (timeSync_limit <= 0)
                        { // Only issue reboots if we aren't syncing times..
                            lastRebootSentAt = DateTime.UtcNow;
                            if (!USB_or_BLE)
                                ;// this.BeginInvoke((MethodInvoker)delegate { mSendMsgCMD((byte)node, 21); });          // 2014.10.31, re-boot GFT to register the Node to loacl database
                            else
                                ;// this.BeginInvoke((MethodInvoker)delegate { mSendMsgBleCMD((byte)node, 21); });
                        }
                        return;                                                                             // 2014.10.31
                    }
                }
            }
            else return;                                                                                   
            /////////////////////////////////////////////////////////////// 2014.10.31
#endif

            //string textInfo = string.Empty;
            //string Bat_msg = string.Empty;
            //now = Environment.TickCount;// &Int32.MaxValue;                
            //dif = now - then;
            //if ((dif > 999) || (dif < 0))
            //{
            //    dif = 999;
            //    if (oldif != 999)
            //        textInfo += "\r\n";
            //}

            //textInfo += string.Format("{0:d3}", dif);

            if (m_bSeqNo[node] == 0xFF) 
                ;//textInfo += " --- ";
            else if (seqn == (m_bSeqNo[node] + 1)) 
                ;//textInfo += " ... ";
            else if (seqn == 0) 
                ;//textInfo += " ,,, ";
            else
            {
                //textInfo += " Err ";
                m_bSeqErr++;
            }
            m_bSeqNo[node] = seqn;
            //Bind
            //if (plen == (CHUNK_BINDING + 1))
                //textInfo += "Bnd:";

            // U1/U0
            //else if (plen == (CHUNK_SHELF_MODE_U + 1))
                //textInfo += string.Format("SU{0:x}: ", buff[5]);

            //D1/D0
            //else if (plen == (CHUNK_SHELF_MODE_D + 1))
                //textInfo += string.Format("SD{0:x}: ", buff[5]);

            // Power off OR Check Battery
            //else 
            if (plen == (CHUNK_POWER_OFF + 1))
            {
                if ((Args[node - 1].rec128 == true && seqn > 0 && Args[node - 1].msg_packet_num == 2)) //(buff[5] == 0 || buff[5] == 0x0D) &&  //(buff[5] == 0x80) || (buff[5] == 0x81) || 
                {
                    if (buff[5] == 0x80 || buff[5] == 0x81)
                    {
                        Args[node - 1].msg_packet_num = 2;
                        Args[node - 1].recv_count = 0;
                        Args[node - 1].rec128 = true;
                    }
                    if ((seqn == 0) && (mlen != 0))
                    {
                        len = buff[4] & 0x0f;
                        if (len > 0)
                        {
                            Array.Copy(buff, 5, Args[node - 1].recvbuff, 0, len);
                            Args[node - 1].recv_count += len;
                            System.Diagnostics.Debug.WriteLine("received");
                        }
                    }
                    else if ((seqn > 0) && (seqn < Args[node - 1].msg_packet_num))
                    {
                        len = buff[4] & 0x0f;

                        Array.Copy(buff, 5, Args[node - 1].recvbuff, Args[node - 1].recv_count, len);
                        Args[node - 1].recv_count += len;

                        if (seqn == Args[node - 1].msg_packet_num - 1)  // Check Battery Packet transmission end
                        {
                            if (node >= 1 && node <= NUM_NODES)
                            {
                                Args[node - 1].rec128 = false;
                                //textInfo += "BatLevel: ";
                                lpHub.selectRow(node - 1);
                                //if (!NodeControl[node - 1].Enabled)
                                //    NodeControl[node - 1].Enabled = true;

                                //NodeControl[node - 1].m_PWR = true;
                                //NodeControl[node - 1].m_Hit = false;
                                //NodeControl[node - 1].m_Usb = false;
                                //NodeControl[node - 1].m_Imm = false;
                                //NodeControl[node - 1].m_Tmp = 0;

                                //SetTimer(this.Handle, 0x300 + node, 3000, null);//new TimerEventHandler(timerCallBack));

                                lpHub.lpDevices[node - 1].live = true;
                                //lpHub.lpDevices[node - 1].gForce = 0;
                                lpHub.lpDevices[node - 1].temperature = 0;
                                if (lpHub.lpDevices[node - 1].mode == 0)
                                {
                                    if (NodeProfile[node - 1].iLocation[0] == 0 && NodeProfile[node - 1].iLocation[1] == 0 && NodeProfile[node - 1].iLocation[2] == 0 && NodeProfile[node - 1].iLocation[3] == 0)
                                    {
                                        lastRebootSentAt = DateTime.UtcNow;
                                        if (!USB_or_BLE)
                                            mSendMsgCMD((byte)node, 21);
                                        else
                                            mSendMsgBleCMD((byte)node, 21);
                                        return;
                                    }
                                }
                                lpHub.lpDevices[node - 1].mode = 1;     // ----> go to Power On mode;    2014.01.20
                                lpHub.lpDevices[node - 1].powerOnFlag = false; // Device is on, unset this flag

                                if (NodeProfile[node - 1].uploadNum_Flag == 1)
                                {
                                    NodeProfile[node - 1].uploadNum_Flag = 0;
                                    lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                    Args[node - 1].mHitFlag = false;
                                    NodeProfile[node - 1].impactCount = 0;
                                    NodeProfile[node - 1].sessionCount = 0;
                                }

                                Wireless_dataInit(node);            // Don't need initialize the evenID and sessionID each time?
                                if (/*(NodeControl[node - 1].m_PWR) && */(lpHub.lpDevices[node - 1].live))
                                {
                                    // int bat_level = Args[node - 1].recvbuff[2] * 256 + Args[node - 1].recvbuff[3];
                                    int bat_level = Args[node - 1].recvbuff[6] * 256 + Args[node - 1].recvbuff[7];
                                    NodeProfile[node - 1].bat_level = bat_level;
                                    lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

                                    bat_level = Args[node - 1].recvbuff[4] * 256 + Args[node - 1].recvbuff[5]; // Re-used variable, this is actually the impact count reported by the GFT
                                    NodeProfile[node - 1].impactNum = bat_level;
                                    if (NodeProfile[node - 1].usbMode == 129 && NodeProfile[node - 1].impactNum == 0) NodeProfile[node - 1].deleteFlag = false; // we can unset this
                                    System.Diagnostics.Debug.WriteLine("node: " + node + ", Impacts: " + bat_level);
                                    //if (node == 26) mSendMsgCMD((byte)node, 191);
                                    NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[0];

                                    // Active Time

                                    ACTIVITY_DATA aData = new ACTIVITY_DATA();
                                    aData.activeTime = BitConverter.ToInt32(new byte[4] { Args[node - 1].recvbuff[11], Args[node - 1].recvbuff[10], Args[node - 1].recvbuff[9], Args[node - 1].recvbuff[8] }, 0);
                                    aData.reportTime = DateTime.UtcNow;

                                    lpHub.lpDevices[node - 1].activeTime = aData.activeTime;
                                    NodeProfile[node - 1].entryNum = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[13], Args[node - 1].recvbuff[12] }, 0);
                                    NodeProfile[node - 1].sessionNum = NodeProfile[node - 1].entryNum - NodeProfile[node - 1].impactNum;

                                    /*if (NodeProfile[node - 1].sData.OnTime > DateTime.MinValue)
                                    {
                                        NodeProfile[node - 1].sData.activeTime = (int)(aData.activeTime / 3000);
                                        NodeProfile[node - 1].sData.OffTime = DateTime.UtcNow;
                                        NodeProfile[node - 1].sData.Duration = (int)(NodeProfile[node - 1].sData.OffTime - NodeProfile[node - 1].sData.OnTime).TotalMinutes;
                                        NodeProfile[node - 1].sData.GID = NodeProfile[node - 1].GID;
                                        gDataWUpdateSession(NodeProfile[node - 1].sData, "SessionWireless", node); // Save the updated active time
                                    }*/

                                    lpHub.lpDevices[node - 1].activeTimeList.Add(aData);// mythis.impactData;            // Event
                                    System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Active Time: " + lpHub.lpDevices[node - 1].activeTime);


                                    NodeProfile[node - 1].bat_level = gftBattery(Args[node - 1].recvbuff[1], Args[node - 1].recvbuff[2], Args[node - 1].recvbuff[3], Args[node - 1].recvbuff[6], Args[node - 1].recvbuff[7], Args[node - 1].recvbuff[14]);
                                    lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

                                    NodeProfile[node - 1].max17047Exist = (Args[node - 1].recvbuff[1] == 1) ? true : false;
                                    lpHub.lpDevices[node - 1].max17047Exist = NodeProfile[node - 1].max17047Exist;
                                    System.Diagnostics.Debug.WriteLine(Args[node - 1].recvbuff[6]);
                                }
                                if (NodeProfile[node - 1].usbMode == 0x81)
                                    lpHub.lpDevices[node - 1].mode = 0x81;     // ----> go to Power On mode and charging;    2014.03.18
                                else
                                    lpHub.lpDevices[node - 1].mode = 1;       // ----> go to Power On mode;    2014.01.20


                                lpHub.lpDevices[node - 1].powerOnFlag = false; // Device is on, unset this flag
                                //cmdProcess(node); //uploadProcess(node);
                                if ((NodeProfile[node - 1].usbMode != 0x81) && (!NodeProfile[node - 1].uploadFlag))              // for sending back channel cmd, 2014.05.16
                                {
                                    if (NodeProfile[node - 1].Back_Command_buf.Count > 0)
                                    {
                                        byte[] cmd_buff;
                                        cmd_buff = NodeProfile[node - 1].Back_Command_buf.Dequeue();      //5
                                        if (!USB_or_BLE)
                                            mSendMsgCMD((byte)node, cmd_buff[0]);
                                        else
                                            mSendMsgBleCMD((byte)node, cmd_buff[0]);
                                        System.Diagnostics.Debug.WriteLine("Back Channel Command Sent 5");
                                    }
                                }

                                // System.Diagnostics.Debug.WriteLine("Time to process: " + (DateTime.Now - start).Ticks.ToString());
                                PowerOffProcess(node);
                                cmdProcess(node, USB_or_BLE); //uploadProcess(node);
                                lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

                                // System.Diagnostics.Debug.WriteLine("Time to process: " + (DateTime.Now - start).Ticks.ToString());
                            }
                        }
                    }
                }
                else if (Args[node - 1].rec128 == false && seqn > 0 && Args[node - 1].status_recv_count > 0 && (Args[node - 1].recSyncTime == true || Args[node - 1].recUploadStatus == true))
                {

                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        var txtBuff = DateTime.Now + "  PT" + node.ToString("D2") + ":";
                        if (Args[node - 1].recSyncTime) txtBuff += " Sync Time Packet, Seq: " + seqn + ", Currently syncing " + timeSync_limit + " Devices";
                        if (Args[node - 1].recUploadStatus) txtBuff += " Upload Status Packet, Seq: " + seqn;
                        RxLog.logText(txtBuff);
                    }
                    Args[node - 1].status_recv_count = 0; // We're not doing anything with the rest of this packet.. just need to know what it is so we can discard it.
                    Args[node - 1].recSyncTime = false;
                    Args[node - 1].recUploadStatus = false;
                }
                else if ((buff[5] == 0) && (buff[7] == 255) && (buff[8] == 255))
                {

                }
                else if (((buff[5] == 0) || (buff[5] == 1) || (buff[5] == 2) || (buff[5] == 3) || (buff[5] == 0xFF) || (buff[5] == 0xFE) || (buff[5] == 0x55) || (buff[5] == 0x56) || (buff[5] == 0x80) || (buff[5] == 0x81)) || (Args[node - 1].recStatus == true && seqn > 0 && Args[node - 1].status_recv_count > 0 && Args[node - 1].msg_packet_num == 2))
                {
                    if (((buff[5] == 0) || (buff[5] == 1) || (buff[5] == 2) || (buff[5] == 3) || (buff[5] == 0xFF) || (buff[5] == 0xFE) || (buff[5] == 0x55) || (buff[5] == 0x56) || (buff[5] == 0x80) || (buff[5] == 0x81)) && Args[node - 1].recStatus == false)
                    {
                        //if (buff[5] == 0xFF)
                        //{
                            //if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))    // Commented by Jason Chen, 2017.03.28
                            //{                                                                                                                               // Commented by Jason Chen, 2017.03.28
                            //var txtBuff = "PT" + node.ToString("D2") + ": 0xFF Packet Received";                                                            // Commented by Jason Chen, 2017.03.28
                            //RxLog.logText(txtBuff);                                                                                                         // Commented by Jason Chen, 2017.03.28
                            //}                                                                                                                               // Commented by Jason Chen, 2017.03.28
                        //}
                        //if (buff[5] == 0xFE)
                        //{
                            //if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            //{
                            //    var txtBuff = "PT" + node.ToString("D2") + ": 0xFE Packet Received";
                            //    RxLog.logText(txtBuff);
                            //}

                        //}
                        Args[node - 1].msg_packet_num = 2;
                        Args[node - 1].recv_count = 0;
                        Args[node - 1].recStatus = true;
                    }
                    if (lpHub.lpDevices[node - 1].mode == 4 && lpHub.lpDevices[node - 1].lastReceiveTime > DateTime.UtcNow.AddSeconds(-1))
                    {
                        return; // Drop this packet
                    }

                    if ((seqn == 0) && (mlen != 0))
                    {
                        len = buff[4] & 0x0f;
                        if (len > 0)
                        {
                            Array.Copy(buff, 5, Args[node - 1].recvbuff, 0, len);
                            Args[node - 1].recv_count += len;
                            System.Diagnostics.Debug.WriteLine("received");
                        }
                    }
                    else if ((seqn > 0) && (seqn < Args[node - 1].msg_packet_num))
                    {
                        Args[node - 1].recStatus = false;
                        len = buff[4] & 0x0f;

                        Array.Copy(buff, 5, Args[node - 1].recvbuff, Args[node - 1].recv_count, len);
                        Args[node - 1].recv_count += len;
                        if (seqn == Args[node - 1].msg_packet_num - 1)
                        {
                            if (node >= 1 && node <= NUM_NODES)
                            {
                                Args[node - 1].status_recv_count++; // Received first part of status packet
                                if (Args[node - 1].recvbuff[0] == 0)                // Power off
                                {
                                    ////////////////////////////////////
                                    if ((NodeProfile[node - 1].usbMode != 0x81))// && (!NodeProfile[node - 1].uploadFlag))              // for sending back channel cmd, 2014.05.16
                                    {
                                        if (NodeProfile[node - 1].Back_Command_buf.Count > 0)
                                        {
                                            byte[] cmd_buff;
                                            cmd_buff = NodeProfile[node - 1].Back_Command_buf.Dequeue();                     //1
                                            if (!USB_or_BLE)
                                                mSendMsgCMD((byte)node, cmd_buff[0]);
                                            else
                                                mSendMsgBleCMD((byte)node, cmd_buff[0]);
                                            System.Diagnostics.Debug.WriteLine("Back Channel Command Sent 1");
                                        }
                                    }

                                    cmdProcess(node, USB_or_BLE);                                                                                   // Added by Jason Chen, 2017.03.28
                                    //////////////////////////////////////////////

                                    // Battery Status Packet
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "PT" + node.ToString("D2") + ": because Power Off Received";                        // Modified by Jason Chen, 2017.03.23
                                        RxLog.logText(txtBuff);
                                    }

                                    //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 2014.05.22
                                    //textInfo += "Pof:";

                                    //UpdateNodeControlLayout(node);                  // Commented by Jason Chen, 2013.12.17
                                    //if (NodeControl[node - 1].UserName != null)
                                    //    lpHub.lpDevices[node - 1].name = (NodeControl[node - 1].UserName + "          ( " + "Power Off" + " )").ToCharArray();
                                    //else
                                    //    lpHub.lpDevices[node - 1].name = ("          ( " + "Power Off" + " )").ToCharArray();

                                    //ResetNodeControls(node);
                                    Args[node - 1].msg_len = 0;                       // reset power cmd length = 0

                                    lpHub.selectRow(node - 1);
                                    if (lpHub.lpDevices[node - 1].live == false)
                                    {
                                        lpHub.lpDevices[node - 1].live = true;
                                        //NodeControl[node - 1].m_PWR = true;
                                        SetTimer(this.Handle, 0x800 + node, 500, null);
                                    }
                                    else
                                        lpHub.lpDevices[node - 1].live = false;

                                    //lpHub.lpDevices[node - 1].alarm = false;
                                    //lpHub.lpDevices[node - 1].gForce = 0;
                                    if (lpHub.lpDevices[node - 1].mode == 4)
                                    {
                                        System.Diagnostics.Debug.WriteLine("Download Interrupted by Power Off");
                                        raiseGftMsg(this, new gftMsgArgs(wirelessGftMsg.MSG_DOWNLOAD_INTERRUPTED, "Download Interrupted\n#" + NodeProfile[node - 1].playerNum + " " + NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName + "'s GFT was Powered off\nbefore the Download could complete.\n\nIf you are unable to power the device on again now, you may\ndownload the data later Wirelessly or by using the USB cable."));
                                    }
                                    lpHub.lpDevices[node - 1].mode = 0;     // ----> go to Power Off mode;    2014.01.20
                                    lpHub.lpDevices[node - 1].charging = false;
                                    lpHub.lpDevices[node - 1].powerOffFlag = false; // Device has powered off, unset this flag
                                    NodeProfile[node - 1].deleteFlag = false; // Device is powered off.. Erase should be complete
                                    //NodeControl[node - 1].setChkPowerOff(false);
                                    lpHub.lpDevices[node - 1].temperature = 0;


                                    /*if (NodeProfile[node - 1].sData.OnTime > DateTime.MinValue)
                                    {
                                        NodeProfile[node - 1].sData.OffTime = DateTime.UtcNow;
                                        NodeProfile[node - 1].sData.Duration = (int)(NodeProfile[node - 1].sData.OffTime - NodeProfile[node - 1].sData.OnTime).TotalMinutes;
                                        NodeProfile[node - 1].sData.GID = NodeProfile[node - 1].GID;
                                        gDataWUpdateSession(NodeProfile[node - 1].sData, "SessionWireless", node); // Save the off time
                                        NodeProfile[node - 1].sData = new uploadDataForWirelessProcess.SESSION_DATA();
                                    }*/


#if true//USE_OF_LIST
                                    //Args[node - 1].recvFIFO.Clear();             //Changed to Clear by Jason Chen, 2013.12.17
                                    //if(lpHub.lpDevices[node-1].powerOnSent) 
                                    //lpHub.lpDevices[node - 1].resetTracking(); // powerOnSent is default true, only returns false if the power on command has been initiated
#endif
                                    //chartRawLinW.Series.Clear();
                                    //chartRawGyroW.Series.Clear();
                                    //chartViewer.Series.Clear();

                                    //int rowCount = dataGridView3.Rows.Count;
                                    //for (int index = 0; index < rowCount; index++) dataGridView3.Rows.RemoveAt(0);

                                    //////////////////////////////////////////////////////////////////////////////////////////////////
                                    int bat_level = Args[node - 1].recvbuff[2] * 256 + Args[node - 1].recvbuff[3];                 // Added by Jason Chen, 2014.04.17
                                    if (bat_level != 0)
                                    {
                                        if (bat_level == 1)
                                            report_flag = 1;                                     // Low battery Packet
                                        else if (bat_level == 2)
                                        {
                                            report_flag = 3;                                     // uplugging Packet
                                            mtimer.Enabled = false;
                                        }


                                        //NodeProfile[node - 1].bat_level = bat_level;
                                        //lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

                                        bat_level = Args[node - 1].recvbuff[4] * 256 + Args[node - 1].recvbuff[5];
                                        NodeProfile[node - 1].impactNum = bat_level;

                                        //NodeProfile[node - 1].sessionNum = BitConverter.ToInt16(new byte[2] { buff[18], buff[19] }, 0);
                                        NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[0];

                                    }
                                    //cmdProcess(node);                                                                                 // Commented by Jason Chen, 2017.03.28

                                    //if ((NodeProfile[node - 1].usbMode != 0x81))// && (!NodeProfile[node - 1].uploadFlag))              // for sending back channel cmd, 2014.05.16
                                    //{
                                    //    if (NodeProfile[node - 1].Back_Command_buf.Count > 0)
                                    //    {
                                    //        byte[] cmd_buff;
                                    //        cmd_buff = NodeProfile[node - 1].Back_Command_buf.Dequeue();                     //1
                                    //        mSendMsgCMD((byte)node, cmd_buff[0]);
                                    //        System.Diagnostics.Debug.WriteLine("Back Channel Command Sent 1");
                                    //    }
                                    //}
                                }
                                else if ((Args[node - 1].recvbuff[0] == 0x55) || (Args[node - 1].recvbuff[0] == 0x56))// || (Args[node - 1].recvbuff[0] == 0xFE))              //2017.03.31
                                {

                                    lpHub.lpDevices[node - 1].mode = 1;     // ----> go to Power On mode;    2014.01.20
                                    lpHub.lpDevices[node - 1].powerOnFlag = false; // Device is on, unset this flag
                                    //lpHub.lpDevices[node - 1].powerOnFlag = false; // Device is on, unset this flag

                                    // Battery Status Packet
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "PT" + node.ToString("D2") + ": Unknown Status Received";
                                        switch (Args[node - 1].recvbuff[0])
                                        {
                                            case 0x55:
                                                txtBuff = "PT" + node.ToString("D2") + ": Ack0 0x55 Status Received";                         // Added by Jason Chen, 2017.03.31
                                                break;                                                                                        // Added by Jason Chen, 2017.03.31
                                            case 0x56:
                                                txtBuff = "PT" + node.ToString("D2") + ": Ack1 0x56 Status Received";
                                                break;
                                            case 0xFE:
                                                txtBuff = "PT" + node.ToString("D2") + ": 0xFE Status Received";
                                                break;
                                            default:
                                                break;

                                        }
                                        RxLog.logText(txtBuff);
                                    }
                                    Args[node - 1].rec128 = false;
                                    lpHub.selectRow(node - 1);
                                    //if (!NodeControl[node - 1].Enabled)
                                    //    NodeControl[node - 1].Enabled = true;

                                    //NodeControl[node - 1].m_PWR = true;
                                    //NodeControl[node - 1].m_Usb = false;

                                    //SetTimer(this.Handle, 0x300 + node, 3000, null);

                                    lpHub.lpDevices[node - 1].live = true;
                                    lpHub.lpDevices[node - 1].temperature = 0;
                                    if (lpHub.lpDevices[node - 1].mode == 0)
                                    {
                                        if (NodeProfile[node - 1].iLocation[0] == 0 && NodeProfile[node - 1].iLocation[1] == 0 && NodeProfile[node - 1].iLocation[2] == 0 && NodeProfile[node - 1].iLocation[3] == 0)
                                        {
                                            lastRebootSentAt = DateTime.UtcNow;
                                            if (!USB_or_BLE)
                                                mSendMsgCMD((byte)node, 21);
                                            else
                                                mSendMsgBleCMD((byte)node, 21);
                                            return;
                                        }
                                    }

                                    if (NodeProfile[node - 1].uploadNum_Flag == 1)
                                    {
                                        NodeProfile[node - 1].uploadNum_Flag = 0;
                                        lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                        Args[node - 1].mHitFlag = false;
                                        NodeProfile[node - 1].impactCount = 0;
                                        NodeProfile[node - 1].sessionCount = 0;
                                    }

                                    Wireless_dataInit(node);            // Don't need initialize the evenID and sessionID each time?
                                    if (/*(NodeControl[node - 1].m_PWR) &&*/ (lpHub.lpDevices[node - 1].live))
                                    {
                                        int impact_cnt = 0;
                                        //if (Args[node - 1].recvbuff[0] == 0x55 || Args[node - 1].recvbuff[0] == 0x56)                                      // Commented by Jason Chen, 2017.03.31, don't need
                                        //{                                                                                                                  // Commented by Jason Chen, 2017.03.31, don't need
                                            impact_cnt = Args[node - 1].recvbuff[3] * 256 + Args[node - 1].recvbuff[4]; // This is in a different position on this version of the packet!
                                            lpHub.lpDevices[node - 1].mode = 4;     // GFT uploading  
                                            //}                                                                                                              // Commented by Jason Chen, 2017.03.31, don't need
                                        //else                                                                                                               // Commented by Jason Chen, 2017.03.31, don't need
                                        //{                                                                                                                  // Commented by Jason Chen, 2017.03.31, don't need
                                        //    impact_cnt = Args[node - 1].recvbuff[4] * 256 + Args[node - 1].recvbuff[5]; // This is the normal version      // Commented by Jason Chen, 2017.03.31, don't need
                                        //}                                                                                                                  // Commented by Jason Chen, 2017.03.31, don't need

                                        //int battery_lvl = gftBattery(Args[node - 1].recvbuff[1], Args[node - 1].recvbuff[2], Args[node - 1].recvbuff[3], Args[node - 1].recvbuff[6], Args[node - 1].recvbuff[7], Args[node - 1].recvbuff[14]);
                                        int entry_cnt = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[13], Args[node - 1].recvbuff[12] }, 0);
                                        //int active_time = BitConverter.ToInt32(new byte[4] { Args[node - 1].recvbuff[11], Args[node - 1].recvbuff[10], Args[node - 1].recvbuff[9], Args[node - 1].recvbuff[8] }, 0);


                                        NodeProfile[node - 1].entryNum = entry_cnt;
                                        NodeProfile[node - 1].sessionNum = entry_cnt - impact_cnt;
                                        //NodeProfile[node - 1].bat_level = battery_lvl; 
                                        NodeProfile[node - 1].impactNum = impact_cnt;
                                        NodeProfile[node - 1].max17047Exist = (Args[node - 1].recvbuff[1] == 1) ? true : false;
                                        NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[0];

                                        lpHub.lpDevices[node - 1].max17047Exist = NodeProfile[node - 1].max17047Exist;
                                        //lpHub.lpDevices[node - 1].battLevel = battery_lvl;
                                        //lpHub.lpDevices[node - 1].activeTime = active_time;


                                        if (NodeProfile[node - 1].usbMode == 129 && NodeProfile[node - 1].impactNum == 0) NodeProfile[node - 1].deleteFlag = false; // we can unset this
                                                                          /*0x81*/
                                        /*// Active Time

                                        ACTIVITY_DATA aData = new ACTIVITY_DATA();
                                        aData.activeTime = active_time;
                                        aData.reportTime = DateTime.UtcNow;
                                        lpHub.lpDevices[node - 1].activeTimeList.Add(aData);*/


                                        // Log Session Count
                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                        {
                                          //var txtBuff = "PT" + node.ToString("D2") + ": Reported Reports " + entry_cnt + ", Impacts/Performance Reports: " + impact_cnt;
                                            var txtBuff = "PT" + node.ToString("D2") + "==>0x55/0x56 Report All entry num = " + entry_cnt + ", and Impacts/Performance = " + impact_cnt;
                                            RxLog.logText(txtBuff);
                                        }
                                    }
                                }
                                else if (Args[node - 1].recvbuff[0] == 0xFE)                                // 0XFE just means Multiple byte back channel cmd fails, 2017.03.31
                                {
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "***PT" + node.ToString("D2") + ": Multiple byte back channel cmd fails***";
                                        RxLog.logText(txtBuff);
                                    }
                                }
                                else if ((Args[node - 1].recvbuff[0] == 0x80) || (Args[node - 1].recvbuff[0] == 0x81))
                                {

                                    lpHub.lpDevices[node - 1].mode = 1;     // ----> go to Power On mode;    2014.01.20
                                    lpHub.lpDevices[node - 1].powerOnFlag = false; // Device is on, unset this flag
                                    //lpHub.lpDevices[node - 1].powerOnFlag = false; // Device is on, unset this flag

                                    // Battery Status Packet
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "PT" + node.ToString("D2") + ": Unknown Status Received";
                                        switch (Args[node - 1].recvbuff[0])
                                        {
                                            case 0x80:
                                                txtBuff = "PT" + node.ToString("D2") + ": Battery Status Received(Waking up)";
                                                break;
                                            case 0x81:
                                                txtBuff = "PT" + node.ToString("D2") + ": Battery Status Received(Charging)";
                                                break;
                                            //case 0x56:
                                            //    txtBuff = "PT" + node.ToString("D2") + ": Ack Status Received";
                                            //    break;
                                            //case 0xFE:
                                            //    txtBuff = "PT" + node.ToString("D2") + ": 0xFE Status Received";
                                            //    break;
                                            default:
                                                break;

                                        }
                                        RxLog.logText(txtBuff);
                                    }
                                    Args[node - 1].rec128 = false;
                                    lpHub.selectRow(node - 1);
                                    //if (!NodeControl[node - 1].Enabled)
                                    //    NodeControl[node - 1].Enabled = true;

                                    //NodeControl[node - 1].m_PWR = true;
                                    //NodeControl[node - 1].m_Usb = false;

                                    //SetTimer(this.Handle, 0x300 + node, 3000, null);

                                    lpHub.lpDevices[node - 1].live = true;
                                    lpHub.lpDevices[node - 1].temperature = 0;
                                    if (lpHub.lpDevices[node - 1].mode == 0)
                                    {
                                        if (NodeProfile[node - 1].iLocation[0] == 0 && NodeProfile[node - 1].iLocation[1] == 0 && NodeProfile[node - 1].iLocation[2] == 0 && NodeProfile[node - 1].iLocation[3] == 0)
                                        {
                                            lastRebootSentAt = DateTime.UtcNow;
                                            if (!USB_or_BLE)
                                                mSendMsgCMD((byte)node, 21);
                                            else
                                                mSendMsgBleCMD((byte)node, 21);
                                            return;
                                        }
                                    }

                                    if (NodeProfile[node - 1].uploadNum_Flag == 1)
                                    {
                                        NodeProfile[node - 1].uploadNum_Flag = 0;
                                        lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                        Args[node - 1].mHitFlag = false;
                                        NodeProfile[node - 1].impactCount = 0;
                                        NodeProfile[node - 1].sessionCount = 0;
                                    }

                                    //Wireless_dataInit(node);           // Don't need initialize the evenID and sessionID each time?    ?????????????????
                                    this.BeginInvoke((MethodInvoker)delegate { Wireless_dataInit(node); });                            // Added by Jason Chen, 2017.03.29
                                    if (/*(NodeControl[node - 1].m_PWR) &&*/ (lpHub.lpDevices[node - 1].live))
                                    {
                                        int impact_cnt = Args[node - 1].recvbuff[4] * 256 + Args[node - 1].recvbuff[5];
                                        int battery_lvl = gftBattery(Args[node - 1].recvbuff[1], Args[node - 1].recvbuff[2], Args[node - 1].recvbuff[3], Args[node - 1].recvbuff[6], Args[node - 1].recvbuff[7], Args[node - 1].recvbuff[14]);
                                        int entry_cnt = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[13], Args[node - 1].recvbuff[12] }, 0);
                                        int active_time = BitConverter.ToInt32(new byte[4] { Args[node - 1].recvbuff[11], Args[node - 1].recvbuff[10], Args[node - 1].recvbuff[9], Args[node - 1].recvbuff[8] }, 0);


                                        NodeProfile[node - 1].entryNum = entry_cnt;
                                        NodeProfile[node - 1].sessionNum = entry_cnt - impact_cnt;
                                        NodeProfile[node - 1].bat_level = battery_lvl;
                                        NodeProfile[node - 1].impactNum = impact_cnt;
                                        NodeProfile[node - 1].max17047Exist = (Args[node - 1].recvbuff[1] == 1) ? true : false;
                                        NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[0];

                                        lpHub.lpDevices[node - 1].max17047Exist = NodeProfile[node - 1].max17047Exist;
                                        lpHub.lpDevices[node - 1].battLevel = battery_lvl;
                                        lpHub.lpDevices[node - 1].activeTime = active_time;


                                        if (NodeProfile[node - 1].usbMode == 129 && NodeProfile[node - 1].impactNum == 0) NodeProfile[node - 1].deleteFlag = false; // we can unset this
                                        /* 0x81 = 129 */
                                        // Active Time

                                        ACTIVITY_DATA aData = new ACTIVITY_DATA();
                                        aData.activeTime = active_time;
                                        aData.reportTime = DateTime.UtcNow;
                                        //lpHub.lpDevices[node - 1].activeTimeList.Add(aData);
                                        this.BeginInvoke((MethodInvoker)delegate { lpHub.lpDevices[node - 1].activeTimeList.Add(aData); });                            // Added by Jason Chen, 2017.03.29


                                        // Log Session Count
                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                        {
                                            var txtBuff = "PT" + node.ToString("D2") + ": Reported Battery: " + battery_lvl + ", Reports " + entry_cnt + ", Impacts/Performance Reports: " + impact_cnt + ", Active Time: " + active_time;
                                            RxLog.logText(txtBuff);
                                        }
                                    }
                                    if (NodeProfile[node - 1].usbMode == 0x81)
                                        lpHub.lpDevices[node - 1].mode = 0x81;     // ----> go to Power On mode and charging;    2014.03.18
                                    else
                                        lpHub.lpDevices[node - 1].mode = 1;       // ----> go to Power On mode;    2014.01.20


                                    if ((Args[node - 1].recvbuff[0] == 0x80) || (Args[node - 1].recvbuff[0] == 0x81))
                                    {
                                        if ((NodeProfile[node - 1].usbMode != 0x81) && (!NodeProfile[node - 1].uploadFlag))              // for sending back channel cmd, 2014.05.16
                                        {
                                            if (NodeProfile[node - 1].Back_Command_buf.Count > 0)
                                            {
                                                byte[] cmd_buff;
                                                cmd_buff = NodeProfile[node - 1].Back_Command_buf.Dequeue();                 //2
                                                if (!USB_or_BLE)
                                                    mSendMsgCMD((byte)node, cmd_buff[0]);
                                                else
                                                    mSendMsgBleCMD((byte)node, cmd_buff[0]);

                                                System.Diagnostics.Debug.WriteLine("Back Channel Command Sent 2 ********************");
                                            }
                                            else
                                            {
                                                System.Diagnostics.Debug.WriteLine("No Back Channel Command Sent 2 ********************");
                                            }
                                        }

                                      //PowerOffProcess(node);
                                        this.BeginInvoke((MethodInvoker)delegate { PowerOffProcess(node); });                            // Added by Jason Chen, 2017.03.29
                                      //cmdProcess(node); //uploadProcess(node);
                                        this.BeginInvoke((MethodInvoker)delegate { cmdProcess(node, USB_or_BLE); });                                 // Added by Jason Chen, 2017.03.30
                                    }

                                }
                                else if ((Args[node - 1].recvbuff[0] == 1) || (Args[node - 1].recvbuff[0] == 2) || (Args[node - 1].recvbuff[0] == 3) || (Args[node - 1].recvbuff[0] == 0xFF))            // Sleep
                                {

                                    if (Args[node - 1].recvbuff[0] == 0xFF)
                                    {
                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                        {
                                            var txtBuff = "PT" + node.ToString("D2") + ": 0xFF Received";
                                            RxLog.logText(txtBuff);
                                        }
                                    }
                                    if (Args[node - 1].recvbuff[0] == 1)
                                    {
                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                        {
                                            var txtBuff = "PT" + node.ToString("D2") + ": Sleep Received";
                                            RxLog.logText(txtBuff);
                                        }
                                        //textInfo += "Sleep:";
                                        if (lpHub.lpDevices[node - 1].recordEnabled) lpHub.lpDevices[node - 1].mode = 2;     // ----> go to Sleep mode;
                                        else lpHub.lpDevices[node - 1].mode = 11;     // ----> go to Sleep mode;
                                    }
                                    else if (Args[node - 1].recvbuff[0] == 2)
                                    {
                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                        {
                                            var txtBuff = "PT" + node.ToString("D2") + ": Rec Enable Received";
                                            RxLog.logText(txtBuff);
                                        }
                                        //textInfo += "Rec enable:";
                                        lpHub.lpDevices[node - 1].mode = 10;     // ----> Record enabled
                                        lpHub.lpDevices[node - 1].recordEnabled = true;
                                    }
                                    else if (Args[node - 1].recvbuff[0] == 3)
                                    {

                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                        {
                                            var txtBuff = "PT" + node.ToString("D2") + ": Rec Disable Received";
                                            RxLog.logText(txtBuff);
                                        }
                                        //textInfo += "Rec disable:";
                                        lpHub.lpDevices[node - 1].mode = 11;     // ----> go to Sleep mode;
                                        lpHub.lpDevices[node - 1].recordEnabled = false;
                                    }

                                    if (NodeProfile[node - 1].uploadNum_Flag == 1)
                                    {
                                        NodeProfile[node - 1].uploadNum_Flag = 0;
                                        lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                        Args[node - 1].mHitFlag = false;
                                        NodeProfile[node - 1].impactCount = 0;
                                        NodeProfile[node - 1].sessionCount = 0;
                                    }

                                    report_flag = 2;                                         // Sleep Packet


                                    int impact_cnt = Args[node - 1].recvbuff[4] * 256 + Args[node - 1].recvbuff[5];
                                    int battery_lvl = gftBattery(Args[node - 1].recvbuff[1], Args[node - 1].recvbuff[2], Args[node - 1].recvbuff[3], Args[node - 1].recvbuff[6], Args[node - 1].recvbuff[7], Args[node - 1].recvbuff[14]);
                                    int entry_cnt = BitConverter.ToInt16(new byte[2] { Args[node - 1].recvbuff[13], Args[node - 1].recvbuff[12] }, 0);
                                    int active_time = BitConverter.ToInt32(new byte[4] { Args[node - 1].recvbuff[11], Args[node - 1].recvbuff[10], Args[node - 1].recvbuff[9], Args[node - 1].recvbuff[8] }, 0);


                                    NodeProfile[node - 1].entryNum = entry_cnt;
                                    NodeProfile[node - 1].sessionNum = entry_cnt - impact_cnt;
                                    NodeProfile[node - 1].bat_level = battery_lvl;
                                    NodeProfile[node - 1].impactNum = impact_cnt;
                                    NodeProfile[node - 1].max17047Exist = (Args[node - 1].recvbuff[1] == 1) ? true : false;
                                    NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[0];

                                    lpHub.lpDevices[node - 1].max17047Exist = NodeProfile[node - 1].max17047Exist;
                                    lpHub.lpDevices[node - 1].battLevel = battery_lvl;
                                    lpHub.lpDevices[node - 1].activeTime = active_time;


                                    if (NodeProfile[node - 1].usbMode == 129 && NodeProfile[node - 1].impactNum == 0) NodeProfile[node - 1].deleteFlag = false; // we can unset this
                                    /*
                                    // Active Time

                                    ACTIVITY_DATA aData = new ACTIVITY_DATA();
                                    aData.activeTime = active_time;
                                    aData.reportTime = DateTime.UtcNow;
                                    lpHub.lpDevices[node - 1].activeTimeList.Add(aData);
                                    */

                                    // Log Session Count
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "PT" + node.ToString("D2") + ": Sleep Status Reported Battery: " + battery_lvl + ", Reports " + entry_cnt + ", Impacts/Performance Reports: " + impact_cnt + ", Active Time: " + active_time;
                                        RxLog.logText(txtBuff);
                                    }
                                    /*
                                    NodeProfile[node - 1].bat_level = gftBattery(Args[node - 1].recvbuff[1], Args[node - 1].recvbuff[2], Args[node - 1].recvbuff[3], Args[node - 1].recvbuff[6], Args[node - 1].recvbuff[7], Args[node - 1].recvbuff[14]);
                                    lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

                                    int impCount = Args[node - 1].recvbuff[4] * 256 + Args[node - 1].recvbuff[5];
                                    NodeProfile[node - 1].impactNum = impCount;
                                    int entryCount = Args[node - 1].recvbuff[12] * 256 + Args[node - 1].recvbuff[13];
                                    NodeProfile[node - 1].entryNum = entryCount;
                                    NodeProfile[node - 1].sessionNum = NodeProfile[node - 1].entryNum - NodeProfile[node - 1].impactNum;
                                    NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[0];*/

                                    /*bat_level = buff[11] * 256 + buff[12];                                           // Added by Jason Chen, 2014.04.16
                                    NodeProfile[node - 1].bat_levelfuel = bat_level;
                                    lpHub.lpDevices[node - 1].bat_Levelfuel = NodeProfile[node - 1].bat_levelfuel;*/

                                    /*
                                    NodeProfile[node - 1].max17047Exist = (Args[node - 1].recvbuff[1] == 1) ? true : false;
                                    lpHub.lpDevices[node - 1].max17047Exist = NodeProfile[node - 1].max17047Exist;
                                     * */
                                }
                            }
                        }
                    }
                    //else
                    //{
                    //}
                }

                else if (Args[node - 1].rec128 == false && seqn > 0 && Args[node - 1].status_recv_count > 0)
                {
                    Args[node - 1].status_recv_count = 0; // We're not doing anything with the rest of this packet.. just need to know what it is so we can discard it.
                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                    {
                        var txtBuff = "PT" + node.ToString("D2") + ": Did not recognize Packet";
                        RxLog.logText(txtBuff);
                    }
                }
                else
                {
                    //m_Edit_Immediate.AppendText(node.ToString("D2")+"-BCD Data Got it\r\n");
                    //System.Diagnostics.Debug.WriteLine("(Node " + node + ")" + " Packet #" + Args[node - 1].msg_packet_num.ToString() + ", Bytes: " + buff[0].ToString() + ' ' + buff[1].ToString() + ' ' + buff[2].ToString() + ' ' + buff[3].ToString() + ' ' + buff[4].ToString() + ' ' + buff[5].ToString() + ' ' + buff[6].ToString());
                    Args[node - 1].status_recv_count++; // Received first part of status packet
                    if (buff[5] == 0x55)
                    {

                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            var txtBuff = "PT" + node.ToString("D2") + ": Upload Status Received";
                            RxLog.logText(txtBuff);
                        }
                        Args[node - 1].recUploadStatus = true; // so we know to discard the second half..
                        //textInfo += "ACK0:";
                        KillTimer(this.Handle, 0x160 + (uint)node);       // Modified by Jason Chen, 20140211
                    }
                    else if (buff[5] == 0x55 || buff[5] == 0x56)
                    {
                        //textInfo += "ACK1:";

                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            var txtBuff = "PT" + node.ToString("D2") + ": Ack Received";
                            RxLog.logText(txtBuff);
                        }
                        KillTimer(this.Handle, 0x160 + (uint)node);       // Modified by Jason Chen, 20140211
                        //NodeProfile[node - 1].impactNum = buff[8] * 256 + buff[9];
                    }
                    else if (buff[5] == 0x57)
                    {
                        /* if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                         {
                             var txtBuff = "PT: Unbind Ack Received +++++++++++++++++++++";
                             RxLog.logText(txtBuff);
                         }
                         textInfo += "Ubind ACK:";

                         //mSendMsg((byte)node, 21);                             // reboot GFT                   
                         //Thread.Sleep(50);
                         //mSendMsg((byte)node, 21);                             // reboot GFT                   
                         //Thread.Sleep(50);
                         //mSendMsg((byte)node, 21);                             // reboot GFT                   
                         //Thread.Sleep(50);

                         byte[] txBuff = new byte[65];
                         txBuff[1] = MSG_HOST_TO_HUB_UNBIND_MODE;
                         txBuff[2] = (byte)node;
                        // GftWirelessHidHub.WriteOutput(ref txBuff);
                         SetTimer(this.Handle, 0x100, 1000, null);//new TimerEventHandler(timerCallBack));
                         //Thread.Sleep(50);*/
                    }
                    else if ((buff[5] == 0x80) || (buff[5] == 0x81))                // new the report of battery level
                    {

                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            var txtBuff = "PT" + node.ToString("D2") + ": Battery Level Received";
                            RxLog.logText(txtBuff);
                        }
                        Args[node - 1].status_recv_count = 0; // there is no follow up packet to this
                        //textInfo += "BatLevel: ";
                        lpHub.selectRow(node - 1);
                        //if (!NodeControl[node - 1].Enabled)
                        //    NodeControl[node - 1].Enabled = true;

                        //NodeControl[node - 1].m_PWR = true;
                        //NodeControl[node - 1].m_Hit = false;
                        //NodeControl[node - 1].m_Usb = false;
                        //NodeControl[node - 1].m_Imm = false;
                        //NodeControl[node - 1].m_Tmp = 0;

                        //SetTimer(this.Handle, 0x300 + node, 3000, null);//new TimerEventHandler(timerCallBack));

                        lpHub.lpDevices[node - 1].live = true;
                        //lpHub.lpDevices[node - 1].gForce = 0;
                        lpHub.lpDevices[node - 1].temperature = 0;
                        if (lpHub.lpDevices[node - 1].mode == 0)
                        {
                            if (NodeProfile[node - 1].iLocation[0] == 0 && NodeProfile[node - 1].iLocation[1] == 0 && NodeProfile[node - 1].iLocation[2] == 0 && NodeProfile[node - 1].iLocation[3] == 0)
                            {
                                lastRebootSentAt = DateTime.UtcNow;
                                if (!USB_or_BLE)
                                    mSendMsgCMD((byte)node, 21);
                                else
                                    mSendMsgBleCMD((byte)node, 21);
                                return;
                            }
                        }
                        lpHub.lpDevices[node - 1].mode = 1;     // ----> go to Power On mode;    2014.01.20
                        lpHub.lpDevices[node - 1].powerOnFlag = false;

                        if (NodeProfile[node - 1].uploadNum_Flag == 1)
                        {
                            NodeProfile[node - 1].uploadNum_Flag = 0;
                            lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                            Args[node - 1].mHitFlag = false;
                            NodeProfile[node - 1].impactCount = 0;
                        }

                        Wireless_dataInit(node);            // Don't need initialize the evenID and sessionID each time???????????????????????????
                        if (/*(NodeControl[node - 1].m_PWR) &&*/ (lpHub.lpDevices[node - 1].live))
                        {
                            int bat_level = buff[7] * 256 + buff[8];
                            NodeProfile[node - 1].bat_level = bat_level;
                            lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

                            bat_level = buff[9] * 256 + buff[10];
                            NodeProfile[node - 1].impactNum = bat_level;

                            NodeProfile[node - 1].usbMode = buff[5];

                            // Active Time
                            lpHub.lpDevices[node - 1].activeTime = BitConverter.ToInt32(new byte[4] { buff[16], buff[15], buff[14], buff[13] }, 0);
                            // Session Count
                            NodeProfile[node - 1].sessionNum = BitConverter.ToInt16(new byte[2] { buff[18], buff[19] }, 0) - NodeProfile[node - 1].impactNum;

                            bat_level = buff[11] * 256 + buff[12];                                           // Added by Jason Chen, 2014.04.16
                            NodeProfile[node - 1].bat_levelfuel = bat_level;
                            lpHub.lpDevices[node - 1].bat_Levelfuel = NodeProfile[node - 1].bat_levelfuel;

                            NodeProfile[node - 1].max17047Exist = (buff[6] == 1) ? true : false;
                            lpHub.lpDevices[node - 1].max17047Exist = NodeProfile[node - 1].max17047Exist;

                        }
                        if (NodeProfile[node - 1].usbMode == 0x81)
                            lpHub.lpDevices[node - 1].mode = 0x81;     // ----> go to Power On mode and charging;    2014.03.18
                        else
                            lpHub.lpDevices[node - 1].mode = 1;       // ----> go to Power On mode;    2014.01.20

                        if ((NodeProfile[node - 1].usbMode != 0x81) && (!NodeProfile[node - 1].uploadFlag))              // for sending back channel cmd, 2014.05.16
                        {
                            if (NodeProfile[node - 1].Back_Command_buf.Count > 0)
                            {
                                byte[] cmd_buff;
                                cmd_buff = NodeProfile[node - 1].Back_Command_buf.Dequeue();                 //3
                                if (!USB_or_BLE)
                                    mSendMsgCMD((byte)node, cmd_buff[0]);
                                else
                                    mSendMsgBleCMD((byte)node, cmd_buff[0]);
                                System.Diagnostics.Debug.WriteLine("Back Channel Command Sent 3");           // Added by Jason Chen, 2017.03.23
                            }
                        }

                        PowerOffProcess(node);
                        cmdProcess(node, USB_or_BLE); //uploadProcess(node);
                        lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;
                    }
                    else if (buff[5] == 0x90)                                    // new setup Ximit status CMD, that is 144, 145
                    {
#if false
                        KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140430

                        textInfo += "XmitSet:  ";

                        // Added by Jason Chen, 2014.04.30
                        NodeProfile[node - 1].AlarmMode = buff[7];
                        mAlarmEnable.Checked = ((buff[7] & 0x80) == 0) ? false : true;
                        mAudio.Checked = ((buff[7] & 0x10) == 0) ? false : true;
                        mVisual.Checked = ((buff[7] & 0x20) == 0) ? false : true;
                        mInterlock.Checked = ((buff[7] & 0x40) == 0) ? false : true;
                        mFieldDataEnable.Checked = ((buff[6] & 0x01) == 0x01) ? true : false;
                        mTestMode.Checked = ((buff[7] & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01
                        ////////////////////////////////////////////////////////////////
#endif
                    }
                    else if (buff[5] == 0x92)                                    // new setup Power Mode CMD, that is 146, 147
                    {
                        //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140430

                        //textInfo += "pwrMode:  ";

                        // Added by Jason Chen, 2014.04.30
                        NodeProfile[node - 1].AlarmMode = buff[7];
                        //mAlarmEnable.Checked = ((buff[7] & 0x80) == 0) ? false : true;
                        //mAudio.Checked = ((buff[7] & 0x10) == 0) ? false : true;
                        //mVisual.Checked = ((buff[7] & 0x20) == 0) ? false : true;
                        //mInterlock.Checked = ((buff[7] & 0x40) == 0) ? false : true;
                        //mFieldDataEnable.Checked = ((buff[6] & 0x01) == 0x01) ? true : false;
                        //mTestMode.Checked = ((buff[7] & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01
                        ////////////////////////////////////////////////////////////////
                    }
                    else if (buff[5] == 0xBE)                              // Report the status of GFT
                    {
                        //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140430

                        //textInfo += "Status:   ";

                        // Added by Jason Chen, 2014.04.30
                        NodeProfile[node - 1].AlarmMode = buff[7];
                        //mAlarmEnable.Checked = ((buff[7] & 0x80) == 0) ? false : true;
                        //mAudio.Checked = ((buff[7] & 0x10) == 0) ? false : true;
                        //mVisual.Checked = ((buff[7] & 0x20) == 0) ? false : true;
                        //mInterlock.Checked = ((buff[7] & 0x40) == 0) ? false : true;
                        //mFieldDataEnable.Checked = ((buff[6] & 0x01) == 0x01) ? true : false;
                        //mTestMode.Checked = ((buff[7] & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01
                        ////////////////////////////////////////////////////////////////                       
                    }
                    else if (buff[5] == 0xD0)                             // Got Setup player number CMD ACK, 2014.04.10
                    {
#if false
                        KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140430

                        textInfo += "PlayerNumACK: ";
                        if (TooltabPage16Exist)
                        {
                            //m_Edit_Immediate.AppendText("\r\n---GFT " + node.ToString("D2") + " got BCD Player Number Setup CMD----\r\n\r\n");
                        }


                        // Added by Jason Chen, 2014.04.30
                        NodeProfile[node - 1].AlarmMode = buff[7];
                        mAlarmEnable.Checked = ((buff[7] & 0x80) == 0) ? false : true;
                        mAudio.Checked = ((buff[7] & 0x10) == 0) ? false : true;
                        mVisual.Checked = ((buff[7] & 0x20) == 0) ? false : true;
                        mInterlock.Checked = ((buff[7] & 0x40) == 0) ? false : true;
                        mFieldDataEnable.Checked = ((buff[6] & 0x01) == 0x01) ? true : false;
                        mTestMode.Checked = ((buff[7] & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01
                        ////////////////////////////////////////////////////////////////

                        //mSendMsg((byte)Convert.ToInt16(m_Node.Text), (byte)Convert.ToInt16(m_bcdData.Text));
                        mSendMsgCMD((byte)node, (byte)Convert.ToInt16(mPlayerNum.Text));//m_bcdSetupData.Text));
#endif
                    }
                    else if (buff[5] == 0xD1)                             // Got Setup Threshold CMD ACK, 2014.04.15
                    {
                        //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140430

                        //textInfo += "AlarmThresACK: ";

                        // Added by Jason Chen, 2014.04.30
                        NodeProfile[node - 1].AlarmMode = buff[7];
                        //mAlarmEnable.Checked = ((buff[7] & 0x80) == 0) ? false : true;
                        //mAudio.Checked = ((buff[7] & 0x10) == 0) ? false : true;
                        //mVisual.Checked = ((buff[7] & 0x20) == 0) ? false : true;
                        //mInterlock.Checked = ((buff[7] & 0x40) == 0) ? false : true;
                        //mFieldDataEnable.Checked = ((buff[6] & 0x01) == 0x01) ? true : false;
                        //mTestMode.Checked = ((buff[7] & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01
                        ////////////////////////////////////////////////////////////////

                        //mSendMsg((byte)Convert.ToInt16(m_Node.Text), (byte)Convert.ToInt16(m_bcdData.Text));
                        int AlarmThres = 60;//Convert.ToInt16(mAlarmThreshold.Text);//  m_bcdSetupData.Text);
                        if (AlarmThres > 200) AlarmThres = 200;
                        if (!USB_or_BLE)
                            mSendMsgCMD((byte)node, (byte)AlarmThres);
                        else
                            mSendMsgBleCMD((byte)node, (byte)AlarmThres);
                    }
                    else if (buff[5] == 0xD3)                             // Got Setup Record Threshold CMD ACK, 2014.04.29
                    {
                        //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140430

                        //textInfo += "RecThresACK: ";
                        int recThres = 10;// Convert.ToInt16(mRecThreshold.Text);//m_bcdSetupData.Text);
                        if (recThres < 10) recThres = 10;
                        mSendMsgData((byte)node, (byte)recThres, USB_or_BLE);
                    }
                    else if (buff[5] == 0xD4)                             // Got Setup Alarm Mode CMD ACK, 2014.05.01
                    {
                        //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140430

                        //textInfo += "AlarmModeACK: ";
                        byte alarmMode = 0;

                        //if (mAlarmEnable.Checked)
                        alarmMode |= 0x80;
                        //else
                        //    alarmMode &= 0x7F;
                        //if (mAudio.Checked)
                        alarmMode |= 0x40;
                        //else
                        //    alarmMode &= 0xBF;
                        //if (mVisual.Checked)
                        alarmMode |= 0x20;
                        //else
                        //    alarmMode &= 0xDF;
                        //if (mInterlock.Checked)
                        //    alarmMode |= 0x10;
                        //else
                        alarmMode &= 0xEF;

                        //mFieldDataEnable.Checked = ((buff[6] & 0x01) == 0x01) ? true : false;
                        //mTestMode.Checked = ((buff[7] & 0x08) == 0x08) ? true : false;       // Added by Jason Chen, 2015.05.01
                        mSendMsgData((byte)node, alarmMode, USB_or_BLE);
                    }
                    else if (buff[5] == 0xFD)                             // Got request of time synchronization, 2014.05.02
                    {
                        //textInfo += "SyncReq:   ";
                        if ((node >= 1) && (node <= NUM_NODES))
                        {
                            // Flag node for time synchronization
                            NodeProfile[node - 1].syncTimeFlag = true;
                            //mSendMsgCMD((byte)node, 210);                   // Intialize a time synchronization CMD, 2014.05.02
                        }
                    }
                    else if (buff[5] == 0xD2)                             // Got Time Sync ACK, 2014.04.28
                    {

                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            string syncType = "";
                            switch (buff[6])
                            {
                                case 0:
                                    syncType = "Completed";
                                    break;
                                case 1:
                                    syncType = "Year";
                                    break;
                                case 2:
                                    syncType = "Month";
                                    break;
                                case 3:
                                    syncType = "Day";
                                    break;
                                case 4:
                                    syncType = "Hour";
                                    break;
                                case 5:
                                    syncType = "Minute";
                                    break;
                                case 6:
                                    syncType = "Second";
                                    break;
                            }
                            var txtBuff = "";
                            if (syncType == "Completed")
                                txtBuff = "                               **********" + " PT" + node.ToString("D2") + ": Time Sync Request Received for node " + node + " For " + syncType + "==>************Node " + node.ToString("D2");   // Changed by Jason Chen, 2017.03.24
                            else
                                txtBuff = DateTime.Now + " PT" + node.ToString("D2") + ": Time Sync Request Received for node " + node + " For " + syncType;
                            RxLog.logText(txtBuff);
                        }
                        System.Diagnostics.Debug.WriteLine("Doing time sync on " + node);
                        Args[node - 1].recSyncTime = true; // ugh..
                        //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140501

                        DateTime dateTime;
                        dateTime = DateTime.UtcNow;  // Always use UTC Time

                        int time = 0;
                        if (NodeProfile[node - 1].syncTimeSentAt > DateTime.UtcNow.AddMilliseconds(-900))
                        {

                            if (buff[6] == 1)
                            {

                                //textInfo += "SyncYear:   ";
                                time = dateTime.Year - 2000;
                                //time = 0;
                                mSendMsgData((byte)node, (byte)time, USB_or_BLE);

                                //mTimeStatus.Text = "Sync Year";
                            }
                            else if (buff[6] == 2)
                            {
                                //textInfo += "SyncMonth:  ";
                                time = dateTime.Month;
                                //time = 1;
                                mSendMsgData((byte)node, (byte)time, USB_or_BLE);
                                //mTimeStatus.Text = "Sync Month";
                            }
                            else if (buff[6] == 3)
                            {
                                //textInfo += "SyncDay:    ";
                                time = dateTime.Day;
                                //time = 1;
                                mSendMsgData((byte)node, (byte)time, USB_or_BLE);
                                //mTimeStatus.Text = "Sync Day";
                            }
                            else if (buff[6] == 4)
                            {
                                //textInfo += "SyncHour:   ";
                                time = dateTime.Hour;
                                //time = 0;
                                mSendMsgData((byte)node, (byte)time, USB_or_BLE);
                                //mTimeStatus.Text = "Sync Hour";
                            }
                            else if (buff[6] == 5)
                            {
                                //textInfo += "SyncMinute: ";
                                time = dateTime.Minute;
                                //time = 0;
                                mSendMsgData((byte)node, (byte)time, USB_or_BLE);
                                //mTimeStatus.Text = "Sync Minute";
                            }
                            else if (buff[6] == 6)
                            {
                                //textInfo += "SyncSecond: ";
                                time = dateTime.Second;
                                //time = 0;
                                mSendMsgData((byte)node, (byte)time, USB_or_BLE);
                                //mTimeStatus.Text = "Sync Second";
                            }
                            else if (buff[6] == 0)
                            {
                                //textInfo += "SyncEnd: ";
                                timeSync_limit--;
                                NodeProfile[node - 1].syncTimeSentAt = DateTime.MinValue;
                                //KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140429
                                //mTimeStatus.Text = "OK.";

                                //SetTimer(this.Handle, 0xA00 + node, 2000, null);  // Added by Jason Chen, 2014.05.01
                            }
                            if (buff[6] > 0)
                            {
                                NodeProfile[node - 1].syncTimeSentAt = DateTime.UtcNow;
                            }
                        }
                        else
                        {

                            if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = DateTime.Now + "  PT" + node.ToString("D2") + ": Time Sync Request Received for node " + node + " has Expired!!!!";
                                RxLog.logText(txtBuff);
                            }
                        }
                    }
                    else if (buff[5] == 0xD5)                             // Got Modify User Name CMD ACK, 2014.05.01
                    {
                        /* string fullName = mFirstName.Text + " " + mLastName.Text;
                         int name_len = fullName.Length;
                         if (name_len > 20)
                             name_len = 20;
                         else if (name_len == 0)
                             name_len = 1;
                         if (buff[6] == 1)
                         {
                             textInfo += "Length:";
                             mSendMsgData((byte)node, (byte)name_len);
                             if (TooltabPage16Exist)
                                 m_Edit_Immediate.AppendText("\r\n---GFT " + node.ToString("X2") + " Send name length---\r\n");

                             mNameStatus.Text = "Name Length";
                         }
                         else
                         {
                             int ii = buff[6];
                             char[] name_i = new char[fullName.Length];
                             name_i = fullName.ToCharArray();
                             if (TooltabPage16Exist)
                                 m_Edit_Immediate.AppendText("\r\n---GFT " + node.ToString("X2") + " Send the letter " + (ii - 2).ToString("D2") + " of name---\r\n");

                             if (ii >= 2)
                             {
                                 //name_i[0] = fullName.ToCharArray()[ii - 2];
                                 mNameStatus.Text = fullName.Substring(ii - 2, 1);
                                 textInfo += "Name " + mNameStatus.Text + ":";
                                 mNameStatus.Text += " of " + "Name";
                                 mSendMsgData((byte)node, (byte)name_i[ii - 2]);
                                 SetTimer(this.Handle, 0xA00 + node, 2000, null);  // Added by Jason Chen, 2014.05.01
                             }
                             else if (ii == 0)
                             {
                                 textInfo += "Name End ";
                                 mNameStatus.Text = "Name End";
                                 mNameStatus.Text = "Name OK!";
                                 KillTimer(this.Handle, 0x120 + (uint)node);       // Modified by Jason Chen, 20140429
                                 SetTimer(this.Handle, 0xA00 + node, 2000, null);  // Added by Jason Chen, 2014.05.01
                             }
                         }*/
                    }
                    //else
                    //{
                    //textInfo += "NoCMD:    ";// "NoACK:";
                    //}
                }
            }
            // Power ON
            else if (plen == (CHUNK_POWER_ON + 1))
            {

                //textInfo += "Pon:";
                if ((seqn == 0) && (mlen != 0))
                {
                    len = buff[4] & 0x0f;
                    if (len > 0)
                    {
                        string tempss = System.Text.Encoding.ASCII.GetString(buff);
                        //NodeControl[node - 1].UserName = tempss.Substring(5, len);
                        NodeProfile[node - 1].UserName = tempss.Substring(5, len);

                        Array.Copy(buff, 5, Args[node - 1].recvbuff, 0, len);              // Somw time, Args[node-1].recvbuff as null, why
                        Args[node - 1].recv_count += len;
                    }
                }
                //              else if ((seqn == 2) || (seqn == 1)|| (seqn == 3))
                else if ((seqn > 0) && (seqn < Args[node - 1].msg_packet_num))
                {
                    len = buff[4] & 0x0f;
                    string temp;
                    temp = System.Text.Encoding.ASCII.GetString(buff);
                    temp = temp.Substring(5, len);
                    //NodeControl[node - 1].UserName += temp;
                    NodeProfile[node - 1].UserName += temp;

                    Array.Copy(buff, 5, Args[node - 1].recvbuff, Args[node - 1].recv_count, len);
                    Args[node - 1].recv_count += len;

                    //if (seqn == 3)  // Power On Packet transmission end
                    if (seqn == Args[node - 1].msg_packet_num - 1)  // Power On Packet transmission end
                    {
                        if (node >= 1 && node <= NUM_NODES)
                        {
                            if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = "PT" + node.ToString("D2") + ": Power On Received\r\n";
                                RxLog.logText(txtBuff);
                            }
                            //UpdateNodeControlLayout(node);                        // Commented by Jason Chen, 2013.12.17
                            lpHub.selectRow(node - 1);

                            //temp = NodeControl[node - 1].UserName.Substring(6);
                            temp = NodeProfile[node - 1].UserName.Substring(6);
                            //NodeControl[node - 1].UserName = getFirstBeforeZero(DELIM_ZERO, temp);
                            NodeProfile[node - 1].UserName = getFirstBeforeZero(DELIM_ZERO, temp);
                            //len = NodeControl[node - 1].UserName.Length;
                            len = NodeProfile[node - 1].UserName.Length;

                            //if (!NodeControl[node - 1].Enabled)
                            //    NodeControl[node - 1].Enabled = true;

                            //NodeControl[node - 1].m_PWR = true;
                            //NodeControl[node - 1].m_Hit = false;
                            //NodeControl[node - 1].m_Usb = false;
                            //NodeControl[node - 1].m_Imm = false;
                            //NodeControl[node - 1].m_Tmp = 0;

                            //SetTimer(this.Handle, 0x300 + node, 3000, null);//new TimerEventHandler(timerCallBack));

                            lpHub.lpDevices[node - 1].live = true;
                            //lpHub.lpDevices[node - 1].gForce = 0;
                            lpHub.lpDevices[node - 1].temperature = 0;
                            lpHub.lpDevices[node - 1].powerOnFlag = false;
                            if (Args[node - 1].recvbuff[33] == 0x81)
                            {
                                lpHub.lpDevices[node - 1].mode = 0x81;     // ----> go to Power On mode;    2014.05.29
                                lpHub.lpDevices[node - 1].charging = true;
                            }
                            else
                            {
                                lpHub.lpDevices[node - 1].mode = 1;        // ----> go to Power On mode;    2014.01.20
                                lpHub.lpDevices[node - 1].charging = false;
                            }
                            Array.Copy(Args[node - 1].recvbuff, 0, lpHub.lpDevices[node - 1].GID, 0, 6);  // get the node in first packet of Power On Stream

                            //if (NodeControl[node - 1].UserName != null)
                            //lpHub.lpDevices[node - 1].name = NodeControl[node - 1].UserName.ToCharArray();
                            if (NodeProfile[node - 1].UserName != null)
                                lpHub.lpDevices[node - 1].name = NodeProfile[node - 1].UserName.ToCharArray();

                            //                          if (NodeControl[node - 1].UserName != null)
                            //                              lpHub.lpDevices[node - 1].name = (NodeControl[node - 1].UserName + "          ( " + "Power On" + " )").ToCharArray();
                            //                          else
                            //                              lpHub.lpDevices[node - 1].name = ("          ( " + "Power On" + " )").ToCharArray();

                            if (Args[node - 1].firstOn == DateTime.MinValue) Args[node - 1].firstOn = DateTime.UtcNow;
                            Wireless_dataInit(node);
                            PowerOnProcess(node);

                            if ((NodeProfile[node - 1].usbMode != 0x81) && (!NodeProfile[node - 1].uploadFlag))              // for sending back channel cmd, 2014.05.16
                            {
                                if (NodeProfile[node - 1].Back_Command_buf.Count > 0)
                                {
                                    byte[] cmd_buff;
                                    cmd_buff = NodeProfile[node - 1].Back_Command_buf.Dequeue();                     //4
                                    if (cmd_buff[0] != 23)                                                           // // Added by Jason Chen, 2017.03.28
                                    {
                                        if (!USB_or_BLE)
                                            mSendMsgCMD((byte)node, cmd_buff[0]);
                                        else
                                            mSendMsgBleCMD((byte)node, cmd_buff[0]);
                                        KillTimer(this.Handle, 0xB00 + (uint)node);       // Added by Jason Chen, 20140619
                                        System.Diagnostics.Debug.WriteLine("Back Channel Command Sent 4");           // Added by Jason Chen, 2017.03.23
                                    }
                                }
                            }

                            cmdProcess(node, USB_or_BLE); //uploadProcess(node);

                            if (NodeProfile[node - 1].bat_level <= 100)
                            {
                                lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;
                            }
                            else
                            {
                                lpHub.lpDevices[node - 1].battLevel = 100;
                            }
                            //NodeControl[node - 1].PlayerNumber = NodeProfile[node - 1].playerNum;

                            lpHub.lpDevices[node - 1].stopRecording = false;                                 // Added by Jason Chen, 2014.06.03
                            //lpHub.lpDevices[node - 1].playerNumer = NodeProfile[node - 1].playerNum;
                        }
#if USE_ORIGINAL_REFRESH
                        lpHub.dgvLpPopulateView(node);                                                       // 2014.10.29 added
#else
                        NodeProfile[node - 1].register = true;                                               // 2014.10.31
                        this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);             // 2014.10.28 added
#endif
                    }
                }
            }
            else if (plen == (CHUNK_IMMEDIATE + 1))
            {
                //textInfo += "Imm:";
                if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                {
                    var txtBuff = "Node" + node.ToString("D2") + "Sequence: " + seqn;
                    RxLog.logText(txtBuff);
                }
                if ((seqn == 0) && (mlen == 12))
                {
                    NodeProfile[node - 1].gftSummary_buff = new byte[64];
                    Array.Copy(buff, 5, NodeProfile[node - 1].gftSummary_buff, 0, mlen);
                }
                else if ((seqn == 1) && (mlen == 12) && NodeProfile[node - 1].gftSummary_buff[0] == 0xAA)
                {
                    Array.Copy(buff, 5, NodeProfile[node - 1].gftSummary_buff, 12, mlen);
                }
                else if ((seqn == 2) && (mlen == 2) && NodeProfile[node - 1].gftSummary_buff[0] == 0xAA) //
                {
                    // Performance Summary Transmission End

                    // Mark it as a performance GFT.. not the best way, but the only way we can
                    NodeProfile[node - 1].gftType = 1;
                    lpHub.lpDevices[node - 1].gftType = 1;

                    Array.Copy(buff, 5, NodeProfile[node - 1].gftSummary_buff, 24, mlen);
                    if (NodeProfile[node - 1].last_gftSummary_buff == null) NodeProfile[node - 1].last_gftSummary_buff = new byte[39]; // only the first 39 bytes should be compared
                    if (!NodeProfile[node - 1].gftSummary_buff.Take(39).ToArray().SequenceEqual(NodeProfile[node - 1].last_gftSummary_buff))
                    { // Is this a duplicate of the last one? If so, don't increase the count, the GFT just didn't get the ACK
                        NodeProfile[node - 1].gftSummary_buff.Take(39).ToArray().CopyTo(NodeProfile[node - 1].last_gftSummary_buff, 0); // store for later comparisons..

                        if (node >= 1 && node <= NUM_NODES)
                        {
#if true
                            if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = "PT" + node.ToString("D2") + ": Performance Immediate Received";
                                RxLog.logText(txtBuff);
                            }
                            lpHub.selectRow(node - 1);
                            //if (!NodeControl[node - 1].Enabled)
                            //    NodeControl[node - 1].Enabled = true;
                            if (!lpHub.lpDevices[node - 1].live) /*|| (!(NodeControl[node - 1].m_PWR)*/
                            {
                                //NodeControl[node - 1].m_PWR = true;
                                lpHub.lpDevices[node - 1].live = true;
                                lpHub.lpDevices[node - 1].name = NodeProfile[node - 1].firstName.ToCharArray();
                                lpHub.lpDevices[node - 1].temperature = 0;
                                //NodeControl[node - 1].PlayerNumber = NodeProfile[node - 1].playerNum;
                                //NodeControl[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                NodeProfile[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                //NodeControl[node - 1].m_Usb = false;

                                Wireless_dataInit(node);
                            }
                            int alarmThres = NodeProfile[node - 1].gftSummary_buff[19];

                            alarmThres = (alarmThres > 10) ? alarmThres : 10;
                            if (alarmThres != NodeProfile[node - 1].alarmThres)
                            {
                                NodeProfile[node - 1].alarmThres = alarmThres;
                                setGftWirelessNode(NodeProfile[node - 1], false);
                            }


                            //byte mode = 0;
                            //ACC_SENSITIVITY_LSB2
                            double linX = lgDecodeDn((byte)NodeProfile[node - 1].gftSummary_buff[13], (byte)NodeProfile[node - 1].gftSummary_buff[14], 0.007808) * NodeProfile[node - 1].mountCoefficient;
                            double linY = lgDecodeDn((byte)NodeProfile[node - 1].gftSummary_buff[15], (byte)NodeProfile[node - 1].gftSummary_buff[16], 0.007808) * NodeProfile[node - 1].mountCoefficient;
                            double linZ = lgDecodeDn((byte)NodeProfile[node - 1].gftSummary_buff[17], (byte)NodeProfile[node - 1].gftSummary_buff[18], 0.007808) * NodeProfile[node - 1].mountCoefficient;

                            float gForceFloat = (float)Math.Sqrt(linX * linX + linY * linY + linZ * linZ); //(Math.Sqrt(byteToShort((byte)NodeProfile[node - 1].gftSummary_buff[8], (byte)NodeProfile[node - 1].gftSummary_buff[9])) * ACC_SENSITIVITY_LSB2 * NodeProfile[node - 1].mountCoefficient);
                            if (gForceFloat > NodeProfile[node - 1].recThres)
                            {
                                //if ((gForceFloat > lpHub.lpDevices[node - 1].gForce) && (gForceFloat > NodeControl[node - 1].m_Tmp))
                                if ((gForceFloat > lpHub.lpDevices[node - 1].gForce) && (gForceFloat > NodeProfile[node - 1].m_Tmp))
                                {
                                    //NodeControl[node - 1].m_Tmp = gForceFloat;// +61;
                                    NodeProfile[node - 1].m_Tmp = gForceFloat;// +61;
                                    //lpHub.lpDevices[node - 1].gForce = gForceFloat;// +61;   
                                }
                                if (NodeProfile[node - 1].m_Tmp > NodeProfile[node - 1].alarmThres)
                                {
                                    //NodeControl[node - 1].m_Imm = true;
                                    //NodeControl[node - 1].m_Hit = true;
                                    lpHub.lpDevices[node - 1].alarm = true;
                                    lpHub.lpDevices[node - 1].lastAlarmgForce = gForceFloat;
                                }

                                lpHub.lpDevices[node - 1].live = true;
                                lpHub.lpDevices[node - 1].impacts++;
                                double rotX = /*(short)*/(gyroDecode(NodeProfile[node - 1].gftSummary_buff[20], NodeProfile[node - 1].gftSummary_buff[21])) * NodeProfile[node - 1].mountCoefficient;
                                double rotY = /*(short)*/(gyroDecode(NodeProfile[node - 1].gftSummary_buff[22], NodeProfile[node - 1].gftSummary_buff[23])) * NodeProfile[node - 1].mountCoefficient;
                                double rotZ = /*(short)*/(gyroDecode(NodeProfile[node - 1].gftSummary_buff[24], NodeProfile[node - 1].gftSummary_buff[25])) * NodeProfile[node - 1].mountCoefficient;
                                rotX = Math.Round(rotX, 1);
                                rotY = Math.Round(rotY, 1);
                                rotZ = Math.Round(rotZ, 1);
                                double Rotation = (double)Math.Sqrt(rotX * rotX + rotY * rotY + rotZ * rotZ);
                                if (Rotation >= (2000 * 0.9) || gForceFloat >= (NodeProfile[node - 1].alarmThres * 0.9))
                                {
                                    lpHub.lpDevices[node - 1].reviewYellow = true;
                                    lpHub.lpDevices[node - 1].thresholdImpacts++;
                                    lpHub.lpDevices[node - 1].lastThresholdTime = DateTime.UtcNow;

                                    if (Rotation < 2000 && gForceFloat < NodeProfile[node - 1].alarmThres)
                                    {
                                        this.withinThreshold++;
                                        lpHub.lpDevices[node - 1].withinThreshold++;
                                    }
                                    else
                                    {
                                        this.aboveThreshold++;
                                        lpHub.lpDevices[node - 1].aboveThreshold++;
                                    }
                                    DateTime outTime;
                                    ushort myShort = byteToShort(NodeProfile[node - 1].gftSummary_buff[7], NodeProfile[node - 1].gftSummary_buff[8]);
                                    if (myShort >= 1000)
                                        myShort = 999;


                                    if ((gForceFloat > NodeProfile[node - 1].alarmThres) || Rotation > 2000)
                                    {
                                        lpHub.lpDevices[node - 1].lastAlarmTime = lpHub.lpDevices[node - 1].lastThresholdTime; // outTime;
                                        lpHub.lpDevices[node - 1].reviewRed = true;
                                        //NodeControl[node - 1].m_Hit = true;
                                        lpHub.lpDevices[node - 1].alarm = true;                // Added by Jason Chen, 2014.05.15
                                        lpHub.lpDevices[node - 1].lastAlarmgForce = gForceFloat;
                                        lpHub.lpDevices[node - 1].lastAlarmImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 
                                    }
                                }
                                if (Rotation > lpHub.lpDevices[node - 1].Rotation)
                                {
                                    lpHub.lpDevices[node - 1].Rotation = Rotation;
                                    lpHub.lpDevices[node - 1].highestRotImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 = first
                                }
                                if (gForceFloat > lpHub.lpDevices[node - 1].gForce)
                                {
                                    lpHub.lpDevices[node - 1].gForce = gForceFloat;
                                    lpHub.lpDevices[node - 1].highestGImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 = first
                                }


                                // New Impact received for this node, update the node profile to mark data available for download
                                NodeProfile[node - 1].newDataOnDevice = true;
                                Args[node - 1].sender = this;
                                Args[node - 1].mHitFlag = false;
                                Args[node - 1].sumbuff = NodeProfile[node - 1].gftSummary_buff;
                                // Added by Jason Chen, 2014.02.21 for bug fix
                                this.Invoke(new ImpactPacketProcess(lpHub.lpDevices[node - 1].PerformanceSummaryPacketProcess), Args[node - 1]);
                                KillTimer(this.Handle, (uint)(0x700 + node));
                                //NodeControl[node - 1].DataActive = true;
                                lpHub.lpDevices[node - 1].lastReceiveTime = DateTime.UtcNow;
                                if (!lpHub.lpDevices[node - 1].firstReceiveSet)
                                {
                                    lpHub.lpDevices[node - 1].firstReceiveSet = true;
                                    lpHub.lpDevices[node - 1].firstReceiveTime = DateTime.UtcNow;
                                }
                                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);             // 2014.10.26 added


#else
  
#if USE_ORIGINAL_REFRESH
 
#else

#endif

#endif
#if USE_ORIGINAL_REFRESH
                                                    // 2014.10.29 added
#else
                                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);             // 2014.10.26 added
#endif
                            }
                        }
                    }

                }
                else if ((seqn == 1) && (mlen == 2) )//&& NodeProfile[node - 1].gftSummary_buff[0] == 0x55)
                {
                    try
                    {
                        if (NodeProfile[node - 1].gftSummary_buff[0] == 0x55)
                        {
                            // Mark it as a performance GFT.. not the best way, but the only way we can
                            NodeProfile[node - 1].gftType = 1;
                            lpHub.lpDevices[node - 1].gftType = 1;

                            // Performance Bucket data Transmission End
                            Array.Copy(buff, 5, NodeProfile[node - 1].gftSummary_buff, 12, mlen);
                            if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = "PT" + node.ToString("D2") + ": Immediate / Performance Bucket Data Received";
                                RxLog.logText(txtBuff);
                            }
                            // Performance Bucket data - 13 Bytes - 2 Packets?
                            UInt32 player_load_raw = BitConverter.ToUInt32(new byte[4] { NodeProfile[node - 1].gftSummary_buff[4], NodeProfile[node - 1].gftSummary_buff[3], NodeProfile[node - 1].gftSummary_buff[2], NodeProfile[node - 1].gftSummary_buff[1] }, 0);
                            int rpe = (int)NodeProfile[node - 1].gftSummary_buff[5];
                            UInt32 explosive_count_raw = BitConverter.ToUInt32(new byte[4] { NodeProfile[node - 1].gftSummary_buff[9], NodeProfile[node - 1].gftSummary_buff[8], NodeProfile[node - 1].gftSummary_buff[7], NodeProfile[node - 1].gftSummary_buff[6] }, 0);
                            UInt32 explosive_power_raw = BitConverter.ToUInt32(new byte[4] { NodeProfile[node - 1].gftSummary_buff[13], NodeProfile[node - 1].gftSummary_buff[12], NodeProfile[node - 1].gftSummary_buff[11], NodeProfile[node - 1].gftSummary_buff[10] }, 0);
                            UInt32 explosive_count = explosive_count_raw;
                            UInt32 explosive_power = explosive_power_raw;
                            UInt32 player_load = player_load_raw;
                            // Adjust our values based on identified oddities, below we adjust for accumulation
                            /*foreach (RPE_DATA prevData in lpHub.lpDevices[node - 1].rpeList)
                            {
                                player_load -= prevData.player_load;
                                explosive_count -= prevData.explosive_count;
                                explosive_power -= prevData.explosive_power;
                            }*/
                            bool isFirst = false;
                            UInt32 lr_explosive_count_raw = 0;
                            UInt32 lr_explosive_power_raw = 0;
                            UInt32 lr_player_load_raw = 0;
                            UInt32 temp_explosive_count = 0;
                            UInt32 temp_explosive_power = 0;
                            UInt32 temp_player_load = 0;
                            if (lpHub.lpDevices[node - 1].rpeList.Count() == 0 || player_load_raw < lpHub.lpDevices[node - 1].rpeList.Last().player_load_raw)
                            {
                                isFirst = true;
                            }
                            else
                            {
                                // Player Load has decreased in value, so, the device must have been off
                                if (lpHub.lpDevices[node - 1].rpeList.Count() > 0)
                                {
                                    player_load = player_load_raw - lpHub.lpDevices[node - 1].rpeList.Last().player_load_raw;
                                    explosive_count = explosive_count_raw - lpHub.lpDevices[node - 1].rpeList.Last().explosive_count_raw;
                                    explosive_power = explosive_power_raw - lpHub.lpDevices[node - 1].rpeList.Last().explosive_power_raw;
                                    lr_explosive_count_raw = lpHub.lpDevices[node - 1].rpeList.Last().explosive_count_raw;
                                    lr_explosive_power_raw = lpHub.lpDevices[node - 1].rpeList.Last().explosive_power_raw;
                                    lr_player_load_raw = lpHub.lpDevices[node - 1].rpeList.Last().player_load_raw;
                                }
                            }

                            // Sum the explosive_power values, we will use this as the "raw" value to account for devices rebooting
                            player_load_raw = 0;
                            foreach (RPE_DATA prevData in lpHub.lpDevices[node - 1].rpeList) player_load_raw += prevData.player_load;
                            lr_player_load_raw = player_load_raw;


                            double dRpe = Math.Round(((double)rpe / 10), 0);
                            DateTime thisReportTime = DateTime.UtcNow;
                            // Find out how many 10 second periods have elapsed since the last report
                            int intervals = 1;
                            if (lpHub.lpDevices[node - 1].rpeList.Count() > 0)
                            {
                                TimeSpan sinceLast = thisReportTime.Subtract(lpHub.lpDevices[node - 1].rpeList.Last().report_time);
                                intervals = (int)Math.Round(sinceLast.TotalSeconds / 10) - 1;
                            }
                            if (intervals < 1) intervals = 1;
                            temp_explosive_count = (UInt32)Math.Round((double)(explosive_count / intervals));
                            temp_explosive_power = (UInt32)Math.Round((double)(explosive_power / intervals));
                            temp_player_load = (UInt32)Math.Round((double)(player_load / intervals));
                            for (int i = 0; i < intervals; i++)
                            {
                                lr_explosive_count_raw += temp_explosive_count;
                                lr_explosive_power_raw += temp_explosive_power;
                                lr_player_load_raw += temp_player_load;
                                if (player_load < 1000) // sometimes we get garbage data.. 
                                {
                                    lpHub.lpDevices[node - 1].rpeList.Add(new RPE_DATA { rpe = dRpe, player_load = temp_player_load, explosive_count = temp_explosive_count, explosive_power = temp_explosive_power, report_time = thisReportTime.AddSeconds(-(intervals - i) * 10), player_load_raw = lr_player_load_raw, explosive_count_raw = lr_explosive_count_raw, explosive_power_raw = lr_explosive_power_raw, first_after_on = isFirst });
                                    lpHub.lpDevices[node - 1].activeTime += 10 * 3000; // Maintain illusion of collectiving active time data.. even though we really aren't!
                                    lpHub.lpDevices[node - 1].cumulativeRPE += dRpe;
                                }
                            }
                            //explosive_power = explosive_power * explosive_count; // Explosive power should be multiplied by the count? That's what they said.

                            //UInt16 _player_load = BitConverter.ToUInt16(new byte[2] { NodeProfile[node - 1].gftSummary_buff[2], NodeProfile[node - 1].gftSummary_buff[1] }, 0);
                            /*if (player_load < 1000) // sometimes we get garbage data.. 
                            {
                                lpHub.lpDevices[node - 1].rpeList.Add(new RPE_DATA { rpe = dRpe, player_load = player_load, explosive_count = explosive_count, explosive_power = explosive_power, report_time = thisReportTime, player_load_raw = player_load_raw, explosive_count_raw = explosive_count_raw, explosive_power_raw = explosive_power_raw, first_after_on = isFirst });
                                lpHub.lpDevices[node - 1].activeTime += 10 * 3000; // Maintain illusion of collectiving active time data.. even though we really aren't!
                                lpHub.lpDevices[node - 1].cumulativeRPE += dRpe;
                            }*/
                            if (node == lastNodeSelected.NodeID || !splitContainer3.Panel1Collapsed)
                            {
                                if (splitContainer3.Panel1Collapsed)
                                {
                                    wirelessViewer_OnNewData(new object(), new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, node));
                                }
                                else if (lastRefresh.AddSeconds(15) > DateTime.UtcNow)
                                {
                                    lastRefresh = DateTime.UtcNow;
                                    mWirelessViewer_OnRowSelectedByList(new object(), new WirelessGFTViewer.NodeOnSelectedArgs(0, 0, node), null/*dataGridView1*/, dataGridView3, teamChart); //
                                }
                            }
                        }
                    }
                    catch (Exception cc)
                    {
                        string ccc = cc.Message;
                        //MessageBox.Show(ccc);
                    }
                }
                else if ((seqn == 1) && (mlen == 11)) // Summary Transmission End
                {
                    Array.Copy(buff, 5, NodeProfile[node - 1].gftSummary_buff, 12, mlen);
                    if (NodeProfile[node - 1].last_gftSummary_buff == null) NodeProfile[node - 1].last_gftSummary_buff = new byte[39]; // only the first 39 bytes should be compared
                    if (!NodeProfile[node - 1].gftSummary_buff.Take(39).ToArray().SequenceEqual(NodeProfile[node - 1].last_gftSummary_buff))
                    { // Is this a duplicate of the last one? If so, don't increase the count, the GFT just didn't get the ACK
                        NodeProfile[node - 1].gftSummary_buff.Take(39).ToArray().CopyTo(NodeProfile[node - 1].last_gftSummary_buff, 0); // store for later comparisons..

                        if (node >= 1 && node <= NUM_NODES)
                        {
#if true
                            if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = "PT" + node.ToString("D2") + ": Immediate Received";
                                RxLog.logText(txtBuff);
                            }
                            lpHub.selectRow(node - 1);
                            //if (!NodeControl[node - 1].Enabled)
                            //    NodeControl[node - 1].Enabled = true;
                            if (!lpHub.lpDevices[node - 1].live) //|| (!(NodeControl[node - 1].m_PWR)))
                            {
                                //NodeControl[node - 1].m_PWR = true;
                                lpHub.lpDevices[node - 1].live = true;
                                lpHub.lpDevices[node - 1].name = NodeProfile[node - 1].firstName.ToCharArray();
                                lpHub.lpDevices[node - 1].temperature = 0;
                                //NodeControl[node - 1].PlayerNumber = NodeProfile[node - 1].playerNum;
                                //NodeControl[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                NodeProfile[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                //NodeControl[node - 1].m_Usb = false;

                                Wireless_dataInit(node);
                            }
                            int alarmThres = NodeProfile[node - 1].gftSummary_buff[15];

                            alarmThres = (alarmThres > 10) ? alarmThres : 10;
                            if (alarmThres != NodeProfile[node - 1].alarmThres)
                            {
                                NodeProfile[node - 1].alarmThres = alarmThres;
                                setGftWirelessNode(NodeProfile[node - 1], false);
                            }

                            float gForceFloat = (float)(Math.Sqrt(byteToShort((byte)NodeProfile[node - 1].gftSummary_buff[8], (byte)NodeProfile[node - 1].gftSummary_buff[9])) * ACC_SENSITIVITY_LSB2 * NodeProfile[node - 1].mountCoefficient);
                            //if ((gForceFloat > lpHub.lpDevices[node - 1].gForce) && (gForceFloat > NodeControl[node - 1].m_Tmp))
                            if ((gForceFloat > lpHub.lpDevices[node - 1].gForce) && (gForceFloat > NodeProfile[node - 1].m_Tmp))
                            {
                                //NodeControl[node - 1].m_Tmp = gForceFloat;// +61;
                                NodeProfile[node - 1].m_Tmp = gForceFloat;// +61;
                                //lpHub.lpDevices[node - 1].gForce = gForceFloat;// +61;   
                            }


                            //if (NodeControl[node - 1].m_Tmp > NodeProfile[node - 1].alarmThres)
                            if (NodeProfile[node - 1].m_Tmp > NodeProfile[node - 1].alarmThres)
                            {
                                //NodeControl[node - 1].m_Imm = true;
                                //NodeControl[node - 1].m_Hit = true;
                                lpHub.lpDevices[node - 1].alarm = true;
                                lpHub.lpDevices[node - 1].lastAlarmgForce = gForceFloat;
                            }

                            lpHub.lpDevices[node - 1].live = true;
                            lpHub.lpDevices[node - 1].impacts++;
                            double rotX = /*(short)*/(gyroDecode(NodeProfile[node - 1].gftSummary_buff[16], NodeProfile[node - 1].gftSummary_buff[17])) * NodeProfile[node - 1].mountCoefficient;
                            double rotY = /*(short)*/(gyroDecode(NodeProfile[node - 1].gftSummary_buff[18], NodeProfile[node - 1].gftSummary_buff[19])) * NodeProfile[node - 1].mountCoefficient;
                            double rotZ = /*(short)*/(gyroDecode(NodeProfile[node - 1].gftSummary_buff[20], NodeProfile[node - 1].gftSummary_buff[21])) * NodeProfile[node - 1].mountCoefficient;
                            rotX = Math.Round(rotX, 1);
                            rotY = Math.Round(rotY, 1);
                            rotZ = Math.Round(rotZ, 1);
                            double Rotation = (double)Math.Sqrt(rotX * rotX + rotY * rotY + rotZ * rotZ);
                            if (Rotation >= (2000 * 0.9) || gForceFloat >= (NodeProfile[node - 1].alarmThres * 0.9))
                            {
                                lpHub.lpDevices[node - 1].reviewYellow = true;
                                lpHub.lpDevices[node - 1].thresholdImpacts++;
                                lpHub.lpDevices[node - 1].lastThresholdTime = DateTime.UtcNow;

                                if (Rotation < 2000 && gForceFloat < NodeProfile[node - 1].alarmThres)
                                {
                                    this.withinThreshold++;
                                    lpHub.lpDevices[node - 1].withinThreshold++;
                                }
                                else
                                {
                                    this.aboveThreshold++;
                                    lpHub.lpDevices[node - 1].aboveThreshold++;
                                }
                                DateTime outTime;
                                ushort myShort = byteToShort(NodeProfile[node - 1].gftSummary_buff[6], NodeProfile[node - 1].gftSummary_buff[7]);
                                if (myShort >= 1000)
                                    myShort = 999;

                                /*if (ValidateTime(out outTime, NodeProfile[node - 1].gftSummary_buff[0], NodeProfile[node - 1].gftSummary_buff[1], NodeProfile[node - 1].gftSummary_buff[2],
                                                              NodeProfile[node - 1].gftSummary_buff[3], NodeProfile[node - 1].gftSummary_buff[4], NodeProfile[node - 1].gftSummary_buff[5],
                                                              myShort))
                                {
                                    //lpHub.lpDevices[node - 1].lastAlarmTime = outTime;
                                    lpHub.lpDevices[node - 1].lastThresholdTime = outTime;
                                }
                                else
                                {
                                    outTime = DateTime.UtcNow;
                                }*/
                                if ((gForceFloat > NodeProfile[node - 1].alarmThres) || Rotation > 2000)
                                {
                                    lpHub.lpDevices[node - 1].lastAlarmTime = lpHub.lpDevices[node - 1].lastThresholdTime; // outTime;
                                    lpHub.lpDevices[node - 1].reviewRed = true;
                                    //NodeControl[node - 1].m_Hit = true;
                                    lpHub.lpDevices[node - 1].alarm = true;                // Added by Jason Chen, 2014.05.15
                                    lpHub.lpDevices[node - 1].lastAlarmgForce = gForceFloat;
                                    lpHub.lpDevices[node - 1].lastAlarmImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 
                                }
                            }
                            if (Rotation > lpHub.lpDevices[node - 1].Rotation)
                            {
                                lpHub.lpDevices[node - 1].Rotation = Rotation;
                                lpHub.lpDevices[node - 1].highestRotImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 = first
                            }
                            if (gForceFloat > lpHub.lpDevices[node - 1].gForce)
                            {
                                lpHub.lpDevices[node - 1].gForce = gForceFloat;
                                lpHub.lpDevices[node - 1].highestGImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 = first
                            }


                            // New Impact received for this node, update the node profile to mark data available for download
                            NodeProfile[node - 1].newDataOnDevice = true;
                            Args[node - 1].sender = this;
                            Args[node - 1].mHitFlag = false;
                            Args[node - 1].sumbuff = NodeProfile[node - 1].gftSummary_buff;
                            // Added by Jason Chen, 2014.02.21 for bug fix
                            this.Invoke(new ImpactPacketProcess(lpHub.lpDevices[node - 1].SummaryPacketProcess), Args[node - 1]);
                            KillTimer(this.Handle, (uint)(0x700 + node));
                            //NodeControl[node - 1].DataActive = true;
                            lpHub.lpDevices[node - 1].lastReceiveTime = DateTime.UtcNow;
                            if (!lpHub.lpDevices[node - 1].firstReceiveSet)
                            {
                                lpHub.lpDevices[node - 1].firstReceiveSet = true;
                                lpHub.lpDevices[node - 1].firstReceiveTime = DateTime.UtcNow;
                            }
                            this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);             // 2014.10.26 added


#else
                        if (buff[5] == 0x01)
                        {
                            //UpdateNodeControlLayout(node);                            //Commented by Jason Chen, 2013.12.17

                            lpHub.selectRow(node - 1);

                            if (!NodeControl[node - 1].Enabled)
                                NodeControl[node - 1].Enabled = true;

                            if ((!lpHub.lpDevices[node - 1].live) || (!(NodeControl[node - 1].m_PWR)))
                            {
                                //PowerOnProcess(node);                      // Removed by Jason Chen, 2014.05.21

                                NodeControl[node - 1].m_PWR = true;
                                lpHub.lpDevices[node - 1].live = true;
                                lpHub.lpDevices[node - 1].name = NodeProfile[node - 1].firstName.ToCharArray();
                                lpHub.lpDevices[node - 1].temperature = 0;
#if USE_ORIGINAL_REFRESH
                            //lpHub.dgvLpPopulateView(node - 1); // 2014.10.01
 
#else
                                //this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node - 1);
                                //this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);     // Modified, 2014.10.26
#endif

                                //lpHub.lpDevices[node - 1].updateDgv = true;


                                NodeControl[node - 1].PlayerNumber = NodeProfile[node - 1].playerNum;
                                NodeControl[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                //NodeControl[node - 1].m_Hit        = false;
                                NodeControl[node - 1].m_Usb = false;
                                //NodeControl[node - 1].m_Imm        = false;
                                //NodeControl[node - 1].m_Tmp        = 0;

                                Wireless_dataInit(node);
                            }

                            //float gForceFloat = (float)(Math.Sqrt(byteToShort((byte)buff[6], (byte)buff[7])) * (ACC_SENSITIVITY_LSB * 4));
                            int alarmThres = buff[10];

                            alarmThres = (alarmThres > 10) ? alarmThres : 10;
                            if (alarmThres != NodeProfile[node - 1].alarmThres)
                            {
                                NodeProfile[node - 1].alarmThres = alarmThres;
                                setGftWirelessNode(NodeProfile[node - 1], false);
                            }

                            float gForceFloat = (float)(Math.Sqrt(byteToShort((byte)buff[6], (byte)buff[7])) * ACC_SENSITIVITY_LSB2);
                            if ((gForceFloat > lpHub.lpDevices[node - 1].gForce) && (gForceFloat > NodeControl[node - 1].m_Tmp))
                            {
                                NodeControl[node - 1].m_Tmp = gForceFloat;// +61;
                                //lpHub.lpDevices[node - 1].gForce = gForceFloat;// +61;   
                            }


                            if (NodeControl[node - 1].m_Tmp > NodeProfile[node - 1].alarmThres)
                            {
                                //NodeControl[node - 1].m_Imm = true;
                                NodeControl[node - 1].m_Hit = true;
                                lpHub.lpDevices[node - 1].alarm = true;
                                lpHub.lpDevices[node - 1].lastAlarmgForce = gForceFloat;
                            }

                            lpHub.lpDevices[node - 1].live = true;

                            //Args[node - 1].recvFIFO.Clear();                          // Added by Jason Chen, 2013.12.19
                            // Changed by Jason Chen , 2014.01.09
                            //SetTimer(this.Handle, 0x400 + node, 1000, null);
                        }
#endif
#if USE_ORIGINAL_REFRESH
                    lpHub.dgvLpPopulateView(node);                                                       // 2014.10.29 added
#else
                            this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);             // 2014.10.26 added
#endif
                        }
                    }
                }
            }
            else if (plen == (CHUNK_HIT_DATA + 1))
            {
                /////////////////////////////////////////////////////////////// 2014.10.31
                //if(!lpHub.lpDevices[node - 1].valid)
                //{
                //    mSendMsgCMD((byte)node, 21);                             // re-boot GFT, Stop Uploading
                //Thread.Sleep(80);
                //mSendMsgCMD((byte)node, 21);                             // re-boot GFT, Stop Uploading
                //Thread.Sleep(80);
                //mSendMsgCMD((byte)node, 21);                             // re-boot GFT, Stop Uploading
                //Thread.Sleep(80);
                //    this.BeginInvoke((MethodInvoker)delegate { m_button_NodeInfo_Click(this, null); });     // Adde by Jason Chen, 2014.10.31
                //    return;
                //}
                /////////////////////////////////////////////////////////////// 2014.10.31

                //textInfo += "Hit:   ";
                if (node >= 1 && node <= NUM_NODES)
                {
                    //if (!NodeControl[node - 1].m_Usb)
                    //{
                    //    if (!NodeControl[node - 1].m_PWR)
                    //    {
                    //NodeControl[node - 1].m_PWR = true;
                    //Wireless_dataInit(node);
                    //    }
                    //NodeControl[node - 1].m_Hit = true;
                    //SetTimer(this.Handle, 0x400 + node, 1000, null);
                    //}
                    m_wHitCount[node - 1] += buff[4] & 0x0f;
                    m_dwUploadCount[node - 1] += buff[4] & 0x0f;
                    //NodeControl[node - 1].m_Upld_Count = m_dwUploadCount[node - 1];
                }

                if ((Args[node - 1].impactFlag) && (Args[node - 1].nodeID == node))
                {
                    int lenc = buff[4] & 0x0f;
                    string temps = "";
                    for (int kk = 0; kk < lenc; kk++)
                    {
                        Args[node - 1].recvbuff[Args[node - 1].recv_count + kk] = buff[kk + 5];
                        temps += string.Format("{0:x2} ", buff[kk + 5]).ToUpper();
                    }
                    Args[node - 1].recvbuffstr += temps;
                    Args[node - 1].recv_count += lenc;
                    //m_Edit_Immediate.AppendText(temps + "\r\n");

                    temps = node.ToString("X2") + " - " + temps;
                    toolStripStatusLabelChart.Text = temps;
                    //wirelessViewer.DebugCmdBoxDisplay(temps + "\r\n");  2014.10.28
                }
                else

                //if((Args[node - 1].nodeID == node))
                {
                    KillTimer(this.Handle, (uint)(0x700 + node));

                    KillTimer(this.Handle, (uint)(0x160 + node));

                    int lenc = buff[4] & 0x0f;
                    int seq = buff[4] & 0xf0;
                    string temps = "";

                    //bool month = (buff[7] >= 1) && (buff[7] <= 12);
                    //bool hour = (buff[9] >= 0) && (buff[9] < 24);

                    bool month = (buff[6] >= 00) && (buff[6] < 100);            // year                    ,2017.04.04             This year 00 or 16??????????   <= 99 ?????????????, 2017.06.20
                    month = month && (buff[7] > 0) && (buff[7] < 13);          // month                   ,2017.04.04
                    bool hour = (buff[8] > 0) && (buff[8] < 32);               // day                     ,2017.04.04
                    hour = hour && (buff[9] >= 0) && (buff[9] < 24);           // hour                    ,2017.04.04
                    hour = hour && (buff[10] >= 0) && (buff[10] < 60);         // minute                  ,2017.04.04
                    hour = hour && (buff[11] >= 0) && (buff[11] < 60);         // second                  ,2017.04.04

                    if ((buff[5] == 0x49))
                    {
                    }
                    if (Args[node - 1].recvbuff == null)
                        return;

                    if (((buff[5] == 0x48) || (buff[5] == 0x58) || (buff[5] == 0x4c) || (buff[5] == 0x49) || (buff[5] == 0x4A)) && (seq == 0) && month && hour)
                    {
                        // Data is being sent from the device, unset the getImpacts flag
                        lpHub.lpDevices[node - 1].getImpactsFlag = false;
                        //NodeControl[node - 1].setChkGetImpacts(false);


                        //byte[] recvBuff = new byte[IMPACT_DATA_LEN];
                        //Args[node - 1].recvQueue.Add(recvBuff);
                        //if ((seq == 0) && (month) && (hour))
                        Args[node - 1].mHitFlag = true;

                        if (buff[5] == 0x58)
                        {
                            //NodeProfile[node - 1].uploadNum_Flag = 1;
                            //lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;

                            //if (getUploadNodeNum() > NUM_UPLOAD_LIMIT)
                            if (NodeProfile[node - 1].uploadNum_Flag == 0)
                            {
                                //09.15                         mSendMsgCMD((byte)node, 21);                               // reboot GFT        
                                //09.15                         m_Edit_RxPacket.AppendText("---> " + node.ToString("D2") + " Send 21 cmd to reboot GFT--03----\r\n");//         2014.09.02   
                                //09.15                         ErrorCount.Text = (++error_count).ToString();      
                                //Thread.Sleep(50);
                                //mSendMsg((byte)node, 21);                             // reboot GFT                   
                                //Thread.Sleep(50);
                                //mSendMsg((byte)node, 21);                             // reboot GFT                   
                                //Thread.Sleep(50);
                            }
                        }

                        m_bHitSeqNo[node - 1] = 1;

                        for (int kk = 0; kk < lenc; kk++)
                        {
                            Args[node - 1].recvbuff[kk] = buff[kk + 5];
                            //recvBuff[kk] = buff[kk + 5];
                            temps += string.Format("{0:x2} ", buff[kk + 5]).ToUpper();
                        }
                        Args[node - 1].recv_count = lenc;
                        Args[node - 1].recvbuffstr = temps;

                        //temps = "Receiving Data...";  2014.10.28
                        //toolStripStatusLabelChart.Text = temps;
                        //MyMarshalToForm("toolStripStatusLabelChart", temps, "YellowGreen");
                        //MyMarshalToForm("toolStripStatusLabelChart", temps, Color.YellowGreen);    2014.10.28
                        //wirelessViewer.DebugCmdBoxDisplay(temps + "\r\n");   2014.10.28

                        //SetTimer(this.Handle, 0x700 + node, 1500, null);

                        //string toolStripStatdddusLabelGFTstr = "Playe Number " + NodeProfile[node - 1].playerNum.ToString();
                        //toolStripStatusLabelChart.BackColor = Color.Yellow;

                        //MyMarshalToForm("toolStripStatusLabelGFT", toolStripStatdddusLabelGFTstr, System.Drawing.Color.Transparent);

                        if (Args[node - 1].recvbuff[0] == 0x58)                                         //Added by Jason, 2014.09.11
                        {
#if false
                            mSendMsgCMD((byte)node, 0x56);                                              // Ack to GFT 0x56     , 2014.09.11
                            m_Edit_RxPacket.AppendText("--->" + node.ToString("D2") + " Send 86 cmd to Head ACK Comfirmed Seq--" + seqn.ToString() + "\r\n");// 2014.09.11   
#endif
                        }

                        //NodeControl[node - 1].DataActive = true;

                        //SetTimer(this.Handle, 0x900 + node, 50, null);
                    }
                    else
                    {
                        if (Args[node - 1].mHitFlag)
                        {
                            //byte[] recvBuff = Args[node - 1].recvQueue[0];
                            if (Args[node - 1].recvbuff[0] == 0x58 || Args[node - 1].recvbuff[0] == 0x5c)
                            {
                                //NodeProfile[node - 1].uploadNum_Flag = 1;
                                //lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                if (NodeProfile[node - 1].uploadNum_Flag == 0)
                                {
                                    //09.15                      mSendMsgCMD((byte)node, 21);                             // reboot GFT                   
                                    //Thread.Sleep(50);
                                    //mSendMsg((byte)node, 21);                             // reboot GFT                   
                                    //Thread.Sleep(50);
                                    //mSendMsg((byte)node, 21);                             // reboot GFT                   
                                    //Thread.Sleep(50);

                                    //09.15                      return;                                                                 // 2014.09.11
                                }
                            }

                            if (seqn != m_bHitSeqNo[node - 1])
                            {

                                temps = node.ToString("X2") + " - " + temps;
                                //toolStripStatusLabelChart.Text = temps;                                       // 09.11
                                //MyMarshalToForm("toolStripStatusLabelChart", temps + "Packet Sequence Error...", "GreenYellow");
                                MyMarshalToForm("toolStripStatusLabelChart", temps + "Retrying Packet...", Color.GreenYellow);
                                SetTimer(this.Handle, 0x900 + node, 50, null);   // 2014.10.28
                                //wirelessViewer.DebugCmdBoxDisplay(temps + "\r\n");                            // 09.11

                                Args[node - 1].mHitFlag = false;
                                m_bHitSeqNo[node - 1] = 1;
                                Args[node - 1].recv_count = 0;
                                Args[node - 1].recvbuffstr = "";

                                //NodeControl[node - 1].DataActive = false;

                                //NodeProfile[node - 1].uplolaNum_Flag = 0;                         // uploading gft number limit
                                //lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uplolaNum_Flag;
                                //NodeProfile[node - 1].uploadFlag = false;                        // Upload flag for forcing upload
                                //NodeProfile[node - 1].impactNum = 0;                              // Total number of Packet
                                //NodeProfile[node - 1].impactCount = 0;                            // Count packet received
                                //NodeProfile[node - 1].deleteFlag = false;

                                //mSendMsgCMD((byte)node, 21);                                        // reboot GFT      , 2014.09.02   
                                //m_Edit_RxPacket.AppendText("--->Send 21 cmd to reboot GFT--01----\r\n");//               2014.09.02   

                                if (Args[node - 1].recvbuff[0] == 0x58)                                                          //Added by Jason, 2014.09.11
                                {
                                    System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Sending Re-Send Command===>02");
                                    if (!USB_or_BLE)
                                        mSendMsgCMD((byte)node, 0xD6);                                                                                                  // ReSend the Impact,  2014.09.11
                                    else
                                        mSendMsgBleCMD((byte)node, 0xD6);
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))    //2017.04.03
                                    {
                                        var txtBuff = "########>>PT" + node.ToString("D2") + ": Download - Sending ReSend Command(0xD6)====>SeqError######## 02";   //2017.04.03
                                        RxLog.logText(txtBuff);
                                    }

                                    //m_Edit_RxPcket.AppendText("---> " + node.ToString("D2") + " Send 0xD6 cmd to ReSend Current Impact SeqE -- 01----\r\n");      // 2014.09.11   
                                    //ErrorCount.Text = (++error_count).ToString();
                                }


                                //Thread.Sleep(50);
                                //mSendMsg((byte)node, 21);                             // reboot GFT                   
                                //Thread.Sleep(50);
                                //mSendMsg((byte)node, 21);                             // reboot GFT                   
                                //Thread.Sleep(50);
                                return;

                            }
                            else
                            {
                                m_bHitSeqNo[node - 1]++;
                                if (m_bHitSeqNo[node - 1] > 15)
                                    m_bHitSeqNo[node - 1] = 0;
                            }

                            temps = "";
                            for (int kk = 0; kk < lenc; kk++)
                            {

                                if (Args[node - 1].recv_count + kk <= IMPACT_DATA_LEN)
                                    Args[node - 1].recvbuff[Args[node - 1].recv_count + kk] = buff[kk + 5];   // Could reach to maxium index, why ?
                                else
                                {
                                    Args[node - 1].mHitFlag = false;
                                    m_bHitSeqNo[node - 1] = 1;

                                    temps = node.ToString("X2") + " - " + temps;
                                    toolStripStatusLabelChart.Text = temps;
                                    //MyMarshalToForm("toolStripStatusLabelChart", temps + "Packet length more " + IMPACT_DATA_LEN.ToString() + " Error...", "GreenYellow"); //09.11
                                    MyMarshalToForm("toolStripStatusLabelChart", temps + "Packet length more " + IMPACT_DATA_LEN.ToString() + " Error...", Color.GreenYellow); //09.11
                                    SetTimer(this.Handle, 0x900 + node, 50, null);   //2014.10.28
                                    //wirelessViewer.DebugCmdBoxDisplay(temps + "\r\n");2014.10.28

                                    if (Args[node - 1].recvbuff[0] == 0x58)                                                     //Added by Jason, 2014.09.11
                                    {
                                        System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Sending Re-Send Command===>03");
                                        if (!USB_or_BLE)
                                            mSendMsgCMD((byte)node, 0xD6);
                                        else
                                            mSendMsgBleCMD((byte)node, 0xD6);
                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))    //2017.04.03
                                        {
                                            var txtBuff = "########>>PT" + node.ToString("D2") + ": Download - Sending ReSend Command(0xD6) ====>ImpactLenError########## 03";      //2017.04.03
                                            RxLog.logText(txtBuff);
                                        }
                                        //m_Edit_RxPcket.AppendText("--->Send 21 cmd to ReSend Current Impact--06----\r\n");     // 2014.09.11
                                        return;                                                                                 // 2014.09.11
                                    }
                                }

                                temps += string.Format("{0:x2} ", buff[kk + 5]).ToUpper();
                            }
                            Args[node - 1].recv_count += lenc;
                            Args[node - 1].recvbuffstr += temps;


                            if (Args[node - 1].recvbuff[0] == 0x49 || Args[node - 1].recvbuff[0] == 0x4A)
                            {
                                if (Args[node - 1].recv_count == ACC_START_DATA_LEN)
                                {
                                    // Commented by Jason Chen, 2017.03.31
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "========>>PT";
                                        if (Args[node - 1].recvbuff[0] == 0x49)
                                             txtBuff += node.ToString("D2") + ": Download - Session Start Received(";
                                        else if (Args[node - 1].recvbuff[0] == 0x4A)
                                            txtBuff += node.ToString("D2") + ": Download - Session End   Received(";
                                        else
                                            txtBuff += node.ToString("D2") + ": Download - Unkonwn       Received(";
                                        txtBuff += (NodeProfile[node - 1].sessionCount + 1) + "/" + NodeProfile[node - 1].sessionNum + ")";
                                        RxLog.logText(txtBuff);
                                    }
                                    // Commented by Jason Chen, 2017.03.31

                                    // Moved to the place by Jason Chen, 2017.04.03
                                    System.Diagnostics.Debug.WriteLine("Sending Ack to Node: " + node + " For " + Args[node - 1].recvbuff[0].ToString());
                                    if (!USB_or_BLE)
                                        mSendMsgCMD((byte)node, 0x56);
                                    else
                                    {
                                        mSendMsgBleCMD((byte)node, 0x56);
                                    }
                                    ////////////////////////////////////////////////

                                    bool ignoreSesStart = false;
                                    if (Args[node - 1].recvbuff[0] == 0x49)
                                    {
                                        // GFT resends until acknowledged.. we don't miss, if we didn't get it, it wasn't there
                                        if (!NodeProfile[node - 1].recvSessionClosed)
                                        {
                                            // There is a missing session end packet
                                            ignoreSesStart = true; // We will combine the sessions
                                            //mSendMsgCMD((byte)node, 0xD6);
                                            //return;
                                        }
                                        /*else*/
                                        {
                                            NodeProfile[node - 1].recvSessionClosed = false;
                                            NodeProfile[node - 1].recvSessionStarted = true;
                                        }
                                    }
                                    Args[node - 1].sender = this;
                                    Args[node - 1].mHitFlag = false;
                                    if (Args[node - 1].lastRecvbuff == null) Args[node - 1].lastRecvbuff = new byte[39]; // only the first 39 bytes should be compared
                                    if (!Args[node - 1].recvbuff.Take(39).ToArray().SequenceEqual(Args[node - 1].lastRecvbuff))
                                    { // Is this a duplicate of the last one? If so, don't increase the count, the GFT just didn't get the ACK

                                        int SessionCount = buff[16] * 256 + buff[17];                                // 2014.09.11, impact numbering included in last packet
                                      //if (SessionCount < 60000)                                                    // If the device powers off during transmission, sometimes it comes through here as a very large impactCount..
                                        if(SessionCount <= NodeProfile[node - 1].sessionNum + 1)                     // Changed by Jason Chen, 2017.04.17
                                            NodeProfile[node - 1].sessionCount = SessionCount;                       // 2017.04.03
                                        //else                                                                       // 2017.04.03
                                        //    NodeProfile[node - 1].sessionCount++; // We have a valid packet..

                                        Args[node - 1].recvbuff.Take(39).ToArray().CopyTo(Args[node - 1].lastRecvbuff, 0);
                                        if (Args[node - 1].recvbuff[0] == 0x4A && NodeProfile[node - 1].recvSessionID > 0 && NodeProfile[node - 1].recvSessionStarted == false && NodeProfile[node - 1].recvSessionClosed == true) //
                                        {
                                            // The GFT recorded multiple session close packets.. To mimic the behaviour of the USB upload, we should use the last one, re-open the session and re-process
                                            if (NodeProfile[node - 1].gftType == 1) gDataWReOpenSession(NodeProfile[node - 1].SessionID, "PerfSessionWireless", node);
                                            else gDataWReOpenSession(NodeProfile[node - 1].SessionID, "SessionWireless", node);
                                            NodeProfile[node - 1].recvSessionStarted = true;
                                            NodeProfile[node - 1].recvSessionClosed = false;
                                        }
                                        // 
                                        if ((Args[node - 1].recvbuff[0] == 0x49 && !ignoreSesStart) || (Args[node - 1].recvbuff[0] == 0x4A && NodeProfile[node - 1].recvSessionStarted == true && NodeProfile[node - 1].recvSessionClosed == false))
                                        { // Filter out phantom session end packets..

                                            if (NodeProfile[node - 1].gftType == 1) this.Invoke(new ImpactPacketProcess(lpHub.lpDevices[node - 1].PerformancePacketProcess), Args[node - 1]);
                                            else this.Invoke(new ImpactPacketProcess(lpHub.lpDevices[node - 1].ImpactPacketProcess), Args[node - 1]);
                                            System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Invoking Packet Process on " + Args[node - 1].recvbuff[0].ToString());
                                            lpHub.lpDevices[node - 1].mode = 4;     // GFT uploading  
                                            KillTimer(this.Handle, (uint)(0x700 + node));
                                        }
                                        else
                                        {
                                            System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Skipping Invoke Packet Process on " + Args[node - 1].recvbuff[0].ToString() + " Bytes: " + Args[node - 1].recvbuff.ToString());
                                        }
                                        if (Args[node - 1].recvbuff[0] == 0x4A)
                                        {
                                            NodeProfile[node - 1].recvSessionClosed = true;
                                            NodeProfile[node - 1].recvSessionStarted = false;

                                            //if (NodeProfile[node - 1].impactCount == NodeProfile[node - 1].impactNum)
                                            if (NodeProfile[node - 1].impactCount >= NodeProfile[node - 1].impactNum && NodeProfile[node - 1].sessionCount >= NodeProfile[node - 1].sessionNum)       // 09.11, Changed // 10.01.2015, Added Check for session count
                                            {

                                                //NodeProfile[node - 1].impactNum = 0;                   09.15
                                                //NodeProfile[node - 1].impactCount = 0;                 09.15
                                                //#if ENABLE_AUTO_UPLOAD
                                                NodeProfile[node - 1].uploadNum_Flag = 0;
                                                lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;

                                                if (autoUpload_enable.Checked)
                                                {
                                                    NodeProfile[node - 1].deleteFlag = true;

                                                    NodeProfile[node - 1].uploadFlag = false;                               // 2014.11.11
                                                    lpHub.lpDevices[node - 1].upldFlag = NodeProfile[node - 1].uploadFlag;  // 2014.11.11

                                                    //NodeProfile[node - 1].uploadNum_Flag = 0;                             // 2014.11.11                                            
                                                    //lpHub.lpDevices[node - 1].upldFlag_num = 0;                           // 2014.11.11                                       
                                                    //Thread.Sleep(75);
                                                    if (!USB_or_BLE)
                                                        mSendMsgCMD((byte)node, 191);           // Erase GFT data, 0xBF
                                                    else
                                                        mSendMsgBleCMD((byte)node, 191);
                                                    lpHub.lpDevices[node - 1].keepPowerOnFlag = false;
                                                    lpHub.lpDevices[node - 1].mode = 3;     // GFT in Erase Mode
                                                    if (NodeProfile[node - 1].gftType == 1) gDataWPerfSessionMarkErased(NodeProfile[node - 1].GID); // 5
                                                    else gDataWSessionMarkErased(NodeProfile[node - 1].GID); // 5
                                                }

                                                NodeProfile[node - 1].upload_req_flag = false;                               // Added by Jason Chen, 201403.18
                                                // NodeProfile[node - 1].impactNum = 0;                  // 09.15
                                                // NodeProfile[node - 1].impactCount = 0;                // 09.15

                                                if (!lpHub.lpDevices[node - 1].live)// || (!(NodeControl[node - 1].m_PWR)))
                                                {
                                                    //PowerOnProcess(node);

                                                    //NodeControl[node - 1].m_PWR = true;
                                                    lpHub.lpDevices[node - 1].live = true;
                                                    lpHub.lpDevices[node - 1].name = NodeProfile[node - 1].firstName.ToCharArray();

                                                }

                                                lpHub.lpDevices[node - 1].LastDate = /*DateTime.Now.ToShortDateString() + " " + */DateTime.UtcNow.ToLongTimeString();// ToShortTimeString();
                                                TimeSpan spanTime = DateTime.UtcNow - lpHub.lpDevices[node - 1].time;
                                                string spanString = /*spanTime.Hours.ToString("D2") + ":" + */spanTime.Minutes.ToString("D2") + ":" + spanTime.Seconds.ToString("D2") + ":" + spanTime.Milliseconds.ToString("D3");
                                                lpHub.lpDevices[node - 1].LastDate = DateTime.UtcNow.ToLongTimeString() + "(" + spanString + ")"; ;
                                                //#end
                                                // Could send another upload request for speeding up upload speed when some node still wake up, 2014.03.18
                                                if (autoUpload_enable.Checked)
                                                {
                                                    for (int node_id = 1; node_id <= NUM_NODES; node_id++)
                                                    {
                                                        if (NodeProfile[node_id - 1].upload_req_flag)
                                                        {
                                                            if (NodeProfile[node_id - 1].uploadNum_Flag != 1)
                                                            {
                                                                if ((lpHub.lpDevices[node_id - 1].mode == 0x81) || (lpHub.lpDevices[node_id - 1].mode == 1))
                                                                {
                                                                    uploadNum_limit = getUploadNodeNum();

                                                                    if (uploadNum_limit < NUM_UPLOAD_LIMIT)
                                                                    {
                                                                        //if (NodeProfile[node - 1].uploadFlag)
                                                                        if (lpHub.lpDevices[node_id - 1].mode == 0x81)
                                                                        {
                                                                            NodeProfile[node_id - 1].uploadNum_Flag = 1;
                                                                            NodeProfile[node_id - 1].impactCount = 0;
                                                                            NodeProfile[node_id - 1].sessionCount = 0;
                                                                            Args[node_id - 1].mHitFlag = false;
                                                                            lpHub.lpDevices[node_id - 1].upldFlag_num = NodeProfile[node_id - 1].uploadNum_Flag;

                                                                            if (!USB_or_BLE)
                                                                                mSendMsgCMD((byte)node_id, 15); // request upload whole impact data
                                                                            else
                                                                                mSendMsgBleCMD((byte)node_id, 15);
                                                                        }
                                                                        else  // mode = 1
                                                                        {
                                                                            if (NodeProfile[node_id - 1].uploadFlag)
                                                                            {
                                                                                NodeProfile[node_id - 1].uploadNum_Flag = 1;
                                                                                NodeProfile[node_id - 1].impactCount = 0;
                                                                                NodeProfile[node_id - 1].sessionCount = 0;
                                                                                Args[node_id - 1].mHitFlag = false;
                                                                                lpHub.lpDevices[node_id - 1].upldFlag_num = NodeProfile[node_id - 1].uploadNum_Flag;

                                                                                if (!USB_or_BLE)
                                                                                    mSendMsgCMD((byte)node_id, 16); // request upload whole impact data
                                                                                else
                                                                                    mSendMsgBleCMD((byte)node_id, 16);
                                                                            }
                                                                        }
                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                                lpHub.lpDevices[node - 1].mode = 4;     // GFT uploading    
                                        }
                                    }
                                    else
                                    {

                                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                        {

                                            //var txtBuff = "**********PT" + node.ToString("D2") + ": Download - Ignoring Duplicate Session Start or Session End";

                                            var txtBuff = "**********PT";
                                            if (Args[node - 1].recvbuff[0] == 0x49)
                                                txtBuff += node.ToString("D2") + ": Download - Duplicate Session Start, Ignoring**********";
                                            else if (Args[node - 1].recvbuff[0] == 0x4A)
                                                txtBuff += node.ToString("D2") + ": Download - Duplicate Session End,   Ignoring**********";
                                            else
                                                txtBuff += node.ToString("D2") + ": Download - Unkonwn Duplicate,       Ignoring**********";
                                            
                                            RxLog.logText(txtBuff);
                                        }
                                        // Moved to the place by Jason Chen, 2017.04.03
                                        //System.Diagnostics.Debug.WriteLine("Resending Ack to Node: " + node + " For " + Args[node - 1].recvbuff[0].ToString());
                                        //mSendMsgCMD((byte)node, 0x56);                           // Avoiding GFT resend again
                                        ////////////////////////////////////////////////

                                    }
                                  //System.Diagnostics.Debug.WriteLine("Sending Ack to Node: " + node + " For " + Args[node - 1].recvbuff[0].ToString());
                                  //mSendMsgCMD((byte)node, 0x56);
                                }  // end if (Args[node - 1].recv_count == ACC_START_DATA_LEN)
                            }
                            if (Args[node - 1].recv_count == PERF_DATA_LEN)
                            {
                                if (Args[node - 1].recvbuff[0] == 0x58)
                                {
                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "PT" + node.ToString("D2") + ": Download - Performance Data Received (" + NodeProfile[node - 1].impactCount + "/" + NodeProfile[node - 1].impactNum + ")";
                                        RxLog.logText(txtBuff);
                                    }
                                    int impactCount = buff[16] * 256 + buff[17];                            // 2014.09.11, impact numbering included in last packet
                                    //if (impactCount < 15000) // If the device powers off during transmission, sometimes it comes through here as a very large impactCount..
                                    //{
                                    //    NodeProfile[node - 1].impactCount = impactCount;                        // 2014.09.11
                                    //}
                                    //else
                                    NodeProfile[node - 1].impactCount++;

                                    //if (!NodeControl[node - 1].Enabled)
                                    //    NodeControl[node - 1].Enabled = true;

                                    if (!lpHub.lpDevices[node - 1].live)// || (!(NodeControl[node - 1].m_PWR)))
                                    {
                                        //PowerOnProcess(node);                                     // Removed by Jason Chen, 2014.05.21
                                        //NodeControl[node - 1].m_PWR = true;
                                        lpHub.lpDevices[node - 1].live = true;

                                        lpHub.lpDevices[node - 1].name = NodeProfile[node - 1].firstName.ToCharArray();
                                        lpHub.lpDevices[node - 1].temperature = 0;
                                        //NodeControl[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                        NodeProfile[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                        //NodeControl[node - 1].PlayerNumber = NodeProfile[node - 1].playerNum;
                                        //NodeControl[node - 1].m_Usb = false;

                                        Wireless_dataInit(node);
                                    }
                                    float gForce = (float)(Math.Sqrt(byteToShort((byte)Args[node - 1].recvbuff[13], (byte)Args[node - 1].recvbuff[14])) * ACC_SENSITIVITY_LSB2);
                                    if (Args[node - 1].recvbuff[0] == 0x58)
                                    {
                                        if (!NodeProfile[node - 1].recvSessionStarted)
                                        {
                                            gDataWReOpenSession(NodeProfile[node - 1].SessionID, "PerfSessionWireless", node);
                                            NodeProfile[node - 1].recvSessionStarted = true;
                                            NodeProfile[node - 1].recvSessionClosed = false;
                                        }
                                        //If the cached sessionid doesn't match the impact ID and we aren't just starting a session
                                        if (NodeProfile[node - 1].recvSessionID != Args[node - 1].recvbuff[11])
                                        {
                                            // Sometimes the session has been "closed" when it's really still open.. ugh
                                            if (!NodeProfile[node - 1].recvSessionStarted)
                                            {
                                                gDataWReOpenSession(NodeProfile[node - 1].SessionID, "PerfSessionWireless", node);
                                                NodeProfile[node - 1].recvSessionStarted = true;
                                                NodeProfile[node - 1].recvSessionClosed = false;
                                            }
                                            NodeProfile[node - 1].recvSessionID = Args[node - 1].recvbuff[11];
                                        }

                                        TimeSpan spanTime;
                                        if (NodeProfile[node - 1].impactCount == 1)
                                        {
                                            lpHub.lpDevices[node - 1].TimeSpan = /*DateTime.Now.ToShortDateString() + " " +*/ DateTime.UtcNow.ToLongTimeString();// ToShortTimeString();
                                            lpHub.lpDevices[node - 1].time = DateTime.UtcNow;
                                        }
                                        else
                                        {
                                            //lpHub.lpDevices[node - 1].LastDate = /*DateTime.Now.ToShortDateString() + " " + */DateTime.Now.ToLongTimeString();// ToShortTimeString();
                                            spanTime = DateTime.UtcNow - lpHub.lpDevices[node - 1].time;
                                            lpHub.lpDevices[node - 1].LastDate = spanTime.Hours.ToString("D2") + ":" + spanTime.Minutes.ToString("D2") + ":" + spanTime.Seconds.ToString("D2") + ":" + spanTime.Milliseconds.ToString("D3");
                                        }


                                        if (NodeProfile[node - 1].impactNum != 0 || NodeProfile[node - 1].sessionNum != 0)
                                        {
                                            lpHub.lpDevices[node - 1].progressValue = 100 * (NodeProfile[node - 1].impactCount + NodeProfile[node - 1].sessionNum) / (NodeProfile[node - 1].impactNum + NodeProfile[node - 1].sessionNum);
                                            if (lpHub.lpDevices[node - 1].progressValue < 0) lpHub.lpDevices[node - 1].progressValue = 0;
                                            NodeProfile[node - 1].processValue = lpHub.lpDevices[node - 1].progressValue;                 // 2014.10.29 added

                                            if (!USB_or_BLE)
                                                mSendMsgCMD((byte)node, 0x56);                                                                                             // Ack to GFT 0x56     , 2014.09.11
                                            else
                                                mSendMsgBleCMD((byte)node, 0x56);
                                        }
                                        else
                                        {
                                            lastRebootSentAt = DateTime.UtcNow;
                                            if (!USB_or_BLE)
                                                mSendMsgCMD((byte)node, 21);                             // e-boot GFT, Stop Uploading
                                            else
                                                mSendMsgBleCMD((byte)node, 21);
                                            NodeProfile[node - 1].impactCount = 0;
                                            NodeProfile[node - 1].sessionCount = 0;
                                            NodeProfile[node - 1].uploadNum_Flag = 0;
                                            lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                            return;
                                        }

                                        if (lpHub.lpDevices[node - 1].progressValue > 0 && lpHub.lpDevices[node - 1].progressValue < 100) // Data is being uploaded, unset the newdata flag
                                            NodeProfile[node - 1].newDataOnDevice = false;

                                        if (lpHub.lpDevices[node - 1].progressValue > 100)
                                            lpHub.lpDevices[node - 1].progressValue = 100;


                                    }
                                    Args[node - 1].sender = this;
                                    Args[node - 1].mHitFlag = false;                                          // Added by Jason Chen, 2014.02.21 for bug fix
                                    this.Invoke(new ImpactPacketProcess(lpHub.lpDevices[node - 1].PerformancePacketProcess), Args[node - 1]);
                                    KillTimer(this.Handle, (uint)(0x700 + node));
                                }
                            }
                            System.Diagnostics.Debug.WriteLine(Args[node - 1].recv_count);
                            if (Args[node - 1].recv_count == IMPACT_DATA_LEN)
                            {

                                if (Args[node - 1].recvbuff[0] == 0x58)                                     // 2014.09.11
                                {

                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                      //var txtBuff = "PT" + node.ToString("D2") + ": Download - Impact Received (" + NodeProfile[node - 1].impactCount + "/" + NodeProfile[node - 1].impactNum + ")";
                                        var txtBuff = "========>>PT" + node.ToString("D2") + ": Download - Impact Received (" + (NodeProfile[node - 1].impactCount + 1) + "/" + NodeProfile[node - 1].impactNum + ")";//2017.03.31
                                        RxLog.logText(txtBuff);
                                    }
                                    int impactCount = buff[16] * 256 + buff[17];                            // 2014.09.11, impact numbering included in last packet
                                  //if (impactCount < 60000) // If the device powers off during transmission, sometimes it comes through here as a very large impactCount..
                                    if(impactCount <= NodeProfile[node - 1].impactNum + 1)                  // Changed by Jason chen, 2017.04.17
                                    {
                                        //NodeProfile[node - 1].impactCount++;
                                        NodeProfile[node - 1].impactCount = impactCount;                        // 2014.09.11
                                    }
                                }
                                //else
                                //    NodeProfile[node - 1].impactCount++;

                                //if (!NodeControl[node - 1].Enabled)
                                //    NodeControl[node - 1].Enabled = true;

                                if (!lpHub.lpDevices[node - 1].live)// || (!(NodeControl[node - 1].m_PWR)))
                                {
                                    //PowerOnProcess(node);                                     // Removed by Jason Chen, 2014.05.21
                                    //NodeControl[node - 1].m_PWR = true;
                                    lpHub.lpDevices[node - 1].live = true;

                                    lpHub.lpDevices[node - 1].name = NodeProfile[node - 1].firstName.ToCharArray();
                                    lpHub.lpDevices[node - 1].temperature = 0;
                                    //lpHub.lpDevices[node - 1].alarm = true;

                                    //NodeControl[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                    NodeProfile[node - 1].UserName = NodeProfile[node - 1].firstName + " " + NodeProfile[node - 1].lastName;
                                    //NodeControl[node - 1].PlayerNumber = NodeProfile[node - 1].playerNum;
                                    //NodeControl[node - 1].m_Usb = false;
                                    //NodeControl[node - 1].m_Hit = false;
                                    //NodeControl[node - 1].m_Imm = false;
                                    //NodeControl[node - 1].m_Tmp = 0;

                                    Wireless_dataInit(node);
                                }

                                float gForce = (float)(Math.Sqrt(byteToShort((byte)Args[node - 1].recvbuff[13], (byte)Args[node - 1].recvbuff[14])) * ACC_SENSITIVITY_LSB2);
                                //if ((gForce > lpHub.lpDevices[node - 1].gForce) && (gForce > NodeControl[node - 1].m_Tmp))
                                if (Args[node - 1].recvbuff[0] == 0x48)
                                {

                                    if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                                    {
                                        var txtBuff = "PT" + node.ToString("D2") + ": Download - Summary Received";
                                        RxLog.logText(txtBuff);
                                    }
                                    // don't do this when downloading, just when viewing
                                    double rotX = /*(short)*/(gyroDecode(Args[node - 1].recvbuff[15], Args[node - 1].recvbuff[16]));
                                    double rotY = /*(short)*/(gyroDecode(Args[node - 1].recvbuff[17], Args[node - 1].recvbuff[18]));
                                    double rotZ = /*(short)*/(gyroDecode(Args[node - 1].recvbuff[19], Args[node - 1].recvbuff[20]));

                                    rotX = Math.Round(rotX, 1);
                                    rotY = Math.Round(rotY, 1);
                                    rotZ = Math.Round(rotZ, 1);
                                    double Rotation = (double)Math.Sqrt(rotX * rotX + rotY * rotY + rotZ * rotZ);
                                    if (Rotation > lpHub.lpDevices[node - 1].Rotation)
                                    {
                                        lpHub.lpDevices[node - 1].Rotation = Rotation;
                                        lpHub.lpDevices[node - 1].highestRotImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 = first
                                    }

                                    if (gForce > lpHub.lpDevices[node - 1].gForce)
                                    {
                                        lpHub.lpDevices[node - 1].gForce = gForce;
                                        lpHub.lpDevices[node - 1].highestGImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 = first
                                    }
                                    //NodeControl[node - 1].m_Tmp = gForce;
                                    NodeProfile[node - 1].m_Tmp = gForce;
                                    //lpHub.lpDevices[node - 1].gForce = gForce;
                                    if ((gForce > NodeProfile[node - 1].alarmThres))
                                    {
                                        //NodeControl[node - 1].m_Tmp = gForce;
                                        //lpHub.lpDevices[node - 1].gForce = gForce;
                                        //NodeControl[node - 1].m_Hit = true;
                                        lpHub.lpDevices[node - 1].alarm = true;                // Added by Jason Chen, 2014.05.15
                                        lpHub.lpDevices[node - 1].lastAlarmgForce = gForce;
                                        lpHub.lpDevices[node - 1].lastAlarmImpact = Args[node - 1].recvFIFO.Count; // Our new impact isn't on this list yet.. so we can just take the count and don't need to adjust for 0 

                                        DateTime outTime;
                                        ushort myShort = byteToShort(Args[node - 1].recvbuff[7], Args[node - 1].recvbuff[8]);
                                        if (myShort >= 1000)
                                            myShort = 999;

                                        if (ValidateTime(out outTime, Args[node - 1].recvbuff[1], Args[node - 1].recvbuff[2], Args[node - 1].recvbuff[3],
                                                                      Args[node - 1].recvbuff[4], Args[node - 1].recvbuff[5], Args[node - 1].recvbuff[6],
                                                                      myShort))
                                        {
                                            lpHub.lpDevices[node - 1].lastAlarmTime = outTime;
                                        }
                                        else
                                        {
                                            //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid date time.\r\n");
                                            //return;
                                        }

                                    }


                                    // if(lastNodeSelected != null) wirelessViewer_OnRowSelected(this, lastNodeSelected);
                                    // New Impact received for this node, update the node profile to mark data available for download
                                    NodeProfile[node - 1].newDataOnDevice = true;
                                }

                                if (Args[node - 1].recvbuff[0] == 0x58) // Impact data upload
                                {
                                    if (!NodeProfile[node - 1].recvSessionStarted)
                                    {
                                        gDataWReOpenSession(NodeProfile[node - 1].SessionID, "SessionWireless", node);
                                        NodeProfile[node - 1].recvSessionStarted = true;
                                        NodeProfile[node - 1].recvSessionClosed = false;
                                    }
                                    //If the cached sessionid doesn't match the impact ID and we aren't just starting a session
                                    if (NodeProfile[node - 1].recvSessionID != Args[node - 1].recvbuff[11])
                                    {
                                        // Sometimes the session has been "closed" when it's really still open.. ugh
                                        if (!NodeProfile[node - 1].recvSessionStarted)
                                        {
                                            gDataWReOpenSession(NodeProfile[node - 1].SessionID, "SessionWireless", node);
                                            NodeProfile[node - 1].recvSessionStarted = true;
                                            NodeProfile[node - 1].recvSessionClosed = false;
                                            /*
                                            // We missed a session Start Packet
                                            //NodeProfile[node - 1].recvSessionStarted = false;
                                            //NodeProfile[node - 1].recvSessionClosed = true;
                                            NodeProfile[node - 1].impactCount = 0;
                                            Args[node - 1].mHitFlag = false; 
                                            mSendMsgCMD((byte)node, 0xD6);
                                            //mSendMsgCMD((byte)node, 16); // request upload whole impact data
                                            //gDataWSessionRemoveIncomplete(NodeProfile[node - 1].GID); // Delete incomplete for this GID
                                            
                                            return;*/
                                        }
                                        /*if (!NodeProfile[node - 1].recvSessionClosed)
                                        {
                                            // We missed a session End Packet(Hard to believe.. since the gft records so many!)
                                            NodeProfile[node - 1].recvSessionStarted = false;
                                            NodeProfile[node - 1].recvSessionClosed = true;
                                            NodeProfile[node - 1].impactCount = 0;
                                            Args[node - 1].mHitFlag = false;
                                            mSendMsgCMD((byte)node, 0xD6);
                                            //mSendMsgCMD((byte)node, 16); // request upload whole impact data
                                            //gDataWSessionRemoveIncomplete(NodeProfile[node - 1].GID); // Delete incomplete for this GID
                                            return;
                                        }
                                        else if (NodeProfile[node - 1].recvSessionStarted)
                                        {*/
                                        NodeProfile[node - 1].recvSessionID = Args[node - 1].recvbuff[11];
                                        /*NodeProfile[node - 1].recvSessionStarted = false;
                                    /*}*/
                                    }

                                    TimeSpan spanTime;
                                    if (NodeProfile[node - 1].impactCount == 1)
                                    {
                                        lpHub.lpDevices[node - 1].TimeSpan = /*DateTime.Now.ToShortDateString() + " " +*/ DateTime.UtcNow.ToLongTimeString();// ToShortTimeString();
                                        lpHub.lpDevices[node - 1].time = DateTime.UtcNow;
                                        //NodeProfile[node - 1].uplolaNum_Flag = 1;
                                        //lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uplolaNum_Flag;
                                    }
                                    else
                                    {
                                        //lpHub.lpDevices[node - 1].LastDate = /*DateTime.Now.ToShortDateString() + " " + */DateTime.Now.ToLongTimeString();// ToShortTimeString();
                                        spanTime = DateTime.UtcNow - lpHub.lpDevices[node - 1].time;
                                        lpHub.lpDevices[node - 1].LastDate = spanTime.Hours.ToString("D2") + ":" + spanTime.Minutes.ToString("D2") + ":" + spanTime.Seconds.ToString("D2") + ":" + spanTime.Milliseconds.ToString("D3");
                                    }


                                    if (NodeProfile[node - 1].impactNum != 0 || NodeProfile[node - 1].sessionNum != 0)
                                    {
                                        lpHub.lpDevices[node - 1].progressValue = 100 * (NodeProfile[node - 1].impactCount + NodeProfile[node - 1].sessionNum) / (NodeProfile[node - 1].impactNum + NodeProfile[node - 1].sessionNum);
                                        if (lpHub.lpDevices[node - 1].progressValue < 0) lpHub.lpDevices[node - 1].progressValue = 0;
                                        NodeProfile[node - 1].processValue = lpHub.lpDevices[node - 1].progressValue;                 // 2014.10.29 added

                                        if (!USB_or_BLE)
                                            mSendMsgCMD((byte)node, 0x56);                                                                                             // Ack to GFT 0x56     , 2014.09.11
                                        else
                                        {
                                            mSendMsgBleCMD((byte)node, 0x56);
                                        }
                                        //m_Edit_RxPacket.AppendText("--->" + node.ToString("D2") + " Send 86 CMD to ACK IMPACT--" + NodeProfile[node - 1].impactCount.ToString("D3") + "\r\n");// 2014.09.11  
                                    }
                                    else
                                    {
                                        lastRebootSentAt = DateTime.UtcNow;
                                        if (!USB_or_BLE)
                                            mSendMsgCMD((byte)node, 21);                             // e-boot GFT, Stop Uploading
                                        else
                                            mSendMsgBleCMD((byte)node, 21);
                                        //m_Edit_RxPacket.AppendText("---> " + node.ToString("D2") + " Send 21 cmd to reboot GFT--05--" + NodeProfile[node - 1].impactCount.ToString("D3") + "\r\n");//         2014.09.02                                           

                                        NodeProfile[node - 1].impactCount = 0;
                                        NodeProfile[node - 1].sessionCount = 0;
                                        NodeProfile[node - 1].uploadNum_Flag = 0;
                                        lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                                        return;
                                    }

                                    if (lpHub.lpDevices[node - 1].progressValue > 0 && lpHub.lpDevices[node - 1].progressValue < 100) // Data is being uploaded, unset the newdata flag
                                        NodeProfile[node - 1].newDataOnDevice = false;

                                    if (lpHub.lpDevices[node - 1].progressValue > 100)
                                        lpHub.lpDevices[node - 1].progressValue = 100;


                                }

                                Args[node - 1].sender = this;
                                Args[node - 1].mHitFlag = false;                                          // Added by Jason Chen, 2014.02.21 for bug fix
                                this.Invoke(new ImpactPacketProcess(lpHub.lpDevices[node - 1].ImpactPacketProcess), Args[node - 1]);

                                //Args[node - 1].mHitFlag = false;
                                KillTimer(this.Handle, (uint)(0x700 + node));
                            }
                            else
                            {
                            }

                            //NodeControl[node - 1].DataActive = true;

                            if ((Args[node - 1].recvbuff[0] == 0x48))   // 2014.10.28
                            {
                                temps = "Receiving Data...." + node.ToString("D2");// temps;                                                        2014.10.28
                                //toolStripStatusLabelChart.Text = temps;
                                //MyMarshalToForm("toolStripStatusLabelChart", temps, "Yellow");
                                MyMarshalToForm("toolStripStatusLabelChart", temps, Color.Yellow);   //2014.10.28
                                //wirelessViewer.DebugCmdBoxDisplay(temps + "\r\n");2014.10.28
                                //show_device_receiving(node);
                                SetTimer(this.Handle, 0x900 + node, 50, null);
                            }
                            lpHub.lpDevices[node - 1].lastReceiveTime = DateTime.UtcNow;

                            if (Args[node - 1].recvbuff[0] != 0x49 && Args[node - 1].recvbuff[0] != 0x4A)
                                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);             // 2014.10.26 added

                        }
                        else
                        {
                            //mSendMsgCMD((byte)node, 21);                                          // reboot GFT, 2014.09.02 


                            if (NodeProfile[node - 1].uploadNum_Flag != 0)                                                                            // Changed by Jason, 2014.09.11
                            {
                                System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Sending Re-Send Command===>01");
                                if (!USB_or_BLE)
                                    mSendMsgCMD((byte)node, 0xD6);                                                                                                  // ReSend Current Impact, 2014.09.11
                                else
                                    mSendMsgBleCMD((byte)node, 0xD6);
                                if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))    //2017.04.03
                                {
                                    var txtBuff = "########>>PT" + node.ToString("D2") + ": Download - Sending ReSend Command(0xD6) ====>#################### 01";      //2017.04.03
                                    RxLog.logText(txtBuff);
                                }
                                //m_Edit_RxPcket.AppendText("---> " + node.ToString("D2") + " Send 0xD6 cmd to ReSend Current impact NoHead--02----\r\n");      //         2014.09.11
                                //ErrorCount.Text = (++error_count).ToString();                                                                                 //         2014.09.11
                            }


                            //Thread.Sleep(50);
                            //mSendMsg((byte)node, 21);                             // reboot GFT
                            //Thread.Sleep(50);
                            //mSendMsg((byte)node, 21);                             // reboot GFT
                            //Thread.Sleep(50);

                            //for (int kk = 0; kk < lenc; kk++)
                            //    temps += string.Format("{0:x2} ", buff[kk + 5]).ToUpper();

                            //toolStripStatusLabelChart.Text = temps + node.ToString("D2");                                                                       //09.11
                            //MyMarshalToForm("toolStripStatusLabelChart", temps + "Wrong Packet received Node " + node.ToString("D2"), System.Drawing.Color.Red);//09.11  
                            //wirelessViewer.DebugCmdBoxDisplay(temps + "\r\n");                                                                                  //09.11

                            Args[node - 1].mHitFlag = false;
                            m_bHitSeqNo[node - 1] = 1;
                            Args[node - 1].recv_count = 0;
                            Args[node - 1].recvbuffstr = "";

                            //NodeControl[node - 1].DataActive = false;

                            //NodeProfile[node - 1].uploadNum_Flag = 0;
                            //lpHub.lpDevices[node - 1].upldFlag_num = NodeProfile[node - 1].uploadNum_Flag;
                            //NodeProfile[node - 1].uploadFlag = false;                                              //????????????????????2014.03.21
                            //NodeProfile[node - 1].impactNum = 0;
                            //NodeProfile[node - 1].impactCount = 0;
                            //NodeProfile[node - 1].deleteFlag = false;
                        }
                    }
                    //lpHub.lpDevices[node - 1].live = true;
                    //lpHub.lpDevices[node - 1].alarm = (buff[5] == 1);
                    //lpHub.lpDevices[node - 1].gForce = (int)Math.Sqrt(buff[6] + buff[7] * 256);
                }
            }
            //else textInfo += "???:";
#if false
            textInfo += string.Format("{0:d2},{1:d2}--", node, buff[2]);

            if (plen == (CHUNK_POWER_OFF + 1))
                for (int jj = 0; jj < mlen + 1; jj++)
                    textInfo += string.Format("{0:x2} ", buff[jj + 4]).ToUpper();     // start from buff[4]
            else if (plen == CHUNK_POWER_ON + 1)
            {
                if ((seqn == 0) && (mlen == 0))
                    for (int jj = 0; jj < plen - 1; jj++)
                        textInfo += string.Format("{0:x2} ", buff[jj + 4]).ToUpper();     // start from buff[4]
                else
                {
                    if (mlen != 0)
                    {
                        for (int jj = 0; jj < mlen + 1; jj++)
                            textInfo += string.Format("{0:x2} ", buff[jj + 4]).ToUpper();     // start from buff[4]
                    }
                }
            }
            else if (plen == CHUNK_IMMEDIATE + 1)
            {
                for (int jj = 0; jj < mlen + 1; jj++)
                    textInfo += string.Format("{0:x2} ", buff[jj + 4]).ToUpper();     // start from buff[4]
            }
            else if (plen == CHUNK_HIT_DATA + 1)
                for (int jj = 0; jj < 14/* mlen + 1*/; jj++)
                    textInfo += string.Format("{0:x2} ", buff[jj + 4]).ToUpper();     // start from buff[4]
#endif


            
            if (plen == (CHUNK_POWER_ON + 1))
            {

                if (node >= 1 && node <= NUM_NODES)
                {
                    // Power On
                    if ((seqn == 0) && (mlen == 0))
                    {
                        Args[node - 1].msg_len = buff[5] * 256 + buff[6];
                        Args[node - 1].msg_packet_num = Args[node - 1].msg_len / 11;

                        if (Args[node - 1].msg_packet_num * 11 < Args[node - 1].msg_len) Args[node - 1].msg_packet_num += 1;

                        Args[node - 1].recv_count = 0;

                        //temp = System.Text.Encoding.ASCII.GetString(buff);
                        //temp = temp.Substring(7);
                        //temp = temp.Substring(0, temp.IndexOf("\0"));
                        //textInfo += string.Format("    - len:{0:d}, build:", Args[node - 1].msg_len) + temp + "  " + NodeProfile[node - 1].deleteFlag.ToString();
                    }
                    else if (seqn == Args[node - 1].msg_packet_num-1)
                    {
                        //if (bat_level > 0)//_f > 2.50f)
                        {
                            //string date_string = DateTime.UtcNow.ToShortDateString().Replace("/", "-");

                            //string bat_level_string = string.Format("{0:d4}", NodeProfile[node - 1].bat_level);
                            //Bat_msg = "Node-" + node.ToString("D2") + ",  " + bat_level_string + ",";

                            //bat_level_string = string.Format("{0:d3}", (NodeProfile[node - 1].bat_level)/10);
                            //Bat_msg += bat_level_string + ",";
#if true
                            //float batFuel = NodeProfile[node - 1].bat_levelfuel;
                            //batFuel = batFuel * 0.625f / 1000f;
                            //bat_level_string = string.Format("{0,9:f3}", batFuel);
#else
                            bat_level_string = NodeProfile[node - 1].bat_levelfuel.ToString("D5");
#endif
                            //Bat_msg += bat_level_string + ",";
#if false
                            batFuel = 0.0f;// NodeProfile[node - 1].bat_currentfuel * 1.5625f * 100f / 1000f;
                            //int current = (int)batFuel;
                            //current = current.ToString().Length;
                            bat_level_string = string.Format("{0,9:f3}", batFuel);
                            if (batFuel == 0.00f)  //GFT2
                                bat_level_string = " " + bat_level_string;
#else
                            //bat_level_string = " " + NodeProfile[node - 1].impactNum.ToString("D4");
#endif
                            //Bat_msg += bat_level_string + ",  ";
                            //Bat_msg += date_string + " " + DateTime.UtcNow.Hour.ToString("D2") + "-" + DateTime.UtcNow.Minute.ToString("D2") + "-" + DateTime.UtcNow.Second.ToString("D2");

                            //if (TooltabPage16Exist)
                            //{
                               //if (!(plen == (CHUNK_HIT_DATA + 1)) && !(plen == (CHUNK_POWER_ON + 1)))     // The Limit added by Jason Chen, 20140219
                                //m_Edit_Immediate.AppendText(Bat_msg + ", Power On\r\n");
                            //}
                        }
                    }
                }
            }
            else if (plen == (CHUNK_POWER_OFF + 1))
            {   // Power Off
                if ((seqn == 0))// && (mlen == 0))
                {
                    // mlen=0 is for Build packet
                    //pBufferIn[5+CHUNK_POWER_OFF] = 0;       // 10
                    //temp = System.Text.Encoding.ASCII.GetString(buff);
                    //temp = temp.Substring(5);
                    //temp = temp.Substring(0, 6);//temp.IndexOf("\0"));
                    //temp = temp.Substring(0, temp.IndexOf("\0"));
                    //textInfo += string.Format("    - len:{0:d}, build:", buff[5] * 256 + buff[6]) + temp;
                    //string zero = "\0";
                    //if(temp.Contains(zero))
                    //    temp = temp.Substring(0, temp.IndexOf("\0"));
                    //textInfo += string.Format("         - len:{0:d}  ", buff[4] & 0x0f) + "  " + "Delete Flag--->"+NodeProfile[node - 1].deleteFlag.ToString(); ;// +temp;// +"\r\n";
#if false
                    if (false && ((buff[5] == 0x80) || (buff[5] == 0x81) || (report_flag == 1) || (report_flag == 3)))                // new the report of battery level
                    {
                        //string date_string = DateTime.UtcNow.ToShortDateString().Replace("/", "-");
                        /*
                        string bat_level_string = string.Format("{0:d4}", NodeProfile[node - 1].bat_level);
                        Bat_msg = "Node-" + node.ToString("D2") + ",  " + bat_level_string + ",";

                        bat_level_string = string.Format("{0:d3}", (NodeProfile[node - 1].bat_level) / 10);
                        Bat_msg += bat_level_string + ",";

                        float batFuel = NodeProfile[node - 1].bat_levelfuel;

                        batFuel = batFuel * 0.625f / 1000f;
                        bat_level_string = string.Format("{0,9:f3}", batFuel);

                        Bat_msg += bat_level_string + ",";

                        bat_level_string = " " + NodeProfile[node - 1].impactNum.ToString("D4");

                        Bat_msg += bat_level_string + ",  ";
                        Bat_msg += date_string + " " + DateTime.UtcNow.Hour.ToString("D2") + "-" + DateTime.UtcNow.Minute.ToString("D2") + "-" + DateTime.UtcNow.Second.ToString("D2");
                        */


                        NodeProfile[node - 1].bat_level = gftBattery(Args[node - 1].recvbuff[1], Args[node - 1].recvbuff[2], Args[node - 1].recvbuff[3], Args[node - 1].recvbuff[6], Args[node - 1].recvbuff[7], Args[node - 1].recvbuff[14]);
                        lpHub.lpDevices[node - 1].battLevel = NodeProfile[node - 1].bat_level;

                        int impCount = Args[node - 1].recvbuff[4] * 256 + Args[node - 1].recvbuff[5];
                        NodeProfile[node - 1].impactNum = impCount;
                        int entryCount = Args[node - 1].recvbuff[12] * 256 + Args[node - 1].recvbuff[13];
                        NodeProfile[node - 1].entryNum = entryCount;
                        NodeProfile[node - 1].sessionNum = NodeProfile[node - 1].entryNum - NodeProfile[node - 1].impactNum;
                        NodeProfile[node - 1].usbMode = Args[node - 1].recvbuff[0];

                        if (RxLog != null && RxLog.isCaptureType(node, Convert.ToInt16(wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                        {
                            var txtBuff = "PT" + node.ToString("D2") + ": Battery Received";
                            RxLog.logText(txtBuff);
                        }
                        textInfo += "Sleep:";
                        //if (TooltabPage16Exist)
                        //{
                            //if ((node == 1) || (node == 3) || (node == 50) || (node == 4))
                            //if (((node >= 6) && (node <= 23))||(node == 48)||(node ==1))
                            //if (((node >= 1) && (node <= 23))||(node == 50)|| (node == 48))
                            //if (((node >= 18) && (node <= 50)))
                            //if (!(NodeProfile[node - 1].usbMode == 0x81))
                            //{
                            //    if (report_flag == 0)
                            //    {
                                    //m_Edit_Immediate.AppendText(Bat_msg + ", Batt Report\r\n");
                            //    }
                            //    else if (report_flag == 1)
                            //    {
                                    //m_Edit_Immediate.AppendText(Bat_msg + ", Low Voltage-->Power off\r\n");
                            //    }
                            //    else if (report_flag == 3)
                            //    {
                                    //m_Edit_Immediate.AppendText(Bat_msg + ", unplugging-->Power off\r\n");
                            //    }
                            //}
                            //if(node == 2)
                            //    m_Edit_Immediate.AppendText(Bat_msg + ", Batt Report\r\n");
                        //}
                    }
#endif
                }
#if USE_ORIGINAL_REFRESH
                lpHub.dgvLpPopulateView(node);                                                       // 2014.10.29 added
#else
                this.BeginInvoke(new DeviceList_Refresh(lpHub.dgvLpPopulateView), node);             // 2014.10.26 added
                calcTeamStats();
#endif
            }
            else if (plen == (CHUNK_IMMEDIATE + 1))
            {   // Immediate
                
                byte[] bufferTemp = new byte[mlen]; 
                Array.Copy(buff, 5, bufferTemp, 0, mlen);
                /*if ((seqn == 0) && (mlen == 0))
                {  // mlen=0 is for Build packet
                    //pBufferIn[5+CHUNK_IMMEDIATE] = 0;        // 12
                    //temp = System.Text.Encoding.ASCII.GetString(buff);
                    //temp = temp.Substring(7);
                    //temp = temp.Substring(0, temp.IndexOf("\0"));
                    textInfo += string.Format("- len:{0:d}, Temp", buff[4] & 0x0F) + NodeControl[node - 1].m_Tmp.ToString();
                }
                else */
                if ((seqn == 0) && (mlen == 12))
                {  // mlen=0 is for Build packet
                    //textInfo += "                          - Impact = "/* + NodeControl[node - 1].m_Tmp.ToString()*/;
                    //textInfo += " - " + DateTime.UtcNow.ToLongTimeString();
                    NodeProfile[node - 1].CRC_CheckSum8 = CalcCRC8(ref bufferTemp, mlen, 0);
                }
                else if ((seqn == 1) && (mlen == 11))   // for Imapct Research CRC check, 
                {
                    //textInfo += "     - ImpactEntryNum = " + (NodeProfile[node - 1].gftSummary_buff[10] * 256 + NodeProfile[node - 1].gftSummary_buff[11]).ToString() + ",";
                    Args[node - 1].recv_count += mlen;
                    NodeProfile[node - 1].CRC_CheckSum8 = CalcCRC8(ref bufferTemp, (byte)(mlen-1), NodeProfile[node - 1].CRC_CheckSum8);//,true);// NodeProfile[node - 1].CRC_CheckSum, false);//NodeProfile.[node - 1].CheckSum

                    UInt16 CRC_recv8 = bufferTemp[mlen-1];// NodeProfile[node - 1].gftSummary_buff[Args[node - 1].recv_count - 1];
                    //textInfo += " -->0x" + CRC_recv8.ToString("x2").ToUpper() + "  " + NodeProfile[node - 1].CRC_CheckSum8.ToString("x2").ToUpper();
                    if (CRC_recv8 == NodeProfile[node - 1].CRC_CheckSum8)
                    {
                        if (!USB_or_BLE)
                            mSendMsgCMD((byte)node, 0x56);            // Send one ACK each time
                        else
                            mSendMsgBleCMD((byte)node, 0x56);
                            
                            ushort countNum = (ushort)(NodeProfile[node - 1].gftSummary_buff[10] << 8 + NodeProfile[node - 1].gftSummary_buff[11]);
                            if( countNum == NodeProfile[node - 1].previousCountNum +1)
                            {
                            	//OK, no duplicate 
                            }
                            else if( countNum == NodeProfile[node - 1].previousCountNum)
                            {
                            	// Duplcate package happen
                            }
                            else
                            {
                            	// Could lose some package
                            }      
                            
                            NodeProfile[node - 1].previousCountNum = countNum;

                                                                            
                    }
                    else
                    {
                            System.Diagnostics.Debug.WriteLine(DateTime.UtcNow.ToString() + ": Sending Re-Send Command===>04");
                        if(!USB_or_BLE)
                            mSendMsgCMD((byte)node, 0xD6);                  // Send one NoACK 
                        else
                            mSendMsgBleCMD((byte)node, 0xD6);
                    }


                    NodeProfile[node - 1].CRC_CheckSum8 = 0x0;

                }

            }
            //else if (plen == (CHUNK_HIT_DATA + 1))
            //{   // Hit
            //    if ((seqn == 0))// && (mlen == 0))
            //    {  // mlen=0 is for Build packet
                    //pBufferIn[5+CHUNK_HIT_DATA] = 0;        // 13
                    //temp = System.Text.Encoding.ASCII.GetString(buff);
                    //temp = temp.Substring(7);
                    //temp = temp.Substring(0, temp.IndexOf("\0"));
                    //textInfo += string.Format("- len:{0:d}", buff[4]);// +temp;
            //   }
            //}

            //if (TooltabPage16Exist)
            //{
            //    if (!(plen == (CHUNK_HIT_DATA + 1)) && !(plen == (CHUNK_POWER_ON + 1)))     // The Limit added by Jason Chen, 20140212                    
            //    {
            //        if ((plen == (CHUNK_POWER_OFF + 1)) || (plen == (CHUNK_IMMEDIATE + 1)))
            //        {
                        //if ((buff[5] != 0x80) && (buff[5] != 0x81) && (buff[5] != 0x01))// && (buff[5] != 0x01))                // new the report of battery level
                        //if ((node == 3)||(node == 1)||(node == 50)||(node == 4))
                        //if (((node >= 1) && (node <= 23)) || (node == 50) || (node == 48))
                        //if (((node >= 18) && (node <= 50)))
                        //if (!(NodeProfile[node - 1].usbMode == 0x81))
           //             if (!(NodeProfile[node - 1].usbMode == 0x81) && (buff[5] != 1) && (buff[5] != 0x80) && (buff[5] != 0x81))    // 09.11
           //             {
                            //if ((node >= 1) && (node <= 50))
                            //if (((node >= 1) && (node <= 25)) || (node == 50))
                            //m_Edit_RxPcket.AppendText(textInfo + "\r\n");
           //             }
           //         }
           //     }
           //     if (plen == (CHUNK_POWER_ON + 1))     // The Limit added by Jason Chen, 20140212                    
           //     {
                    //m_Edit_RxPcket.AppendText(textInfo + "\r\n");
           //     }
                //if ((plen == CHUNK_HIT_DATA + 1) && (mEnableRawData.Checked))
                //{
                    //m_Edit_RxPcket.AppendText(textInfo + "\r\n");
                //}
           // }

            //string[] strLines = m_Edit_RxPcket.Lines;
            //int line_count = strLines.Length;
            //strLines = m_Edit_RxPacket.Text.Split(DELIM_CRLF);
            //int line_count1 = strLines.Length;

            //string[] strLines1 = m_Edit_Immediate.Lines;
            //int line_count1 = strLines1.Length;


            //temp = string.Format("Rx Packet : [{0:d}]   Line Number: [{1:d}]", ++m_dwTotalRxPacket, line_count/*m_bSeqErr*/);
            //m_Static_RxPacket.Text = temp;
            //if (line_count > 4000)
            //    m_Edit_RxPcket.Clear();

            //temp = string.Format("Line Number: : [{0:d}]", line_count1);
            //m_LineNum.Text = temp;
            //if (line_count1 > 4000)
            //    m_Edit_Immediate.Clear();
            //then = now;
            //oldif = dif;
            //System.Diagnostics.Debug.WriteLine(textInfo);
        }

    }

    public class uploadDataForWirelessProcess
    {
        Form1 mythis;
        public static string conString = WirelessGFTViewer.Properties.Settings.Default.gForceTrackerConnectionString;

        internal const double GYRO_SENSITIVITY_LSB = 0.07; //70mdps/digit
        internal const double ACC_SENSITIVITY_LSB2 = (200d / 128d);
        internal const double LOWG_SENSITIVITY_LSB_16G = 0.007808;

        const byte MAX_DEVICE_CNT = 103;//63;                                         // Added by Jason Chen, 2017.06.20
        // Location Masks

        const byte GFT_MNT_LOC_INOUTMSK = 0x1;
        const byte GFT_MNT_LOC_USB_FRONT_REAR_MSK = 0x2;
        const byte GFT_MNT_LOC_ZONE_MSK = 0x30;
        const byte GFT_MNT_LOC_ZONE_CROWN = 0x80;
        const byte GFT_MNT_LOC_ZONE_RIGHT = 0x10;
        const byte GFT_MNT_LOC_ZONE_LEFT = 0x20;
        const byte GFT_MNT_LOC_ZONE_BACK = 0x40;
        const byte GFT_MNT_LOC_VER = 0x4;

        internal const double ACC_SENSITIVITY_LSB = 0.244;
        internal const double ACC_LOW_SENSITIVITY_LSB = (0.192f);
        
        public struct ACTIVITY_DATA
        {
            public DateTime reportTime;
            public Int32 activeTime;
        }
        public struct FG_DATA
        {
            public int x_start;
            public int x_end;
            public DateTime d_start;
            public DateTime d_end;
            public uint player_load;
        }

        public struct IMPACT_DATA     // Event Database Table
        {
            public Int32 eid;
            public Int32 sid;
            public DateTime time;
            public Int32 millisecond;
            public Double linX;
            public Double linY;
            public Double linZ;
            public Double linR;
            public Double thresholdR;
            public Double thresholdG;

            public Double azimuth;
            public Double elevation;

            public Double rotX;
            public Double rotY;
            public Double rotZ;
            public Double rotR;

            public Double hic;
            public Double gsi;
            public Double pcs;

            public byte locationId;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataX;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 120)]
            public byte[] dataZ;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataX;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] rdataZ;
        };

        public struct SESSION_DATA    // Session Database Table
        {
            public Int32 SID;
            public int UID;
            public string GID;
            public DateTime Date;     //or count for firmware data
            public DateTime OnTime;
            public DateTime OffTime;
            public Int32 Duration;
            public Int32 Impact;
            public Int32 Alarm;
            public byte WTHR;
            public Int32 WTH;
            public Int32 Threshold;
            public Int32 sports;
            public string name;
            public Int32 activeTime;
            public bool iMntInside;
            public double[] iLocation;// = new double[4];
            public float fCoefficient;

            public string fwVersion;

            public string swVersion;
            public bool closed;
        };

        public uploadDataForWirelessProcess(Form1 mThis)
        {
            mythis = mThis;

            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;

            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        private ushort byteToShort(byte h, byte l)
        {
            return (ushort)(((ushort)h << 8) | l);

        }

#if ENABLE_ACC_OUTPUT
        private double gyroDecode(byte h, byte l)                  // Added by Jason Chen for LG test, 2014.05.07
        {
            short lgdata = (short)(h << 8 | l);
            lgdata = (short)(lgdata / 16);
            return (0.012 * lgdata);
        }
#else
         private double gyroDecode(byte h, byte l)
        {
            return (GYRO_SENSITIVITY_LSB * (short)(h << 8 | l));
        }

#endif

         public static double lgDecodeDn(byte h, byte l, double lgRes)
         {
            short d;
            d = (short)((short)(h << 8 | l) );

            return (lgRes * d);
         }

        private double AccDecode(byte c)
        {
            //if((curGFT.hwMajor ==0x1) && (curGFT.hwMinor == 0))
            return (ACC_SENSITIVITY_LSB2 * (c - 128));
            //return (ACC_SENSITIVITY_LSB * (c * 4 - 512));
        }

        private double GSI(Double[] my40)
        {
            int i = 0;
            double gsi = 0;
            // gsi = Accumulation a(t)power2.5dt (in impact duration)
            for (i = 0; i < 40; i++) gsi += Math.Pow(my40[i], 2.5);
            return gsi / 1000;
        }

        private double HIC15(Double[] my40)
        {
            int i = 0;
            int j = 0;
            double tHic = 0;
            double hic = 0;
            for (i = 0; i < 26; i++)
            {
                tHic = 0;
                for (j = 0; j < 15; j++)
                    tHic += my40[i + j];
                tHic = 15 * Math.Pow((tHic) / 15, 2.5);
                if (hic < tHic) hic = tHic;
            }
            return hic / 1000;
        }
        public static double[] mQmulti(double[] a, double[] b)
        {
            double[] c = new double[4];
            c[0] = a[0] * b[0] - a[1] * b[1] - a[2] * b[2] - a[3] * b[3];
            c[1] = a[0] * b[1] + a[1] * b[0] + a[2] * b[3] - a[3] * b[2];
            c[2] = a[0] * b[2] + a[2] * b[0] + a[3] * b[1] - a[1] * b[3];
            c[3] = a[0] * b[3] + a[3] * b[0] + a[1] * b[2] - a[2] * b[1];
            return c;

        }
        public double[] mCalibrate(ref double[] a, ref double[] q)
        {
            if (q[0] == 0 || q[1] == 0 || q[2] == 0 || q[3] == 0)
            {
            }
            //q[0] = 0;
            //q[1] = 0;
            //q[2] = 1;
            //q[3] = 0;


            double[] v = new double[4];
            double[] c = new double[4];
            double[] m = new double[4];
            double[] r = null;
            c[0] = q[0];
            c[1] = -q[1];
            c[2] = -q[2];
            c[3] = -q[3];

            v[0] = 0;
            v[1] = a[0];
            v[2] = a[1];
            v[3] = a[2];
            m = mQmulti(v, c);
            r = mQmulti(q, m);
            r[0] = r[1];
            r[1] = r[2];
            r[2] = r[3];
            //r = mQrotation(ref v, ref q);
            return r;
        }

        private double[] getAzmuthBack(Double[] myPoint)
        {
            //map to desired axis
            double z = myPoint[0];
            double y = myPoint[1];
            double x = myPoint[2];

            double[] myResult = new double[2];
            //int i = 0;
            double azmuth = 0;
            double elevation = 0;
            double res = Math.Sqrt(x * x + y * y);

            if (y > 0 && x > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180;
            else if (x < 0 && y > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180 + 360;
            else if (x != 0 && y < 0) azmuth = 180 + (Math.Atan(x / y) / Math.PI) * 180;
            else if (y == 0)
            {
                if (x > 0) azmuth = 90;
                else if (x < 0) azmuth = 270;
                else azmuth = 0;
            }
            else if (x == 0)
            {
                if (y > 0) azmuth = 0;
                else if (y < 0) azmuth = 180;
                else azmuth = 0;
            }
            if (x == 0 && y == 0) elevation = z > 0 ? 90 : z == 0 ? 0 : -90;

            if (z > 0) elevation = (Math.Atan(z / res) / Math.PI) * 180;
            else if (z < 0) elevation = (Math.Atan(z / res) / Math.PI) * 180;
            else elevation = 0;

            myResult[0] = azmuth;
            myResult[1] = elevation;

            return myResult;
        }

        public static void transLoc(ref double[] a, byte loc)
        {
            double[] b = new double[3];
            b[0] = a[0];
            b[1] = a[1];
            b[2] = a[2];

            if ((loc & GFT_MNT_LOC_INOUTMSK) == 0) // if outside
            {


                if ((loc & GFT_MNT_LOC_USB_FRONT_REAR_MSK) > 0) //fornt
                {
                    a[0] = -b[0];
                    a[2] = -b[2];

                }
                else
                {
                    a[1] = -b[1];
                    a[2] = -b[2];
                }
            }
            else
            {
                if ((loc & GFT_MNT_LOC_USB_FRONT_REAR_MSK) > 0) //fornt
                {
                    ;


                }
                else
                {
                    a[0] = -b[0];
                    a[1] = -b[1];
                }
            }
            if ((loc & GFT_MNT_LOC_VER) != 0)
            {
                b[0] = a[0];
                b[1] = a[1];
                b[2] = a[2];

                a[0] = -b[1];
                a[1] = b[0];
            }


        }
        public static void lgEncode(ref byte[] gR, double gC)
        {
            short d = 0;
            d = (short)(gC / LOWG_SENSITIVITY_LSB_16G);
            int gR0 = (int)(d / 256);
            int gR1 = d % 256;
            if ((gR0 < 0))
                gR0 = 256 + gR0;
            if ((gR1 < 0))
                gR1 = 256 + gR1;

            gR[0] = (byte)gR0;
            gR[1] = (byte)gR1;
        }

        public static byte AccEncode(double c, byte mode)
        {
            short s;
            if (mode == 0)
            {
                if (c >= 200)
                    return (byte)255;
                if (c <= -200)
                    return 0;
                return (byte)(c / ACC_SENSITIVITY_LSB2 + 128);
            }
            else
            {


                if (c < 0)
                    return (byte)(255 - (byte)(Math.Abs(c / ACC_LOW_SENSITIVITY_LSB)));

                else

                    return (byte)((c / ACC_LOW_SENSITIVITY_LSB));
            }

            // return (ACC_SENSITIVITY_LSB2 * (c - 128));

        }
        public static double AccDecode(byte c, byte mode)
        {
            short tmp = 0;
            if (mode == 0)
            {
                //if((curGFT.hwMajor ==0x1) && (curGFT.hwMinor == 0))
                return (ACC_SENSITIVITY_LSB2 * (c - 128));

                //  return (ACC_SENSITIVITY_LSB * (c * 4 - 512));
            }
            else
            {
                tmp = (short)(c << 8);
                return (ACC_LOW_SENSITIVITY_LSB * (((short)(c << 8)) >> 8));
            }
        }


        public static void gyroEncode(ref byte[] gR, double gC)
        {
            short d;
            d = (short)(gC / GYRO_SENSITIVITY_LSB);
            gR[0] = (byte)(d / 256);
            gR[1] = (byte)(d % 256);

        }

#if false
        private double[] getAzmuthAutoOld(double[] myPoint, double[] iLocation)
        {
            //map to desired axis
            double z = myPoint[0];
            double y = myPoint[1];
            double x = myPoint[2];

            double[] b = new double[3];
            double[] v = new double[3];
            b[0] = myPoint[0];
            b[1] = myPoint[1];
            b[2] = myPoint[2];


            v = mCalibrate(ref b, ref iLocation);

            x = v[0];
            y = v[1];
            z = v[2];

            double[] myResult = new double[2];
            //int i = 0;
            double azmuth = 0;
            double elevation = 0;
            double res = Math.Sqrt(x * x + y * y);

            if (y > 0 && x >= 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180;
            else if (x < 0 && y > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180 + 360;
            else if (y < 0) azmuth = 180 + (Math.Atan(x / y) / Math.PI) * 180;
            else if (y == 0)
            {
                if (x > 0) azmuth = 90;
                else if (x < 0) azmuth = 270;
                else azmuth = 0;
            }

            if (x == 0 && y == 0) elevation = z > 0 ? 90 : z == 0 ? 0 : -90;

            if (z < 0) elevation = (-Math.Atan(Math.Abs(z / res)) / Math.PI) * 180;
            else if (z > 0) elevation = (Math.Atan(Math.Abs(z / res)) / Math.PI) * 180;
            else elevation = 0;

            myResult[0] = azmuth;
            myResult[1] = elevation;

            return myResult;
        }
#endif
        public static double[] getAzmuthAuto(Double[] myPoint)
        {
            //map to desired axis

            double x = myPoint[0];
            double y = myPoint[1];
            double z = myPoint[2];
#if true
            double[] b = new double[3];
            double[] v = new double[3];
            // b[0] = myPoint[0];
            // b[1] = myPoint[1];
            // b[2] = myPoint[2];
#if true
            //v = gCalc.mCalibrate(ref b, ref gCalc.qua);

            x = myPoint[0];// v[0];
            y = myPoint[1];///v[1];
            z = myPoint[2];// v[2];
#endif
#endif
            double[] myResult = new double[2];
            //int i = 0;
            double azmuth = 0;
            double elevation = 0;
            double res = Math.Sqrt(x * x + y * y);
            if (y > 0 && x >= 0)
            {
                azmuth = (Math.Atan(x / y) / Math.PI) * 180;

            }
            else if (x < 0 && y > 0)
            {

                azmuth = (Math.Atan(x / y) / Math.PI) * 180 + 360;

            }

            else if (y < 0)
            {

                azmuth = 180 + (Math.Atan(x / y) / Math.PI) * 180;

            }

            else if (y == 0)
            {
                if (x > 0)
                {
                    azmuth = 90;

                }
                else if (x < 0)
                {
                    azmuth = 270;

                }
                else
                {

                    azmuth = 0;
                }
            }

            if (x == 0 && y == 0)
                elevation = z > 0 ? 90 : z == 0 ? 0 : -90;
            else if (z < 0)
                elevation = (Math.Atan(Math.Abs(z / res)) / Math.PI) * 180;
            else if (z > 0)
                elevation = (-Math.Atan(Math.Abs(z / res)) / Math.PI) * 180;
            else
                elevation = 0;
            azmuth += 180;
            azmuth %= 360;

            myResult[0] = azmuth;
            myResult[1] = elevation;

            return myResult;
        }
#if USE_OF_LIST
#else

        private void gDataInsertImpact(Form1.IMPACT_DATA iData, string EventsTable)
        {
            System.Diagnostics.Debug.WriteLine("Writing Impact to Database: " + iData.sid + "::" + iData.eid);
            // Retrieve the connection string from the settings file.
            // Open the connection using the connection string.
            try
            {
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + EventsTable + " ON", con))
                    {
                        com.ExecuteNonQuery();
                        string query = "INSERT INTO " + EventsTable + " (EID,SID,IMPACT_TIME, O_IMPACT_TIME,LINEAR_ACC_X,LINEAR_ACC_Y," +
                                       " LINEAR_ACC_Z,LINEAR_ACC_RESULTANT,AZIMUTH,ELEVATION,ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z," +
                                       " ROTATIONAL_ACC_RESULTANT,HIC,GSI,PCS,LOCATION_ID,DATAX,DATAY,DATAZ,RDATAX,RDATAY,RDATAZ,millisecond, O_millisecond,UID, MAX_ROT_X, MAX_ROT_Y, MAX_ROT_Z, BrIC,DATAX_RAW,DATAY_RAW,DATAZ_RAW,RDATAX_RAW,RDATAY_RAW,RDATAZ_RAW)" +
                                       "VALUES(@EID,@SID,@IMPACT_TIME,@IMPACT_TIME,@LINEAR_ACC_X,@LINEAR_ACC_Y,@LINEAR_ACC_Z,@LINEAR_ACC_RESULTANT," +
                                       "@AZIMUTH, @ELEVATION, " +
                                       "@ROTATIONAL_ACC_X,@ROTATIONAL_ACC_Y,@ROTATIONAL_ACC_Z,@ROTATIONAL_ACC_RESULTANT,@HIC,@GSI,@PCS,@LOCATION_ID,@dataX,@dataY,@dataZ,@rdataX,@rdataY,@rdataZ,@millisecond,@millisecond,@UID,@MAX_ROT_X,@MAX_ROT_Y,@MAX_ROT_Z,@BrIC, @dataX_raw,@dataY_raw,@dataZ_raw,@rdataX_raw,@rdataY_raw,@rdataZ_raw)";
                        com.CommandText = query;
                        com.Parameters.AddWithValue("@EID", iData.eid);
                        com.Parameters.AddWithValue("@SID", iData.sid);
                        com.Parameters.AddWithValue("@IMPACT_TIME", iData.time);
                        com.Parameters.AddWithValue("@LINEAR_ACC_X", iData.linX);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Y", iData.linY);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Z", iData.linZ);
                        com.Parameters.AddWithValue("@LINEAR_ACC_RESULTANT", iData.linR);

                        com.Parameters.AddWithValue("@AZIMUTH", iData.azimuth);
                        com.Parameters.AddWithValue("@ELEVATION", iData.elevation);

                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_X", iData.rotX);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Y", iData.rotY);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Z", iData.rotZ);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_RESULTANT", iData.rotR);

                        com.Parameters.AddWithValue("@HIC", iData.hic);
                        com.Parameters.AddWithValue("@GSI", iData.gsi);
                        com.Parameters.AddWithValue("@PCS", iData.pcs);
                        com.Parameters.AddWithValue("@LOCATION_ID", iData.locationId);

                        com.Parameters.AddWithValue("@dataX", iData.dataX);
                        com.Parameters.AddWithValue("@dataY", iData.dataY);
                        com.Parameters.AddWithValue("@dataZ", iData.dataZ);

                        com.Parameters.AddWithValue("@rdataX", iData.rdataX);
                        com.Parameters.AddWithValue("@rdataY", iData.rdataY);
                        com.Parameters.AddWithValue("@rdataZ", iData.rdataZ);
                        com.Parameters.AddWithValue("@millisecond", iData.millisecond);
                        com.Parameters.AddWithValue("@UID", iData.UID);

                        com.Parameters.AddWithValue("@MAX_ROT_X", iData.iMaxRotX);
                        com.Parameters.AddWithValue("@MAX_ROT_Y", iData.iMaxRotY);
                        com.Parameters.AddWithValue("@MAX_ROT_Z", iData.iMaxRotZ);
                        com.Parameters.AddWithValue("@BrIC", iData.iBrIC);

                        com.Parameters.AddWithValue("@dataX_raw", iData.dataX_raw);
                        com.Parameters.AddWithValue("@dataY_raw", iData.dataY_raw);
                        com.Parameters.AddWithValue("@dataZ_raw", iData.dataZ_raw);

                        com.Parameters.AddWithValue("@rdataX_raw", iData.rdataX_raw);
                        com.Parameters.AddWithValue("@rdataY_raw", iData.rdataY_raw);
                        com.Parameters.AddWithValue("@rdataZ_raw", iData.rdataZ_raw);
                        num = com.ExecuteNonQuery();

                        //mythis.wirelessViewer.DebugDataBoxDisplay(" Event Insert     OK -->" + (iData.eid).ToString("x") + "\r\n");
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }

        private void gDataWUpdateEvent(Form1.IMPACT_DATA iData)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    //SID=@SID,UID=@SID,
                    string query = "UPDATE EventsWireless SET SID=@SID,IMPACT_TIME=@IMPACT_TIME,LINEAR_ACC_X=@LINEAR_ACC_X,LINEAR_ACC_Y=@LINEAR_ACC_Y," +
                                                            "LINEAR_ACC_Z=@LINEAR_ACC_Z,LINEAR_ACC_RESULTANT=@LINEAR_ACC_RESULTANT,AZIMUTH=@AZIMUTH,ELEVATION=@ELEVATION,"+
                                                            "ROTATIONAL_ACC_X=@ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y=@ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z=@ROTATIONAL_ACC_Z,"+
                                                            "ROTATIONAL_ACC_RESULTANT=@ROTATIONAL_ACC_RESULTANT,HIC=@HIC,GSI=@GSI,PCS=@PCS,LOCATION_ID=@LOCATION_ID,"+
                                                            "DATAX=@DATAX,DATAY=@DATAY,DATAZ=@DATAZ,RDATAX=@RDATAX,RDATAY=@RDATAY,RDATAZ=@RDATAZ,millisecond=@millisecond,UID=@UID, MAX_ROT_X=@MAX_ROT_X, MAX_ROT_Y=@MAX_ROT_Y, MAX_ROT_Z=@MAX_ROT_Z, BrIC=@BrIC"
                                                            +" WHERE (EID = @EID)";


                    //string query = "INSERT INTO " + "EventWireless" + " (EID,SID,IMPACT_TIME,LINEAR_ACC_X,LINEAR_ACC_Y," +
                    //               " LINEAR_ACC_Z,LINEAR_ACC_RESULTANT,AZIMUTH,ELEVATION,ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z," +
                    //               " ROTATIONAL_ACC_RESULTANT,HIC,GSI,PCS,LOCATION_ID,DATAX,DATAY,DATAZ,RDATAX,RDATAY,RDATAZ,millisecond,UID)" +
                    //               "VALUES(@EID,@SID,@IMPACT_TIME,@LINEAR_ACC_X,@LINEAR_ACC_Y,@LINEAR_ACC_Z,@LINEAR_ACC_RESULTANT," +
                    //               "@AZIMUTH, @ELEVATION, " +
                    //               "@ROTATIONAL_ACC_X,@ROTATIONAL_ACC_Y,@ROTATIONAL_ACC_Z,@ROTATIONAL_ACC_RESULTANT,@HIC,@GSI,@PCS,@LOCATION_ID,@dataX,@dataY,@dataZ,@rdataX,@rdataY,@rdataZ,@millisecond,@UID)";

#if true
                    //using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        //com.ExecuteNonQuery();
                        //com.CommandText = query;
                        com.Parameters.AddWithValue("@EID", iData.eid);
                        com.Parameters.AddWithValue("@SID", iData.sid);
                        com.Parameters.AddWithValue("@IMPACT_TIME", iData.time);
                        com.Parameters.AddWithValue("@LINEAR_ACC_X", iData.linX);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Y", iData.linY);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Z", iData.linZ);
                        com.Parameters.AddWithValue("@LINEAR_ACC_RESULTANT", iData.linR);

                        com.Parameters.AddWithValue("@AZIMUTH", iData.azimuth);
                        com.Parameters.AddWithValue("@ELEVATION", iData.elevation);

                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_X", iData.rotX);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Y", iData.rotY);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Z", iData.rotZ);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_RESULTANT", iData.rotR);

                        com.Parameters.AddWithValue("@HIC", iData.hic);
                        com.Parameters.AddWithValue("@GSI", iData.gsi);
                        com.Parameters.AddWithValue("@PCS", iData.pcs);
                        com.Parameters.AddWithValue("@LOCATION_ID", iData.locationId);

                        com.Parameters.AddWithValue("@DATAX", iData.dataX);
                        com.Parameters.AddWithValue("@DATAY", iData.dataY);
                        com.Parameters.AddWithValue("@DATAZ", iData.dataZ);

                        com.Parameters.AddWithValue("@RDATAX", iData.rdataX);
                        com.Parameters.AddWithValue("@RDATAY", iData.rdataY);
                        com.Parameters.AddWithValue("@RDATAZ", iData.rdataZ);
                        com.Parameters.AddWithValue("@millisecond", iData.millisecond);
                        com.Parameters.AddWithValue("@UID", iData.UID);

                        com.Parameters.AddWithValue("@MAX_ROT_X", iData.iMaxRotX);
                        com.Parameters.AddWithValue("@MAX_ROT_Y", iData.iMaxRotY);
                        com.Parameters.AddWithValue("@MAX_ROT_Z", iData.iMaxRotZ);
                        com.Parameters.AddWithValue("@BrIC", iData.iBrIC);

                        num = com.ExecuteNonQuery();

                        //wirelessViewer.DebugDataBoxDisplay("Session Update OK!....\r\n");
                        //mythis.wirelessViewer.DebugDataBoxDisplay(" Session Update OK -->" + (sessionData.SID).ToString("x") + "\r\n");
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                //string dd = me.ToString();
                //throw;
                throw me;
                //mythis.wirelessViewer.DebugDataBoxDisplay("Session Update" + me.Message + "\r\n");
            }
        }

        private bool getEventWireless(Form1.IMPACT_DATA iData, int node_id)                             // Added by Jason Chen for Duplicated record check, 2014.02.13
        {
            bool ret = false;

            //if (node_id > 63 || node_id < 1) return ret;

            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();
                    //if (mycon.State != ConnectionState.Open)
                    //    return false;

                    SqlCeCommand cmd = new SqlCeCommand("SELECT EID,SID FROM EventsWireless WHERE (IMPACT_TIME = @mIMPACT_TIME) AND (millisecond = @mMillisecond) AND (UID = @mUID)", mycon);

                    cmd.Parameters.AddWithValue("@mIMPACT_TIME", iData.time);
                    cmd.Parameters.AddWithValue("@mMillisecond", iData.millisecond);
                    cmd.Parameters.AddWithValue("@mUID", node_id);

                    //int mySession = mythis.NodeProfile[node_id - 1].SessionID;

                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            //iData.eid = (Int32)rdr["EID"];
                            //iData.sid = (Int32)rdr["SID"];

                            mythis.NodeProfile[node_id - 1].SessionID = (Int32)rdr["SID"];
                            mythis.NodeProfile[node_id - 1].EventID   = (Int32)rdr["EID"];
                            ret = true;
                            System.Diagnostics.Debug.WriteLine("Node: " + node_id + " Found Dupe: " + rdr["SID"] + "::" + rdr["EID"]);

                            if (mythis.RxLog != null && mythis.RxLog.isCaptureType(node_id, Convert.ToInt16(mythis.wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = "PT" + node_id.ToString("D2") + ":                                                                                       Download - Ignoring Duplicate Impact(Research)*****";
                                mythis.RxLog.logText(txtBuff);
                            }
                        }
                        else
                            ret = false;
                    }
                    catch (Exception me)
                    {
                        //mythis.m_Edit_Immediate.AppendText(me.Message + "\r\n");
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }


        private bool getSessionWireless(SESSION_DATA sData, int node_id)                             // Added by Jason Chen for Duplicated record check, 2014.02.13
        {
            bool ret = false;
            
            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();

                    SqlCeCommand cmd = new SqlCeCommand("SELECT SID FROM SessionWireless WHERE (O_SDATE = @O_SDATE) AND (UID = @mUID)", mycon);

                    cmd.Parameters.AddWithValue("@O_SDATE", sData.Date);
                    cmd.Parameters.AddWithValue("@mUID", node_id);

                    //int mySession = mythis.NodeProfile[node_id - 1].SessionID;

                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            //iData.eid = (Int32)rdr["EID"];
                            //iData.sid = (Int32)rdr["SID"];

                            mythis.NodeProfile[node_id - 1].SessionID = (Int32)rdr["SID"];
                            ret = true;
                        }
                        else
                            ret = false;
                    }
                    catch (Exception me)
                    {
                        //mythis.m_Edit_Immediate.AppendText(me.Message + "\r\n");
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }

        private bool CheckEventWirelessByEID(Form1.IMPACT_DATA iData, int node_id)                             // Added by Jason Chen for Duplicated record check, 2014.02.13
        {
            bool ret = false;

            //if (node_id > 63 || node_id < 1) return ret;

            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();
                    //if (mycon.State != ConnectionState.Open)
                    //    return false;

                    SqlCeCommand cmd = new SqlCeCommand("SELECT EID,SID FROM EventsWireless WHERE EID = @mEID AND SID = @SID", mycon);

                    cmd.Parameters.AddWithValue("@mEID", iData.eid);
                    cmd.Parameters.AddWithValue("@mEID", iData.sid);
                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            //mythis.NodeProfile[node_id - 1].SessionID = (Int32)rdr["SID"];
                            //mythis.NodeProfile[node_id - 1].EventID = (Int32)rdr["EID"];
                            ret = true;
                        }
                        else
                            ret = false;
                    }
                    catch (Exception me)
                    {
                        //mythis.m_Edit_Immediate.AppendText(me.Message + "\r\n");
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }

        private bool getSessionWireless(int node_id)
        {
            bool ret = false;

            if (node_id > MAX_DEVICE_CNT || node_id < 1) return ret;

            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();
                    //if (mycon.State != ConnectionState.Open)
                    //    return false;

                    SqlCeCommand cmd = new SqlCeCommand("SELECT SID,UID,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH,THRESHOLD FROM SessionWireless WHERE (UID = @nodeID) AND (SID = @mSID)", mycon);

                    cmd.Parameters.AddWithValue("@nodeID", node_id);
                    int mySession = mythis.NodeProfile[node_id - 1].SessionID;
                    cmd.Parameters.AddWithValue("@mSID", mySession);
                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            mythis.NodeProfile[node_id - 1].IMPACT = (Int32)rdr["IMPACT"];
                            mythis.NodeProfile[node_id - 1].WTH = (Int32)rdr["WTH"];
                            mythis.NodeProfile[node_id - 1].ALARM = (int)rdr["ALARM"];
                            ret = true;
                        }
                        else
                        {
                            mythis.NodeProfile[node_id - 1].IMPACT = 0;
                            mythis.NodeProfile[node_id - 1].WTH    = 0;
                            mythis.NodeProfile[node_id - 1].ALARM  = 0;
                            ret = false;
                        }
                    }
                    catch (Exception me)
                    {
                        //mythis.m_Edit_Immediate.AppendText(me.Message + "\r\n");
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }


        private void gDataWUpdateSession(SESSION_DATA sessionData, string SessionTable, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    //SID=@SID,UID=@SID,
                    // ' SDATE=CASE WHEN @SDATE < SDATE Then @SDATE Else SDATE END,ON_TIME=CASE WHEN @ON_TIME < ON_TIME Then @ON_TIME Else ON_TIME END,
                    //CASE WHEN @ON_TIME < ON_TIME Then @ON_TIME Else ON_TIME END
                    string query = "UPDATE SessionWireless SET GID=@GID,SPORTS=@SPORTS,NAME=@NAME,SDATE=@SDATE,ON_TIME=@ON_TIME,OFF_TIME=@OFF_TIME,O_OFF_TIME=@O_OFF_TIME,DURATION=DATEDIFF(minute, @ON_TIME,@OFF_TIME),IMPACT=@IMPACT,ALARM=@ALARM,WTH_RANGE=@WTH_RANGE,WTH=@WTH,THRESHOLD=@THRESHOLD,ExerciseType=@ExerciseType,ACTIVE=@ACTIVE, ROT_THRESHOLD=@ROT_THRESHOLD,GFT_FIRMWARE=@GFT_FIRMWARE,GFT_SOFTWARE=@GFT_SOFTWARE,iLOCATION0=@iLOCATION0,iLOCATION1=@iLOCATION1,iLOCATION2=@iLOCATION2,iLOCATION3=@iLOCATION3,MNT_INSIDE=@MNT_INSIDE,UDATE=@UDATE,BAT_STATUS=@BAT_STATUS, coefficient=@coefficient@"
                                 + " WHERE (UID = @UID) AND (SID = @SID)";

#if true
                    //using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        //com.ExecuteNonQuery();
                        //com.CommandText = query;

                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@GID", sessionData.GID);        // Added by jason Chen according Jerad's requirement, 2014.06.16
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@NAME",   sessionData.name);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@O_OFF_TIME", sessionData.OffTime);
                        //com.Parameters.AddWithValue("@DURATION", sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        com.Parameters.AddWithValue("@ACTIVE", sessionData.activeTime);

                        com.Parameters.AddWithValue("@ROT_THRESHOLD", "2000");
                        com.Parameters.AddWithValue("@GFT_FIRMWARE", sessionData.fwVersion);
                        com.Parameters.AddWithValue("@GFT_SOFTWARE", sessionData.swVersion);
                        com.Parameters.AddWithValue("@iLOCATION0", sessionData.iLocation[0]);
                        com.Parameters.AddWithValue("@iLOCATION1", sessionData.iLocation[1]);
                        com.Parameters.AddWithValue("@iLOCATION2", sessionData.iLocation[2]);
                        com.Parameters.AddWithValue("@iLOCATION3", sessionData.iLocation[3]);
                        com.Parameters.AddWithValue("@MNT_INSIDE", sessionData.iMntInside);
                        com.Parameters.AddWithValue("@coefficient@", sessionData.fCoefficient);
                        com.Parameters.AddWithValue("@UDATE", DateTime.UtcNow);
                        //if (!mythis.NodeProfile[nodeID - 1].max17047Exist) com.Parameters.AddWithValue("@BAT_STATUS", Math.Round((float)mythis.NodeProfile[nodeID - 1].bat_level / WirelessGFTViewer.LP_HUB.BATT_LEVEL_100PECENT * 100));
                        com.Parameters.AddWithValue("@BAT_STATUS", Math.Round((float)mythis.NodeProfile[nodeID - 1].bat_level / WirelessGFTViewer.LP_HUB.BATT_LEVEL_100PECENT * 100));
                        
                        num = com.ExecuteNonQuery();

                        //wirelessViewer.DebugDataBoxDisplay("Session Update OK!....\r\n");
                        //mythis.wirelessViewer.DebugDataBoxDisplay(" Session Update OK -->" + (sessionData.SID).ToString("x") + "\r\n");
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                System.Diagnostics.Debug.WriteLine(me.ToString());
                //string dd = me.ToString();
                //throw;
                //throw me;
                //mythis.wirelessViewer.DebugDataBoxDisplay("Session Update" + me.Message + "\r\n");
            }
        }

        private void gDataInsertSession(SESSION_DATA sessionData, string SessionTable, string dongle_mid, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
#if true
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    {
                        com.ExecuteNonQuery();

                        string query = "INSERT INTO " + SessionTable + " (SID,UID,GID,SPORTS,NAME,SDATE,O_SDATE,ON_TIME,O_ON_TIME,OFF_TIME,O_OFF_TIME,DURATION,IMPACT,ALARM,WTH_RANGE,WTH,THRESHOLD,ExerciseType,ACTIVE, ERASED, MID,ROT_THRESHOLD,GFT_FIRMWARE,GFT_SOFTWARE,iLOCATION0,iLOCATION1,iLOCATION2,iLOCATION3,MNT_INSIDE,UDATE,BAT_STATUS, coefficient )" +
                            "VALUES(@SID,@UID,@GID,@SPORTS,@NAME,@SDATE, @O_SDATE,@ON_TIME,@ON_TIME,@OFF_TIME,@OFF_TIME,@DURATION,@IMPACT,@ALARM,@WTH_RANGE,@WTH,@THRESHOLD,@ExerciseType,@ACTIVE,@ERASED,@MID,@ROT_THRESHOLD,@GFT_FIRMWARE,@GFT_SOFTWARE,@iLOCATION0,@iLOCATION1,@iLOCATION2,@iLOCATION3,@MNT_INSIDE,@UDATE,@BAT_STATUS,@coefficient@ )";

                        com.CommandText = query;
                        com.Parameters.AddWithValue("@SID",          sessionData.SID);
                        com.Parameters.AddWithValue("@UID",          sessionData.UID);
                        com.Parameters.AddWithValue("@GID",          sessionData.GID);
                        com.Parameters.AddWithValue("@SPORTS",       sessionData.sports);
                        com.Parameters.AddWithValue("@NAME",         sessionData.name);
                        com.Parameters.AddWithValue("@SDATE",        sessionData.Date);
                        com.Parameters.AddWithValue("@O_SDATE",      sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME",      sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME",     sessionData.OffTime);
                        com.Parameters.AddWithValue("@DURATION",     sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT",       sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM",        sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE",    sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH",          sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD",    sessionData.Threshold);
                        com.Parameters.AddWithValue("@ACTIVE",       sessionData.activeTime);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        com.Parameters.AddWithValue("@ERASED",       "N");
                        com.Parameters.AddWithValue("@MID", dongle_mid);
                        com.Parameters.AddWithValue("@ROT_THRESHOLD", "2000");
                        com.Parameters.AddWithValue("@GFT_FIRMWARE", sessionData.fwVersion);
                        com.Parameters.AddWithValue("@GFT_SOFTWARE", sessionData.swVersion);
                        com.Parameters.AddWithValue("@iLOCATION0", sessionData.iLocation[0]);
                        com.Parameters.AddWithValue("@iLOCATION1", sessionData.iLocation[1]);
                        com.Parameters.AddWithValue("@iLOCATION2", sessionData.iLocation[2]);
                        com.Parameters.AddWithValue("@iLOCATION3", sessionData.iLocation[3]);
                        com.Parameters.AddWithValue("@MNT_INSIDE", sessionData.iMntInside);
                        com.Parameters.AddWithValue("@UDATE", DateTime.UtcNow);
                        com.Parameters.AddWithValue("@BAT_STATUS", mythis.NodeProfile[nodeID - 1].bat_level);
                        com.Parameters.AddWithValue("@coefficient@", sessionData.fCoefficient);
                        num = com.ExecuteNonQuery();

                        //mythis.wirelessViewer.DebugDataBoxDisplay(" Session Insert OK -->" + (sessionData.SID).ToString("x") + "\r\n");
                    }
#endif
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string dd = me.ToString();
                throw;
            }
        }

        private void gDataInsertPerf(Form1.IMPACT_DATA iData, string EventsTable)
        {
            System.Diagnostics.Debug.WriteLine("Writing Performance Data to Database: " + iData.sid + "::" + iData.eid);
            // Retrieve the connection string from the settings file.
            // Open the connection using the connection string.
            try
            {
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    //int uID = 1;
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + EventsTable + " ON", con))
                    {
                        com.ExecuteNonQuery();
                        string query = "INSERT INTO " + EventsTable + " (EID,SID,PERF_TIME, O_PERF_TIME,LINEAR_ACC_X,LINEAR_ACC_Y," +
                                       " LINEAR_ACC_Z,LINEAR_ACC_RESULTANT,AZIMUTH,ELEVATION,ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z," +
                                       " ROTATIONAL_ACC_RESULTANT,LOCATION_ID,DATAX,DATAY,DATAZ,RDATAX,RDATAY,RDATAZ,millisecond, O_millisecond,UID, MAX_ROT_X, MAX_ROT_Y, MAX_ROT_Z, BrIC,DATAX_RAW,DATAY_RAW,DATAZ_RAW,RDATAX_RAW,RDATAY_RAW,RDATAZ_RAW," +
                                       " bucket_1, bucket_2, bucket_3, bucket_4, bucket_5, bucket_6, bucket_7, bucket_8, bucket_9, bucket_10, exp_count, exp_power)" +
                                       "VALUES(@EID,@SID,@IMPACT_TIME,@IMPACT_TIME,@LINEAR_ACC_X,@LINEAR_ACC_Y,@LINEAR_ACC_Z,@LINEAR_ACC_RESULTANT," +
                                       "@AZIMUTH, @ELEVATION, " +
                                       "@ROTATIONAL_ACC_X,@ROTATIONAL_ACC_Y,@ROTATIONAL_ACC_Z,@ROTATIONAL_ACC_RESULTANT,@LOCATION_ID,@dataX,@dataY,@dataZ,@rdataX,@rdataY,@rdataZ,@millisecond,@millisecond,@UID,@MAX_ROT_X,@MAX_ROT_Y,@MAX_ROT_Z,@BrIC, @dataX_raw,@dataY_raw,@dataZ_raw,@rdataX_raw,@rdataY_raw,@rdataZ_raw," +
                                       "@Bucket_1@,@Bucket_2@,@Bucket_3@,@Bucket_4@,@Bucket_5@,@Bucket_6@,@Bucket_7@,@Bucket_8@,@Bucket_9@,@Bucket_10@,@EXP_COUNT@,@EXP_POWER@)";
                        com.CommandText = query;
                        com.Parameters.AddWithValue("@EID", iData.eid);
                        com.Parameters.AddWithValue("@SID", iData.sid);
                        com.Parameters.AddWithValue("@IMPACT_TIME", iData.time);
                        com.Parameters.AddWithValue("@LINEAR_ACC_X", iData.linX);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Y", iData.linY);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Z", iData.linZ);
                        com.Parameters.AddWithValue("@LINEAR_ACC_RESULTANT", iData.linR);

                        com.Parameters.AddWithValue("@AZIMUTH", iData.azimuth);
                        com.Parameters.AddWithValue("@ELEVATION", iData.elevation);

                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_X", iData.rotX);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Y", iData.rotY);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Z", iData.rotZ);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_RESULTANT", iData.rotR);

                        //com.Parameters.AddWithValue("@HIC", iData.hic);
                        //com.Parameters.AddWithValue("@GSI", iData.gsi);
                        //com.Parameters.AddWithValue("@PCS", iData.pcs);
                        com.Parameters.AddWithValue("@LOCATION_ID", iData.locationId);

                        com.Parameters.AddWithValue("@dataX", iData.dataX);
                        com.Parameters.AddWithValue("@dataY", iData.dataY);
                        com.Parameters.AddWithValue("@dataZ", iData.dataZ);

                        com.Parameters.AddWithValue("@rdataX", iData.rdataX);
                        com.Parameters.AddWithValue("@rdataY", iData.rdataY);
                        com.Parameters.AddWithValue("@rdataZ", iData.rdataZ);
                        com.Parameters.AddWithValue("@millisecond", iData.millisecond);
                        com.Parameters.AddWithValue("@UID", iData.UID);

                        com.Parameters.AddWithValue("@MAX_ROT_X", iData.iMaxRotX);
                        com.Parameters.AddWithValue("@MAX_ROT_Y", iData.iMaxRotY);
                        com.Parameters.AddWithValue("@MAX_ROT_Z", iData.iMaxRotZ);
                        com.Parameters.AddWithValue("@BrIC", iData.iBrIC);

                        com.Parameters.AddWithValue("@dataX_raw", iData.dataX_raw);
                        com.Parameters.AddWithValue("@dataY_raw", iData.dataY_raw);
                        com.Parameters.AddWithValue("@dataZ_raw", iData.dataZ_raw);

                        com.Parameters.AddWithValue("@rdataX_raw", iData.rdataX_raw);
                        com.Parameters.AddWithValue("@rdataY_raw", iData.rdataY_raw);
                        com.Parameters.AddWithValue("@rdataZ_raw", iData.rdataZ_raw);

                        com.Parameters.AddWithValue("@Bucket_1@", iData.buckets[0]);
                        com.Parameters.AddWithValue("@Bucket_2@", iData.buckets[1]);
                        com.Parameters.AddWithValue("@Bucket_3@", iData.buckets[2]);
                        com.Parameters.AddWithValue("@Bucket_4@", iData.buckets[3]);
                        com.Parameters.AddWithValue("@Bucket_5@", iData.buckets[4]);
                        com.Parameters.AddWithValue("@Bucket_6@", iData.buckets[5]);
                        com.Parameters.AddWithValue("@Bucket_7@", iData.buckets[6]);
                        com.Parameters.AddWithValue("@Bucket_8@", iData.buckets[7]);
                        com.Parameters.AddWithValue("@Bucket_9@", iData.buckets[8]);
                        com.Parameters.AddWithValue("@Bucket_10@", iData.buckets[9]);
                        com.Parameters.AddWithValue("@EXP_COUNT@", iData.buckets[10]); //exp_count
                        com.Parameters.AddWithValue("@EXP_POWER@", iData.buckets[11]); //exp_power
                        // bucket_1, bucket_2, bucket_3, bucket_4, bucket_5, bucket_6, bucket_7, bucket_8, bucket_9, bucket_10, exp_count, exp_power
                        // @Bucket_1@,@Bucket_2@,@Bucket_3@,@Bucket_4@,@Bucket_5@,@Bucket_6@,@Bucket_7@,@Bucket_8@,@Bucket_9@,@Bucket_10@,@EXP_COUNT@,@EXP_POWER@
                        // bucket_1=@Bucket_1@, bucket_2=@Bucket_2@, bucket_3=@Bucket_3@, bucket_4=@Bucket_4@, bucket_5=@Bucket_5@, bucket_6=@Bucket_6@, bucket_7=@Bucket_7@, bucket_8=@Bucket_8@, bucket_9=@Bucket_9@, bucket_10=@Bucket_10@, exp_count=@EXP_COUNT@, exp_power=@EXP_POWER@

                        num = com.ExecuteNonQuery();

                        //mythis.wirelessViewer.DebugDataBoxDisplay(" Event Insert     OK -->" + (iData.eid).ToString("x") + "\r\n");
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }

        private void gDataWUpdatePerfEvent(Form1.IMPACT_DATA iData)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    string query = "UPDATE PerfWireless SET SID=@SID,IMPACT_TIME=@IMPACT_TIME,LINEAR_ACC_X=@LINEAR_ACC_X,LINEAR_ACC_Y=@LINEAR_ACC_Y," +
                                                            "LINEAR_ACC_Z=@LINEAR_ACC_Z,LINEAR_ACC_RESULTANT=@LINEAR_ACC_RESULTANT,AZIMUTH=@AZIMUTH,ELEVATION=@ELEVATION," +
                                                            "ROTATIONAL_ACC_X=@ROTATIONAL_ACC_X,ROTATIONAL_ACC_Y=@ROTATIONAL_ACC_Y,ROTATIONAL_ACC_Z=@ROTATIONAL_ACC_Z," +
                                                            "ROTATIONAL_ACC_RESULTANT=@ROTATIONAL_ACC_RESULTANT,HIC=@HIC,GSI=@GSI,PCS=@PCS,LOCATION_ID=@LOCATION_ID," +
                                                            "DATAX=@DATAX,DATAY=@DATAY,DATAZ=@DATAZ,RDATAX=@RDATAX,RDATAY=@RDATAY,RDATAZ=@RDATAZ,millisecond=@millisecond,UID=@UID, MAX_ROT_X=@MAX_ROT_X, MAX_ROT_Y=@MAX_ROT_Y, MAX_ROT_Z=@MAX_ROT_Z, BrIC=@BrIC, " +
                                                            "bucket_1=@Bucket_1@, bucket_2=@Bucket_2@, bucket_3=@Bucket_3@, bucket_4=@Bucket_4@, bucket_5=@Bucket_5@, bucket_6=@Bucket_6@, bucket_7=@Bucket_7@, bucket_8=@Bucket_8@, bucket_9=@Bucket_9@, bucket_10=@Bucket_10@, exp_count=@EXP_COUNT@, exp_power=@EXP_POWER@ " +
                                                            " WHERE (EID = @EID)";

                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@EID", iData.eid);
                        com.Parameters.AddWithValue("@SID", iData.sid);
                        com.Parameters.AddWithValue("@IMPACT_TIME", iData.time);
                        com.Parameters.AddWithValue("@LINEAR_ACC_X", iData.linX);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Y", iData.linY);
                        com.Parameters.AddWithValue("@LINEAR_ACC_Z", iData.linZ);
                        com.Parameters.AddWithValue("@LINEAR_ACC_RESULTANT", iData.linR);

                        com.Parameters.AddWithValue("@AZIMUTH", iData.azimuth);
                        com.Parameters.AddWithValue("@ELEVATION", iData.elevation);

                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_X", iData.rotX);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Y", iData.rotY);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_Z", iData.rotZ);
                        com.Parameters.AddWithValue("@ROTATIONAL_ACC_RESULTANT", iData.rotR);

                        com.Parameters.AddWithValue("@HIC", iData.hic);
                        com.Parameters.AddWithValue("@GSI", iData.gsi);
                        com.Parameters.AddWithValue("@PCS", iData.pcs);
                        com.Parameters.AddWithValue("@LOCATION_ID", iData.locationId);

                        com.Parameters.AddWithValue("@DATAX", iData.dataX);
                        com.Parameters.AddWithValue("@DATAY", iData.dataY);
                        com.Parameters.AddWithValue("@DATAZ", iData.dataZ);

                        com.Parameters.AddWithValue("@RDATAX", iData.rdataX);
                        com.Parameters.AddWithValue("@RDATAY", iData.rdataY);
                        com.Parameters.AddWithValue("@RDATAZ", iData.rdataZ);
                        com.Parameters.AddWithValue("@millisecond", iData.millisecond);
                        com.Parameters.AddWithValue("@UID", iData.UID);

                        com.Parameters.AddWithValue("@MAX_ROT_X", iData.iMaxRotX);
                        com.Parameters.AddWithValue("@MAX_ROT_Y", iData.iMaxRotY);
                        com.Parameters.AddWithValue("@MAX_ROT_Z", iData.iMaxRotZ);
                        com.Parameters.AddWithValue("@BrIC", iData.iBrIC);

                        com.Parameters.AddWithValue("@Bucket_1@", iData.buckets[0]);
                        com.Parameters.AddWithValue("@Bucket_2@", iData.buckets[1]);
                        com.Parameters.AddWithValue("@Bucket_3@", iData.buckets[2]);
                        com.Parameters.AddWithValue("@Bucket_4@", iData.buckets[3]);
                        com.Parameters.AddWithValue("@Bucket_5@", iData.buckets[4]);
                        com.Parameters.AddWithValue("@Bucket_6@", iData.buckets[5]);
                        com.Parameters.AddWithValue("@Bucket_7@", iData.buckets[6]);
                        com.Parameters.AddWithValue("@Bucket_8@", iData.buckets[7]);
                        com.Parameters.AddWithValue("@Bucket_9@", iData.buckets[8]);
                        com.Parameters.AddWithValue("@Bucket_10@", iData.buckets[9]);
                        com.Parameters.AddWithValue("@EXP_COUNT@", iData.exp_count);
                        com.Parameters.AddWithValue("@EXP_POWER@", iData.exp_power);
                        // bucket_1=@Bucket_1@, bucket_2=@Bucket_2@, bucket_3=@Bucket_3@, bucket_4=@Bucket_4@, bucket_5=@Bucket_5@, bucket_6=@Bucket_6@, bucket_7=@Bucket_7@, bucket_8=@Bucket_8@, bucket_9=@Bucket_9@, bucket_10=@Bucket_10@, exp_count=@EXP_COUNT@, exp_power=@EXP_POWER@

                        num = com.ExecuteNonQuery();

                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                throw me;
            }
        }

        private bool getPerfWireless(Form1.IMPACT_DATA iData, int node_id)
        {
            bool ret = false;

            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();

                    SqlCeCommand cmd = new SqlCeCommand("SELECT EID,SID FROM PerfWireless WHERE (PERF_TIME = @mIMPACT_TIME) AND (millisecond = @mMillisecond) AND (UID = @mUID)", mycon);

                    cmd.Parameters.AddWithValue("@mIMPACT_TIME", iData.time);
                    cmd.Parameters.AddWithValue("@mMillisecond", iData.millisecond);
                    cmd.Parameters.AddWithValue("@mUID", node_id);


                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            mythis.NodeProfile[node_id - 1].SessionID = (Int32)rdr["SID"];
                            mythis.NodeProfile[node_id - 1].EventID = (Int32)rdr["EID"];
                            ret = true;
                            System.Diagnostics.Debug.WriteLine("Node: " + node_id + " Found Dupe: " + rdr["SID"] + "::" + rdr["EID"]);

                            if (mythis.RxLog != null && mythis.RxLog.isCaptureType(node_id, Convert.ToInt16(mythis.wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                            {
                                var txtBuff = "PT" + node_id.ToString("D2") + ": Download - Ignoring Duplicate Impact";
                                mythis.RxLog.logText(txtBuff);
                            }
                        }
                        else
                            ret = false;
                    }
                    catch (Exception me)
                    {
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }


        private bool getPerfSessionWireless(SESSION_DATA sData, int node_id)                             // Added by Jason Chen for Duplicated record check, 2014.02.13
        {
            bool ret = false;

            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();

                    SqlCeCommand cmd = new SqlCeCommand("SELECT SID FROM PerfSessionWireless WHERE (O_SDATE = @O_SDATE) AND (UID = @mUID)", mycon);

                    cmd.Parameters.AddWithValue("@O_SDATE", sData.Date);
                    cmd.Parameters.AddWithValue("@mUID", node_id);

                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            mythis.NodeProfile[node_id - 1].SessionID = (Int32)rdr["SID"];
                            ret = true;
                        }
                        else
                            ret = false;
                    }
                    catch (Exception me)
                    {
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }

        private bool CheckPerfWirelessByEID(Form1.IMPACT_DATA iData, int node_id)                             // Added by Jason Chen for Duplicated record check, 2014.02.13
        {
            bool ret = false;

            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();

                    SqlCeCommand cmd = new SqlCeCommand("SELECT EID,SID FROM PerfWireless WHERE EID = @mEID AND SID = @SID", mycon);

                    cmd.Parameters.AddWithValue("@mEID", iData.eid);
                    cmd.Parameters.AddWithValue("@mEID", iData.sid);
                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            ret = true;
                        }
                        else
                            ret = false;
                    }
                    catch (Exception me)
                    {
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }

        private bool getPerfSessionWireless(int node_id)
        {
            bool ret = false;

            if (node_id > MAX_DEVICE_CNT || node_id < 1) return ret;

            try
            {
                using (SqlCeConnection mycon = new SqlCeConnection(conString))
                {
                    mycon.Open();

                    SqlCeCommand cmd = new SqlCeCommand("SELECT SID,UID,ON_TIME,OFF_TIME,DURATION,IMPACT,ALARM,WTH,THRESHOLD FROM PerfSessionWireless WHERE (UID = @nodeID) AND (SID = @mSID)", mycon);

                    cmd.Parameters.AddWithValue("@nodeID", node_id);
                    int mySession = mythis.NodeProfile[node_id - 1].SessionID;
                    cmd.Parameters.AddWithValue("@mSID", mySession);
                    try
                    {
                        SqlCeDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            mythis.NodeProfile[node_id - 1].IMPACT = (Int32)rdr["IMPACT"];
                            mythis.NodeProfile[node_id - 1].WTH = (Int32)rdr["WTH"];
                            mythis.NodeProfile[node_id - 1].ALARM = (int)rdr["ALARM"];
                            ret = true;
                        }
                        else
                        {
                            mythis.NodeProfile[node_id - 1].IMPACT = 0;
                            mythis.NodeProfile[node_id - 1].WTH = 0;
                            mythis.NodeProfile[node_id - 1].ALARM = 0;
                            ret = false;
                        }
                    }
                    catch (Exception me)
                    {
                        throw me;
                    }
                    return ret;
                }
            }
            catch (Exception me)
            {

                string ddd = me.ToString();
                throw;
            }
        }


        private void gDataWUpdatePerfSession(SESSION_DATA sessionData, string SessionTable, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    string query = "UPDATE PerfSessionWireless SET GID=@GID,SPORTS=@SPORTS,NAME=@NAME,SDATE=@SDATE,ON_TIME=@ON_TIME,OFF_TIME=@OFF_TIME,O_OFF_TIME=@O_OFF_TIME,DURATION=DATEDIFF(minute, @ON_TIME,@OFF_TIME),IMPACT=@IMPACT,ALARM=@ALARM,WTH_RANGE=@WTH_RANGE,WTH=@WTH,THRESHOLD=@THRESHOLD,ExerciseType=@ExerciseType,ACTIVE=@ACTIVE, ROT_THRESHOLD=@ROT_THRESHOLD,GFT_FIRMWARE=@GFT_FIRMWARE,GFT_SOFTWARE=@GFT_SOFTWARE,iLOCATION0=@iLOCATION0,iLOCATION1=@iLOCATION1,iLOCATION2=@iLOCATION2,iLOCATION3=@iLOCATION3,MNT_INSIDE=@MNT_INSIDE,UDATE=@UDATE,BAT_STATUS=@BAT_STATUS, coefficient=@coefficient@"
                                 + " WHERE (UID = @UID) AND (SID = @SID)";

                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {

                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@GID", sessionData.GID);        // Added by jason Chen according Jerad's requirement, 2014.06.16
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@NAME", sessionData.name);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@O_OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        com.Parameters.AddWithValue("@ACTIVE", sessionData.activeTime);

                        com.Parameters.AddWithValue("@ROT_THRESHOLD", "2000");
                        com.Parameters.AddWithValue("@GFT_FIRMWARE", sessionData.fwVersion);
                        com.Parameters.AddWithValue("@GFT_SOFTWARE", sessionData.swVersion);
                        com.Parameters.AddWithValue("@iLOCATION0", sessionData.iLocation[0]);
                        com.Parameters.AddWithValue("@iLOCATION1", sessionData.iLocation[1]);
                        com.Parameters.AddWithValue("@iLOCATION2", sessionData.iLocation[2]);
                        com.Parameters.AddWithValue("@iLOCATION3", sessionData.iLocation[3]);
                        com.Parameters.AddWithValue("@MNT_INSIDE", sessionData.iMntInside);
                        com.Parameters.AddWithValue("@coefficient@", sessionData.fCoefficient);
                        com.Parameters.AddWithValue("@UDATE", DateTime.UtcNow);
                        com.Parameters.AddWithValue("@BAT_STATUS", Math.Round((float)mythis.NodeProfile[nodeID - 1].bat_level / WirelessGFTViewer.LP_HUB.BATT_LEVEL_100PECENT * 100));

                        num = com.ExecuteNonQuery();

                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                System.Diagnostics.Debug.WriteLine(me.ToString());
            }
        }

        private void gDataInsertPerfSession(SESSION_DATA sessionData, string SessionTable, string dongle_mid, int nodeID)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();

                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    int num = 5;
                    using (SqlCeCommand com = new SqlCeCommand("SET IDENTITY_INSERT " + SessionTable + " ON", con))
                    {
                        com.ExecuteNonQuery();

                        string query = "INSERT INTO " + SessionTable + " (SID,UID,GID,SPORTS,NAME,SDATE,O_SDATE,ON_TIME,O_ON_TIME,OFF_TIME,O_OFF_TIME,DURATION,IMPACT,ALARM,WTH_RANGE,WTH,THRESHOLD,ExerciseType,ACTIVE, ERASED, MID,ROT_THRESHOLD,GFT_FIRMWARE,GFT_SOFTWARE,iLOCATION0,iLOCATION1,iLOCATION2,iLOCATION3,MNT_INSIDE,UDATE,BAT_STATUS, coefficient )" +
                            "VALUES(@SID,@UID,@GID,@SPORTS,@NAME,@SDATE, @O_SDATE,@ON_TIME,@ON_TIME,@OFF_TIME,@OFF_TIME,@DURATION,@IMPACT,@ALARM,@WTH_RANGE,@WTH,@THRESHOLD,@ExerciseType,@ACTIVE,@ERASED,@MID,@ROT_THRESHOLD,@GFT_FIRMWARE,@GFT_SOFTWARE,@iLOCATION0,@iLOCATION1,@iLOCATION2,@iLOCATION3,@MNT_INSIDE,@UDATE,@BAT_STATUS,@coefficient@ )";

                        com.CommandText = query;
                        com.Parameters.AddWithValue("@SID", sessionData.SID);
                        com.Parameters.AddWithValue("@UID", sessionData.UID);
                        com.Parameters.AddWithValue("@GID", sessionData.GID);
                        com.Parameters.AddWithValue("@SPORTS", sessionData.sports);
                        com.Parameters.AddWithValue("@NAME", sessionData.name);
                        com.Parameters.AddWithValue("@SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@O_SDATE", sessionData.Date);
                        com.Parameters.AddWithValue("@ON_TIME", sessionData.OnTime);
                        com.Parameters.AddWithValue("@OFF_TIME", sessionData.OffTime);
                        com.Parameters.AddWithValue("@DURATION", sessionData.Duration);
                        com.Parameters.AddWithValue("@IMPACT", sessionData.Impact);
                        com.Parameters.AddWithValue("@ALARM", sessionData.Alarm);
                        com.Parameters.AddWithValue("@WTH_RANGE", sessionData.WTHR);
                        com.Parameters.AddWithValue("@WTH", sessionData.WTH);
                        com.Parameters.AddWithValue("@THRESHOLD", sessionData.Threshold);
                        com.Parameters.AddWithValue("@ACTIVE", sessionData.activeTime);
                        com.Parameters.AddWithValue("@ExerciseType", "Game");
                        com.Parameters.AddWithValue("@ERASED", "N");
                        com.Parameters.AddWithValue("@MID", dongle_mid);
                        com.Parameters.AddWithValue("@ROT_THRESHOLD", "2000");
                        com.Parameters.AddWithValue("@GFT_FIRMWARE", sessionData.fwVersion);
                        com.Parameters.AddWithValue("@GFT_SOFTWARE", sessionData.swVersion);
                        com.Parameters.AddWithValue("@iLOCATION0", sessionData.iLocation[0]);
                        com.Parameters.AddWithValue("@iLOCATION1", sessionData.iLocation[1]);
                        com.Parameters.AddWithValue("@iLOCATION2", sessionData.iLocation[2]);
                        com.Parameters.AddWithValue("@iLOCATION3", sessionData.iLocation[3]);
                        com.Parameters.AddWithValue("@MNT_INSIDE", sessionData.iMntInside);
                        com.Parameters.AddWithValue("@UDATE", DateTime.UtcNow);
                        com.Parameters.AddWithValue("@BAT_STATUS", mythis.NodeProfile[nodeID - 1].bat_level);
                        com.Parameters.AddWithValue("@coefficient@", sessionData.fCoefficient);
                        num = com.ExecuteNonQuery();

                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                string dd = me.ToString();
                throw;
            }
        }
#endif
        static bool ValidateTime(out DateTime outTime, byte year, byte month, byte day, byte hour, byte minute, byte second, ushort milli)
        {
            string format = "yyyy:MM:dd:HH:mm:ss:fff";
            string myTime;
            if (year > DateTime.UtcNow.Year - 2000) year = (byte)(int)(DateTime.UtcNow.Year - 2000); // Year was corrupted.. Is this a firmware bug?
            myTime = string.Format("{0:d2}", 2000 + year) + ":";
            myTime += string.Format("{0:d2}", month) + ":";
            myTime += string.Format("{0:d2}", day) + ":";
            myTime += string.Format("{0:d2}", hour) + ":";
            myTime += string.Format("{0:d2}", minute) + ":";
            myTime += string.Format("{0:d2}", second) + ":";
            myTime += string.Format("{0:d3}", milli);

            return DateTime.TryParseExact(myTime, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out outTime);
        }
        const byte LOCATION_CAL_HIGH_G_CUNT = 1;
        const byte LOCATION_CAL_CNT_THRESHOLD = 9;

        public void performanceSummaryDataForWireless(byte[] sumBuff, int nodeID)
        {
            DateTime outTime;
            Form1.IMPACT_DATA iData = new Form1.IMPACT_DATA();// mythis.impactData;            // Event
            SESSION_DATA sData = new SESSION_DATA();// mythis.sessionData;           // Session
            sData.Impact = 0;
            sData.Alarm = 0;
            sData.WTH = 0;

            byte mode = 0;

            double[] azmuthElev = new double[2];
            //hold max point
            double[] myPoint = new double[3];
            double x, y, z, v;
            uint k = 0;//j = 0, , i=0;
            //byte c = 0;
            //double mLinMax = 0;
            //TimeSpan sessionLen;
            sData.Impact = 0;
            sData.WTH = 0;
            sData.Alarm = 0;
            ushort myShort = 0;
            sData.sports = 0;
            //double[] my40 = new double[40];
            ///bool bGettMyPoint = false;
            myPoint[0] = 0;
            myPoint[1] = 0;
            myPoint[2] = 0;
            //int maxIndex = 0;
            double[] calInput = new double[3];
            double[] calResult = new double[3];
            byte[] gRaw = new byte[3];
            mythis.NodeProfile[nodeID - 1].initQ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_INOUTMSK) > 0);
            try
            {
                //save to database
                //session
                if (ValidateTime(out outTime, sumBuff[1], sumBuff[2],
                                              sumBuff[3], sumBuff[4],
                                              sumBuff[5], sumBuff[6], myShort))
                    sData.OffTime = outTime;
                else
                {
                    return;
                }
                myShort = byteToShort(sumBuff[7], sumBuff[8]);
                if (myShort >= 1000)
                    myShort = 999;

                if (ValidateTime(out outTime, sumBuff[1], sumBuff[2], sumBuff[3],
                                              sumBuff[4], sumBuff[5], sumBuff[6],
                                              myShort))
                {
                    iData.time = outTime;
                }
                else
                {
                    // Device has incorrect timestamp
                    mythis.lpHub.lpDevices[nodeID - 1].timeSynced = 0;
                    mythis.NodeProfile[nodeID - 1].syncTimeFlag = true;
                    return;
                }
                iData.millisecond = myShort;


                calInput[0] = (short)gyroDecode(sumBuff[20], sumBuff[21]) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[1] = (short)gyroDecode(sumBuff[22], sumBuff[23]) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[2] = (short)gyroDecode(sumBuff[24], sumBuff[25]) * mythis.NodeProfile[nodeID - 1].mountCoefficient;

                calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                iData.rotX = Math.Round(calResult[0], 0);
                iData.rotY = Math.Round(calResult[1], 0);
                iData.rotZ = Math.Round(calResult[2], 0);

                iData.rotR = (short)Math.Sqrt(iData.rotX * iData.rotX + iData.rotY * iData.rotY + iData.rotZ * iData.rotZ);


                calInput[0] = lgDecodeDn(sumBuff[13], sumBuff[14], LOWG_SENSITIVITY_LSB_16G);
                calInput[1] = lgDecodeDn(sumBuff[15], sumBuff[16], LOWG_SENSITIVITY_LSB_16G);
                calInput[2] = lgDecodeDn(sumBuff[17], sumBuff[18], LOWG_SENSITIVITY_LSB_16G);
                /*calInput[0] = ((double)(Math.Sqrt(byteToShort((byte)sumBuff[13], (byte)sumBuff[14])) * ACC_SENSITIVITY_LSB2 * mythis.NodeProfile[nodeID - 1].mountCoefficient)) / 7.808;
                calInput[1] = ((double)(Math.Sqrt(byteToShort((byte)sumBuff[15], (byte)sumBuff[16])) * ACC_SENSITIVITY_LSB2 * mythis.NodeProfile[nodeID - 1].mountCoefficient)) / 7.808;
                calInput[2] = ((double)(Math.Sqrt(byteToShort((byte)sumBuff[17], (byte)sumBuff[18])) * ACC_SENSITIVITY_LSB2 * mythis.NodeProfile[nodeID - 1].mountCoefficient)) / 7.808;*/

                /*calInput[0] = AccDecode((byte)sumBuff[12], mode) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[1] = AccDecode((byte)sumBuff[13], mode) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[2] = AccDecode((byte)sumBuff[14], mode) * mythis.NodeProfile[nodeID - 1].mountCoefficient;*/

                calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                v = System.Math.Sqrt(calInput[0] * calInput[0] + calInput[1] * calInput[1] + calInput[2] * calInput[2]);
                        
                myPoint[0] = calResult[0];
                myPoint[1] = calResult[1];
                myPoint[2] = calResult[2];
                iData.linX = Math.Round(calResult[0], 2);
                iData.linY = Math.Round(calResult[1], 2);
                iData.linZ = Math.Round(calResult[2], 2);
                iData.linR = Math.Round(v, 2);
                iData.thresholdG = mythis.NodeProfile[nodeID - 1].alarmThres;
                iData.thresholdR = 2000;

                azmuthElev = getAzmuthAuto(myPoint);
                iData.azimuth = Math.Round(azmuthElev[0], 2);
                iData.elevation = Math.Round(azmuthElev[1], 2);
                mythis.Args[nodeID - 1].EventID += 1;
                iData.eid = mythis.Args[nodeID - 1].EventID;
                iData.UID = nodeID - 1;
                TimeSpan secondsSince2k = iData.time - DateTime.Parse("2000-01-01");
                iData.millisort = secondsSince2k.TotalSeconds * 1000 + iData.millisecond + ((double)iData.eid / 1000);


                k = (uint)mythis.Args[nodeID - 1].recvFIFO.Count;
                //for (int kk = 0; kk < k; kk++)
                //{
                //    if ((mythis.Args[nodeID - 1].recvFIFO[kk].time == iData.time) && (mythis.Args[nodeID - 1].recvFIFO[kk].millisecond == iData.millisecond))
                //    {
                //        mythis.Args[nodeID - 1].EventID -= 1;
                //        return;
                //    }
                //}
                mythis.Args[nodeID - 1].recvFIFO.Add(iData);
                if (iData.linR >= 0.9 * iData.thresholdG || iData.rotR >= 0.9 * iData.thresholdR) mythis.teamImpacts.Add(iData);

            }
            catch (Exception ex)
            {
                {
                }
            }
        }

        public void summaryDataForWireless(byte[] sumBuff, int nodeID)
        {
            DateTime outTime;
            Form1.IMPACT_DATA iData = new Form1.IMPACT_DATA();// mythis.impactData;            // Event
            SESSION_DATA sData = new SESSION_DATA();// mythis.sessionData;           // Session
            sData.Impact = 0;
            sData.Alarm = 0;
            sData.WTH = 0;

            byte mode = 0;

            double[] azmuthElev = new double[2];
            //hold max point
            double[] myPoint = new double[3];
            double x, y, z, v;
            uint k = 0;//j = 0, , i=0;
            //byte c = 0;
            //double mLinMax = 0;
            //TimeSpan sessionLen;
            sData.Impact = 0;
            sData.WTH = 0;
            sData.Alarm = 0;
            ushort myShort = 0;
            sData.sports = 0;
            //double[] my40 = new double[40];
            ///bool bGettMyPoint = false;
            myPoint[0] = 0;
            myPoint[1] = 0;
            myPoint[2] = 0;
            //int maxIndex = 0;
            double[] calInput = new double[3];
            double[] calResult = new double[3];
            byte[] gRaw = new byte[3];
            mythis.NodeProfile[nodeID - 1].initQ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_INOUTMSK) > 0);
            try
            {
                //save to database
                //session
                //int sessionStartTimeF = 1;
                //int sessionEndTimeF = 0;
                if (ValidateTime(out outTime, sumBuff[0], sumBuff[1],
                                              sumBuff[2], sumBuff[3],
                                              sumBuff[4], sumBuff[5], myShort))
                    sData.OffTime = outTime;
                else
                {
                    //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid Start date time.\r\n");
                    return;
                }
                myShort = byteToShort(sumBuff[6], sumBuff[7]);
                if (myShort >= 1000)
                    myShort = 999;

                if (ValidateTime(out outTime, sumBuff[0], sumBuff[1], sumBuff[2],
                                              sumBuff[3], sumBuff[4], sumBuff[5],
                                              myShort))
                {
                    iData.time = outTime;
                }
                else
                {
                    // Device has incorrect timestamp
                    mythis.lpHub.lpDevices[nodeID - 1].timeSynced = 0;
                    mythis.NodeProfile[nodeID - 1].syncTimeFlag = true;
                    //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid date time.\r\n");
                    return;
                }
                iData.millisecond = myShort;


                //iData.rotX = /*(short)*/(gyroDecode(sumBuff[16], sumBuff[17]));
                //iData.rotY = /*(short)*/(gyroDecode(sumBuff[18], sumBuff[19]));
                //iData.rotZ = /*(short)*/(gyroDecode(sumBuff[20], sumBuff[21]));
                calInput[0] = (short)gyroDecode(sumBuff[16], sumBuff[17]) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[1] = (short)gyroDecode(sumBuff[18], sumBuff[19]) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[2] = (short)gyroDecode(sumBuff[20], sumBuff[21]) * mythis.NodeProfile[nodeID - 1].mountCoefficient;

                calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                iData.rotX = Math.Round(calResult[0], 0);
                iData.rotY = Math.Round(calResult[1], 0);
                iData.rotZ = Math.Round(calResult[2], 0);

                //iData.rotR = /*(short)*/Math.Sqrt(iData.rotX * iData.rotX + iData.rotY * iData.rotY + iData.rotZ * iData.rotZ);
                //iData.rotR = Math.Round(iData.rotR, 1);
                iData.rotR = (short)Math.Sqrt(iData.rotX * iData.rotX + iData.rotY * iData.rotY + iData.rotZ * iData.rotZ);
                //maxIndex = (byte)recvBuff[12];


                calInput[0] = AccDecode((byte)sumBuff[12], mode) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[1] = AccDecode((byte)sumBuff[13], mode) * mythis.NodeProfile[nodeID - 1].mountCoefficient;
                calInput[2] = AccDecode((byte)sumBuff[14], mode) * mythis.NodeProfile[nodeID - 1].mountCoefficient;

                calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                v = System.Math.Sqrt(calInput[0] * calInput[0] + calInput[1] * calInput[1] + calInput[2] * calInput[2]);

                //iData.gsi = Math.Round(GSI(my40), 1);
                //iData.hic = Math.Round(HIC15(my40), 1);                             
                myPoint[0] = calResult[0];
                myPoint[1] = calResult[1];
                myPoint[2] = calResult[2];
                iData.linX = Math.Round(calResult[0], 2);
                iData.linY = Math.Round(calResult[1], 2);
                iData.linZ = Math.Round(calResult[2], 2);
                iData.linR = Math.Round(v, 2);
                iData.thresholdG = mythis.NodeProfile[nodeID - 1].alarmThres;
                iData.thresholdR = 2000;

                azmuthElev = getAzmuthAuto(myPoint);
                iData.azimuth = Math.Round(azmuthElev[0], 2);
                iData.elevation = Math.Round(azmuthElev[1], 2);
                /*
                if ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_INOUTMSK) > 0)
                {
                    iData.elevation = -Math.Round(azmuthElev[1], 2);
                }
                else
                {
                    iData.elevation = Math.Round(azmuthElev[1], 2);
                    iData.azimuth = 360 - iData.azimuth;
                }
                if ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_VER) != 0)
                {
                    iData.azimuth = iData.azimuth + 270;
                    iData.azimuth = iData.azimuth % 360;
                }
                if ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_USB_FRONT_REAR_MSK) > 0)
                {
                    iData.azimuth = (iData.azimuth + 180) % 360;
                }

                if ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_ZONE_LEFT) > 0)
                {
                    if ((iData.azimuth > 250))
                    {
                        iData.azimuth = iData.azimuth - 30;
                    }
                }
                else if ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_ZONE_CROWN) > 0)
                {
                    // Nothing to do?
                }*/
                mythis.Args[nodeID - 1].EventID += 1;
               iData.eid = mythis.Args[nodeID - 1].EventID;
               iData.UID = nodeID - 1;

               TimeSpan secondsSince2k = iData.time - DateTime.Parse("2000-01-01");
               iData.millisort = secondsSince2k.TotalSeconds * 1000 + iData.millisecond + ((double)iData.eid / 1000);

                k = (uint)mythis.Args[nodeID - 1].recvFIFO.Count;
                for (int kk = 0; kk < k; kk++)
                {
                    if ((mythis.Args[nodeID - 1].recvFIFO[kk].time == iData.time) && (mythis.Args[nodeID - 1].recvFIFO[kk].millisecond == iData.millisecond))
                    {
                        mythis.Args[nodeID - 1].EventID -= 1;
                        return;
                    }
                }
                mythis.Args[nodeID - 1].recvFIFO.Add(iData);
                if (iData.linR >= 0.9 * iData.thresholdG || iData.rotR >= 0.9 * iData.thresholdR) mythis.teamImpacts.Add(iData);

#if IMPACT_DATA_TABLE
                        Form1.IMPACT_DATA_TABLE iDataGrid = new Form1.IMPACT_DATA_TABLE();// mythis.impactData;            // Event
                        iDataGrid.sid       = iData.sid;
                        iDataGrid.eid       = iData.eid;
                        iDataGrid.time      = iData.time;
                        iDataGrid.rotX      = iData.rotX;
                        iDataGrid.rotY      = iData.rotY;
                        iDataGrid.rotZ      = iData.rotX;
                        iDataGrid.rotR      = iData.rotR;
                        iDataGrid.linX      = iData.linX;
                        iDataGrid.linY      = iData.linY;
                        iDataGrid.linZ      = iData.linZ;
                        iDataGrid.linR      = iData.linR;
                        //iDataGrid.hic       = iData.hic;
                        //iDataGrid.gsi       = iData.gsi;
                        iDataGrid.azimuth   = iData.azimuth;
                        iDataGrid.elevation = iData.elevation;
                        mythis.Args[nodeID - 1].recvFIFOTable.Add(iDataGrid);
#endif
            }
            catch (Exception ex)
            {
                //DisplayException(mythis.Name, ex);
                //MessageBox.Show("uploadData()");
                // throw ex;
                //m_Edit_RxPacket
                //if (mythis.m_Edit_RxPacket.Visible)
                {
                }
            }
        }

        public void uploadDataForWirelessStartSession(byte[] recvBuff, int nodeID)
        {
            bool duplicateSIDFlag = false;
            DateTime sessionEndTime;

            DateTime outTime;
            SESSION_DATA sData = new SESSION_DATA();// mythis.sessionData;           // Session

            sData.Impact = 0;
            sData.Alarm = 0;
            sData.WTH = 0;

            mythis.NodeProfile[nodeID - 1].initQ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_INOUTMSK) > 0);
            sData.iLocation = mythis.NodeProfile[nodeID - 1].iLocation;
            sData.iMntInside = mythis.NodeProfile[nodeID - 1].mntInside;
            sData.swVersion = Application.ProductVersion;
            sData.fwVersion = ""; // Need to store this

            // Run Post Processing on the last session
            //if (mythis.NodeProfile[nodeID - 1].recvSessionID > 0) postProcessSession(mythis.NodeProfile[nodeID - 1].SessionID, nodeID);
            mythis.NodeProfile[nodeID - 1].recvSessionID = recvBuff[11];
            mythis.NodeProfile[nodeID - 1].SessionID++;

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                SqlCeDataReader myReader = null;
                bool uniqueSID = false;
                SqlCeCommand myCommand;
                System.Diagnostics.Debug.WriteLine("Loop Starts...");
                while (!uniqueSID)
                {
                    System.Diagnostics.Debug.WriteLine("Loop..");
                    myReader = null;
                    try
                    {
                        if (con.State.ToString() != "Open") con.Open();
                        myCommand = new SqlCeCommand("SELECT COUNT(*) as cnt from SessionWireless where (SID = @SID@)", con);
                        myCommand.Parameters.AddWithValue("@SID@", mythis.NodeProfile[nodeID - 1].SessionID);
                        using (myReader = myCommand.ExecuteReader())
                        {
                            if (myReader.Read())
                            {
                                if (myReader.GetInt32(0) == 0) uniqueSID = true;
                                else
                                {
                                    mythis.NodeProfile[nodeID - 1].SessionID++;
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        string temp = e.Message;
                        mythis.NodeProfile[nodeID - 1].SessionID++;
                    }
                    if (uniqueSID)
                    {
                        uniqueSID = false;
                        // Also check the Events Table
                        myReader = null;
                        try
                        {
                            if (con.State.ToString() != "Open") con.Open();
                            myCommand = new SqlCeCommand("SELECT COUNT(*) as cnt from EventsWireless where (SID = @SID@)", con);
                            myCommand.Parameters.AddWithValue("@SID@", mythis.NodeProfile[nodeID - 1].SessionID);
                            using (myReader = myCommand.ExecuteReader())
                            {
                                if (myReader.Read())
                                {
                                    if (myReader.GetInt32(0) == 0) uniqueSID = true;
                                    else
                                    {
                                        mythis.NodeProfile[nodeID - 1].SessionID++;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            string temp = e.Message;
                            mythis.NodeProfile[nodeID - 1].SessionID++;
                        }
                    }

                    Random rnd = new Random();
                    if (uniqueSID) // our second check...
                    {
                        if(mythis.SessionIDs.Contains(mythis.NodeProfile[nodeID - 1].SessionID)) {
                            uniqueSID = false;
                            mythis.NodeProfile[nodeID - 1].SessionID++; // may have been used already
                        } else {
                            mythis.SessionIDs.Add(mythis.NodeProfile[nodeID - 1].SessionID);
                        }
                    }
                    if (uniqueSID)
                    { // We have verified this SID isn't taken in the database, now lets verify that another node isn't about to try to use it..
                        //Thread.Sleep(nodeID * 50); // Sleep this thread to avoid a possible race condition
                        for (int i = 0; i < mythis.NodeProfile.Count; i++)
                        {
                            if (nodeID - 1 != i && mythis.NodeProfile[nodeID - 1].SessionID == mythis.NodeProfile[i].SessionID)
                            {
                                mythis.NodeProfile[nodeID - 1].SessionID++;
                                if (rnd.Next(0, nodeID + 10) > nodeID + 10 / 1.25) mythis.NodeProfile[nodeID - 1].SessionID += nodeID; // Sometimes tick it up by the nodeID, to avoid a possible race condition
                                uniqueSID = false;
                                System.Diagnostics.Debug.WriteLine("Loop2...");
                            }
                        }
                    }
                }
            } 
            TimeSpan sessionLen;
            sData.Impact = 0;
            sData.WTH = 0;
            sData.Alarm = 0;
            ushort myShort = 0;
            sData.sports = 0;
            if (ValidateTime(out outTime, recvBuff[1], recvBuff[2],
                              recvBuff[3], recvBuff[4],
                              recvBuff[5], recvBuff[6], myShort))
            {
                if (sData.OnTime == DateTime.MinValue) sData.OnTime = outTime;
                sData.OffTime = outTime;
            }
            else
            {
                //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid Start date time.\r\n");
                return;
            }
            sData.OffTime = sData.OnTime;
            sData.Date = sData.OnTime;


            duplicateSIDFlag = getSessionWireless(sData, nodeID);
            if (duplicateSIDFlag)
            {
                // We are receiving this session again.. Delete the old one
                gDataWSessionRemoveIncomplete(sData, mythis.NodeProfile[nodeID - 1].GID);
            }
            sessionLen = (sData.OffTime - sData.OnTime);
            sData.Duration = (int)sessionLen.TotalMinutes;
            sData.UID = nodeID;
            sData.GID = mythis.NodeProfile[nodeID - 1].GID;       // Added by Jason Chen according to Jerad'requirement, 2014.06.16
            sData.Threshold = mythis.NodeProfile[nodeID - 1].alarmThres;
            sData.activeTime = 0;
            sData.SID = mythis.NodeProfile[nodeID - 1].SessionID;
            sData.name = mythis.NodeProfile[nodeID - 1].firstName;//.ToCharArray();
            sData.fCoefficient = mythis.NodeProfile[nodeID - 1].mountCoefficient;
            duplicateSIDFlag = getSessionWireless(nodeID);
            if (duplicateSIDFlag)//getSessionWireless(nodeID))
            {
                sData.Impact = mythis.NodeProfile[nodeID - 1].IMPACT;
                sData.WTH = mythis.NodeProfile[nodeID - 1].WTH;
                sData.Alarm = mythis.NodeProfile[nodeID - 1].ALARM;

                gDataWUpdateSession(sData, "SessionWireless", nodeID);
            }
            else
            {
                //if(sData
                string dongle_mid = string.Format("{0:x2}{1:x2}{2:x2}{3:x4}", mythis.lpHub.sopIdx, mythis.lpHub.chBase, mythis.lpHub.chHop, mythis.lpHub.hubSeed);  //2014.11.11
                gDataInsertSession(sData, "SessionWireless", dongle_mid, nodeID);     // Insert sData into Session table
            }
        }
        private void gDataWSessionRemoveIncomplete(SESSION_DATA sData, string gid)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    int num = 5;
                    con.Open();
                    string query = "DELETE FROM EventsWireless WHERE SID IN (SELECT SID FROM SessionWireless WHERE ERASED='N' AND O_SDATE = @SDATE AND GID = @GID)";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        com.Parameters.AddWithValue("@SDATE", sData.Date);
                        num = com.ExecuteNonQuery();
                    }
                    query = "DELETE FROM SessionWireless WHERE ERASED='N' AND O_SDATE = @SDATE AND GID = @GID";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        com.Parameters.AddWithValue("@SDATE", sData.Date);
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                Console.WriteLine(me.ToString());
            }
        }
        public void uploadDataForWirelessEndSession(byte[] recvBuff, int nodeID)
        {
            // Grab the Session
            SESSION_DATA sData = new SESSION_DATA();
            Thread.Sleep(1000);
            string query = "SELECT TOP 1 * FROM SessionWireless WHERE SID = @SID";
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand(query, con);
                    myCommand.Parameters.AddWithValue("@SID", mythis.NodeProfile[nodeID - 1].SessionID);
                    myReader = myCommand.ExecuteReader();
                    if (myReader.Read())
                    {
                        sData.UID = nodeID;
                        sData.SID = mythis.NodeProfile[nodeID - 1].SessionID;
                        sData.GID = myReader.GetString(2);
                        sData.sports = myReader.GetInt32(3);
                        sData.name = myReader.GetString(4);
                        sData.Date = myReader.GetDateTime(5);
                        sData.OnTime = myReader.GetDateTime(6);
                        sData.OffTime = myReader.GetDateTime(7);
                        sData.Duration = myReader.GetInt32(8);
                        sData.Impact = myReader.GetInt32(10);
                        sData.Alarm = myReader.GetInt32(11);
                        sData.WTH = myReader.GetInt32(13);
                        sData.Threshold = myReader.GetInt32(14);
                        sData.activeTime = myReader.GetInt32(17);
                        sData.fwVersion = myReader.GetString(21);
                        sData.swVersion = myReader.GetString(22);
                        sData.iLocation = new double[4];
                        sData.iLocation[0] = myReader.GetInt32(23);
                        sData.iLocation[1] = myReader.GetInt32(24);
                        sData.iLocation[2] = myReader.GetInt32(25);
                        sData.iLocation[3] = myReader.GetInt32(26);
                        sData.iMntInside = myReader.GetInt32(27) > 0 ? true : false;
                        //System.Diagnostics.Debug.WriteLine(myReader.GetString(33));
                        sData.fCoefficient = (float)myReader.GetDouble(33);
                        if (sData.Duration > 0) sData.closed = true;
                        else sData.closed = false;
                    }
                    else
                    {
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            if (!sData.closed) // Did we already process the session end packet?
            {
                DateTime outTime;
                DateTime outNewTime;
                DateTime oldTime = DateTime.MinValue;
                DateTime newTime = DateTime.MinValue;
                TimeSpan sessionLen;
                ushort myShort = 0;

                if (ValidateTime(out outTime, recvBuff[1], recvBuff[2],
                                  recvBuff[3], recvBuff[4],
                                  recvBuff[5], recvBuff[6], myShort))
                {
                    if (sData.OnTime == DateTime.MinValue) sData.OnTime = outTime;
                    sData.OffTime = outTime;
                    if (sData.OnTime == sData.OffTime)
                    {
                        System.Diagnostics.Debug.WriteLine("GFT Reported Off time identical to on time!");
                    }
                }
                else
                {
                    //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid End date time.\r\n");
                    return;
                }
                if (ValidateTime(out outTime, recvBuff[21], recvBuff[22],
                                  recvBuff[23], recvBuff[24],
                                  recvBuff[25], recvBuff[26], myShort))
                {
                    if (ValidateTime(out outNewTime, recvBuff[29], recvBuff[30],
                                      recvBuff[31], recvBuff[32],
                                      recvBuff[33], recvBuff[34], myShort))
                    {
                        oldTime = outTime;
                        newTime = outNewTime;
                    }
                }

                // Log the time correction data and raw packet for later analysis

                string sql = "INSERT INTO [TimeCorrection] ([SID],[UID],[DATE_FROM],[DATE_TO],[RAW_DATA]) VALUES(@SID@,@UID@,@DATE_FROM@,@DATE_TO@,@RAW_DATA@)";
                try
                {
                    // Open the connection using the connection string.
                    using (SqlCeConnection con = new SqlCeConnection(conString))
                    {
                        DateTime sql_oldTime = oldTime;
                        DateTime sql_newTime = newTime;
                        // SQL can't accept datetime.minvalue..
                        if (sql_oldTime == DateTime.MinValue) sql_oldTime = DateTime.Parse("1999-01-01");
                        if (sql_newTime == DateTime.MinValue) sql_newTime = DateTime.Parse("1999-01-01");
                        con.Open();
                        int num = 5;
                        using (SqlCeCommand com = new SqlCeCommand(sql, con))
                        {
                            com.Parameters.AddWithValue("@SID@", sData.SID);
                            com.Parameters.AddWithValue("@UID@", sData.UID);
                            com.Parameters.AddWithValue("@DATE_FROM@", sql_oldTime);
                            com.Parameters.AddWithValue("@DATE_TO@", sql_newTime);
                            com.Parameters.AddWithValue("@RAW_DATA@", recvBuff);
                            num = com.ExecuteNonQuery();
                        }
                        try
                        {
                            con.Close();
                        }
                        catch (Exception me)
                        {
                            Console.WriteLine(me.ToString());
                        }
                    }
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }

                //

                sessionLen = (sData.OffTime - sData.OnTime);
                sData.Duration = (int)sessionLen.TotalMinutes;

                sData.activeTime = (int)Math.Round((double)BitConverter.ToInt32(new byte[4] { recvBuff[18], recvBuff[17], recvBuff[16], recvBuff[15] }, 0) / 3000);

                sData.Impact = mythis.NodeProfile[nodeID - 1].IMPACT;
                sData.WTH = mythis.NodeProfile[nodeID - 1].WTH;
                sData.Alarm = mythis.NodeProfile[nodeID - 1].ALARM;
                sData = fixTimeStamps(nodeID, sData, oldTime, newTime);
                System.Diagnostics.Debug.WriteLine("Node " + nodeID + ", Session " + sData.SID + " has time correction from " + oldTime + " to " + newTime);
                gDataWUpdateSession(sData, "SessionWireless", nodeID);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Session End Packet Data Ignored");
            }
            if (mythis.NodeProfile[nodeID - 1].impactCount >= mythis.NodeProfile[nodeID - 1].impactNum && mythis.NodeProfile[nodeID - 1].sessionCount >= mythis.NodeProfile[nodeID - 1].sessionNum)
            {

                if (mythis.RxLog != null && mythis.RxLog.isCaptureType(nodeID, Convert.ToInt16(mythis.wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                {
                    var txtBuff = "          Session Marked Complete for Node " + nodeID;               // 2
                    mythis.RxLog.logText(txtBuff);
                }
                Form1.gDataWSessionMarkErased(mythis.NodeProfile[nodeID - 1].GID); // It is now safe to erase any 0 impact sessions less than 5 minutes
            }



            //mythis.NodeProfile[nodeID - 1].recvSessionID = 0; // This needs to be set back to 0
            //mythis.NodeProfile[nodeID - 1].impactNum = 0; // Set this back to 0
        }

        public void uploadDataForWirelessStartPerfSession(byte[] recvBuff, int nodeID)
        {
            bool duplicateSIDFlag = false;
            DateTime sessionEndTime;

            DateTime outTime;
            SESSION_DATA sData = new SESSION_DATA();// mythis.sessionData;           // Session

            sData.Impact = 0;
            sData.Alarm = 0;
            sData.WTH = 0;

            mythis.NodeProfile[nodeID - 1].initQ((mythis.NodeProfile[nodeID - 1].mntLoc & GFT_MNT_LOC_INOUTMSK) > 0);
            sData.iLocation = mythis.NodeProfile[nodeID - 1].iLocation;
            sData.iMntInside = mythis.NodeProfile[nodeID - 1].mntInside;
            sData.swVersion = Application.ProductVersion;
            sData.fwVersion = ""; // Need to store this

            mythis.NodeProfile[nodeID - 1].recvSessionID = recvBuff[11];
            mythis.NodeProfile[nodeID - 1].SessionID++;

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                SqlCeDataReader myReader = null;
                bool uniqueSID = false;
                SqlCeCommand myCommand;
                System.Diagnostics.Debug.WriteLine("Loop Starts...");
                while (!uniqueSID)
                {
                    System.Diagnostics.Debug.WriteLine("Loop..");
                    myReader = null;
                    try
                    {
                        if (con.State.ToString() != "Open") con.Open();
                        myCommand = new SqlCeCommand("SELECT COUNT(*) as cnt from PerfSessionWireless where (SID = @SID@)", con);
                        myCommand.Parameters.AddWithValue("@SID@", mythis.NodeProfile[nodeID - 1].SessionID);
                        using (myReader = myCommand.ExecuteReader())
                        {
                            if (myReader.Read())
                            {
                                if (myReader.GetInt32(0) == 0) uniqueSID = true;
                                else
                                {
                                    mythis.NodeProfile[nodeID - 1].SessionID++;
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        string temp = e.Message;
                        mythis.NodeProfile[nodeID - 1].SessionID++;
                    }
                    if (uniqueSID)
                    {
                        uniqueSID = false;
                        // Also check the Events Table
                        myReader = null;
                        try
                        {
                            if (con.State.ToString() != "Open") con.Open();
                            myCommand = new SqlCeCommand("SELECT COUNT(*) as cnt from PerfWireless where (SID = @SID@)", con);
                            myCommand.Parameters.AddWithValue("@SID@", mythis.NodeProfile[nodeID - 1].SessionID);
                            using (myReader = myCommand.ExecuteReader())
                            {
                                if (myReader.Read())
                                {
                                    if (myReader.GetInt32(0) == 0) uniqueSID = true;
                                    else
                                    {
                                        mythis.NodeProfile[nodeID - 1].SessionID++;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            string temp = e.Message;
                            mythis.NodeProfile[nodeID - 1].SessionID++;
                        }
                    }

                    Random rnd = new Random();
                    if (uniqueSID) // our second check...
                    {
                        if (mythis.SessionIDs.Contains(mythis.NodeProfile[nodeID - 1].SessionID))
                        {
                            uniqueSID = false;
                            mythis.NodeProfile[nodeID - 1].SessionID++; // may have been used already
                        }
                        else
                        {
                            mythis.SessionIDs.Add(mythis.NodeProfile[nodeID - 1].SessionID);
                        }
                    }
                    if (uniqueSID)
                    { // We have verified this SID isn't taken in the database, now lets verify that another node isn't about to try to use it..
                        for (int i = 0; i < mythis.NodeProfile.Count; i++)
                        {
                            if (nodeID - 1 != i && mythis.NodeProfile[nodeID - 1].SessionID == mythis.NodeProfile[i].SessionID)
                            {
                                mythis.NodeProfile[nodeID - 1].SessionID++;
                                if (rnd.Next(0, nodeID + 10) > nodeID + 10 / 1.25) mythis.NodeProfile[nodeID - 1].SessionID += nodeID; // Sometimes tick it up by the nodeID, to avoid a possible race condition
                                uniqueSID = false;
                                System.Diagnostics.Debug.WriteLine("Loop2...");
                            }
                        }
                    }
                }
            }
            TimeSpan sessionLen;
            sData.Impact = 0;
            sData.WTH = 0;
            sData.Alarm = 0;
            ushort myShort = 0;
            sData.sports = 0;
            if (ValidateTime(out outTime, recvBuff[1], recvBuff[2],
                              recvBuff[3], recvBuff[4],
                              recvBuff[5], recvBuff[6], myShort))
            {
                if (sData.OnTime == DateTime.MinValue) sData.OnTime = outTime;
                sData.OffTime = outTime;
            }
            else
            {
                //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid Start date time.\r\n");
                return;
            }
            sData.OffTime = sData.OnTime;
            sData.Date = sData.OnTime;


            duplicateSIDFlag = getPerfSessionWireless(sData, nodeID);
            if (duplicateSIDFlag)
            {
                // We are receiving this session again.. Delete the old one
                gDataWPerfSessionRemoveIncomplete(sData, mythis.NodeProfile[nodeID - 1].GID);
            }
            sessionLen = (sData.OffTime - sData.OnTime);
            sData.Duration = (int)sessionLen.TotalMinutes;
            sData.UID = nodeID;
            sData.GID = mythis.NodeProfile[nodeID - 1].GID;       // Added by Jason Chen according to Jerad'requirement, 2014.06.16
            sData.Threshold = mythis.NodeProfile[nodeID - 1].alarmThres;
            sData.activeTime = 0;
            sData.SID = mythis.NodeProfile[nodeID - 1].SessionID;
            sData.name = mythis.NodeProfile[nodeID - 1].firstName;//.ToCharArray();
            sData.fCoefficient = mythis.NodeProfile[nodeID - 1].mountCoefficient;
            duplicateSIDFlag = getSessionWireless(nodeID);
            if (duplicateSIDFlag)//getSessionWireless(nodeID))
            {
                sData.Impact = mythis.NodeProfile[nodeID - 1].IMPACT;
                sData.WTH = mythis.NodeProfile[nodeID - 1].WTH;
                sData.Alarm = mythis.NodeProfile[nodeID - 1].ALARM;

                gDataWUpdatePerfSession(sData, "PerfSessionWireless", nodeID);
            }
            else
            {
                //if(sData
                string dongle_mid = string.Format("{0:x2}{1:x2}{2:x2}{3:x4}", mythis.lpHub.sopIdx, mythis.lpHub.chBase, mythis.lpHub.chHop, mythis.lpHub.hubSeed);  //2014.11.11
                gDataInsertPerfSession(sData, "PerfSessionWireless", dongle_mid, nodeID);     // Insert sData into Session table
            }
        }
        private void gDataWPerfSessionRemoveIncomplete(SESSION_DATA sData, string gid)
        {
            try
            {
                // Open the connection using the connection string.
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    int num = 5;
                    con.Open();
                    string query = "DELETE FROM PerfWireless WHERE SID IN (SELECT SID FROM PerfSessionWireless WHERE ERASED='N' AND O_SDATE = @SDATE AND GID = @GID)";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        com.Parameters.AddWithValue("@SDATE", sData.Date);
                        num = com.ExecuteNonQuery();
                    }
                    query = "DELETE FROM PerfSessionWireless WHERE ERASED='N' AND O_SDATE = @SDATE AND GID = @GID";
                    using (SqlCeCommand com = new SqlCeCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@GID", gid);
                        com.Parameters.AddWithValue("@SDATE", sData.Date);
                        num = com.ExecuteNonQuery();
                    }
                    try
                    {
                        con.Close();
                    }
                    catch (Exception me)
                    {
                        Console.WriteLine(me.ToString());
                    }
                }
            }
            catch (Exception me)
            {
                Console.WriteLine(me.ToString());
            }
        }
        public void uploadDataForWirelessEndPerfSession(byte[] recvBuff, int nodeID)
        {
            // Grab the Session
            SESSION_DATA sData = new SESSION_DATA();
            Thread.Sleep(1000);
            string query = "SELECT TOP 1 * FROM PerfSessionWireless WHERE SID = @SID";
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                try
                {
                    con.Open();
                    SqlCeDataReader myReader = null;
                    SqlCeCommand myCommand = new SqlCeCommand(query, con);
                    myCommand.Parameters.AddWithValue("@SID", mythis.NodeProfile[nodeID - 1].SessionID);
                    myReader = myCommand.ExecuteReader();
                    if (myReader.Read())
                    {
                        sData.UID = nodeID;
                        sData.SID = mythis.NodeProfile[nodeID - 1].SessionID;
                        sData.GID = myReader.GetString(2);
                        sData.sports = myReader.GetInt32(3);
                        sData.name = myReader.GetString(4);
                        sData.Date = myReader.GetDateTime(5);
                        sData.OnTime = myReader.GetDateTime(6);
                        sData.OffTime = myReader.GetDateTime(7);
                        sData.Duration = myReader.GetInt32(8);
                        sData.Impact = myReader.GetInt32(10);
                        sData.Alarm = myReader.GetInt32(11);
                        sData.WTH = myReader.GetInt32(13);
                        sData.Threshold = myReader.GetInt32(14);
                        sData.activeTime = myReader.GetInt32(17);
                        sData.fwVersion = myReader.GetString(21);
                        sData.swVersion = myReader.GetString(22);
                        sData.iLocation = new double[4];
                        sData.iLocation[0] = myReader.GetInt32(23);
                        sData.iLocation[1] = myReader.GetInt32(24);
                        sData.iLocation[2] = myReader.GetInt32(25);
                        sData.iLocation[3] = myReader.GetInt32(26);
                        sData.iMntInside = myReader.GetInt32(27) > 0 ? true : false;
                        //System.Diagnostics.Debug.WriteLine(myReader.GetString(33));
                        sData.fCoefficient = (float)myReader.GetDouble(33);
                        if (sData.Duration > 0) sData.closed = true;
                        else sData.closed = false;
                    }
                    else
                    {
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            if (!sData.closed) // Did we already process the session end packet?
            {
                DateTime outTime;
                DateTime outNewTime;
                DateTime oldTime = DateTime.MinValue;
                DateTime newTime = DateTime.MinValue;
                TimeSpan sessionLen;
                ushort myShort = 0;

                if (ValidateTime(out outTime, recvBuff[1], recvBuff[2],
                                  recvBuff[3], recvBuff[4],
                                  recvBuff[5], recvBuff[6], myShort))
                {
                    if (sData.OnTime == DateTime.MinValue) sData.OnTime = outTime;
                    sData.OffTime = outTime;
                    if (sData.OnTime == sData.OffTime)
                    {
                        System.Diagnostics.Debug.WriteLine("GFT Reported Off time identical to on time!");
                    }
                }
                else
                {
                    //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid End date time.\r\n");
                    return;
                }
                if (ValidateTime(out outTime, recvBuff[21], recvBuff[22],
                                  recvBuff[23], recvBuff[24],
                                  recvBuff[25], recvBuff[26], myShort))
                {
                    if (ValidateTime(out outNewTime, recvBuff[29], recvBuff[30],
                                      recvBuff[31], recvBuff[32],
                                      recvBuff[33], recvBuff[34], myShort))
                    {
                        oldTime = outTime;
                        newTime = outNewTime;
                    }
                }

                // Log the time correction data and raw packet for later analysis

                string sql = "INSERT INTO [TimeCorrection] ([SID],[UID],[DATE_FROM],[DATE_TO],[RAW_DATA]) VALUES(@SID@,@UID@,@DATE_FROM@,@DATE_TO@,@RAW_DATA@)";
                try
                {
                    // Open the connection using the connection string.
                    using (SqlCeConnection con = new SqlCeConnection(conString))
                    {
                        DateTime sql_oldTime = oldTime;
                        DateTime sql_newTime = newTime;
                        // SQL can't accept datetime.minvalue..
                        if (sql_oldTime == DateTime.MinValue) sql_oldTime = DateTime.Parse("1999-01-01");
                        if (sql_newTime == DateTime.MinValue) sql_newTime = DateTime.Parse("1999-01-01");
                        con.Open();
                        int num = 5;
                        using (SqlCeCommand com = new SqlCeCommand(sql, con))
                        {
                            com.Parameters.AddWithValue("@SID@", sData.SID);
                            com.Parameters.AddWithValue("@UID@", sData.UID);
                            com.Parameters.AddWithValue("@DATE_FROM@", sql_oldTime);
                            com.Parameters.AddWithValue("@DATE_TO@", sql_newTime);
                            com.Parameters.AddWithValue("@RAW_DATA@", recvBuff);
                            num = com.ExecuteNonQuery();
                        }
                        try
                        {
                            con.Close();
                        }
                        catch (Exception me)
                        {
                            Console.WriteLine(me.ToString());
                        }
                    }
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }

                //

                sessionLen = (sData.OffTime - sData.OnTime);
                sData.Duration = (int)sessionLen.TotalMinutes;

                sData.activeTime = (int)Math.Round((double)BitConverter.ToInt32(new byte[4] { recvBuff[18], recvBuff[17], recvBuff[16], recvBuff[15] }, 0) / 3000);

                sData.Impact = mythis.NodeProfile[nodeID - 1].IMPACT;
                sData.WTH = mythis.NodeProfile[nodeID - 1].WTH;
                sData.Alarm = mythis.NodeProfile[nodeID - 1].ALARM;
                sData = fixTimeStamps(nodeID, sData, oldTime, newTime);
                System.Diagnostics.Debug.WriteLine("Node " + nodeID + ", Session " + sData.SID + " has time correction from " + oldTime + " to " + newTime);
                gDataWUpdatePerfSession(sData, "PerfSessionWireless", nodeID);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Session End Packet Data Ignored");
            }
            if (mythis.NodeProfile[nodeID - 1].impactCount >= mythis.NodeProfile[nodeID - 1].impactNum && mythis.NodeProfile[nodeID - 1].sessionCount >= mythis.NodeProfile[nodeID - 1].sessionNum)
            {

                if (mythis.RxLog != null && mythis.RxLog.isCaptureType(nodeID, Convert.ToInt16(mythis.wirelessViewer.dataGridView.SelectedRows[0].Cells[0].Value)))
                {
                    var txtBuff = "(Perf)Session Marked Complete for Node " + nodeID;            // 1
                    mythis.RxLog.logText(txtBuff);
                }
                Form1.gDataWPerfSessionMarkErased(mythis.NodeProfile[nodeID - 1].GID); // It is now safe to erase any 0 impact sessions less than 5 minutes
            }

        }
        public SESSION_DATA fixTimeStamps(int nodeID, SESSION_DATA sData, DateTime oldTime, DateTime newTime)
        {
            if (newTime > DateTime.Parse("2010-01-01"))
            {
                TimeSpan timeDiff = newTime - oldTime;
                if (oldTime > sData.OnTime)
                {
                    // Great, we have a time change during the session
                    sData.OnTime = sData.OnTime + timeDiff; // Update the power on time
                    sData.Date = sData.OnTime;
                    // Iterate over the impacts untill...
                    string sql = "";
                    if (oldTime < newTime)
                    {
                        // a) We reach oldTime (only works if adjustment is an increase) - This is our primary use case
                        sql = "UPDATE EventsWireless SET IMPACT_TIME = DATEADD(second, @timeDiff@, IMPACT_TIME) WHERE SID = @SID@ AND IMPACT_TIME < @OLDTIME@";
                    }
                    else if (oldTime > newTime)
                    {
                        // b) We reach a timestamp that is earlier than one already read - This is possible, but unlikely use case
                        // Correct action is to iterate over each impact until we reach one which is earlier.. But that is costly and time inefficient. Could disrupt an upload.. Might need to move this to the upload processing, since we are reading out the data anyways
                        // This query is efficient, but could potentially be incorrect, ie: if the clock was forward 1 hour and was corrected.. COuld cause issues if we hit the same time again... only do this if the time difference is greater than the session time
                        if (timeDiff.Minutes > sData.Duration)
                        {
                            sql = "UPDATE EventsWireless SET IMPACT_TIME = DATEADD(second, @timeDiff@, IMPACT_TIME) WHERE SID = @SID@ AND IMPACT_TIME < @OLDTIME@ AND  IMPACT_TIME > @NEWTIME@";
                        }
                    }
                    if (sql != "")
                    {
                        System.Diagnostics.Debug.WriteLine(sql);
                        System.Diagnostics.Debug.WriteLine(sData.SID);

                        System.Diagnostics.Debug.WriteLine(timeDiff.TotalSeconds);
                        System.Diagnostics.Debug.WriteLine(oldTime);
                        System.Diagnostics.Debug.WriteLine(newTime);
                        try
                        {
                            // Open the connection using the connection string.
                            using (SqlCeConnection con = new SqlCeConnection(conString))
                            {
                                con.Open();
                                int num = 5;
                                using (SqlCeCommand com = new SqlCeCommand(sql, con))
                                {
                                    com.Parameters.AddWithValue("@SID@", sData.SID);
                                    com.Parameters.AddWithValue("@timeDiff@", timeDiff.TotalSeconds);
                                    com.Parameters.AddWithValue("@OLDTIME@", oldTime);
                                    com.Parameters.AddWithValue("@NEWTIME@", newTime);
                                    num = com.ExecuteNonQuery();
                                }
                                try
                                {
                                    con.Close();
                                }
                                catch (Exception me)
                                {
                                    Console.WriteLine(me.ToString());
                                }
                            }
                        }
                        catch (Exception me)
                        {
                            Console.WriteLine(me.ToString());
                        }
                    }
                }
                

                // Correct timestamps on any sessions already downloaded who have an invalid date and have not yet been erased
                string sqlqry;
                
                
                try
                {
                    // Open the connection using the connection string.
                    using (SqlCeConnection con = new SqlCeConnection(conString))
                    {
                        con.Open();
                        int num = 5;
                        sqlqry = "UPDATE EventsWireless SET IMPACT_TIME = DATEADD(second, @timeDiff@, IMPACT_TIME) WHERE SID IN (SELECT SID FROM SessionWireless WHERE SID <> @SID@ AND IMPACT_TIME < @OLDTIME@ AND UID = @UID@ AND ERASED = 'N' AND DATEADD(second, @timeDiff@, ON_TIME) < @NEWTIME@ AND DATEADD(second, @timeDiff@, OFF_TIME) < @NEWTIME@ AND ON_TIME < '2010-01-01')";
                        using (SqlCeCommand com = new SqlCeCommand(sqlqry, con))
                        {
                            com.Parameters.AddWithValue("@SID@", sData.SID);
                            com.Parameters.AddWithValue("@UID@", sData.UID);
                            com.Parameters.AddWithValue("@timeDiff@", timeDiff.TotalSeconds);
                            com.Parameters.AddWithValue("@OLDTIME@", oldTime);
                            com.Parameters.AddWithValue("@NEWTIME@", newTime);
                            num = com.ExecuteNonQuery();
                        }
                        sqlqry = "UPDATE SessionWireless SET SDATE = DATEADD(second, @timeDiff@, SDATE), ON_TIME = DATEADD(second, @timeDiff@, ON_TIME), OFF_TIME = DATEADD(second, @timeDiff@, OFF_TIME) WHERE SID <> @SID@ AND SDATE < @OLDTIME@ AND UID = @UID@ AND ERASED = 'N' AND DATEADD(second, @timeDiff@, ON_TIME) < @NEWTIME@ AND DATEADD(second, @timeDiff@, OFF_TIME) < @NEWTIME@ AND ON_TIME < '2010-01-01'";
                        using (SqlCeCommand com = new SqlCeCommand(sqlqry, con))
                        {
                            com.Parameters.AddWithValue("@SID@", sData.SID);
                            com.Parameters.AddWithValue("@UID@", sData.UID);
                            com.Parameters.AddWithValue("@timeDiff@", timeDiff.TotalSeconds);
                            com.Parameters.AddWithValue("@OLDTIME@", oldTime);
                            com.Parameters.AddWithValue("@NEWTIME@", newTime);
                            num = com.ExecuteNonQuery();
                        }
                        try
                        {
                            con.Close();
                        }
                        catch (Exception me)
                        {
                            Console.WriteLine(me.ToString());
                        }
                    }
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return sData;
        }


        public SESSION_DATA fixTimeStampsPerf(int nodeID, SESSION_DATA sData, DateTime oldTime, DateTime newTime)
        {
            if (newTime > DateTime.Parse("2010-01-01"))
            {
                TimeSpan timeDiff = newTime - oldTime;
                if (oldTime > sData.OnTime)
                {
                    // Great, we have a time change during the session
                    sData.OnTime = sData.OnTime + timeDiff; // Update the power on time
                    sData.Date = sData.OnTime;
                    // Iterate over the impacts untill...
                    string sql = "";
                    if (oldTime < newTime)
                    {
                        // a) We reach oldTime (only works if adjustment is an increase) - This is our primary use case
                        sql = "UPDATE PerfWireless SET PERF_TIME = DATEADD(second, @timeDiff@, PERF_TIME) WHERE SID = @SID@ AND PERF_TIME < @OLDTIME@";
                    }
                    else if (oldTime > newTime)
                    {
                        // b) We reach a timestamp that is earlier than one already read - This is possible, but unlikely use case
                        // Correct action is to iterate over each impact until we reach one which is earlier.. But that is costly and time inefficient. Could disrupt an upload.. Might need to move this to the upload processing, since we are reading out the data anyways
                        // This query is efficient, but could potentially be incorrect, ie: if the clock was forward 1 hour and was corrected.. COuld cause issues if we hit the same time again... only do this if the time difference is greater than the session time
                        if (timeDiff.Minutes > sData.Duration)
                        {
                            sql = "UPDATE PerfWireless SET IMPACT_TIME = DATEADD(second, @timeDiff@, PERF_TIME) WHERE SID = @SID@ AND PERF_TIME < @OLDTIME@ AND  PERF_TIME > @NEWTIME@";
                        }
                    }
                    if (sql != "")
                    {
                        System.Diagnostics.Debug.WriteLine(sql);
                        System.Diagnostics.Debug.WriteLine(sData.SID);

                        System.Diagnostics.Debug.WriteLine(timeDiff.TotalSeconds);
                        System.Diagnostics.Debug.WriteLine(oldTime);
                        System.Diagnostics.Debug.WriteLine(newTime);
                        try
                        {
                            // Open the connection using the connection string.
                            using (SqlCeConnection con = new SqlCeConnection(conString))
                            {
                                con.Open();
                                int num = 5;
                                using (SqlCeCommand com = new SqlCeCommand(sql, con))
                                {
                                    com.Parameters.AddWithValue("@SID@", sData.SID);
                                    com.Parameters.AddWithValue("@timeDiff@", timeDiff.TotalSeconds);
                                    com.Parameters.AddWithValue("@OLDTIME@", oldTime);
                                    com.Parameters.AddWithValue("@NEWTIME@", newTime);
                                    num = com.ExecuteNonQuery();
                                }
                                try
                                {
                                    con.Close();
                                }
                                catch (Exception me)
                                {
                                    Console.WriteLine(me.ToString());
                                }
                            }
                        }
                        catch (Exception me)
                        {
                            Console.WriteLine(me.ToString());
                        }
                    }
                }


                // Correct timestamps on any sessions already downloaded who have an invalid date and have not yet been erased
                string sqlqry;


                try
                {
                    // Open the connection using the connection string.
                    using (SqlCeConnection con = new SqlCeConnection(conString))
                    {
                        con.Open();
                        int num = 5;
                        sqlqry = "UPDATE PerfWireless SET PERF_TIME = DATEADD(second, @timeDiff@, PERF_TIME) WHERE SID IN (SELECT SID FROM PerfSessionWireless WHERE SID <> @SID@ AND PERF_TIME < @OLDTIME@ AND UID = @UID@ AND ERASED = 'N' AND DATEADD(second, @timeDiff@, ON_TIME) < @NEWTIME@ AND DATEADD(second, @timeDiff@, OFF_TIME) < @NEWTIME@ AND ON_TIME < '2010-01-01')";
                        using (SqlCeCommand com = new SqlCeCommand(sqlqry, con))
                        {
                            com.Parameters.AddWithValue("@SID@", sData.SID);
                            com.Parameters.AddWithValue("@UID@", sData.UID);
                            com.Parameters.AddWithValue("@timeDiff@", timeDiff.TotalSeconds);
                            com.Parameters.AddWithValue("@OLDTIME@", oldTime);
                            com.Parameters.AddWithValue("@NEWTIME@", newTime);
                            num = com.ExecuteNonQuery();
                        }
                        sqlqry = "UPDATE PerfSessionWireless SET SDATE = DATEADD(second, @timeDiff@, SDATE), ON_TIME = DATEADD(second, @timeDiff@, ON_TIME), OFF_TIME = DATEADD(second, @timeDiff@, OFF_TIME) WHERE SID <> @SID@ AND SDATE < @OLDTIME@ AND UID = @UID@ AND ERASED = 'N' AND DATEADD(second, @timeDiff@, ON_TIME) < @NEWTIME@ AND DATEADD(second, @timeDiff@, OFF_TIME) < @NEWTIME@ AND ON_TIME < '2010-01-01'";
                        using (SqlCeCommand com = new SqlCeCommand(sqlqry, con))
                        {
                            com.Parameters.AddWithValue("@SID@", sData.SID);
                            com.Parameters.AddWithValue("@UID@", sData.UID);
                            com.Parameters.AddWithValue("@timeDiff@", timeDiff.TotalSeconds);
                            com.Parameters.AddWithValue("@OLDTIME@", oldTime);
                            com.Parameters.AddWithValue("@NEWTIME@", newTime);
                            num = com.ExecuteNonQuery();
                        }
                        try
                        {
                            con.Close();
                        }
                        catch (Exception me)
                        {
                            Console.WriteLine(me.ToString());
                        }
                    }
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
            return sData;
        }

        public void uploadPerformanceDataForWireless(byte[] recvBuff, int nodeID, bool lastImpact = false)
        {
            DateTime outTime;

            Form1.IMPACT_DATA iData = new Form1.IMPACT_DATA();// mythis.impactData;            // Event
            iData.buckets = new int[12];

            int locCnt = 0;
            byte mode = 0;
            double[] azmuthElev = new double[2];
            //hold max point
            double[] myPoint = new double[3];
            
            //int locCnt = 0;

            int maxIndex = 0;
            iData.dataX = new Byte[120];
            iData.dataY = new Byte[120];
            iData.dataZ = new Byte[120];
            iData.rdataX = new Byte[64];
            iData.rdataY = new Byte[64];
            iData.rdataZ = new Byte[64];


            iData.dataX_raw = new Byte[120];
            iData.dataY_raw = new Byte[120];
            iData.dataZ_raw = new Byte[120];
            iData.rdataX_raw = new Byte[64];
            iData.rdataY_raw = new Byte[64];
            iData.rdataZ_raw = new Byte[64];

            double x, y, z, v;
            //double xx, yy, zz;// vv;
            uint j = 0, k = 0, l = 0;//, i=0;
            byte c = 0;
            double mLinMax = 0;
            ushort myShort = 0;
            double[] my40 = new double[40];
            bool bGettMyPoint = false;
            myPoint[0] = 0;
            myPoint[1] = 0;
            myPoint[2] = 0;
            double[] calInput = new double[3];
            double[] calResult = new double[3];
            byte[] gRaw = new byte[3];
            float coefficient = mythis.NodeProfile[nodeID - 1].mountCoefficient;
            if (coefficient < 0.1) coefficient = 1;
            try
            {
                if (recvBuff[0] == 0x58)
                {
                    myShort = byteToShort(recvBuff[7], recvBuff[8]);
                    if (myShort >= 1000)
                        myShort = 999;

                    if (ValidateTime(out outTime, recvBuff[1], recvBuff[2], recvBuff[3],
                                                  recvBuff[4], recvBuff[5], recvBuff[6],
                                                  myShort))
                    {
                        iData.time = outTime;
                    }
                    else
                    {
                        return;
                    }
                    iData.millisecond = myShort;

                    bool duplicateFlag = false;
                    bool duplicateSIDFlag = false;

                    if (recvBuff[0] == 0x58)
                    {
                        duplicateFlag = getPerfWireless(iData, nodeID);

                        if (!duplicateFlag)
                        {

                            if (mythis.NodeProfile[nodeID - 1].recvSessionID != recvBuff[11])
                            {
                                // We should NEVER end up here again...

                            }
                            mLinMax = (double)(Math.Sqrt(byteToShort((byte)recvBuff[13], (byte)recvBuff[14])) * ACC_SENSITIVITY_LSB2) * coefficient;

                            iData.sid = mythis.NodeProfile[nodeID - 1].SessionID;
                        }
                        else
                        {
                            mythis.NodeProfile[nodeID - 1].recvSessionID = recvBuff[11];
                            return; // We already have this impact
                        }
                    }
                    calInput[0] = (short)gyroDecode(recvBuff[15], recvBuff[16]) * coefficient;
                    calInput[1] = (short)gyroDecode(recvBuff[17], recvBuff[18]) * coefficient;
                    calInput[2] = (short)gyroDecode(recvBuff[19], recvBuff[20]) * coefficient;

                    l = 21;
                    for (c = 0; c < 12; c++)
                        iData.buckets[c] = recvBuff[l++] * 256 + recvBuff[l++];

                    calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                    transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                    iData.rotX = Math.Round(calResult[0], 0);

                    //y

                    iData.rotY = Math.Round(calResult[1], 0);
                    //z

                    iData.rotZ = Math.Round(calResult[2], 0);

                    iData.rotR = (short)Math.Sqrt(iData.rotX * iData.rotX + iData.rotY * iData.rotY + iData.rotZ * iData.rotZ);

                    mythis.NodeProfile[nodeID - 1].IMPACT++;
                    if (mLinMax >= mythis.NodeProfile[nodeID - 1].alarmThres || iData.rotR >= 2000)
                        mythis.NodeProfile[nodeID - 1].ALARM++; //sData.Alarm++;
                    if (mLinMax >= (mythis.NodeProfile[nodeID - 1].alarmThres) * 0.9 || iData.rotR >= (2000 * 0.9))
                        mythis.NodeProfile[nodeID - 1].WTH++; //sData.WTH++;

                    maxIndex = (byte)recvBuff[12];

                    k = 0;
                    //mLinMax = 0;
                    for (j = 1; j < 5; j++)//if (j < 7)    // 1 ~ 5 for Acclerometer data points, 5 * 12 = 60 points
                    {
                        for (c = 0; c < 5; c++)
                        {

                            iData.dataX_raw[c * 2 + (j - 1) * 10] = (byte)recvBuff[l++];
                            iData.dataX_raw[c * 2 + 1 + (j - 1) * 10] = (byte)recvBuff[l++];
                            iData.dataY_raw[c * 2 + (j - 1) * 10] = (byte)recvBuff[l++];
                            iData.dataY_raw[c * 2 + 1 + (j - 1) * 10] = (byte)recvBuff[l++];
                            iData.dataZ_raw[c * 2 + (j - 1) * 10] = (byte)recvBuff[l++];
                            iData.dataZ_raw[c * 2 + 1 + (j - 1) * 10] = (byte)recvBuff[l++];
                            //System.Diagnostics.Debug.WriteLine("Lin Z Point " + ((c * 2 + (j - 1) * 10)/2).ToString() + ": " + recvBuff[l - 2].ToString() + ", " + recvBuff[l - 1].ToString() + ", Offset: " + (l - 1).ToString());
                            l -= 6; // Reset l..
                            //calInput[0] = AccDecode((byte)recvBuff[c * 3 + 21 + (j - 1) * 60], mode) * coefficient;
                            //calInput[1] = AccDecode((byte)recvBuff[c * 3 + 22 + (j - 1) * 60], mode) * coefficient;
                            //calInput[2] = AccDecode((byte)recvBuff[c * 3 + 23 + (j - 1) * 60], mode) * coefficient;

                            // Accelerometer Data
                            calInput[0] = lgDecodeDn(recvBuff[l++], recvBuff[l++], LOWG_SENSITIVITY_LSB_16G);
                            calInput[1] = lgDecodeDn(recvBuff[l++], recvBuff[l++], LOWG_SENSITIVITY_LSB_16G);
                            calInput[2] = lgDecodeDn(recvBuff[l++], recvBuff[l++], LOWG_SENSITIVITY_LSB_16G);
                            System.Diagnostics.Debug.WriteLine("Lin Point " + ((c * 2 + (j - 1) * 10) / 2).ToString() + ": " + ((float)Math.Sqrt(calInput[0] * calInput[0] + calInput[1] * calInput[1] + calInput[2] * calInput[2])).ToString());
                            calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                            transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                            lgEncode(ref gRaw, calResult[0]);
                            iData.dataX[c * 2 + (j - 1) * 10] = gRaw[0];
                            iData.dataX[c * 2 + 1 + (j - 1) * 10] = gRaw[1];

                            lgEncode(ref gRaw, calResult[1]);
                            iData.dataY[c * 2 + (j - 1) * 10] = gRaw[0];
                            iData.dataY[c * 2 + 1 + (j - 1) * 10] = gRaw[1];

                            lgEncode(ref gRaw, calResult[2]);
                            iData.dataZ[c * 2 + (j - 1) * 10] = gRaw[0];
                            iData.dataZ[c * 2 + 1 + (j - 1) * 10] = gRaw[1];

                            // Gyro Data

                            //x
                            iData.rdataX_raw[c * 2 + (j - 1) * 10] = recvBuff[l++];
                            iData.rdataX_raw[c * 2 + 1 + (j - 1) * 10] = recvBuff[l++];
                            //y
                            iData.rdataY_raw[c * 2 + (j - 1) * 10] = recvBuff[l++];
                            iData.rdataY_raw[c * 2 + 1 + (j - 1) * 10] = recvBuff[l++];
                            //z
                            iData.rdataZ_raw[c * 2 + (j - 1) * 10] = recvBuff[l++];
                            iData.rdataZ_raw[c * 2 + 1 + (j - 1) * 10] = recvBuff[l++];
                            l -= 6; // Reset l..

                            calInput[0] = gyroDecode(recvBuff[l++], recvBuff[l++]);
                            calInput[1] = gyroDecode(recvBuff[l++], recvBuff[l++]);
                            calInput[2] = gyroDecode(recvBuff[l++], recvBuff[l++]);

                            calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                            transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);


                            gyroEncode(ref gRaw, calResult[0]);
                            iData.rdataX[c * 2 + (j - 1) * 10] = gRaw[0];
                            iData.rdataX[c * 2 + 1 + (j - 1) * 10] = gRaw[1];

                            gyroEncode(ref gRaw, calResult[1]);
                            iData.rdataY[c * 2 + (j - 1) * 10] = gRaw[0];
                            iData.rdataY[c * 2 + 1 + (j - 1) * 10] = gRaw[1];

                            gyroEncode(ref gRaw, calResult[2]);
                            iData.rdataZ[c * 2 + (j - 1) * 10] = gRaw[0];
                            iData.rdataZ[c * 2 + 1 + (j - 1) * 10] = gRaw[1];

                            /*v = System.Math.Sqrt(calInput[0] * calInput[0] + calInput[1] * calInput[1] + calInput[2] * calInput[2]);

                            if (bGettMyPoint == false && v > mythis.NodeProfile[nodeID - 1].recThres)
                            {
                                if (mythis.NodeProfile[nodeID - 1].recThres < LOCATION_CAL_CNT_THRESHOLD)
                                {
                                    myPoint[0] = calResult[0];
                                    myPoint[1] = calResult[1];
                                    myPoint[2] = calResult[2];
                                    bGettMyPoint = true;
                                }
                                else
                                {
                                    if (++locCnt <= 3)
                                    {
                                        myPoint[0] += calResult[0];
                                        myPoint[1] += calResult[1];
                                        myPoint[2] += calResult[2];
                                    }

                                    if (locCnt >= 4)
                                    {
                                        myPoint[0] /= 3;
                                        myPoint[1] /= 3;
                                        myPoint[2] /= 3;
                                        bGettMyPoint = true;

                                    }
                                }

                            }
                            if (k == maxIndex)
                            {
                                iData.linX = Math.Round(calResult[0], 2);
                                iData.linY = Math.Round(calResult[1], 2);
                                iData.linZ = Math.Round(calResult[2], 2);
                                iData.linR = Math.Round(v, 2);
                                iData.thresholdG = mythis.NodeProfile[nodeID - 1].alarmThres;
                                iData.thresholdR = 2000;
                            }
                            iData.dataX[c + (j - 1) * 20] = AccEncode(calResult[0], mode);
                            iData.dataY[c + (j - 1) * 20] = AccEncode(calResult[1], mode);
                            iData.dataZ[c + (j - 1) * 20] = AccEncode(calResult[2], mode);

                            if (k % 3 == 0)
                            {
                                my40[k / 3] = v;
                            }
                            k++;*/
                        }
                    }
                    /*
                    for (j = 7; j <= 10; j++)  //else    // 7 ~ 10 for gyroscope data points, 4 * 8 = 32 points, 6 bytes each point
                    {
                        for (c = 0; c < 8; c++)
                        {
                            //x
                            iData.rdataX_raw[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 381 + (j - 7) * 48];
                            iData.rdataX_raw[c * 2 + 1 + (j - 7) * 16] = recvBuff[c * 6 + 382 + (j - 7) * 48];
                            //y
                            iData.rdataY_raw[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 383 + (j - 7) * 48];
                            iData.rdataY_raw[c * 2 + 1 + (j - 7) * 16] = recvBuff[c * 6 + 384 + (j - 7) * 48];
                            //z
                            iData.rdataZ_raw[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 385 + (j - 7) * 48];
                            iData.rdataZ_raw[c * 2 + 1 + (j - 7) * 16] = recvBuff[c * 6 + 386 + (j - 7) * 48];

                            calInput[0] = gyroDecode(recvBuff[c * 6 + 381 + (j - 7) * 48], recvBuff[c * 6 + 382 + (j - 7) * 48]) * coefficient;
                            calInput[1] = gyroDecode(recvBuff[c * 6 + 383 + (j - 7) * 48], recvBuff[c * 6 + 384 + (j - 7) * 48]) * coefficient;
                            calInput[2] = gyroDecode(recvBuff[c * 6 + 385 + (j - 7) * 48], recvBuff[c * 6 + 386 + (j - 7) * 48]) * coefficient;

                            calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                            transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                            if (Math.Abs(calResult[0]) > iData.iMaxRotX) iData.iMaxRotX = calResult[0];
                            if (Math.Abs(calResult[1]) > iData.iMaxRotY) iData.iMaxRotY = calResult[1];
                            if (Math.Abs(calResult[2]) > iData.iMaxRotZ) iData.iMaxRotZ = calResult[2];

                            gyroEncode(ref gRaw, calResult[0]);
                            iData.rdataX[c * 2 + (j - 7) * 16] = gRaw[0];
                            iData.rdataX[c * 2 + 1 + (j - 7) * 16] = gRaw[1];
                            //y
                            gyroEncode(ref gRaw, calResult[1]);
                            iData.rdataY[c * 2 + (j - 7) * 16] = gRaw[0];
                            iData.rdataY[c * 2 + 1 + (j - 7) * 16] = gRaw[1];
                            //z
                            gyroEncode(ref gRaw, calResult[2]);
                            iData.rdataZ[c * 2 + (j - 7) * 16] = gRaw[0];
                            iData.rdataZ[c * 2 + 1 + (j - 7) * 16] = gRaw[1];
                        }
                    }

                    iData.gsi = Math.Round(GSI(my40), 1);
                    iData.hic = Math.Round(HIC15(my40), 1);
                    iData.iBrIC = Math.Round(BrIC(new double[3] { iData.iMaxRotX, iData.iMaxRotY, iData.iMaxRotZ }), 2);
                    
                    azmuthElev = getAzmuthAuto(myPoint);
                    iData.azimuth = Math.Round(azmuthElev[0], 2);
                    iData.elevation = Math.Round(azmuthElev[1], 2);
                    */

                    if (recvBuff[0] == 0x58)
                    {

                        if (!duplicateFlag)
                        {
                            mythis.NodeProfile[nodeID - 1].EventID = mythis.getUniqueEventID(); //+= 1;
                            iData.eid = mythis.NodeProfile[nodeID - 1].EventID;

                            iData.UID = nodeID;

                            gDataInsertPerf(iData, "PerfWireless");     // Insert iData into Event table 
                        }
                        //Temperature display Disable, so don't need to update the view
                    }
                }
            }
            catch (Exception ex)
            {
                {
                }
            }
        }

        public void uploadDataForWireless(byte[] recvBuff, int nodeID, bool lastImpact = false)
        {
            DateTime outTime;

            Form1.IMPACT_DATA iData = new Form1.IMPACT_DATA();// mythis.impactData;            // Event

            int locCnt = 0;
            byte mode = 0;
            double[] azmuthElev = new double[2];
            //hold max point
            double[] myPoint = new double[3];
 
            
 
            //int locCnt = 0;
 
            int maxIndex = 0;
            iData.dataX = new Byte[120];
            iData.dataY = new Byte[120];
            iData.dataZ = new Byte[120];
            iData.rdataX = new Byte[64];
            iData.rdataY = new Byte[64];
            iData.rdataZ = new Byte[64];


            iData.dataX_raw = new Byte[120];
            iData.dataY_raw = new Byte[120];
            iData.dataZ_raw = new Byte[120];
            iData.rdataX_raw = new Byte[64];
            iData.rdataY_raw = new Byte[64];
            iData.rdataZ_raw = new Byte[64];

            double x, y, z, v;
            //double xx, yy, zz;// vv;
            uint j = 0, k = 0;//, i=0;
            byte c = 0;
            double mLinMax = 0;
            ushort myShort = 0;
            double[] my40 = new double[40];
            bool bGettMyPoint = false;
            myPoint[0] = 0;
            myPoint[1] = 0;
            myPoint[2] = 0;
            double[] calInput = new double[3];
            double[] calResult = new double[3];
            byte[] gRaw = new byte[3];
            float coefficient = mythis.NodeProfile[nodeID - 1].mountCoefficient;
            if (coefficient < 0.1) coefficient = 1;
            try
            {
                //MyMarshalToForm("toolStripStatusLabelGFT", "Downloading Data by Wireless", System.Drawing.Color.Red);
                if ((recvBuff[0] == 0x48) || (recvBuff[0]==0x58))//for (j = 0; j < 11; j++)
                {
                    myShort = byteToShort(recvBuff[7], recvBuff[8]);
                    if (myShort >= 1000)
                        myShort = 999;

                    if (ValidateTime(out outTime, recvBuff[1], recvBuff[2], recvBuff[3],
                                                  recvBuff[4], recvBuff[5], recvBuff[6],
                                                  myShort))
                    {
                        iData.time = outTime;
                    }
                    else
                    {
                        //mythis.wirelessViewer.DebugDataBoxDisplay("Upload data has invalid date time.\r\n");
                        return;
                    }
                    iData.millisecond = myShort;

                    bool duplicateFlag = false;
                    bool duplicateSIDFlag = false;

                    if (recvBuff[0] == 0x58)
                    {
                        duplicateFlag    = getEventWireless(iData, nodeID);
                        
                        if (!duplicateFlag)
                        {

                            if (mythis.NodeProfile[nodeID - 1].recvSessionID != recvBuff[11])
                            {
                                // We should NEVER end up here again...
                                
                            }
                            mLinMax = (double)(Math.Sqrt(byteToShort((byte)recvBuff[13], (byte)recvBuff[14])) * ACC_SENSITIVITY_LSB2) * coefficient;
 
                            iData.sid = mythis.NodeProfile[nodeID - 1].SessionID;
                        }
                        else
                        {
                            mythis.NodeProfile[nodeID - 1].recvSessionID = recvBuff[11];
                            return; // We already have this impact
                        }
                    }
                    calInput[0] = (short)gyroDecode(recvBuff[15], recvBuff[16]) * coefficient;
                    calInput[1] = (short)gyroDecode(recvBuff[17], recvBuff[18]) * coefficient;
                    calInput[2] = (short)gyroDecode(recvBuff[19], recvBuff[20]) * coefficient;

                    calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                    transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                    iData.rotX = Math.Round(calResult[0], 0);

                    //y

                    iData.rotY = Math.Round(calResult[1], 0);
                    //z

                    iData.rotZ = Math.Round(calResult[2], 0);

                    iData.rotR = (short)Math.Sqrt(iData.rotX * iData.rotX + iData.rotY * iData.rotY + iData.rotZ * iData.rotZ);

                    mythis.NodeProfile[nodeID - 1].IMPACT++;
                    if (mLinMax >= mythis.NodeProfile[nodeID - 1].alarmThres || iData.rotR >= 2000)
                        mythis.NodeProfile[nodeID - 1].ALARM++; //sData.Alarm++;
                    if (mLinMax >= (mythis.NodeProfile[nodeID - 1].alarmThres) * 0.9 || iData.rotR >= (2000 * 0.9))
                        mythis.NodeProfile[nodeID - 1].WTH++; //sData.WTH++;
                    
                    maxIndex = (byte)recvBuff[12];

                    k = 0;
                    //mLinMax = 0;
                    for (j = 1; j <= 6; j++)//if (j < 7)    // 1 ~ 6 for Acclerometer data points, 6 * 20 = 120 points
                    {
                        for (c = 0; c < 20; c++)
                        {

                            iData.dataX_raw[c + (j - 1) * 20] = (byte)recvBuff[c * 3 + 21 + (j - 1) * 60];
                            iData.dataY_raw[c + (j - 1) * 20] = (byte)recvBuff[c * 3 + 22 + (j - 1) * 60];
                            iData.dataZ_raw[c + (j - 1) * 20] = (byte)recvBuff[c * 3 + 23 + (j - 1) * 60];

                            calInput[0] = AccDecode((byte)recvBuff[c * 3 + 21 + (j - 1) * 60], mode) * coefficient;
                            calInput[1] = AccDecode((byte)recvBuff[c * 3 + 22 + (j - 1) * 60], mode) * coefficient;
                            calInput[2] = AccDecode((byte)recvBuff[c * 3 + 23 + (j - 1) * 60], mode) * coefficient;

                            calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                            transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);
                            v = System.Math.Sqrt(calInput[0] * calInput[0] + calInput[1] * calInput[1] + calInput[2] * calInput[2]);

                            if (bGettMyPoint == false && v > mythis.NodeProfile[nodeID - 1].recThres)
                            {
                                if (mythis.NodeProfile[nodeID - 1].recThres < LOCATION_CAL_CNT_THRESHOLD)
                                {
                                    myPoint[0] = calResult[0];
                                    myPoint[1] = calResult[1];
                                    myPoint[2] = calResult[2];
                                    bGettMyPoint = true;
                                }
                                else
                                {
                                    if (++locCnt <= 3)
                                    {
                                        myPoint[0] += calResult[0];
                                        myPoint[1] += calResult[1];
                                        myPoint[2] += calResult[2];
                                    }

                                    if (locCnt >= 4)
                                    {
                                        myPoint[0] /= 3;
                                        myPoint[1] /= 3;
                                        myPoint[2] /= 3;
                                        bGettMyPoint = true;

                                    }
                                }

                            }                   
                            if (k == maxIndex)
                            {
                                iData.linX = Math.Round(calResult[0], 2);
                                iData.linY = Math.Round(calResult[1], 2);
                                iData.linZ = Math.Round(calResult[2], 2);
                                iData.linR = Math.Round(v, 2);
                                iData.thresholdG = mythis.NodeProfile[nodeID - 1].alarmThres;
                                iData.thresholdR = 2000;
                            }
                            iData.dataX[c + (j - 1) * 20] = AccEncode(calResult[0], mode);
                            iData.dataY[c + (j - 1) * 20] = AccEncode(calResult[1], mode);
                            iData.dataZ[c + (j - 1) * 20] = AccEncode(calResult[2], mode);

                            if (k % 3 == 0)
                            {
                                my40[k / 3] = v;
                            }
                            k++;
                        }
                    }

                    for (j = 7; j <= 10; j++)  //else    // 7 ~ 10 for gyroscope data points, 4 * 8 = 32 points, 6 bytes each point
                    {
                        for (c = 0; c < 8; c++)
                        {
                            //x
                            iData.rdataX_raw[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 381 + (j - 7) * 48];
                            iData.rdataX_raw[c * 2 + 1 + (j - 7) * 16] = recvBuff[c * 6 + 382 + (j - 7) * 48];
                            //y
                            iData.rdataY_raw[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 383 + (j - 7) * 48];
                            iData.rdataY_raw[c * 2 + 1 + (j - 7) * 16] = recvBuff[c * 6 + 384 + (j - 7) * 48];
                            //z
                            iData.rdataZ_raw[c * 2 + (j - 7) * 16] = recvBuff[c * 6 + 385 + (j - 7) * 48];
                            iData.rdataZ_raw[c * 2 + 1 + (j - 7) * 16] = recvBuff[c * 6 + 386 + (j - 7) * 48];

                            calInput[0] = gyroDecode(recvBuff[c * 6 + 381 + (j - 7) * 48], recvBuff[c * 6 + 382 + (j - 7) * 48]) * coefficient;
                            calInput[1] = gyroDecode(recvBuff[c * 6 + 383 + (j - 7) * 48], recvBuff[c * 6 + 384 + (j - 7) * 48]) * coefficient;
                            calInput[2] = gyroDecode(recvBuff[c * 6 + 385 + (j - 7) * 48], recvBuff[c * 6 + 386 + (j - 7) * 48]) * coefficient;

                            calResult = mCalibrate(ref calInput, ref mythis.NodeProfile[nodeID - 1].qua);
                            transLoc(ref calResult, mythis.NodeProfile[nodeID - 1].mntLoc);

                            if (Math.Abs(calResult[0]) > iData.iMaxRotX) iData.iMaxRotX = calResult[0];
                            if (Math.Abs(calResult[1]) > iData.iMaxRotY) iData.iMaxRotY = calResult[1];
                            if (Math.Abs(calResult[2]) > iData.iMaxRotZ) iData.iMaxRotZ = calResult[2];

                            gyroEncode(ref gRaw, calResult[0]);
                            iData.rdataX[c * 2 + (j - 7) * 16] = gRaw[0];
                            iData.rdataX[c * 2 + 1 + (j - 7) * 16] = gRaw[1];
                            //y
                            gyroEncode(ref gRaw, calResult[1]);
                            iData.rdataY[c * 2 + (j - 7) * 16] = gRaw[0];
                            iData.rdataY[c * 2 + 1 + (j - 7) * 16] = gRaw[1];
                            //z
                            gyroEncode(ref gRaw, calResult[2]);
                            iData.rdataZ[c * 2 + (j - 7) * 16] = gRaw[0];
                            iData.rdataZ[c * 2 + 1 + (j - 7) * 16] = gRaw[1];
                        }
                    }

                    iData.gsi = Math.Round(GSI(my40), 1);
                    iData.hic = Math.Round(HIC15(my40), 1);
                    iData.iBrIC = Math.Round(BrIC(new double[3]{iData.iMaxRotX, iData.iMaxRotY, iData.iMaxRotZ}), 2);

                    azmuthElev = getAzmuthAuto(myPoint);
                    iData.azimuth = Math.Round(azmuthElev[0], 2);
                    iData.elevation = Math.Round(azmuthElev[1], 2);


                    //save impact to database
//#if USE_OF_LIST
                    if(recvBuff[0] == 0x48)
                    {
                        mythis.Args[nodeID-1].EventID += 1;
                        iData.eid = mythis.Args[nodeID - 1].EventID;

                        k = (uint)mythis.Args[nodeID - 1].recvFIFO.Count;
                        for (int kk = 0;  kk< k; kk++)
                        {
                            if ((mythis.Args[nodeID - 1].recvFIFO[kk].time == iData.time) && (mythis.Args[nodeID - 1].recvFIFO[kk].millisecond == iData.millisecond))
                            {
                                mythis.Args[nodeID - 1].EventID -= 1;
                                return;
                            }
                        }
                        mythis.Args[nodeID - 1].recvFIFO.Add(iData);

                      #if IMPACT_DATA_TABLE
                        Form1.IMPACT_DATA_TABLE iDataGrid = new Form1.IMPACT_DATA_TABLE();// mythis.impactData;            // Event
                        iDataGrid.sid       = iData.sid;
                        iDataGrid.eid       = iData.eid;
                        iDataGrid.time      = iData.time;
                        iDataGrid.rotX      = iData.rotX;
                        iDataGrid.rotY      = iData.rotY;
                        iDataGrid.rotZ      = iData.rotX;
                        iDataGrid.rotR      = iData.rotR;
                        iDataGrid.linX      = iData.linX;
                        iDataGrid.linY      = iData.linY;
                        iDataGrid.linZ      = iData.linZ;
                        iDataGrid.linR      = iData.linR;
                        iDataGrid.hic       = iData.hic;
                        iDataGrid.gsi       = iData.gsi;
                        iDataGrid.azimuth   = iData.azimuth;
                        iDataGrid.elevation = iData.elevation;
                        mythis.Args[nodeID - 1].recvFIFOTable.Add(iDataGrid);
                      #endif
                    } // if (recvBuff[0] == 0x58)
//#else
                    else if (recvBuff[0] == 0x58)
                    {

                        if (!duplicateFlag)
                        {
                            mythis.NodeProfile[nodeID - 1].EventID = mythis.getUniqueEventID(); //+= 1;
                            iData.eid = mythis.NodeProfile[nodeID - 1].EventID;
                            /*bool eidTaken = true;
                            while (eidTaken)
                            {
                                eidTaken = false;
                                for (int i = 0; i < mythis.NodeProfile.Count(); i++) {
                                    if (mythis.NodeProfile[i].EventID == mythis.NodeProfile[nodeID - 1].EventID && i != nodeID - 1)
                                    {
                                        mythis.NodeProfile[nodeID - 1].EventID++;
                                        eidTaken = true;
                                    }
                                }
                            }*/
                            iData.UID = nodeID;
                            //if(CheckEventWirelessByEID(iData, nodeID))
                            //    gDataWUpdateEvent(iData);
                            //else
                                gDataInsertImpact(iData, "EventsWireless");     // Insert iData into Event table 
                        }
                        //Temperature display Disable, so don't need to update the view
                    } 
                }
            }
            catch (Exception ex)
            {
                //DisplayException(mythis.Name, ex);
                //MessageBox.Show("uploadData()");
               // throw ex;
                //m_Edit_RxPacket
                //if (mythis.m_Edit_RxPacket.Visible)
                {
                }
            }
        }
        public double BrIC(double[] maxRot) {
            return Math.Sqrt(Math.Pow(maxRot[0] / Form1.BrIC_CX, 2) + Math.Pow(maxRot[1] / Form1.BrIC_CY, 2) + Math.Pow(maxRot[2] / Form1.BrIC_CZ, 2));
        }

    }
}

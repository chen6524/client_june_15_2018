﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gForceTrackerWireless;
using WirelessGFTViewer.Properties;
using System.Threading;

namespace WirelessGFTViewer
{
    public partial class WirelessViewer : UserControl
    {
        public event LightOnChangedEventHandler OnRowSelected;
        public event UploadAllHandler OnButtonUploadAll;
        public event RemoveFromHubHandler OnMnuRemoveFromHub;
        public event RemoveFromHubDataGridHandler onMnuRemoveFromHubDG;
        public event GetImpactsHandler OnMnuGetImpacts;
        public event GetImpactsDataGridHandler OnMnuGetImpactsDG;
        public event PowerOffOneHandler OnMnuPowerOff;
        public event PowerOffOneDataGridHandler OnMnuPowerOffDG;
        public event StopRecordingHandler OnMnuStopRecording;
        public event StopRecordingDataGridHandler OnMnuStopRecordingDG;
        public event StartRecordingHandler OnMnuStartRecording;
        public event StartRecordingDataGridHandler OnMnuStartRecordingDG;
        public event PowerOffAllHandler OnButtonPowerOffAll;
        public event EnableProximityAllHandler OnButtonEnableProximityAll;
        public event DisableProximityAllHandler OnButtonDisableProximityAll;
        public event StopRecordingAllHandler OnButtonStopRecordingAll;
        public event StartRecordingAllHandler OnButtonStartRecordingAll;

        public event PowerOnOneHandler OnMnuPowerOn;
        public event PowerOnOneDataGridHandler OnMnuPowerOnDG;
        public event PowerOnAllHandler OnButtonPowerOnAll;

        private LP_HUB lpHub;
        private List<Form1.NODE_PROFILE> NodeProfile;
        public bool uploadFlag = false;
        private NodeOnSelectedArgs mnuRightClickRow = null;


        public WirelessViewer()
        {
            InitializeComponent();
            dgvLpInit();

        }

        public DataGridView dataGridView
        {
            get
            {
                return dgvViewer;
            }
        }

        public void setHub(ref LP_HUB lpHub) {
            this.lpHub = lpHub;
        }

        public void setProfile(ref List<Form1.NODE_PROFILE> NodeProfile)
        {
            this.NodeProfile = NodeProfile;
        }

        private void dgvLpInit()
        {
            dgvViewer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgvViewer.RowsDefaultCellStyle.BackColor = System.Drawing.Color.White;
            dgvViewer.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(228)))), ((int)(((byte)(255)))));
            dgvViewer.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(245)))), ((int)(((byte)(169)))));
            dgvViewer.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dgvViewer.ColumnCount = 13;

            dgvViewer.Columns[0].Name = " ";// Number";
            dgvViewer.Columns[0].Width = 0;
            dgvViewer.Columns[0].Visible= false;
            dgvViewer.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

            //dgvViewer.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvViewer.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[1].Name = "#";
            dgvViewer.Columns[1].ToolTipText = "Player Number";
            dgvViewer.Columns[1].Width = 30;
            dgvViewer.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

            dgvViewer.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;
            dgvViewer.Columns[2].DefaultCellStyle.ForeColor = Color.Blue;
            dgvViewer.Columns[2].Name = "Name";
            dgvViewer.Columns[2].Width = 60;
            dgvViewer.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;


            dgvViewer.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[3].DefaultCellStyle.ForeColor = Color.Blue;
            dgvViewer.Columns[3].Name = "Position";
            dgvViewer.Columns[3].Width = 90;
            dgvViewer.Columns[3].Visible = false;
            dgvViewer.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;




            dgvViewer.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[4].Name = "Max g";
            dgvViewer.Columns[4].Width = 45;
            dgvViewer.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;


            dgvViewer.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[5].Name = "Max R.";
            dgvViewer.Columns[5].Width = 45;
            dgvViewer.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;

            dgvViewer.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvViewer.Columns[5].DefaultCellStyle.BackColor = Color.Black;
            dgvViewer.Columns[6].Name = "Alarm";
            dgvViewer.Columns[6].ToolTipText = "Alarm Condition";
            dgvViewer.Columns[6].Width = 40;
            dgvViewer.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;




            /*dgvViewer.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[8].Name = "Start Time";
            dgvViewer.Columns[8].Width = 95;
            dgvViewer.Columns[8].Visible = false;
            dgvViewer.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;*/

            /*dgvViewer.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[9].Name = "Time Span";
            dgvViewer.Columns[9].Width = 105;
            dgvViewer.Columns[9].Visible = false;
            dgvViewer.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;*/


            dgvViewer.Columns[7].Name = "Ping";
            dgvViewer.Columns[7].Width = 70;
            dgvViewer.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

            //dgvViewer.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft; 
            dgvViewer.Columns[8].Name = "Status";
            dgvViewer.Columns[8].Width = 70;
            dgvViewer.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;


            dgvViewer.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[9].Name = "Bat";
            dgvViewer.Columns[9].ToolTipText = "Battery Level";
            dgvViewer.Columns[9].Width = 40;
            dgvViewer.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;

            dgvViewer.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[10].DefaultCellStyle.ForeColor = Color.Blue;
            dgvViewer.Columns[10].ToolTipText = "Click to Download Impacts";
            dgvViewer.Columns[10].Name = "Download";
            dgvViewer.Columns[10].Width = 60;
            dgvViewer.Columns[10].SortMode = DataGridViewColumnSortMode.NotSortable;


            dgvViewer.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvViewer.Columns[11].Name = "Progress";
            dgvViewer.Columns[11].Width = 50;
            dgvViewer.Columns[11].Visible = true;
            dgvViewer.Columns[11].SortMode = DataGridViewColumnSortMode.NotSortable;


            dgvViewer.Columns[12].Name = " "; // Values: Powered on = 1, Powered off = 9
            dgvViewer.Columns[12].Width = 0;
            dgvViewer.Columns[12].Visible = false;
            dgvViewer.Columns[12].SortMode = DataGridViewColumnSortMode.NotSortable;


            //dgvViewer.
            //dgvViewer.AllowUserToDeleteRows = false;
            //dgvViewer.AllowUserToAddRows = false;
            //dgvViewer.ReadOnly = true;
            //dgvViewer.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            //dgvViewer.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            //dgvViewer.RowTemplate.Height = 30;
        }

        private int m_selectedRow;
        private int m_NodeID;

        public int selectedRow
        {
            get
            {
                return m_selectedRow;
            }
            set
            {
                if (m_selectedRow != value)
                {
                    m_selectedRow = value;
                }
            }
        }

        public int node_ID
        {
            get
            {
                return m_NodeID;
            }
            set
            {
                if (m_NodeID != value)
                {
                    m_NodeID = value;
                }
            }

        }


        public void show_device_receiving(int node) {
            foreach (DataGridViewRow cur_row in dataGridView.Rows)
            {
                if (cur_row != null)
                    if (node == Convert.ToInt16(cur_row.Cells[0].Value.ToString()))
                    {
                        string test = cur_row.Cells[1].Value.ToString();
                        cur_row.Cells[3].Value = "Receiving...";
                    }
                else
                    return;

            }
        }
#if false
        public void DebugCmdBoxDisplay(string textInfo)
        {
            //if ((m_DebugCmdBox.Visible) && (textInfo != null))
            //    m_DebugCmdBox.AppendText(textInfo);
        }


        public void DebugCmdBoxClear()
        {
            //m_DebugCmdBox.Clear();
            //m_DebugDataBox.Clear();
        }
#endif
#if false
        public void DebugDataBoxDisplay(string textInfo)
        {
            //if ((m_DebugDataBox.Visible) && (textInfo != null))
            //    m_DebugDataBox.AppendText(textInfo);
        }
#endif

        private void dgvViewer_MouseClick(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu mnuRightClick = new ContextMenu();

                int currentMouseOverRow = dgvViewer.HitTest(e.X, e.Y).RowIndex;
                int currentMouseOverCol = dgvViewer.HitTest(e.X, e.Y).ColumnIndex;
                if (currentMouseOverRow >= 0)
                {
                    if(dgvViewer.CurrentCell != null) dgvViewer.Rows[dgvViewer.CurrentCell.RowIndex].Selected = false;
                    dgvViewer.Rows[currentMouseOverRow].Selected = true;
                    dgvViewer.CurrentCell = dgvViewer[currentMouseOverCol, currentMouseOverRow];
                    MenuItem RemoveFromHub = new MenuItem("Remove From Hub");
                    MenuItem StopRecording = new MenuItem("Stop Recording Impacts");
                    MenuItem StartRecording = new MenuItem("Start Recording Impacts");
                    MenuItem GetImpacts = new MenuItem("View All Impacts");
                    MenuItem PowerOff = new MenuItem("Power Off");
                    MenuItem PowerOn = new MenuItem("Power On");
                    int thisNodeID = Convert.ToInt16(dgvViewer.Rows[currentMouseOverRow].Cells[0].Value.ToString());

                    if (lpHub.lpDevices[thisNodeID - 1].startRecordingFlag)
                    {
                        StartRecording.Enabled = false;
                        StartRecording.Checked = true;
                        StartRecording.Text += ": Waiting for response from GFT";
                    }
                    if (lpHub.lpDevices[thisNodeID - 1].stopRecordingFlag)
                    {
                        StopRecording.Enabled = false;
                        StopRecording.Checked = true;
                        StopRecording.Text += ": Waiting for response from GFT";
                    }
                    if (lpHub.lpDevices[thisNodeID - 1].powerOffSent == false)
                    {
                        PowerOff.Enabled = false;
                        PowerOff.Checked = true;
                        PowerOff.Text += ": Waiting for response from GFT";
                    }

                    if (lpHub.lpDevices[thisNodeID - 1].powerOnSent == false)
                    {
                        PowerOn.Enabled = false;
                        PowerOn.Checked = true;
                        PowerOn.Text += ": Waiting for response from GFT";
                    }
                    if (lpHub.lpDevices[thisNodeID - 1].getImpactsSent == false)
                    {
                        GetImpacts.Enabled = false;
                        GetImpacts.Checked = true;
                        GetImpacts.Text += ": Waiting for response from GFT";
                    }

                    mnuRightClickRow = new NodeOnSelectedArgs(currentMouseOverRow, currentMouseOverCol, thisNodeID);
                    RemoveFromHub.Click += new System.EventHandler(OnMnuRemoveFromHub);
                    StopRecording.Click += new System.EventHandler(OnMnuStopRecording);
                    StartRecording.Click += new System.EventHandler(OnMnuStartRecording);
                    GetImpacts.Click += new System.EventHandler(OnMnuGetImpacts);
                    PowerOff.Click += new System.EventHandler(OnMnuPowerOff);
                    PowerOn.Click += new System.EventHandler(OnMnuPowerOn);


                    if (lpHub.lpDevices[thisNodeID - 1].mode == 0)
                    {
                        mnuRightClick.MenuItems.Add(PowerOn);
                        mnuRightClick.MenuItems.Add(RemoveFromHub);
                    }
                    else
                    {
                        mnuRightClick.MenuItems.Add(GetImpacts);

                        if (lpHub.lpDevices[thisNodeID - 1].stopRecording)
                            mnuRightClick.MenuItems.Add(StartRecording);
                        else
                            mnuRightClick.MenuItems.Add(StopRecording);

                        mnuRightClick.MenuItems.Add(PowerOff);
                    }
                    mnuRightClick.Show(dgvViewer, new Point(e.X, e.Y));
                }
            } else {
                return;
            }
        }

        private void dgvViewer_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            UInt32 row = (UInt32)(e.RowIndex);
            if ((row == 0xffffffff) || (e.RowIndex > 102/*62*/) || (e.RowIndex < 0)) 
                return;


                //string sss = (dgvViewer.Rows[e.RowIndex].Cells[0].Value).ToString();
                int n_id = 0;
                try
                {
                    n_id = Convert.ToInt32(dgvViewer.Rows[e.RowIndex].Cells[0].Value);
                    //n_id = Convert.ToInt32(e.RowIndex + 1);
                }
                catch (Exception ee)
                {
                    string temp = ee.Message;
                    return;
                }
                if (n_id == 0)
                    return;
                node_ID = n_id;

                //else node_ID = 9;
                //UID = (Int32)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
#if ENABLE_DASHBOARD
                if (e.ColumnIndex == 14)
                {
                    if (Convert.ToBoolean(dgvViewer.Rows[e.RowIndex].Cells[14].Value) == false)
                        dgvViewer.Rows[e.RowIndex].Cells[14].Value = true;
                    else
                        dgvViewer.Rows[e.RowIndex].Cells[14].Value = false;
                }

#else
                if (e.ColumnIndex == 10)
                {
                    if (Convert.ToBoolean(dgvViewer.Rows[e.RowIndex].Cells[10].Value) == false)
                        dgvViewer.Rows[e.RowIndex].Cells[10].Value = true;
                    else
                        dgvViewer.Rows[e.RowIndex].Cells[10].Value = false;
                }

#endif
                    if (OnRowSelected != null)
                {
                    OnRowSelected(this, new NodeOnSelectedArgs(e.RowIndex, e.ColumnIndex, node_ID));
                }
        }

        private void dgvViewer_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            int n_id = 0;
            try
            {
                n_id = Convert.ToInt32(dgvViewer.Rows[e.RowIndex].Cells[0].Value);
                //n_id = Convert.ToInt32(e.RowIndex + 1);
            }
            catch (Exception ee)
            {
                string temp = ee.Message;
            }


            if (n_id == 0)
                dgvViewer.Cursor = Cursors.Default;
            else
                dgvViewer.Cursor = Cursors.Hand;
        }

        private void dgvViewer_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
#if false
            int n_id = 0;
            if (e.ColumnIndex == 6)
            {

                for (int i = 0; i < 63; i++)
                {
                    try
                    {
                        n_id = Convert.ToInt32(dgvViewer.Rows[i].Cells[0].Value);
                    }
                    catch (Exception ee)
                    {
                        string temp = ee.Message;
                    }
                    if (n_id != 0)
                    {
                        DataGridViewCell cellImpact = new DataGridViewTextBoxCell();
                        cellImpact.Value = "0.00";
                        dgvViewer.Rows[i].Cells[6] = cellImpact;
                    }
                }
            }
#else

#if ENABLE_DASHBOARD
            if (e.ColumnIndex == 14)
#else
            if (e.ColumnIndex == 10)
#endif
            {
                if (OnButtonUploadAll != null)
                {
                    OnButtonUploadAll(this, null);
                }
            }
#endif
        }

        public void removeFromHub_Click(Object sender, EventArgs e)
        {
            if (onMnuRemoveFromHubDG != null)
                onMnuRemoveFromHubDG(this, mnuRightClickRow);
        }

        public void getImpacts_Click(Object sender, EventArgs e)
        {
            if (OnMnuGetImpactsDG != null)
                OnMnuGetImpactsDG(this, mnuRightClickRow);
        }

        public void powerOffOne_Click(Object sender, EventArgs e)
        {
            if (OnMnuPowerOffDG != null)
                OnMnuPowerOffDG(this, mnuRightClickRow);
        }

        public void powerOnOne_Click(Object sender, EventArgs e)
        {
            if (OnMnuPowerOnDG != null)
                OnMnuPowerOnDG(this, mnuRightClickRow);
        }

        public void stopRecording_Click(Object sender, EventArgs e)
        {
            if (OnMnuStopRecordingDG != null)
                OnMnuStopRecordingDG(this, mnuRightClickRow);
        }

        public void startRecording_Click(Object sender, EventArgs e)
        {
            if (OnMnuStartRecordingDG != null)
                OnMnuStartRecordingDG(this, mnuRightClickRow);
        }

        private void UploadAll_Click(object sender, EventArgs e)
        {
            if (OnButtonUploadAll != null)
            {
                OnButtonUploadAll(this, null);
            }
        }

        private void powerOffGFTs_Click(object sender, EventArgs e)
        {
            if (OnButtonPowerOffAll != null)
            {
                this.powerOnGFTs.BackColor = this.powerOffGFTs.BackColor;
                OnButtonPowerOffAll(this, null);
            }
        }
        private void powerOnGFTs_Click(object sender, EventArgs e)
        {

            if (OnButtonPowerOnAll != null)
            {
                this.powerOnGFTs.BackColor = Color.Green;
                OnButtonPowerOnAll(this, null);
            }
        }


        private void stopRecordingGFTs_Click(object sender, EventArgs e)
        {
            if (OnButtonStopRecordingAll != null)
            {
                OnButtonStopRecordingAll(this, null);
            }
        }

        private void startRecordingGFTs_Click(object sender, EventArgs e)
        {
            if (OnButtonStartRecordingAll != null)
            {
                OnButtonStartRecordingAll(this, null);
            }
        }
        private void proximityDisableAll_Click(object sender, EventArgs e)
        {
            if (OnButtonDisableProximityAll != null)
            {
                OnButtonDisableProximityAll(this, null);
            }
        }
        private void proximityEnableAll_Click(object sender, EventArgs e)
        {
            if (OnButtonEnableProximityAll != null)
            {
                OnButtonEnableProximityAll(this, null);
            }
        }
    }

    public class NodeOnSelectedArgs : EventArgs
    {
        public readonly int selectedRow;
        public readonly int selectedColumn;
        public readonly int NodeID;
        
        public NodeOnSelectedArgs(int row, int column, int nodeID)
        {
            selectedRow = row;
            selectedColumn = column;
            NodeID = nodeID;
        }
    }

    public delegate void LightOnChangedEventHandler(object sender, NodeOnSelectedArgs args);
    public delegate void UploadAllHandler(object sender, EventArgs args);
    public delegate void PowerOffAllHandler(object sender, EventArgs args);
    public delegate void PowerOnAllHandler(object sender, EventArgs args);
    public delegate void EnableProximityAllHandler(object sender, EventArgs args);
    public delegate void DisableProximityAllHandler(object sender, EventArgs args);
    public delegate void RemoveFromHubHandler(object sender, EventArgs args);
    public delegate void RemoveFromHubDataGridHandler(object sender, NodeOnSelectedArgs args);
    public delegate void StopRecordingAllHandler(object sender, EventArgs args);
    public delegate void StartRecordingAllHandler(object sender, EventArgs args);

    public delegate void GetImpactsHandler(object sender, EventArgs args);
    public delegate void GetImpactsDataGridHandler(object sender, NodeOnSelectedArgs args);

    public delegate void DownloadHandler(object sender, EventArgs args);
    public delegate void DownloadDataGridHandler(object sender, NodeOnSelectedArgs args);

    public delegate void PowerOffOneHandler(object sender, EventArgs args);
    public delegate void PowerOffOneDataGridHandler(object sender, NodeOnSelectedArgs args);

    public delegate void PowerOnOneHandler(object sender, EventArgs args);
    public delegate void PowerOnOneDataGridHandler(object sender, NodeOnSelectedArgs args);

    public delegate void StopRecordingHandler(object sender, EventArgs args);
    public delegate void StopRecordingDataGridHandler(object sender, NodeOnSelectedArgs args);

    public delegate void StartRecordingHandler(object sender, EventArgs args);
    public delegate void StartRecordingDataGridHandler(object sneder, NodeOnSelectedArgs args);

    #region lp_HUB
    public class LP_DEVICE
    {
        //lp info
        public bool valid = false;
        public byte nodeID;
        public byte[] MID = new byte[4];
        public byte nodePwrMode = 0;
        public char[] name = new char[20];
        public char[] position = new char[20];
        public int playerNumer;
        public byte[] GID = new byte[6];
        //hit
        public bool alarm = false;
        public float gForce = 0;
        public float lastAlarmgForce = 0;
        public int lastAlarmImpact = 0;
        public int thresholdImpacts = 0;
        public int aboveThreshold = 0;
        public int withinThreshold = 0;
        public int impacts = 0;
        public int sessions = 0;
        public DateTime lastAlarmTime;
        public DateTime lastThresholdTime;
        public double Rotation = 0;
        public int highestRotImpact = 0;
        public int highestGImpact = 0;
        public DateTime time;
        public int mode = 0;                                        //
        public bool charging = false; // sometimes GFT reports mode 1 instead of 0x81 even though it's charging. Power on reports correctly.
        public bool recordEnabled = true;
        public int timeSynced = 1;
        public bool reviewYellow = false;
        public bool reviewRed = false;
        public bool live = false; //is power on 
        //public byte[] powerOn1 = new byte[15];
        //public byte[] powerOn2 = new byte[15];
        public bool pwrOnValid = false;
        public float temperature = 0.0f;
        public int battLevel = 0;
        public int bat_Levelfuel = 6792;                                // Added by Jason Chen, 2014.04.16
        public bool max17047Exist = false;                              // Added by Jason Chen, 2014.04.16
        public String LastDate;// = "00:00:00";//DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
        public String TimeSpan = DateTime.UtcNow.ToShortDateString() + " " + DateTime.UtcNow.ToLongTimeString();
        public bool upldFlag;
        public int  upldFlag_num;
        public int progressValue = 0;

        public bool receivingData = false;

        public bool powerOffFlag = false;
        public bool powerOnFlag = false;
        public bool keepPowerOnFlag = false;
        public bool stopRecordingFlag = false;
        public bool startRecordingFlag = false;
        public bool getImpactsFlag = false;
        public bool getImpactsSent = true; // this is only false when the command has been sent, but the device has not gone to sleep since
        public bool powerOffSent = true; // this is only false when the command has been sent, but the device has not gone to sleep since
        public bool powerOnSent = true; // this is only false when the command has been sent, but the device has not gone to sleep since

        public bool stopRecording = false;                            // Added by Jason Chen, 2014.06.03
        public int impact_data_len = 573;                             // 09.11

        public Int32 activeTime = 0;

        public double cumulativeRPE = 0;

        public bool firstReceiveSet = false;
        public DateTime firstReceiveTime;
        public DateTime lastReceiveTime;
        public DateTime lastPingTime;
        public bool updateDgv = true;

        public System.Drawing.Bitmap batImg = Resources.BattOff_2;
        public string status = "";
        public string commandStatus = "";

        public int selectedImpact = 0;

        public int gftType = 0;

        public List<Form1.ACTIVITY_DATA> activeTimeList;

        public List<Form1.RPE_DATA> rpeList;

        public LP_DEVICE()
        {
            activeTimeList = new List<Form1.ACTIVITY_DATA>();
            rpeList = new List<Form1.RPE_DATA>();
        }

        public void processCmds()
        {
            if (powerOffFlag == false && powerOffSent == false)
            {
                if (this.mode > 0)
                {
                    powerOffFlag = true;
                }
                else
                {
                    powerOffSent = true;
                }
            }
            if (powerOnFlag == false && powerOnSent == false)
            {
                if (this.mode > 0)
                {
                    powerOnSent = true;
                } else
                {
                    powerOnFlag = true; // Device is not on, try again.
                }
            }
            if (getImpactsFlag == false && getImpactsSent == false && this.mode > 0) getImpactsSent = true;
        }

        public void resetTracking()
        {
            alarm = false;
            highestGImpact = 0;
            highestRotImpact = 0;
            gForce = 0;
            Rotation = 0;
            timeSynced = 0;
            lastAlarmgForce = 0;
            thresholdImpacts = 0;
            aboveThreshold = 0;
            withinThreshold = 0;
            impacts = 0;
            sessions = 0;
            powerOffFlag = false;
            powerOnFlag = false;
            getImpactsFlag = false;
            powerOffSent = true;
            powerOnSent = true;
            reviewYellow = false;
            reviewRed = false;
            charging = false;
        }


        public void PerformanceSummaryPacketProcess(Form1.procArgs args)
        {
            Form1 myThis = args.sender as Form1;
            int recv_len = args.recv_count;
            if (args.nodeID == nodeID)
            {
                //label18.Text = "this is from Node " + nodeID.ToString() + "  " + packetLen.ToString();
                    uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                    processRecvbuff.performanceSummaryDataForWireless(args.sumbuff, nodeID);
                    myThis.wirelessViewer_OnNewData(this, myThis.lastNodeSelected);

                    args.impactFlag = false;
                args.mHitFlag = false;
                args.recv_count = 0;
                args.recvbuffstr = "";
            }
        }

        public void SummaryPacketProcess(Form1.procArgs args)
        {
            Form1 myThis = args.sender as Form1;
#if false
            myThis.processRecv(nodeID, args.recvbuff, args.recv_count);
#else

            int recv_len = args.recv_count;
            if (args.nodeID == nodeID)
            {
                //label18.Text = "this is from Node " + nodeID.ToString() + "  " + packetLen.ToString();
                    uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                    processRecvbuff.summaryDataForWireless(args.sumbuff, nodeID);
#if ENABLE_DASHBOARD
                    myThis.wirelessViewer_OnNewData(this, myThis.lastNodeSelected);
#else
                    if (myThis.lastNodeSelected != null) myThis.wirelessViewer_OnRowSelected(this, myThis.lastNodeSelected); // Only refresh the right side display if it's a preview impact for the selected device
#endif
                    args.impactFlag = false;
                args.mHitFlag = false;
                args.recv_count = 0;
                args.recvbuffstr = "";
            }
#endif
        }
        public void PerformancePacketProcess(Form1.procArgs args)
        {
            Form1 myThis = args.sender as Form1;

            int recv_len = args.recv_count;
            if (args.nodeID == nodeID)
            {
                if (recv_len == 291)
                {

                    uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                    processRecvbuff.uploadPerformanceDataForWireless(args.recvbuff, nodeID, (myThis.NodeProfile[nodeID - 1].impactCount >= myThis.NodeProfile[nodeID - 1].impactNum ? true : false)); //, 

                }
                else if (recv_len == 39)
                {
                    if (args.recvbuff[0] == 0x49)
                    {
                        uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                        processRecvbuff.uploadDataForWirelessStartPerfSession(args.recvbuff, nodeID);
                    }
                    else if (args.recvbuff[0] == 0x4A)
                    {
                        uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                        processRecvbuff.uploadDataForWirelessEndPerfSession(args.recvbuff, nodeID);
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("?");
                }
                args.impactFlag = false;
                args.mHitFlag = false;
                args.recv_count = 0;
                args.recvbuffstr = "";
            }
        }
        public void ImpactPacketProcess(Form1.procArgs args)
        {
            Form1 myThis = args.sender as Form1;
#if false
            myThis.processRecv(nodeID, args.recvbuff, args.recv_count);
#else

            int recv_len = args.recv_count;
            if (args.nodeID == nodeID)
            {
                //label18.Text = "this is from Node " + nodeID.ToString() + "  " + packetLen.ToString();
                if (recv_len == 573)
                {
                    //byte[] recvBuff = new byte[573];
                    //Array.Copy(args.recvbuff, recvBuff, 573);
                    //args.recvQueue.Enqueue(recvBuff);

                    uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                    processRecvbuff.uploadDataForWireless(args.recvbuff, nodeID, (myThis.NodeProfile[nodeID - 1].impactCount >= myThis.NodeProfile[nodeID - 1].impactNum ? true : false)); //, 
#if ENABLE_DASHBOARD
                    if (args.recvbuff[0] == 0x48) myThis.wirelessViewer_OnNewData(this, myThis.lastNodeSelected);
#else
                    if (args.recvbuff[0] == 0x48 && myThis.lastNodeSelected != null) myThis.wirelessViewer_OnRowSelected(this, myThis.lastNodeSelected); // Only refresh the right side display if it's a preview impact for the selected device
#endif

                }
                else if (recv_len == 39)
                {
                    if (args.recvbuff[0] == 0x49)
                    {
                        uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                        processRecvbuff.uploadDataForWirelessStartSession(args.recvbuff, nodeID);
                    }
                    else if (args.recvbuff[0] == 0x4A)
                    {
                        uploadDataForWirelessProcess processRecvbuff = new uploadDataForWirelessProcess(myThis);
                        processRecvbuff.uploadDataForWirelessEndSession(args.recvbuff, nodeID);
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("?");
                }
                args.impactFlag = false;
                args.mHitFlag = false;
                args.recv_count = 0;
                args.recvbuffstr = "";
            }
#endif
        }
    };

    public class LP_HUB
    {
        delegate void SetDgvCallback();
        internal const byte MAX_DEVICE_CNT = 103;//63;

        internal const int BATT_LEVEL_10PECENT  = 0x67 * 10;//103
        internal const int BATT_LEVEL_20PECENT  = 0x6A * 10;//106
        internal const int BATT_LEVEL_30PECENT  = 0x6F * 10;//111
        internal const int BATT_LEVEL_40PECENT  = 0x75 * 10;//117
        internal const int BATT_LEVEL_50PECENT  = 0x76 * 10;//118
        internal const int BATT_LEVEL_60PECENT  = 0x77 * 10;//119
        internal const int BATT_LEVEL_70PECENT  = 0x78 * 10;//120
        internal const int BATT_LEVEL_80PECENT  = 0x79 * 10;//121
        internal const int BATT_LEVEL_90PECENT  = 0x7A * 10;//122
        internal const int BATT_LEVEL_100PECENT = 0x7B * 10;//123

        internal const int F_BATT_LEVEL_10PECENT = 5440;//103
        internal const int F_BATT_LEVEL_20PECENT = 5584;//106
        internal const int F_BATT_LEVEL_30PECENT = 5712;//111
        internal const int F_BATT_LEVEL_40PECENT = 5856;//117
        internal const int F_BATT_LEVEL_50PECENT = 6000;//118
        internal const int F_BATT_LEVEL_60PECENT = 6144;//119
        internal const int F_BATT_LEVEL_70PECENT = 6288;//120
        internal const int F_BATT_LEVEL_80PECENT = 6432;//121
        internal const int F_BATT_LEVEL_90PECENT = 6576;//122
        internal const int F_BATT_LEVEL_100PECENT = 6720;//123

        public List<LP_DEVICE> lpDevices = new List<LP_DEVICE>();
        //public SyncList<LP_DEVICE_VIEW> lpDevicesView = new SyncList<LP_DEVICE_VIEW>();

        public DataGridView dgv;
        private Bitmap[] tempBitmap = new Bitmap[MAX_DEVICE_CNT];// Resources.Batt50_2;
        //private ImageList imageList;

        //internal byte state = 63;
        //internal byte version;

        internal byte sopIdx;
        internal byte chBase;
        internal byte chHop;
        internal short hubSeed;
        internal byte maxNodes;
        internal byte BCD;
        internal byte chnMin;
        internal byte chnMax;
        internal byte curCh;
        internal byte upldMod;
        

        public LP_HUB(DataGridView dgvg)//, ImageList imageL)
        {
            LP_DEVICE lpd;
            dgv = dgvg;
            //imageList = imageL;

            for (byte i = 0; i < MAX_DEVICE_CNT; i++)
            {
                lpd = new LP_DEVICE();
                lpd.nodeID = (byte)(i + 1);
                lpDevices.Add(lpd);

                //DataGridViewRow myDGVR = new DataGridViewRow();
                //dgv.Rows.Add(myDGVR);

                tempBitmap[i] = Resources.Batt50_2;
            }
            //dgv.DataSource = lpDevicesView;  
        }
        
        internal void refreshBinding()
        {
            //dgv.DataSource = null;
            //dgv.DataSource = lpDevicesView;  
        }

#if false
            internal bool updateDevice(byte nodId, ref byte[] MID, byte pwrMode)
            {
                foreach (LP_DEVICE dev in lpDevices)
                    if (dev.nodeID == nodId)
                    {
                        dev.valid = true;
                        dev.nodePwrMode = pwrMode;
                        Array.Copy(MID, dev.MID, dev.MID.Length);
                        //update view list
                        // updateDgvList(dev);  
                        break;
                    }
                dgvLpPopulateView();
                return true;
            }
#endif
        internal void selectRow(int mRow)
        {
            //dgv.CurrentCell = dgv[2, mRow];
            //dgv.Rows[mRow].Selected = true;

            //dgv.CurrentCell = dgv[1, mRow];
           // dgv.Rows[mRow].Selected = true;
        }

        internal void dgvLpUpdateView()
        {
            if (this.dgv.InvokeRequired)
            {
                SetDgvCallback d = new SetDgvCallback(dgvLpUpdateView);
                dgv.Invoke(d);
            }
            else
            {
                foreach (LP_DEVICE dev in this.lpDevices)
                {
                    if (dev.valid == true && dev.updateDgv == true)
                    {
                        dev.updateDgv = false;
                        updateDGView(dev.nodeID);
                    }
                }
            }
        }

        internal void dgvLpPopulateView()
        {
                int i = 0;
               // lpDevices.Clear();
                dgv.Rows.Clear();
                foreach (LP_DEVICE dev in this.lpDevices)
                {
                    dev.resetTracking();
                    //if (dev.valid == true)
                    {
                        DataGridViewRow myDGVR = new DataGridViewRow();
                        i = dgv.Rows.Add(myDGVR);
                        dev.updateDgv = true;
                        updateDGView(i);
                    }
                }
        }

        internal void update_device_pings()
        {
            int node = 0;
            foreach (DataGridViewRow cur_row in dgv.Rows)
            {
                if (cur_row != null)
                {
                    node = Convert.ToInt16(cur_row.Cells[0].Value.ToString());
                    if (lpDevices[node - 1].mode != 4) // if it's uploading, it isn't showing last ping time..
                    {
                        cur_row.Cells[11].Value = System.DateTime.UtcNow.Subtract(lpDevices[node - 1].lastPingTime).TotalSeconds.ToString("f0");
                    }
                }
            }
        }

        internal void dgvLpPopulateView(int nodeID)
        {
            //updateDGView(nodeID - 1);
            lpDevices[nodeID-1].updateDgv = true;
            updateDGView(nodeID);
            
        }

        internal int getRowNum(int nodeID)
        {
            int i;
            int rowCount = dgv.Rows.Count;
            for (i = 0; i < rowCount; i++)
            {
                int cellValue = Convert.ToInt32(dgv.Rows[i].Cells[0].Value);
                if (cellValue == nodeID)
                {
                    return i;
                }
            }
            return -1;
        }
        public bool deviceDownloading()
        {
            int nodeID;
            bool returnval = false;
            for (nodeID = 1; nodeID < 55; nodeID++)
            {
                if (lpDevices[nodeID - 1].valid)
                {
                    if (lpDevices[nodeID - 1].progressValue > 0 && lpDevices[nodeID - 1].progressValue < 100) returnval = true;
                }
            }
            return returnval;
        }
        public void updateDGView(int mRow1)
        {
            bool downloadingFlag = false;
            int mRow = getRowNum(mRow1);

            int rowCount = dgv.Rows.Count;
          
            if (mRow < 0 || mRow > rowCount)
                return;


            LP_DEVICE dev = lpDevices[mRow1-1];

            if (!dev.valid)
                return;

            //dgv.Rows[mRow].DefaultCellStyle.BackColor = Color.Black;

            DataGridViewCell cellID = new DataGridViewTextBoxCell();
#if true
            cellID.Value = dev.nodeID;
            dgv.Rows[mRow].Cells[0] = cellID;
#else
            cellID.Value = dev.upldFlag_num;
            dgv.Rows[mRow].Cells[0] = cellID;
#endif

            DataGridViewCell cellName = new DataGridViewTextBoxCell();
            cellName.Value = new string(dev.name);
            cellName.ToolTipText = "Session Data";
            dgv.Rows[mRow].Cells[2] = cellName;


            if (dev.playerNumer > 0)
            {
#if false
                DataGridViewCell cellplayerNum = new DataGridViewTextBoxCell();
                cellplayerNum.Value     = dev.playerNumer;
                dgv.Rows[mRow].Cells[1] = cellplayerNum;
#else
                //DataGridViewButtonCell cellplayerNum = new DataGridViewTextBoxCell();
                DataGridViewCell cellplayerNum = new DataGridViewButtonCell();
                cellplayerNum.Value = dev.playerNumer;
                cellplayerNum.ToolTipText = "Click to Get Field Impact";
                dgv.Rows[mRow].Cells[1] = cellplayerNum;

#endif
            }

            if (dev.mode == 4)
            { // Show Download Progress Cell
                DataGridViewCell cellprogress = new DataGridViewProgressCell();   // Adde by Jason chen for upload progressBar, 2014.02.10
                cellprogress.Value = dev.progressValue;

                dgv.Rows[mRow].Cells[11] = cellprogress;
            }
            else
            {

                DataGridViewCell cellDateLast = new DataGridViewTextBoxCell();   // Adde by Jason chen for upload time, 2014.02.10
                cellDateLast.Value = dev.LastDate;
                //cellDateLast.ToolTipText = "Click Upload from GFT to Cloud";
                dgv.Rows[mRow].Cells[11] = cellDateLast;
                if (dev.lastPingTime.ToLocalTime().ToString("t") != "12:00 AM") dgv.Rows[mRow].Cells[11].Value = dev.lastPingTime.ToLocalTime().ToString("t");
            }
            // Show Fuel Guage Cell
            RPEFuelCell cellFuel = new RPEFuelCell();
            cellFuel.activityData = dev.activeTimeList;
            cellFuel.rpeData = dev.rpeList;
            cellFuel.activeTime = dev.activeTime;
            cellFuel.firstReceiveTime = dev.firstReceiveTime;
            cellFuel.gftType = dev.gftType;
            if (dev.gftType == 0)
            {
                if (dev.activeTimeList.Count > 0)
                {
                    Form1.ACTIVITY_DATA lData = dev.activeTimeList.Last();
                    Form1.ACTIVITY_DATA fData = dev.activeTimeList.First();

                    // Find the report that immediately preceeeds the first report in the last 5 minutes
                    foreach (Form1.ACTIVITY_DATA aData in dev.activeTimeList)
                    {
                        if (aData.reportTime >= DateTime.UtcNow.AddMinutes(-5))
                        {
                            break;
                        }
                        fData = aData;
                    }
                    int deltaA = (int)Math.Round(((double)(lData.activeTime - fData.activeTime)) / (double)3000, 0);
                    TimeSpan deltaT = lData.reportTime - fData.reportTime;
                    if (deltaT.Seconds > 0)
                    {
                        int activePct = (int)Math.Round(((double)deltaA / (double)deltaT.TotalSeconds) * 100, 0);
                        if (activePct > 100) activePct = 100;
                        if (activePct < 0) activePct = 0;
                        cellFuel.activeVal = activePct;
                    }
                    else
                    {
                        cellFuel.activeVal = 0;
                    }
                }
                else
                {
                    cellFuel.activeVal = 0;
                }
            }
            else
            {
                if (dev.rpeList.Count > 0)
                {
                    Form1.RPE_DATA lData = dev.rpeList.Last();
                    Form1.RPE_DATA fData = dev.rpeList.First();

                    // Get the average RPE over the last 5 minutes
                    double totalRPE = 0;
                    double totalReports = 0;
                    /*foreach (Form1.RPE_DATA aData in dev.rpeList)
                    {
                        if (aData.report_time >= DateTime.UtcNow.AddMinutes(-5))
                        {
                            totalRPE += aData.rpe;
                            totalReports++;
                        }
                    }

                    if (totalReports > 0)
                    {
                        double aveRPE = totalRPE / totalReports;
                        cellFuel.rpeVal = aveRPE;
                    }
                    else
                    {
                        cellFuel.rpeVal = 0;
                    }*/
                    cellFuel.rpeVal = lData.rpe;
                }
                else
                {
                    cellFuel.rpeVal = 0;
                }
            }
            dgv.Rows[mRow].Cells[7] = cellFuel;
            if (dev.rpeList.Count > 0)
            {
                //dgv.Rows[mRow].Cells[9].Value = dev.rpeList.Last().explosive_power;
                dgv.Rows[mRow].Cells[10].Value = dev.rpeList.Last().rpe;
                double pl = 0;
                double ep = 0;
                double ec = 0;
                foreach (Form1.RPE_DATA aData in dev.rpeList)
                {
                    //if (aData.report_time >= DateTime.UtcNow.AddMinutes(-5))
                    {
                        pl += aData.player_load;
                        ep += aData.explosive_power;
                        ec += aData.explosive_count;
                    }

                }
                if(dev.rpeList.Count() > 0) {
                    dgv.Rows[mRow].Cells[8].Value = Math.Round(pl/60, 1);
                    dgv.Rows[mRow].Cells[9].Value = Math.Round(ep / 100, 1).ToString() + "s";
                    dgv.Rows[mRow].Cells[9].Value = ec;
                }
            }
            //DataGridViewCell cellDateSpan = new DataGridViewTextBoxCell();   // Adde by Jason chen for upload time, 2014.02.10
            //cellDateSpan.Value = dev.TimeSpan;
            //dgv.Rows[mRow].Cells[8] = cellDateSpan;


            if (dev.mode == 1 && dev.charging) dev.mode = 0x81; 
#if ENABLE_DASHBOARD
            if (dev.live)
            {
                if (dev.mode == 0x81)
                {
                    // Battery is charging
                        if (dev.battLevel >= 100)
                            dev.batImg = Resources.Batt100_2c;
                        else if ((dev.battLevel >= 90) && (dev.battLevel < 100))
                            dev.batImg = Resources.Batt90_2c;
                        else if ((dev.battLevel >= 80) && (dev.battLevel < 90))
                            dev.batImg = Resources.Batt80_2c;
                        else if ((dev.battLevel >= 70) && (dev.battLevel < 80))
                            dev.batImg = Resources.Batt70_2c;
                        else if ((dev.battLevel >= 60) && (dev.battLevel < 70))
                            dev.batImg = Resources.Batt60_2c;
                        else if ((dev.battLevel >= 50) && (dev.battLevel < 60))
                            dev.batImg = Resources.Batt50_2c;
                        else if ((dev.battLevel >= 40) && (dev.battLevel < 50))
                            dev.batImg = Resources.Batt40_2c;
                        else if ((dev.battLevel >= 30) && (dev.battLevel < 40))
                            dev.batImg = Resources.Batt30_2c;
                        else if ((dev.battLevel > 20) && (dev.battLevel < 30))
                            dev.batImg = Resources.Batt20_2c;
                        else if ((dev.battLevel > 10) && (dev.battLevel <= 20))
                            dev.batImg = Resources.Batt10_2c;
                        else
                            dev.batImg = Resources.Batt10_2c;
                }
                else
                {

                    //cellPwr.Value = global::WirelessGFTViewer.Properties.Resources.PowerOn1;//imageList.Images[19];

                        if (dev.battLevel >= 100)
                            dev.batImg = Resources.Batt100_2;
                        else if ((dev.battLevel >= 90) && (dev.battLevel < 100))
                            dev.batImg = Resources.Batt90_2;
                        else if ((dev.battLevel >= 80) && (dev.battLevel < 90))
                            dev.batImg = Resources.Batt80_2;
                        else if ((dev.battLevel >= 70) && (dev.battLevel < 80))
                            dev.batImg = Resources.Batt70_2;
                        else if ((dev.battLevel >= 60) && (dev.battLevel < 70))
                            dev.batImg = Resources.Batt60_2;
                        else if ((dev.battLevel >= 50) && (dev.battLevel < 60))
                            dev.batImg = Resources.Batt50_2;
                        else if ((dev.battLevel >= 40) && (dev.battLevel < 50))
                            dev.batImg = Resources.Batt40_2;
                        else if ((dev.battLevel >= 30) && (dev.battLevel < 40))
                            dev.batImg = Resources.Batt30_2;
                        else if ((dev.battLevel > 20) && (dev.battLevel < 30))
                            dev.batImg = Resources.Batt20_2;
                        else if ((dev.battLevel > 10) && (dev.battLevel <= 20))
                            dev.batImg = Resources.Batt10_2;
                        else
                            dev.batImg = Resources.Batt10_2;
                    
                }
            }
            else
            {
                //cellPwr.Value = global::WirelessGFTViewer.Properties.Resources.PowerOff1;//imageList.Images[20];
                dev.batImg = Resources.BattOff_2;// Batt_off;// PowerOff1;//imageList.Images[20];        
            }
#else
            DataGridViewCell cellPwr = new DataGridViewImageCell();
            
            if (dev.live)
            {
                if (dev.mode == 0x81)
                {
                    // Battery is charging
                    if (!dev.max17047Exist)
                    {
                    if (dev.battLevel >= BATT_LEVEL_100PECENT)
                        cellPwr.Value = Resources.Batt100_2c;//imageList.Images[20];
                    else if ((dev.battLevel >= BATT_LEVEL_90PECENT) && (dev.battLevel < BATT_LEVEL_100PECENT))
                        cellPwr.Value = Resources.Batt90_2c;//imageList.Images[20];
                    else if ((dev.battLevel >= BATT_LEVEL_80PECENT) && (dev.battLevel < BATT_LEVEL_90PECENT))
                        cellPwr.Value = Resources.Batt80_2c;
                    else if ((dev.battLevel >= BATT_LEVEL_70PECENT) && (dev.battLevel < BATT_LEVEL_80PECENT))
                        cellPwr.Value = Resources.Batt70_2c;
                    else if ((dev.battLevel >= BATT_LEVEL_60PECENT) && (dev.battLevel < BATT_LEVEL_70PECENT))
                        cellPwr.Value = Resources.Batt60_2c;
                    else if ((dev.battLevel >= BATT_LEVEL_50PECENT) && (dev.battLevel < BATT_LEVEL_60PECENT))
                        cellPwr.Value = Resources.Batt50_2c;
                    else if ((dev.battLevel >= BATT_LEVEL_40PECENT) && (dev.battLevel < BATT_LEVEL_50PECENT))
                        cellPwr.Value = Resources.Batt40_2c;
                    else if ((dev.battLevel >= BATT_LEVEL_30PECENT) && (dev.battLevel < BATT_LEVEL_40PECENT))
                        cellPwr.Value = Resources.Batt30_2c;
                    else if ((dev.battLevel > BATT_LEVEL_20PECENT) && (dev.battLevel < BATT_LEVEL_30PECENT))
                        cellPwr.Value = Resources.Batt20_2c;
                    else if ((dev.battLevel > BATT_LEVEL_10PECENT) && (dev.battLevel <= BATT_LEVEL_20PECENT))
                        cellPwr.Value = Resources.Batt10_2c;
                    else
                        cellPwr.Value = Resources.Batt10_2c;
                    }
                    else
                    {
                        if (dev.bat_Levelfuel >= F_BATT_LEVEL_100PECENT)
                            cellPwr.Value = Resources.Batt100_2c;//imageList.Images[20];
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_90PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_100PECENT))
                            cellPwr.Value = Resources.Batt90_2c;//imageList.Images[20];
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_80PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_90PECENT))
                            cellPwr.Value = Resources.Batt80_2c;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_70PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_80PECENT))
                            cellPwr.Value = Resources.Batt70_2c;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_60PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_70PECENT))
                            cellPwr.Value = Resources.Batt60_2c;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_50PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_60PECENT))
                            cellPwr.Value = Resources.Batt50_2c;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_40PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_50PECENT))
                            cellPwr.Value = Resources.Batt40_2c;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_30PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_40PECENT))
                            cellPwr.Value = Resources.Batt30_2c;
                        else if ((dev.bat_Levelfuel > F_BATT_LEVEL_20PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_30PECENT))
                            cellPwr.Value = Resources.Batt20_2c;
                        else if ((dev.bat_Levelfuel > F_BATT_LEVEL_10PECENT) && (dev.bat_Levelfuel <= F_BATT_LEVEL_20PECENT))
                            cellPwr.Value = Resources.Batt10_2c;
                        else
                            cellPwr.Value = Resources.Batt10_2c;
                    }
                }
                else
                {

                    //cellPwr.Value = global::WirelessGFTViewer.Properties.Resources.PowerOn1;//imageList.Images[19];
                    if (!dev.max17047Exist)
                    {
                        if (dev.battLevel >= BATT_LEVEL_100PECENT)
                            cellPwr.Value = Resources.Batt100_2;//imageList.Images[20];
                        else if ((dev.battLevel >= BATT_LEVEL_90PECENT) && (dev.battLevel < BATT_LEVEL_100PECENT))
                            cellPwr.Value = Resources.Batt90_2;//imageList.Images[20];
                        else if ((dev.battLevel >= BATT_LEVEL_80PECENT) && (dev.battLevel < BATT_LEVEL_90PECENT))
                            cellPwr.Value = Resources.Batt80_2;
                        else if ((dev.battLevel >= BATT_LEVEL_70PECENT) && (dev.battLevel < BATT_LEVEL_80PECENT))
                            cellPwr.Value = Resources.Batt70_2;
                        else if ((dev.battLevel >= BATT_LEVEL_60PECENT) && (dev.battLevel < BATT_LEVEL_70PECENT))
                            cellPwr.Value = Resources.Batt60_2;
                        else if ((dev.battLevel >= BATT_LEVEL_50PECENT) && (dev.battLevel < BATT_LEVEL_60PECENT))
                            cellPwr.Value = Resources.Batt50_2;
                        else if ((dev.battLevel >= BATT_LEVEL_40PECENT) && (dev.battLevel < BATT_LEVEL_50PECENT))
                            cellPwr.Value = Resources.Batt40_2;
                        else if ((dev.battLevel >= BATT_LEVEL_30PECENT) && (dev.battLevel < BATT_LEVEL_40PECENT))
                            cellPwr.Value = Resources.Batt30_2;
                        else if ((dev.battLevel > BATT_LEVEL_20PECENT) && (dev.battLevel < BATT_LEVEL_30PECENT))
                            cellPwr.Value = Resources.Batt20_2;
                        else if ((dev.battLevel > BATT_LEVEL_10PECENT) && (dev.battLevel <= BATT_LEVEL_20PECENT))
                            cellPwr.Value = Resources.Batt10_2;
                        else
                            cellPwr.Value = Resources.Batt10_2;
                    }
                    else
                    {
                        if (dev.bat_Levelfuel >= F_BATT_LEVEL_100PECENT)
                            cellPwr.Value = Resources.Batt100_2;//imageList.Images[20];
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_90PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_100PECENT))
                            cellPwr.Value = Resources.Batt90_2;//imageList.Images[20];
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_80PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_90PECENT))
                            cellPwr.Value = Resources.Batt80_2;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_70PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_80PECENT))
                            cellPwr.Value = Resources.Batt70_2;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_60PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_70PECENT))
                            cellPwr.Value = Resources.Batt60_2;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_50PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_60PECENT))
                            cellPwr.Value = Resources.Batt50_2;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_40PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_50PECENT))
                            cellPwr.Value = Resources.Batt40_2;
                        else if ((dev.bat_Levelfuel >= F_BATT_LEVEL_30PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_40PECENT))
                            cellPwr.Value = Resources.Batt30_2;
                        else if ((dev.bat_Levelfuel > F_BATT_LEVEL_20PECENT) && (dev.bat_Levelfuel < F_BATT_LEVEL_30PECENT))
                            cellPwr.Value = Resources.Batt20_2;
                        else if ((dev.bat_Levelfuel > F_BATT_LEVEL_10PECENT) && (dev.bat_Levelfuel <= F_BATT_LEVEL_20PECENT))
                            cellPwr.Value = Resources.Batt10_2;
                        else
                            cellPwr.Value = Resources.Batt10_2;
                    }
                }
                tempBitmap[mRow] = (Bitmap)cellPwr.Value;
            }
            else
            {
                //cellPwr.Value = global::WirelessGFTViewer.Properties.Resources.PowerOff1;//imageList.Images[20];
                cellPwr.Value = Resources.BattOff_2;// Batt_off;// PowerOff1;//imageList.Images[20];                
                tempBitmap[mRow] = Resources.Batt50_2;
            }
            dgv.Rows[mRow].Cells[9] = cellPwr;
#endif

            DataGridViewCell cellImpact = new DataGridViewTextBoxCell();            
            cellImpact.Value = (dev.gForce).ToString("f2")+"g";
            dgv.Rows[mRow].Cells[3] = cellImpact;
            dgv.Rows[mRow].Cells[17].Value = dev.gForce; // For Sorting
            dgv.Rows[mRow].Cells[4].Value = (dev.Rotation).ToString("f1") + "°/s";
            dgv.Rows[mRow].Cells[18].Value = dev.Rotation; // For Sorting

            dgv.Rows[mRow].Cells[5].Value = dev.thresholdImpacts; // Threshold Impact Count
            dgv.Rows[mRow].Cells[6].Value = dev.impacts; // Total Hits
            if (true)
            {
                //dgv.Rows[mRow].Cells[8].Value = Math.Round(dev.cumulativeRPE,1);


            }
            else
            {
                dgv.Rows[mRow].Cells[8].Value = TimeSpan.FromSeconds(dev.activeTime / 3000).ToString();
            }
  

#if true
            DataGridViewCell cellMode = new DataGridViewTextBoxCell();   // Added by Jason Chen, 2014.01.20
            DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();

            float batFuel = dev.bat_Levelfuel;
            batFuel = batFuel * 0.625f / 1000f;
            String BatVol = string.Format("{0,4:f2}", batFuel)+"V";
            string lastDevMode = "";

            if (dgv.Rows[mRow].Cells[16].Value != null) lastDevMode = dgv.Rows[mRow].Cells[16].Value.ToString();
            if (dev.mode >= 1) {
                if (dev.lastThresholdTime > dev.firstReceiveTime && (dev.reviewYellow || dev.reviewRed))
                {
                    dgv.Rows[mRow].Cells[16].Value = "3";
                    dgv.Rows[mRow].Cells[16].Value += dev.lastThresholdTime.Month.ToString().PadLeft(2, '0');
                    dgv.Rows[mRow].Cells[16].Value += dev.lastThresholdTime.Day.ToString().PadLeft(2, '0');
                    dgv.Rows[mRow].Cells[16].Value += dev.lastThresholdTime.Hour.ToString().PadLeft(2, '0');
                    dgv.Rows[mRow].Cells[16].Value += dev.lastThresholdTime.Minute.ToString().PadLeft(2, '0');
                    dgv.Rows[mRow].Cells[16].Value += dev.lastThresholdTime.Second.ToString().PadLeft(2, '0');
                    dgv.Rows[mRow].Cells[16].Value += (300 - dev.playerNumer).ToString().PadLeft(3, '0');//Math.Round((DateTime.UtcNow - dev.lastThresholdTime).TotalSeconds)
                } else {
                    dgv.Rows[mRow].Cells[16].Value = "1" + (300 - dev.playerNumer).ToString().PadLeft(13, '0');
                }
            } else
                dgv.Rows[mRow].Cells[16].Value = "0" + dev.playerNumer.ToString().PadLeft(13, '0'); //dgv.Rows[mRow].Cells[12].Value = "9999"; //

            string powerMode = "";
            string commandStatus = "";
            if (dev.mode == 1)
            {
                powerMode = "On (" + dev.battLevel.ToString() + "%)";
                if (dev.stopRecording)
                {
                    powerMode = "On " + "(Not rec.)";
                }
                cellStyle.ForeColor = Color.Black;// Color.Green;
            }
            else if (dev.mode == 0x81)
            {
                powerMode = "On (Charging " + dev.battLevel.ToString() + "%)";
                cellStyle.ForeColor = Color.Black;//Color.Green;
            }
            else if (dev.mode == 0)
            {
                powerMode = "Off (" + dev.battLevel.ToString() + "%)";
                cellStyle.ForeColor = Color.Black;//Color.Red;
                dev.processCmds();
            }
            else if (dev.mode == 2)
            {
                powerMode = "Sleep (" + dev.battLevel.ToString() + "%)";
                cellStyle.ForeColor = Color.Black;//Color.YellowGreen;

                DataGridViewCell cellPwr1 = new DataGridViewImageCell();
                cellPwr1.Value = tempBitmap[mRow];
                dgv.Rows[mRow].Cells[12] = cellPwr1;

                dev.processCmds();

            }
            else if (dev.mode == 3)
            {
                powerMode = "Erasing...";
                cellStyle.ForeColor = Color.Black;
            }
            else if (dev.mode == 10)
            {
                
                if (dev.recordEnabled) powerMode = "On";
                else powerMode = "On " + "(Not rec.)";
                cellStyle.ForeColor = Color.Black;
                dev.stopRecording = false;
            }
            else if (dev.mode == 11)
            {
                powerMode = "Sleep " + "(Not rec.)";
                cellStyle.ForeColor = Color.Black;
                dev.stopRecording = true;
                dev.processCmds();
            }
            else
            {
                powerMode = "Downloading...";
                cellStyle.ForeColor = Color.Black;
                downloadingFlag = true;
            }
            if ((dev.mode == 1 || dev.mode == 10) && dev.lastReceiveTime > DateTime.UtcNow.AddSeconds(-3))
            {
                powerMode = "Receiving...";
            }
            Font cellStyleSmall = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            if (dev.startRecordingFlag && dev.stopRecording)
            {
                commandStatus += "\nStart Recording Pending";
                //cellMode.Value += "\nStart Recording Pending";
                cellStyle.Font = cellStyleSmall;
            }
            if (dev.stopRecordingFlag && dev.mode != 11 && dev.stopRecording != true) 
            {
                commandStatus += "\nStop Recording Pending";
                //cellMode.Value += "\nStop Recording Pending";
                cellStyle.Font = cellStyleSmall;
            }
            if (dev.powerOffSent == false && dev.mode != 0)
            {
                commandStatus += "\nPower Off Pending";
                //cellMode.Value += "\nPower Off Pending";
                cellStyle.Font = cellStyleSmall;
            }
            if (dev.getImpactsSent == false)
            {
                commandStatus += "\nView All Impacts Pending";
                //cellMode.Value += "\nView All Impacts Pending";
                cellStyle.Font = cellStyleSmall;
            }
            cellMode.Value = powerMode + commandStatus;
            //cellStyle.Font =Da
#if ENABLE_DASHBOARD

            dev.commandStatus = commandStatus;
            dev.status = powerMode;
            //if (dev.mode >= 1 && dev.lastPingTime < DateTime.UtcNow.AddMinutes(-3))
            //{ // No communication from device in last 3 minutes
            //    if (!dev.max17047Exist && false)
            //    {
            //        /*if (dev.battLevel >= BATT_LEVEL_10PECENT) dev.status = "Device Out of Range";
            //        else dev.status = "Battery Drained";*/
            //        if (dev.battLevel < BATT_LEVEL_10PECENT) dev.status = "Battery Drained";
            //        else dev.status = "Out of Range";
            //    }
            //    else
            //    {
            //        /*if (dev.bat_Levelfuel >= F_BATT_LEVEL_10PECENT) dev.status = "Device Out of Range";
            //        else dev.status = "Battery Drained";*/
            //        if (dev.bat_Levelfuel < F_BATT_LEVEL_10PECENT) dev.status = "Battery Drained";
            //        else dev.status = "Out of Range";
            //    }
            //}
            simplePlayerCell cellPlayer = new simplePlayerCell();
            dgv.Rows[mRow].Cells[2] = cellPlayer;
            cellPlayer.mode = dev.mode;
            cellPlayer.Image = dev.batImg;
            cellPlayer.playerName = new string(dev.name);
            cellPlayer.playerNumber = dev.playerNumer.ToString();
            cellPlayer.gftStatus = dev.status;
            cellPlayer.commandStatus = commandStatus;
            cellPlayer.reviewYellow = dev.reviewYellow;
            cellPlayer.reviewRed = dev.reviewRed;
            batteryStatusCell cellBatteryStatus = new batteryStatusCell();
            dgv.Rows[mRow].Cells[12] = cellBatteryStatus;
            cellBatteryStatus.mode = dev.mode;
            cellBatteryStatus.Image = dev.batImg;
            cellBatteryStatus.gftStatus = dev.status;
            cellBatteryStatus.commandStatus = commandStatus;
            cellBatteryStatus.battery_pct = dev.battLevel;
            //cellPwr.Value = cellName.Value = new string(dev.name) + "\r\n" + "#" + dev.playerNumer + ", GFT:"; //"Player Name\r\n#90 GFT: Power On";

            //dgv.Rows[mRow].Cells[12] = cellMode;
           // dgv.Rows[mRow].Cells[12].Style = cellStyle;
#else
            dgv.Rows[mRow].Cells[8] = cellMode;
            dgv.Rows[mRow].Cells[8].Style = cellStyle;
#endif


            DataGridViewCell cellCheck = new DataGridViewCheckBoxCell();
            cellCheck.Value = dev.upldFlag;
            cellCheck.ToolTipText = "Click to Enable Download";
#if ENABLE_DASHBOARD
            dgv.Rows[mRow].Cells[14] = cellCheck;
#else
            dgv.Rows[mRow].Cells[10] = cellCheck;
#endif

#else
            DataGridViewCell cellButton = new DataGridViewButtonCell();   // Added by Jason Chen, 2014.02.04
            DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
            if (dev.mode == 1)
            {
                cellButton.Value = "Power On ";
                cellButton.Style.ForeColor = Color.Lime;
                cellStyle.ForeColor = Color.Lime;
            }
            else if (dev.mode == 0)
            {
                cellButton.Value = "Power Off";
                cellButton.Style.ForeColor = Color.Red;
                cellStyle.ForeColor = Color.Red;
            }
            else
            {
                cellButton.Value = "Sleep    ";
                cellButton.Style.ForeColor = Color.YellowGreen;
                cellStyle.ForeColor = Color.YellowGreen;

                DataGridViewCell cellAlarm1 = new DataGridViewImageCell();
                cellAlarm1.Value = global::WirelessGFTViewer.Properties.Resources.PowerOn1;//imageList.Images[23];//global::WirelessGFTViewer.Properties.Resources.user_green;
                dgv.Rows[mRow].Cells[5] = cellAlarm1;

                DataGridViewCell cellPwr1 = new DataGridViewImageCell();
                cellPwr1.Value = tempBitmap[mRow];
                dgv.Rows[mRow].Cells[4] = cellPwr1;

            }
            //cellStyle.Font =Da
            cellButton.Style.BackColor = Color.YellowGreen;
            cellButton.ToolTipText = "Click Upload";
            dgv.Rows[mRow].Cells[3] = cellButton;
            dgv.Rows[mRow].Cells[3].Style = cellStyle;          
#endif

            /*            if (dev.mode == 1)
            {
                dgv.Rows[mRow].Cells[3].Style.ForeColor = Color.Lime;
            }
            else if (dev.mode == 0)
            {
                dgv.Rows[mRow].Cells[3].Style.ForeColor = Color.Red;
            }
            else
            {
                dgv.Rows[mRow].Cells[3].Style.ForeColor = Color.YellowGreen;
            }
            */

            //DataGridViewCell cellAlarmNew = new DataGridViewImageCell();
            //if (dev.alarm)
            //    cellAlarmNew.Value = global::WirelessGFTViewer.Properties.Resources.PowerOff1;//imageList.Images[22];// global::WirelessGFTViewer.Properties.Resources.user_red;
            //else if (dev.live)
            //    cellAlarmNew.Value = global::WirelessGFTViewer.Properties.Resources.PowerOn1;//imageList.Images[23];//global::WirelessGFTViewer.Properties.Resources.user_green;
            //else
            //    cellAlarmNew.Value = global::WirelessGFTViewer.Properties.Resources.user_gray; //imageList.Images[24];//global::WirelessGFTViewer.Properties.Resources.user_gray;
            //dgv.Rows[mRow].Cells[5] = cellAlarmNew;

            //DataGridViewCell cellImpact = new DataGridViewTextBoxCell();
            //cellImpact.Value = (dev.gForce).ToString("f2");
            //dgv.Rows[mRow].Cells[5] = cellImpact;


            //DataGridViewCell temperature = new DataGridViewTextBoxCell();
            //temperature.Value = ((int)(dev.temperature)).ToString();
            //dgv.Rows[mRow].Cells[6] = temperature;

            //selectRow(mRow);
            // don't need do this sort during downloading, 2014.10.30
            ///////////////////////////////////////////////////////////////////////////
#if ENABLE_DASHBOARD
            if (!downloadingFlag)
            {
                if (dgv.Rows[mRow].Cells[16].Value != null)
                {
                    if (lastDevMode != dgv.Rows[mRow].Cells[16].Value.ToString())
                    {
                        dgv.Sort(dgv.Columns[16], ListSortDirection.Descending);

                    }
                }
            }
#else
            if (!downloadingFlag)                      
            {
                if (dgv.Rows[mRow].Cells[12].Value != null)
                {
                    if (lastDevMode != dgv.Rows[mRow].Cells[12].Value.ToString())
                    {
                        dgv.Sort(dgv.Columns[12], ListSortDirection.Ascending);
                    }
                }
            }
#endif
            ////////////////////////////////////////////////////////////////////////////
        }

        internal byte isNodeBinded(ref byte[] mid)
        {
            byte nodeID = 0;
            foreach (LP_DEVICE dev in lpDevices)
                if (dev.MID.SequenceEqual(mid))
                {
                    nodeID = dev.nodeID;
                    break;
                }
            return nodeID;
        }
#if false
            internal void processImmediate(LP_DATA_PKT lpDataPkt)
            {
                foreach (LP_DEVICE dev in lpDevices)
                    if (dev.nodeID == lpDataPkt.nodeID)
                    {
                        dev.alarm = true;
                        dev.gForce = lpDataPkt.load[1];

                    }
                dgvLpPopulateView();
            }

            internal void processHitData(LP_DATA_PKT lpDataPkt)
            {
                foreach (LP_DEVICE dev in lpDevices)
                {
                    if (dev.nodeID == lpDataPkt.nodeID)
                    {
                        dev.alarm = (lpDataPkt.load[0] == 1);
                        dev.gForce = (int)Math.Sqrt(lpDataPkt.load[1] + lpDataPkt.load[2] * 256);
                    }
                }
                dgvLpPopulateView();
            }
            /// <summary>
            /// processing uploaded data
            /// </summary>
            /// <param name="lpDataPkt"></param>
            internal void processUploadData(LP_DATA_PKT lpDataPkt)
            {
                foreach (LP_DEVICE dev in lpDevices)
                {
                    if (dev.nodeID == lpDataPkt.nodeID)
                    {
                        ;
                    }
                }
            }

            internal void liveChanged(LP_DATA_PKT lpDataPkt)
            {
                foreach (LP_DEVICE dev in lpDevices)
                    if (dev.nodeID == lpDataPkt.nodeID)
                    {
                        if (lpDataPkt.type == (byte)gLpMsgType.CHUNK_POWER_ON)
                        {
                            if ((lpDataPkt.seq == 0) && (lpDataPkt.len == 0)) break;
                            else
                            {
                                if (lpDataPkt.seq == 0)
                                {
                                    //dev.name = mDev.name;
                                    dev.live = true;
                                    if (lpDataPkt.len - 1 > 0)
                                        Array.Copy(lpDataPkt.load, 1, dev.name, 0, lpDataPkt.len - 1);
                                    break;
                                }
                                else
                                {
                                    // Array.Copy(lpDataPkt.load, 1, dev.name, 0, lpDataPkt.len - 1);

                                }
                            }
                        }
                        else // power off
                        {
                            dev.live = false;
                        }
                    }
                dgvLpPopulateView();
            }
#endif
    }

    #endregion
}


﻿using Microsoft.Win32.SafeHandles;
using Microsoft.VisualBasic;
using System.ComponentModel;
//using ExcelLibrary.SpreadSheet;
//using ExcelLibrary.CompoundDocumentFormat;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Collections;
using System.Threading;
using System;
using System.Timers;
using System.Globalization;
using ArtaFlexHidWin;


namespace gForceTrackerWireless
{

    public partial class Form1 : UsbAwareForm
    {
        private double getMax(Double[] my40)
        {

            int i = 0;
            int j = 0;
            double tHic = 0;
            double hic = 0;
            for (i = 0; i < 26; i++)
            {
                tHic = 0;
                for (j = 0; j < 15; j++) tHic += my40[i + j];
                tHic = 15 * Math.Pow((tHic) / 15, 2.5);
                if (hic < tHic) hic = tHic;
            }
            return hic / 1000;
        }
        /* max[(t2-t1) (accum (atdt)/(t2-t1))^2.5]
         * 
         */
        private double HIC15(Double[] my40)
        {
            int i = 0;
            int j = 0;
            double tHic = 0;
            double hic = 0;
            for (i = 0; i < 26; i++)
            {
                tHic = 0;
                for (j = 0; j < 15; j++)
                    tHic += my40[i + j];
                tHic = 15 * Math.Pow((tHic) / 15, 2.5);
                if (hic < tHic) hic = tHic;
            }
            return hic / 1000;
        }
        private double GSI(Double[] my40)
        {
            int i = 0;
            double gsi = 0;
            // gsi = Accumulation a(t)power2.5dt (in impact duration)
            for (i = 0; i < 40; i++) gsi += Math.Pow(my40[i], 2.5);
            return gsi / 1000;
        }

        //PCS =  10 * ( [ 0.4718 * sGSI + 0.4742 * sHIC + 0.4336 * sLIN + 0.2164 * sROT ] + 2 )
        private double HITsp(double gsi, double hic, double lin, double rot)
        {
            //this need rotation acceleration to calculate
            return 10 * ((0.4718 * gsi + 0.4742 * hic + 0.4336 * lin + 0.2164 * rot) + 2);
        }
        /*
         * Quadrant I:
           azimuth = arctan (delta x/delta y)
           Quadrant II:
           azimuth = arctan (delta x / delta y) + 360°
           Quadrant III or IV: azimuth = arctan (delta x / delta y)  + 180°
          y
         *|
         *|
       z *x____________ x
         *
         * y is zero degree clockwise
         *
         */

        private double[] getAzmuthJaw(Double[] myPoint)
        {
            //map to desired axis
            double x = myPoint[0];
            double y = myPoint[1];
            double z = myPoint[2];
            double[] myResult = new double[2];
            //int i = 0;
            double azmuth = 0;
            double elevation = 0;
            double res = Math.Sqrt(x * x + y * y);

            if (y > 0 && x > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180;
            else if (x < 0 && y > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180 + 360;
            else if (x != 0 && y < 0) azmuth = 180 + (Math.Atan(x / y) / Math.PI) * 180;
            else if (y == 0)
            {
                if (x > 0) azmuth = 90;
                else if (x < 0) azmuth = 270;
                else azmuth = 0;
            }
            else if (x == 0)
            {
                if (y > 0) azmuth = 0;
                else if (y < 0) azmuth = 180;
                else azmuth = 0;
            }
            if (x == 0 && y == 0) elevation = z > 0 ? -90 : z == 0 ? 0 : 90;
            if (z < 0) elevation = (Math.Atan(Math.Abs(z / res)) / Math.PI) * 180;
            else if (z > 0) elevation = (-Math.Atan(Math.Abs(z / res)) / Math.PI) * 180;
            else elevation = 0;

            myResult[0] = azmuth;
            myResult[1] = elevation;

            return myResult;
        }


        private double[] getAzmuthBack(Double[] myPoint)
        {
            //map to desired axis
            double z = myPoint[0];
            double y = myPoint[1];
            double x = myPoint[2];

            double[] myResult = new double[2];
            //int i = 0;
            double azmuth = 0;
            double elevation = 0;
            double res = Math.Sqrt(x * x + y * y);

            if (y > 0 && x > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180;
            else if (x < 0 && y > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180 + 360;
            else if (x != 0 && y < 0) azmuth = 180 + (Math.Atan(x / y) / Math.PI) * 180;
            else if (y == 0)
            {
                if (x > 0) azmuth = 90;
                else if (x < 0) azmuth = 270;
                else azmuth = 0;
            }
            else if (x == 0)
            {
                if (y > 0) azmuth = 0;
                else if (y < 0) azmuth = 180;
                else azmuth = 0;
            }
            if (x == 0 && y == 0) elevation = z > 0 ? 90 : z == 0 ? 0 : -90;

            if (z > 0) elevation = (Math.Atan(z / res) / Math.PI) * 180;
            else if (z < 0) elevation = (Math.Atan(z / res) / Math.PI) * 180;
            else elevation = 0;

            myResult[0] = azmuth;
            myResult[1] = elevation;

            return myResult;
        }

        private double[] getAzmuthYasZ(Double[] myPoint)
        {
            //map to desired axis
            double x = myPoint[0];
            double z = myPoint[1];
            double y = myPoint[2];

            double[] myResult = new double[2];
            //int i = 0;
            double azmuth = 0;
            double elevation = 0;
            double res = Math.Sqrt(x * x + y * y);

            if (y > 0 && x > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180;
            else if (x < 0 && y > 0) azmuth = (Math.Atan(x / y) / Math.PI) * 180 + 360;
            else if (x != 0 && y < 0) azmuth = 180 + (Math.Atan(x / y) / Math.PI) * 180;
            else if (y == 0)
            {
                if (x > 0) azmuth = 90;
                else if (x < 0) azmuth = 270;
                else azmuth = 0;
            }
            else if (x == 0)
            {
                if (y > 0) azmuth = 0;
                else if (y < 0) azmuth = 180;
                else azmuth = 0;
            }
            if (x == 0 && y == 0) elevation = z > 0 ? 90 : z == 0 ? 0 : -90;

            if (z > 0) elevation = (Math.Atan(z / res) / Math.PI) * 180;
            else if (z < 0) elevation = (Math.Atan(z / res) / Math.PI) * 180;
            else elevation = 0;

            myResult[0] = azmuth;
            myResult[1] = elevation;

            return myResult;
        }

        private void myExportSessionRawData(Int32 mSessionID)
        {
            int i;
            byte[] dataX;
            byte[] dataY;
            byte[] dataZ;

            byte[] rdataX;
            byte[] rdataY;
            byte[] rdataZ;
            double mDataX, mDataY, mDataZ, mDataR;
            DateTime mtime;
            DateTime tmpTime;
            int millisecond;
            //create new xls file
            string file = mSessionID.ToString();
            file = file + ".xlsx";

            //System.IO.Stream myStream;
            FileInfo newFile = new FileInfo(file);
            //using(myStream = new System.IO.FileStream(file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Properties.Author = "gForceTracker";
                p.Workbook.Properties.Title = "gft export data";
                p.Workbook.Properties.Company = "GForce";

                p.Workbook.Worksheets.Add("linear");
                p.Workbook.Worksheets.Add("gyro");
                ExcelWorksheet ws = p.Workbook.Worksheets[1]; // 1 is the position of the worksheet
                ws.Name = "Linear";
                ExcelWorksheet ws2 = p.Workbook.Worksheets[2]; // 1 is the position of the worksheet
                ws2.Name = "Gyro";
                // int rowIndex = 1;
                //int colIndex = 1;                               
                // Column indexes for clarity
                // int timeIndex = 1;
                //  int xIndex = 2;
                //  int yIndex = 3;
                //  int zIndex = 4;
                // int rIndex = 5;
                Byte[] bin = p.GetAsByteArray();
                File.WriteAllBytes(file, bin);
                // p.Stream.Flush();
            }
            int row = 2;
            int col = 1;
            int rowg = 2;
            int colg = 1;
            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
                // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                //int num = 5;
                //int uID = 1;
#if FALSE             
                SqlCeCommand command = new SqlCeCommand("SELECT  dataX FROM Events WHERE ", con);
                byte[] buffer = (byte[])command.ExecuteScalar();
                con.Close();
                return buffer;
#endif
                // create session header
                SqlCeCommand cmd = new SqlCeCommand("Select * from Session WHERE SID LIKE @mySID", con);
                cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;

                SqlCeDataReader rdr = cmd.ExecuteReader();
                //using (myStream = new System.IO.FileStream(file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
                using (ExcelPackage p = new ExcelPackage(newFile))
                {
                    //ExcelWorksheet ws = p.Workbook.Worksheets[1];
                    ExcelWorksheet ws = p.Workbook.Worksheets["Linear"];
                    ExcelWorksheet wsg = p.Workbook.Worksheets["Gyro"];
                    var newCell = ws.Cells[row, col];

                    while (rdr.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        //Session Info
                        //Setting Value in cell
                        newCell = ws.Cells[row, col++];
                        newCell.Value = "Session ID";
                        newCell = ws.Cells[row, col++];
                        newCell.Value = "UID";

                        col = 1;
                        row++;

                        newCell = ws.Cells[row, col++];
                        newCell.Value = rdr.GetInt32(0);
                        newCell = ws.Cells[row, col++];
                        newCell.Value = rdr.GetInt32(1);
                        //worksheet.Cells[row, col++] = new Cell(rdr.GetString(1));
                    }

                    //impacts
                    row++;
                    col = 1;
                    do
                    {
                        // Set the background colours
                        var cell = ws.Cells[row, col++];
                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(Color.GreenYellow);
                    } while (col != 6);
                    col = 1;
                    // Set the cell values
                    var cell_time = ws.Cells[row, col++];
                    var cell_x = ws.Cells[row, col++];
                    var cell_y = ws.Cells[row, col++];
                    var cell_z = ws.Cells[row, col++];
                    var cell_r = ws.Cells[row, col++];
                    cell_time.Value = "time";
                    cell_x.Value = "X";
                    cell_y.Value = "Y";
                    cell_z.Value = "Z";
                    cell_r.Value = "Resultant";

                    //gyro
                    //ws.InsertRow
                    wsg.Cells[rowg, colg++].Value = "Time";
                    //y
                    wsg.Cells[rowg, colg++].Value = "X";
                    //z
                    wsg.Cells[rowg, colg++].Value = "Y";
                    //r
                    wsg.Cells[rowg, colg++].Value = "Z";

                    wsg.Cells[rowg, colg++].Value = "Resultant";
                    p.Save();
                    p.Dispose();
                }
                row++;
                cmd = new SqlCeCommand("select EID, IMPACT_TIME, LINEAR_ACC_RESULTANT, AZIMUTH,ELEVATION,dataX,dataY,dataZ,rdataX,rdataY,rdataZ,millisecond from Events where SID = @mySID", con);
                cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;

                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    //using (myStream = new System.IO.FileStream(file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
                    using (ExcelPackage p = new ExcelPackage(newFile))
                    {
                        //ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        ExcelWorksheet wsg = p.Workbook.Worksheets["Gyro"];
                        ExcelWorksheet ws = p.Workbook.Worksheets["Linear"];
                        var newCell = ws.Cells[row, col];
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        //Session Info

                        mtime = (DateTime)rdr["IMPACT_TIME"];
                        millisecond = (int)rdr["millisecond"];
                        tmpTime = new DateTime(mtime.Year, mtime.Month, mtime.Day, mtime.Hour, mtime.Minute, mtime.Second, millisecond);

                        //mtime.AddMilliseconds(millisecond - mtime.Millisecond);
                        // newCell = ws.Cells[row, col++];
                        // newCell.Value = tmpTime;
                        //worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");


                        dataX = (byte[])rdr["dataX"];
                        dataY = (byte[])rdr["dataY"];
                        dataZ = (byte[])rdr["dataZ"];

                        rdataX = (byte[])rdr["rdataX"];
                        rdataY = (byte[])rdr["rdataY"];
                        rdataZ = (byte[])rdr["rdataZ"];
                        //linear
                        col = 1;
                        row++;

                        ws.Column(1).Style.Numberformat.Format = "HH:mm:ss.000";
                        ws.Column(2).Style.Numberformat.Format = "#,##0.00";
                        ws.Column(3).Style.Numberformat.Format = "#,##0.00";
                        ws.Column(4).Style.Numberformat.Format = "#,##0.00";
                        ws.Column(5).Style.Numberformat.Format = "#,##0.00";
                        ws.InsertRow(row, row + 120);
                        for (i = 0; i < 120; i++)
                        {
                            mDataX = AccDecode(dataX[i]);
                            mDataY = AccDecode(dataY[i]);
                            mDataZ = AccDecode(dataZ[i]);
                            mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);
                            //time
                            // ws.InsertRow(row, row);
                            ws.Cells[row, col++].Value = tmpTime;
                            //x                    
                            //ws.InsertRow
                            ws.Cells[row, col++].Value = (float)mDataX;

                            //y
                            ws.Cells[row, col++].Value = (float)mDataY;
                            //z
                            ws.Cells[row, col++].Value = (float)mDataZ;
                            //r
                            ws.Cells[row, col++].Value = (float)mDataR;

                            //worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                            //worksheet.Cells[row, col++] = new Cell(mDataX, "#,##0.00");
                            //worksheet.Cells[row, col++] = new Cell(mDataY, "#,##0.00");
                            //worksheet.Cells[row, col++] = new Cell(mDataZ, "#,##0.00");
                            //worksheet.Cells[row, col++] = new Cell(mDataR, "#,##0.00");
                            tmpTime = tmpTime.AddTicks(3136);
                            col = 1;
                            row++;
                        }
                        colg = 1;
                        rowg++;
                        wsg.Column(1).Style.Numberformat.Format = "HH:mm:ss.000";
                        wsg.Column(2).Style.Numberformat.Format = "#,##0.00";
                        wsg.Column(3).Style.Numberformat.Format = "#,##0.00";
                        wsg.Column(4).Style.Numberformat.Format = "#,##0.00";
                        wsg.Column(5).Style.Numberformat.Format = "#,##0.00";
                        tmpTime = new DateTime(mtime.Year, mtime.Month, mtime.Day, mtime.Hour, mtime.Minute, mtime.Second, millisecond);
                        wsg.InsertRow(rowg, rowg + 32);

                        for (i = 0; i < 32; i++)
                        {
                            mDataX = gyroDecode(rdataX[i * 2], rdataX[i * 2 + 1]);
                            mDataY = gyroDecode(rdataY[i * 2], rdataY[i * 2 + 1]);
                            mDataZ = gyroDecode(rdataZ[i * 2], rdataZ[i * 2 + 1]);
                            mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);

                            //time
                            wsg.Cells[rowg, colg++].Value = tmpTime;
                            //x
                            wsg.Cells[rowg, colg++].Value = (float)mDataX;

                            //y
                            wsg.Cells[rowg, colg++].Value = (float)mDataY;
                            //z
                            wsg.Cells[rowg, colg++].Value = (float)mDataZ;
                            //r
                            wsg.Cells[rowg, colg++].Value = (float)mDataR;

                            tmpTime = tmpTime.AddTicks(10000000 / 800); //760 let's make 40 ms
                            colg = 1;
                            rowg++;
                        }
                        p.Save();
                        p.Dispose();
                        //p.Stream.Flush();
                        //p.Stream.Flush();                    
                    }
                }
            }
            System.Diagnostics.Process.Start(file);
            //workbook.Save(file);
            // p.Save();
            //myStream.Close();
#if FALSE 
                Byte[] bin = p.GetAsByteArray();
                try
                {
                    File.WriteAllBytes(file, bin);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please close exel file first then try again", "Output file is opened", MessageBoxButtons.OK);
                    //throw ex;
                }

#endif



#if FALSE
            int row = 2;
            int col = 0;
            //Session Info
            

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
                // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                //int num = 5;
                //int uID = 1;
        

           
#if FALSE             
                SqlCeCommand command = new SqlCeCommand("SELECT  dataX FROM Events WHERE ", con);
                byte[] buffer = (byte[])command.ExecuteScalar();
                con.Close();
                return buffer;
#endif
               // create session header
                SqlCeCommand cmd = new SqlCeCommand("Select * from Session WHERE SID LIKE @mySID" , con);
                cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;
       
                SqlCeDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    //SessionID = Convert.ToInt32(myReader["column1"]);
                     //Session Info
                    worksheet.Cells[row, col++] = new Cell("Session ID");
                    worksheet.Cells[row, col++] = new Cell("UID");
                    col = 0;
                    row++;
                    worksheet.Cells[row, col++] = new Cell(rdr.GetInt32(0));
                    worksheet.Cells[row, col++] = new Cell(rdr.GetInt32(1));
                    //worksheet.Cells[row, col++] = new Cell(rdr.GetString(1));
                    
                }

                //impacts
                row++;
                col = 0;
                worksheet.Cells[row, col++] = new Cell("Impact ID");
                worksheet.Cells[row, col++] = new Cell("Impact Time");
                col = 0;
                row++;
               
                 
                cmd = new SqlCeCommand("select EID, IMPACT_TIME, LINEAR_ACC_RESULTANT, AZIMUTH,ELEVATION,dataX,dataY,dataZ,rdataX,rdataY,rdataZ,millisecond from Events where SID = @mySID", con);
                cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;
                
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {

                    //SessionID = Convert.ToInt32(myReader["column1"]);
                    //Session Info
                    worksheet.Cells[row, col++] = new Cell( (Int32)rdr["EID"]);
                    mtime = (DateTime)rdr["IMPACT_TIME"];
                    millisecond = (int)rdr["millisecond"];
                    tmpTime  = new DateTime(mtime.Year, mtime.Month, mtime.Day, mtime.Hour, mtime.Minute, mtime.Second,millisecond);
                   
                    //mtime.AddMilliseconds(millisecond - mtime.Millisecond);
                    worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                    

                    dataX = (byte[])rdr["dataX"];
                    dataY = (byte[])rdr["dataY"];
                    dataZ = (byte[])rdr["dataZ"];

                    rdataX = (byte[])rdr["rdataX"];
                    rdataY = (byte[])rdr["rdataY"];
                    rdataZ = (byte[])rdr["rdataZ"];
                    //linear
                    col = 0;
                    row++;
                    worksheet.Cells[row, col++] = new Cell("Time");
                    worksheet.Cells[row, col++] = new Cell("LinX");
                    worksheet.Cells[row, col++] = new Cell("LinY");
                    worksheet.Cells[row, col++] = new Cell("LinZ");
                    worksheet.Cells[row, col++] = new Cell("LinRes");
                    col = 0;
                    row++;
                    
                    for (i = 0; i < 120; i++ )
                    {
                        mDataX = AccDecode(dataX[i]);
                        mDataY = AccDecode(dataY[i]);
                        mDataZ = AccDecode(dataZ[i]);
                        mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);

                        worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                        worksheet.Cells[row, col++] = new Cell(mDataX, "#,##0.00");
                        worksheet.Cells[row, col++] = new Cell(mDataY, "#,##0.00");
                        worksheet.Cells[row, col++] = new Cell(mDataZ, "#,##0.00");
                        worksheet.Cells[row, col++] = new Cell(mDataR, "#,##0.00");
                        tmpTime = tmpTime.AddTicks(3136);
                        col = 0;
                        row++;
                    }
                    worksheet.Cells[row, col++] = new Cell("Time");
                    worksheet.Cells[row, col++] = new Cell("RotX");
                    worksheet.Cells[row, col++] = new Cell("RotY");
                    worksheet.Cells[row, col++] = new Cell("RotZ");
                    worksheet.Cells[row, col++] = new Cell("RotRes");
                    col = 0;
                    row++;
                    //gyro

                    tmpTime = mtime;// =  (DateTime)rdr["IMPACT_TIME"];
                    for (i = 0; i < 32; i++)
                    {

                        mDataX = gyroDecode(rdataX[i * 2], rdataX[i * 2 + 1]);
                        mDataY = gyroDecode(rdataY[i * 2], rdataY[i * 2 + 1]);
                        mDataZ = gyroDecode(rdataZ[i * 2], rdataZ[i * 2 + 1]);
                        mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);
                        worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                        worksheet.Cells[row, col++] = new Cell(mDataX, "#,##");
                        worksheet.Cells[row, col++] = new Cell(mDataY, "#,##");
                        worksheet.Cells[row, col++] = new Cell(mDataZ, "#,##");
                        worksheet.Cells[row, col++] = new Cell(mDataR, "#,##0.00");

                        tmpTime = tmpTime.AddTicks(10000000 / 800); //760 let's make 40 ms
                        col = 0;
                        row++;
                    }
             
                }
                // worksheet.Cells.ColumnWidth[0, 3] = 20;
                workbook.Worksheets.Add(worksheet);
                try
                {
                    //workbook.Save(file);
                     string file = mSessionID.ToString();
                     file = file + ".xlsx";
                     Byte[] bin = p.GetAsByteArray();
                     File.WriteAllBytes(file, bin);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please close exel file first then try again", "Output file is opened", MessageBoxButtons.OK);
                    //throw ex;
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
#endif
        }

        private void myExportSessionRawData2(Int32 mSessionID)
        {
            int i;
            byte[] dataX;
            byte[] dataY;
            byte[] dataZ;
            //byte insertNo = 0;
            byte[] rdataX;
            byte[] rdataY;
            byte[] rdataZ;
            double mDataX, mDataY, mDataZ, mDataR;
            DateTime mtime;
            DateTime tmpTime;
            int millisecond;
            //create new xls file
            string file = mSessionID.ToString();
            file = "c:\\gftreports\\" + file + ".xlsx";

            //System.IO.Stream myStream;
            FileInfo newFile = new FileInfo(file);
            //using(myStream = new System.IO.FileStream(file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Properties.Author = "gForceTracker";
                p.Workbook.Properties.Title = "gft export data";
                p.Workbook.Properties.Company = "GForce";

                p.Workbook.Worksheets.Add("linear");
                p.Workbook.Worksheets.Add("gyro");
                ExcelWorksheet ws = p.Workbook.Worksheets[1]; // 1 is the position of the worksheet
                ws.Name = "Linear";
                ExcelWorksheet wsg = p.Workbook.Worksheets[2]; // 1 is the position of the worksheet
                wsg.Name = "Gyro";
                // int rowIndex = 1;
                //int colIndex = 1;

                // Column indexes for clarity
                // int timeIndex = 1;
                //  int xIndex = 2;
                //  int yIndex = 3;
                //  int zIndex = 4;
                // int rIndex = 5;
                // p.Stream.Flush();           
                int row = 1;
                int col = 1;

                int rowg = 1;
                int colg = 1;
                using (SqlCeConnection con = new SqlCeConnection(conString))
                {
                    con.Open();
                    // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                    //int num = 5;
                    //int uID = 1;
#if FALSE             
                SqlCeCommand command = new SqlCeCommand("SELECT  dataX FROM Events WHERE ", con);
                byte[] buffer = (byte[])command.ExecuteScalar();
                con.Close();
                return buffer;
#endif
                    // create session header
                    SqlCeCommand cmd = new SqlCeCommand("Select * from Session WHERE SID LIKE @mySID", con);
                    cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;

                    SqlCeDataReader rdr = cmd.ExecuteReader();
                    //using (myStream = new System.IO.FileStream(file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))

#if FALSE
                    while (rdr.Read())
                    {
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        //Session Info

                        //Setting Value in cell

                        ws.Cells[row, col++] .Value = "Session ID";
                        ws.Cells[row, col++].Value = "UID";

                        col = 1;
                        row++;

                        ws.Cells[row, col++].Value = rdr.GetInt32(0);
                        ws.Cells[row, col++].Value = rdr.GetInt32(1);

                        //worksheet.Cells[row, col++] = new Cell(rdr.GetString(1));

                    }
#endif
                    //impacts
                    row++;
                    col = 1;
                    do
                    {
                        // Set the background colours
                        var cell = ws.Cells[row, col++];
                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(Color.GreenYellow);
                    } while (col != 6);
                    col = 1;
                    // Set the cell values
                    var cell_time = ws.Cells[row, col++];
                    var cell_x = ws.Cells[row, col++];
                    var cell_y = ws.Cells[row, col++];
                    var cell_z = ws.Cells[row, col++];
                    var cell_r = ws.Cells[row, col++];
                    cell_time.Value = "time";
                    cell_x.Value = "X";
                    cell_y.Value = "Y";
                    cell_z.Value = "Z";
                    cell_r.Value = "Resultant";

                    //gyro
                    rowg++;
                    colg = 1;
                    do
                    {
                        // Set the background colours
                        var cell = wsg.Cells[rowg, colg++];
                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(Color.LightBlue);

                    } while (colg != 6);
                    colg = 1;
                    //ws.InsertRow
                    wsg.Cells[rowg, colg++].Value = "Time";
                    //y
                    wsg.Cells[rowg, colg++].Value = "X";
                    //z
                    wsg.Cells[rowg, colg++].Value = "Y";
                    //r
                    wsg.Cells[rowg, colg++].Value = "Z";

                    wsg.Cells[rowg, colg++].Value = "Resultant";
                    row++;
                    rowg++;
                    cmd = new SqlCeCommand("select EID, IMPACT_TIME, LINEAR_ACC_RESULTANT, AZIMUTH,ELEVATION,dataX,dataY,dataZ,rdataX,rdataY,rdataZ,millisecond from Events where SID = @mySID", con);
                    cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;

                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        //using (myStream = new System.IO.FileStream(file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))                
                        //SessionID = Convert.ToInt32(myReader["column1"]);
                        //Session Info

                        mtime = (DateTime)rdr["IMPACT_TIME"];
                        millisecond = (int)rdr["millisecond"];
                        tmpTime = new DateTime(mtime.Year, mtime.Month, mtime.Day, mtime.Hour, mtime.Minute, mtime.Second, millisecond);

                        //mtime.AddMilliseconds(millisecond - mtime.Millisecond);
                        // newCell = ws.Cells[row, col++];
                        // newCell.Value = tmpTime;
                        //worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                        dataX = (byte[])rdr["dataX"];
                        dataY = (byte[])rdr["dataY"];
                        dataZ = (byte[])rdr["dataZ"];

                        rdataX = (byte[])rdr["rdataX"];
                        rdataY = (byte[])rdr["rdataY"];
                        rdataZ = (byte[])rdr["rdataZ"];
                        //linear
                        col = 1;
                        // row++;                                
                        ws.InsertRow(row, row + 120);
                        for (i = 0; i < 120; i++)
                        {
                            mDataX = AccDecode(dataX[i]);
                            mDataY = AccDecode(dataY[i]);
                            mDataZ = AccDecode(dataZ[i]);
                            mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);
                            //time
                            // ws.InsertRow(row, row);
                            ws.Cells[row, col++].Value = tmpTime;
                            //x                    
                            //ws.InsertRow
                            ws.Cells[row, col++].Value = (float)mDataX;

                            //y
                            ws.Cells[row, col++].Value = (float)mDataY;
                            //z
                            ws.Cells[row, col++].Value = (float)mDataZ;
                            //r
                            ws.Cells[row, col++].Value = (float)mDataR;


                            //worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                            //worksheet.Cells[row, col++] = new Cell(mDataX, "#,##0.00");
                            //worksheet.Cells[row, col++] = new Cell(mDataY, "#,##0.00");
                            //worksheet.Cells[row, col++] = new Cell(mDataZ, "#,##0.00");
                            //worksheet.Cells[row, col++] = new Cell(mDataR, "#,##0.00");
                            tmpTime = tmpTime.AddTicks(3136);
                            col = 1;
                            row++;
                        }
                        colg = 1;
                        // rowg++;

                        tmpTime = new DateTime(mtime.Year, mtime.Month, mtime.Day, mtime.Hour, mtime.Minute, mtime.Second, millisecond);

                        wsg.InsertRow(rowg, rowg + 32);
                        for (i = 0; i < 32; i++)
                        {

                            mDataX = gyroDecode(rdataX[i * 2], rdataX[i * 2 + 1]);
                            mDataY = gyroDecode(rdataY[i * 2], rdataY[i * 2 + 1]);
                            mDataZ = gyroDecode(rdataZ[i * 2], rdataZ[i * 2 + 1]);
                            mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);

                            //time
                            wsg.Cells[rowg, colg++].Value = tmpTime;
                            //x
                            wsg.Cells[rowg, colg++].Value = (float)mDataX;

                            //y
                            wsg.Cells[rowg, colg++].Value = (float)mDataY;
                            //z
                            wsg.Cells[rowg, colg++].Value = (float)mDataZ;
                            //r
                            wsg.Cells[rowg, colg++].Value = (float)mDataR;

                            tmpTime = tmpTime.AddTicks(10000000 / 800); //760 let's make 40 ms
                            colg = 1;
                            rowg++;
                        }

                    }
                    ws.Column(1).Style.Numberformat.Format = "HH:mm:ss.000";
                    ws.Column(1).Width = 20;
                    ws.Column(2).Style.Numberformat.Format = "#,##0.00";
                    ws.Column(3).Style.Numberformat.Format = "#,##0.00";
                    ws.Column(4).Style.Numberformat.Format = "#,##0.00";
                    ws.Column(5).Style.Numberformat.Format = "#,##0.00";

                    wsg.Column(1).Style.Numberformat.Format = "HH:mm:ss.000";
                    wsg.Column(1).Width = 20;
                    wsg.Column(2).Style.Numberformat.Format = "#,##0.00";
                    wsg.Column(3).Style.Numberformat.Format = "#,##0.00";
                    wsg.Column(4).Style.Numberformat.Format = "#,##0.00";
                    wsg.Column(5).Style.Numberformat.Format = "#,##0.00";
                }//connection
                Byte[] bin = p.GetAsByteArray();
                File.WriteAllBytes(file, bin);
                p.Dispose();
            } //excel package
            System.Diagnostics.Process.Start(file);
            //workbook.Save(file);
            // p.Save();
            //myStream.Close();
#if FALSE 
                Byte[] bin = p.GetAsByteArray();
                try
                {
                    File.WriteAllBytes(file, bin);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please close exel file first then try again", "Output file is opened", MessageBoxButtons.OK);
                    //throw ex;
                }

#endif



#if FALSE
            int row = 2;
            int col = 0;
            //Session Info
            

            using (SqlCeConnection con = new SqlCeConnection(conString))
            {
                con.Open();
                // Insert into the SqlCe table. ExecuteNonQuery is best for inserts.
                //int num = 5;
                //int uID = 1;
        

           
#if FALSE             
                SqlCeCommand command = new SqlCeCommand("SELECT  dataX FROM Events WHERE ", con);
                byte[] buffer = (byte[])command.ExecuteScalar();
                con.Close();
                return buffer;
#endif
               // create session header
                SqlCeCommand cmd = new SqlCeCommand("Select * from Session WHERE SID LIKE @mySID" , con);
                cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;
       
                SqlCeDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    //SessionID = Convert.ToInt32(myReader["column1"]);
                     //Session Info
                    worksheet.Cells[row, col++] = new Cell("Session ID");
                    worksheet.Cells[row, col++] = new Cell("UID");
                    col = 0;
                    row++;
                    worksheet.Cells[row, col++] = new Cell(rdr.GetInt32(0));
                    worksheet.Cells[row, col++] = new Cell(rdr.GetInt32(1));
                    //worksheet.Cells[row, col++] = new Cell(rdr.GetString(1));
                    
                }

                //impacts
                row++;
                col = 0;
                worksheet.Cells[row, col++] = new Cell("Impact ID");
                worksheet.Cells[row, col++] = new Cell("Impact Time");
                col = 0;
                row++;
               
                 
                cmd = new SqlCeCommand("select EID, IMPACT_TIME, LINEAR_ACC_RESULTANT, AZIMUTH,ELEVATION,dataX,dataY,dataZ,rdataX,rdataY,rdataZ,millisecond from Events where SID = @mySID", con);
                cmd.Parameters.Add("@mySID", SqlDbType.Int).Value = mSessionID;
                
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {

                    //SessionID = Convert.ToInt32(myReader["column1"]);
                    //Session Info
                    worksheet.Cells[row, col++] = new Cell( (Int32)rdr["EID"]);
                    mtime = (DateTime)rdr["IMPACT_TIME"];
                    millisecond = (int)rdr["millisecond"];
                    tmpTime  = new DateTime(mtime.Year, mtime.Month, mtime.Day, mtime.Hour, mtime.Minute, mtime.Second,millisecond);
                   
                    //mtime.AddMilliseconds(millisecond - mtime.Millisecond);
                    worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                    

                    dataX = (byte[])rdr["dataX"];
                    dataY = (byte[])rdr["dataY"];
                    dataZ = (byte[])rdr["dataZ"];

                    rdataX = (byte[])rdr["rdataX"];
                    rdataY = (byte[])rdr["rdataY"];
                    rdataZ = (byte[])rdr["rdataZ"];
                    //linear
                    col = 0;
                    row++;
                    worksheet.Cells[row, col++] = new Cell("Time");
                    worksheet.Cells[row, col++] = new Cell("LinX");
                    worksheet.Cells[row, col++] = new Cell("LinY");
                    worksheet.Cells[row, col++] = new Cell("LinZ");
                    worksheet.Cells[row, col++] = new Cell("LinRes");
                    col = 0;
                    row++;
                    
                    for (i = 0; i < 120; i++ )
                    {
                        mDataX = AccDecode(dataX[i]);
                        mDataY = AccDecode(dataY[i]);
                        mDataZ = AccDecode(dataZ[i]);
                        mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);

                        worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                        worksheet.Cells[row, col++] = new Cell(mDataX, "#,##0.00");
                        worksheet.Cells[row, col++] = new Cell(mDataY, "#,##0.00");
                        worksheet.Cells[row, col++] = new Cell(mDataZ, "#,##0.00");
                        worksheet.Cells[row, col++] = new Cell(mDataR, "#,##0.00");
                        tmpTime = tmpTime.AddTicks(3136);
                        col = 0;
                        row++;
                    }
                    worksheet.Cells[row, col++] = new Cell("Time");
                    worksheet.Cells[row, col++] = new Cell("RotX");
                    worksheet.Cells[row, col++] = new Cell("RotY");
                    worksheet.Cells[row, col++] = new Cell("RotZ");
                    worksheet.Cells[row, col++] = new Cell("RotRes");
                    col = 0;
                    row++;
                    //gyro

                    tmpTime = mtime;// =  (DateTime)rdr["IMPACT_TIME"];
                    for (i = 0; i < 32; i++)
                    {

                        mDataX = gyroDecode(rdataX[i * 2], rdataX[i * 2 + 1]);
                        mDataY = gyroDecode(rdataY[i * 2], rdataY[i * 2 + 1]);
                        mDataZ = gyroDecode(rdataZ[i * 2], rdataZ[i * 2 + 1]);
                        mDataR = Math.Sqrt(mDataX * mDataX + mDataY * mDataY + mDataZ * mDataZ);
                        worksheet.Cells[row, col++] = new Cell(tmpTime, @"HH:mm:ss.000");
                        worksheet.Cells[row, col++] = new Cell(mDataX, "#,##");
                        worksheet.Cells[row, col++] = new Cell(mDataY, "#,##");
                        worksheet.Cells[row, col++] = new Cell(mDataZ, "#,##");
                        worksheet.Cells[row, col++] = new Cell(mDataR, "#,##0.00");

                        tmpTime = tmpTime.AddTicks(10000000 / 800); //760 let's make 40 ms
                        col = 0;
                        row++;
                    }
             
                }
                // worksheet.Cells.ColumnWidth[0, 3] = 20;
                workbook.Worksheets.Add(worksheet);
                try
                {
                    //workbook.Save(file);
                     string file = mSessionID.ToString();
                     file = file + ".xlsx";
                     Byte[] bin = p.GetAsByteArray();
                     File.WriteAllBytes(file, bin);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please close exel file first then try again", "Output file is opened", MessageBoxButtons.OK);
                    //throw ex;
                }
                try
                {
                    con.Close();
                }
                catch (Exception me)
                {
                    Console.WriteLine(me.ToString());
                }
            }
#endif
        }
    }
}
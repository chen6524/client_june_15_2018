using System;
using System.IO;
using System.Text;

namespace ArtaFlexHidWin
{
	#region Event definitions
	/// <summary>
	/// Arguments for button changed event
	/// </summary>
    public class HIDInportEventArgs : EventArgs
    {
		/// <summary>Current states of the buttons</summary>
        public readonly BuzzInputReport myInputReport;
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="arrStates">State of the buttons</param>
        public HIDInportEventArgs(BuzzInputReport arrStates)
        {
            myInputReport = arrStates;
        }
    }
	/// <summary>
	/// Delegate for button event
	/// </summary>
    public delegate void HIDInputEventHandler(object sender, HIDInportEventArgs args);
	#endregion

	/// <summary>
	/// Class that defines a controller of Buzz handsets : Representation of the USB hardware device
	/// </summary>
    public class BuzzHandsetDevice : HIDDevice
    {
		#region Public attributes/methods
		/// <summary>
		/// Event fired when one or more button state changes
		/// </summary>
        public event HIDInputEventHandler OnRxPacketReceived;

        private BuzzInputReport oBuzIn;
		/// <summary>
		/// Creates an input report for use in the HID device framework
		/// </summary>
		/// <returns>A new input report for this device</returns>
		public override InputReport CreateInputReport()
		{
			return new BuzzInputReport(this);
		}

        /// <summary>
        /// Writes an Tx Buffer report for use in the HID device framework
        /// </summary>
        /// <returns> Out this buffer by ref byte[] of this report for this device</returns>
        public void WriteOutput(ref byte[] txBuff)
        {
            BuzzOutputReport oReport = new BuzzOutputReport(this);	// create output report
            oReport.SetOutputBuffer(ref txBuff);
            try
            {
                Write(oReport); // write the output report
                //System.Diagnostics.Debug.WriteLine("Output:" + oReport.Buffer[3].ToString());
            }
            catch (HIDDeviceException ex)
            {
                string temp = ex.Message;
                // Device may have been removed!
            }
        }


		/// <summary>
		/// Finds the Buzz handset. 
		/// </summary>
		/// <returns>A new BuzzHandsetDevice or null if not found.</returns>
        public static BuzzHandsetDevice FindBuzzHandset(int mVID, int mPID)
        {
			//return (BuzzHandsetDevice)FindDevice(0x1CD1, 0x0403, typeof(BuzzHandsetDevice));
            return (BuzzHandsetDevice)FindDevice(mVID, mPID, typeof(BuzzHandsetDevice));
        }


		#endregion

        /*
        public BuzzInputReport getInputReport
        {
            get
            {
                return oBuzIn;
            }
        }*/

		#region Overrides
		/// <summary>
		/// Fired when data has been received over USB
		/// </summary>
		/// <param name="oInRep">Input report received</param>
		protected override void HandleDataReceived(InputReport oInRep)
		{
			// Fire the event handler if assigned
            if (OnRxPacketReceived != null)
			{
				/*BuzzInputReport*/ oBuzIn = (BuzzInputReport)oInRep;
                OnRxPacketReceived(this, new HIDInportEventArgs(oBuzIn));
			}
		}
		/// <summary>
		/// Dispose.
		/// </summary>
		/// <param name="bDisposing">True if object is being disposed - else is being finalised</param>
		protected override void Dispose(bool bDisposing)
		{
			if ( bDisposing )
			{
				// before we go, turn all lights off
				//SetLights(false, false, false, false);
			}
			base.Dispose(bDisposing);
		}
		#endregion
    }

	#region Device reports
	/// <summary>
	/// Output report for Buzz device
	/// </summary>
	public class BuzzOutputReport : OutputReport
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="oDev">Device constructing this report</param>
		public BuzzOutputReport(HIDDevice oDev) : base(oDev) {}

        /// <summary>
        /// SetOutputBuffer
        /// </summary>
        /// <param name="txBuff">HID Set output Buffer of this report</param>
        public void SetOutputBuffer(ref byte[]txBuff)
        {
            byte[] arrBuff = Buffer;
            Array.Copy(txBuff,arrBuff,BufferLength);
        }
	}

	/// <summary>
	/// Input report for Buzz Handset device
	/// </summary>
	public class BuzzInputReport : InputReport
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="oDev">Constructing device</param>
		public BuzzInputReport(HIDDevice oDev) : base(oDev)
		{
		}
	}
	#endregion
}

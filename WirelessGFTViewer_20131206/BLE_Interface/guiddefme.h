#ifndef _GUIDDEF_
#define _GUIDDEF_


#include <guiddef.h>


// {DFDD3E89-D394-4023-9D8F-C453B2CFFC22}
#ifdef WIN32
//static const GUID GUID_SPEED_TEST_SERVICE = { 0xdfdd3e89, 0xd394, 0x4023, { 0x9d, 0x8f, 0xc4, 0x53, 0xb2, 0xcf, 0xfc, 0x22 } };
  static const GUID GUID_SPEED_TEST_SERVICE = { 0x0000180a, 0x0000, 0x1000, { 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb } };
#endif
//#define UUID_SPEED_TEST_SERVICE               0x22, 0xfc, 0xcf, 0xb2, 0x53, 0xc4, 0x8f, 0x9d, 0x23, 0x40, 0x94, 0xd3, 0x89, 0x3e, 0xdd, 0xdf
#define UUID_SPEED_TEST_SERVICE              0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0x0a, 0x18, 0x00, 0x00

#define LED_SERVICE_SERV_UUID       0x1110
  static const GUID GUID_LED_TEST_SERVICE = { 0xF0001110, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
  // LED0 Characteristic defines
#define LS_LED0_UUID                0x1111
  static const GUID GUID_LED_TEST_CHARACTERISTIC_LED0 = { 0xF0001111, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
  // LED1 Characteristic defines
#define LS_LED1_UUID                0x1112
  static const GUID GUID_LED_TEST_CHARACTERISTIC_LED1 = { 0xF0001112, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };

  static const GUID GUID_BTN_TEST_SERVICE = { 0xF0001120, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
  static const GUID GUID_BTN_TEST_CHARACTERISTIC_BTN0 = { 0xF0001121, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
  static const GUID GUID_BTN_TEST_CHARACTERISTIC_BTN1 = { 0xF0001122, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };

  static const GUID GUID_DATA_TEST_SERVICE = { 0xF0001130, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
  static const GUID GUID_DATA_TEST_CHARACTERISTIC_STRING = { 0xF0001131, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
  static const GUID GUID_DATA_TEST_CHARACTERISTIC_STREAM = { 0xF0001132, 0x0451, 0x4000,{ 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };


// {8A848ACE-9082-474E-8990-4F1755C695AF}
#ifdef WIN32
static const GUID GUID_SPEED_TEST_CHARACTERISTIC_DATA = { 0x8a848ace, 0x9082, 0x474e, { 0x00, 0x90, 0x4f, 0x17, 0x55, 0xc6, 0x95, 0xaf } };
#endif
#define UUID_SPEED_TEST_CHARACTERISTIC_DATA  0xaf, 0x95, 0xc6, 0x55, 0x17, 0x4f, 0x90, 0x00, 0x4e, 0x47, 0x82, 0x90, 0xce, 0x8a, 0x84, 0x8a

// {6D4824D7-D102-4BAB-83B1-C3322541721D}
#ifdef WIN32
static const GUID GUID_SPEED_TEST_CHARACTERISTIC_CONTROL_POINT = { 0x6d4824d7, 0xd102, 0x4bab, { 0x83, 0xb1, 0xc3, 0x32, 0x25, 0x41, 0x72, 0x1d } };
#endif
#define UUID_SPEED_TEST_CHARACTERISTIC_CONTROL_POINT  0x1d, 0x72, 0x41, 0x25, 0x32, 0xc3, 0xb1, 0x83, 0xab, 0x4b, 0x02, 0xd1, 0xd7, 0x24, 0x48, 0x6d

GUID GUID_BLUETOOTHLE_DEVICE_INTERFACE = { 0x781aee18, 0x7733, 0x4ce4, { 0xad, 0xd0, 0x91, 0xf4, 0x1c, 0x67, 0xb5, 0x92 } };

#define BTW_GATT_UUID_DESCRIPTOR_CLIENT_CONFIG                    0x2902      /*  Client Characteristic Configuration */
const GUID guidClntConfigDesc = { BTW_GATT_UUID_DESCRIPTOR_CLIENT_CONFIG, 0, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB };

#ifndef GATT_MAX_ATTR_LEN 
#define GATT_MAX_ATTR_LEN     600 
#endif




#endif

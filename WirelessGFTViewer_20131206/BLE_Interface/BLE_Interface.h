#include "guiddefme.h"

#pragma once

//using namespace System;

#define WM_CONNECTED        (WM_USER + 101)
#define WM_FRAME_RECEIVED   (WM_USER + 102)
#define WM_FRAME_ACKED      (WM_USER + 103)


namespace BLE_Interface {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Text;
	using namespace System::Runtime::InteropServices;

	using namespace System::Diagnostics;

	//BTH_LE_GATT_SERVICE         m_service;

	//BLUETOOTH_ADDRESS m_bth;

	//BLUETOOTH_GATT_EVENT_HANDLE m_pEventHandle;   //HANLE

	BTH_LE_GATT_CHARACTERISTIC  m_charSpeedTestLED0;
	BTH_LE_GATT_DESCRIPTOR      m_descrSpeedTestLED0ClientConfig;
	BTH_LE_GATT_CHARACTERISTIC  m_charSpeedTestLED1;
	BTH_LE_GATT_DESCRIPTOR      m_descrSpeedTestLED1ClientConfig;

	BTH_LE_GATT_CHARACTERISTIC  m_charSpeedTestBTN0;
	BTH_LE_GATT_DESCRIPTOR      m_descrSpeedTestBTN0ClientConfig;
	BTH_LE_GATT_CHARACTERISTIC  m_charSpeedTestBTN1;
	BTH_LE_GATT_DESCRIPTOR      m_descrSpeedTestBTN1ClientConfig;

	BTH_LE_GATT_CHARACTERISTIC  m_charSpeedTestString;
	BTH_LE_GATT_DESCRIPTOR      m_descrSpeedTestStringClientConfig;
	BTH_LE_GATT_CHARACTERISTIC  m_charSpeedTestStream;
	BTH_LE_GATT_DESCRIPTOR      m_descrSpeedTestStreamClientConfig;

	VOID WINAPI NotificationCallback(BTH_LE_GATT_EVENT_TYPE EventType, PVOID EventOutParameter, PVOID Context)
		//VOID WINAPI NotificationCallback(BTH_LE_GATT_EVENT_TYPE EventType, PVOID EventOutParameter, PVOID Context)
	{
		// ods("EventType:%d\n", EventType);		 
		HWND bt_wnd = (HWND)Context;
		//String^ s = L"BT_Bridge";
		//LPCWSTR name_s = (LPCWSTR)Marshal::StringToHGlobalUni(s).ToPointer();
		//HWND bt_wnd = FindWindow(0, name_s);

		BLUETOOTH_GATT_VALUE_CHANGED_EVENT *pEvent = (BLUETOOTH_GATT_VALUE_CHANGED_EVENT *)EventOutParameter;
		if (pEvent->ChangedAttributeHandle == m_charSpeedTestStream.AttributeHandle)
		{
			BTW_GATT_VALUE *p = (BTW_GATT_VALUE *)malloc(sizeof(BTW_GATT_VALUE));
			if (p)
			{
				p->len = (USHORT)pEvent->CharacteristicValue->DataSize;
				memcpy(p->value, pEvent->CharacteristicValue->Data, pEvent->CharacteristicValue->DataSize);
				PostMessage(bt_wnd, WM_FRAME_RECEIVED, (WPARAM)p, (LPARAM)bt_wnd);
			}
		}
		else if (pEvent->ChangedAttributeHandle == m_charSpeedTestString.AttributeHandle)
		{
			BTW_GATT_VALUE *p = (BTW_GATT_VALUE *)malloc(sizeof(BTW_GATT_VALUE));
			if (p)
			{
				p->len = (USHORT)pEvent->CharacteristicValue->DataSize;
				memcpy(p->value, pEvent->CharacteristicValue->Data, pEvent->CharacteristicValue->DataSize);
				PostMessage(bt_wnd, WM_FRAME_ACKED, (WPARAM)p, (LPARAM)bt_wnd);
			}
		}
	}

	//BOOL SendData(array<BYTE>^%Data, int len)

	public ref class BLE_ReceiveEventArgs :public EventArgs
	{
	public:
		array<System::Byte>^ inBuffer;
		int length;
	
	public:
		BLE_ReceiveEventArgs(){};

		BLE_ReceiveEventArgs(array<System::Byte>^% inBuf, int len)
		{

			//pin_ptr<BYTE> array_pin = &inBuf[0];
			//BYTE* data_ptr = array_pin;

			inBuffer = inBuf;
			length = len;
		}
	};

    public delegate void BLE_ReceiveEventHandler(Object ^sender, BLE_ReceiveEventArgs ^args);


	public ref class BLE_Interface_Form : public System::Windows::Forms::Form
	{
		// TODO: Add your methods for this class here.
	public:
		bool m_bConnected;

	private:
		HANDLE m_hDevice1 = INVALID_HANDLE_VALUE;
		HANDLE m_hServiceLED = INVALID_HANDLE_VALUE;
		HANDLE m_hServiceBTN = INVALID_HANDLE_VALUE;
		HANDLE m_hServiceData = INVALID_HANDLE_VALUE;
		HMODULE hLib1 = NULL;
		   BLUETOOTH_GATT_EVENT_HANDLE m_pEventHandle;

	public:
		BLE_Interface_Form(void)
		{
			//
			//TODO: Add the constructor code here
			//
			m_pEventHandle = INVALID_HANDLE_VALUE;
			m_bConnected = FALSE;
		}


	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~BLE_Interface_Form()
		{
			if (hLib1 != NULL)
			{
				Close();
			}
		}

	public:
		void Close()
		{
			if (m_pEventHandle != INVALID_HANDLE_VALUE)
			{
				FP_BluetoothGATTUnregisterEvent pDereg = (FP_BluetoothGATTUnregisterEvent)GetProcAddress(hLib1, "BluetoothGATTUnregisterEvent");
				if (pDereg)
				{
					pDereg(m_pEventHandle, BLUETOOTH_GATT_FLAG_NONE);
					m_pEventHandle = INVALID_HANDLE_VALUE;
				}
			}

			if (m_hServiceLED != INVALID_HANDLE_VALUE)
				CloseHandle(m_hServiceLED);

			if (m_hServiceBTN != INVALID_HANDLE_VALUE)
				CloseHandle(m_hServiceBTN);

			if (m_hServiceData != INVALID_HANDLE_VALUE)
				CloseHandle(m_hServiceData);

			if (m_hDevice1 != INVALID_HANDLE_VALUE)
				CloseHandle(m_hDevice1);

			if (hLib1 != NULL)
			{
				FreeLibrary(hLib1);
			}


		}

	public:
		void CloseHandleOnly()
		{
			if (m_hServiceLED != INVALID_HANDLE_VALUE)
				CloseHandle(m_hServiceLED);

			if (m_hServiceBTN != INVALID_HANDLE_VALUE)
				CloseHandle(m_hServiceBTN);

			if (m_hServiceData != INVALID_HANDLE_VALUE)
				CloseHandle(m_hServiceData);

			if (m_hDevice1 != INVALID_HANDLE_VALUE)
				CloseHandle(m_hDevice1);

			if (hLib1 != NULL)
			{
				FreeLibrary(hLib1);
			}

		}


		bool IsOSWin8(bool load_lib)
		{
			bool bIsWin8 = false;

			OSVERSIONINFOEX osvi;
			DWORDLONG dwlConditionMask = 0;

			// Initialize the OSVERSIONINFOEX structure.
			ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
			osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
			osvi.dwMajorVersion = 6;
			osvi.dwMinorVersion = 2;
			osvi.wServicePackMajor = 0;

			// Initialize the condition mask.
			VER_SET_CONDITION(dwlConditionMask, VER_MAJORVERSION, VER_GREATER_EQUAL);
			VER_SET_CONDITION(dwlConditionMask, VER_MINORVERSION, VER_GREATER_EQUAL);
			VER_SET_CONDITION(dwlConditionMask, VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL);

			// Perform the test.
			if (VerifyVersionInfo(&osvi, VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR, dwlConditionMask))
			{
				bIsWin8 = true;

				if (load_lib)
				if ((hLib1 = LoadLibrary(L"BluetoothApis.dll")) == NULL)
				{
					bIsWin8 = true;
				}

			}

			return bIsWin8;
		}

	public:
		bool IsBLERadioExist()
		{
			// Lets check if BT is plugged in
			bool ret;
			HANDLE hRadio = INVALID_HANDLE_VALUE;
			BLUETOOTH_FIND_RADIO_PARAMS params = { sizeof(BLUETOOTH_FIND_RADIO_PARAMS) };
			HBLUETOOTH_RADIO_FIND hf = BluetoothFindFirstRadio(&params, &hRadio);

			if (hf == NULL)
			{
				ret = false;
				//return FALSE;
			}
			else
			{
				BLUETOOTH_RADIO_INFO RadioInfo;
				RadioInfo.dwSize = sizeof(BLUETOOTH_RADIO_INFO);
				DWORD result = ERROR_INVALID_PARAMETER;
				result = BluetoothGetRadioInfo(hRadio, &RadioInfo);
				if (result == ERROR_SUCCESS)
				{					
					BLUETOOTH_ADDRESS *pAddress = &RadioInfo.address;
					char ble_addr[13];
					for (int i = 0; i < 6; i++)
					{
						ble_addr[i] = pAddress->rgBytes[5 - i];
					}


					String ^ble_addr_s = String::Format(L"{0:x2}{1:x2}{2:x2}{3:x2}{4:x2}{5:x2}", ble_addr[0], ble_addr[1], ble_addr[2], ble_addr[3], ble_addr[4], ble_addr[5]);
					Debug::WriteLine(ble_addr_s);
					Debug::WriteLine(gcnew String(RadioInfo.szName));

					//String ^DeviceClassID = String::Format(L"{0:d}", RadioInfo.ulClassofDevice);
					Debug::WriteLine(L"0x{0:x}",RadioInfo.ulClassofDevice);
				}

#if 0
				BLUETOOTH_DEVICE_SEARCH_PARAMS m_search_params = { sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS), 0 };
				m_search_params.fReturnAuthenticated = 0;// 1;
				m_search_params.fReturnRemembered    = 0;
				m_search_params.fReturnUnknown = 1;
				m_search_params.fReturnConnected = 0;// 1;
				m_search_params.fIssueInquiry        = 1;
				m_search_params.cTimeoutMultiplier   = 15;
				m_search_params.hRadio               = hRadio;

				HBLUETOOTH_DEVICE_FIND m_bt_dev = NULL;

				BLUETOOTH_DEVICE_INFO m_device_info = { sizeof(BLUETOOTH_DEVICE_INFO), 0, };
				ZeroMemory(&m_device_info, sizeof(BLUETOOTH_DEVICE_INFO));
				m_device_info.dwSize = sizeof(BLUETOOTH_DEVICE_INFO);

				GUID guidServices[10];
				DWORD numServices, result1;
				
				m_bt_dev = BluetoothFindFirstDevice(&m_search_params, &m_device_info);
				//if (m_bt_dev != NULL)
				{
					if (m_bt_dev == NULL)
					{
						DWORD result = WSAGetLastError();
						if (result == ERROR_NO_MORE_ITEMS)
						{
							Debug::WriteLine(L"  No More Items!");
						}
					}
					do
					{
						Debug::WriteLine(gcnew String(m_device_info.szName));
						Debug::WriteLine(L"   Address: {0:x2}:{1:x2}:{2:x2}:{3:x2}:{4:x2}:{5:x2}", 
							m_device_info.Address.rgBytes[5], m_device_info.Address.rgBytes[4], 
							m_device_info.Address.rgBytes[3], m_device_info.Address.rgBytes[2],
							m_device_info.Address.rgBytes[1], m_device_info.Address.rgBytes[0]);
						//Debug::WriteLine(L"  Class: 0x{0:x8}", m_device_info.ulClassofDevice);
						//Debug::WriteLine(L"  Connected: {0:s}", m_device_info.fConnected ? L"true" : L"false");
						//Debug::WriteLine(L"  Authenticated: {0:s}", m_device_info.fAuthenticated ? L"true" : L"false");
						//Debug::WriteLine(L"  Remembered: {0:x}", m_device_info.fRemembered ? L"true" : L"false");

						//if(!BluetoothFindNextDevice(m_bt_dev, &m_device_info))
						//	break;
#if 0
						result1 = BluetoothEnumerateInstalledServices(hRadio, &m_device_info, &numServices, guidServices);
						int i = 0;
						if (result1 == ERROR_SUCCESS)
						{
							Debug::WriteLine(L"BluetoothEnumerateInstalledServices() should be fine!");
							for (i = 0; i < numServices;i++)
								Debug::WriteLine(L"   GUID: {0:x}-{1:x}-{2:x}-{3:x}{4:x}{5:x}{6:x}{7:x}{8:x}{9:x}{10:x} --->{11:d}", guidServices[i].Data1, guidServices[i].Data2, guidServices[i].Data3, 
								guidServices[i].Data4[0], guidServices[i].Data4[1], guidServices[i].Data4[2], guidServices[i].Data4[3], 
								guidServices[i].Data4[4], guidServices[i].Data4[5], guidServices[i].Data4[6], guidServices[i].Data4[7],i);
						}
#endif

					} while (BluetoothFindNextDevice(m_bt_dev, &m_device_info));
				}

				if (BluetoothFindDeviceClose(m_bt_dev) == TRUE)
					Debug::WriteLine("BluetoothFindDeviceClose(m_bt_dev) is OK!");
				else
					Debug::WriteLine(" BluetoothFindDeviceClose(m_bt_dev) failed with error code {0:d}", GetLastError());
#endif
				ret = true;
			}

			CloseHandle(hRadio);
			BluetoothFindRadioClose(hf);
			return ret;
		}

	private:
		String ^MyUuidToString(GUID *uuid)
		{

			String ^part1 = uuid->Data1.ToString("x8");
			String ^part2 = uuid->Data2.ToString("x4");
			String ^part3 = uuid->Data3.ToString("x4");
			String ^part4 = uuid->Data4[0].ToString("x2") + uuid->Data4[1].ToString("x2") + "-" + uuid->Data4[2].ToString("x2") + uuid->Data4[3].ToString("x2")
				+ uuid->Data4[4].ToString("x2") + uuid->Data4[5].ToString("x2") + uuid->Data4[6].ToString("x2") + uuid->Data4[7].ToString("x2");
			part1 = "{" + part1 + "-" + part2 + "-" + part3 + "-" + part4 + "}";
			part1 = part1->ToUpper();
			return part1;
			//char *buffer1 = new char[part1->Length+1]
			//	= part1->ToCharArray();
		}

    private:
		bool GetDeviceName(BLUETOOTH_ADDRESS *pbth, char *name)
		{
			static GUID GUID_BLUETOOTHLE_DEVICE_INTERFACE = { 0x781aee18, 0x7733, 0x4ce4, { 0xad, 0xd0, 0x91, 0xf4, 0x1c, 0x67, 0xb5, 0x92 } };

			{
				HDEVINFO  hardwareDeviceInfo;
				bool bRetVal = FALSE;

				if ((hardwareDeviceInfo = SetupDiGetClassDevs((LPGUID)&GUID_BLUETOOTHLE_DEVICE_INTERFACE,
					NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE))) == INVALID_HANDLE_VALUE)
				{
					OutputDebugStringA("SetupDiGetClassDevs returned INVALID_HANDLE_VALUE.\n");
					return bRetVal;
				}
				// Enumerate devices of LE_DEVICE interface class
				for (DWORD dwIndex = 0;; dwIndex++)
				{
					SP_DEVINFO_DATA DeviceInfoData;

					memset(&DeviceInfoData, 0, sizeof(DeviceInfoData));
					DeviceInfoData.cbSize = sizeof(DeviceInfoData);

					OutputDebugStringA("Call SetupDiEnumDeviceInfo");
					if (!SetupDiEnumDeviceInfo(hardwareDeviceInfo, dwIndex, &DeviceInfoData))
						break;

					CHAR hwid[30];
					DWORD dwBytes = dwBytes = sizeof (hwid);

					if (!SetupDiGetDeviceRegistryPropertyA(hardwareDeviceInfo, &DeviceInfoData, SPDRP_HARDWAREID, NULL, (PBYTE)hwid, dwBytes, &dwBytes))
					{
						OutputDebugStringA("Continue in loop");
						continue;
					}
					int bda0, bda1, bda2, bda3, bda4, bda5;
					sscanf_s(hwid, "BTHLE\\Dev_%02x%02x%02x%02x%02x%02x", &bda0, &bda1, &bda2, &bda3, &bda4, &bda5);

					if ((pbth->rgBytes[0] == (BYTE)bda5) && (pbth->rgBytes[1] == (BYTE)bda4) && (pbth->rgBytes[2] == (BYTE)bda3)
						&& (pbth->rgBytes[3] == (BYTE)bda2) && (pbth->rgBytes[4] == (BYTE)bda1) && (pbth->rgBytes[5] == (BYTE)bda0))
					{
						DWORD dwBytes = 251;
						if (SetupDiGetDeviceRegistryPropertyA(hardwareDeviceInfo, &DeviceInfoData, SPDRP_FRIENDLYNAME, NULL, (PBYTE)name, dwBytes, &dwBytes))
						{
							bRetVal = TRUE;
						}
						break;
					}
				}
				SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
				return bRetVal;
			}

			WORD    wVersionRequested = MAKEWORD(2, 2);
			WSADATA wsaData;
			WSAStartup(wVersionRequested, &wsaData);
			if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
				/* Tell the user that we could not find a usable */
				/* WinSock DLL.                                  */
				wprintf(L"Could not find a usable version of Winsock.dll\n");
				WSACleanup();
				return false;
			}
			else
			{
				//wprintf(L"The Winsock 2.2 dll was found okay\n");
			}


			WSAQUERYSETW qs = { 0 };
#define OFFSET 1024
			BYTE abyBuf[sizeof(WSAQUERYSETW)+OFFSET]; // provide a sufficient large buffer for returned query set
			WSAQUERYSETW *pQS = (WSAQUERYSETW*)abyBuf;
			HANDLE hLookup = INVALID_HANDLE_VALUE;
			DWORD dwFlags = LUP_CONTAINERS | LUP_RETURN_ADDR | LUP_RETURN_TYPE | LUP_RETURN_NAME;

			//Prepare WSAQUERYSETW
			qs.dwSize = sizeof (WSAQUERYSETW);
			qs.dwNameSpace = NS_BTH;
			if (WSALookupServiceBeginW(&qs, dwFlags, &hLookup) != SOCKET_ERROR)
			{
				//Retrive next record
				DWORD dwBufLen = sizeof(WSAQUERYSETW)+OFFSET;
				while (WSALookupServiceNextW(hLookup, dwFlags, &dwBufLen, pQS) != SOCKET_ERROR)
				{
					BYTE *p = (BYTE *)pQS->lpcsaBuffer->RemoteAddr.lpSockaddr->sa_data;
					if ((pbth->rgBytes[0] == p[0]) && (pbth->rgBytes[1] == p[1]) && (pbth->rgBytes[2] == p[2])
						&& (pbth->rgBytes[3] == p[3]) && (pbth->rgBytes[4] == p[4]) && (pbth->rgBytes[5] == p[5]))
					{
						if (pQS->lpszServiceInstanceName[0])
							WideCharToMultiByte(CP_UTF8, 0, pQS->lpszServiceInstanceName, -1, name, 251, 0, 0);
						else
							name[0] = 0;

						WSALookupServiceEnd(hLookup);
						return TRUE;
					}
				}
				if (hLookup != INVALID_HANDLE_VALUE)
					WSALookupServiceEnd(hLookup);
			}
			else
			{
				DWORD wsErr = WSAGetLastError();
				//ods("%d\n", wsErr);
			}
			WSACleanup();
			return false;
		}

	public:
		//int GetDeviceList([Out]String^% ble_addr_s, [Out]String^% ble_name_s)
		int GetDeviceList([Out] List<String^>^% ble_addr_s, [Out] List<String^>^% ble_name_s)
		{
			HDEVINFO                            hardwareDeviceInfo;
			PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;
			ULONG                               predictedLength = 0;
			ULONG                               requiredLength = 0, bytes = 0;
			WCHAR								szBda[13] = { 0 };

			int m_numDevices = 0;
			//GUID guid_t = GUID_SPEED_TEST_SERVICE;

			if ((hardwareDeviceInfo = SetupDiGetClassDevs(NULL, NULL, NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT)) != INVALID_HANDLE_VALUE)
			{
				SP_DEVINFO_DATA DeviceInfoData;

				memset(&DeviceInfoData, 0, sizeof(DeviceInfoData));
				DeviceInfoData.cbSize = sizeof(DeviceInfoData);

				//WCHAR szService[80];
				GUID guid = GUID_SPEED_TEST_SERVICE;

				String ^szService1 = MyUuidToString(&guid);

				//String ^szService1 = L"BTH\\MS_BTHLE\\";
				Debug::WriteLine(szService1);

				//ods("%S\n", szService);
				for (DWORD n = 0; SetupDiEnumDeviceInfo(hardwareDeviceInfo, n, &DeviceInfoData); n++)
				{
					DWORD dwBytes = 0;

					SetupDiGetDeviceInstanceId(hardwareDeviceInfo, &DeviceInfoData, NULL, 0, &dwBytes);

					PWSTR szInstanceId = new WCHAR[dwBytes];
					if (szInstanceId)
					{
						if (SetupDiGetDeviceInstanceId(hardwareDeviceInfo, &DeviceInfoData, szInstanceId, dwBytes, &dwBytes))
						{
							_wcsupr_s(szInstanceId, dwBytes);
							String ^ szInstanceId_t = gcnew String(szInstanceId);					
							Debug::WriteLine(szInstanceId_t);
							//if (wcsstr(szInstanceId, szService))
							if (szInstanceId_t->Contains(szService1))
							{
								OutputDebugStringW(szInstanceId);
								WCHAR buf[13];
								wchar_t* pStart;
								wchar_t* pEnd;
								pStart = wcsrchr(szInstanceId, '_');
								pEnd = wcsrchr(szInstanceId, '\\');
								if (pStart && pEnd)
								{
									*pEnd = 0;
									wcscpy_s(buf, pStart + 1);									

									BLUETOOTH_ADDRESS bth;
									int bda[6];
									if (swscanf_s(buf, L"%02x%02x%02x%02x%02x%02x", &bda[0], &bda[1], &bda[2], &bda[3], &bda[4], &bda[5]) == 6)
									{
										for (int i = 0; i < 6; i++)
											bth.rgBytes[5 - i] = (BYTE)bda[i];
									}
									char name[251];
									GetDeviceName(&bth, &name[0]);

									//String ^ddd = gcnew String(name);
									//ble_addr = ddd;
									//mBT_Name->Items->Add(ddd);

									m_numDevices++;
									//String ^ddd = gcnew String(buf);
									//char* address = (char *)Marshal::StringToHGlobalAnsi(ddd).ToPointer();
									char ble_addr[13];
									for (int i = 0; i < 13; i++)
									{
										ble_addr[i] = (char)buf[i];
									}


									ble_addr_s->Add(gcnew String(ble_addr));
									ble_name_s->Add(gcnew String(name));
									//ble_name = ddd;
									//mBT_Addr->Items->Add(ddd);
									//delete[] szInstanceId;
									//break;
								}
							}
						}
						delete[] szInstanceId;
					}
				}
				SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
			}
			return m_numDevices;
		}

		bool lookup([Out]String^% ble_addr_s, [Out]String^% ble_name_s)
		{
			WORD    wVersionRequested = MAKEWORD(2, 2);
			WSADATA wsaData;
			WSAStartup(wVersionRequested, &wsaData);
			if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
				/* Tell the user that we could not find a usable */
				/* WinSock DLL.                                  */
				wprintf(L"Could not find a usable version of Winsock.dll\n");
				WSACleanup();
				return false;
			}
			else
			{
				//wprintf(L"The Winsock 2.2 dll was found okay\n");
			}

			char name[251];
			BLUETOOTH_ADDRESS pbth;

			pbth.rgBytes[0] = 0xC0;
			pbth.rgBytes[1] = 0xFE;
			pbth.rgBytes[2] = 0x10;
			pbth.rgBytes[3] = 0x7A;
			pbth.rgBytes[4] = 0x73;
			pbth.rgBytes[5] = 0x20;

			WSAQUERYSETW qs = { 0 };
#define OFFSET 1024
			BYTE abyBuf[sizeof(WSAQUERYSETW)+OFFSET]; // provide a sufficient large buffer for returned query set
			WSAQUERYSETW *pQS = (WSAQUERYSETW*)abyBuf;
			HANDLE hLookup = INVALID_HANDLE_VALUE;
			DWORD dwFlags = LUP_CONTAINERS | LUP_RETURN_ADDR | LUP_RETURN_TYPE | LUP_RETURN_NAME;
			//DWORD dwFlags = LUP_CONTAINERS | LUP_RETURN_ALL | LUP_FLUSHCACHE;

			//Prepare WSAQUERYSETW
			qs.dwSize = sizeof (WSAQUERYSETW);
			qs.dwNameSpace = NS_BTH;
			if (WSALookupServiceBeginW(&qs, dwFlags, &hLookup) != SOCKET_ERROR)
			{
				//Retrive next record
				DWORD dwBufLen = sizeof(WSAQUERYSETW)+OFFSET;
				while (WSALookupServiceNextW(hLookup, dwFlags, &dwBufLen, pQS) != SOCKET_ERROR)
				{
					//String ^ ppp = gcnew String();
					Debug::WriteLine(gcnew String(pQS->lpszServiceInstanceName));

					BYTE *p = (BYTE *)pQS->lpcsaBuffer->RemoteAddr.lpSockaddr->sa_data;
					if ((pbth.rgBytes[0] == p[0]) && (pbth.rgBytes[1] == p[1]) && (pbth.rgBytes[2] == p[2])
						&& (pbth.rgBytes[3] == p[3]) && (pbth.rgBytes[4] == p[4]) && (pbth.rgBytes[5] == p[5]))
					{
						if (pQS->lpszServiceInstanceName[0])
							WideCharToMultiByte(CP_UTF8, 0, pQS->lpszServiceInstanceName, -1, name, 251, 0, 0);
						else
							name[0] = 0;

						WSALookupServiceEnd(hLookup);
						return TRUE;
					}
				}
				if (hLookup != INVALID_HANDLE_VALUE)
					WSALookupServiceEnd(hLookup);
			}
			else
			{
				DWORD wsErr = WSAGetLastError();
				Debug::Write("WSAGetLastError() = ");
				Debug::Write(wsErr);
				Debug::Write("\r\n");
				//ods("%d\n", wsErr);
			}
			WSACleanup();
			return false;
		}

		private:
		void BdaToString(PWCHAR buffer, BLUETOOTH_ADDRESS *btha)
		{
			WCHAR c;
			for (int i = 0; i < 6; i++)
			{
				c = btha->rgBytes[5 - i] >> 4;
				buffer[2 * i] = c < 10 ? c + '0' : c + 'A' - 10;
				c = btha->rgBytes[5 - i] & 0x0f;
				buffer[2 * i + 1] = c < 10 ? c + '0' : c + 'A' - 10;
			}
		}

		private:
		HANDLE OpenBleDevice(BLUETOOTH_ADDRESS *btha)
		{
			HDEVINFO                            hardwareDeviceInfo;
			SP_DEVICE_INTERFACE_DATA            deviceInterfaceData;
			PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;
			ULONG                               predictedLength = 0;
			ULONG                               requiredLength = 0, bytes = 0;
			WCHAR								szBda[13] = { 0 };
			HANDLE								hDevice = INVALID_HANDLE_VALUE;
			DWORD								err;

			if ((hardwareDeviceInfo = SetupDiGetClassDevs((LPGUID)&GUID_BLUETOOTHLE_DEVICE_INTERFACE,
				NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE))) == INVALID_HANDLE_VALUE)
			{
				//ods("SetupDiGetClassDevs failed: %x\n", GetLastError());
				return hDevice;
			}

			deviceInterfaceData.cbSize = sizeof (SP_DEVICE_INTERFACE_DATA);

			// Enumerate devices of LE_DEVICE interface class
			for (int i = 0;; i++)
			{
				if (!SetupDiEnumDeviceInterfaces(hardwareDeviceInfo, 0,
					(LPGUID)&GUID_BLUETOOTHLE_DEVICE_INTERFACE, i, &deviceInterfaceData))
				{
					if ((err = GetLastError()) == ERROR_NO_MORE_ITEMS)
						;// ods("OpenBleDevice device not found\n");
					else
						;// ods("OpenBleDevice:ERROR SetupDiEnumDeviceInterfaces failed:%d\n", err);
					break;
				}
				SetupDiGetDeviceInterfaceDetail(hardwareDeviceInfo, &deviceInterfaceData,
					NULL, 0, &requiredLength, NULL);

				if ((err = GetLastError()) != ERROR_INSUFFICIENT_BUFFER)
				{
					;// ods("OpenBleDevice:ERROR SetupDiGetDeviceInterfaceDetail failed %d\n", err);
					break;
				}

				predictedLength = requiredLength;

				deviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA) new CHAR[predictedLength + 2];
				if (deviceInterfaceDetailData == NULL)
				{
					;// ods("OpenBleDevice:ERROR Couldn't allocate %d bytes for device interface details.\n", predictedLength);
					break;
				}
				RtlZeroMemory(deviceInterfaceDetailData, predictedLength + 2);
				deviceInterfaceDetailData->cbSize = sizeof (SP_DEVICE_INTERFACE_DETAIL_DATA);
				if (!SetupDiGetDeviceInterfaceDetail(hardwareDeviceInfo, &deviceInterfaceData,
					deviceInterfaceDetailData, predictedLength, &requiredLength, NULL))
				{
					;// ods("OpenBleDevice:ERROR SetupDiGetDeviceInterfaceDetail\n");
					delete deviceInterfaceDetailData;
					break;
				}

				_wcsupr_s(deviceInterfaceDetailData->DevicePath, wcslen(deviceInterfaceDetailData->DevicePath) + 1);
				BdaToString(szBda, btha);
				if (wcsstr(deviceInterfaceDetailData->DevicePath, szBda) != NULL)
				{
					//ods("Opening interface:%S\n", deviceInterfaceDetailData->DevicePath);

					hDevice = CreateFile(deviceInterfaceDetailData->DevicePath,
						GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

					delete deviceInterfaceDetailData;
					break;
				}
				delete deviceInterfaceDetailData;
			}
			SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
			return hDevice;
		}

		private:
		HANDLE OpenBleService(BLUETOOTH_ADDRESS *btha, GUID *guid)
		{
			HDEVINFO                            hardwareDeviceInfo;
			SP_DEVICE_INTERFACE_DATA            deviceInterfaceData;
			PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;
			ULONG                               predictedLength = 0;
			ULONG                               requiredLength = 0, bytes = 0;
			WCHAR								szBda[13] = { 0 };
			HANDLE								hService = INVALID_HANDLE_VALUE;
			DWORD								err;

			if ((hardwareDeviceInfo = SetupDiGetClassDevs((LPGUID)guid,
				NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE))) == INVALID_HANDLE_VALUE)
			{
				;// ods("OpenBleServiceSetupDiGetClassDevs failed: %x\n", GetLastError());
				return hService;
			}

			deviceInterfaceData.cbSize = sizeof (SP_DEVICE_INTERFACE_DATA);

			// Enumerate devices of LE_DEVICE interface class
			for (int i = 0;; i++)
			{
				if (!SetupDiEnumDeviceInterfaces(hardwareDeviceInfo, 0, guid, i, &deviceInterfaceData))
				{
					if ((err = GetLastError()) == ERROR_NO_MORE_ITEMS)
					{
						;// ods("OpenBleService device not found\n");
					}
					else
					{
						;// ods("OpenBleService:ERROR SetupDiEnumDeviceInterfaces failed:%d\n", err);
					}
					break;
				}
				SetupDiGetDeviceInterfaceDetail(hardwareDeviceInfo, &deviceInterfaceData,
					NULL, 0, &requiredLength, NULL);

				if ((err = GetLastError()) != ERROR_INSUFFICIENT_BUFFER)
				{
					// ods("OpenBleService:ERROR SetupDiGetDeviceInterfaceDetail failed %d\n", err);
					break;
				}

				predictedLength = requiredLength;

				deviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA) new CHAR[predictedLength + 2];
				if (deviceInterfaceDetailData == NULL)
				{
					// ods("OpenBleService:ERROR Couldn't allocate %d bytes for device interface details.\n", predictedLength);
					break;
				}
				RtlZeroMemory(deviceInterfaceDetailData, predictedLength + 2);
				deviceInterfaceDetailData->cbSize = sizeof (SP_DEVICE_INTERFACE_DETAIL_DATA);
				if (!SetupDiGetDeviceInterfaceDetail(hardwareDeviceInfo, &deviceInterfaceData,
					deviceInterfaceDetailData, predictedLength, &requiredLength, NULL))
				{
					// ods("OpenBleService :ERROR SetupDiGetDeviceInterfaceDetail\n");
					delete deviceInterfaceDetailData;
					break;
				}

				_wcsupr_s(deviceInterfaceDetailData->DevicePath, wcslen(deviceInterfaceDetailData->DevicePath) + 1);
				BdaToString(szBda, btha);
				if (wcsstr(deviceInterfaceDetailData->DevicePath, szBda) != NULL)
				{
					// ods("OpenBleService Opening interface:%S\n", deviceInterfaceDetailData->DevicePath);

					hService = CreateFile(deviceInterfaceDetailData->DevicePath,
						GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

					if (hService == INVALID_HANDLE_VALUE)
					{
						;// ods("OpenBleService (hService == INVALID_HANDLE_VALUE) GetLastError() = %d\n", GetLastError());
					}

					delete deviceInterfaceDetailData;
					break;
				}
				delete deviceInterfaceDetailData;
			}
			SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
			return hService;
		}

		private:
		bool Init(BLUETOOTH_ADDRESS *m_bth)
		{

			if ((m_hDevice1 = OpenBleDevice(m_bth)) == INVALID_HANDLE_VALUE)
				return false;

			USHORT num = 0;
			HRESULT hr;

			FP_BluetoothGATTGetServices pBluetoothGATTGetServices = (FP_BluetoothGATTGetServices)GetProcAddress(hLib1, "BluetoothGATTGetServices");
			hr = pBluetoothGATTGetServices(m_hDevice1, 0, NULL, &num, BLUETOOTH_GATT_FLAG_NONE);
			if (num == 0)
				return false;

			PBTH_LE_GATT_SERVICE pServices = (PBTH_LE_GATT_SERVICE)malloc(sizeof(BTH_LE_GATT_SERVICE)* num);
			if (pServices == NULL)
				return false;

			RtlZeroMemory(pServices, sizeof(BTH_LE_GATT_SERVICE)* num);

			hr = pBluetoothGATTGetServices(m_hDevice1, num, pServices, &num, BLUETOOTH_GATT_FLAG_NONE);
			if (S_OK != hr)
			{
				// ods("BluetoothGATTGetServices - Actual Data %x", hr);
				return false;
			}

			// search for Speed Test services.

			BTH_LE_GATT_SERVICE m_serviceLED;
			BTH_LE_GATT_SERVICE m_serviceButton;
			BTH_LE_GATT_SERVICE m_serviceData;

			_BTH_LE_UUID uuidSpeedTestLED;
			_BTH_LE_UUID uuidSpeedTestButton;
			_BTH_LE_UUID uuidSpeedTestData;

			uuidSpeedTestLED.IsShortUuid = FALSE;
			uuidSpeedTestLED.Value.LongUuid = GUID_LED_TEST_SERVICE;

			uuidSpeedTestButton.IsShortUuid = FALSE;
			uuidSpeedTestButton.Value.LongUuid = GUID_BTN_TEST_SERVICE;

			uuidSpeedTestData.IsShortUuid = FALSE;
			uuidSpeedTestData.Value.LongUuid = GUID_DATA_TEST_SERVICE;
			for (int kk = 0; kk < num; kk++)
			{
				if (IsBthLEUuidMatch(pServices[kk].ServiceUuid, uuidSpeedTestLED))
					m_serviceLED = pServices[kk];
				else if (IsBthLEUuidMatch(pServices[kk].ServiceUuid, uuidSpeedTestButton))
					m_serviceButton = pServices[kk];
				else if (IsBthLEUuidMatch(pServices[kk].ServiceUuid, uuidSpeedTestData))
					m_serviceData = pServices[kk];
				else
				{
				}
			}


			PBTH_LE_GATT_CHARACTERISTIC pChars;

			FP_BluetoothGATTGetCharacteristics pBluetoothGATTGetCharacteristics = (FP_BluetoothGATTGetCharacteristics)GetProcAddress(hLib1, "BluetoothGATTGetCharacteristics");

			// Go through all characteristics of the Speed Test service to find Control Point and Data
			num = 0;
			pBluetoothGATTGetCharacteristics(m_hDevice1, &m_serviceLED, 0, NULL, &num, BLUETOOTH_GATT_FLAG_NONE);
			if (num != 0)
			{
				if ((pChars = new BTH_LE_GATT_CHARACTERISTIC[num]) == NULL)
					return false;

				if ((pBluetoothGATTGetCharacteristics(m_hDevice1, &m_serviceLED, num, pChars, &num, BLUETOOTH_GATT_FLAG_NONE)) != S_OK)
				{
					//ods("LoadCharacteristics hr:0x%x\n", hr);
					delete[] pChars;
					return false;
				}
			}
			// search for Speed Test service characteristics.

			m_charSpeedTestLED0.AttributeHandle = 0;
			m_charSpeedTestLED1.AttributeHandle = 0;

			_BTH_LE_UUID charLED0, charLED1;
			charLED0.IsShortUuid = FALSE;
			charLED0.Value.LongUuid = GUID_LED_TEST_CHARACTERISTIC_LED0;
			charLED1.IsShortUuid = FALSE;
			charLED1.Value.LongUuid = GUID_LED_TEST_CHARACTERISTIC_LED1;
			for (int i = 0; i < num; i++)
			{
				BTH_LE_GATT_DESCRIPTOR *pClientConfig;

				if (IsBthLEUuidMatch(pChars[i].CharacteristicUuid, charLED0))
				{
					m_charSpeedTestLED0 = pChars[i];
					pClientConfig = &m_descrSpeedTestLED0ClientConfig;
				}
				else if (IsBthLEUuidMatch(pChars[i].CharacteristicUuid, charLED1))
				{
					m_charSpeedTestLED1 = pChars[i];
					pClientConfig = &m_descrSpeedTestLED1ClientConfig;
				}
				else
					continue;

				PBTH_LE_GATT_DESCRIPTOR pDescrs = NULL;
				USHORT numDescr = 0;

				FP_BluetoothGATTGetDescriptors pBluetoothGATTGetDescriptors = (FP_BluetoothGATTGetDescriptors)GetProcAddress(hLib1, "BluetoothGATTGetDescriptors");
				pBluetoothGATTGetDescriptors(m_hDevice1, &pChars[i], 0, NULL, &numDescr, BLUETOOTH_GATT_FLAG_NONE);
				if (numDescr != 0)
				{
					if ((pDescrs = new BTH_LE_GATT_DESCRIPTOR[numDescr]) == NULL)
						break;

					if ((pBluetoothGATTGetDescriptors(m_hDevice1, &pChars[i], numDescr, pDescrs, &numDescr, BLUETOOTH_GATT_FLAG_NONE)) != S_OK)
					{
						//ods("LoadCharacteristics hr:0x%x\n", hr);
						delete[] pDescrs;
						break;
					}
				}

				// search for client configuration descriptor.
				_BTH_LE_UUID uuidClientConfig;
				uuidClientConfig.IsShortUuid = TRUE;
				uuidClientConfig.Value.ShortUuid = BTW_GATT_UUID_DESCRIPTOR_CLIENT_CONFIG;

				for (int j = 0; j < numDescr; j++)
				{
					if (IsBthLEUuidMatch(pDescrs[j].DescriptorUuid, uuidClientConfig))
					{
						*pClientConfig = pDescrs[j];
						break;
					}
				}
				delete[]pDescrs;
			}

			// Go through all characteristics of the Speed Test service to find Control Point and Data
			num = 0;
			pBluetoothGATTGetCharacteristics(m_hDevice1, &m_serviceData, 0, NULL, &num, BLUETOOTH_GATT_FLAG_NONE);
			if (num != 0)
			{
				if ((pChars = new BTH_LE_GATT_CHARACTERISTIC[num]) == NULL)
					return false;

				if ((pBluetoothGATTGetCharacteristics(m_hDevice1, &m_serviceData, num, pChars, &num, BLUETOOTH_GATT_FLAG_NONE)) != S_OK)
				{
					//ods("LoadCharacteristics hr:0x%x\n", hr);
					delete[] pChars;
					return false;
				}
			}

			m_charSpeedTestString.AttributeHandle = 0;
			m_charSpeedTestStream.AttributeHandle = 0;

			_BTH_LE_UUID charString, charStream;
			charString.IsShortUuid = FALSE;
			charString.Value.LongUuid = GUID_DATA_TEST_CHARACTERISTIC_STRING;
			charStream.IsShortUuid = FALSE;
			charStream.Value.LongUuid = GUID_DATA_TEST_CHARACTERISTIC_STREAM;
			for (int i = 0; i < num; i++)
			{
				BTH_LE_GATT_DESCRIPTOR *pClientConfig;

				if (IsBthLEUuidMatch(pChars[i].CharacteristicUuid, charString))
				{
					m_charSpeedTestString = pChars[i];
					pClientConfig = &m_descrSpeedTestStringClientConfig;
				}
				else if (IsBthLEUuidMatch(pChars[i].CharacteristicUuid, charStream))
				{
					m_charSpeedTestStream = pChars[i];
					pClientConfig = &m_descrSpeedTestStreamClientConfig;
				}
				else
					continue;

				PBTH_LE_GATT_DESCRIPTOR pDescrs = NULL;
				USHORT numDescr = 0;

				FP_BluetoothGATTGetDescriptors pBluetoothGATTGetDescriptors = (FP_BluetoothGATTGetDescriptors)GetProcAddress(hLib1, "BluetoothGATTGetDescriptors");
				pBluetoothGATTGetDescriptors(m_hDevice1, &pChars[i], 0, NULL, &numDescr, BLUETOOTH_GATT_FLAG_NONE);
				if (numDescr != 0)
				{
					if ((pDescrs = new BTH_LE_GATT_DESCRIPTOR[numDescr]) == NULL)
						break;

					if ((pBluetoothGATTGetDescriptors(m_hDevice1, &pChars[i], numDescr, pDescrs, &numDescr, BLUETOOTH_GATT_FLAG_NONE)) != S_OK)
					{
						//ods("LoadCharacteristics hr:0x%x\n", hr);
						delete[] pDescrs;
						break;
					}
				}

				// search for client configuration descriptor.
				_BTH_LE_UUID uuidClientConfig;
				uuidClientConfig.IsShortUuid = TRUE;
				uuidClientConfig.Value.ShortUuid = BTW_GATT_UUID_DESCRIPTOR_CLIENT_CONFIG;

				for (int j = 0; j < numDescr; j++)
				{
					if (IsBthLEUuidMatch(pDescrs[j].DescriptorUuid, uuidClientConfig))
					{
						*pClientConfig = pDescrs[j];
						break;
					}
				}
				delete[]pDescrs;
			}
			//free(pServices);


			free(pServices);

			GUID guid = GUID_LED_TEST_SERVICE;// GUID_SPEED_TEST_SERVICE;
			m_hServiceLED = OpenBleService(m_bth, &guid);
			if (m_hServiceLED == INVALID_HANDLE_VALUE)
				return false;

			//			guid = GUID_BTN_TEST_SERVICE;// GUID_SPEED_TEST_SERVICE;
			//			m_hServiceBTN = OpenBleService(m_bth, &guid);
			//			if (m_hServiceBTN == INVALID_HANDLE_VALUE)
			//				return false;

			guid = GUID_DATA_TEST_SERVICE;// GUID_SPEED_TEST_SERVICE;
			m_hServiceData = OpenBleService(m_bth, &guid);
			if (m_hServiceData == INVALID_HANDLE_VALUE)
				return false;
			return true;
		}


		public:
		bool InitTest(String ^mBTAddr)
		{
			//UpdateData(TRUE);

			WCHAR buf[13];
			int   bda[6];
			BLUETOOTH_ADDRESS bth = { 0 };
			//int sel = mBT_Addr->SelectedIndex;

			if (mBTAddr->Length == 12)
			{
				String ^addrBt = mBTAddr;
				//addrBt->ToCharArray();
				ASCIIEncoding^ ascii = gcnew ASCIIEncoding;

				array<Byte>^ ddd = ascii->GetBytes(addrBt);
				for (int j = 0; j < 12; j++)
				{
					buf[j] = ddd[j];
				}
				buf[12] = 0;

				//m_cbDevices.GetLBText(m_cbDevices.GetCurSel(), buf);
				if (swscanf_s(buf, L"%02x%02x%02x%02x%02x%02x", &bda[0], &bda[1], &bda[2], &bda[3], &bda[4], &bda[5]) == 6)
				{
					for (int i = 0; i < 6; i++)
						bth.rgBytes[5 - i] = (BYTE)bda[i];
				}
			}
#if 0
			// if we are running second time interfaces are probably already initialized
			if ((m_btDeviceInterface != NULL) && (m_btDeviceInterface->m_bth.ullLong == bth.ullLong))
			{
				//OnConnected(m_btDeviceInterface->m_bConnected, (LPARAM)m_btDeviceInterface);
				return;
			}

			delete m_btDeviceInterface;

			//m_btDeviceInterface = new CBtWin8Interface(&bth, hLib, this);

			m_btDeviceInterface->Init();

			// on Win7 we will receive notification when device is connected and will intialize dialog there
			if (!m_bWin8)
				return;

			CBtWin8Interface *pWin8BtInterface = dynamic_cast<CBtWin8Interface *>(m_btDeviceInterface);

			// Assume that we are connected.  Failed attempt to read battery will change that to FALSE.
			pWin8BtInterface->m_bConnected = TRUE;

			// register for notifications with the status
			pWin8BtInterface->m_bConnected = pWin8BtInterface->SetDescriptors(0, 0);

			pWin8BtInterface->RegisterNotification();

			if (pWin8BtInterface->m_bConnected)
				OnConnected(TRUE, (LPARAM)pWin8BtInterface);
#else 
			if (m_bConnected) return true;


			if (!Init(&bth))
			{
				m_bConnected = false;
				return false;
			}

			if (!Data_SetDescriptors(1, 1))
			{
				m_bConnected = false;
				//return false;
			}

			RegisterNotification();

			//RegisterNotification();

			m_bConnected = true;;

			//if (pWin8BtInterface->m_bConnected)
			//	 OnConnected(TRUE, (LPARAM)pWin8BtInterface);
			return true;
#endif
		}

		public:event BLE_ReceiveEventHandler ^OnBLEDataReceived;
	    public:event BLE_ReceiveEventHandler ^OnBLEAckReceived;

		private:
			void BLE_HandleDataReceived(System::IntPtr wP, System::IntPtr lP)
			{				
				//if (OnBLEDataReceived != nullptr)
				{
					BTW_GATT_VALUE *p1 = (BTW_GATT_VALUE*)wP.ToPointer();

					array<System::Byte> ^inBuff = gcnew array<System::Byte>(p1->len);
					pin_ptr<byte> data_array_pin = &inBuff[0];
					memcpy(data_array_pin, p1->value, p1->len);

					BLE_ReceiveEventArgs ^args = gcnew BLE_ReceiveEventArgs(inBuff, p1->len);
					OnBLEDataReceived(this, args);				
				}
		    }

			void BLE_HandleACKReceived(System::IntPtr wP, System::IntPtr lP)
			{
				//if (OnBLEPacketReceived != NULL)
				{
					BTW_GATT_VALUE *p1 = (BTW_GATT_VALUE*)wP.ToPointer();
					array<System::Byte> ^inBuff = gcnew array<System::Byte>(p1->len);
					pin_ptr<byte> data_array_pin = &inBuff[0];
					memcpy(data_array_pin, p1->value, p1->len);
					BLE_ReceiveEventArgs ^args = gcnew BLE_ReceiveEventArgs(inBuff, p1->len);

					//BLE_ReceiveEventArgs ^args = gcnew BLE_ReceiveEventArgs();// wP, lP);
					OnBLEAckReceived(this, args);
				}
			}
	
#if 1
		protected:
			//	 void System::Windows::Forms::ContainerControl::WndProc(System::Windows::Forms::Message %m)
			virtual void WndProc(System::Windows::Forms::Message %m) override
			{
				//Int32 aaa = m.WParam.ToInt32();				

				if (m.Msg == WM_FRAME_RECEIVED)
				{
					//FrameReceived(m.WParam, m.LParam);
					//OnFrameArrived();// gcnew EventArgs());
					BLE_HandleDataReceived(m.WParam, m.LParam);
					//aaa = 101;
				}
				else if (m.Msg == WM_CONNECTED)
				{
					//aaa = 101;
				}
				else if (m.Msg == WM_FRAME_ACKED)
				{
					//OnACKArrived();// gcnew EventArgs());


					BLE_HandleACKReceived(m.WParam, m.LParam);
					//aaa = 102;
					//m_OutstandingFrames--;
					//SetEvent(m_hFlowEvent);
				}
				else
				{
				}

				Form::WndProc(m);

			}
#endif

		public:
			bool RegisterNotification()
			{
				HRESULT hr = E_FAIL;

				// register to receive notification
				LPBYTE buf = (LPBYTE)malloc(sizeof (BLUETOOTH_GATT_VALUE_CHANGED_EVENT_REGISTRATION)+sizeof (BTH_LE_GATT_CHARACTERISTIC)+16);

				PBLUETOOTH_GATT_VALUE_CHANGED_EVENT_REGISTRATION p = (PBLUETOOTH_GATT_VALUE_CHANGED_EVENT_REGISTRATION)buf;
				p->NumCharacteristics = 2;
				p->Characteristics[0] = m_charSpeedTestStream;
				p->Characteristics[1] = m_charSpeedTestString;

				if (m_pEventHandle == INVALID_HANDLE_VALUE)//BLUETOOTH_GATT_EVENT_HANDLE
				{
#if 1				
					FP_BluetoothGATTRegisterEvent pReg = (FP_BluetoothGATTRegisterEvent)GetProcAddress(hLib1, "BluetoothGATTRegisterEvent");
					if (pReg)
					{
						HWND bt_wnd = static_cast<HWND>(this->Handle.ToPointer());

						BLUETOOTH_GATT_EVENT_HANDLE m_ph;
						hr = pReg(m_hServiceData, CharacteristicValueChangedEvent, p, &NotificationCallback, bt_wnd, &m_ph, BLUETOOTH_GATT_FLAG_NONE);
						m_pEventHandle = m_ph;


						//ods("BluetoothGATTRegisterEvent hr:0x%x\n", hr);
					}

#else				 
					// HRESULT hr = BluetoothGATTRegisterEvent(m_hService, CharacteristicValueChangedEvent, p, NotificationCallback, NULL, &m_pEventHandle, BLUETOOTH_GATT_FLAG_NONE);
#endif
				}
				free(buf);

				if (hr == S_OK)
					return true;
				else
					return false;
			}

			public:
			bool Data_SetDescriptors(USHORT Control, USHORT Data)
			{
				HRESULT hr = E_FAIL;

				if (m_hServiceData == INVALID_HANDLE_VALUE)
					return false;

				FP_BluetoothGATTSetDescriptorValue pSetDescr = (FP_BluetoothGATTSetDescriptorValue)GetProcAddress(hLib1, "BluetoothGATTSetDescriptorValue");
				if (pSetDescr)
				{
					BTH_LE_GATT_DESCRIPTOR_VALUE DescriptorValue;

					RtlZeroMemory(&DescriptorValue, sizeof(DescriptorValue));

					DescriptorValue.DescriptorType = ClientCharacteristicConfiguration;
					DescriptorValue.ClientCharacteristicConfiguration.IsSubscribeToNotification = (Control & 0x01) ? 1 : 0;
					DescriptorValue.ClientCharacteristicConfiguration.IsSubscribeToIndication = (Control & 0x02) ? 1 : 0;

					hr = pSetDescr(m_hServiceData, &m_descrSpeedTestStreamClientConfig, &DescriptorValue, BLUETOOTH_GATT_FLAG_NONE);
					//ods("BluetoothGATTSetDescriptorValue hr:0x%x\n", hr);
					if (hr == S_OK)
					{
						DescriptorValue.DescriptorType = ClientCharacteristicConfiguration;
						DescriptorValue.ClientCharacteristicConfiguration.IsSubscribeToNotification = (Data & 0x01) ? 1 : 0;
						DescriptorValue.ClientCharacteristicConfiguration.IsSubscribeToIndication = (Data & 0x02) ? 1 : 0;

						hr = pSetDescr(m_hServiceData, &m_descrSpeedTestStringClientConfig, &DescriptorValue, BLUETOOTH_GATT_FLAG_NONE);
						//ods("BluetoothGATTSetDescriptorValue hr:0x%x\n", hr);
					}
				}
				if (hr == S_OK)
					return true;
				else
					return false;
			}

			public:
			BOOL SendCommand(BYTE Command, BYTE sParam)
			{
				FP_BluetoothGATTSetCharacteristicValue pWriteChar = (FP_BluetoothGATTSetCharacteristicValue)GetProcAddress(hLib1, "BluetoothGATTSetCharacteristicValue");
				if (pWriteChar == NULL)
					return FALSE;

				if (m_hServiceData == INVALID_HANDLE_VALUE)
					return FALSE;

				BYTE buf[sizeof (BTH_LE_GATT_CHARACTERISTIC_VALUE)+10];
				BTH_LE_GATT_CHARACTERISTIC_VALUE *Value = (BTH_LE_GATT_CHARACTERISTIC_VALUE *)buf;
				RtlZeroMemory(Value, sizeof(BTH_LE_GATT_CHARACTERISTIC_VALUE));

				Value->DataSize = 2;
				Value->Data[0] = (BYTE)Command;
				Value->Data[1] = (BYTE)(sParam);

				pWriteChar(m_hServiceData, &m_charSpeedTestString, Value, NULL, 0);
				return FALSE;
			}

			public:
				BOOL LED_Control(BYTE nLed, BYTE ON_OFF)
				{
					FP_BluetoothGATTSetCharacteristicValue pWriteChar = (FP_BluetoothGATTSetCharacteristicValue)GetProcAddress(hLib1, "BluetoothGATTSetCharacteristicValue");
					if (pWriteChar == NULL)
						return FALSE;

					if (m_hServiceLED == INVALID_HANDLE_VALUE)
						return FALSE;

					BYTE buf[sizeof(BTH_LE_GATT_CHARACTERISTIC_VALUE) + 10];
					BTH_LE_GATT_CHARACTERISTIC_VALUE *Value = (BTH_LE_GATT_CHARACTERISTIC_VALUE *)buf;
					RtlZeroMemory(Value, sizeof(BTH_LE_GATT_CHARACTERISTIC_VALUE));

					Value->DataSize = 1;
					Value->Data[0] = (BYTE)ON_OFF;

					if (nLed == 0)
						pWriteChar(m_hServiceLED, &m_charSpeedTestLED0, Value, NULL, 0);
					else
						pWriteChar(m_hServiceLED, &m_charSpeedTestLED1, Value, NULL, 0);
					return FALSE;
				}

            public:
			BOOL SendData(array<BYTE>^%Data, int len)
			{
				if (len > 512)
				{
					//ods("-%S data too long\n", __FUNCTIONW__);
					return (FALSE);
				}
				FP_BluetoothGATTSetCharacteristicValue pWriteChar = (FP_BluetoothGATTSetCharacteristicValue)GetProcAddress(hLib1, "BluetoothGATTSetCharacteristicValue");
				if (pWriteChar == NULL)
					return FALSE;

				if (m_hServiceData == INVALID_HANDLE_VALUE)
					return FALSE;

				HRESULT hr;

				PBTH_LE_GATT_CHARACTERISTIC_VALUE pValue = (PBTH_LE_GATT_CHARACTERISTIC_VALUE)malloc(sizeof (BTH_LE_GATT_CHARACTERISTIC_VALUE)+512);
				if (pValue)
				{
					RtlZeroMemory(pValue, sizeof (BTH_LE_GATT_CHARACTERISTIC_VALUE)+512);

					pValue->DataSize = len;

					pin_ptr<BYTE> array_pin = &Data[0];
					BYTE* data_ptr = array_pin;
											
					memcpy(pValue->Data, data_ptr, len);

					hr = pWriteChar(m_hServiceData, &m_charSpeedTestStream, pValue, NULL, BLUETOOTH_GATT_FLAG_WRITE_WITHOUT_RESPONSE);

					free(pValue);
				}
				return FALSE;
			}



	};

}

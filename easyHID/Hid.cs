
using Microsoft.Win32.SafeHandles; 
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
//using System.Windows.Forms;
using System.Threading;
using System.Timers;
using System.IO;
///  <summary>
///  For communicating with HID-class USB devices.
///  Includes routines for sending and receiving reports via control transfers and to 
///  retrieve information about and configure a HID.
///  </summary>
///  

namespace easyHID
{  
    public partial class Hid  
    {         
        //  Used in error messages.
        
        private const String MODULE_NAME = "Hid";
        //private static System.Timers.Timer tmrReadTimeout;
        internal HIDP_CAPS Capabilities; 
        internal HIDD_ATTRIBUTES DeviceAttributes;
        uint CallBackBytesRead;

        System.Threading.ManualResetEvent ResetEvent = new ManualResetEvent(false);



        ///  <summary>
        ///  Retrieves a structure with information about a device's capabilities. 
        ///  </summary>
        ///  
        ///  <param name="hidHandle"> a handle to a device. </param>
        ///  
        ///  <returns>
        ///  An HIDP_CAPS structure.
        ///  </returns>
        
        internal HIDP_CAPS GetDeviceCapabilities( SafeFileHandle hidHandle ) 
        {             
            IntPtr preparsedData = new System.IntPtr(); 
            Int32 result = 0; 
            Boolean success = false;  

			try
			{
				//  ***
				//  API function: HidD_GetPreparsedData

				//  Purpose: retrieves a pointer to a buffer containing information about the device's capabilities.
				//  HidP_GetCaps and other API functions require a pointer to the buffer.

				//  Requires: 
				//  A handle returned by CreateFile.
				//  A pointer to a buffer.

				//  Returns:
				//  True on success, False on failure.
				//  ***

				success = HidD_GetPreparsedData(hidHandle, ref preparsedData);

				//  ***
				//  API function: HidP_GetCaps

				//  Purpose: find out a device's capabilities.
				//  For standard devices such as joysticks, you can find out the specific
				//  capabilities of the device.
				//  For a custom device where the software knows what the device is capable of,
				//  this call may be unneeded.

				//  Accepts:
				//  A pointer returned by HidD_GetPreparsedData
				//  A pointer to a HIDP_CAPS structure.

				//  Returns: True on success, False on failure.
				//  ***

				result = HidP_GetCaps(preparsedData, ref Capabilities);
				if ((result != 0))
				{
					Debug.WriteLine("");
					Debug.WriteLine("  Usage: " + Convert.ToString(Capabilities.Usage, 16));
					Debug.WriteLine("  Usage Page: " + Convert.ToString(Capabilities.UsagePage, 16));
					Debug.WriteLine("  Input Report Byte Length: " + Capabilities.InputReportByteLength);
					Debug.WriteLine("  Output Report Byte Length: " + Capabilities.OutputReportByteLength);
					Debug.WriteLine("  Feature Report Byte Length: " + Capabilities.FeatureReportByteLength);
					Debug.WriteLine("  Number of Link Collection Nodes: " + Capabilities.NumberLinkCollectionNodes);
					Debug.WriteLine("  Number of Input Button Caps: " + Capabilities.NumberInputButtonCaps);
					Debug.WriteLine("  Number of Input Value Caps: " + Capabilities.NumberInputValueCaps);
					Debug.WriteLine("  Number of Input Data Indices: " + Capabilities.NumberInputDataIndices);
					Debug.WriteLine("  Number of Output Button Caps: " + Capabilities.NumberOutputButtonCaps);
					Debug.WriteLine("  Number of Output Value Caps: " + Capabilities.NumberOutputValueCaps);
					Debug.WriteLine("  Number of Output Data Indices: " + Capabilities.NumberOutputDataIndices);
					Debug.WriteLine("  Number of Feature Button Caps: " + Capabilities.NumberFeatureButtonCaps);
					Debug.WriteLine("  Number of Feature Value Caps: " + Capabilities.NumberFeatureValueCaps);
					Debug.WriteLine("  Number of Feature Data Indices: " + Capabilities.NumberFeatureDataIndices);

					//  ***
					//  API function: HidP_GetValueCaps

					//  Purpose: retrieves a buffer containing an array of HidP_ValueCaps structures.
					//  Each structure defines the capabilities of one value.
					//  This application doesn't use this data.

					//  Accepts:
					//  A report type enumerator from hidpi.h,
					//  A pointer to a buffer for the returned array,
					//  The NumberInputValueCaps member of the device's HidP_Caps structure,
					//  A pointer to the PreparsedData structure returned by HidD_GetPreparsedData.

					//  Returns: True on success, False on failure.
					//  ***                    
					
					Int32 vcSize = Capabilities.NumberInputValueCaps;
					Byte[] valueCaps = new Byte[vcSize];
					
					result = HidP_GetValueCaps(HidP_Input, valueCaps, ref vcSize, preparsedData);
					
					// (To use this data, copy the ValueCaps byte array into an array of structures.)                   

				}
			}
			catch (Exception ex)
			{
				DisplayException(MODULE_NAME, ex);
                //MessageBox.Show("getDeviceCapabilities()");   
			}
			finally
			{
				 //  ***
					//  API function: HidD_FreePreparsedData
                    
					//  Purpose: frees the buffer reserved by HidD_GetPreparsedData.
                    
					//  Accepts: A pointer to the PreparsedData structure returned by HidD_GetPreparsedData.
                    
					//  Returns: True on success, False on failure.
					//  ***

				if (preparsedData != IntPtr.Zero)
				{
					success = HidD_FreePreparsedData(preparsedData);
				}
			} 
            
            return Capabilities;             
        }

		///  <summary>
		///  reads a Feature report from the device.
		///  </summary>
		///  
		///  <param name="hidHandle"> the handle for learning about the device and exchanging Feature reports. </param>	
		///  <param name="myDeviceDetected"> tells whether the device is currently attached.</param>
		///  <param name="inFeatureReportBuffer"> contains the requested report.</param>
		///  <param name="success"> read success</param>

		internal Boolean GetFeatureReport(SafeFileHandle hidHandle, ref Byte[] inFeatureReportBuffer)
		{
			Boolean success;

			try
			{
				//  ***
				//  API function: HidD_GetFeature
				//  Attempts to read a Feature report from the device.

				//  Requires:
				//  A handle to a HID
				//  A pointer to a buffer containing the report ID and report
				//  The size of the buffer. 

				//  Returns: true on success, false on failure.
				//  ***                    

				success = HidD_GetFeature(hidHandle, inFeatureReportBuffer, inFeatureReportBuffer.Length);

				Debug.Print("HidD_GetFeature success = " + success);
				return success;
			}
			catch (Exception ex)
			{
				DisplayException(MODULE_NAME, ex);
                Debug.WriteLine("getFeaureReport()");
                throw;
			}
		}             


        ///  <summary>
        ///  Creates a 32-bit Usage from the Usage Page and Usage ID. 
        ///  Determines whether the Usage is a system mouse or keyboard.
        ///  Can be modified to detect other Usages.
        ///  </summary>
        ///  
        ///  <param name="MyCapabilities"> a HIDP_CAPS structure retrieved with HidP_GetCaps. </param>
        ///  
        ///  <returns>
        ///  A String describing the Usage.
        ///  </returns>
        
        internal String GetHidUsage( HIDP_CAPS MyCapabilities ) 
        {             
            Int32 usage = 0; 
            String usageDescription = ""; 
            
            try 
            { 
                //  Create32-bit Usage from Usage Page and Usage ID.
                
                usage = MyCapabilities.UsagePage * 256 + MyCapabilities.Usage; 
                
                if ( usage == Convert.ToInt32( 0X102 ) )
                 { 
                    usageDescription = "mouse"; } 
                
                if ( usage == Convert.ToInt32( 0X106 ) )
                 { 
                    usageDescription = "keyboard"; }                   
            } 
            catch ( Exception ex ) 
            { 
                DisplayException( MODULE_NAME, ex );
                Debug.WriteLine("getHidUsage()");   
            } 
            
            return usageDescription;             
        }


		///  <summary>
		///  reads an Input report from the device using a control transfer.
		///  </summary>
		///  
		///  <param name="hidHandle"> the handle for learning about the device and exchanging Feature reports. </param>
		///  <param name="myDeviceDetected"> tells whether the device is currently attached. </param>
		///  <param name="inputReportBuffer"> contains the requested report. </param>
		///  <param name="success"> read success </param>

		internal Boolean GetInputReportViaControlTransfer(SafeFileHandle hidHandle, ref Byte[] inputReportBuffer)
		{
			Boolean success= false;

			try
			{
				//  ***
				//  API function: HidD_GetInputReport

				//  Purpose: Attempts to read an Input report from the device using a control transfer.
				//  Supported under Windows XP and later only.

				//  Requires:
				//  A handle to a HID
				//  A pointer to a buffer containing the report ID and report
				//  The size of the buffer. 

				//  Returns: true on success, false on failure.
				//  ***

				success = HidD_GetInputReport(hidHandle, inputReportBuffer, inputReportBuffer.Length + 1);

				Debug.Print("HidD_GetInputReport success = " + success);
				return success;
			}
			catch (Exception ex)
			{
				DisplayException(MODULE_NAME, ex);
                Debug.WriteLine("GetInputReportViaControlTransfer()");   
				throw;
			}
		} 
		        
        ///  <summary>
        ///  Retrieves the number of Input reports the host can store.
        ///  </summary>
        ///  
        ///  <param name="hidDeviceObject"> a handle to a device  </param>
        ///  <param name="numberOfInputBuffers"> an integer to hold the returned value. </param>
        ///  
        ///  <returns>
        ///  True on success, False on failure.
        ///  </returns>
        
        internal Boolean GetNumberOfInputBuffers( SafeFileHandle hidDeviceObject, ref Int32 numberOfInputBuffers ) 
        {             
            Boolean success = false;

            try
            {
                
                {
                
                    success = HidD_GetNumInputBuffers(hidDeviceObject, ref numberOfInputBuffers);
                }

                return success;
            }
            catch (Exception ex)
            {
                DisplayException( MODULE_NAME, ex );
                Debug.WriteLine("GetNumberOfInputBuffers()");   
                throw ; 
            }                       
        }
		///  <summary>
		///  writes a Feature report to the device.
		///  </summary>
		///  
		///  <param name="outFeatureReportBuffer"> contains the report ID and report data. </param>
		///  <param name="hidHandle"> handle to the device.  </param>
		///  
		///  <returns>
		///   True on success. False on failure.
		///  </returns>            

		internal Boolean SendFeatureReport(SafeFileHandle hidHandle, Byte[] outFeatureReportBuffer)
		{
			Boolean success = false;

			try
			{
				//  ***
				//  API function: HidD_SetFeature

				//  Purpose: Attempts to send a Feature report to the device.

				//  Accepts:
				//  A handle to a HID
				//  A pointer to a buffer containing the report ID and report
				//  The size of the buffer. 

				//  Returns: true on success, false on failure.
				//  ***

				success = HidD_SetFeature(hidHandle, outFeatureReportBuffer, outFeatureReportBuffer.Length);

				Debug.Print("HidD_SetFeature success = " + success);

				return success;
			}
			catch (Exception ex)
			{
				DisplayException(MODULE_NAME, ex);
				throw;
			}
		}             
		    ///  <summary>
            ///  Writes an Output report to the device using a control transfer.
            ///  </summary>
            ///  
            ///  <param name="outputReportBuffer"> contains the report ID and report data. </param>
            ///  <param name="hidHandle"> handle to the device.  </param>
            ///  
            ///  <returns>
            ///   True on success. False on failure.
            ///  </returns>            
            
            internal Boolean SendOutputReportViaControlTransfer(SafeFileHandle hidHandle, Byte[] outputReportBuffer) 
            {                 
                Boolean success = false;

                try
                {
                    //  ***
                    //  API function: HidD_SetOutputReport

                    //  Purpose: 
                    //  Attempts to send an Output report to the device using a control transfer.
                    //  Requires Windows XP or later.

                    //  Accepts:
                    //  A handle to a HID
                    //  A pointer to a buffer containing the report ID and report
                    //  The size of the buffer. 

                    //  Returns: true on success, false on failure.
                    //  ***                    
                    //for (int i = 0; i < 3 && !success; i++ )
                    success = HidD_SetOutputReport(hidHandle, outputReportBuffer, outputReportBuffer.Length);
                   

                    Debug.Print("HidD_SetOutputReport success = " + success);

                    return success;
                }
                catch (Exception ex)
                {
                    DisplayException(MODULE_NAME, ex);
                    Debug.WriteLine("SendOutputReportViaControlTransfer()");
                    //throw ; 
                    return success;
                }
                
            }    

        ///  <summary>
        ///  sets the number of input reports the host will store.
        ///  Requires Windows XP or later.
        ///  </summary>
        ///  
        ///  <param name="hidDeviceObject"> a handle to the device.</param>
        ///  <param name="numberBuffers"> the requested number of input reports.  </param>
        ///  
        ///  <returns>
        ///  True on success. False on failure.
        ///  </returns>
        
        internal Boolean SetNumberOfInputBuffers( SafeFileHandle hidDeviceObject, Int32 numberBuffers ) 
        {              
            try 
            { 
               
                {                     
                    //  ***
                    //  API function: HidD_SetNumInputBuffers
                    
                    //  Purpose: Sets the number of Input reports the host can store.
                    //  If the buffer is full and another report arrives, the host drops the 
                    //  oldest report.
                    
                    //  Requires:
                    //  A handle to a HID
                    //  An integer to hold the number of buffers. 
                    
                    //  Returns: true on success, false on failure.
                    //  ***
                    
                    HidD_SetNumInputBuffers( hidDeviceObject, numberBuffers );
                    return true;                    
                } 
               
                            } 
            catch ( Exception ex ) 
            { 
                DisplayException( MODULE_NAME, ex ); 
                throw ; 
            }            
        } 
                
       
        

        ///  <summary>
        ///  Provides a central mechanism for exception handling.
        ///  Displays a message box that describes the exception.
        ///  </summary>
        ///  
        ///  <param name="moduleName">  the module where the exception occurred. </param>
        ///  <param name="e"> the exception </param>
        
        internal static void DisplayException( String moduleName, Exception e ) 
        {             
            String message = null; 
            String caption = null; 
            
            //  Create an error message.
            
            message = "Exception: " + e.Message + "\r\n" + "Module: " + moduleName + "\r\n" + "Method: " + e.TargetSite.Name; 
            
            caption = "My Exception"; 
            
            Debug.Write( message );             
        }       
  
          public bool isDeviceAttached
            {
            get
                {
                return deviceInformation.deviceAttached;
                }
            private set
                {
                deviceInformation.deviceAttached = value;
                }
            }

        public bool OutputControl(byte[] outputReportBuffer)
        {
            String byteValue = null;
            Int32 count = 0;
            bool ret = false;
           // Byte[] inputReportBuffer = null;
            Boolean success = false;
            try
            {
                success = false;
                if (!deviceInformation.hidUsbHandle.IsInvalid)
                {
                   

                        //  Store the report ID in the first byte of the buffer:

                        outputReportBuffer[0] = 0;

                        success = SendOutputReportViaControlTransfer(deviceInformation.hidUsbHandle, outputReportBuffer);
                        
                        if (success)
                        {
                           
                            //  Display the report data in the form's list box.

                           
                           
                            for (count = 0; count <= outputReportBuffer.Length - 1; count++)
                            {
                                //  Display bytes as 2-character hex strings.

                                byteValue = String.Format("{0:X2} ", outputReportBuffer[count]);
                              
                            }
                            ret = true;
                        }
                        else
                        {
                            detachUsbDevice();
                         
                            ret = false;
                        }
                    }
                    else
                    {
                        ;
                    }
            }
            catch (Exception ex)
            {
                DisplayException(this.Name, ex);
                Debug.WriteLine("OutputControl exception");
            }
             return ret;
        }
       
          public bool OutputReports(byte[] outputReportBuffer)
          {
                  bool success = false;
                  int numberOfBytesWritten = 0;

                  // Make sure a device is attached
                  if (!isDeviceAttached)
                  {
                      Debug.WriteLine("EasyUsb:writeReportToDevice(): -> No device attached!");
                      return success;
                  }
                  try
                  {
                      // Set an output report via interrupt to the device
                      success = WriteFile(
                          deviceInformation.writeHandle,
                          outputReportBuffer,
                          outputReportBuffer.Length,
                          ref numberOfBytesWritten,
                          IntPtr.Zero);

                      if (success) Debug.WriteLine("EasyUsb:writeReportToDevice(): -> Write report succeeded");
                      else Debug.WriteLine("EasyUsb:writeReportToDevice(): -> Write report failed!");

                      return success;
                  }
                  catch (Exception)
                  {
                      // An error - send out some debug and return failure
                      Debug.WriteLine("EasyUsb:writeReportToDevice(): -> EXCEPTION: When attempting to send an output report");
                      return false;
                  }  
          }
            
          public bool inputControl(ref byte[] inputReportBuffer)
          {
              String byteValue = null;
              Int32 count = 0;
              
              Boolean success = false;
              try
              {
                  success = false;
                  if (!deviceInformation.hidUsbHandle.IsInvalid)
                  {
                      //  Read a report using a control transfer.

                      success = GetInputReportViaControlTransfer(deviceInformation.hidUsbHandle, ref inputReportBuffer);

                      if (success)
                      {

                          //  Display the report data received in the form's list box.

                          for (count = 0; count <= inputReportBuffer.Length - 1; count++)
                          {
                              //  Display bytes as 2-character Hex strings.

                              byteValue = String.Format("{0:X2} ", inputReportBuffer[count]);

                          }
                      }
                      else
                      {
                          detachUsbDevice();
                         
                      }

                  }
                  else
                  {
                      ;
                  }
              }
              catch (Exception ex)
              {
                 
                  Debug.WriteLine("InputControl()");
              }
              return success;
          }
          unsafe void DeviceWriteControlIOCompletionCallback(uint errorCode, uint numBytes, NativeOverlapped* nativeOverlapped)
          {
              try
              {
                  Debug.WriteLine("Callback called...");
                  CallBackBytesRead = numBytes;
                  ResetEvent.Set();
              }
              finally
              {
                  System.Threading.Overlapped.Unpack(nativeOverlapped);
                  System.Threading.Overlapped.Free(nativeOverlapped);
              }
          }
          const int ErrorIOPending = 997;
          public bool inputReports(ref byte[] inputReportBuffer, ref int numberRead)
          {
              
               unsafe{

                  IntPtr eventObject = IntPtr.Zero;
                  //AutoResetEvent event_2 = new AutoResetEvent(false);
                  //NativeOverlapped hidOverlapped = new NativeOverlapped();
                  IntPtr nonManagedBuffer = IntPtr.Zero;
                  IntPtr nonManagedOverlapped = IntPtr.Zero;
                  bool result = false;
                  bool success =false;
                  int numberOfBytesRead = 0;
                  
                  // Make sure a device is attached
                  if (!isDeviceAttached)
                  {
                      Debug.WriteLine("EasyUsb:readReportFromDevice(): -> No device attached!");
                      return false;
                  }

                  try
                  {
                      // Prepare an event object for the overlapped ReadFile
                      //eventObject = CreateEvent(IntPtr.Zero, false, false, "");

                      //hidOverlapped.OffsetLow = 0;
                      //hidOverlapped.OffsetHigh = 0;
                      //hidOverlapped.EventHandle = eventObject;

                      // Allocate memory for the unmanaged input buffer and overlap structure.
                      nonManagedBuffer = Marshal.AllocHGlobal(inputReportBuffer.Length);
                      //nonManagedOverlapped = Marshal.AllocHGlobal(Marshal.SizeOf(hidOverlapped));
                      //Marshal.StructureToPtr(hidOverlapped, nonManagedOverlapped, false);
                      Overlapped overlapped = new Overlapped();

                      eventObject = CreateEvent(IntPtr.Zero, false, false, "");

                      overlapped.EventHandleIntPtr = eventObject;
                      overlapped.OffsetHigh = 0;
                      overlapped.OffsetLow = 0;

                      NativeOverlapped* nativeOverlapped = overlapped.Pack(
                      DeviceWriteControlIOCompletionCallback,                 //for some reason this isn't being called..
                      null        //eventObject
                      );
                      ResetEvent.Reset();
                      // Read the input report buffer
                      Debug.WriteLine("EasyUsb:readReportFromDevice(): -> Attempting to ReadFile");
                      success = ReadFile(
                          deviceInformation.readHandle,
                          nonManagedBuffer,
                          inputReportBuffer.Length,
                          ref numberOfBytesRead,
                          nativeOverlapped);

                      if (!success)
                      {
                          // We are now waiting for the FileRead to complete
                          Debug.WriteLine("EasyUsb:readReportFromDevice(): -> ReadFile started, waiting for completion...");

                          // Wait a maximum of 3 seconds for the FileRead to complete
                          result = ResetEvent.WaitOne(deviceInformation.timeOut);
                          numberOfBytesRead = (int)CallBackBytesRead;

                          if (result)
                          {
                              // Has the ReadFile completed successfully?
                              //case (System.Int32)WAIT_OBJECT_0:
                                  success = true;
                                  //System.Threading.Overlapped.Unpack(nativeOverlapped);
                                  //System.Threading.Overlapped.Free(nativeOverlapped);
                                  CancelIo(deviceInformation.readHandle);
                                  // Get the number of bytes transferred
                                 // GetOverlappedResult(deviceInformation.readHandle, nonManagedOverlapped, ref numberOfBytesRead, false);
                                  //numberRead = numberOfBytesRead;
                                  Debug.WriteLine(string.Format("EasyUsb:readReportFromDevice(): -> ReadFile successful (overlapped) {0} bytes read", numberOfBytesRead));
                                  //break;

                              // Did the FileRead operation timeout?
                                  
                          }
                          else
                          {

                              int error = Marshal.GetLastWin32Error();
                              Debug.WriteLine("Last Error: " + error);
                                  success = false;
                                  numberRead = 0;
                            // Cancel the ReadFile operation
                            if (!deviceInformation.readHandle.IsClosed)
                                CancelIo(deviceInformation.readHandle);
                            //if (!deviceInformation.hidHandle.IsInvalid) deviceInformation.hidHandle.Close();
                            //if (!deviceInformation.readHandle.IsInvalid) deviceInformation.readHandle.Close();
                            //if (!deviceInformation.writeHandle.IsInvalid) deviceInformation.writeHandle.Close();

                            // Detach the USB device to try to get us back in a known state
                            //detachUsbDevice();
                            //int error = Marshal.GetLastWin32Error();
                            //if (ErrorIOPending != error)
                            {
                                // failed to execute DeviceIoControl using overlapped I/O ...

                                //System.Threading.Overlapped.Unpack(nativeOverlapped);
                                //System.Threading.Overlapped.Free(nativeOverlapped);
                            }
                             Debug.WriteLine("EasyUsb:readReportFromDevice(): -> ReadFile timedout!");
                          }
                          
                      }
                      if (success)
                      {
                          // Report receieved correctly, copy the unmanaged input buffer over to the managed buffer
                          Marshal.Copy(nonManagedBuffer, inputReportBuffer, 0, numberOfBytesRead);
                          Debug.WriteLine(string.Format("EasyUsb:readReportFromDevice(): -> ReadFile successful {0} bytes read", numberOfBytesRead));
                      }
                  }
                  catch (Exception)
                  {
                      // An error - send out some debug and return failure
                      Debug.WriteLine("EasyUsb:readReportFromDevice(): -> EXCEPTION: When attempting to receive an input report");
                      //return false;
                  }
                  try
                  {
                      // Release non-managed objects before returning
                      Marshal.FreeHGlobal(nonManagedBuffer);
                      Marshal.FreeHGlobal(nonManagedOverlapped);

                      // Close the file handle to release the object
                      CloseHandle(eventObject);
                  }
                  catch (Exception)
                  {
                      Debug.WriteLine("EasyUsb:readReportFromDevice(): -> EXCEPTION: When release resource");
                  }
                  return success;
              }
         }
    } 
} 

///  <summary>
///  Routines for detecting devices and receiving device notifications.
///  </summary>

using Microsoft.Win32.SafeHandles; 
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Timers;
 
using System.Collections.Generic;
using System.Threading;
using System.Text;

public class gftEventArgs : EventArgs
{
    public gftEventArgs(gftEvents newEvent) {
        this.gftEvent = newEvent;
    }

    public gftEvents gftEvent;

    public enum gftEvents
    {
        CONNECTED,
        DISCONNECTED
    }
}


namespace easyHID
{
	public partial class Hid : Control
	{
        
        private struct deviceInformationStructure
        {
            public UInt16 targetVid;                // Our target device's VID
            public UInt16 targetPid;                // Our target device's PID                  
            public bool deviceAttached;             // Device attachment state flag
            public UInt16 timeOut;
            public HIDD_ATTRIBUTES attributes;      // HID Attributes
            public HIDP_CAPS capabilities;          // HID Capabilities
            public SafeFileHandle readHandle;       // Read handle from the device
            public SafeFileHandle writeHandle;      // Write handle to the device
            public FileStream fileStreamDeviceData;
            public SafeFileHandle hidUsbHandle;    
            public String devicePathName;           // The device's path name
            public IntPtr deviceNotificationHandle; // The device's notification handle
            public bool isReadSuccess;
        };
        deviceInformationStructure deviceInformation;

        public delegate void usbEventsHandler(object sender, gftEventArgs ge);

        public event usbEventsHandler usbEvent;

        /// <summary>
        /// Define the event
        /// </summary>

        public Hid(IntPtr Handle,int vid, int pid)
        {
            Debug.WriteLine("easyHID:easyHID() -> Class constructor called");

            // Set the deviceAttached flag to false
            deviceInformation.deviceAttached = false;

            // Store the target device's VID and PID in the device attributes
            deviceInformation.targetVid = (UInt16)vid;
            deviceInformation.targetPid = (UInt16)pid;
            // Register for device notifications

            //myRegisterForDeviceNotifications(this.Handle);
            myRegisterForDeviceNotifications(Handle);
            deviceInformation.timeOut = 1000;
        }
        public void setTimeOut(UInt16 time)
        {
            deviceInformation.timeOut = time;

        }
       
        /// <summary>
        /// The usb event thrower
        /// </summary>
        /// <param name="e"></param>
        protected virtual void onUsbEvent(gftEventArgs ge)
        {
            if (usbEvent != null)
            {
                Debug.WriteLine("easyHIDs:onUsbEvent() -> Throwing a USB event to a listener");
                usbEvent(this, ge);
            }
            else Debug.WriteLine("easyHIDs:onUsbEvent() -> Attempted to throw a USB event, but no one was listening");
        }
   
		///  <summary>
		///  Compares two device path names. Used to find out if the device name 
		///  of a recently attached or removed device matches the name of a 
		///  device the application is communicating with.
		///  </summary>
		///  
		///  <param name="m"> a WM_DEVICECHANGE message. A call to RegisterDeviceNotification
		///  causes WM_DEVICECHANGE messages to be passed to an OnDeviceChange routine.. </param>
		///  <param name="mydevicePathName"> a device pathname returned by 
		///  SetupDiGetDeviceInterfaceDetail in an SP_DEVICE_INTERFACE_DETAIL_DATA structure. </param>
		///  
		///  <returns>
		///  True if the names match, False if not.
		///  </returns>
		///  
		internal Boolean DeviceNameMatch(Message m, String mydevicePathName)
		{
			Int32 stringSize;

			try
			{
				DEV_BROADCAST_DEVICEINTERFACE_1 devBroadcastDeviceInterface = new DEV_BROADCAST_DEVICEINTERFACE_1();
				DEV_BROADCAST_HDR devBroadcastHeader = new DEV_BROADCAST_HDR();

				// The LParam parameter of Message is a pointer to a DEV_BROADCAST_HDR structure.

				Marshal.PtrToStructure(m.LParam, devBroadcastHeader);

				if ((devBroadcastHeader.dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE))
				{
					// The dbch_devicetype parameter indicates that the event applies to a device interface.
					// So the structure in LParam is actually a DEV_BROADCAST_INTERFACE structure, 
					// which begins with a DEV_BROADCAST_HDR.

					// Obtain the number of characters in dbch_name by subtracting the 32 bytes
					// in the strucutre that are not part of dbch_name and dividing by 2 because there are 
					// 2 bytes per character.

					stringSize = System.Convert.ToInt32((devBroadcastHeader.dbch_size - 32) / 2);

					// The dbcc_name parameter of devBroadcastDeviceInterface contains the device name. 
					// Trim dbcc_name to match the size of the String.         

					devBroadcastDeviceInterface.dbcc_name = new Char[stringSize + 1];

					// Marshal data from the unmanaged block pointed to by m.LParam 
					// to the managed object devBroadcastDeviceInterface.

					Marshal.PtrToStructure(m.LParam, devBroadcastDeviceInterface);

					// Store the device name in a String.

					String DeviceNameString = new String(devBroadcastDeviceInterface.dbcc_name, 0, stringSize);

					// Compare the name of the newly attached device with the name of the device 
					// the application is accessing (mydevicePathName).
					// Set ignorecase True.

					if ((String.Compare(DeviceNameString, mydevicePathName, true) == 0))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			catch (Exception ex)
			{
                MessageBox.Show("DeviceNameMatch()");
			}

			return false;
		}

		///  <summary>
		///  Use SetupDi API functions to retrieve the device path name of an
		///  attached device that belongs to a device interface class.
		///  </summary>
		///  
		///  <param name="myGuid"> an interface class GUID. </param>
		///  <param name="devicePathName"> a pointer to the device path name 
		///  of an attached device. </param>
		///  
		///  <returns>
		///   True if a device is found, False if not. 
		///  </returns>

		internal Boolean FindDeviceFromGuid(System.Guid myGuid, ref String[] devicePathName)
		{
			Int32 bufferSize = 0;
			IntPtr detailDataBuffer = IntPtr.Zero;
			Boolean deviceFound;
			IntPtr deviceInfoSet = new System.IntPtr();
			Boolean lastDevice = false;
			Int32 memberIndex = 0;
			SP_DEVICE_INTERFACE_DATA MyDeviceInterfaceData = new SP_DEVICE_INTERFACE_DATA();
			Boolean success;

			try
			{
				// ***
				//  API function

				//  summary 
				//  Retrieves a device information set for a specified group of devices.
				//  SetupDiEnumDeviceInterfaces uses the device information set.

				//  parameters 
				//  Interface class GUID.
				//  Null to retrieve information for all device instances.
				//  Optional handle to a top-level window (unused here).
				//  Flags to limit the returned information to currently present devices 
				//  and devices that expose interfaces in the class specified by the GUID.

				//  Returns
				//  Handle to a device information set for the devices.
				// ***

				deviceInfoSet = SetupDiGetClassDevs(ref myGuid, IntPtr.Zero, IntPtr.Zero, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

				deviceFound = false;
				memberIndex = 0;

				// The cbSize element of the MyDeviceInterfaceData structure must be set to
				// the structure's size in bytes. 
				// The size is 28 bytes for 32-bit code and 32 bits for 64-bit code.

				MyDeviceInterfaceData.cbSize = Marshal.SizeOf(MyDeviceInterfaceData);

				do
				{
					// Begin with 0 and increment through the device information set until
					// no more devices are available.
				
					// ***
					//  API function

					//  summary
					//  Retrieves a handle to a SP_DEVICE_INTERFACE_DATA structure for a device.
					//  On return, MyDeviceInterfaceData contains the handle to a
					//  SP_DEVICE_INTERFACE_DATA structure for a detected device.

					//  parameters
					//  DeviceInfoSet returned by SetupDiGetClassDevs.
					//  Optional SP_DEVINFO_DATA structure that defines a device instance 
					//  that is a member of a device information set.
					//  Device interface GUID.
					//  Index to specify a device in a device information set.
					//  Pointer to a handle to a SP_DEVICE_INTERFACE_DATA structure for a device.

					//  Returns
					//  True on success.
					// ***

					success = SetupDiEnumDeviceInterfaces
						(deviceInfoSet,
						IntPtr.Zero,
						ref myGuid,
						memberIndex,
						ref MyDeviceInterfaceData);

					// Find out if a device information set was retrieved.

					if (!success)
					{
						lastDevice = true;

					}
					else
					{
						// A device is present.

						// ***
						//  API function: 

						//  summary:
						//  Retrieves an SP_DEVICE_INTERFACE_DETAIL_DATA structure
						//  containing information about a device.
						//  To retrieve the information, call this function twice.
						//  The first time returns the size of the structure.
						//  The second time returns a pointer to the data.

						//  parameters
						//  DeviceInfoSet returned by SetupDiGetClassDevs
						//  SP_DEVICE_INTERFACE_DATA structure returned by SetupDiEnumDeviceInterfaces
						//  A returned pointer to an SP_DEVICE_INTERFACE_DETAIL_DATA 
						//  Structure to receive information about the specified interface.
						//  The size of the SP_DEVICE_INTERFACE_DETAIL_DATA structure.
						//  Pointer to a variable that will receive the returned required size of the 
						//  SP_DEVICE_INTERFACE_DETAIL_DATA structure.
						//  Returned pointer to an SP_DEVINFO_DATA structure to receive information about the device.

						//  Returns
						//  True on success.
						// ***                     

						success = SetupDiGetDeviceInterfaceDetail
							(deviceInfoSet,
							ref MyDeviceInterfaceData,
							IntPtr.Zero,
							0,
							ref bufferSize,
							IntPtr.Zero);

						// Allocate memory for the SP_DEVICE_INTERFACE_DETAIL_DATA structure using the returned buffer size.

						detailDataBuffer = Marshal.AllocHGlobal(bufferSize);

						// Store cbSize in the first bytes of the array. The number of bytes varies with 32- and 64-bit systems.

						Marshal.WriteInt32(detailDataBuffer, (IntPtr.Size == 4) ? (4 + Marshal.SystemDefaultCharSize) : 8);

						// Call SetupDiGetDeviceInterfaceDetail again.
						// This time, pass a pointer to DetailDataBuffer
						// and the returned required buffer size.

						success = SetupDiGetDeviceInterfaceDetail
							(deviceInfoSet,
							ref MyDeviceInterfaceData,
							detailDataBuffer,
							bufferSize,
							ref bufferSize,
							IntPtr.Zero);

						// Skip over cbsize (4 bytes) to get the address of the devicePathName.

						IntPtr pDevicePathName = new IntPtr(detailDataBuffer.ToInt32() + 4);

						// Get the String containing the devicePathName.

						devicePathName[memberIndex] = Marshal.PtrToStringAuto(pDevicePathName);

						if (detailDataBuffer != IntPtr.Zero)
						{
							// Free the memory allocated previously by AllocHGlobal.

							Marshal.FreeHGlobal(detailDataBuffer);
						}
						deviceFound = true;
					}
					memberIndex = memberIndex + 1;
				}
				while (!((lastDevice == true)));

				

				return deviceFound;
			}
			catch (Exception ex)
			{
				
                MessageBox.Show("FindDeviceFromGuid()");
                throw;
			}
				finally
			{
				
				// ***
				//  API function

				//  summary
				//  Frees the memory reserved for the DeviceInfoSet returned by SetupDiGetClassDevs.

				//  parameters
				//  DeviceInfoSet returned by SetupDiGetClassDevs.

				//  returns
				//  True on success.
				// ***

				if (deviceInfoSet != IntPtr.Zero)
				{
					SetupDiDestroyDeviceInfoList(deviceInfoSet);
				}
			}
		}
	
#if false
		///  <summary>
		///  Requests to receive a notification when a device is attached or removed.
		///  </summary>
		///  
		///  <param name="devicePathName"> handle to a device. </param>
		///  <param name="formHandle"> handle to the window that will receive device events. </param>
		///  <param name="classGuid"> device interface GUID. </param>
		///  <param name="deviceNotificationHandle"> returned device notification handle. </param>
		///  
		///  <returns>
		///  True on success.
		///  </returns>
		///  
		internal Boolean RegisterForDeviceNotifications(String devicePathName, IntPtr formHandle, Guid classGuid, ref IntPtr deviceNotificationHandle)
		{
			// A DEV_BROADCAST_DEVICEINTERFACE header holds information about the request.

			DEV_BROADCAST_DEVICEINTERFACE devBroadcastDeviceInterface = new DEV_BROADCAST_DEVICEINTERFACE();
			IntPtr devBroadcastDeviceInterfaceBuffer = IntPtr.Zero;
			Int32 size = 0;

			try
			{
				// Set the parameters in the DEV_BROADCAST_DEVICEINTERFACE structure.

				// Set the size.

				size = Marshal.SizeOf(devBroadcastDeviceInterface);
				devBroadcastDeviceInterface.dbcc_size = size;

				// Request to receive notifications about a class of devices.

				devBroadcastDeviceInterface.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;

				devBroadcastDeviceInterface.dbcc_reserved = 0;

				// Specify the interface class to receive notifications about.

				devBroadcastDeviceInterface.dbcc_classguid = classGuid;

				// Allocate memory for the buffer that holds the DEV_BROADCAST_DEVICEINTERFACE structure.

				devBroadcastDeviceInterfaceBuffer = Marshal.AllocHGlobal(size);

				// Copy the DEV_BROADCAST_DEVICEINTERFACE structure to the buffer.
				// Set fDeleteOld True to prevent memory leaks.

				Marshal.StructureToPtr(devBroadcastDeviceInterface, devBroadcastDeviceInterfaceBuffer, true);

				// ***
				//  API function

				//  summary
				//  Request to receive notification messages when a device in an interface class
				//  is attached or removed.

				//  parameters 
				//  Handle to the window that will receive device events.
				//  Pointer to a DEV_BROADCAST_DEVICEINTERFACE to specify the type of 
				//  device to send notifications for.
				//  DEVICE_NOTIFY_WINDOW_HANDLE indicates the handle is a window handle.

				//  Returns
				//  Device notification handle or NULL on failure.
				// ***

				deviceNotificationHandle = RegisterDeviceNotification(formHandle, devBroadcastDeviceInterfaceBuffer, DEVICE_NOTIFY_WINDOW_HANDLE);
                deviceInformation.deviceNotificationHandle = deviceNotificationHandle;
				// Marshal data from the unmanaged block devBroadcastDeviceInterfaceBuffer to
				// the managed object devBroadcastDeviceInterface

				Marshal.PtrToStructure(devBroadcastDeviceInterfaceBuffer, devBroadcastDeviceInterface);



				if ((deviceNotificationHandle.ToInt32() == IntPtr.Zero.ToInt32()))
				{        
					return false;
				}
				else
				{
					return true;
				}
			}
			catch (Exception ex)
			{
                MessageBox.Show("RegisterForDeviceNotifications()");
                throw;
			}
			finally
			{
				if (devBroadcastDeviceInterfaceBuffer != IntPtr.Zero)
				{
					// Free the memory allocated previously by AllocHGlobal.

					Marshal.FreeHGlobal(devBroadcastDeviceInterfaceBuffer);
				}
			}
		}
#endif
		///  <summary>
		///  Requests to stop receiving notification messages when a device in an
		///  interface class is attached or removed.
		///  </summary>
		///  
		///  <param name="deviceNotificationHandle"> handle returned previously by
		///  RegisterDeviceNotification. </param>

		public void StopReceivingDeviceNotifications(IntPtr deviceNotificationHandle)
		{
			try
			{
				// ***
				//  API function

				//  summary
				//  Stop receiving notification messages.

				//  parameters
				//  Handle returned previously by RegisterDeviceNotification.  

				//  returns
				//  True on success.
				// ***

				//  Ignore failures.

				Hid.UnregisterDeviceNotification(deviceNotificationHandle);
			}
			catch (Exception ex)
			{
                MessageBox.Show("StopReceivingDeviceNotifications()");
			}
		}

        public Boolean myRegisterForDeviceNotifications(IntPtr windowHandle)
        {
            IntPtr deviceNotificationHandle;
            Debug.WriteLine("easyHID:registerForDeviceNotifications() -> Method called");

            // A DEV_BROADCAST_DEVICEINTERFACE header holds information about the request.
            DEV_BROADCAST_DEVICEINTERFACE devBroadcastDeviceInterface = new DEV_BROADCAST_DEVICEINTERFACE();
            IntPtr devBroadcastDeviceInterfaceBuffer = IntPtr.Zero;
            Int32 size = 0;

            // Get the required GUID
            System.Guid systemHidGuid = new Guid();
            Hid.HidD_GetHidGuid(ref systemHidGuid);

            try
            {
                // Set the parameters in the DEV_BROADCAST_DEVICEINTERFACE structure.
                size = Marshal.SizeOf(devBroadcastDeviceInterface);
                devBroadcastDeviceInterface.dbcc_size = size;
                devBroadcastDeviceInterface.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
                devBroadcastDeviceInterface.dbcc_reserved = 0;
                devBroadcastDeviceInterface.dbcc_classguid = systemHidGuid;

                devBroadcastDeviceInterfaceBuffer = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(devBroadcastDeviceInterface, devBroadcastDeviceInterfaceBuffer, true);

                // Register for notifications and store the returned handle
                 deviceNotificationHandle = RegisterDeviceNotification(windowHandle, devBroadcastDeviceInterfaceBuffer, DEVICE_NOTIFY_WINDOW_HANDLE);
                 deviceInformation.deviceNotificationHandle = deviceNotificationHandle;
                Marshal.PtrToStructure(devBroadcastDeviceInterfaceBuffer, devBroadcastDeviceInterface);

                if (( deviceNotificationHandle.ToInt32() == IntPtr.Zero.ToInt32()))
                {
                    Debug.WriteLine("easyHID:registerForDeviceNotifications() -> Notification registration failed");
                    return false;
                }
                else
                {
                    Debug.WriteLine("easyHID:registerForDeviceNotifications() -> Notification registration succeded");
                    return true;
                }
            }
            catch (Exception)
            {
                Debug.WriteLine("easyHID:registerForDeviceNotifications() -> EXCEPTION: An unknown exception has occured!");
            }
            finally
            {
                // Free the memory allocated previously by AllocHGlobal.
                if (devBroadcastDeviceInterfaceBuffer != IntPtr.Zero)
                    Marshal.FreeHGlobal(devBroadcastDeviceInterfaceBuffer);
            }
            return false;
        }

        private bool findHidDevices(ref String[] listOfDevicePathNames, ref int numberOfDevicesFound)
        {
           
            
            // Detach the device if it's currently attached
            if (isDeviceAttached) detachUsbDevice();
            // Initialise the internal variables required for performing the search
            Int32 bufferSize = 0;
            IntPtr detailDataBuffer = IntPtr.Zero;
            Boolean deviceFound;
            IntPtr deviceInfoSet = new System.IntPtr();
            Boolean lastDevice = false;
            Int32 listIndex = 0;
            SP_DEVICE_INTERFACE_DATA deviceInterfaceData = new SP_DEVICE_INTERFACE_DATA();
            Boolean success;

            //1  Get the required GUID
            System.Guid systemHidGuid = new Guid();
            HidD_GetHidGuid(ref systemHidGuid);
            Debug.WriteLine(string.Format("easyHID:findHidDevices() -> Fetched GUID for HID devices ({0})", systemHidGuid.ToString()));

            try
            {
                // Here we populate a list of plugged-in devices matching our class GUID (DIGCF_PRESENT specifies that the list
                // should only contain devices which are plugged in)
                Debug.WriteLine("easyHID:findHidDevices() -> Using SetupDiGetClassDevs to get all devices with the correct GUID");
                //2
                deviceInfoSet = SetupDiGetClassDevs(ref systemHidGuid, IntPtr.Zero, IntPtr.Zero, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

                // Reset the deviceFound flag and the memberIndex counter
                deviceFound = false;
                listIndex = 0;

                deviceInterfaceData.cbSize = Marshal.SizeOf(deviceInterfaceData);

                // Look through the retrieved list of class GUIDs looking for a match on our interface GUID
                do
                {
                    Debug.WriteLine("easyHID:findHidDevices() -> Enumerating devices");
                    //3
                    success = SetupDiEnumDeviceInterfaces
                        (deviceInfoSet,
                        IntPtr.Zero,
                        ref systemHidGuid,
                        listIndex,
                        ref deviceInterfaceData);

                    if (!success)
                    {
                        Debug.WriteLine("easyHID:findHidDevices() -> No more devices left - giving up");
                        lastDevice = true;
                    }
                    else
                    {
                        // The target device has been found, now we need to retrieve the device path so we can open
                        // the read and write handles required for USB communication

                        // First call is just to get the required buffer size for the real request
                        success = SetupDiGetDeviceInterfaceDetail
                            (deviceInfoSet,
                            ref deviceInterfaceData,
                            IntPtr.Zero,
                            0,
                            ref bufferSize,
                            IntPtr.Zero);

                        // Allocate some memory for the buffer
                        detailDataBuffer = Marshal.AllocHGlobal(bufferSize);
                        Marshal.WriteInt32(detailDataBuffer, (IntPtr.Size == 4) ? (4 + Marshal.SystemDefaultCharSize) : 8);

                        // Second call gets the detailed data buffer
                        //Debug.WriteLine("easyHID:findHidDevices() -> Getting details of the device");
                        success = SetupDiGetDeviceInterfaceDetail
                            (deviceInfoSet,
                            ref deviceInterfaceData,
                            detailDataBuffer,
                            bufferSize,
                            ref bufferSize,
                            IntPtr.Zero);

                        // Skip over cbsize (4 bytes) to get the address of the devicePathName.
                        IntPtr pDevicePathName = new IntPtr(detailDataBuffer.ToInt32() + 4);

                        // Get the String containing the devicePathName.
                        listOfDevicePathNames[listIndex] = Marshal.PtrToStringAuto(pDevicePathName);

                        //Debug.WriteLine(string.Format("easyHID:findHidDevices() -> Found matching device (memberIndex {0})", memberIndex));
                        deviceFound = true;
                    }
                    listIndex = listIndex + 1;
                }
                while (!((lastDevice == true)));
            }
            catch (Exception)
            {
                // Something went badly wrong... output some debug and return false to indicated device discovery failure
                Debug.WriteLine("easyHID:findHidDevices() -> EXCEPTION: Something went south whilst trying to get devices with matching GUIDs - giving up!");
                return false;
            }
            finally
            {
                // Clean up the unmanaged memory allocations
                if (detailDataBuffer != IntPtr.Zero)
                {
                    // Free the memory allocated previously by AllocHGlobal.
                    Marshal.FreeHGlobal(detailDataBuffer);
                }

                if (deviceInfoSet != IntPtr.Zero)
                {
                    SetupDiDestroyDeviceInfoList(deviceInfoSet);
                }
            }

            if (deviceFound)
            {
                Debug.WriteLine(string.Format("easyHID:findHidDevices() -> Found {0} devices with matching GUID", listIndex - 1));
                numberOfDevicesFound = listIndex - 2;
            }
            else Debug.WriteLine("easyHID:findHidDevices() -> No matching devices found");

            return deviceFound;
        }
        public void findTargetDevice()
        {
            SafeFileHandle hidHandle;
            Debug.WriteLine("easyHID:findTargetDevice() -> Method called");

            bool deviceFoundByGuid = false;
            String[] listOfDevicePathNames = new String[128]; // 128 is the maximum number of USB devices allowed on a single host
            int listIndex = 0;
            bool success = false;
            int numberOfDevicesFound = 0;
            Byte[] inputReportBuffer = new byte[65];
            try
            {
               
                // Get all the devices with the correct HID GUID
                deviceFoundByGuid = findHidDevices(ref listOfDevicePathNames, ref numberOfDevicesFound);

                if (deviceFoundByGuid)
                {
                    Debug.WriteLine("easyHID:findTargetDevice() -> Devices with matching GUID found...");
                    listIndex = 0;
                    
                    do
                    {
                        Debug.WriteLine(string.Format("easyHID:findTargetDevice() -> Performing CreateFile to listIndex {0}", listIndex));
                        deviceInformation.hidUsbHandle = CreateFile(listOfDevicePathNames[listIndex], 0, FILE_SHARE_READ | FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                        if (!deviceInformation.hidUsbHandle.IsInvalid)
                        {
                            deviceInformation.attributes.size = Marshal.SizeOf(deviceInformation.attributes);
                            success = HidD_GetAttributes(deviceInformation.hidUsbHandle, ref deviceInformation.attributes);
                            if (success)
                            {
                                Debug.WriteLine(string.Format("easyHID:findTargetDevice() -> Found device with VID {0}, PID {1} and Version number {2}",
                                    Convert.ToString(deviceInformation.attributes.vendorID, 16),
                                    Convert.ToString(deviceInformation.attributes.productID, 16),
                                    Convert.ToString(deviceInformation.attributes.versionNumber, 16)));

                                //  Do the VID and PID of the device match our target device?
                                if ((deviceInformation.attributes.vendorID == deviceInformation.targetVid) &&
                                    (deviceInformation.attributes.productID == deviceInformation.targetPid))
                                {
                                    // Matching device found
                                    Debug.WriteLine("easyHID:findTargetDevice() -> Device with matching VID and PID found!");
                                     
                                    isDeviceAttached = true;
                                    // Store the device's pathname in the device information
                                    deviceInformation.devicePathName = listOfDevicePathNames[listIndex];
                                    
                                }                                         
                                else
                                {
                                    // Wrong device, close the handle
                                    deviceInformation.hidUsbHandle.Close();
                                    Debug.WriteLine("easyHID:findTargetDevice() -> Device didn't match... Continuing...");
                                   
                                }
                            }
                            else
                            {
                                //  Something went rapidly south...  give up!
                                Debug.WriteLine("easyHID:findTargetDevice() -> Something bad happened - couldn't fill the HIDD_ATTRIBUTES, giving up!");
                                 
                            }
                            
                        }

                        //  Move to the next device, or quit if there are no more devices to examine
                        listIndex++;
                    }
                    while (!(isDeviceAttached || (listIndex == numberOfDevicesFound + 1)));
                }
               
                // If we found a matching device then we need discover more details about the attached device
                // and then open read and write handles to the device to allow communication
                if ( isDeviceAttached)
                {
                    // Query the HID device's capabilities (primarily we are only really interested in the 
                    // input and output report byte lengths as this allows us to validate information sent
                    // to and from the device does not exceed the devices capabilities.
                    //
                    // We could determine the 'type' of HID device here too, but since this class is only
                    // for generic HID communication we don't care...
                   // queryDeviceCapabilities();

                    if (success)
                    {
                        // Open the readHandle to the device
                        Debug.WriteLine("easyHID:findTargetDevice() -> Opening a readHandle to the device");
                        deviceInformation.readHandle = CreateFile(
                            deviceInformation.devicePathName,
                            GENERIC_READ,
                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                            IntPtr.Zero, OPEN_EXISTING,
                            FILE_FLAG_OVERLAPPED,
                            IntPtr.Zero);
                        ThreadPool.BindHandle(deviceInformation.readHandle);
                        // Did we open the readHandle successfully?
                        if (deviceInformation.readHandle.IsInvalid)
                        {
                            
                            Debug.WriteLine("easyHID:findTargetDevice() -> Unable to open a readHandle to the device!");
                        }
                        else
                        {
                            Debug.WriteLine("easyHID:findTargetDevice() -> Opening a writeHandle to the device");
                            deviceInformation.writeHandle = CreateFile(
                                deviceInformation.devicePathName,
                                GENERIC_WRITE,
                                FILE_SHARE_READ | FILE_SHARE_WRITE,
                                IntPtr.Zero,
                                OPEN_EXISTING,
                                0,//FILE_FLAG_WRITE_THROUGH,
                                IntPtr.Zero);

                            // Did we open the writeHandle successfully?
                            if (deviceInformation.writeHandle.IsInvalid)
                            {
                                Debug.WriteLine("easyHID:findTargetDevice() -> Unable to open a writeHandle to the device!");

                                // Attempt to close the writeHandle
                                deviceInformation.writeHandle.Close();
                            }
                            else
                            {
                                // Device is now discovered and ready for use, update the status
                                isDeviceAttached = true;
                                gftEventArgs gftArgs = new gftEventArgs(gftEventArgs.gftEvents.CONNECTED);
                                onUsbEvent(gftArgs); // Throw an event
                            }
                        }
                    }
                }
                else
                {
                    //  The device wasn't detected.
                    Debug.WriteLine("easyHID:findTargetDevice() -> No matching device found!");
                }
            }
            catch (Exception)
            {
                Debug.WriteLine("easyHID:findTargetDevice() -> EXCEPTION: Unknown - device not found");
                
            }
        }

        private void queryDeviceCapabilities()
        {
            IntPtr preparsedData = new System.IntPtr();
            Int32 result = 0;
            Boolean success = false;

            try
            {
                // Get the preparsed data from the HID driver
                success = HidD_GetPreparsedData(deviceInformation.hidUsbHandle, ref preparsedData);

                // Get the HID device's capabilities
                result = HidP_GetCaps(preparsedData, ref deviceInformation.capabilities);
                if ((result != 0))
                {
                    Debug.WriteLine("easyHID:queryDeviceCapabilities() -> Device query results:");
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Usage: {0}",
                        Convert.ToString(deviceInformation.capabilities.Usage, 16)));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Usage Page: {0}",
                        Convert.ToString(deviceInformation.capabilities.UsagePage, 16)));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Input Report Byte Length: {0}",
                        deviceInformation.capabilities.InputReportByteLength));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Output Report Byte Length: {0}",
                        deviceInformation.capabilities.OutputReportByteLength));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Feature Report Byte Length: {0}",
                        deviceInformation.capabilities.FeatureReportByteLength));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Link Collection Nodes: {0}",
                        deviceInformation.capabilities.NumberLinkCollectionNodes));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Input Button Caps: {0}",
                        deviceInformation.capabilities.NumberFeatureButtonCaps));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Input Value Caps: {0}",
                        deviceInformation.capabilities.NumberInputValueCaps));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Input Data Indices: {0}",
                        deviceInformation.capabilities.NumberInputDataIndices));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Output Button Caps: {0}",
                        deviceInformation.capabilities.NumberOutputButtonCaps));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Output Value Caps: {0}",
                        deviceInformation.capabilities.NumberOutputValueCaps));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Output Data Indices: {0}",
                        deviceInformation.capabilities.NumberOutputDataIndices));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Feature Button Caps: {0}",
                        deviceInformation.capabilities.NumberFeatureButtonCaps));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Feature Value Caps: {0}",
                        deviceInformation.capabilities.NumberFeatureValueCaps));
                    Debug.WriteLine(string.Format("easyHID:queryDeviceCapabilities() ->     Number of Feature Data Indices: {0}",
                        deviceInformation.capabilities.NumberFeatureDataIndices));
                }
            }
            catch (Exception)
            {
                // Something went badly wrong... this shouldn't happen, so we throw an exception
                Debug.WriteLine("easyHID:queryDeviceCapabilities() -> EXECEPTION: An unrecoverable error has occurred!");
                MessageBox.Show("queryDeviceCapabilities()");
            }
            finally
            {
                // Free up the memory before finishing
                if (preparsedData != IntPtr.Zero)
                {
                    success = HidD_FreePreparsedData(preparsedData);
                }
            }
        }
        private void detachUsbDevice()
        {
            Debug.WriteLine("easyHID:detachUsbDevice() -> Method called");

            // Is a device currently attached?
            if (isDeviceAttached)
            {
                Debug.WriteLine("easyHID:detachUsbDevice() -> Detaching device and closing file handles");
                // Set the device status to detached;
                isDeviceAttached = false;

                // Close the readHandle, writeHandle and hidHandle
                if (!deviceInformation.hidUsbHandle.IsInvalid) deviceInformation.hidUsbHandle.Close();
                if (!deviceInformation.readHandle.IsInvalid) deviceInformation.readHandle.Close();
                if (!deviceInformation.writeHandle.IsInvalid) deviceInformation.writeHandle.Close();
                //if (deviceInformation.fileStreamDeviceData.CanRead) deviceInformation.fileStreamDeviceData.Close();

                // Throw an event
                gftEventArgs gftArgs = new gftEventArgs(gftEventArgs.gftEvents.DISCONNECTED);
                onUsbEvent(gftArgs); // Throw an event
            }
            else Debug.WriteLine("easyHID:detachUsbDevice() -> No device attached");
        }


        ///  <summary>
        ///   Overrides WndProc to enable checking for and handling WM_DEVICECHANGE messages.
        ///  </summary>
        ///  
        ///  <param name="m"> a Windows Message </param>

        //protected override void WndProc(ref Message m)
       public void checkMessage(ref Message m)
        {
            try
            {
                //  The OnDeviceChange routine processes WM_DEVICECHANGE messages.

                if (m.Msg == Hid.WM_DEVICECHANGE)
                {
                    OnDeviceChange(m);
                }

                //  Let the base form process the message.

                //base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                DisplayException(this.Name, ex);
                MessageBox.Show("WndProc()");
            }
        }

        ///  <summary>
        ///  Called when a WM_DEVICECHANGE message has arrived,
        ///  indicating that a device has been attached or removed.
        ///  </summary>
        ///  
        ///  <param name="m"> a message with information about the device </param>

        internal void OnDeviceChange(Message m)
        {
            Debug.WriteLine("WM_DEVICECHANGE");
            //if (m.Msg != WM_DEVICECHANGE) return;
            try
            {


                if ((m.WParam.ToInt32() == Hid.DBT_DEVICEARRIVAL))// || (m.WParam.ToInt32() == DeviceManagement.DBT_DEVNODES_CHANGED) )
                {
                    //  If WParam contains DBT_DEVICEARRIVAL, a device has been attached.

                    Debug.WriteLine("A device has been attached.");
                    //Debug.WriteLine("easyHID:handleDeviceNotificationMessages() -> New device attached");
                    // If our target device is not currently attached, this could be our device, so we attempt to find it.
                    if (!isDeviceAttached)
                    {
                        findTargetDevice();
                        //onUsbEvent(EventArgs.Empty); // Generate an event
                    }
                   

                }
                else if ((m.WParam.ToInt32() == Hid.DBT_DEVICEREMOVECOMPLETE))
                {

                    //  If WParam contains DBT_DEVICEREMOVAL, a device has been removed.

                    Debug.WriteLine("A device has been removed.");

                    //  Find out if it's the device we're communicating with.
                    if (isNotificationForTargetDevice(m))
                    {
                        // If so detach the USB device.
                        Debug.WriteLine("easyHID:handleDeviceNotificationMessages() -> The target USB device has been removed - detaching...");
                        detachUsbDevice();
                        gftEventArgs gftArgs = new gftEventArgs(gftEventArgs.gftEvents.DISCONNECTED);
                        onUsbEvent(gftArgs); // Throw an event
                    }
                    
                }
                else
                {
                    // Other message
                    //findTargetDevice();
                    Debug.WriteLine("easyHID:handleDeviceNotificationMessages() -> Unknown notification message");
                       
                }
              
            }
            catch (Exception ex)
            {
                DisplayException(this.Name, ex);
                MessageBox.Show("OnDeviceChange()");
            }
        }
        /// <summary>
        /// isNotificationForTargetDevice - Compares the target devices pathname against the
        /// pathname of the device which caused the event message
        /// </summary>
        private Boolean isNotificationForTargetDevice(Message m)
        {
            Int32 stringSize;
            if (deviceInformation.devicePathName == null) return false;
            try
            {
                DEV_BROADCAST_DEVICEINTERFACE_1 devBroadcastDeviceInterface = new DEV_BROADCAST_DEVICEINTERFACE_1();
                DEV_BROADCAST_HDR devBroadcastHeader = new DEV_BROADCAST_HDR();

                Marshal.PtrToStructure(m.LParam, devBroadcastHeader);

                // Is the notification event concerning a device interface?
                if ((devBroadcastHeader.dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE))
                {
                    // Get the device path name of the affected device
                    stringSize = System.Convert.ToInt32((devBroadcastHeader.dbch_size - 32) / 2);
                    devBroadcastDeviceInterface.dbcc_name = new Char[stringSize + 1];
                    Marshal.PtrToStructure(m.LParam, devBroadcastDeviceInterface);
                    String deviceNameString = new String(devBroadcastDeviceInterface.dbcc_name, 0, stringSize);

                    // Compare the device name with our target device's pathname (strings are moved to lower case
                    // using en-US to ensure case insensitivity accross different regions)
                    if ((String.Compare(deviceNameString.ToLower(new System.Globalization.CultureInfo("en-US")),
                        deviceInformation.devicePathName.ToLower(new System.Globalization.CultureInfo("en-US")), true) == 0)) return true;
                    else return false;
                }
            }
            catch (Exception)
            {
                Debug.WriteLine("easyHID:isNotificationForTargetDevice() -> EXCEPTION: An unknown exception has occured!");
                return false;
            }
            return false;
        }

        ///  <summary>
        ///  Remove any Input reports waiting in the buffer.
        ///  </summary>
        ///  
        ///  <param name="hidHandle"> a handle to a device.   </param>
        ///  
        ///  <returns>
        ///  True on success, False on failure.
        ///  </returns>

        internal Boolean FlushQueue(SafeFileHandle hidHandle)
        {
            Boolean success = false;

            try
            {
                //  ***
                //  API function: HidD_FlushQueue

                //  Purpose: Removes any Input reports waiting in the buffer.

                //  Accepts: a handle to the device.

                //  Returns: True on success, False on failure.
                //  ***

                success = HidD_FlushQueue(hidHandle);

                return success;
            }
            catch (Exception ex)
            {
                DisplayException(MODULE_NAME, ex);
                Debug.WriteLine("flushQueue()");
                throw;
            }
        }         
	}
}






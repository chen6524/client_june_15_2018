using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;

namespace easyHID
{
    public partial class Hid
	{
		
        internal const UInt32 FILE_FLAG_OVERLAPPED = 0x40000000;
        internal const UInt32 FILE_SHARE_READ = 1;
        internal const UInt32 FILE_SHARE_WRITE = 2;
        internal const UInt32 GENERIC_READ = 0x80000000;
        internal const UInt32 GENERIC_WRITE = 0x40000000;
        internal const Int32 INVALID_HANDLE_VALUE = -1;
        internal const UInt32 OPEN_EXISTING = 3;
        internal const Int32 WAIT_TIMEOUT = 0x102;
        internal const Int32 WAIT_OBJECT_0 = 0;
        internal const UInt32 FILE_FLAG_WRITE_THROUGH = 0x80000000;
        internal const UInt32 FILE_FLAG_NO_BUFFERING = 0x20000000;
        [StructLayout(LayoutKind.Sequential)]
        internal class SECURITY_ATTRIBUTES
            {
            internal Int32 nLength;
            internal Int32 lpSecurityDescriptor;
            internal Int32 bInheritHandle;
            }

        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern Int32 CancelIo(
            SafeFileHandle hFile
            );

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern IntPtr CreateEvent(
            IntPtr SecurityAttributes,
            Boolean bManualReset,
            Boolean bInitialState,
            String lpName
            );

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern SafeFileHandle CreateFile(
            String lpFileName,
            UInt32 dwDesiredAccess,
            UInt32 dwShareMode,
            IntPtr lpSecurityAttributes,
            UInt32 dwCreationDisposition,
            UInt32 dwFlagsAndAttributes,
            IntPtr hTemplateFile
            );
  
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        unsafe internal static extern Boolean GetOverlappedResult(SafeFileHandle hFile, NativeOverlapped * lpOverlapped, ref Int32 lpNumberOfBytesTransferred, Boolean bWait);
        
        [DllImport("kernel32.dll", SetLastError = true)]
        unsafe internal static extern Boolean ReadFile(SafeFileHandle hFile, IntPtr lpBuffer, Int32 nNumberOfBytesToRead, ref Int32 lpNumberOfBytesRead, NativeOverlapped* lpOverlapped); 

        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern Int32 WaitForSingleObject(
            IntPtr hHandle,
            Int32 dwMilliseconds);

        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern Boolean WriteFile(
            SafeFileHandle hFile,
            Byte[] lpBuffer,
            Int32 nNumberOfBytesToWrite,
            ref Int32 lpNumberOfBytesWritten,
            IntPtr lpOverlapped
            );

        [DllImport("kernel32", SetLastError = true)]
        internal static extern bool CloseHandle(
            IntPtr h
            );

        
	}
} 

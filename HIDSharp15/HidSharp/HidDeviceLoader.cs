﻿#region License
/* Copyright 2010, 2013 James F. Bellinger <http://www.zer7.com/software/hidsharp>

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRinpuANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.ComponentModel;


public class gftEventArgs : EventArgs
{
	public gftEventArgs(gftEvents newEvent) {
		this.gftEvent = newEvent;
	}


	private gftEvents gftEvent;


	public gftEvents getEvent
	{
		get { return this.gftEvent; }
	}

	public enum gftEvents
	{
		CONNECTED,
		DISCONNECTED
	}
}


namespace HidSharp
{
    /// <summary>
    /// Detects USB HID class devices connected to the system.
    /// </summary>
    [ComVisible(true), Guid("CD7CBD7D-7204-473c-AA2A-2B9622CFC6CC")]
    public class HidDeviceLoader
    {

		private DateTime lastReport;
		public bool deviceAttached = false;             // Device attachment state flag

		private HidDevice MyHid; 	// The device

		private int timeOut = 3000;
		private HidStream myStream;
		private int vid;
		private int pid;
        private bool async = false;

        private BackgroundWorker bwAsyncRead;
        public delegate void PacketProcess(ref byte[] buff);
        private PacketProcess async_callback;

		public delegate void usbEventsHandler(object sender, gftEventArgs ge);

		public event usbEventsHandler usbEvent;

        /// <summary>
        /// Initializes a new instance of the <see cref="HidDeviceLoader"/> class.
        /// </summary>
		public HidDeviceLoader(int vid, int pid)
        {
			this.vid = vid;
			this.pid = pid;
			lastReport = DateTime.Now;
			Platform.HidSelector.Instance.usbEvent += new Platform.HidManager.usbEventHandler (onUsbEvent);
			Console.WriteLine (Platform.HidSelector.Instance.getPlatform ());
			Platform.HidSelector.Instance.AddDeviceListeners ();
        }

		public void onUsbEvent(object sender, int e, System.IntPtr handle) {
			switch (e) {
			case 1: // Disconnect
				//checkStatus();
				if (deviceAttached) {
					foreach (HidDevice hDev in GetRemovedDevices()) {
						if (hDev.DevicePath == MyHid.DevicePath) {
							detachUsbDevice ();
						}
					}
				}
				break;
			case 0: // Connect
				if(!deviceAttached) findTargetDevice(vid, pid);
				break;
			}
		}

        public void startAsync(PacketProcess async_callback)
        {
            if (bwAsyncRead == null)
            {
                bwAsyncRead = new BackgroundWorker();
                bwAsyncRead.WorkerReportsProgress = false;
                bwAsyncRead.DoWork += new DoWorkEventHandler(bwAsyncRead_DoWork);
                bwAsyncRead.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwAsync_RunWorkerCompleted);
            }
            this.async_callback = async_callback;
            async = true;
            timeOut = -1;
            if (!bwAsyncRead.IsBusy && async)
                bwAsyncRead.RunWorkerAsync();
        }

        public void stopAsync()
        {
            timeOut = 3000;
            async = false;
        }

        private void bwAsyncRead_DoWork(object sender, DoWorkEventArgs e)
        {
            byte[] buff = new byte[65];
            int nread = 0;
            if (inputReports(ref buff, ref nread))
            {
                byte[] cbuff = new byte[65];
                Array.Copy(buff, 1, cbuff, 0, buff.Length - 1); // Callback expects byte 0 to be trimmed
                if(async_callback != null)
                    async_callback(ref cbuff);
            }
        }
        private void bwAsync_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!bwAsyncRead.IsBusy && async)
                bwAsyncRead.RunWorkerAsync();
        }

		public void checkStatus()
		{
			/*if (deviceAttached) {

				try {
					if (DateTime.Now.AddSeconds (-15) > lastReport) {
						if (myStream != null) myStream.Close();
						if (MyHid.TryOpen (out myStream)) {
							lastReport = DateTime.Now;
						} else {
							detachUsbDevice ();
						}
					}
				} catch {
					detachUsbDevice ();
				}
			}*/
		}
		/// <summary>
		/// The usb event thrower
		/// </summary>
		/// <param name="e"></param>
		protected virtual void onUsbEvent(gftEventArgs e)
		{
			if (usbEvent != null)
			{
				System.Diagnostics.Debug.WriteLine("HIDSharp:onUsbEvent() -> Throwing a USB event to a listener");
				usbEvent(this, e);
			}
			else System.Diagnostics.Debug.WriteLine("HIDSharp:onUsbEvent() -> Attempted to throw a USB event, but no one was listening");
		}

        /// <summary>
        /// Gets a list of connected USB devices.
        /// This overload is meant for Visual Basic 6 and COM clients.
        /// </summary>
        /// <returns>The device list.</returns>
        public IEnumerable GetDevicesVB()
        {
            return GetDevices();
        }

        /// <summary>
        /// Gets a list of connected USB devices.
        /// </summary>
        /// <returns>The device list.</returns>
        public IEnumerable<HidDevice> GetDevices()
        {
            return Platform.HidSelector.Instance.GetDevices();
        }

		public IEnumerable<HidDevice> GetRemovedDevices()
		{
			return Platform.HidSelector.Instance.GetRemovedDevices();
		}

        /// <summary>
        /// Gets a list of connected USB devices, filtered by some criteria.
        /// </summary>
        /// <param name="vendorID">The vendor ID, or null to not filter by vendor ID.</param>
        /// <param name="productID">The product ID, or null to not filter by product ID.</param>
        /// <param name="productVersion">The product version, or null to not filter by product version.</param>
        /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
        /// <returns>The filtered device list.</returns>
        public IEnumerable<HidDevice> GetDevices
            (int? vendorID = null, int? productID = null, int? productVersion = null, string serialNumber = null)
        {
            int vid = vendorID ?? -1, pid = productID ?? -1, ver = productVersion ?? -1;
            foreach (HidDevice hid in GetDevices())
            {
                if ((hid.VendorID == vendorID || vid < 0) &&
                    (hid.ProductID == productID || pid < 0) &&
                    (hid.ProductVersion == productVersion || ver < 0) &&
                    (hid.SerialNumber == serialNumber || string.IsNullOrEmpty(serialNumber)))
                {
                    yield return hid;
                }
            }
        }

        /// <summary>
        /// Gets the first connected USB device that matches specified criteria.
        /// </summary>
        /// <param name="vendorID">The vendor ID, or null to not filter by vendor ID.</param>
        /// <param name="productID">The product ID, or null to not filter by product ID.</param>
        /// <param name="productVersion">The product version, or null to not filter by product version.</param>
        /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
        /// <returns>The device, or null if none was found.</returns>
        public HidDevice GetDeviceOrDefault
            (int? vendorID = null, int? productID = null, int? productVersion = null, string serialNumber = null)
        {
            return GetDevices(vendorID, productID, productVersion, serialNumber).FirstOrDefault();
        }

		public void findTargetDevice(int vid, int pid) {
			try {
				MyHid = GetDeviceOrDefault(vid, pid);
				if(MyHid != null) {
					if (MyHid.SerialNumber != null) {
						// Device is now discovered and ready for use, update the status

						byte[] outputReportBuffer = new byte[65];
						outputReportBuffer[0] = (byte)0;
						outputReportBuffer[1] = 0x18;
						outputReportBuffer[2] = (byte) 0;
						int numberOfBytesWritten = 0;
						if (myStream != null) myStream.Close();
						Thread.Sleep(1000);
						if (MyHid.TryOpen (out myStream)) {
							Console.WriteLine("Connected");
							deviceAttached = true;
							gftEventArgs gftArgs = new gftEventArgs (gftEventArgs.gftEvents.CONNECTED);
							onUsbEvent (gftArgs); // Throw an event
						}
					}
				}
			}
			catch(Exception e) {
				Console.WriteLine ("findTargetDevice: " + e.Message);
			}
		}

		public void setTimeOut(int timeOut)
		{
			this.timeOut = timeOut;
		}
		public bool inputReports(ref byte[] inputReportBuffer, ref int numberRead)
		{
				lastReport = DateTime.Now;
				bool result = false;
				bool success =false;
				int numberOfBytesRead = 0;
				byte[] inputReportBuffer2 = new byte[64];

				// Make sure a device is attached
				if (!deviceAttached)
				{
					System.Diagnostics.Debug.WriteLine("HIDSharp:readReportFromDevice(): -> No device attached!");
					return false;
				}
			
				try
				{
					myStream.ReadTimeout = this.timeOut;

					if(myStream.Read(inputReportBuffer2, numberOfBytesRead, inputReportBuffer2.Length) > 0) {
						success = true;//
						numberOfBytesRead += inputReportBuffer2.Length;
					}
					else success = false;
					if (!success)
					{
						// We are now waiting for the FileRead to complete
						System.Diagnostics.Debug.WriteLine("HIDSharp:readReportFromDevice(): -> ReadFile started, waiting for completion...");

						numberOfBytesRead += inputReportBuffer2.Length;
						numberRead = numberOfBytesRead;

						if (result)
						{
							success = true;

							System.Diagnostics.Debug.WriteLine(string.Format("HIDSharp:readReportFromDevice(): -> ReadFile successful (overlapped) {0} bytes read", numberOfBytesRead));
						}
						else
						{
							success = false;
							numberRead = 0;
							// Detach the USB device to try to get us back in a known state
							detachUsbDevice();
							System.Diagnostics.Debug.WriteLine("HIDSharp:readReportFromDevice(): -> ReadFile timedout!");
						}

					}
					if (success)
					{
						// Report receieved correctly, for MacOSX we must pad it at the beginning with a 0 byte
					if (Platform.HidSelector.Instance.getPlatform() == "Mac") 
                            Array.Copy(inputReportBuffer2, 0, inputReportBuffer, 1, inputReportBuffer2.Length);
                        else
                            Array.Copy(inputReportBuffer2, 0, inputReportBuffer, 0, inputReportBuffer2.Length);
						System.Diagnostics.Debug.WriteLine(string.Format("HIDSharp:readReportFromDevice(): -> ReadFile successful {0} bytes read", numberOfBytesRead));
					}
				}
				catch (Exception e)
				{
					// An error - send out some debug and return failure
				System.Diagnostics.Debug.WriteLine("HIDSharp:readReportFromDevice(): -> EXCEPTION: When attempting to receive an input report");
					return false;
				}
			lastReport = DateTime.Now;
			return success;

		}
		public bool OutputReports(byte[] outputReportBuffer)
		{
			lastReport = DateTime.Now;
			int numberOfBytesWritten = 0;

			// Make sure a device is attached
			if (!deviceAttached)
			{
				System.Diagnostics.Debug.WriteLine("HIDSharp:writeReportToDevice(): -> No device attached!");
				return false;
			}
            int retry = 0;
            bool returnval = false;
            while (deviceAttached && !returnval && retry < 5)
            {
                try
                {
                    if (Platform.HidSelector.Instance.getPlatform() == "Mac")
                        myStream.Write(outputReportBuffer, numberOfBytesWritten + 1, outputReportBuffer.Length - 1); // MacOSX HID needs us to truncate 1 Byte for the GFT's hid reports..
                    else
                        myStream.Write(outputReportBuffer, numberOfBytesWritten, outputReportBuffer.Length);

                    lastReport = DateTime.Now;
                    returnval = true;
                }
                catch (Exception e)
                {
                    // An error - send out some debug and return failure
                    System.Diagnostics.Debug.WriteLine("HIDSharp:writeReportToDevice(): -> EXCEPTION: When attempting to send an output report");
                    //detachUsbDevice ();
                    //return false;
                    Thread.Sleep(1000); // Sleep for a second..
                    retry++;
                }
            }
            return returnval;
		}
		public void detachUsbDevice() {
            if (deviceAttached)
            {
                deviceAttached = false;
                onUsbEvent(new gftEventArgs(gftEventArgs.gftEvents.DISCONNECTED)); // Throw an event
            }
		}
    }
}
